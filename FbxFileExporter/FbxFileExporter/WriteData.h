#pragma once
#include <fbxsdk.h>
#include <iostream>

extern FILE *BinaryFile;

char *ReplaceBlank(const char *value, const char replace);

void WriteString(const char *value);
void WriteString(char *value);
void WriteString(const char *value1, const char *value2);
void WriteIntString(int ivalue, const char *svalue);
void WriteIntString(const char *header, int ivalue, const char *svalue);
void WriteIntString(const char *header, int ivalue, const char *svalue1, const char *svalue2);
void WriteString(char *header, char *value1, char *value2, char *value3);
void WriteIntFloat(const char *svalue, int ivalue0, int ivalue1, float fvalue);
void WriteInt(int value);
void WriteInt(const char *svalue, int ivalue);
void WriteInt(const char *svalue, int ivalue0, int ivalue1);
void WriteInt(int value0, int value1, int value2, int value3);
void WriteFloat(float value);
void WriteFloat(float value0, float value1);
void WriteFloat(float value0, float value1, float value2, float value3);
void WriteFloat(double value);
void WriteFloat(double value1, double value2);
void WriteFloat(double value1, double value2, double value3);
void WriteFloat(double value1, double value2, double value3, double value4);
void WriteFloat(char *header, float value);
void WriteDouble(double value);
void WriteDouble(char *header, double value);
void WriteMatrix(FbxAMatrix value);
void WriteMatrix(char *header, FbxDouble4x4 value);
void Write2DVector(FbxVector2 value);
void Write2DVector(char *header, FbxVector2 value);
void Write2DVector(char *header, FbxDouble2 value);
void WriteUVVector2D(FbxVector2 value);
void Write3DVector(FbxVector4 value);
void Write4DVector(char *header, FbxDouble4 value);
void Write3DVector(char *header, FbxDouble3 value);

void WriteTransform(const char *header, FbxAMatrix mvalue, FbxDouble3 dvalue0, FbxDouble3 dvalue1, FbxDouble3 dvalue2);

void WriteBool(bool bValue);
void WriteBool(char *pszHeader, bool bValue);

void WriteColor(FbxPropertyT<FbxDouble3> value);
void WriteColor(FbxColor value);
void WriteColor(char *header, FbxPropertyT<FbxDouble3> value);
void WriteColor(char *header, FbxColor value);