#include "WriteData.h"

FbxString FbxNameString;

char *ReplaceBlank(const char *value, const char replace)
{
	int nLength = (int)strlen(value);

	if (nLength > 0)
	{
		FbxNameString = value;
		FbxNameString.ReplaceAll(' ', replace);
	}
	else
	{
		FbxNameString = "Null";
	}

	return(FbxNameString.Buffer());
}

void WriteString(const char *value)
{
	int Length = (int)strlen(value);

	::fwrite(&Length, sizeof(int), 1, BinaryFile);
	::fwrite(value, sizeof(char), Length, BinaryFile);
}

void WriteString(char *value)
{
	int Length = (int)strlen(value);

	::fwrite(&Length, sizeof(int), 1, BinaryFile);
	::fwrite(value, sizeof(char), Length, BinaryFile);
}

void WriteString(const char *value1, const char *value2)
{
	WriteString(value1);
	WriteString(value2);
}

void WriteString(char *header, char *value1, char *value2, char *value3)
{
	WriteString(header);
	WriteString(value1);
	WriteString(value2);
	WriteString(value3);
}

void WriteIntString(int value, const char *svalue)
{
	::fwrite(&value, sizeof(int), 1, BinaryFile);
	WriteString(svalue);
}

void WriteIntString(const char *header, int ivalue, const char *svalue)
{
	WriteString(header);
	::fwrite(&ivalue, sizeof(int), 1, BinaryFile);
	WriteString(svalue);
}

void WriteIntString(const char *header, int ivalue, const char *svalue1, const char *svalue2)
{
	WriteString(header);
	::fwrite(&ivalue, sizeof(int), 1, BinaryFile);
	WriteString(svalue1);
	WriteString(svalue2);
}

void WriteIntFloat(const char *svalue, int ivalue0, int ivalue1, float fvalue)
{
	WriteString(svalue);
	WriteInt(ivalue0);
	WriteInt(ivalue1);
	WriteFloat(fvalue);
}


void WriteInt(int value)
{
	::fwrite(&value, sizeof(int), 1, BinaryFile);
}

void WriteInt(const char *svalue, int ivalue)
{
	WriteString(svalue);
	WriteInt(ivalue);
}

void WriteInt(const char *svalue, int ivalue0, int ivalue1)
{
	WriteString(svalue);
	WriteInt(ivalue0);
	WriteInt(ivalue1);
}

void WriteInt(int value0, int value1, int value2, int value3)
{
	WriteInt(value0);
	WriteInt(value1);
	WriteInt(value2);
	WriteInt(value3);
}

void WriteFloat(float value)
{
	::fwrite(&value, sizeof(float), 1, BinaryFile);
}

void WriteFloat(float value0, float value1)
{
	WriteFloat(value0);
	WriteFloat(value1);
}

void WriteFloat(float value0, float value1, float value2, float value3)
{
	WriteFloat(value0);
	WriteFloat(value1);
	WriteFloat(value2);
	WriteFloat(value3);
}

void WriteFloat(double value)
{
	float fValue = (float)value;
	::fwrite(&fValue, sizeof(float), 1, BinaryFile);
}

void WriteFloat(double value1, double value2)
{
	float fValue1 = (float)value1;
	float fValue2 = (float)value2;
	::fwrite(&fValue1, sizeof(float), 1, BinaryFile);
	::fwrite(&fValue2, sizeof(float), 1, BinaryFile);
}

void WriteFloat(double value1, double value2, double value3)
{
	float fValue1 = (float)value1;
	float fValue2 = (float)value2;
	float fValue3 = (float)value3;
	::fwrite(&fValue1, sizeof(float), 1, BinaryFile);
	::fwrite(&fValue2, sizeof(float), 1, BinaryFile);
	::fwrite(&fValue3, sizeof(float), 1, BinaryFile);
}

void WriteFloat(double value1, double value2, double value3, double value4)
{
	float fValue1 = (float)value1;
	float fValue2 = (float)value2;
	float fValue3 = (float)value3;
	float fValue4 = (float)value4;
	::fwrite(&fValue1, sizeof(float), 1, BinaryFile);
	::fwrite(&fValue2, sizeof(float), 1, BinaryFile);
	::fwrite(&fValue3, sizeof(float), 1, BinaryFile);
	::fwrite(&fValue4, sizeof(float), 1, BinaryFile);
}

void WriteFloat(char *header, float value)
{
	WriteString(header);
	WriteFloat(value);
}

void WriteDouble(double value)
{
	::fwrite(&value, sizeof(double), 1, BinaryFile);
}

void WriteDouble(char *header, double value)
{
	WriteString(header);
	WriteDouble(value);
}

void WriteMatrix(FbxAMatrix value)
{
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++) WriteFloat((float)value[i][j]);
	}
}

void WriteMatrix(char *header, FbxDouble4x4 value)
{
	WriteString(header);
	for (int i = 0; i < 4; i++)
	{
		for (int j = 0; j < 4; j++) WriteFloat((float)value[i][j]);
	}
}

void Write2DVector(FbxVector2 value)
{
	float value1 = (float)value[0];
	float value2 = (float)value[1];
	::fwrite(&value1, sizeof(float), 1, BinaryFile);
	::fwrite(&value2, sizeof(float), 1, BinaryFile);
}

void Write2DVector(char *header, FbxVector2 value)
{
	WriteString(header);
	Write2DVector(value);
}

void Write2DVector(char *header, FbxDouble2 value)
{
	WriteString(header);
	FbxVector2 vectorvalue;
	vectorvalue[0] = value[0];
	vectorvalue[1] = vectorvalue[1];
	Write2DVector(vectorvalue);
}

void WriteUVVector2D(FbxVector2 value)
{
	for (int i = 0; i < 2; ++i)
		WriteFloat((float)value[i]);
}

void Write3DVector(FbxVector4 value)
{
	float value1 = (float)value[0];
	float value2 = (float)value[1];
	float value3 = (float)value[2];

	::fwrite(&value1, sizeof(float), 1, BinaryFile);
	::fwrite(&value2, sizeof(float), 1, BinaryFile);
	::fwrite(&value3, sizeof(float), 1, BinaryFile);
}

void Write4DVector(char *header, FbxDouble4 value)
{
	WriteString(header);

	float value1 = (float)value[0];
	float value2 = (float)value[1];
	float value3 = (float)value[2];
	float value4 = (float)value[3];

	::fwrite(&value1, sizeof(float), 1, BinaryFile);
	::fwrite(&value2, sizeof(float), 1, BinaryFile);
	::fwrite(&value3, sizeof(float), 1, BinaryFile);
	::fwrite(&value4, sizeof(float), 1, BinaryFile);
}

void Write3DVector(char *header, FbxDouble3 value)
{
	WriteString(header);

	float value1 = (float)value[0];
	float value2 = (float)value[1];
	float value3 = (float)value[2];

	::fwrite(&value1, sizeof(float), 1, BinaryFile);
	::fwrite(&value2, sizeof(float), 1, BinaryFile);
	::fwrite(&value3, sizeof(float), 1, BinaryFile);
}

void WriteTransform(const char *header, FbxAMatrix mvalue, FbxDouble3 dvalue0, FbxDouble3 dvalue1, FbxDouble3 dvalue2)
{
	WriteString(header);
	WriteMatrix(mvalue);
	Write3DVector(dvalue0);
	Write3DVector(dvalue1);
	Write3DVector(dvalue2);
}

void WriteBool(bool bValue)
{
	int nValue = (bValue) ? 1 : 0;
	::fwrite(&nValue, sizeof(int), 1, BinaryFile);
}

void WriteBool(char *pszHeader, bool bValue)
{
	WriteString(pszHeader);
	WriteBool(bValue);
}

void WriteColor(FbxPropertyT<FbxDouble3> value)
{
	float value1 = (float)value.Get()[0];
	::fwrite(&value1, sizeof(float), 1, BinaryFile);
	float value2 = (float)value.Get()[1];
	::fwrite(&value2, sizeof(float), 1, BinaryFile);
	float value3 = (float)value.Get()[2];
	::fwrite(&value3, sizeof(float), 1, BinaryFile);
}

void WriteColor(char *header, FbxPropertyT<FbxDouble3> value)
{
	WriteString(header);
	WriteColor(value);
}

void WriteColor(FbxColor value)
{
	float value1 = (float)value.mRed;
	::fwrite(&value1, sizeof(float), 1, BinaryFile);
	float value2 = (float)value.mGreen;
	::fwrite(&value2, sizeof(float), 1, BinaryFile);
	float value3 = (float)value.mBlue;
	::fwrite(&value3, sizeof(float), 1, BinaryFile);
}

void WriteColor(char *header, FbxColor value)
{
	WriteString(header);
	WriteColor(value);
}
