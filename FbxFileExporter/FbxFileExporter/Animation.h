#pragma once
#include "WriteData.h"

void WriteAnimation(FbxScene *Scene);

//여러 애니메이션 정보를 한번에 저장할 때 사용할 거?
//사실 모델 자체에 여러 애니메이션 정보가 같이 있음
//그러니 매니저 자체도 여러개로 만들어서 각각의 fbx 파일에 있는 애니메이션을 모아서 한번에 저장하자
void WriteAnimation(FbxScene **pfbxScenes, int nSeparatedAnimations);