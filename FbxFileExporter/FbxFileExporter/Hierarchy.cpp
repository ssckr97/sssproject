#include "Hierarchy.h"
FbxAMatrix GetGeometryOffsetTransform(FbxNode* pNode)
{
	const FbxVector4 lT = pNode->GetGeometricTranslation(FbxNode::eSourcePivot);
	const FbxVector4 lR = pNode->GetGeometricRotation(FbxNode::eSourcePivot);
	const FbxVector4 lS = pNode->GetGeometricScaling(FbxNode::eSourcePivot);

	return FbxAMatrix(lT, lR, lS);
}

void WriteSkinDeformations(FbxMesh *Mesh)
{
	FbxSkin *SkinDeformer = (FbxSkin *)Mesh->GetDeformer(0, FbxDeformer::eSkin);
	int ClusterCount = SkinDeformer->GetClusterCount();

	WriteInt("<BoneNames>:", ClusterCount);

	//뼈 이름 모두 저장
	for (int j = 0; j < ClusterCount; j++)
	{
		FbxCluster *Cluster = SkinDeformer->GetCluster(j);

		FbxNode *ClusterLinkNode = Cluster->GetLink();
		WriteString(ReplaceBlank((char *)ClusterLinkNode->GetName(), '_'));
	}

	WriteInt("<BoneOffsets>:", ClusterCount);

	//뼈의 오프셋 행렬 저장
	FbxAMatrix GeometryOffset = GetGeometryOffsetTransform(Mesh->GetNode());
	for (int j = 0; j < ClusterCount; j++)
	{
		FbxCluster *Cluster = SkinDeformer->GetCluster(j);

		FbxAMatrix BindPoseMeshToRoot; //Cluster Transform
		Cluster->GetTransformMatrix(BindPoseMeshToRoot);
		FbxAMatrix BindPoseBoneToRoot; //Cluster Link Transform
		Cluster->GetTransformLinkMatrix(BindPoseBoneToRoot);

		FbxAMatrix VertextToLinkNode = BindPoseBoneToRoot.Inverse() * BindPoseMeshToRoot * GeometryOffset;

		WriteMatrix(VertextToLinkNode);
	}

	int ControlPointCount = Mesh->GetControlPointsCount();

	int *BonesPerVertex = new int[ControlPointCount];
	::memset(BonesPerVertex, 0, ControlPointCount * sizeof(int));
	
	//각 controlpoint 마다 영향을 주는 뼈의 개수 저장
	for (int j = 0; j < ClusterCount; j++)
	{
		FbxCluster *Cluster = SkinDeformer->GetCluster(j);

		int ControlPointIndices = Cluster->GetControlPointIndicesCount();
		int *pControlPointIndices = Cluster->GetControlPointIndices();
		for (int k = 0; k < ControlPointIndices; k++) BonesPerVertex[pControlPointIndices[k]] += 1;
	}
	//	int nMaxBonesPerVertex = 0;
	//	for (int i = 0; i < nControlPoints; i++) if (pnBonesPerVertex[i] > nMaxBonesPerVertex) nMaxBonesPerVertex = pnBonesPerVertex[i];

	int **BoneIDs = new int*[ControlPointCount];
	float **BoneWeights = new float*[ControlPointCount];

	//controlpoint에 영향을 주는 뼈의 이름과 그 뼈가 가하는 가중치 공간 할당
	for (int i = 0; i < ControlPointCount; i++)
	{
		BoneIDs[i] = new int[BonesPerVertex[i]];
		BoneWeights[i] = new float[BonesPerVertex[i]];
		::memset(BoneIDs[i], 0, BonesPerVertex[i] * sizeof(int));
		::memset(BoneWeights[i], 0, BonesPerVertex[i] * sizeof(float));
	}

	int *Bones = new int[ControlPointCount];
	::memset(Bones, 0, ControlPointCount * sizeof(int));

	//가중치랑 뼈 id 값 채워넣기
	for (int j = 0; j < ClusterCount; j++)
	{
		FbxCluster *Cluster = SkinDeformer->GetCluster(j);

		int ControlPointIndices = Cluster->GetControlPointIndicesCount();
		int *pControlPointIndices = Cluster->GetControlPointIndices();
		double *ControlPointWeights = Cluster->GetControlPointWeights();

		for (int k = 0; k < ControlPointIndices; k++)
		{
			int nVertex = pControlPointIndices[k];
			BoneIDs[nVertex][Bones[nVertex]] = j;
			BoneWeights[nVertex][Bones[nVertex]] = (float)ControlPointWeights[k];
			Bones[nVertex] += 1;
		}
	}

	//가중치 크기가 큰 순서대로 정렬
	for (int i = 0; i < ControlPointCount; i++)
	{
		for (int j = 0; j < BonesPerVertex[i] - 1; j++)
		{
			for (int k = j + 1; k < BonesPerVertex[i]; k++)
			{
				if (BoneWeights[i][j] < BoneWeights[i][k])
				{
					float Temp = BoneWeights[i][j];
					BoneWeights[i][j] = BoneWeights[i][k];
					BoneWeights[i][k] = Temp;
					int nTemp = BoneIDs[i][j];
					BoneIDs[i][j] = BoneIDs[i][k];
					BoneIDs[i][k] = nTemp;
				}
			}
		}
	}

	int(*SkinningIndices)[4] = new int[ControlPointCount][4];
	float(*SkinningWeights)[4] = new float[ControlPointCount][4];

	//가장 큰 가중치 부터 4개까지만 저장하는 공간으로 값 옮기기
	for (int i = 0; i < ControlPointCount; i++)
	{
		::memset(SkinningIndices[i], 0, 4 * sizeof(int));
		::memset(SkinningWeights[i], 0, 4 * sizeof(float));

		for (int j = 0; j < BonesPerVertex[i]; j++)
		{
			if (j < 4)
			{
				SkinningIndices[i][j] = BoneIDs[i][j];
				SkinningWeights[i][j] = BoneWeights[i][j];
			}
		}
	}
	
	//4개의 가중치 까지만 영향을 주는 것으로 계산함, 큰 영향을 끼치는 4개의 가중치가 합이 1이 되도록 값 저장
	for (int i = 0; i < ControlPointCount; i++)
	{
		float SumOfBoneWeights = 0.0f;
		for (int j = 0; j < 4; j++) SumOfBoneWeights += SkinningWeights[i][j];
		for (int j = 0; j < 4; j++) SkinningWeights[i][j] /= SumOfBoneWeights;
	}

	// 영향을 주는 뼈의 인덱스를 저장함
	WriteInt("<BoneIndices>:", ControlPointCount);
	for (int i = 0; i < ControlPointCount; i++)
	{
		WriteInt(SkinningIndices[i][0], SkinningIndices[i][1], SkinningIndices[i][2], SkinningIndices[i][3]);
	}

	// 영향을 주는 뼈의 가중치 값을 저장
	WriteInt("<BoneWeights>:", ControlPointCount);
	for (int i = 0; i < ControlPointCount; i++)
	{
		WriteFloat(SkinningWeights[i][0], SkinningWeights[i][1], SkinningWeights[i][2], SkinningWeights[i][3]);
	}

	for (int i = 0; i < ControlPointCount; i++)
	{
		if (BoneIDs[i]) delete[] BoneIDs[i];
		if (BoneWeights[i]) delete[] BoneWeights[i];
	}
	if (BoneIDs) delete[] BoneIDs;
	if (BoneWeights) delete[] BoneWeights;

	if (Bones) delete[] Bones;
	if (BonesPerVertex) delete[]BonesPerVertex;

	if (SkinningIndices) delete[] SkinningIndices;
	if (SkinningWeights) delete[] SkinningWeights;
}

void WriteControlPoints(FbxMesh *pfbxMesh, int nControlPoints)
{
	WriteInt("<ControlPoints>:", nControlPoints);
	FbxVector4 *ControlPoints = pfbxMesh->GetControlPoints();
	for (int i = 0; i < nControlPoints; i++) Write3DVector(ControlPoints[i]);
}

void WriteControlPointUVs(FbxMesh *pfbxMesh, int nControlPoints)
{
	FbxVector2 *pControlPointUVs = new FbxVector2[nControlPoints];

	int nUVsPerVertex = pfbxMesh->GetElementUVCount(); //UVs Per Polygon's Vertex
	nUVsPerVertex = 1;
	WriteInt("<UVs>:", nControlPoints, nUVsPerVertex);

	if (nUVsPerVertex > 0)
	{
		int nPolygons = pfbxMesh->GetPolygonCount();

		for (int k = 0; k < 1; k++)
		{
			WriteInt("<UV>:", k);
			FbxGeometryElementUV *pfbxElementUV = pfbxMesh->GetElementUV(k);
			for (int i = 0; i < nPolygons; i++)
			{
				int nPolygonSize = pfbxMesh->GetPolygonSize(i); //Triangle: 3, Triangulate()
				for (int j = 0; j < nPolygonSize; j++)
				{
					int nControlPointIndex = pfbxMesh->GetPolygonVertex(i, j);
					switch (pfbxElementUV->GetMappingMode())
					{
					case FbxGeometryElement::eByControlPoint:
					{
						switch (pfbxElementUV->GetReferenceMode())
						{
						case FbxGeometryElement::eDirect:
							pControlPointUVs[nControlPointIndex] = pfbxElementUV->GetDirectArray().GetAt(nControlPointIndex);
							break;
						case FbxGeometryElement::eIndexToDirect:
							pControlPointUVs[nControlPointIndex] = pfbxElementUV->GetDirectArray().GetAt(pfbxElementUV->GetIndexArray().GetAt(nControlPointIndex));
							break;
						default:
							break;
						}
						break;
					}
					case FbxGeometryElement::eByPolygonVertex:
					{
						int nTextureUVIndex = pfbxMesh->GetTextureUVIndex(i, j);
						switch (pfbxElementUV->GetReferenceMode())
						{
						case FbxGeometryElement::eDirect:
						case FbxGeometryElement::eIndexToDirect:
							pControlPointUVs[nControlPointIndex] = pfbxElementUV->GetDirectArray().GetAt(nTextureUVIndex);
							break;
						default:
							break;
						}
						break;
					}
					case FbxGeometryElement::eByPolygon:
					case FbxGeometryElement::eAllSame:
					case FbxGeometryElement::eNone:
						break;
					default:
						break;
					}
				}
			}
			for (int i = 0; i < nControlPoints; i++) WriteUVVector2D(pControlPointUVs[i]);

		}

		
	}
}

void WriteControlPointNormals(FbxMesh *pfbxMesh, int nControlPoints)
{
	FbxVector4 *pControlPointNormals = new FbxVector4[nControlPoints];

	int nNormalsPerVertex = pfbxMesh->GetElementNormalCount();
	WriteInt("<Normals>:", nControlPoints, nNormalsPerVertex);

	if (nNormalsPerVertex > 0)
	{
		int nPolygons = pfbxMesh->GetPolygonCount();

		for (int k = 0; k < 1; k++)
		{
			WriteInt("<Normal>:", k);

			FbxGeometryElementNormal *pfbxElementNormal = pfbxMesh->GetElementNormal(k);
			if (pfbxElementNormal->GetMappingMode() == FbxGeometryElement::eByControlPoint)
			{
				if (pfbxElementNormal->GetReferenceMode() == FbxGeometryElement::eDirect)
				{
					for (int i = 0; i < nControlPoints; i++) pControlPointNormals[i] = pfbxElementNormal->GetDirectArray().GetAt(i);
				}
			}
			else if (pfbxElementNormal->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
			{
				for (int i = 0, nVertexID = 0; i < nPolygons; i++)
				{
					int nPolygonSize = pfbxMesh->GetPolygonSize(i);
					for (int j = 0; j < nPolygonSize; j++, nVertexID++)
					{
						int nControlPointIndex = pfbxMesh->GetPolygonVertex(i, j);
						switch (pfbxElementNormal->GetReferenceMode())
						{
						case FbxGeometryElement::eDirect:
							pControlPointNormals[nControlPointIndex] = pfbxElementNormal->GetDirectArray().GetAt(nVertexID);
							break;
						case FbxGeometryElement::eIndexToDirect:
							pControlPointNormals[nControlPointIndex] = pfbxElementNormal->GetDirectArray().GetAt(pfbxElementNormal->GetIndexArray().GetAt(nVertexID));
							break;
						default:
							break;
						}
					}
				}
			}
		}
		for (int i = 0; i < nControlPoints; i++) Write3DVector(pControlPointNormals[i]);
	}
}

void WriteControlPointTangents(FbxMesh *pfbxMesh, int nControlPoints)
{
	FbxVector4 *pControlPointTangents = new FbxVector4[nControlPoints];

	int nTangentsPerVertex = pfbxMesh->GetElementTangentCount();
	WriteInt("<Tangents>:", nControlPoints, nTangentsPerVertex);

	if (nTangentsPerVertex > 0)
	{
		int nPolygons = pfbxMesh->GetPolygonCount();

		for (int k = 0; k < 1; k++)
		{
			WriteInt("<Tangent>:", k);

			FbxGeometryElementTangent *pfbxElementTangent = pfbxMesh->GetElementTangent(k);
			if (pfbxElementTangent->GetMappingMode() == FbxGeometryElement::eByControlPoint)
			{
				if (pfbxElementTangent->GetReferenceMode() == FbxGeometryElement::eDirect)
				{
					for (int i = 0; i < nControlPoints; i++) pControlPointTangents[i] = pfbxElementTangent->GetDirectArray().GetAt(i);
				}
			}
			else if (pfbxElementTangent->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
			{
				for (int i = 0, nVertexID = 0; i < nPolygons; i++)
				{
					int nPolygonSize = pfbxMesh->GetPolygonSize(i);
					for (int j = 0; j < nPolygonSize; j++, nVertexID++)
					{
						int nControlPointIndex = pfbxMesh->GetPolygonVertex(i, j);
						switch (pfbxElementTangent->GetReferenceMode())
						{
						case FbxGeometryElement::eDirect:
							pControlPointTangents[nControlPointIndex] = pfbxElementTangent->GetDirectArray().GetAt(nVertexID);
							break;
						case FbxGeometryElement::eIndexToDirect:
							pControlPointTangents[nControlPointIndex] = pfbxElementTangent->GetDirectArray().GetAt(pfbxElementTangent->GetIndexArray().GetAt(nVertexID));
							break;
						default:
							break;
						}
					}
				}
			}
		}
		for (int i = 0; i < nControlPoints; i++) Write3DVector(pControlPointTangents[i]);
	}
}

void WriteControlPointBiTangents(FbxMesh *pfbxMesh, int nControlPoints)
{
	FbxVector4 *pControlPointBiTangents = new FbxVector4[nControlPoints];

	int nBiTangentsPerVertex = pfbxMesh->GetElementBinormalCount();
	WriteInt("<BiTangents>:", nControlPoints, nBiTangentsPerVertex);

	if (nBiTangentsPerVertex > 0)
	{
		int nPolygons = pfbxMesh->GetPolygonCount();

		for (int k = 0; k < 1; k++)
		{
			WriteInt("<BiTangent>:", k);

			FbxGeometryElementBinormal *pfbxElementBiTangent = pfbxMesh->GetElementBinormal(k);
			if (pfbxElementBiTangent->GetMappingMode() == FbxGeometryElement::eByControlPoint)
			{
				if (pfbxElementBiTangent->GetReferenceMode() == FbxGeometryElement::eDirect)
				{
					for (int i = 0; i < nControlPoints; i++) pControlPointBiTangents[i] = pfbxElementBiTangent->GetDirectArray().GetAt(i);
				}
			}
			else if (pfbxElementBiTangent->GetMappingMode() == FbxGeometryElement::eByPolygonVertex)
			{
				for (int i = 0, nVertexID = 0; i < nPolygons; i++)
				{
					int nPolygonSize = pfbxMesh->GetPolygonSize(i);
					for (int j = 0; j < nPolygonSize; j++, nVertexID++)
					{
						int nControlPointIndex = pfbxMesh->GetPolygonVertex(i, j);
						switch (pfbxElementBiTangent->GetReferenceMode())
						{
						case FbxGeometryElement::eDirect:
							pControlPointBiTangents[nControlPointIndex] = pfbxElementBiTangent->GetDirectArray().GetAt(nVertexID);
							break;
						case FbxGeometryElement::eIndexToDirect:
							pControlPointBiTangents[nControlPointIndex] = pfbxElementBiTangent->GetDirectArray().GetAt(pfbxElementBiTangent->GetIndexArray().GetAt(nVertexID));
							break;
						default:
							break;
						}
					}
				}
			}
		}
		for (int i = 0; i < nControlPoints; i++) Write3DVector(pControlPointBiTangents[i]);
	}
}

void WritePolygonVertexIndices(FbxMesh *pfbxMesh, int nPolygons)
{
	int nPolygonIndices = nPolygons * 3; //Triangle: 3, Triangulate(), nIndices = nPolygons * 3

	int *pnPolygonIndices = new int[nPolygonIndices];
	for (int i = 0, k = 0; i < nPolygons; i++)
	{
		for (int j = 0; j < 3; j++) pnPolygonIndices[k++] = pfbxMesh->GetPolygonVertex(i, j);
	}

	FbxNode *pfbxNode = pfbxMesh->GetNode();
	int nMaterials = pfbxNode->GetMaterialCount();
	WriteInt("<SubIndices>:", nPolygonIndices, nMaterials);

	if (nMaterials > 1)
	{
		int nElementMaterials = pfbxMesh->GetElementMaterialCount();
		for (int i = 0; i < nElementMaterials; i++)
		{
			FbxGeometryElementMaterial *pfbxElementMaterial = pfbxMesh->GetElementMaterial(i);
			FbxGeometryElement::EReferenceMode nReferenceMode = pfbxElementMaterial->GetReferenceMode();
			switch (nReferenceMode)
			{
			case FbxGeometryElement::eDirect:
				break;
			case FbxGeometryElement::eIndex:
			case FbxGeometryElement::eIndexToDirect:
			{
				int *pnSubIndices = new int[nMaterials];
				memset(pnSubIndices, 0, sizeof(int) * nMaterials);

				int nSubIndices = pfbxElementMaterial->GetIndexArray().GetCount();
				for (int j = 0; j < nSubIndices; j++) pnSubIndices[pfbxElementMaterial->GetIndexArray().GetAt(j)]++;

				int **ppnSubIndices = new int*[nMaterials];
				for (int k = 0; k < nMaterials; k++)
				{
					pnSubIndices[k] *= 3;
					ppnSubIndices[k] = new int[pnSubIndices[k]];
				}

				int *pnToAppends = new int[nMaterials];
				memset(pnToAppends, 0, sizeof(int) * nMaterials);
				for (int j = 0, k = 0; j < nSubIndices; j++)
				{
					int nMaterial = pfbxElementMaterial->GetIndexArray().GetAt(j);
					for (int i = 0; i < 3; i++) ppnSubIndices[nMaterial][pnToAppends[nMaterial]++] = pnPolygonIndices[k++];
				}

				for (int k = 0; k < nMaterials; k++)
				{
					WriteInt("<SubIndex>:", k, pnSubIndices[k]);
					for (int j = 0; j < pnSubIndices[k]; j++) WriteInt(ppnSubIndices[k][j]);
				}

				if (pnSubIndices) delete[] pnSubIndices;
				for (int k = 0; k < nMaterials; k++) if (ppnSubIndices[k]) delete[] ppnSubIndices[k];
				if (ppnSubIndices) delete[] ppnSubIndices;
				if (pnToAppends) delete[] pnToAppends;

				break;
			}
			}
		}
	}
	else
	{
		WriteInt("<SubIndex>:", 0, nPolygonIndices);
		for (int i = 0; i < nPolygonIndices; i++) WriteInt(pnPolygonIndices[i]);
	}

	if (pnPolygonIndices) delete[] pnPolygonIndices;
}

void WritePolygons(FbxMesh *pfbxMesh, int nPolygons)
{
	WriteInt("<Polygons>:", nPolygons);

	WritePolygonVertexIndices(pfbxMesh, nPolygons);

	WriteString("</Polygons>");
}

void WriteMesh(FbxMesh *pfbxMesh)
{
	int nControlPoints = pfbxMesh->GetControlPointsCount();
	if (nControlPoints > 0)
	{
		WriteControlPoints(pfbxMesh, nControlPoints);

		WriteControlPointUVs(pfbxMesh, nControlPoints);

		WriteControlPointNormals(pfbxMesh, nControlPoints);
		WriteControlPointTangents(pfbxMesh, nControlPoints);
		WriteControlPointBiTangents(pfbxMesh, nControlPoints);
	}

	int nPolygons = pfbxMesh->GetPolygonCount();
	if (nPolygons > 0) WritePolygons(pfbxMesh, nPolygons);
}

void WriteTextureInfo(FbxTexture *pfbxTexture, int nBlendMode)
{
	WriteString(ReplaceBlank(pfbxTexture->GetName(), '_'));

	FbxFileTexture *pfbxFileTexture = FbxCast<FbxFileTexture>(pfbxTexture);
	if (pfbxFileTexture)
	{
		FbxString fbxPathName = pfbxFileTexture->GetRelativeFileName();
		FbxString fbxFileName = fbxPathName.Right(fbxPathName.GetLen() - fbxPathName.ReverseFind('\\') - 1);
		WriteIntString(0, ReplaceBlank(fbxFileName.Buffer(), '_')); //0:"File"
	}
	else
	{
		FbxProceduralTexture *pfbxProceduralTexture = FbxCast<FbxProceduralTexture>(pfbxTexture);
		if (pfbxProceduralTexture) WriteInt(1); //1:"Procedural"
	}

	WriteFloat(pfbxTexture->GetScaleU(), pfbxTexture->GetScaleV());
	WriteFloat(pfbxTexture->GetTranslationU(), pfbxTexture->GetTranslationV());
	WriteBool(pfbxTexture->GetSwapUV());
	WriteFloat(pfbxTexture->GetRotationU(), pfbxTexture->GetRotationV(), pfbxTexture->GetRotationW());

	WriteInt(pfbxTexture->GetAlphaSource()); //Alpha Source: 0:"None", 1:"Intensity", 2:"Black"
	WriteInt(pfbxTexture->GetCroppingLeft(), pfbxTexture->GetCroppingTop(), pfbxTexture->GetCroppingRight(), pfbxTexture->GetCroppingBottom());

	WriteInt(pfbxTexture->GetMappingType()); //Mapping Type: 0:"Null", 1:"Planar", 2:"Spherical", 3:"Cylindrical", 4:"Box", 5:"Face", 6:"UV", 7:"Environment"
	if (pfbxTexture->GetMappingType() == FbxTexture::ePlanar) WriteInt(pfbxTexture->GetPlanarMappingNormal()); //PlanarMappingNormal: 0:"X", 1:"Y", 2:"Z"

	WriteInt(nBlendMode); //Blend Mode: -1:"None", 0:"Translucent", 1:"Additive", 2:"Modulate", 3:"Modulate2", 4:"Over", 5:"Normal", 6:"Dissolve", 7:"Darken", 8:"ColorBurn", 9:"LinearBurn", 10:"DarkerColor", "Lighten", "Screen", "ColorDodge", "LinearDodge", "LighterColor", "SoftLight", "HardLight", "VividLight", "LinearLight", "PinLight", "HardMix", "Difference", "Exclusion", "Substract", "Divide", "Hue", "Saturation", "Color", "Luminosity", "Overlay"

	WriteFloat(pfbxTexture->GetDefaultAlpha()); //Default Alpha

	WriteInt((pfbxFileTexture) ? pfbxFileTexture->GetMaterialUse() : -1); //Material Use: 0: "Model Material", 1:"Default Material"

	WriteInt(pfbxTexture->GetTextureUse()); //Texture Use: 0:"Standard", 1:"Shadow Map", 2:"Light Map", 3:"Spherical Reflexion Map", 4:"Sphere Reflexion Map", 5:"Bump Normal Map"
}

void FindAndWriteTextureInfoByProperty(FbxSurfaceMaterial *pfbxMaterial, FbxProperty *pfbxProperty)
{
	if (pfbxProperty->IsValid())
	{
		int nLayeredTextures = pfbxProperty->GetSrcObjectCount<FbxLayeredTexture>();
		if (nLayeredTextures > 0)
		{
			WriteInt("<LayeredTextures>:", nLayeredTextures);
			for (int i = 0; i < nLayeredTextures; i++)
			{
				FbxLayeredTexture *pfbxLayeredTexture = pfbxProperty->GetSrcObject<FbxLayeredTexture>(i);
				int nTextures = pfbxLayeredTexture->GetSrcObjectCount<FbxTexture>();
				WriteInt("<LayeredTexture>:", i, nTextures);
				for (int j = 0; j < nTextures; j++)
				{
					FbxTexture *pfbxTexture = pfbxLayeredTexture->GetSrcObject<FbxTexture>(j);
					if (pfbxTexture)
					{
						WriteInt("<Texture>:", j);
						FbxLayeredTexture::EBlendMode nBlendMode;
						pfbxLayeredTexture->GetTextureBlendMode(j, nBlendMode);
						WriteTextureInfo(pfbxTexture, (int)nBlendMode);
					}
				}
			}
		}
		else
		{
			int nTextures = pfbxProperty->GetSrcObjectCount<FbxTexture>();
			WriteInt("<Textures>:", nTextures);
			if (nTextures > 0)
			{
				for (int j = 0; j < nTextures; j++)
				{
					FbxTexture *pfbxTexture = pfbxProperty->GetSrcObject<FbxTexture>(j);
					if (pfbxTexture)
					{
						WriteInt("<Texture>:", j);
						WriteTextureInfo(pfbxTexture, -1);
					}
				}
			}
		}
	}
}

void WriteHardwareShaderImplementation(FbxSurfaceMaterial *pfbxMaterial, const FbxImplementation *pfbxImplementation, FbxString fbxstrImplemenationType)
{
	if (pfbxImplementation)
	{
		const FbxBindingTable *pfbxRootTable = pfbxImplementation->GetRootTable();
		FbxString fbxstrFileName = "\"" + pfbxRootTable->DescAbsoluteURL.Get() + "\"";
		FbxString fbxstrTechniqueName = "\"" + pfbxRootTable->DescTAG.Get() + "\"";

		WriteString("<HardwareShader>:", ReplaceBlank(fbxstrImplemenationType.Buffer(), '_'), fbxstrFileName.Buffer(), fbxstrTechniqueName.Buffer());

		const FbxBindingTable *pfbxTable = pfbxImplementation->GetRootTable();
		int nEntries = (int)pfbxTable->GetEntryCount();

		WriteInt("<Entries>:", nEntries);
		for (int i = 0; i < nEntries; i++)
		{
			const FbxBindingTableEntry& fbxEntry = pfbxTable->GetEntry(i);
			const char *pstrEntrySrcType = fbxEntry.GetEntryType(true);

			WriteIntString("<Entry>:", i, fbxEntry.GetSource());

			FbxProperty fbxProperty;
			if (!strcmp(FbxPropertyEntryView::sEntryType, pstrEntrySrcType))
			{
				fbxProperty = pfbxMaterial->FindPropertyHierarchical(fbxEntry.GetSource());
				if (!fbxProperty.IsValid())
				{
					fbxProperty = pfbxMaterial->RootProperty.FindHierarchical(fbxEntry.GetSource());
				}
			}
			else if (!strcmp(FbxConstantEntryView::sEntryType, pstrEntrySrcType))
			{
				fbxProperty = pfbxImplementation->GetConstants().FindHierarchical(fbxEntry.GetSource());
			}

			if (fbxProperty.IsValid())
			{
				int nTextures = fbxProperty.GetSrcObjectCount<FbxTexture>();
				if (nTextures > 0)
				{
					WriteInt("<Textures>:", nTextures);

					int nFileTextures = fbxProperty.GetSrcObjectCount<FbxFileTexture>();
					WriteInt("<FileTextures>:", nFileTextures);
					for (int j = 0; j < nFileTextures; j++)
					{
						FbxFileTexture *pfbxFileTexture = fbxProperty.GetSrcObject<FbxFileTexture>(j);
						WriteString(pfbxFileTexture->GetFileName());
					}
					WriteString("\n");

					int nLayeredTextures = fbxProperty.GetSrcObjectCount<FbxLayeredTexture>();
					WriteInt("<LayeredTextures>:", nLayeredTextures);
					for (int j = 0; j < nLayeredTextures; j++)
					{
						FbxLayeredTexture *pfbxLayeredTexture = fbxProperty.GetSrcObject<FbxLayeredTexture>(j);
						WriteString(pfbxLayeredTexture->GetName());
					}
					WriteString("\n");

					int nProceduralTextures = fbxProperty.GetSrcObjectCount<FbxProceduralTexture>();
					WriteInt("<ProceduralTextures>:", nProceduralTextures);
					for (int j = 0; j < nProceduralTextures; ++j)
					{
						FbxProceduralTexture *pfbxProceduralTexture = fbxProperty.GetSrcObject<FbxProceduralTexture>(j);
						WriteString(pfbxProceduralTexture->GetName());
					}
					WriteString("</Textures>");
				}
				else
				{
					FbxDataType lFbxType = fbxProperty.GetPropertyDataType();
					if (FbxBoolDT == lFbxType)
					{
						WriteBool("<Bool>:", fbxProperty.Get<FbxBool>());
					}
					else if ((FbxIntDT == lFbxType) || (FbxEnumDT == lFbxType))
					{
						WriteInt("<Int>:", fbxProperty.Get<FbxInt>());
					}
					else if (FbxFloatDT == lFbxType)
					{
						WriteFloat("<Float>:", fbxProperty.Get<FbxFloat>());
					}
					else if (FbxDoubleDT == lFbxType)
					{
						WriteDouble("<Double>:", fbxProperty.Get<FbxDouble>());
					}
					else if ((FbxStringDT == lFbxType) || (FbxUrlDT == lFbxType) || (FbxXRefUrlDT == lFbxType))
					{
						WriteString("<String>:", fbxProperty.Get<FbxString>().Buffer());
					}
					else if (FbxDouble2DT == lFbxType)
					{
						Write2DVector("<Vector2D>:", fbxProperty.Get<FbxDouble2>());
					}
					else if ((FbxDouble3DT == lFbxType) || (FbxColor3DT == lFbxType))
					{
						Write3DVector("<Vector3D>:", fbxProperty.Get<FbxDouble3>());
					}
					else if ((FbxDouble4DT == lFbxType) || (FbxColor4DT == lFbxType))
					{
						Write4DVector("<Vector4D>:", fbxProperty.Get<FbxDouble4>());
					}
					else if (FbxDouble4x4DT == lFbxType)
					{
						WriteMatrix("<Matrix>:", fbxProperty.Get<FbxDouble4x4>());
					}
					else
					{
						WriteString("<Unknown>");
					}
				}
			}
		}
		WriteString("</Entries>");
	}
}

void WriteMaterials(FbxGeometry *pfbxGeometry)
{
	FbxNode *pfbxNode = pfbxGeometry->GetNode();
	if (pfbxNode)
	{
		int nMaterials = pfbxNode->GetMaterialCount();
		WriteInt("<Materials>:", nMaterials);

		for (int i = 0; i < nMaterials; i++)
		{
			FbxSurfaceMaterial *pfbxMaterial = pfbxNode->GetMaterial(i);
			if (pfbxMaterial)
			{
				//재질 이름 함께 저장됨
				WriteIntString("<Material>:", i, ReplaceBlank(pfbxMaterial->GetName(), '_'));

				int nProperties = FbxLayerElement::sTypeTextureCount;
				// 텍스쳐의 속성
				//WriteInt("<TextureProperties>:", nProperties);
				//for (int j = 0; j < nProperties; j++)
				//{
				//	FbxProperty fbxProperty = pfbxMaterial->FindProperty(FbxLayerElement::sTextureChannelNames[j]);
				//	//텍스쳐 이름들로 추정되는 부분 너무 많으니 번호별로 따로 저장해야 할듯하다.
				//	WriteIntString("<Property>:", j, FbxLayerElement::sTextureChannelNames[j], ReplaceBlank(fbxProperty.GetName(), '_'));
				//	std::cout << FbxLayerElement::sTextureChannelNames[j] << std::endl;
				//	//0:DiffuseColor(FbxSurfaceMaterial::sDiffuse), 1:DiffuseFactor, 2:EmissiveColor, 3:EmissiveFactor, 4:AmbientColor, 5:AmbientFactor, 6:SpecularColor, 7:SpecularFactor, 8:ShininessExponent
				//	//9:NormalMap, 10:Bump, 11:TransparentColor, 12:TransparencyFactor, 13:ReflectionColor, 14:ReflectionFactor, 15:DisplacementColor, 16:VectorDisplacementColor
				//	FindAndWriteTextureInfoByProperty(pfbxMaterial, &fbxProperty);
				//}
				//WriteString("</TextureProperties>");

				// 빛의 의해 그림자가 만들어지는 방식을 말하는 것일까 phong 과 lambert 두가지 모델이 존재함
				//WriteString("<ShadingModel>:", ReplaceBlank((char *)pfbxMaterial->ShadingModel.Get().Buffer(), '_'));
				FbxString fbxstrImplemenationType = "HLSL";
				const FbxImplementation *pfbxImplementation = GetImplementation(pfbxMaterial, FBXSDK_IMPLEMENTATION_HLSL);
				if (!pfbxImplementation)
				{
					pfbxImplementation = GetImplementation(pfbxMaterial, FBXSDK_IMPLEMENTATION_CGFX);
					fbxstrImplemenationType = "CGFX";
				}
				if (pfbxImplementation)
				{
					//WriteHardwareShaderImplementation(pfbxMaterial, pfbxImplementation, fbxstrImplemenationType);
				}
				else if (pfbxMaterial->GetClassId().Is(FbxSurfacePhong::ClassId))
				{
					WriteString("<Phong>:");

					FbxSurfacePhong *pfbxSurfacePhong = (FbxSurfacePhong *)pfbxMaterial;
					WriteColor(pfbxSurfacePhong->Ambient);
					WriteColor(pfbxSurfacePhong->Diffuse);
					WriteColor(pfbxSurfacePhong->Specular);
					WriteColor(pfbxSurfacePhong->Emissive);
					WriteFloat((float)pfbxSurfacePhong->TransparencyFactor);
					WriteFloat((float)pfbxSurfacePhong->Shininess);
					WriteFloat((float)pfbxSurfacePhong->ReflectionFactor);
				}
				else if (pfbxMaterial->GetClassId().Is(FbxSurfaceLambert::ClassId))
				{
					WriteString("<Lambert>:");

					FbxSurfaceLambert *pfbxSurfaceLambert = (FbxSurfaceLambert *)pfbxMaterial;
					WriteColor(pfbxSurfaceLambert->Ambient);
					WriteColor(pfbxSurfaceLambert->Diffuse);
					WriteColor(pfbxSurfaceLambert->Emissive);
					WriteFloat((float)pfbxSurfaceLambert->TransparencyFactor);
				}
				else
				{
					WriteString("<Unknown>");
				}
			}
		}
		WriteString("</Materials>");
	}
}

void WriteHierarchy(int *frame, FbxNode *node)
{
	WriteIntString("<Frame>:", *frame, ReplaceBlank(node->GetName(), '_'));

	FbxAMatrix LocalTransform = node->EvaluateLocalTransform(FBXSDK_TIME_INFINITE);

	FbxDouble3 T = LocalTransform.GetT();
	FbxDouble3 R = LocalTransform.GetR();
	FbxDouble3 S = LocalTransform.GetS();

	R[0] = R[0] * (3.1415926f / 180.f);
	R[1] = R[1] * (3.1415926f / 180.f);
	R[2] = R[2] * (3.1415926f / 180.f);

	WriteTransform("<Transform>:", LocalTransform, S, R, T);

	FbxNodeAttribute *NodeAttribute = node->GetNodeAttribute();
	if (NodeAttribute)
	{
		FbxNodeAttribute::EType nAttributeType = NodeAttribute->GetAttributeType();
		if (nAttributeType == FbxNodeAttribute::eMesh)
		{
			FbxMesh *Mesh = (FbxMesh *)node->GetNodeAttribute();
			int nSkinDeformers = Mesh->GetDeformerCount(FbxDeformer::eSkin);
			if (nSkinDeformers > 0)
			{
				WriteString("<SkinDeformations>:");
				WriteSkinDeformations(Mesh);
				WriteString("</SkinDeformations>");
			}

			WriteString("<Mesh>:", (char *)Mesh->GetName());
			WriteMesh(Mesh);
			WriteString("</Mesh>");

			WriteMaterials(Mesh);
			//WriteTexture(Mesh);
		}
	}

	int ChildCount = node->GetChildCount();

	WriteInt("<Children>:", ChildCount);

	for (int i = 0; i < ChildCount; i++)
	{
		(*frame)++;
		WriteHierarchy(frame, node->GetChild(i));
	}

	WriteString("</Frame>");
}

void WriteHierarchy(FbxScene *scene)
{
	FbxNode *RootNode = scene->GetRootNode();

	int Frame = 0;
	int ChildCount = RootNode->GetChildCount();

	WriteHierarchy(&Frame, RootNode);

	//for (int i = 0; i < ChildCount; i++, Frame++)
	//{
	//	WriteHierarchy(&Frame, RootNode->GetChild(i));
	//}
}