#include "Animation.h"

int GetAnimationCurves(FbxAnimLayer *AnimationLayer, FbxNode *node)
{
	int nAnimationCurves = 0;

	FbxAnimCurve *AnimationCurve = NULL;
	if (AnimationCurve = node->LclTranslation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y)) nAnimationCurves++;
	if (AnimationCurve = node->LclTranslation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z)) nAnimationCurves++;
	if (AnimationCurve = node->LclRotation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_X)) nAnimationCurves++;
	if (AnimationCurve = node->LclRotation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y)) nAnimationCurves++;
	if (AnimationCurve = node->LclRotation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z)) nAnimationCurves++;
	if (AnimationCurve = node->LclScaling.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_X)) nAnimationCurves++;
	if (AnimationCurve = node->LclScaling.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y)) nAnimationCurves++;
	if (AnimationCurve = node->LclScaling.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z)) nAnimationCurves++;

	return(nAnimationCurves);
}

int GetAnimationLayerCurveNodes(FbxAnimLayer *AnimationLayer, FbxNode *node)
{
	int nAnimationCurveNodes = 0;
	if (GetAnimationCurves(AnimationLayer, node) > 0) nAnimationCurveNodes = 1;

	for (int i = 0; i < node->GetChildCount(); i++)
	{
		nAnimationCurveNodes += GetAnimationLayerCurveNodes(AnimationLayer, node->GetChild(i));
	}

	return(nAnimationCurveNodes);
}

void WriteCurveKeys(const char *Header, FbxAnimCurve *AnimationCurve, bool bRotationAngle)
{
	int nKeys = AnimationCurve->KeyGetCount();
	WriteInt(Header, nKeys);

	for (int i = 0; i < nKeys; i++)
	{
		FbxTime fbxKeyTime = AnimationCurve->KeyGetTime(i);
		float fkeyTime = (float)fbxKeyTime.GetSecondDouble();
		WriteFloat(fkeyTime);
	}

	for (int i = 0; i < nKeys; i++)
	{
		float fKeyValue = static_cast<float>(AnimationCurve->KeyGetValue(i));
		if (bRotationAngle) fKeyValue = (3.1415926f / 180.f) * fKeyValue;
		WriteFloat(fKeyValue);
	}
}

void WriteChannels(FbxAnimLayer *AnimationLayer, FbxNode *node, int *CurveNode)
{
	if (GetAnimationCurves(AnimationLayer, node) > 0)
	{
		WriteIntString("<AnimationCurve>:", (*CurveNode)++, ReplaceBlank(node->GetName(), '_'));

		FbxAnimCurve *pfbxAnimationCurve = NULL;

		pfbxAnimationCurve = node->LclTranslation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_X);
		if (pfbxAnimationCurve) WriteCurveKeys("<TX>:", pfbxAnimationCurve, false);

		pfbxAnimationCurve = node->LclTranslation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y);
		if (pfbxAnimationCurve) WriteCurveKeys("<TY>:", pfbxAnimationCurve, false);

		pfbxAnimationCurve = node->LclTranslation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z);
		if (pfbxAnimationCurve) WriteCurveKeys("<TZ>:", pfbxAnimationCurve, false);

		pfbxAnimationCurve = node->LclRotation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_X);
		if (pfbxAnimationCurve) WriteCurveKeys("<RX>:", pfbxAnimationCurve, true);

		pfbxAnimationCurve = node->LclRotation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y);
		if (pfbxAnimationCurve) WriteCurveKeys("<RY>:", pfbxAnimationCurve, true);

		pfbxAnimationCurve = node->LclRotation.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z);
		if (pfbxAnimationCurve) WriteCurveKeys("<RZ>:", pfbxAnimationCurve, true);

		pfbxAnimationCurve = node->LclScaling.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_X);
		if (pfbxAnimationCurve) WriteCurveKeys("<SX>:", pfbxAnimationCurve, false);

		pfbxAnimationCurve = node->LclScaling.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Y);
		if (pfbxAnimationCurve) WriteCurveKeys("<SY>:", pfbxAnimationCurve, false);

		pfbxAnimationCurve = node->LclScaling.GetCurve(AnimationLayer, FBXSDK_CURVENODE_COMPONENT_Z);
		if (pfbxAnimationCurve) WriteCurveKeys("<SZ>:", pfbxAnimationCurve, false);

		WriteString("</AnimationCurve>");
	}
}

void WriteAnimation(FbxAnimLayer *AnimationLayer, FbxNode *node, int *CurveNode)
{
	WriteChannels(AnimationLayer, node, CurveNode);

	for (int i = 0; i < node->GetChildCount(); i++)
	{
		WriteAnimation(AnimationLayer, node->GetChild(i), CurveNode);
	}
}

void WriteAnimation(FbxAnimStack *AnimStack, FbxNode *node)
{
	int nAnimationLayers = AnimStack->GetMemberCount<FbxAnimLayer>();
	WriteInt("<AnimationLayers>:", nAnimationLayers);

	for (int i = 0; i < nAnimationLayers; i++)
	{
		FbxAnimLayer *AnimationLayer = AnimStack->GetMember<FbxAnimLayer>(i);
		int nLayerCurveNodes = GetAnimationLayerCurveNodes(AnimationLayer, node);

		int CurveNode = 0;
		WriteIntFloat("<AnimationLayer>:", i, nLayerCurveNodes, float(AnimationLayer->Weight) / 100.0f);
		WriteAnimation(AnimationLayer, node, &CurveNode);
		WriteString("</AnimationLayer>");
	}

	WriteString("</AnimationLayers>");
}

void WriteAnimation(FbxScene *Scene)
{
	int nAnimationStacks = Scene->GetSrcObjectCount<FbxAnimStack>();
	WriteInt("<AnimationSets>:", nAnimationStacks);

	for (int i = 0; i < nAnimationStacks; i++)
	{
		FbxAnimStack *pfbxAnimStack = Scene->GetSrcObject<FbxAnimStack>(i);

		WriteIntString("<AnimationSet>:", i, ReplaceBlank(pfbxAnimStack->GetName(), '_'));
		FbxTime fbxTimeStart = pfbxAnimStack->LocalStart;
		FbxTime fbxTimeStop = pfbxAnimStack->LocalStop;
		WriteFloat((float)fbxTimeStart.GetSecondDouble(), (float)fbxTimeStop.GetSecondDouble());

		WriteAnimation(pfbxAnimStack, Scene->GetRootNode());
		WriteString("</AnimationSet>");
	}

	WriteString("</AnimationSets>");
}

void WriteAnimation(FbxScene **ppfbxScenes, int nSeparatedAnimations)
{
	int nAnimationStacks = 0;
	int *pnAnimationStacks = new int[nSeparatedAnimations];

	for (int i = 0; i < nSeparatedAnimations; i++)
	{
		pnAnimationStacks[i] = ppfbxScenes[i]->GetSrcObjectCount<FbxAnimStack>();
		nAnimationStacks += pnAnimationStacks[i];
	}

	WriteInt("<AnimationSets>:", nAnimationStacks);

	for (int i = 0, k = 0; i < nSeparatedAnimations; i++)
	{
		for (int j = 0; j < pnAnimationStacks[i]; j++, k++)
		{
			FbxAnimStack *pfbxAnimStack = ppfbxScenes[i]->GetSrcObject<FbxAnimStack>(j);
			WriteIntString("<AnimationSet>:", k, ReplaceBlank(pfbxAnimStack->GetName(), '_'));
			FbxTime fbxTimeStart = pfbxAnimStack->LocalStart;
			FbxTime fbxTimeStop = pfbxAnimStack->LocalStop;
			WriteFloat((float)fbxTimeStart.GetSecondDouble(), (float)fbxTimeStop.GetSecondDouble());

			WriteAnimation(pfbxAnimStack, ppfbxScenes[i]->GetRootNode());
			WriteString("</AnimationSet>");
		}
	}

	WriteString("</AnimationSets>");

	delete[] pnAnimationStacks;
}