#include <iostream>
#include "WriteData.h"
#include "Hierarchy.h"
#include "Animation.h"

FILE *BinaryFile = NULL;

int main()
{
	FbxManager* fbxManager = NULL;
	FbxScene* fbxScene = NULL;
	FbxIOSettings* fbxIos = NULL;
	FbxImporter* fbxImporter = NULL;
	bool fbxStatus;

	fbxManager = FbxManager::Create();
	fbxIos = FbxIOSettings::Create(fbxManager, IOSROOT);

	fbxManager->SetIOSettings(fbxIos);

	fbxImporter = FbxImporter::Create(fbxManager, "");
	FbxString filename = "Angrybot.FBX";

	const char *WriteFileName = "Sample.bin";

	fbxStatus = fbxImporter->Initialize(filename, -1, fbxManager->GetIOSettings());

	if (!fbxStatus) {
		printf("fbxImporter->init fail\n");
		printf("Error returned: %s\n\n", fbxImporter->GetStatus().GetErrorString());
		exit(-1);
	}

	fbxScene = FbxScene::Create(fbxManager, "scene");

	bool result = fbxImporter->Import(fbxScene);

	//const int animationCount = 3;
	//const char *animationFileNames[animationCount];
	//FbxScene *fbxScenes[animationCount];

	//animationFileNames[0] = "sample1.fbx";
	//animationFileNames[1] = "sample2.fbx";
	//animationFileNames[2] = "sample3.fbx";

	//for (int i = 0; i < animationCount; ++i)
	//{
	//	fbxScenes[i] = FbxScene::Create(fbxManager, animationFileNames[i]);
	//	FbxImporter *aniImporter = FbxImporter::Create(fbxManager, "");
	//	fbxStatus = aniImporter->Initialize(filename, -1, fbxManager->GetIOSettings());
	//	if (!fbxStatus) {
	//		printf("fbxImporter->init fail\n");
	//		printf("Error returned: %s\n\n", fbxImporter->GetStatus().GetErrorString());
	//		exit(-1);
	//	}

	//	aniImporter->Import(fbxScenes[i]);

	//	aniImporter->Destroy();
	//}


	FbxGeometryConverter fbxGeomConverter(fbxManager);                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                                 
	fbxGeomConverter.Triangulate(fbxScene, true);
	fbxGeomConverter.RemoveBadPolygonsFromMeshes(fbxScene, NULL);

	::fopen_s(&BinaryFile, WriteFileName, "wb");

	WriteString("<Hierarchy>");
	WriteHierarchy(fbxScene);
	WriteString("</Hierarchy>");

	WriteString("<Animation>");
	WriteAnimation(fbxScene);
	// WriteAnimation(fbxScenes, animationCount); // 여러 애니메이션 정보 넣기
	WriteString("</Animation>");

	::fclose(BinaryFile);

	if (fbxManager) fbxManager->Destroy();
	return 0;
}