#include "stdafx.h"
#include "Timer.h"


GameTimer::GameTimer()
{
	if (::QueryPerformanceFrequency((LARGE_INTEGER *)&performanceFrequencyPerSec))
	{
		hardwareHasPerformanceCounter = TRUE;
		::QueryPerformanceCounter((LARGE_INTEGER *)&lastPerformanceCounter);
		timeScale = 1.0f / (double)performanceFrequencyPerSec;
	}
	else
	{
		hardwareHasPerformanceCounter = FALSE;
		lastPerformanceCounter = ::timeGetTime();
		timeScale = 0.001f;
	}
	basePerformanceCounter = lastPerformanceCounter;
	pausedPerformanceCounter = 0;
	stopPerformanceCounter = 0;

	sampleCount = 0;
	currentFrameRate = 0;
	framesPerSecond = 0;
	fpsTimeElapsed = 0.0f;
}
GameTimer::~GameTimer()
{
}

void GameTimer::Tick(float lockFPS)
{
	if (stopped)
	{
		timeElapsed = 0.0f;
		return;
	}

	float fTimeElapsed;

	if (hardwareHasPerformanceCounter)
	{
		::QueryPerformanceCounter((LARGE_INTEGER *)&currentPerformanceCounter);
	}
	else
	{
		currentPerformanceCounter = ::timeGetTime();
	}
	::QueryPerformanceCounter((LARGE_INTEGER *)&currentPerformanceCounter);

	//마지막으로 이 함수를 호출한 이후 경과한 시간을 계산한다.
	fTimeElapsed = float((currentPerformanceCounter - lastPerformanceCounter) * timeScale);
	if (lockFPS > 0.0f)
	{
		//이 함수의 파라메터(fLockFPS)가 0보다 크면 이 시간만큼 호출한 함수를 기다리게 한다.
		while (fTimeElapsed < (1.0f / lockFPS))
		{
			if (hardwareHasPerformanceCounter)
			{
				::QueryPerformanceCounter((LARGE_INTEGER *)&currentPerformanceCounter);
			}
			else
			{
				currentPerformanceCounter = ::timeGetTime();
			}
			//마지막으로 이 함수를 호출한 이후 경과한 시간을 계산한다.
			fTimeElapsed = float((currentPerformanceCounter - lastPerformanceCounter) * timeScale);
		}
	}
	//현재 시간을 m_nLastTime에 저장한다.
	lastPerformanceCounter = currentPerformanceCounter;

	/* 마지막 프레임 처리 시간과 현재 프레임 처리 시간의 차이가 1초보다 작으면 현재 프레임 처리 시간
	을 m_fFrameTime[0]에 저장한다. */
	if (fabsf(fTimeElapsed - timeElapsed) < 1.0f)
	{
		::memmove(&frameTime[1], frameTime, (MAX_SAMPLE_COUNT - 1) * sizeof(float));
		frameTime[0] = fTimeElapsed;
		if (sampleCount < MAX_SAMPLE_COUNT) sampleCount++;
	}
	//초당 프레임 수를 1 증가시키고 현재 프레임 처리 시간을 누적하여 저장한다.
	framesPerSecond++;
	fpsTimeElapsed += fTimeElapsed;
	if (fpsTimeElapsed > 1.0f)
	{
		currentFrameRate = framesPerSecond;
		framesPerSecond = 0;
		fpsTimeElapsed = 0.0f;
	}
	//누적된 프레임 처리 시간의 평균을 구하여 프레임 처리 시간을 구한다.
	timeElapsed = 0.0f;
	for (ULONG i = 0; i < sampleCount; i++) timeElapsed += frameTime[i];
	if (sampleCount > 0) timeElapsed /= sampleCount;
}

unsigned long GameTimer::GetFrameRate(LPTSTR string, int characters)
{
	//현재 프레임 레이트를 문자열로 변환하여 lpszString 버퍼에 쓰고 “ FPS”와 결합한다.
	if (string)
	{
		_itow_s(currentFrameRate, string, characters, 10);
		wcscat_s(string, characters, _T(" FPS)"));
	}
	return(currentFrameRate);
}

float GameTimer::GetTimeElapsed()
{
	return(timeElapsed);
}

float GameTimer::GetTotalTime()
{
	if (stopped) return(float(((stopPerformanceCounter - pausedPerformanceCounter) - basePerformanceCounter) * timeScale));
	return(float(((currentPerformanceCounter - pausedPerformanceCounter) - basePerformanceCounter) * timeScale));
}

void GameTimer::Reset()
{
	__int64 nPerformanceCounter;
	::QueryPerformanceCounter((LARGE_INTEGER*)&nPerformanceCounter);

	basePerformanceCounter = nPerformanceCounter;
	lastPerformanceCounter = nPerformanceCounter;
	stopPerformanceCounter = 0;
	stopped = false;
}

void GameTimer::Start()
{
	__int64 nPerformanceCounter;
	::QueryPerformanceCounter((LARGE_INTEGER *)&nPerformanceCounter);
	if (stopped)
	{
		pausedPerformanceCounter += (nPerformanceCounter - stopPerformanceCounter);
		lastPerformanceCounter = nPerformanceCounter;
		stopPerformanceCounter = 0;
		stopped = false;
	}
}

void GameTimer::Stop()
{
	if (!stopped)
	{
		::QueryPerformanceCounter((LARGE_INTEGER *)&stopPerformanceCounter);
		stopped = true;
	}
}