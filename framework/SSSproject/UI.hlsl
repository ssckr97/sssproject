#include "Shaders.hlsli"

// Scene
#define SCENE_START         0
#define SCENE_LOBBY         1
#define SCENE_INGAME        2
#define SCENE_ITEM_GET		4

// Texture
#define INGAME_PLAYERTYPE   0
#define INGAME_HP           3
#define INGAME_STEMINA      4
#define INGAME_MAP_ALL      5
#define INGAME_MAP          6
#define INGAME_ACCESSORIES  8
#define INGAME_WEAPONE      19
#define INGAME_GET_ITEM     36

#define LOBBY_BASE          64
#define LOBBY_PLAYERTYPE    65
#define LOBBY_MY_READY      68
#define LOBBY_OTHER_READY   70
#define LOBBY_OUTSIDE       71

#define INGAME_AIM          72
#define INGAME_ZOOM         73

#define START_BASE          74

#define LOBBY_PLAYERNAME    75

#define INGAME_PLAYERNAME    79

#define INGAME_MAGAZINE		83 
#define INGAME_MAGAZINE_LEFT		84

#define INGAME_MY_ITEM		36
#define INGAME_NEW_ITEM		37

#define ITEM_ACCESSORIES	8
#define ITEM_GUN			19
#define ITEM_SWORD			25
#define ITEM_BOW			31

#define ITEM_NUM_SET		28
#define ITEM_WEAPON_START	19
#define ITEM_ACCESSORY_START	8

#define SCAR 0
#define G28 1
#define AK 2
#define M4 3
#define SNIPER 4
#define BLACKAK 5

#define SCAR_AMMO 30
#define G28_AMMO 15
#define AK_AMMO 30
#define M4_AMMO 40
#define SNIPER_AMMO 5
#define BLACKAK_AMMO 40

// 이거 4의 배수로 뭔가 맞춰봐라
struct UIINFO
{
	int scenenum;	// 0 ~ 4 ?
    
    int playerJob; // 0 ~ 3
    int playerJob1; // 0 ~ 3
    int playerJob2; // 0 ~ 3
    int playerJob3; // 0 ~ 3

    int playerState; // 0 ~ 3
    int playerState1; // 0 ~ 3
    int playerState2; // 0 ~ 3
    int playerState3; // 0 ~ 3
    
	int accessory;// 0 ~ 10

	int weapon;	// 0 ~ 17
	int nowspot; // 0 ~ 8
    
    float hp;       // 0 ~ 1
    float hp1; // 0 ~ 1
    float hp2; // 0 ~ 1
    float hp3; // 0 ~ 1
    
    float stemina;  // 0 ~ 1

	float ammo;		// 0 ~ 1
	float newitem;

    bool isHead;
    int players;
    
    int myid;

    int passedmap0;
    int passedmap1;
    int passedmap2;
    int passedmap3;
    int passedmap4;
    int passedmap5;
    int passedmap6;
    int passedmap7;
    int passedmap8;
};

cbuffer cbUi : register(b11)
{
	UIINFO gUis;
}

struct VS_UI_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
	float3 textinfo : TEXINFO;
};

struct VS_UI_OUTPUT
{
	float4 position : SV_POSITION;
	float2 uv : TEXCOORD;
	uint textureid : TEXID;
	uint sceneid : SCENEID;
    uint playerid : PLAYERID;
};

VS_UI_OUTPUT VSUiTextured(VS_UI_INPUT input)
{
	VS_UI_OUTPUT output;

	output.position = float4(input.position, 1.0f);
	output.uv = input.uv;
    uint textureIdx = (uint) input.textinfo.x;
    uint sceneIdx = (uint) input.textinfo.y;
    uint playerIdx = (uint) input.textinfo.z;
    
    if (sceneIdx == SCENE_START)
    {
    }
    else if (sceneIdx == SCENE_LOBBY)
    {
        // player의 1의자리는 PlayerType에 대한 정보, 10의 자리는 Ready에 대한 정보
        if (textureIdx == LOBBY_PLAYERTYPE)
        {
            if (playerIdx == 0)
                textureIdx += gUis.playerJob;
            else if (playerIdx == 1)
                textureIdx += gUis.playerJob1;
            else if (playerIdx == 2)
                textureIdx += gUis.playerJob2;
            else
                textureIdx += gUis.playerJob3;
        }
        else if (textureIdx == LOBBY_OTHER_READY)
        {
            
            if (playerIdx == 0)
                textureIdx += gUis.playerState;
            else if (playerIdx == 1)
                textureIdx += gUis.playerState1;
            else if (playerIdx == 2)
                textureIdx += gUis.playerState2;
            else
                textureIdx += gUis.playerState3;
            
            if (textureIdx == LOBBY_OTHER_READY || textureIdx == LOBBY_OTHER_READY + 2)
                textureIdx = -1;
            else
                textureIdx = LOBBY_OTHER_READY;
        }
        else if(textureIdx == LOBBY_MY_READY)
        {
             textureIdx += gUis.isHead;
        }
        else if (textureIdx == LOBBY_OUTSIDE)
        {
            if (gUis.myid == 0)
            {
            
                if (gUis.playerJob != playerIdx)
                {
                    textureIdx = -1;
                }
            }
            else if (gUis.myid == 1)
            {
                if (gUis.playerJob1 != playerIdx)
                {
                    textureIdx = -1;
                }
            }
            else if (gUis.myid == 2)
            {
                if (gUis.playerJob2 != playerIdx)
                {
                    textureIdx = -1;
                }
            }
            else if (gUis.myid == 3)
            {
                if (gUis.playerJob3 != playerIdx)
                {
                    textureIdx = -1;
                }
            }

        }
        else if(textureIdx >= LOBBY_PLAYERNAME && textureIdx < LOBBY_PLAYERNAME + 4)
        {
            if(textureIdx >= LOBBY_PLAYERNAME + gUis.players)
            {
                textureIdx = -1;
            }
        }
    }
    else if (sceneIdx == SCENE_INGAME)
    {
        if (textureIdx == INGAME_PLAYERTYPE)
        {
            if (playerIdx == 0)
            {
                if(gUis.myid == 0)
                    textureIdx += gUis.playerJob;
                else if(gUis.myid == 1)
                    textureIdx += gUis.playerJob1;
                else if (gUis.myid == 2)
                    textureIdx += gUis.playerJob2;
                else if (gUis.myid == 3)
                    textureIdx += gUis.playerJob3;
            }
            else if (playerIdx == 1 && gUis.players > 1)
            {
                if (gUis.myid == 0)
                    textureIdx += gUis.playerJob1;
                else 
                    textureIdx += gUis.playerJob;
            }
            else if (playerIdx == 2 && gUis.players > 2)
            {
                if(gUis.myid >= 2)
                    textureIdx += gUis.playerJob1;
                else
                    textureIdx += gUis.playerJob2;
            }
            else if (playerIdx == 3 && gUis.players > 3)
            {
                if(gUis.myid == 3)
                    textureIdx += gUis.playerJob2;
                else
                    textureIdx += gUis.playerJob3;
            }
            else
            {
                textureIdx = -1;
            }

        }
        else if (textureIdx == INGAME_MAP_ALL)
        {
            if (playerIdx == 0)
            {
                textureIdx += gUis.passedmap0;
            }
            else if (playerIdx == 1)
            {
                textureIdx += gUis.passedmap1;
            }
            else if (playerIdx == 2)
            {
                textureIdx += gUis.passedmap2;
            }
            else if (playerIdx == 3)
            {
                textureIdx += gUis.passedmap3;
            }
            else if (playerIdx == 4)
            {
                textureIdx += gUis.passedmap4;
            }
            else if (playerIdx == 5)
            {
                textureIdx += gUis.passedmap5;
            }
            else if (playerIdx == 6)
            {
                textureIdx += gUis.passedmap6;
            }
            else if (playerIdx == 7)
            {
                textureIdx += gUis.passedmap7;
            }
            else if (playerIdx == 8)
            {
                textureIdx += gUis.passedmap8;
            }
            if ((textureIdx == INGAME_MAP_ALL) && (playerIdx != 9))
                textureIdx = -1;
            if(playerIdx == 9)
                textureIdx = INGAME_MAP_ALL;
        }
        else if (textureIdx == INGAME_ACCESSORIES)
        {
            textureIdx += gUis.accessory;
        }
        else if(textureIdx == INGAME_WEAPONE)
        {
            textureIdx += gUis.weapon;
        }
        else if (textureIdx == INGAME_PLAYERNAME)
        {
            if (playerIdx == 1 && gUis.players > 1)
            {
                if(gUis.myid == 1)   
                    textureIdx += 0;
                else
                    textureIdx += 1;
            }
            else if (playerIdx == 2 && gUis.players > 2)
            {
                if(gUis.myid == 2)
                    textureIdx += 1;
                else
                    textureIdx += 2;
            }
            else if (playerIdx == 3 && gUis.players > 3)
            {
                if (gUis.myid == 3)
                    textureIdx += 2;
                else
                    textureIdx += 3;
            }
            else
                textureIdx = -1;
            
            }
		else if (textureIdx == INGAME_MAGAZINE || textureIdx == INGAME_MAGAZINE_LEFT)
		{
            if (gUis.myid == 0)
            {
                if(gUis.playerJob != 0)
                    textureIdx = -1;              
            }
            else if (gUis.myid == 1)
            {
                if (gUis.playerJob1 != 0)
                    textureIdx = -1;
            }
            else if (gUis.myid == 2)
            {
                if (gUis.playerJob2 != 0)
                    textureIdx = -1;
            }
            else if (gUis.myid == 3)
            {
                if (gUis.playerJob3 != 0)
                    textureIdx = -1;
            }
        }
        else if(textureIdx == INGAME_HP)
        {
            if(playerIdx == 0)
                ;
            else if(playerIdx == 1 && gUis.players > 1)
                ;
            else if(playerIdx == 2 && gUis.players > 2)
                ;
            else if(playerIdx == 3 && gUis.players > 3)
                ;
            else
                textureIdx = -1;
        }
    }
	else if (sceneIdx == SCENE_ITEM_GET)
	{
		if (textureIdx == INGAME_MY_ITEM)
		{
			if (gUis.newitem < ITEM_GUN)
			{
				textureIdx = gUis.accessory + ITEM_ACCESSORY_START + ITEM_NUM_SET;
			}
			else
			{
				textureIdx = gUis.weapon + ITEM_NUM_SET + ITEM_WEAPON_START;
			}
		}
		else if (textureIdx == INGAME_NEW_ITEM)
		{
			textureIdx = gUis.newitem + ITEM_NUM_SET;
		}
	}
    
    output.textureid = textureIdx;
    output.sceneid = sceneIdx;
    output.playerid = playerIdx;

	return(output);
}

float4 PSUiTextured(VS_UI_OUTPUT input) : SV_TARGET
{
    if(input.textureid == -1) 
        return float4(0.0f, 0.0f, 0.0f, 0.0f);
    
    if (input.textureid == INGAME_HP || input.textureid == INGAME_STEMINA)
    {
        if (input.textureid == INGAME_HP)
        {
            if (gUis.myid == 0)
            {
                if (input.playerid == 0)
                {
                    if (input.uv.x > gUis.hp)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else if (input.playerid == 1)
                {
                    if (input.uv.x > gUis.hp1)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else if (input.playerid == 2)
                {
                    if (input.uv.x > gUis.hp2)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else
                {
                    if (input.uv.x > gUis.hp3)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                
            }
            else if (gUis.myid == 1)
            {
                if (input.playerid == 0)
                {
                    if (input.uv.x > gUis.hp1)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else if (input.playerid == 1)
                {
                    if(input.uv.x > gUis.hp)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else if (input.playerid == 2)
                {
                    if (input.uv.x > gUis.hp2)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else
                {
                    if (input.uv.x > gUis.hp3)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
            }
            else if (gUis.myid == 2)
            {
                if (input.playerid == 0)
                {
                    if (input.uv.x > gUis.hp2)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else if (input.playerid == 1)
                {
                    if (input.uv.x > gUis.hp)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else if (input.playerid == 2)
                {
                    if (input.uv.x > gUis.hp1)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else
                {
                    if (input.uv.x > gUis.hp3)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
            }
            else if (gUis.myid == 3)
            {
                if (input.playerid == 0)
                {
                    if (input.uv.x > gUis.hp3)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else if (input.playerid == 1)
                {
                    if (input.uv.x > gUis.hp)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else if (input.playerid == 2)
                {
                    if (input.uv.x > gUis.hp1)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
                else
                {
                    if (input.uv.x > gUis.hp2)
                    {
                        return float4(0.0f, 0.0f, 0.0f, 0.0f);
                    }
                }
            }
        }
        else
        {
            if (input.uv.x > gUis.stemina)
            {
                return float4(0.0f, 0.0f, 0.0f, 0.0f);
            }
        }
    }

	if (input.textureid == INGAME_MAGAZINE_LEFT)
	{
		float y = input.uv.y;
		float ammo;
		if(gUis.weapon == SCAR)
			ammo = 1.0f - (gUis.ammo/ SCAR_AMMO);
		if (gUis.weapon == G28)
			ammo = 1.0f - (gUis.ammo / G28_AMMO);
		if (gUis.weapon == AK)
			ammo = 1.0f - (gUis.ammo / AK_AMMO);
		if (gUis.weapon == M4)
			ammo = 1.0f - (gUis.ammo / M4_AMMO);
		if (gUis.weapon == SNIPER)
			ammo = 1.0f - (gUis.ammo / SNIPER_AMMO);
		if (gUis.weapon == BLACKAK)
			ammo = 1.0f - (gUis.ammo / BLACKAK_AMMO);
		if (y < ammo)
		{
			return float4(0.0f, 0.0f, 0.0f, 0.0f);
		}
	}
    
	

	float4 color = gtxtUiTexture[input.textureid].Sample(gssWrap, input.uv);
	if (gUis.scenenum != input.sceneid)
	{
		color.w = float(0.0f);
	}

	return(color);
}

struct VS_MiniMap_INPUT
{
    float2 position : POSITION;
    float4 color : COLOR;
};

struct GS_MiniMap_INPUT
{
    float2 position : POSITION;
    float4 color : COLOR;
};

struct PS_MiniMap_INPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

GS_MiniMap_INPUT VSMiniMap(VS_MiniMap_INPUT input)
{
    GS_MiniMap_INPUT output;
    
    output.position = input.position * 2.0f;
    output.position.y = output.position.y + 720 - 200;
    output.color = input.color;

    return output;
}

[maxvertexcount(9)]
void GSMiniMap(point GS_MiniMap_INPUT input[1], inout PointStream<PS_MiniMap_INPUT> outStream)
{
    PS_MiniMap_INPUT output;
    
    [unroll]
    for (int j = 0; j < 9; ++j)
    {
        float2 position;
        
        if (j == 0) 
            position = input[0].position + float2(-1.0f, -1.0f);
        else if (j == 1) 
            position = input[0].position + float2(0.0f, -1.0f);
        else if (j == 2) 
            position = input[0].position + float2(1.0f, -1.0f);
        else if (j == 3) 
            position = input[0].position + float2(-1.0f, 0.0f);
        else if (j == 4) 
            position = input[0].position + float2(0.0f, 0.0f);
        else if (j == 5) 
            position = input[0].position + float2(1.0f, 0.0f);
        else if (j == 6) 
            position = input[0].position + float2(-1.0f, 1.0f);
        else if (j == 7) 
            position = input[0].position + float2(0.0f, 1.0f);
        else if (j == 8) 
            position = input[0].position + float2(1.0f, 1.0f);
        
        position.x -= 640;
        position.y -= 360;
        position.x /= 640;
        position.y /= 360;
        
        output.position = float4(position, 0.0f, 1.0f);
        output.color = input[0].color;
        outStream.Append(output);
    }
    outStream.RestartStrip();

}

float4 PSMiniMap(PS_MiniMap_INPUT input) : SV_TARGET
{
    return input.color;
}

PS_MiniMap_INPUT VSMiniMapBG(VS_MiniMap_INPUT input)
{
    PS_MiniMap_INPUT output;
    
    float2 position = input.position;
    position.y = position.y + 720 - 200;
    
    position.x -= 640;
    position.y -= 360;
    position.x /= 640;
    position.y /= 360;
    
    output.position = float4(position, 0.0f, 1.0f);
    output.color = input.color;

    return output;
}

float4 PSMiniMapBG(PS_MiniMap_INPUT input) : SV_TARGET
{
    return input.color;
}
