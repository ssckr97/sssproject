#pragma once
#include "Timer.h"
#include "Scene.h"
#include "Camera.h"
#include "Player.h"
#include "ProcessFile.h"
#include "Picking.h"
using std::numeric_limits;
struct client {

	int pri_size = 0;
	char packet_buf[MAX_PACKET_SIZE];
	client() { memset(packet_buf, 0, MAX_PACKET_SIZE); }
	std::chrono::high_resolution_clock::time_point prev_time = std::chrono::high_resolution_clock::now();

	std::mutex cl_mu;

};

struct ClientInfo {
	char id;
	char job;
};

struct VS_CB_SHADOW_INFO
{
	XMFLOAT4X4 viewProjection[MAX_CASCADE_SIZE];
	XMFLOAT4 cascadeLength[MAX_CASCADE_SIZE];

	float shadowTexSize;
	float cascadeNum;
};

class GameFramework
{
private:
	HINSTANCE hinstance;
	HWND hwnd;
	int windowClientWidth;
	int windowClientHeight;

	IDXGIFactory4* factory;
	//DXGI 팩토리 인터페이스에 대한 포인터이다.

	IDXGISwapChain3* swapChain;
	//스왑 체인 인터페이스에 대한 포인터이다. 주로 디스플레이를 제어하기 위하여 필요하다.

	ID3D12Device* device;
	//Direct3D 디바이스 인터페이스에 대한 포인터이다. 주로 리소스를 생성하기 위하여 필요하다.

	bool msaa4xEnable = false;
	UINT msaa4xQualityLevels = 0;
	//MSAA 다중 샘플링을 활성화하고 다중 샘플링 레벨을 설정한다.

	static const UINT renderTargetBuffersNum = 2;
	//스왑 체인의 후면 버퍼의 개수이다.

	UINT swapChainBufferIndex;
	//현재 스왑 체인의 후면 버퍼 인덱스이다.

	ID3D12Resource* renderTargetBuffers[renderTargetBuffersNum];
	ID3D12DescriptorHeap* rtvDescriptorHeap;
	UINT rtvDescriptorIncrementSize;
	D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle[renderTargetBuffersNum];
	//렌더 타겟 버퍼, 서술자 힙 인터페이스 포인터, 렌더 타겟 서술자 원소의 크기이다.

	ID3D12Resource* depthStencilBuffer[COMMAND_IDX_NUM];
	ID3D12DescriptorHeap* dsvDescriptorHeap;
	UINT dsvDescriptorIncrementSize;
	D3D12_CPU_DESCRIPTOR_HANDLE depthStencilHandle[COMMAND_IDX_NUM];
	//깊이-스텐실 버퍼, 서술자 힙 인터페이스 포인터, 깊이-스텐실 서술자 원소의 크기이다.

	UINT shadowMapBuffersNum;
	ID3D12Resource* shadowMapBuffer[COMMAND_IDX_NUM];
	D3D12_CPU_DESCRIPTOR_HANDLE shadowMapHandle[COMMAND_IDX_NUM];

	ID3D12Resource* lightCameraBuffer = NULL;
	VS_CB_SHADOW_INFO* mappedLightCamera = NULL;

	ID3D12CommandQueue* commandQueue;
	ID3D12CommandAllocator* commandAllocator[COMMAND_IDX_NUM][CommandListStateNum];
	ID3D12GraphicsCommandList* commandList[COMMAND_IDX_NUM][CommandListStateNum];
	// 커멘드 리스트의 실행 순서를 정할 수 있다. CommandListStateMain, Pre, Mid, Post

	ID3D12CommandList* shadowCommandLists[COMMAND_IDX_NUM][CommandListStateNum - 1 + THREADS_NUM];
	// main, compute, render_threads(2), mid : 5
	ID3D12CommandList* renderCommandLists[COMMAND_IDX_NUM][1 + RENDER_THREADS_NUM];

	//명령 큐, 명령 할당자, 명령 리스트 인터페이스 포인터이다.

	std::vector<RenderInfo*> renderInfo;
	ComputeInfo* computeInfo;
	UINT commandListIdx;
	// 멀티쓰레드 사용에 사용되는 변수들.

	ID3D12PipelineState* pipelineState;
	//그래픽스 파이프라인 상태 객체에 대한 인터페이스 포인터이다.

	ID3D12Fence* fence;
	UINT64 fenceValues[renderTargetBuffersNum];
	HANDLE fenceEvent;
	//펜스 인터페이스 포인터, 펜스의 값, 이벤트 핸들이다.

#if defined(_DEBUG)
	ID3D12Debug *debugController;
#endif

	GameTimer gameTimer;
	// 게임 프레임 워크에서 사용할 타이머이다.

	_TCHAR frameRate[90];
	// 프레임레이트를 주 윈도우의 캡션에 출력하기 위한 문자열이다.

	ProcessFile* processFile;

	client cl;

	bool isKeyDown = false;
	bool isConnect = false;
	bool isGameStart = false;

	bool isMouseDown;
	bool ingameBuild;
	bool isLoadMapFinish = false;
	bool isLoadMap = false;

	bool showBounding = true;
	bool showItem = true;

	Picking* picking;

	SOCKET cSocket;
	bool InitConnect = false;

	float showBoundingCoolTime = -1.0f;
	float zoomCoolTime = -1.0f;
	float clickCoolTime = -1.0f;
	float loadingCoolTime = -1.0f;
	float jumpAttackEffectCoolTime = -1.0f;

	std::vector<GameObject*> tZombies;


public:
	int sceneNum = SCENE_START;

	std::vector<std::string> mapFileNames;
	Scene* scene;

	Camera* camera = NULL;
	Camera** lightCamera = NULL;

	float total;
	float elapsed;

	//플레이어 객체에 대한 포인터이다.

	char myid = 0;
	char userNum = 0;

	float* cascadePos = NULL;
	UINT cascadeNum = 0;
	float cascadeBias = 0;

	Player* player = NULL;
	//마지막으로 마우스 버튼을 클릭할 때의 마우스 커서의 위치이다.
	POINT oldCursorPos;

	bool isClearMap = false;
	bool addCharacter = false;
	bool allKill = false;
	int mapNum = 0;
	float testTime = 0;

	float tTime = 0.0f;

	std::chrono::high_resolution_clock::time_point sendtime;


	HANDLE renderBeginEvent[RENDER_THREADS_NUM];
	HANDLE renderShadowFinishEvent[RENDER_THREADS_NUM];
	HANDLE renderSceneFinishEvent[RENDER_THREADS_NUM];
	HANDLE computeBeginEvent;
	HANDLE computeFinishEvent;
	HANDLE presentBeginEvent;
	HANDLE presentFinishEvent;
	HANDLE presentChangeIdxEvent;

	std::vector<std::thread> renderThreads;
	std::thread computeThread;
	std::thread presentThread;

	// MultiThread에 사용되는 변수들

	static std::mutex locker;
	static std::atomic<bool> isLive;

	GameFramework();
	~GameFramework();

	bool OnCreate(HINSTANCE hinstance, HWND hwnd);
	//프레임워크를 초기화하는 함수이다(주 윈도우가 생성되면 호출된다).

	void OnDestroy();
	void CreateSwapChain();
	void CreateDirect3DDevice();
	void CreateRtvAndDsvDescriptorHeaps();
	void CreateCommandQueueAndList();
	//스왑 체인, 디바이스, 서술자 힙, 명령 큐/할당자/리스트를 생성하는 함수이다.

	void CreateRenderTargetViews();
	void CreateDepthStencilView();
	// 랜더타겟 뷰와 뎁스스텐실 뷰를 생성한다.

	void ChangeSwapChainState();
	// 창모드, 전체화면 모드를 전환하는 함수이다.

	void BuildObjects();
	void ReleaseObjects();
	//렌더링할 메쉬와 게임 객체를 생성하고 소멸하는 함수이다.

	void AnimateObjects();
	void FrameAdvance();
	//프레임워크의 핵심(사용자 입력, 애니메이션, 렌더링)을 구성하는 함수이다.

	void MoveToNextFrame();
	// 다음 프레임으로 이동한다.

	void OnProcessingMouseMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam);
	void OnProcessingKeyboardMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam);
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam);
	//윈도우의 메시지(키보드, 마우스 입력)를 처리하는 함수이다.

	void ProcessInput();
	void ProcessFileIO(ID3D12Device * device, ID3D12GraphicsCommandList * commandList);
	void CreateThreadsAndEvents();
	void CreateLightCameras();
	void UpdateLightCameras();
	void CreateShaderVariables();
	void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, int idx);
	void ReleaseShaderVariables();
	void processPacket();
	void SetSwapchinIdx(int idx);
	void GarbageCollection();
	void ProcessQuadTree(GameObject* gObject, bool isPlayer , bool isSkined);
	void ScreenPicking(POINT cursorPos);
	void RifleAttack();
	void SwordAttack();
	void BowAttack();
	void BowNearAttack(bool isPunch);
	void ArrowCollision();
	void GetHeight(GameObject* gObject);
	bool CollisionCheck(GameObject* gObj , bool isPlayer);
	std::map<float, UINT> SortBoundingBox(GameObject* terrain, InstancingShader* insShader, SkinnedInstancingShader* skinInsShader);
	void SetMapFilePath();
	XMFLOAT3 StartPos(int idx);
	void BuildInGame(ID3D12GraphicsCommandList* commandList);

	void InitSocketAndConnect(HWND hWnd);
	void OnProcessSocketMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam);

	void PrintFrame();

	void BeginRender();
	void Render();
	void EndRender();

	static void RenderThread(GameFramework* framework, int idx);
	static void ComputeThread(GameFramework* framework);
	static void PresentThread(GameFramework* framework);

	void GetAnimationNum(UCHAR keys[256], int character);
	void sendLoadMapPacket(int mapNum);
	void sendhitpacket(int id, int damage, XMFLOAT3 pos);
	void sendZombieHitPacket(int damage);
	void sendHillPacket();

	float CalcuateDamage(float strPower, float defPower, float hitArea);
};