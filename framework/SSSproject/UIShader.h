#pragma once
#include "Shader.h"
#include "ObjectShader.h"

class Player;
class SkinnedInstancingShader;
class ParticleShader;

struct VS_VB_MiniMap {
	XMFLOAT2 position;
	XMFLOAT4 color;

	VS_VB_MiniMap() {};
	VS_VB_MiniMap(XMFLOAT2 position, XMFLOAT4 color)
	{
		this->position = position;
		this->color = color;
	}
};

class UIShader : public StandardShader {
private:
	ObjectBuffer* objectBuffer;

public:
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context);
	virtual void Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseObjects();
	virtual void ReleaseUploadBuffer();
	virtual void ReleaseShaderVariables();
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_BLEND_DESC CreateBlendState();
};

class MiniMapShader : public Shader {
private:

	ID3D12Resource* buffer;
	VS_VB_MiniMap* mappedBuffer;
	D3D12_VERTEX_BUFFER_VIEW vertexBufferView;

	ID3D12Resource* bgBuffer;
	VS_VB_MiniMap* vertex;
	ID3D12Resource* uploadBgBuffer;
	D3D12_VERTEX_BUFFER_VIEW bgVertexBufferView;

	VS_VB_MiniMap* info;
	UINT bufferSize = 0;

	XMFLOAT3 center{ 50.0f, 0.0f, 50.0f };

	Player* player;
	SkinnedInstancingShader* skinInsShader;
	ParticleShader* particleShader;

public:
	MiniMapShader();
	~MiniMapShader();

	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature);
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateGeometryShader();
	virtual D3D12_PRIMITIVE_TOPOLOGY_TYPE SetTopologyType();

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx = 0);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseShaderVariables();

	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context = NULL);
	virtual void Animate();
	bool IsShowObject(XMFLOAT3 playerPos, XMFLOAT3 objectPos);
	void SetObject(Player* player, SkinnedInstancingShader* skinInsShader, ParticleShader* particleShader);

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx, bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList* commandList, bool isShadow, UINT cameraIdx);

};