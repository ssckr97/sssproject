﻿// stdafx.h : 자주 사용하지만 자주 변경되지는 않는
// 표준 시스템 포함 파일 또는 프로젝트 특정 포함 파일이 들어 있는
// 포함 파일입니다.
//

#pragma once

// 콘솔창 띄우기
#ifdef UNICODE
#pragma comment(linker, "/entry:wWinMainCRTStartup /subsystem:console") 
#else
#pragma comment(linker, "/entry:WinMainCRTStartup /subsystem:console") 
#endif

enum GraphicsRootParameters
{
	GraphicsRootPlayer = 0,						// CBV
	GraphicsRootCamera,							// CBV
	GraphicsRootGameobject,						// DESCRIPTORTABLE
	GraphicsRootMaterialInfo,					// ROOTCONSTANT
	GraphicsRootTimeInfo,						// ROOTCONSTANT
	GraphicsRootMaterials,						// CBV
	GraphicsRootLights,							// CBV
	GraphicsRootParticleAge,					// ROOTCONSTANT
	GraphicsRootLightCamera,					// CBV
	GraphicsRootSkinnedVerticesNum,				// ROOTCONSTANT
	GraphicsRootFogInfo,						// CBV
	GraphicsRootUiInfo,							// ?
	// Constant Buffer

	GraphicsRootTerrainTexture,					// DESCRIPTORTABLE
	GraphicsRootTerrainNormalTexture,			// DESCRIPTORTABLE
	GraphicsRootTerrainAlphaTexture,			// DESCRIPTORTABLE
	GraphicsRootDiffuseTextures,				// DESCRIPTORTABLE
	GraphicsRootNormalTextures,					// DESCRIPTORTABLE
	GraphicsRootSpecularTextures,				// DESCRIPTORTABLE
	GraphicsRootMetallicTextures,				// DESCRIPTORTABLE
	GraphicsRootBillboardTextures,				// DESCRIPTORTABLE
	GraphicsRootParticleTextures,				// DESCRIPTORTABLE
	GraphicsRootDepthTexture,					// DESCRIPTORTABLE
	GraphicsRootShadowTexture,					// DESCRIPTORTABLE
	GraphicsRootSkyBoxTexture,					// DESCRIPTORTABLE
	GraphicsRootUiTexture,						// DESCRIPTORTABLE
	// TextureResource

	GraphicsRootParticleInfo,					// SRV
	GraphicsRootSkinnedPositionWorld,			// SRV
	GraphicsRootSkinnedNormalWorld,				// SRV
	GraphicsRootSkinnedTangentWorld,			// SRV
	GraphicsRootSkinnedBiTangentWorld,			// SRV
	// StructuredBuffer

	GraphicsRootParametersNum					// PAMETERSNUM
};

enum ComputeRootParameters
{
	ComputeRootTimeInfo = 0,					// ROOTCONSTANT
	ComputeRootParticleInfo,					// ROOTCONSTANT
	ComputeRootParticleInfoSRV,					// SRV
	ComputeRootParticleCopySRV,					// SRV
	ComputeRootParticleInfoUAV,					// UAV
	ComputeRootCamera,							// ROOTCONSTANT
	ComputeRootParticleSortInfo,				// ROOTCONSTANT
	ComputeRootBoneOffset,						// CBV
	ComputeRootBoneTransform,					// SRV
	ComputeRootSkinnedPosition,					// SRV
	ComputeRootSkinnedNormal,					// SRV
	ComputeRootSkinnedTangent,					// SRV
	ComputeRootSkinnedBiTangent,				// SRV
	ComputeRootSkinnedBoneIndex,				// SRV
	ComputeRootSkinnedBoneWeight,				// SRV
	ComputeRootSkinnedPositionWorld,			// UAV
	ComputeRootSkinnedNormalWorld,				// UAV
	ComputeRootSkinnedTangentWorld,				// UAV
	ComputeRootSkinnedBiTangentWorld,			// UAV
	ComputeRootSkinnedVerticesNum,				// ROOTCONSTANT
	ComputeRootParametersNum					// PAMETERSNUM
};

enum CommandListState
{
	CommandListStateMain = 0,
	CommandListStateMid,
	CommandListStatePost,
	CommandListStateNum
};

enum InstanceShaderPSType
{
	InstanceShaderPS_Default = 0,
	InstanceShaderPS_Blend,
	InstanceShaderPS_Grass,
	InstanceShaderPS_Bounding,
	InstanceShaderPS_Num
};

enum SkinnedShaderPSType
{
	SkinnedShaderPS_Skinned = 0,
	SkinnedShaderPS_Standard,
	SkinnedShaderPS_Bounding,
	SkinnedShaderPS_Num
};

enum TextureResource
{
	ResourceTexture2D = 0,
	ResourceTexture2D_Array,	// texture들의 배열
	ResourceTexture2DArray,		// textureArray
	ResourceTextureCube,
	ResourceBuffer
};

enum ObjectType
{
	ObjectTypePlayer = 0,
	ObjectTypeObject = 1
};

enum ShaderType
{
	ShaderTypeStandard = 1,
	ShaderTypeInstance,
	ShaderTypeSkinned,
	ShaderTypeBounding
};

enum VertexStructure
{
	VertexPosition = 0x01,
	VertexColor = 0x02,
	VertexNormal = 0x04,
	VertexTangent = 0x08,
	VertexTexCoord0 = 0x10,
	VertexTexCoord1 = 0x20
};

enum PlayerJob
{
	PlayerJobRifle = 0,
	PlayerJobSword,
	PlayerJobBow
};

enum PlayerDirection
{
	DirectionNoMove = 0,

	DirectionForword = 2,
	DirectionBackword = 4,
	DirectionRight = 7,
	DirectionLeft = 8,

	DirectionFR = DirectionForword + DirectionRight,
	DirectionFL = DirectionForword + DirectionLeft,
	DirectionBR = DirectionBackword + DirectionRight,
	DirectionBL = DirectionBackword + DirectionLeft,

	DirectionUp = 0x10,
	DirectionDown = 0x20
};

enum AnimationType
{
	AnimationTypeOnce = 0,
	AnimationTypeLoop,
	AnimationTypePingpong
};

enum AnimationChange // 총 모델 애니메이션 번호
{
	MainCharacterAim = 0,
	MainCharacterIdle = 1,
	MainCharacterIdle2,
	MainCharacterIdle3,
	MainCharacterIdle4,

	MainCharacterAimUp = 5,
	MainCharacterAimDown = 6,

	MainCharacterWalk = 7,
	MainCharacterWalkBack = 8,
	MainCharacterWalkLeft = 9,
	MainCharacterWalkRight = 10,

	MainCharacterRun = 11,
	MainCharacterRunBack = 12,
	MainCharacterRunLeft = 13,
	MainCharacterRunRight = 14,

	MainCharacterFire = 17,
	MainCharacterWalkingFire = 18,
	MainCharacterReload = 19,
	MainCharacterWalkingReload = 20,
	MainCharacterHit = 21,
	MainCharacterDeath = 22
};

enum SwordModelAnimationChange
{
	SwordCharacterIdle = 0,
	SwordCharacterIdle2,

	SwordCharacterWalk,
	SwordCharacterWalkBack,
	SwordCharacterWalkLeft,
	SwordCharacterWalkRight,

	SwordCharacterRun,
	SwordCharacterRunBack,
	SwordCharacterRunLeft,
	SwordCharacterRunRight,

	SwordCharacterAttack,
	SwordCharacterAttack2,
	SwordCharacterAttack3,

	SwordCharacterHit,
	SwordCharacterDeath
};

enum BowModelAnimationChange
{
	BowCharacterIdle = 0,
	BowCharacterAiming,

	BowCharacterWalk,
	BowCharacterWalkBack,
	BowCharacterWalkLeft,
	BowCharacterWalkRight,

	BowCharacterAimWalk,
	BowCharacterAimWalkBack,
	BowCharacterAimWalkLeft,
	BowCharacterAimWalkRight,

	BowCharacterRun,
	BowCharacterRunBack,
	BowCharacterRunLeft,
	BowCharacterRunRight,

	BowCharacterDrawArrow,
	BowCharacterOverDraw, // 당기는 모션 -> 에이밍 모션 으로 넘어가서 유지 하는 식으로
	BowCharacterRelease,

	BowCharacterPunch,
	BowCharacterKick,
	BowCharacterReact,
	BowCharacterDeath
};

enum ZombieAnimationChange {
	ZombieDizzIdle,
	ZombieDrunkIdle,
	ZombieHeadMoveIdle,
	ZombieMiddleHandle,
	ZombieScreamIdle,
	ZombieWalk1,
	ZombieWalk2,
	ZombieWalk3,
	ZombieRun,
	InjuredRun,
	ShakeHandRun,
	HeadFreeRun,
	ZombieAttack1,
	ZombieAttack2,
	ZombieKick,
	ZombieReaction1,
	ZombieReaction2,
	ZombieMiddleReaction,
	ZombieDeath1,
	ZombieDeath2
};
enum BossAnimationChange {
	BossIdle,
	BossIdle2,
	BossIdle3,
	BossIdle4,
	BossRoar,
	BossWalk,
	BossRun,
	BossPunch,
	BossSwiping,
	BossJumpAttack,
	BossDeath
};


enum ShaderOrder
{
	ShaderOrderTerrain = 0,
	ShaderOrderInstancing,
	ShaderOrderSkinnedInstancing,
	ShaderOrderSkinnedObject,
	ShaderOrderParticle,
	ShaderOrderWeaponTrail,
	ShaderOrderSkyBox,
	ShaderOrderUI,
	ShaderOrderMiniMap
};

enum TREEMESHORDER {
	DOWNLEFTTREE,
	DOWNTREE,
	DOWNRIGHTTREE,
	LEFTTREE,
	RIGHTTREE,
	UPLEFTTREE,
	UPTREE,
	UPRIGHTTREE
};

enum ObjectTypeForBounding {
	ShaderTypeForBoundingTerrain = 0,
	ShaderTypeForBoundingInstancing,
	ShaderTypeForBoundingSkinnedInstancing
};


enum ZOMBIESTATE {
	IDLE,
	WANDER,
	CHASE,
	ATTACK,
	DEATH
};

enum BoundingOrder {
	BoundingOrderAll = 0,
	BoundingOrderRULeg,
	BoundingOrderRDLeg,
	BoundingOrderBody,
	BoundingOrderHead,
	BoundingOrderRUShoulder,
	BoundingOrderRDShoulder,
	BoundingOrderLUShoulder,
	BoundingOrderLDShoulder,
	BoundingOrderLULeg,
	BoundingOrderLDLeg
};

enum CanGoWay {
	CanGoWayLeft = 0x01,
	CanGoWayRight = 0x02,
	CanGoWayDown = 0x04,
	CanGoWayUp = 0x08
};

// 열거형이 아니거나 hlsl 에서 같은 값을 사용할 경우 #define을 사용
#define MATERIAL_DIFFUSE_MAP		0x01
#define MATERIAL_NORMAL_MAP			0x02
#define MATERIAL_SPECULAR_MAP		0x04
#define MATERIAL_METALLIC_MAP		0x08
#define MATERIAL_EMISSION_MAP		0x10

#define PARTICLE_TYPE_DISAPPEAR		0
#define PARTICLE_TYPE_FOREVER		1
#define PARTICLE_TYPE_TORNADO		2
#define PARTICLE_TYPE_SMALLER		3

#define MAX_LIGHTS					50
#define MAX_MATERIALS				700

#define POINT_LIGHT					1
#define SPOT_LIGHT					2
#define DIRECTIONAL_LIGHT			3

#define SCENE_START					0
#define SCENE_LOBBY					1
#define SCENE_INGAME				2
#define SCENE_INGAME_ZOOM			3
#define SCENE_INGAME_ITEM			4
#define SCENE_LOADING				5
#define SCENE_GAMEOVER				6
#define SCENE_VICTORY				7


#define SKINNED_BUFFERSIZE	60
#define BUFFERSIZE	1000
// BufferSize (하나의 버퍼를 생성할 때 BUFFERSIZE만큼의 크기로 버퍼를 생성한다. 해당 크기만큼 정보를 담을 수 있다.)
#define PARTICLESIZE 32
// Particle 한덩어리당 PARTICLESIZE만큼의 입자를 갖는다.
#define BLOCKSIZE 256.0f
// ComputeShader가 동작할 때 BLOCKSIZE만큼 동작한다. ex) [numthreads(BLOCKSIZE, 1, 1)] , Dispatch(static_cast<int>(ceil(PARTICLESIZE / BLOCKSIZE)), 1, 1);
#define SKINNED_ANIMATION_BONES		128
// 스키닝 애니메이션 본의 갯수
#define ANIMATION_CALLBACK_EPSILON	0.015f

#define VERTEXT_BONE_INDEX_WEIGHT		0x1000

#define LOD_VALUE 1000
#define NOPLAYER 1000.0f

#define FOG_LINEAR 1.0f
#define FOG_EXP 2.0f
#define FOG_EXP2

#define TERRAINBOUNDINGS 256

#define SCAR_AMMO 30
#define G28_AMMO 15
#define AK_AMMO 30
#define M4_AMMO 40
#define SNIPER_AMMO 5
#define BLACKAK_AMMO 40


#define _WINSOCK_DEPRECATED_NO_WARNINGS // 최신 VC++ 컴파일 시 경고 방지
#include "targetver.h"
#pragma comment ( lib , "ws2_32.lib" )

#include "targetver.h"

#define WIN32_LEAN_AND_MEAN             // 거의 사용되지 않는 내용을 Windows 헤더에서 제외합니다.
// Windows 헤더 파일
#include <windows.h>

// C 런타임 헤더 파일입니다.
#include <stdlib.h>
#include <malloc.h>
#include <memory.h>
#include <tchar.h>
#include <iostream>
#include <algorithm>
#include <vector>
#include <unordered_set>
#include <map>
#include <unordered_map>
#include <random>
#include <assert.h>
#include <Mmsystem.h>
#include <math.h>
#include <string>
#include <fstream>
#include <thread>
#include <mutex>
#include <atomic>
#include <chrono>
// 여기서 프로그램에 필요한 추가 헤더를 참조합니다.
#include <winsock2.h>
#include <string>
#include <wrl.h>
#include <shellapi.h>
#include <timeapi.h>

#include <comdef.h>
#include <d3d12.h>
#include <dxgi1_4.h>
#include <D3Dcompiler.h>
#include <DirectXMath.h>
#include <DirectXPackedVector.h>
#include <DirectXColors.h>
#include <DirectXCollision.h>

using namespace DirectX;
using namespace DirectX::PackedVector;

using Microsoft::WRL::ComPtr;

#pragma comment(lib, "d3dcompiler.lib")
#pragma comment(lib, "d3d12.lib")
#pragma comment(lib, "dxgi.lib")

#pragma comment(lib, "winmm.lib")
// timeGetTime() 사용하기 위한 pragma

#define FRAME_BUFFER_WIDTH		1280	//500
#define FRAME_BUFFER_HEIGHT		720		//500 
#define SHADOW_BUFFER_SIZE		2048
#define MAX_CASCADE_SIZE		3

#define CAMERAS_NUM				MAX_CASCADE_SIZE + 1
#define RENDER_THREADS_NUM		1
#define COMPUTE_THREADS_NUM		1
#define THREADS_NUM				RENDER_THREADS_NUM + COMPUTE_THREADS_NUM
#define COMMAND_IDX_NUM			2	
#define PLAYERSPEED				160.0f
#define ZOMBIESPEED				5.0f
#define ZOMBIERUN				30.0f
#define ZOMBIEBOUNCING			30.0f
#define ZOMBIE_MODEL_START		3
#define ZOMBIE_START_NUMBER     1000
#define NOPOTAL					100
#define MIDDLEBOSS 8
#define FINALBOSS 14

// swapChainBufferIdx와 같은 값을 갖는다. 

constexpr auto MAX_PACKET_SIZE = 255;
constexpr auto MAX_BUF_SIZE = 1024;

#define SERVERIP   "127.0.0.1"
#define SERVERPORT 9000			
#define WM_SOCKET  (WM_USER + 1)
//#define _WITH_SWAPCHAIN_FULLSCREEN_STATE

//#define _WITH_PLAYER_TOP

#define _WITH_SHADOW_USE

#define EPSILION				1.0e-10f

extern UINT gnCbvSrvDescriptorIncrementSize;

extern ID3D12Resource *CreateBufferResource(ID3D12Device*device, ID3D12GraphicsCommandList* commandList, void* data, UINT bytes, D3D12_HEAP_TYPE heapType = D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATES resourceStates = D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, ID3D12Resource **uploadBuffer = NULL, D3D12_RESOURCE_FLAGS flags = D3D12_RESOURCE_FLAG_NONE);
extern ID3D12Resource *CreateTextureResourceFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, wchar_t *pszFileName, ID3D12Resource **ppd3dUploadBuffer, D3D12_RESOURCE_STATES d3dResourceStates = D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
extern ID3D12Resource* CreateTexture2DResource(ID3D12Device* pd3dDevice, ID3D12GraphicsCommandList* pd3dCommandList, UINT nWidth, UINT nHeight, UINT nElements, UINT nMipLevels, DXGI_FORMAT dxgiFormat, D3D12_RESOURCE_FLAGS d3dResourceFlags, D3D12_RESOURCE_STATES d3dResourceStates, D3D12_CLEAR_VALUE* pd3dClearValue);
extern void ResourceBarrier(ID3D12GraphicsCommandList* commandList, ID3D12Resource* resource, D3D12_RESOURCE_STATES before, D3D12_RESOURCE_STATES after);
extern void WaitForGpuComplete(ID3D12CommandQueue* commandQueue, ID3D12Fence* fence, UINT64 fenceValue, HANDLE fenceEvent);

extern BYTE ReadStringFromFile(FILE *pInFile, char *pstrToken);
extern int ReadIntegerFromFile(FILE *pInFile);
extern float ReadFloatFromFile(FILE *pInFile);

// XMVECTOR를 사용한 연산이 이득이므로 XMVECTOR로 변환하여 계산하는 경우가 많음. (XMLoadFloat3)
// 반환 값은 XMFLOAT3로 반환하는 경우가 많음 (XMStoreFLoat3)

inline bool IsZero(float value) { return ((fabsf(value) < EPSILION)); }
inline bool IsZero(float fValue, float fEpsilon) { return((fabsf(fValue) < fEpsilon)); }
inline bool IsEqual(float A, float B) { return (::IsZero(A - B)); }
inline bool IsEqual(float fA, float fB, float fEpsilon) { return(::IsZero(fA - fB, fEpsilon)); }
inline float InverseSqrt(float value) { return 1.0f / sqrtf(value); }
inline void Swap(float* S, float* T) { float temp = *S; *S = *T; *T = temp; }
inline float RandomPercent();

//3차원 벡터의 연산
namespace Vector3
{
	// XMVECTOR의 값을 XMFLOAT3로 변환시켜주는 함수
	inline XMFLOAT3 XMVectorToFloat3(const XMVECTOR& vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, vector);
		return(result);
	}

	// XMFLOAT3의 스칼라 곱 연산 (정규화된 벡터로 연산 수행)
	inline XMFLOAT3 ScalarProduct(const XMFLOAT3& vector, float scalar, bool normalize = true)
	{
		XMFLOAT3 result;
		if (normalize)
			XMStoreFloat3(&result, XMVector3Normalize(XMLoadFloat3(&vector)) *scalar);
		else
			XMStoreFloat3(&result, XMLoadFloat3(&vector) * scalar);
		return(result);
	}

	// XMFLOAT3 두개의 덧셈 연산
	inline XMFLOAT3 Add(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMLoadFloat3(&vector1) + XMLoadFloat3(&vector2));
		return(result);
	}

	// XMFLOAT3 두개의 덧셈 연산( 두번째 값은 스칼라 곱 후 더함 )
	inline XMFLOAT3 Add(const XMFLOAT3& vector1, const XMFLOAT3& vector2, float scalar)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMLoadFloat3(&vector1) + (XMLoadFloat3(&vector2) *scalar));
		return(result);
	}

	// XMFLOAT3 두개의 뺄셈 연산
	inline XMFLOAT3 Subtract(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMLoadFloat3(&vector1) - XMLoadFloat3(&vector2));
		return(result);
	}

	// XMFLOAT3 두개의 내적 연산
	inline float DotProduct(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3Dot(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2)));
		return(result.x);
	}

	// XMFLOAT3 두개의 외적 연산 (정규화된 벡터로 연산 수행)
	inline XMFLOAT3 CrossProduct(const XMFLOAT3& vector1, const XMFLOAT3& vector2, bool normalize = true)
	{
		XMFLOAT3 result;
		if (normalize)
			XMStoreFloat3(&result, XMVector3Normalize(XMVector3Cross(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2))));
		else
			XMStoreFloat3(&result, XMVector3Cross(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2)));
		return(result);
	}

	// XMFLOAT3 의 정규화 연산
	inline XMFLOAT3 Normalize(const XMFLOAT3& vector)
	{
		XMFLOAT3 normal;
		XMStoreFloat3(&normal, XMVector3Normalize(XMLoadFloat3(&vector)));
		return(normal);
	}

	// XMFLOAT3 의 크기를 구하는 함수
	inline float Length(const XMFLOAT3& vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3Length(XMLoadFloat3(&vector)));
		return(result.x);
	}

	// XMFLOAT3 두개 사이의 각도를 구하는 함수 (도 값으로 리턴함)
	inline float Angle(const XMVECTOR& vector1, const XMVECTOR& vector2)
	{
		XMVECTOR angle = XMVector3AngleBetweenNormals(vector1, vector2);
		return(XMConvertToDegrees(acosf(XMVectorGetX(angle))));
		// 라디안 값을 도 값으로 바꾸어 반환
	}

	// XMFLOAT3 두개 사이의 각도를 구하는 함수 (도 값으로 리턴함)
	inline float Angle(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		return(Angle(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2)));
	}

	// XMFLOAT3 와 행렬 연산 (입력 벡터는 노멀 벡터)
	// 회전 및 스케일링을 위해 입력 행 0, 1, 2를 사용하여 변환을 수행하고 행 3을 무시합니다.
	inline XMFLOAT3 TransformNormal(const XMFLOAT3& vector, const XMMATRIX& transform)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3TransformNormal(XMLoadFloat3(&vector), transform));
		return(result);
	}

	// XMFLOAT3 와 행렬 연산 
	// 입력 벡터의 w 구성 요소를 무시하고 대신 1.0 값을 사용합니다. 반환되는 벡터의 w 구성 요소는 항상 1.0입니다.
	inline XMFLOAT3 TransformCoord(const XMFLOAT3& vector, const XMMATRIX& transform)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3TransformCoord(XMLoadFloat3(&vector), transform));
		return(result);
	}

	// XMFLOAT3 와 행렬 연산 
	inline XMFLOAT3 TransformCoord(const XMFLOAT3& vector, const XMFLOAT4X4& matrix)
	{
		return(TransformCoord(vector, XMLoadFloat4x4(&matrix)));
	}

	// 3-차원 벡터가 영벡터인 가를 반환하는 함수이다
	inline bool IsZero(XMFLOAT3& vector)
	{
		if (::IsZero(vector.x) && ::IsZero(vector.y) && ::IsZero(vector.z))
			return (true);
		return false;
	}

	inline XMFLOAT3 Floor(const XMFLOAT3 vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVectorFloor(XMLoadFloat3(&vector)));
		return(result);
	}

	inline XMFLOAT3 Round(const XMFLOAT3& vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVectorRound(XMLoadFloat3(&vector)));
		return(result);
	}

}
//4차원 벡터의 연산
namespace Vector4
{
	// XMFLAOT4 두개의 덧셈 연산
	inline XMFLOAT4 Add(const XMFLOAT4& vector1, const XMFLOAT4& vector2)
	{
		XMFLOAT4 result;
		XMStoreFloat4(&result, XMLoadFloat4(&vector1) + XMLoadFloat4(&vector2));
		return(result);
	}

	// 4-차원 벡터와 스칼라(실수)의 곱을 반환하는 함수이다.
	inline XMFLOAT4 Multiply(float scalar, XMFLOAT4& vector)
	{
		XMFLOAT4 result;
		XMStoreFloat4(&result, scalar * XMLoadFloat4(&vector));
		return(result);
	}

	inline XMFLOAT4 TransformCoord(const XMFLOAT4& vector, const XMFLOAT4X4& matrix)
	{
		return TransformCoord(vector, matrix);
	}
}
//행렬의 연산
namespace Matrix4x4
{
	// 단위행렬 생성
	inline XMFLOAT4X4 Identity()
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixIdentity());
		return(result);
	}

	// 행렬 두개의 곱을 연산
	inline XMFLOAT4X4 Multiply(const XMFLOAT4X4& matrix1, const XMFLOAT4X4& matrix2)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMLoadFloat4x4(&matrix1) * XMLoadFloat4x4(&matrix2));
		return(result);
	}

	// 행렬 두개의 곱을 연산
	inline XMFLOAT4X4 Multiply(const XMFLOAT4X4& matrix1, const XMMATRIX& matrix2)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMLoadFloat4x4(&matrix1) * matrix2);
		return(result);
	}

	// 행렬 두개의 곱을 연산
	inline XMFLOAT4X4 Multiply(const XMMATRIX& matrix1, const XMFLOAT4X4& matrix2)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, matrix1 * XMLoadFloat4x4(&matrix2));
		return(result);
	}

	// 행렬의 역행렬을 구하는 함수
	inline XMFLOAT4X4 Inverse(const XMFLOAT4X4& matrix)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixInverse(NULL, XMLoadFloat4x4(&matrix)));
		return(result);
	}

	// 0.0f 의 값으로 채워진 4x4 행렬
	inline XMFLOAT4X4 Zero()
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4(&xmf4x4Result, XMMatrixSet(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f));
		return(xmf4x4Result);
	}

	// 두 행렬을 보간 계산하는 함수
	inline XMFLOAT4X4 Interpolate(XMFLOAT4X4& xmf4x4Matrix1, XMFLOAT4X4& xmf4x4Matrix2, float t)
	{
		XMFLOAT4X4 xmf4x4Result;
		XMVECTOR S0, R0, T0, S1, R1, T1;
		XMMatrixDecompose(&S0, &R0, &T0, XMLoadFloat4x4(&xmf4x4Matrix1));
		XMMatrixDecompose(&S1, &R1, &T1, XMLoadFloat4x4(&xmf4x4Matrix2));
		XMVECTOR S = XMVectorLerp(S0, S1, t);
		XMVECTOR T = XMVectorLerp(T0, T1, t);
		XMVECTOR R = XMQuaternionSlerp(R0, R1, t);
		XMStoreFloat4x4(&xmf4x4Result, XMMatrixAffineTransformation(S, XMVectorZero(), R, T));
		return(xmf4x4Result);
	}

	// 행렬의 스케일을 늘리는 함수
	inline XMFLOAT4X4 Scale(XMFLOAT4X4& xmf4x4Matrix, float fScale)
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4(&xmf4x4Result, XMLoadFloat4x4(&xmf4x4Matrix) * fScale);
		/*
				XMVECTOR S, R, T;
				XMMatrixDecompose(&S, &R, &T, XMLoadFloat4x4(&xmf4x4Matrix));
				S = XMVectorScale(S, fScale);
				T = XMVectorScale(T, fScale);
				R = XMVectorScale(R, fScale);
				//R = XMQuaternionMultiply(R, XMVectorSet(0, 0, 0, fScale));
				XMStoreFloat4x4(&xmf4x4Result, XMMatrixAffineTransformation(S, XMVectorZero(), R, T));
		*/
		return(xmf4x4Result);
	}

	// 두 행렬을 더하는 함수
	inline XMFLOAT4X4 Add(XMFLOAT4X4& xmmtx4x4Matrix1, XMFLOAT4X4& xmmtx4x4Matrix2)
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4(&xmf4x4Result, XMLoadFloat4x4(&xmmtx4x4Matrix1) + XMLoadFloat4x4(&xmmtx4x4Matrix2));
		return(xmf4x4Result);
	}

	// 행렬의 전치행렬을 구하는 함수
	inline XMFLOAT4X4 Transpose(const XMFLOAT4X4& xmmtx4x4Matrix)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixTranspose(XMLoadFloat4x4(&xmmtx4x4Matrix)));
		return(xmmtx4x4Result);
	}

	// 시야 기반 왼손좌표계 투영변환행렬을 만든다. 
	// FovAngleY:하향시야각(라디안), AspectRatio:뷰 공간의 가로,세로 비율X:Y, NearZ:근평면까지의 거리(0이상), FarZ:원평면까지의 거리(0이상)
	inline XMFLOAT4X4 PerspectiveFovLH(float fovAngleY, float aspectRatio, float nearZ, float farZ)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixPerspectiveFovLH(fovAngleY, aspectRatio, nearZ, farZ));
		return(result);
	}

	inline XMFLOAT4X4 OrthographicLH(float w, float h, float zn, float zf)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixOrthographicLH(w, h, zn, zf));
		return(result);
	}

	inline XMFLOAT4X4 OrthographicOffCenterLH(float l, float r, float b, float t, float zn, float zf)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixOrthographicOffCenterLH(l, r, b, t, zn, zf));
		return(result);
	}

	// 카메라의 위치, Look벡터, Up벡터를 사용하여 왼손좌표계의 카메라변환 행렬을 만든다.
	inline XMFLOAT4X4 LookAtLH(const XMFLOAT3& eyePosition, const XMFLOAT3& lookAtPosition, const XMFLOAT3& upDirection)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixLookAtLH(XMLoadFloat3(&eyePosition), XMLoadFloat3(&lookAtPosition), XMLoadFloat3(&upDirection)));
		return(result);
	}
}

//정점의 색상을 무작위로(Random) 설정하기 위해 사용한다. 각 정점의 색상은 난수(Random Number)를 생성하여 지정한다.
#define RANDOM_COLOR XMFLOAT4(rand() / float(RAND_MAX), rand() / float(RAND_MAX), rand() / float(RAND_MAX), rand() / float(RAND_MAX))
#define PI 3.141592f
inline float RandomPercent()
{
	std::default_random_engine dre;
	std::uniform_real_distribution<float> urd(-1, 1);

	return urd(dre);
}

inline float GetBoundingDamage(UINT order)
{
	if (order == BoundingOrderHead)
		return 2.0f;
	if (order == BoundingOrderBody)
		return 1.0f;
	if (order == BoundingOrderRDLeg || order == BoundingOrderRULeg || order == BoundingOrderLDLeg || order == BoundingOrderLULeg)
		return 0.7f;
	if (order == BoundingOrderRDShoulder || order == BoundingOrderRUShoulder || order == BoundingOrderLDShoulder || order == BoundingOrderLUShoulder)
		return 0.5f;
}

inline UINT GetBoundingWeapon(UINT order)
{
	if (order == 7) return 12;
	if (order == 8) return 7;
	if (order == 9) return 8;
	if (order == 10) return 9;
	if (order == 11) return 10;
	if (order == 12) return 11;
}

#define MODEL_WEAPON_START	7
#define MODEL_WEAPON_END	12
// 무기의 바운딩박스 시작, 끝 번호