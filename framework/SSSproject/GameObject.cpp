#include "stdafx.h"
#include "GameObject.h"
#include "Shader.h"
#include "Scene.h"
#include "Animation.h"
#include "Texture.h"
#include "Material.h"


int ReadIntegerFromFile(FILE *pInFile)
{
	int nValue = 0;
	UINT nReads = (UINT)::fread(&nValue, sizeof(int), 1, pInFile);
	return(nValue);
}

float ReadFloatFromFile(FILE *pInFile)
{
	float fValue = 0;
	UINT nReads = (UINT)::fread(&fValue, sizeof(float), 1, pInFile);
	return(fValue);
}

BYTE ReadStringFromFile(FILE *pInFile, char *pstrToken)
{
	BYTE nStrLength = 0;
	UINT nReads = 0;
	nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, pInFile);
	nReads = (UINT)::fread(pstrToken, sizeof(char), nStrLength, pInFile);
	pstrToken[nStrLength] = '\0';

	return(nStrLength);
}

GameObject::GameObject(int meshesNum)
{
	XMStoreFloat4x4(&world, XMMatrixIdentity());
	transform = Matrix4x4::Identity();

	this->meshesNum = meshesNum;
	if(this->meshesNum>0)
		meshes = new Mesh*[meshesNum];
	for (int i = 0; i < meshesNum; ++i)
	{
		meshes[i] = NULL;
	}
	maxVelocityXZ = 6.0f;
	friction = 20.0f;
	ctime = std::chrono::high_resolution_clock::now();
}

GameObject::~GameObject()
{
	ReleaseShaderVariables();

	if (meshes)
	{
		for (int i = 0; i < meshesNum; i++)
		{
			if (meshes[i]) {
				meshes[i]->Release();
				meshes[i] = NULL;
			}
		}
	}
	if (boundingMesh)
	{
		boundingMesh->Release();
		boundingMesh = NULL;
	}

	if (materials)
	{
		for (int i = 0; i < materialsNum; ++i)
		{
			if (materials[i]) {
				materials[i]->Release();
				materials[i] = NULL;
			}
		}
	}

	if (cbGameObject) {
		cbGameObject->Unmap(0, NULL);
		cbGameObject->Release();
	}

	if (materials) delete[] materials;

	if (skinnedAnimationController) delete skinnedAnimationController;
}

void GameObject::AddRef()
{
	references++;

	if (sibling) sibling->AddRef();
	if (child) child->AddRef();
}

void GameObject::Release()
{
	if (sibling) sibling->Release();
	if (child) child->Release();

	if (--references <= 0) delete this;
}

void GameObject::SetShader(Shader *shader)
{
	materialsNum = 1;
	materials = new Material*[materialsNum];
	materials[0] = new Material(0);
	materials[0]->SetShader(shader);
}
void GameObject::SetMesh(int idx, Mesh *mesh)
{
	if (this->meshes[idx]) 
		this->meshes[idx]->Release();
	this->meshes[idx] = mesh;
	if (this->meshes[idx]) 
		this->meshes[idx]->AddRef();
}

void GameObject::SetBoundingMesh(BoundingBoxMesh * mesh)
{
	boundingMesh = mesh;
}

void GameObject::SetMaterial(int idx, Material *material)
{
	if (this->materials[idx]) 
		this->materials[idx]->Release();
	this->materials[idx] = material;
	if (this->materials[idx]) 
		this->materials[idx]->AddRef();
}

void GameObject::SetMaterial(int idx, UINT reflection)
{
	if (!materials[idx]) materials[idx] = new Material(1);
	materials[idx]->reflection = reflection;
}

void GameObject::SetReflection(UINT reflection)
{
	this->reflection = reflection;

}

void GameObject::SetTextureMask(UINT texturemask)
{
	this->textureMask = texturemask;

}

void GameObject::SetTextureIdx(UINT idx)
{
	this->textureIdx = idx;
}

void GameObject::SetChild(GameObject *child)
{
	if (this->child)
	{
		if (child) child->sibling = this->child->sibling;
		this->child->sibling = child;
	}
	else
	{
		this->child = child;
	}
	if (child)
	{
		child->parent = this;
	}
}

void GameObject::SetChild(GameObject *pChild, bool referenceUpdate)
{
	if (pChild)
	{
		pChild->parent = this;
		if (referenceUpdate) pChild->AddRef();
	}
	if (child)
	{
		if (pChild) pChild->sibling = child->sibling;
		child->sibling = pChild;
	}
	else
	{
		child = pChild;
	}
}

void GameObject::SetPipelineStatesNum(UINT idx)
{
	pipeLineStatesNum = idx;
}

void GameObject::GetHierarchyMaterial(std::vector<MATERIAL>& materialReflection, int& idx)
{
	if (materials)
	{
		for (int i = 0; i < materialsNum; ++i)
		{
			MATERIAL temp;
			temp.diffuse = materials[i]->albedoColor;
			temp.ambient = materials[i]->ambientColor;
			temp.emissive = materials[i]->emissiveColor;
			temp.specular = materials[i]->specularColor;

			materialReflection.emplace_back(temp);
			materials[i]->reflection = 8 + idx++;
		}
	}
	if (sibling) sibling->GetHierarchyMaterial(materialReflection, idx);
	if (child) child->GetHierarchyMaterial(materialReflection, idx);
}


UINT GameObject::GetReflection()
{
	return reflection;
}

UINT GameObject::GetTextureMask()
{
	return textureMask;
}

int GameObject::GetFramesNum()
{
	return framesNum;
}

void GameObject::CreateMaterials(UINT materialsNum)
{
	materials = new Material*[materialsNum]; 
	this->materialsNum = materialsNum;
	for (int i = 0; i < (int)materialsNum; ++i)
	{
		materials[i] = NULL;
	}
}

void GameObject::CreateCbvGPUDescriptorHandles(int handlesNum)
{
	this->handlesNum = handlesNum;
	cbvGPUDescriptorHandle = new D3D12_GPU_DESCRIPTOR_HANDLE[handlesNum];
}

void GameObject::SetCbvGPUDescriptorHandlesOnlyStandardMesh(D3D12_GPU_DESCRIPTOR_HANDLE handle, int objectsNum, int framesNum)
{
	static int counter = 0;

	if (shaderType == ShaderTypeStandard)
	{
		CreateCbvGPUDescriptorHandles(objectsNum);

		for (int i = 0; i < objectsNum; ++i)
			cbvGPUDescriptorHandle[i].ptr = handle.ptr + (::gnCbvSrvDescriptorIncrementSize * (objectsNum * counter + i));

		counter++;
	}

	if (sibling) sibling->SetCbvGPUDescriptorHandlesOnlyStandardMesh(handle, objectsNum, framesNum);
	if (child) child->SetCbvGPUDescriptorHandlesOnlyStandardMesh(handle, objectsNum, framesNum);

	if (framesNum == counter)
		counter = 0;
}

void GameObject::SetCbvGPUDescriptorHandlePtrHierarchy(UINT64 cbvGPUDescriptorHandlePtr, int idx, int objectsNum)
{
	if (!cbvGPUDescriptorHandle && parent == NULL)
		CreateCbvGPUDescriptorHandles();
	if (!cbvGPUDescriptorHandle && parent != NULL)
		CreateCbvGPUDescriptorHandles(objectsNum);

	static int counter = 0;
	
	if (counter == 0)
		cbvGPUDescriptorHandle[counter++].ptr = cbvGPUDescriptorHandlePtr + (::gnCbvSrvDescriptorIncrementSize * idx);
	else
		cbvGPUDescriptorHandle[idx].ptr = cbvGPUDescriptorHandlePtr + (::gnCbvSrvDescriptorIncrementSize * (objectsNum * counter++ + idx));

	if (sibling) sibling->SetCbvGPUDescriptorHandlePtrHierarchy(cbvGPUDescriptorHandlePtr, idx, objectsNum);
	if (child) child->SetCbvGPUDescriptorHandlePtrHierarchy(cbvGPUDescriptorHandlePtr, idx, objectsNum);

	if (parent == NULL)
		counter = 0;
}

void GameObject::SetMaterialCbvGPUDescriptorHandlePtrHierarchy(UINT64 cbvGPUDescriptorHandlePtr, int maximum)
{
	static int counter = 0;

	if (materials)
	{
		for (int i = 0; i < materialsNum; ++i)
		{
			materials[i]->cbvGPUDescriptorHandle.ptr = cbvGPUDescriptorHandlePtr + (::gnCbvSrvDescriptorIncrementSize * ((maximum * counter) + i));
		}
	}
	counter++;
	if (sibling) sibling->SetMaterialCbvGPUDescriptorHandlePtrHierarchy(cbvGPUDescriptorHandlePtr, maximum);
	if (child) child->SetMaterialCbvGPUDescriptorHandlePtrHierarchy(cbvGPUDescriptorHandlePtr, maximum);

	if (parent == NULL)
		counter = 0;
}

void GameObject::ReleaseUploadBuffers()
{
	if (meshes)
	{ 
		for (int i = 0; i < meshesNum; i++)
		{
			if (meshes[i]) meshes[i]->ReleaseUploadBuffers();
		}
	}
	if (materials)
	{
		for (int i = 0; i < materialsNum; ++i)
		{
			if (materials[i]) materials[i]->ReleaseUploadBuffers();
		}
	}
	if (sibling) sibling->ReleaseUploadBuffers();
	if (child) child->ReleaseUploadBuffers();
}

void GameObject::UpdateTransform(XMFLOAT4X4 *parent)
{
	world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

	if (sibling) sibling->UpdateTransform(parent);
	if (child) child->UpdateTransform(&world);
}

void GameObject::UpdateTransformOnlyStandardMesh(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, int framesNum, XMFLOAT4X4 *parent)
{
	static int counter = 0;

	if (shaderType == ShaderTypeStandard) {

		world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

		CB_GAMEOBJECT_INFO *mappedCbGameObject = (CB_GAMEOBJECT_INFO *)((UINT8 *)cbMappedGameObjects[counter] + (cbGameObjectBytes * idx));
		XMStoreFloat4x4(&mappedCbGameObject->world, XMMatrixTranspose(XMLoadFloat4x4(&world)));

		counter++;
	}

	if (sibling) sibling->UpdateTransformOnlyStandardMesh(cbMappedGameObjects, cbGameObjectBytes, idx, framesNum, parent);
	if (child) child->UpdateTransformOnlyStandardMesh(cbMappedGameObjects, cbGameObjectBytes, idx, framesNum, &world);

	if (counter == framesNum)
		counter = 0;
}

WeaponPoint GameObject::UpdateTransformOnlyStandardMesh(VS_VB_INSTANCE_OBJECT** vbMappedGameObjects, int idx, int framesNum, int weaponNum, XMFLOAT4X4 *parent, XMFLOAT4X4* toArrow)
{
	static int counter = 0;
	static WeaponPoint result{};

	if (shaderType == ShaderTypeStandard) {

		world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

		if(toArrow != NULL && counter == 0)
			*toArrow = world;
		XMStoreFloat4x4(&vbMappedGameObjects[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&world)));

		if (weaponNum == counter)
		{
			if (this->child)
			{
				result.end = this->child->world;
				if (this->child->sibling)
					if(this->child->sibling->sibling)
					result.start = this->child->sibling->sibling->world;
			}
		}

		counter++;
	}

	if (sibling) sibling->UpdateTransformOnlyStandardMesh(vbMappedGameObjects, idx, framesNum, weaponNum, parent, toArrow);
	if (child) child->UpdateTransformOnlyStandardMesh(vbMappedGameObjects, idx, framesNum, weaponNum, &world, toArrow);


	if (counter == framesNum)
	{
		counter = 0;
	}

	return result;
}

void GameObject::UpdateTransform(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, XMFLOAT4X4 *parent)
{
	static int counter = 0;

	world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

	CB_GAMEOBJECT_INFO *mappedCbGameObject = (CB_GAMEOBJECT_INFO *)((UINT8 *)cbMappedGameObjects[counter] + (cbGameObjectBytes * idx));
	XMStoreFloat4x4(&mappedCbGameObject->world, XMMatrixTranspose(XMLoadFloat4x4(&world)));

	counter++;
	if (sibling) sibling->UpdateTransform(cbMappedGameObjects, cbGameObjectBytes, idx, parent);
	if (child) child->UpdateTransform(cbMappedGameObjects, cbGameObjectBytes, idx, &world);

	if (parent == NULL)
		counter = 0;
}

void GameObject::UpdateTransform(VS_VB_INSTANCE_OBJECT** worldCashe, int idx, XMFLOAT4X4* parent)
{
	static int counter = 0;

	world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;
	world = (isCollisionSkinnedModel) ? Matrix4x4::Multiply(transform, toParent->world) : world;
	
	XMStoreFloat4x4(&worldCashe[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&world)));

	counter++;
	if (sibling) sibling->UpdateTransform(worldCashe, idx, parent);
	if (child) child->UpdateTransform(worldCashe, idx, &world);

	if (parent == NULL)
		counter = 0;
}

void GameObject::UpdateBoundingTransForm(VS_VB_INSTANCE_BOUNDING * vbMappedBoudnigs, int idx, XMFLOAT4X4 pWorld, int bidx)
{
	auto fworld = Matrix4x4::Multiply(pWorld, world);
	XMStoreFloat4x4(&vbMappedBoudnigs[idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&fworld)));
}

void GameObject::UpdateSkinnedBoundingTransform(VS_VB_INSTANCE_BOUNDING** vbMappedBoundings, std::vector<BoundingOrientedBox>& obbs, std::vector<GameObject*>* objVec, int idx, int framesNum, XMFLOAT4X4* parent)
{
	static int counter = 0;

	if (shaderType == ShaderTypeBounding) {

		world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

		XMStoreFloat4x4(&vbMappedBoundings[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&world)));
		if (_isnan(world._11) == false) {
			BoundingOrientedBox tempObb{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.5f, 0.5f, 0.5f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
			tempObb.Transform(obbs[counter], XMLoadFloat4x4(&world));
			if(objVec)
				(*objVec)[counter]->world = world;
		}
		counter++;
	}

	if (sibling) sibling->UpdateSkinnedBoundingTransform(vbMappedBoundings, obbs, objVec, idx, framesNum, parent);
	if (child) child->UpdateSkinnedBoundingTransform(vbMappedBoundings, obbs, objVec, idx, framesNum, &world);

	if (counter == framesNum)
		counter = 0;
}


void GameObject::Animate(float timeElapsed, XMFLOAT4X4* parent)
{
	OnPrepareRender();
	if (skinnedAnimationController)
	{
	if(!skinnedAnimationController->middleTransform)
		skinnedAnimationController->AdvanceTime(timeElapsed, this);
	else
		skinnedAnimationController->MiddleAnimation(timeElapsed, this);
	}
	if (sibling) sibling->Animate(timeElapsed, parent);
	if (child) child->Animate(timeElapsed, &world);
}

std::vector<BoundingOrientedBox>& GameObject::GetobbVector()
{
	return obbvec;
}

GameObject *GameObject::FindFrame(char *frameName)
{
	GameObject *frameObject = NULL;
	if (!strncmp(this->frameName, frameName, strlen(frameName))) return(this);

	if (sibling) if (frameObject = sibling->FindFrame(frameName)) return(frameObject);
	if (child) if (frameObject = child->FindFrame(frameName)) return(frameObject);

	return(NULL);
}

_TCHAR * GameObject::FindReplicatedTexture(_TCHAR * textureName)
{
	for (int i = 0; i < materialsNum; i++)
	{
		if (materials[i])
		{
			for (int j = 0; j < materials[i]->texturesNum; j++)
			{
				if (materials[i]->textures[j])
				{
					if (!_tcsncmp(materials[i]->textureNames[j], textureName, _tcslen(textureName))) return(materials[i]->textureNames[j]);
				}
			}
		}
	}
	_TCHAR *name = NULL;
	if (sibling) if (name = sibling->FindReplicatedTexture(textureName)) return(name);
	if (child) if (name = child->FindReplicatedTexture(textureName)) return(name);

	return(NULL);
}

void GameObject::FindAndSetSkinnedMesh(SkinnedMesh **SkinnedMeshes, int *SkinnedMeshnum)
{
	if (meshes[0] && (meshes[0]->GetType() & VERTEXT_BONE_INDEX_WEIGHT)) SkinnedMeshes[(*SkinnedMeshnum)++] = (SkinnedMesh *)(meshes[0]);

	if (sibling) sibling->FindAndSetSkinnedMesh(SkinnedMeshes, SkinnedMeshnum);
	if (child) child->FindAndSetSkinnedMesh(SkinnedMeshes, SkinnedMeshnum);
}

void GameObject::LoadMaterialsFromFile(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, GameObject * parent, FILE * inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5])
{
	char pstrToken[64] = { '\0' };

	int nMaterial = 0;
	BYTE nStrLength = 0;

	UINT nReads = (UINT)::fread(&materialsNum, sizeof(int), 1, inFile);


	materials = new Material*[materialsNum];
	for (int i = 0; i < materialsNum; i++)
		materials[i] = NULL;

	Material *pMaterial = NULL;

	for (; ; )
	{
		nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
		nReads = (UINT)::fread(pstrToken, sizeof(char), nStrLength, inFile);
		pstrToken[nStrLength] = '\0';

		if (!strcmp(pstrToken, "<Material>:"))
		{
			nReads = (UINT)::fread(&nMaterial, sizeof(int), 1, inFile);

			pMaterial = new Material(7); //0:Albedo, 1:Specular, 2:Metallic, 3:Normal, 4:Emission, 5:DetailAlbedo, 6:DetailNormal

			UINT nMeshType = GetMeshType();
			if (nMeshType & (VertexPosition | VertexNormal | VertexTangent | VertexTexCoord0))
			{
				if (nMeshType & VERTEXT_BONE_INDEX_WEIGHT)
				{
					pMaterial->SetSkinnedAnimationShader();
				}
				else
				{
					pMaterial->SetStandardShader();
				}
			}

			SetMaterial(nMaterial, pMaterial);
		}
		else if (!strcmp(pstrToken, "<AlbedoColor>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->albedoColor), sizeof(float), 4, inFile);
		}
		else if (!strcmp(pstrToken, "<EmissiveColor>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->emissiveColor), sizeof(float), 4, inFile);
		}
		else if (!strcmp(pstrToken, "<SpecularColor>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->specularColor), sizeof(float), 4, inFile);
		}
		else if (!strcmp(pstrToken, "<Glossiness>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->glossiness), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<Smoothness>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->smoothness), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<Metallic>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->metallic), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<SpecularHighlight>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->specularHighlight), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<GlossyReflection>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->glossyReflection), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<AlbedoMap>:"))
		{
			pMaterial->LoadTextureFromFile(device, commandList, MATERIAL_DIFFUSE_MAP, 6, pMaterial->textureNames[0], &(pMaterial->textures[0]), parent, inFile, folderName, textureMapNames.diffuse, textureIdx[0]);
		}
		else if (!strcmp(pstrToken, "<NormalMap>:"))
		{
			materials[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_NORMAL_MAP, 7, pMaterial->textureNames[1], &(pMaterial->textures[1]), parent, inFile, folderName, textureMapNames.normal, textureIdx[1]);
		}
		else if (!strcmp(pstrToken, "<SpecularMap>:"))
		{
			materials[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_SPECULAR_MAP, 8, pMaterial->textureNames[2], &(pMaterial->textures[2]), parent, inFile, folderName, textureMapNames.specular, textureIdx[2]);
		}
		else if (!strcmp(pstrToken, "<MetallicMap>:"))
		{
			materials[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_METALLIC_MAP, 9, pMaterial->textureNames[3], &(pMaterial->textures[3]), parent, inFile, folderName, textureMapNames.metallic, textureIdx[3]);
		}
		else if (!strcmp(pstrToken, "<EmissionMap>:"))
		{
			char pstrTextureName[64] = { '\0' };

			BYTE nStrLength = 64;
			UINT nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
			nReads = (UINT)::fread(pstrTextureName, sizeof(char), nStrLength, inFile);
		}
		else if (!strcmp(pstrToken, "<DetailAlbedoMap>:"))
		{
			char pstrTextureName[64] = { '\0' };

			BYTE nStrLength = 64;
			UINT nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
			nReads = (UINT)::fread(pstrTextureName, sizeof(char), nStrLength, inFile);
		}
		else if (!strcmp(pstrToken, "<DetailNormalMap>:"))
		{
			char pstrTextureName[64] = { '\0' };

			BYTE nStrLength = 64;
			UINT nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
			nReads = (UINT)::fread(pstrTextureName, sizeof(char), nStrLength, inFile);
		}
		else if (!strcmp(pstrToken, "</Materials>"))
		{
			break;
		}
	}
}

void GameObject::LoadAnimationFromFile(FILE *pInFile, LoadedModelInfo *pLoadedModel)
{
	char pstrToken[64] = { '\0' };
	UINT nReads = 0;

	int nAnimationSets = 0;

	for (; ; )
	{
		::ReadStringFromFile(pInFile, pstrToken);
		if (!strcmp(pstrToken, "<AnimationSets>:"))
		{
			nAnimationSets = ::ReadIntegerFromFile(pInFile);
			pLoadedModel->animationSets = new AnimationSets(nAnimationSets);
		}
		else if (!strcmp(pstrToken, "<FrameNames>:"))
		{
			pLoadedModel->animationSets->animatedBoneFrames = ::ReadIntegerFromFile(pInFile);
			pLoadedModel->animationSets->animatedBoneFrameCaches = new GameObject*[pLoadedModel->animationSets->animatedBoneFrames];

			for (int j = 0; j < pLoadedModel->animationSets->animatedBoneFrames; j++)
			{
				::ReadStringFromFile(pInFile, pstrToken);
				pLoadedModel->animationSets->animatedBoneFrameCaches[j] = pLoadedModel->modelRootObject->FindFrame(pstrToken);

#ifdef _WITH_DEBUG_SKINNING_BONE
				TCHAR pstrDebug[256] = { 0 };
				TCHAR pwstrAnimationBoneName[64] = { 0 };
				TCHAR pwstrBoneCacheName[64] = { 0 };
				size_t nConverted = 0;
				mbstowcs_s(&nConverted, pwstrAnimationBoneName, 64, pstrToken, _TRUNCATE);
				mbstowcs_s(&nConverted, pwstrBoneCacheName, 64, pLoadedModel->m_ppAnimatedBoneFrameCaches[j]->m_pstrFrameName, _TRUNCATE);
				_stprintf_s(pstrDebug, 256, _T("AnimationBoneFrame:: Cache(%s) AnimationBone(%s)\n"), pwstrBoneCacheName, pwstrAnimationBoneName);
				OutputDebugString(pstrDebug);
#endif
			}
		}
		else if (!strcmp(pstrToken, "<AnimationSet>:"))
		{
			int nAnimationSet = ::ReadIntegerFromFile(pInFile);

			::ReadStringFromFile(pInFile, pstrToken); //Animation Set Name

			float fLength = ::ReadFloatFromFile(pInFile);
			int nFramesPerSecond = ::ReadIntegerFromFile(pInFile);
			int nKeyFrames = ::ReadIntegerFromFile(pInFile);

			pLoadedModel->animationSets->animationSets[nAnimationSet] = new AnimationSet(fLength, nFramesPerSecond, nKeyFrames, pLoadedModel->animationSets->animatedBoneFrames, pstrToken);

			for (int i = 0; i < nKeyFrames; i++)
			{
				::ReadStringFromFile(pInFile, pstrToken);
				if (!strcmp(pstrToken, "<Transforms>:"))
				{
					int nKey = ::ReadIntegerFromFile(pInFile); //i
					float fKeyTime = ::ReadFloatFromFile(pInFile);

					AnimationSet *animationSet = pLoadedModel->animationSets->animationSets[nAnimationSet];
					animationSet->keyFrameTimes[i] = fKeyTime;
					nReads = (UINT)::fread(animationSet->keyFrameTransforms[i], sizeof(XMFLOAT4X4), pLoadedModel->animationSets->animatedBoneFrames, pInFile);
				}
			}
		}
		else if (!strcmp(pstrToken, "</AnimationSets>"))
		{
			break;
		}
	}
}


GameObject* GameObject::LoadFrameHierarchyFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, GameObject* parent, FILE* inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5], int& frameCounter, int& materialCounter, int* skinnedMeshsNum, int* standardMeshNum, int* boundingMeshNum)
{
	char token[64] = { '\0' };

	BYTE strLength = 0;
	UINT readsNum = 0;

	int frameNum = 0, texturesNum = 0;
	static int maximum = 0;
	GameObject *gameObject = NULL;

	for (; ; )
	{
		::ReadStringFromFile(inFile, token);

		if (!strcmp(token, "<Frame>:"))
		{
			gameObject = new GameObject(1);

			readsNum = (UINT)::fread(&frameNum, sizeof(int), 1, inFile);
			readsNum = (UINT)::fread(&texturesNum, sizeof(int), 1, inFile);

			readsNum = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
			readsNum = (UINT)::fread(gameObject->frameName, sizeof(char), strLength, inFile);
			gameObject->frameName[strLength] = '\0';
			frameCounter++;
		}
		else if (!strcmp(token, "<Transform>:"))
		{
			XMFLOAT3 xmf3Position, xmf3Rotation, xmf3Scale;
			XMFLOAT4 xmf4Rotation;
			readsNum = (UINT)::fread(&xmf3Position, sizeof(float), 3, inFile);
			readsNum = (UINT)::fread(&xmf3Rotation, sizeof(float), 3, inFile); //Euler Angle
			readsNum = (UINT)::fread(&xmf3Scale, sizeof(float), 3, inFile);
			readsNum = (UINT)::fread(&xmf4Rotation, sizeof(float), 4, inFile); //Quaternion
		}
		else if (!strcmp(token, "<TransformMatrix>:"))
		{
			readsNum = (UINT)::fread(&gameObject->transform, sizeof(float), 16, inFile);
		}
		else if (!strcmp(token, "<Mesh>:"))
		{
			StandardMesh *mesh = new StandardMesh(device, commandList);
			BoundingBoxMesh* bMesh = NULL;

			mesh->LoadMeshFromFile(device, commandList, inFile);

			if (strncmp(mesh->GetmeshName(), "Cube", 256) == 0)
			{
				bMesh = new BoundingBoxMesh(device, commandList, 0.5f, 0.5f, 0.5f);
				if (boundingMeshNum) {
					bMesh->meshnum = *boundingMeshNum;
				}
				gameObject->SetBoundingMesh(bMesh);
				gameObject->shaderType = ShaderTypeBounding;
				if (boundingMeshNum) (*boundingMeshNum)++;
			}
			else 
			{
				if (standardMeshNum) {
					mesh->meshnum = *standardMeshNum;
				}
				gameObject->SetMesh(0, mesh);
				gameObject->shaderType = ShaderTypeStandard;
				if (standardMeshNum) (*standardMeshNum)++;
			}
		}
		else if (!strcmp(token, "<SkinningInfo>:"))
		{
			if (skinnedMeshsNum) (*skinnedMeshsNum)++;

			SkinnedMesh *pSkinnedMesh = new SkinnedMesh(device, commandList);
			pSkinnedMesh->LoadSkinInfoFromFile(device, commandList, inFile);
			pSkinnedMesh->CreateShaderVariables(device, commandList);

			::ReadStringFromFile(inFile, token); //<Mesh>:
			if (!strcmp(token, "<Mesh>:")) pSkinnedMesh->LoadMeshFromFile(device, commandList, inFile);

			gameObject->SetMesh(0, pSkinnedMesh);
			gameObject->shaderType = ShaderTypeSkinned;
		}
		else if (!strcmp(token, "<Materials>:"))
		{
			gameObject->LoadMaterialsFromFile(device, commandList, parent, inFile, folderName, textureMapNames, textureIdx);
			materialCounter = max(materialCounter, gameObject->materialsNum);
		}
		else if (!strcmp(token, "<Children>:"))
		{
			int childsNum = 0;
			readsNum = (UINT)::fread(&childsNum, sizeof(int), 1, inFile);
			if (childsNum > 0)
			{
				for (int i = 0; i < childsNum; i++)
				{
					GameObject *child = GameObject::LoadFrameHierarchyFromFile(device, commandList, gameObject, inFile, folderName, textureMapNames, textureIdx, frameCounter, materialCounter, skinnedMeshsNum, standardMeshNum, boundingMeshNum);
					if (child) gameObject->SetChild(child);
#ifdef _WITH_DEBUG_FRAME_HIERARCHY
					TCHAR pstrDebug[256] = { 0 };
					_stprintf_s(pstrDebug, 256, _T("(Frame: %p) (Parent: %p)\n"), pChild, gameObject);
					OutputDebugString(pstrDebug);
#endif
				}
			}
		}
		else if (!strcmp(token, "</Frame>"))
		{
			gameObject->framesNum = frameCounter;
			gameObject->maximumMaterialsNum = materialCounter;
			break;
		}
	}
	return(gameObject);
}


void GameObject::PrintFrameInfo(GameObject* gameObject, GameObject* parent)
{
	TCHAR strDebug[256] = { 0 };

	_stprintf_s(strDebug, 256, _T("(Frame: %p) (Parent: %p)\n"), gameObject, parent);
	OutputDebugString(strDebug);

	if (gameObject->sibling) GameObject::PrintFrameInfo(gameObject->sibling, parent);
	if (gameObject->child) GameObject::PrintFrameInfo(gameObject->child, gameObject);
}

GameObject* GameObject::LoadGeometryFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, char* fileName, char* folderName)
{
	TEXTURENAME textureMapName;
	int textureMapIdx[5] {};
	FILE *inFile = NULL;
	::fopen_s(&inFile, fileName, "rb");
	::rewind(inFile);
	int frameCounter = 0, materialCounter = 0;
	GameObject *gameObject = GameObject::LoadFrameHierarchyFromFile(device, commandList, NULL, inFile, folderName, textureMapName, textureMapIdx, frameCounter, materialCounter, NULL, NULL, NULL);
	gameObject->textureName = textureMapName;

	//::fclose(inFile);

#ifdef _WITH_DEBUG_FRAME_HIERARCHY
	TCHAR pstrDebug[256] = { 0 };
	_stprintf_s(pstrDebug, 256, _T("Frame Hierarchy\n"));
	OutputDebugString(pstrDebug);

	CGameObject::PrintFrameInfo(pGameObject, NULL);
#endif

	return(gameObject);
}

LoadedModelInfo* GameObject::LoadGeometryAndAnimationFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, char* fileName, char* folderName)
{
	TEXTURENAME textureMapName;
	int textureMapIdx[5]{};

	char token[64] = { '\0' };
	LoadedModelInfo *modelInfo = new LoadedModelInfo();

	FILE *inFile = NULL;
	::fopen_s(&inFile, fileName, "rb");
	if (inFile == NULL)
	{
		std::cout << "빈파일!" << std::endl;
		return modelInfo;
	}
	::rewind(inFile);
	int frameCounter = 0, materialCounter = 0;

	for (; ; )
	{
		if (::ReadStringFromFile(inFile, token))
		{
			if (!strcmp(token, "<Hierarchy>:"))
			{
				modelInfo->modelRootObject = GameObject::LoadFrameHierarchyFromFile(device, commandList, NULL, inFile, folderName, textureMapName, textureMapIdx, frameCounter, materialCounter, &modelInfo->skinnedMeshNum, &modelInfo->standardMeshNum, &modelInfo->boundingMeshNum);
				::ReadStringFromFile(inFile, token); //"</Hierarchy>"
				modelInfo->modelRootObject->textureName = textureMapName;
			}
			else if (!strcmp(token, "<Animation>:"))
			{
				GameObject::LoadAnimationFromFile(inFile, modelInfo);
				modelInfo->PrepareSkinning();
			}
			else if (!strcmp(token, "</Animation>:"))
			{
				break;
			}
		}
		else
		{
			break;
		}
	}
	::fclose(inFile);

#ifdef _WITH_DEBUG_FRAME_HIERARCHY
	TCHAR pstrDebug[256] = { 0 };
	_stprintf_s(pstrDebug, 256, _T("Frame Hierarchy\n"));
	OutputDebugString(pstrDebug);

	CGameObject::PrintFrameInfo(pGameObject, NULL);
#endif

	return(modelInfo);
}



void GameObject::OnPrepareRender()
{
}

void GameObject::Render(ID3D12GraphicsCommandList* commandList, int idx)
{
	OnPrepareRender();

	if (materialsNum > 0)
	{
		for (int j = 0; j < materialsNum; ++j)
		{
			if (materials)
			{
				if (materials[j]->shader)
					materials[j]->shader->OnPrepareRender(commandList, idx, false);
				materials[j]->UpdateShaderVariables(commandList);
				// GameObject Material
			}

			if (meshes)
			{
				for (int i = 0; i < meshesNum; i++)
				{
					if (meshes[i])
					{
						if (shaderType != ShaderTypeSkinned)
							UpdateShaderVariables(commandList, idx);
						//GameObject World Matrix
						meshes[i]->Render(commandList, j);
					}
				}
			}
		}
	}

	if (sibling) sibling->Render(commandList, idx);
	if (child) child->Render(commandList, idx);
}

// 계층모델 인스턴싱
void GameObject::Render(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, bool isShadow)
{
	static int counter = 0;

	if (materialsNum > 0)
	{
		for (int j = 0; j < materialsNum; ++j)
		{
			if (materials)
			{
				materials[j]->UpdateShaderVariables(commandList);
				// GameObject Material
			}

			if (meshes)
			{
				for (int i = 0; i < meshesNum; i++)
				{
					if (meshes[i])
						meshes[i]->Render(commandList, j, instancesNum, instancingGameObjectBufferView[counter]);

				}
			}
		}
	}
	counter++;

	if (sibling) sibling->Render(commandList, instancesNum, instancingGameObjectBufferView, isShadow);
	if (child) child->Render(commandList, instancesNum, instancingGameObjectBufferView, isShadow);

	if (parent == NULL)
	{
		counter = 0;
	}
}

// 애니메이션 단일객체 렌더링
void GameObject::Render(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, ID3D12Resource** buffers[4], bool isShadow)
{
	static int counter = 0;
	static int skinnedMeshCount = 0;
	
	if (materialsNum > 0)
	{
		for (int j = 0; j < materialsNum; ++j)
		{
			if (materials)
			{
				if (materials[j]->shader)
				{
					if(shaderType == ShaderTypeSkinned)
						materials[j]->shader->OnPrepareRender(commandList, 1, isShadow);
					else
						materials[j]->shader->OnPrepareRender(commandList, 0, isShadow);
				}
				materials[j]->UpdateShaderVariables(commandList);
				// GameObject Material
			}

			if (meshes)
			{
 				for (int i = 0; i < meshesNum; i++)
				{
					if (shaderType == ShaderTypeSkinned)
					{
						ID3D12Resource* buffer[4] = { buffers[0][skinnedMeshCount], buffers[1][skinnedMeshCount], buffers[2][skinnedMeshCount], buffers[3][skinnedMeshCount] };

						if (meshes[i])
							meshes[i]->Render(commandList, j, buffer);
					}
					else
					{
						if (meshes[i])
							meshes[i]->Render(commandList, j, instancesNum, instancingGameObjectBufferView[counter]);
					}
				}
			}
		}
	}

	if(shaderType == ShaderTypeSkinned)
		skinnedMeshCount++;

	if (shaderType == ShaderTypeStandard)
		counter++;

	if (sibling) sibling->Render(commandList, instancesNum, instancingGameObjectBufferView, buffers, isShadow);
	if (child) child->Render(commandList, instancesNum, instancingGameObjectBufferView, buffers, isShadow);

	if (parent == NULL)
	{
		counter = 0;
		skinnedMeshCount = 0;
	}
}


// 무기 하나 들기 예시
void GameObject::Render(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, ID3D12Resource** buffers[4], bool isShadow, int weapon0, int weapon1)
{
	static int counter = 0;
	static int skinnedMeshCount = 0;

	if (materialsNum > 0)
	{
		for (int j = 0; j < materialsNum; ++j)
		{
			if (materials)
			{
				if (materials[j]->shader)
				{
					if (shaderType == ShaderTypeSkinned)
						materials[j]->shader->OnPrepareRender(commandList, 1, isShadow);
					else
						materials[j]->shader->OnPrepareRender(commandList, 0, isShadow);
				}
				materials[j]->UpdateShaderVariables(commandList);
				// GameObject Material
			}

			if (meshes)
			{
				for (int i = 0; i < meshesNum; i++)
				{
					if (shaderType == ShaderTypeSkinned)
					{
						ID3D12Resource* buffer[4] = { buffers[0][skinnedMeshCount], buffers[1][skinnedMeshCount], buffers[2][skinnedMeshCount], buffers[3][skinnedMeshCount] };

						if (meshes[i])
							meshes[i]->Render(commandList, j, buffer);
					}
					else
					{
						std::vector<D3D12_VERTEX_BUFFER_VIEW> bufferViews;

						bufferViews.emplace_back(instancingGameObjectBufferView[counter]);
						if (meshes[i])
						{
							meshes[i]->Render(commandList, j, instancesNum, instancingGameObjectBufferView[counter], weapon0);
							if (weapon1 != -1)
								meshes[i]->Render(commandList, j, instancesNum, instancingGameObjectBufferView[counter], weapon1);
						}
					}
				}
			}
		}
	}

	if (shaderType == ShaderTypeSkinned)
		skinnedMeshCount++;

	if (shaderType == ShaderTypeStandard)
		counter++;

	if (sibling) sibling->Render(commandList, instancesNum, instancingGameObjectBufferView, buffers, isShadow, weapon0, weapon1);
	if (child) child->Render(commandList, instancesNum, instancingGameObjectBufferView, buffers, isShadow, weapon0, weapon1);

	if (parent == NULL)
	{
		counter = 0;
		skinnedMeshCount = 0;
	}
}

// 애니메이션 인스턴싱
void GameObject::Render(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingStandardBufferView, D3D12_VERTEX_BUFFER_VIEW instancingObjIdxBufferView, ID3D12Resource** buffers[4], bool isShadow)
{
	static int counter = 0;
	static int skinnedMeshCount = 0;

	OnPrepareRender();

	if (materialsNum > 0)
	{
		for (int j = 0; j < materialsNum; ++j)
		{
			if (materials)
			{
				if (materials[j]->shader)
					materials[j]->shader->OnPrepareRender(commandList, 0, isShadow);
				materials[j]->UpdateShaderVariables(commandList);
				// GameObject Material
			}

			if (meshes)
			{
				for (int i = 0; i < meshesNum; i++)
				{
					if (meshes[i])
					{

						if (shaderType != ShaderTypeSkinned)
						{
							meshes[i]->Render(commandList, j, instancesNum, instancingStandardBufferView[counter]);
						}
						else
						{
							ID3D12Resource* buffer[4] = { buffers[0][skinnedMeshCount], buffers[1][skinnedMeshCount], buffers[2][skinnedMeshCount], buffers[3][skinnedMeshCount] };
							meshes[i]->Render(commandList, j, instancesNum, instancingObjIdxBufferView, buffer);
						}

					}
				}
			}
		}
	}
	if (shaderType == ShaderTypeSkinned)
		skinnedMeshCount++;

	if (shaderType == ShaderTypeStandard)
		counter++;

	if (sibling) sibling->Render(commandList, instancesNum, instancingStandardBufferView, instancingObjIdxBufferView, buffers, isShadow);
	if (child) child->Render(commandList, instancesNum, instancingStandardBufferView, instancingObjIdxBufferView, buffers, isShadow);

	if (parent == NULL)
	{
		counter = 0;
		skinnedMeshCount = 0;
	}
}

void GameObject::BillboardRender(ID3D12GraphicsCommandList* commandList, Camera* camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferView)
{
	OnPrepareRender();

	commandList->IASetVertexBuffers(0, 1, &instancingBufferView);
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
	commandList->DrawInstanced(1, instancesNum, 0, 0);
}

int GameObject::GetParentFrame(char name[])
{
	GameObject *p = GetParent();
	if (p == NULL) return 0;
	else if (strcmp("swat:Hips", name) == 0 || strcmp("Hips", name) == 0) return 0;
	else if (strcmp(frameName, name) == 0)	return 1;
	else if (strcmp(p->frameName, name) == 0) return 1;
	else p->GetParentFrame(name);
}

void GameObject::BoundingRender(ID3D12GraphicsCommandList * commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingGameObjectBufferView)
{
	OnPrepareRender();

	if (boundingObject != NULL) {
		if (boundingMesh) {
			boundingMesh->Render(commandList, 0, instancesNum, instancingGameObjectBufferView, -1);
		}
	}
}

void GameObject::SkinnedBoundingRender(ID3D12GraphicsCommandList * commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, int weaponNum)
{
	static int counter = 0;

	OnPrepareRender();

	if (shaderType == ShaderTypeBounding)
	{
		boundingMesh->Render(commandList, 0, instancesNum, instancingGameObjectBufferView[counter], weaponNum);
		counter++;
	}

	if (sibling) sibling->SkinnedBoundingRender(commandList, instancesNum, instancingGameObjectBufferView, weaponNum);
	if (child) child->SkinnedBoundingRender(commandList, instancesNum, instancingGameObjectBufferView, weaponNum);

	if (parent == NULL)
		counter = 0;
}

RotatingObject::RotatingObject(int meshesNum)
{
	this->meshesNum = meshesNum;
	rotationAxis = XMFLOAT3(0.0f, 1.0f, 0.0f);
	rotationSpeed = 90.0f;
}

RotatingObject::~RotatingObject()
{
}

void RotatingObject::Animate(float timeElapsed, XMFLOAT4X4* parent)
{
	GameObject::Rotate(&rotationAxis, rotationSpeed * timeElapsed);
}

void GameObject::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList *commandList)
{
	UINT cbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
	cbGameObject = ::CreateBufferResource(device, commandList, NULL, cbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

	cbGameObject->Map(0, NULL, (void **)&cbMappedGameObject);
}
void GameObject::ReleaseShaderVariables()
{
	if (cbGameObject)
	{
		cbGameObject->Unmap(0, NULL);
		cbGameObject->Release();
	}
}

void GameObject::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList, int idx)
{
	// Player using ConstantBufferView
	if (type != ObjectTypePlayer)
	{
		if (parent == NULL)
			commandList->SetGraphicsRootDescriptorTable(GraphicsRootGameobject, cbvGPUDescriptorHandle[0]);
		else
			commandList->SetGraphicsRootDescriptorTable(GraphicsRootGameobject, cbvGPUDescriptorHandle[idx]);
	}
}

void GameObject::SetVelocity(XMFLOAT3 velocity)
{
	this->velocity = velocity;
}

void GameObject::SetPosition(float x, float y, float z)
{
	transform._41 = x;
	transform._42 = y;
	transform._43 = z;

	UpdateTransform(NULL);
}
void GameObject::SetPosition(XMFLOAT3 position)
{
	SetPosition(position.x, position.y, position.z);
}
void GameObject::SetScale(float x, float y, float z)
{
	XMMATRIX scale = XMMatrixScaling(x, y, z);
	transform = Matrix4x4::Multiply(scale, transform);

	UpdateTransform(NULL);
}
void GameObject::SetWorld(XMFLOAT4X4 world)
{
	this->world = world;
}
void GameObject::SetTransform(XMFLOAT4X4 transform)
{
	this->transform = transform;
}
void GameObject::SetLook(XMFLOAT3 look)
{
	transform._31 = look.x;
	transform._32 = look.y;
	transform._33 = look.z;

	XMFLOAT3 up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	XMFLOAT3 right = Vector3::CrossProduct(up, look);

	transform._21 = up.x;
	transform._22 = up.y;
	transform._23 = up.z;

	transform._11 = right.x;
	transform._12 = right.y;
	transform._13 = right.z;

	UpdateTransform(NULL);
}


void GameObject::SetRight(XMFLOAT3 right)
{
	transform._11 = right.x;
	transform._12 = right.y;
	transform._13 = right.z;

	UpdateTransform(NULL);
}
void GameObject::SetUp(XMFLOAT3 up)
{
	transform._21 = up.x;
	transform._22 = up.y;
	transform._23 = up.z;

	UpdateTransform(NULL);
}
void GameObject::SetAllScale(float x, float y, float z)
{
	allScale.x = x;
	allScale.y = y;
	allScale.z = z;
}
void GameObject::SettPos(XMFLOAT3 position)
{
	tPos = position;
}
void GameObject::SetVel(XMFLOAT3 vel)
{
	velocity = vel;
}
void GameObject::Sethp(int hp)
{
	this->hp = hp;

}
void GameObject::Setid(int id)
{
	this->id = id;
}
void GameObject::SetDir(int dir)
{
	direction = dir;
}
void GameObject::SetTime(std::chrono::high_resolution_clock::time_point time)
{
	ctime = time;
}
void GameObject::ChangePlayerAnimation(int status1, int status2, int character)
{
	if (skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockanimation) return;

	if (upAnimationStatus != status1 && loAnimationStatus != status2)
	{
		skinnedAnimationController->SetTrackAnimationSet(0, status1, status2);
		upAnimationStatus = status1;
		loAnimationStatus = status2;
	}
	else if (upAnimationStatus != status1)
	{
		skinnedAnimationController->SetTrackAnimationSet(0, status1, loAnimationStatus);
		upAnimationStatus = status1;
	}
	else if (loAnimationStatus != status2)
	{
		skinnedAnimationController->SetTrackAnimationSet(0, upAnimationStatus, status2);
		loAnimationStatus = status2;
	}

	if (skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation == false)
	{
		if (character == PlayerJobBow)
		{
			if (status1 == BowCharacterPunch || status1 == BowCharacterKick)
			{
				skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockanimation = true;
				skinnedAnimationController->animationTracks[0].position = 0.0f;
			}
			else if (status1 == BowCharacterDrawArrow)
			{
				skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation = true;
				skinnedAnimationController->animationTracks[0].position = 0.0f;
			}
			else if (status1 == BowCharacterRelease)
			{
				skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation = true;
				skinnedAnimationController->animationTracks[0].position = 0.0f;
			}
		}
		else if (character == PlayerJobSword)
		{
			if (status1 == SwordCharacterAttack || status1 == SwordCharacterAttack2 || status1 == SwordCharacterAttack3)
			{
				skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockanimation = true;
				skinnedAnimationController->animationTracks[0].position = 0.0f;
			}
		}
		else if (character == PlayerJobRifle)
		{
			if (status1 == MainCharacterReload || status1 == MainCharacterWalkingReload)
			{
				skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation = true;
				skinnedAnimationController->animationTracks[0].position = 0.0f;
			}
		}
	}
}
XMFLOAT3 GameObject::GetVelocity()
{
	return velocity;
}
XMFLOAT3 GameObject::GetPosition()
{
	return(XMFLOAT3(world._41, world._42, world._43));
}
XMFLOAT3 GameObject::GetTransformPosition()
{
	return(XMFLOAT3(transform._41, transform._42, transform._43));
}
//게임 객체의 로컬 z-축 벡터를 반환한다.
XMFLOAT3 GameObject::GetLook()
{
	return(Vector3::Normalize(XMFLOAT3(world._31, world._32, world._33)));
}
//게임 객체의 로컬 y-축 벡터를 반환한다.
XMFLOAT3 GameObject::GetUp()
{
	return(Vector3::Normalize(XMFLOAT3(world._21, world._22, world._23)));
}
//게임 객체의 로컬 x-축 벡터를 반환한다.
XMFLOAT3 GameObject::GetRight()
{
	return(Vector3::Normalize(XMFLOAT3(world._11, world._12, world._13)));
}

XMFLOAT3 & GameObject::GetLookRefernce()
{
	return XMFLOAT3(world._31, world._32, world._33);
}



XMFLOAT4X4 GameObject::GetWorld()
{
	return XMFLOAT4X4(world);
}

XMFLOAT4X4 GameObject::GetTransform()
{
	return transform;
}

Material* GameObject::GetMaterial(int idx)
{
	return materials[idx];
}

XMFLOAT3 GameObject::GetAllScale()
{
	return XMFLOAT3(allScale.x, allScale.y, allScale.z);
}

XMFLOAT3 & GameObject::GetInputAngle()
{
	return inputangle;
}

//게임 객체를 로컬 x-축 방향으로 이동한다.
void GameObject::MoveStrafe(float distance)
{
	XMFLOAT3 position = GetPosition();
	XMFLOAT3 right = GetRight();
	position = Vector3::Add(position, right, distance);
	GameObject::SetPosition(position);
}
//게임 객체를 로컬 y-축 방향으로 이동한다.
void GameObject::MoveUp(float distance)
{
	XMFLOAT3 position = GetPosition();
	XMFLOAT3 up = GetUp();
	position = Vector3::Add(position, up, distance);
	GameObject::SetPosition(position);
}
//게임 객체를 로컬 z-축 방향으로 이동한다.
void GameObject::MoveForward(float distance)
{
	XMFLOAT3 position = GetPosition();
	XMFLOAT3 look = GetLook();
	position = Vector3::Add(position, look, distance);
	GameObject::SetPosition(position);
}
XMFLOAT3 GameObject::GetFinalVec(GameObject* ob, float force, XMFLOAT3 dir , float elapsedTime)
{
	XMFLOAT3 vForce = XMFLOAT3(0, 0, 0);
	XMFLOAT3 obLook = ob->GetLook();
	XMFLOAT3 obRight = ob->GetRight();
	XMFLOAT3 obUp = ob->GetUp();

	if (direction == DirectionForword)
		vForce = Vector3::Add(vForce, dir, force);
	if (direction == DirectionBackword)
		vForce = Vector3::Add(vForce, obLook, -force);
	//화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.

	if (direction == DirectionRight) vForce = Vector3::Add(vForce, obRight, force);
	if (direction == DirectionLeft) vForce = Vector3::Add(vForce, obRight, -force);
	//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.

	XMFLOAT3 opRight = XMFLOAT3(-obRight.x, -obRight.y, -obRight.z);
	XMFLOAT3 lrVec = Vector3::Normalize(Vector3::Add(obLook, obRight));
	XMFLOAT3 mlrVec = Vector3::Normalize(Vector3::Add(obLook, opRight));

	//대각선 이동 
	if (direction == DirectionFR) vForce = Vector3::Add(vForce, lrVec, force);
	if (direction == DirectionFL) vForce = Vector3::Add(vForce, mlrVec, force);
	if (direction == DirectionBR) vForce = Vector3::Add(vForce, mlrVec, -force);
	if (direction == DirectionBL) vForce = Vector3::Add(vForce, lrVec, -force);


	if (direction == DirectionUp) vForce = Vector3::Add(vForce, obUp, force);
	if (direction == DirectionDown) vForce = Vector3::Add(vForce, obUp, -force);
	//‘Page Up’을 누르면 로컬 y-축 방향으로 이동한다. ‘Page Down’을 누르면 반대 방향으로 이동한다.


	acc = XMFLOAT3(vForce.x / mass, 0, vForce.z / mass); // 가속도 
	float vlength = Vector3::Length(velocity);


	if (vlength < maxVelocityXZ)
		velocity = Vector3::Add(velocity, XMFLOAT3((acc.x * elapsedTime), 0, (acc.z * elapsedTime)));

	XMFLOAT3 fv = Vector3::Add(XMFLOAT3(velocity.x * elapsedTime, 0.0f, velocity.z * elapsedTime), XMFLOAT3(acc.x * elapsedTime * elapsedTime * 0.5f, 0.0f, acc.z * elapsedTime * elapsedTime * 0.5f));

	return fv;

}


void GameObject::Move(int direction, float force, bool updateVelocity , float elapsedTime , std::vector<GameObject*>& tZombies)
{
	if (direction != 0)
	{

		XMFLOAT3 vForce = XMFLOAT3(0, 0, 0);
		XMFLOAT3 obLook = GetLook();
		XMFLOAT3 obRight = GetRight();
		XMFLOAT3 obUp = GetUp();

		finalvec = XMFLOAT3(0, 0, 0);

		for(auto& a : tZombies) {
			if (a->id == id)
				continue;
			if (Vector3::Length(Vector3::Subtract(GetPosition(), a->GetPosition())) < 2.0f) {
				XMFLOAT3 ozDir = Vector3::Subtract(GetPosition(), a->GetPosition()); // 내가 밀려날 방향 
				finalvec = Vector3::Add(finalvec, GetFinalVec(this, ZOMBIEBOUNCING ,ozDir , elapsedTime));
			}

		}
		
		finalvec = Vector3::Add(finalvec, GetFinalVec(this, force, obLook, elapsedTime));


		SetColPos(Vector3::Add(GetPosition(), finalvec));
		
		colObbBox = obbvec[0];
		colObbBox.Center.x = colPos.x;
		colObbBox.Center.z = colPos.z;

		colObbBox.Extents = Vector3::ScalarProduct(colObbBox.Extents, 2.0f, false);

		
		//플레이어를 현재 위치 벡터에서 Shift 벡터만큼 이동한다.
		float length = sqrtf(velocity.x * velocity.x + velocity.z * velocity.z);

		if (direction == DirectionNoMove)
		{
			length = Vector3::Length(velocity);
			float deceleration = (friction * elapsedTime);
			if (deceleration > length) deceleration = length;
			velocity = Vector3::Add(velocity, Vector3::ScalarProduct(velocity, -deceleration, false));
		}
		else {
			length = Vector3::Length(velocity);
			float deceleration = ((friction - 19.0f) * elapsedTime);
			if (deceleration > length) deceleration = length;
			velocity = Vector3::Add(velocity, Vector3::ScalarProduct(velocity, -deceleration, false));
		}
	}

}

void GameObject::RealMove()
{
	SetPosition(Vector3::Add(GetPosition(), finalvec));
	colPos = GetPosition();

}

//게임 객체를 주어진 각도로 회전한다.
void GameObject::Rotate(float pitch, float yaw, float roll)
{
	XMMATRIX rotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(pitch), XMConvertToRadians(yaw), XMConvertToRadians(roll));
	transform = Matrix4x4::Multiply(rotate, transform);

	UpdateTransform(NULL);
}

void GameObject::Rotate(XMFLOAT3* axis, float angle)
{
	XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(axis), XMConvertToRadians(angle));
	transform = Matrix4x4::Multiply(rotate, transform);

	UpdateTransform(NULL);
}

void GameObject::Rotate(XMFLOAT4 *quaternion)
{
	XMMATRIX rotate = XMMatrixRotationQuaternion(XMLoadFloat4(quaternion));
	transform = Matrix4x4::Multiply(rotate, transform);

	UpdateTransform(NULL);
}

HeightMapTerrain::HeightMapTerrain(ID3D12Device *device, ID3D12GraphicsCommandList* commandList, LPCTSTR fileName,  int width, int length, int blockWidth, int blockLength, XMFLOAT3 scale, XMFLOAT4 color) : GameObject(0)
{
	this->width = width;
	this->length = length;

	int cxQuadsPerBlock = blockWidth - 1;
	int czQuadsPerBlock = blockLength - 1;

	this->scale = scale;

	heightMapImage = new HeightMapImage(fileName, width, length, scale);

	long cxBlocks = (this->width - 1) / cxQuadsPerBlock;
	long czBlocks = (this->length - 1) / czQuadsPerBlock;

	meshesNum = cxBlocks * czBlocks;

	meshes = new Mesh*[meshesNum];

	for (int i = 0; i < meshesNum; i++) 
		meshes[i] = NULL;
	HeightMapGridMesh *heightMapGridMesh = NULL;
	for (int z = 0, zStart = 0; z < czBlocks; z++)
	{
		for (int x = 0, xStart = 0; x < cxBlocks; x++)
		{
			xStart = x * (blockWidth - 1);
			zStart = z * (blockLength - 1);
			heightMapGridMesh = new HeightMapGridMesh(device, commandList, xStart,
				zStart, blockWidth, blockLength, scale, color, heightMapImage);
			SetMesh(x + (z*cxBlocks), heightMapGridMesh);
		}
	}
}
float HeightMapTerrain::GetTerrainHeight(float x, float y)
{
	//TriangleTests::Intersects()
	return 1.0f;
}
void HeightMapTerrain::SetBoundingTransform()
{
	HeightMapGridMesh* terrainMesh = (HeightMapGridMesh*)GetMesh(0);
	// UINT : 0 ~ 10 : ShaderType, 10~ BoundingIndex
	for (int i = 0; i < TERRAINBOUNDINGS; ++i)
	{
		terrainMesh->GetBoundings()[i].Transform(terrainMesh->GetBoundings()[i], XMLoadFloat4x4(&GetWorld()));
	}
}
HeightMapTerrain::~HeightMapTerrain(void)
{
	if (heightMapImage) delete heightMapImage;
}

SuperCobraObject::SuperCobraObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature)
{
}

SuperCobraObject::~SuperCobraObject()
{
}

void SuperCobraObject::PrepareAnimate()
{
	mainRotorFrame = FindFrame("MainRotor");
	tailRotorFrame = FindFrame("TailRotor");
}

void SuperCobraObject::Animate(float timeElapsed, XMFLOAT4X4 *parent)
{
	if (mainRotorFrame)
	{
		XMMATRIX rotate = XMMatrixRotationY(XMConvertToRadians(360.0f * 4.0f) * timeElapsed);
		mainRotorFrame->SetTransform(Matrix4x4::Multiply(rotate, mainRotorFrame->GetTransform()));
	}
	if (tailRotorFrame)
	{
		XMMATRIX rotate = XMMatrixRotationX(XMConvertToRadians(360.0f * 4.0f) * timeElapsed);
		tailRotorFrame->SetTransform(Matrix4x4::Multiply(rotate, tailRotorFrame->GetTransform()));
	}

}

TreeObject::TreeObject()
{
}
TreeObject::~TreeObject()
{
}
bool TreeObject::IsFar(Camera* camera)
{
	float length = Vector3::Length(Vector3::Subtract(GetPosition(), camera->GetPosition()));
	if (length >= LOD_VALUE)
	{
		return true;
	}
	return false;
}

BillboardObject::BillboardObject()
{
}

BillboardObject::~BillboardObject()
{
}

void BillboardObject::UpdateTransform(VS_VB_INSTANCE_BILLBOARD * vbMappedBillboardObjects, int idx)
{
	vbMappedBillboardObjects[idx].position = billboardVertex.position;
	vbMappedBillboardObjects[idx].instanceInfo = billboardVertex.billboardInfo;
}

bool BillboardObject::IsFar(Camera* camera)
{
	float length = Vector3::Length(Vector3::Subtract(billboardVertex.position, camera->GetPosition()));
	if (length < LOD_VALUE)
	{
		return false;
	}
	return true;
}

AnimationObject::AnimationObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, LoadedModelInfo *model, int animationTracks, bool isDummy)
{
	LoadedModelInfo *pAngrybotModel = model;

	SetChild(pAngrybotModel->modelRootObject, isDummy);
	skinnedAnimationController = new AnimationController(device, commandList, animationTracks, pAngrybotModel);
}
AnimationObject::AnimationObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature)
{
}

AnimationObject::~AnimationObject()
{
}


void AnimationObject::UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, XMFLOAT4X4 *xmf4x4World)
{
}