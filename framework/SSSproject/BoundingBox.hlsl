#include "Shaders.hlsli"

struct VS_BOUNDING_INPUT
{
    float3 position : POSITION;
    float4x4 transform : WORLDMATRIX;
    
};
struct VS_BOUNDING_OUTPUT
{
    float4 position : SV_POSITION;
};

VS_BOUNDING_OUTPUT VS_Bounding(VS_BOUNDING_INPUT input)
{
    VS_BOUNDING_OUTPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), input.transform), gmtxView), gmtxProjection);
    return (output);
}
float4 PS_Bounding(VS_BOUNDING_OUTPUT input) : SV_TARGET
{    
    return float4(0.0f, 1.0f, 0.0f, 1.0f);
}