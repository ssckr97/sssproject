#include "stdafx.h"
#include "InstanceShader.h"
#include "Material.h"
#include "Scene.h"

InstancingShader::InstancingShader()
{
}
InstancingShader::~InstancingShader()
{
}

// 0 : Default 1 : Blending 2: Billboard(GS)
void InstancingShader::CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = InstanceShaderPS_Num;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	shadowPipelineStateNum = 3;
	shadowPipelineStates = new ID3D12PipelineState*[shadowPipelineStateNum];

	Shader::CreateShader(device, graphicsRootSignature);
	auto boundingPipeLineStateDesc = pipelineStateDesc;

	pipelineStateDesc.BlendState.AlphaToCoverageEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].BlendEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;

	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[1]);
	// 블렌딩 오브젝트.

	pipelineStateDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;
	hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[2]);

	UINT inputElementDescsNum = 5;
	D3D12_INPUT_ELEMENT_DESC *BoundinginputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	BoundinginputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	BoundinginputElementDescs[1] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[2] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[3] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[4] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

	boundingPipeLineStateDesc.InputLayout.NumElements = inputElementDescsNum;
	boundingPipeLineStateDesc.InputLayout.pInputElementDescs = BoundinginputElementDescs;

	boundingPipeLineStateDesc.RasterizerState.FillMode = D3D12_FILL_MODE_WIREFRAME;
	boundingPipeLineStateDesc.VS = Shader::CompileShaderFromFile(L"BoundingBox.hlsl", "VS_Bounding", "vs_5_1", &vertexShaderBlob);
	boundingPipeLineStateDesc.PS = Shader::CompileShaderFromFile(L"BoundingBox.hlsl", "PS_Bounding", "ps_5_1", &pixelShaderBlob);

	hResult = device->CreateGraphicsPipelineState(&boundingPipeLineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[3]);


	Shader::CreateShadowShader(device, graphicsRootSignature);

	pipelineStateDesc.PS = CreateShadowPixelShader();
	pipelineStateDesc.BlendState.AlphaToCoverageEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].BlendEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;

	hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&shadowPipelineStates[1]);

	pipelineStateDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;
	hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&shadowPipelineStates[2]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();
	if (geometryShaderBlob) geometryShaderBlob->Release();
	
	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
	if (boundingPipeLineStateDesc.InputLayout.pInputElementDescs) delete[] boundingPipeLineStateDesc.InputLayout.pInputElementDescs;
}

void InstancingShader::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx)
{

	UINT tempObjectByte = 0;
	ID3D12Resource*** tempObject = NULL;
	VS_VB_INSTANCE_OBJECT*** tempMappedObject = NULL;
	D3D12_VERTEX_BUFFER_VIEW** tempObjectBufferView = NULL;

	tempObjectByte = sizeof(VS_VB_INSTANCE_OBJECT);

	tempObject = new ID3D12Resource**[CAMERAS_NUM];
	tempMappedObject = new VS_VB_INSTANCE_OBJECT**[CAMERAS_NUM];
	tempObjectBufferView = new D3D12_VERTEX_BUFFER_VIEW*[CAMERAS_NUM];

	for (int i = 0; i < CAMERAS_NUM; ++i)
	{
		tempObject[i] = new ID3D12Resource*[instanceBuffer[idx]->framesNum];
		tempMappedObject[i] = new VS_VB_INSTANCE_OBJECT*[instanceBuffer[idx]->framesNum];
		tempObjectBufferView[i] = new D3D12_VERTEX_BUFFER_VIEW[instanceBuffer[idx]->framesNum];
	}

	for (int j = 0; j < CAMERAS_NUM; ++j) {
		for (int i = 0; i < instanceBuffer[idx]->framesNum; ++i)
		{
			tempObject[j][i] = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTANCE_OBJECT) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
			//인스턴스 정보를 저장할 정점 버퍼를 업로드 힙 유형으로 생성한다.

			tempObject[j][i]->Map(0, NULL, (void **)&tempMappedObject[j][i]);
			//정점 버퍼(업로드 힙)에 대한 포인터를 저장한다.

			tempObjectBufferView[j][i].BufferLocation = tempObject[j][i]->GetGPUVirtualAddress();
			tempObjectBufferView[j][i].StrideInBytes = tempObjectByte;
			tempObjectBufferView[j][i].SizeInBytes = tempObjectByte * ((UINT)BUFFERSIZE);
			//정점 버퍼에 대한 뷰를 생성한다.
		}
	}
	instanceBuffer[idx]->vbGameObjectBytes = tempObjectByte;

	instanceBuffer[idx]->vbGameObjects = tempObject;
	instanceBuffer[idx]->vbMappedGameObjects = tempMappedObject;
	instanceBuffer[idx]->instancingGameObjectBufferView = tempObjectBufferView;

	//바운딩 정점 버퍼 생성 
	if (instanceBuffer[idx]->objects[0]->GetBoundingObject() != NULL) {
		for (int i = 0; i < instanceBuffer[idx]->objects[0]->GetBoundingObject()->size(); ++i) {
			//boundingBuffer 
			UINT tempBoundingByte = 0;
			ID3D12Resource* tempBounding = NULL;
			VS_VB_INSTANCE_BOUNDING* tempMappedBounding = NULL;
			D3D12_VERTEX_BUFFER_VIEW tempBoundingBufferView;

			tempBoundingByte = sizeof(VS_VB_INSTANCE_BOUNDING);

			tempBounding = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTANCE_BOUNDING) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

			tempBounding->Map(0, NULL, (void **)&tempMappedBounding);

			tempBoundingBufferView.BufferLocation = tempBounding->GetGPUVirtualAddress();
			tempBoundingBufferView.StrideInBytes = tempBoundingByte;
			tempBoundingBufferView.SizeInBytes = tempBoundingByte * ((UINT)BUFFERSIZE);

			instanceBuffer[idx]->vbBoundingBytes = tempBoundingByte;
			instanceBuffer[idx]->vbBounding.emplace_back(tempBounding);
			instanceBuffer[idx]->vbMappedBounding.emplace_back(tempMappedBounding);
			instanceBuffer[idx]->instancingBoundingBufferView.emplace_back(tempBoundingBufferView);
		}
	}

	// Create Cashe	
	instanceBuffer[idx]->worldCashe = new VS_VB_INSTANCE_OBJECT*[instanceBuffer[idx]->framesNum];
	instanceBuffer[idx]->isVisable = new bool*[BUFFERSIZE];

	for (int i = 0; i < BUFFERSIZE; ++i)
	{
		instanceBuffer[idx]->isVisable[i] = new bool[CAMERAS_NUM];

		for (int j = 0; j < CAMERAS_NUM; ++j)
		{
			instanceBuffer[idx]->isVisable[i][j] = true;
		}
	}

	for (int i = 0; i < CAMERAS_NUM; ++i)
	{
		instanceBuffer[idx]->renderObjNum[i] = 0;
	}

	for (int i = 0; i < instanceBuffer[idx]->framesNum; ++i)
	{
		instanceBuffer[idx]->worldCashe[i] = new VS_VB_INSTANCE_OBJECT[BUFFERSIZE];
	}
}
void InstancingShader::UpdateShaderVariables(ID3D12GraphicsCommandList * commandList, UINT cameraIdx)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		int objNum = 0;
		for (int j = 0; j < instanceBuffer[k]->objects.size() - 1; ++j)
		{
			if (instanceBuffer[k]->isVisable[j][cameraIdx])
			{
				for (int i = 0; i < instanceBuffer[k]->framesNum; ++i)
				{
					instanceBuffer[k]->vbMappedGameObjects[cameraIdx][i][objNum] = instanceBuffer[k]->worldCashe[i][j];
				}
				objNum++;
			}
		}
		instanceBuffer[k]->renderObjNum[cameraIdx] = objNum;
	}
}
void InstancingShader::ReleaseShaderVariables()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->vbGameObjects)
		{
			for (int i = 0; i < CAMERAS_NUM; ++i) {
				for (int j = 0; j < instanceBuffer[k]->framesNum; ++j)
				{
					instanceBuffer[k]->vbGameObjects[i][j]->Unmap(0, NULL);
					instanceBuffer[k]->vbGameObjects[i][j]->Release();
				}
			}
			for (int i = 0; i < instanceBuffer[k]->vbBounding.size(); ++i) {
				instanceBuffer[k]->vbBounding[i]->Unmap(0, NULL);
				instanceBuffer[k]->vbBounding[i]->Release();
			}
		}
	}
}

void InstancingShader::SetWTShader(WeaponTrailerShader * wtShader)
{
	this->wtShader = wtShader;
}

void InstancingShader::BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature *graphicsRootSignature, std::vector<void*>& context, int hObjectSize)
{
	objects.reserve(100);

	HeightMapTerrain *terrain = (HeightMapTerrain *)context[0];
	objects.emplace_back(terrain);
	float terrainWidth = terrain->GetWidth(), terrainLength = terrain->GetLength();

	// Cube Object Build
	InstanceBuffer* cubeObject = new InstanceBuffer();
	cubeObject->objectsNum = 5 + 1;		// + 1 : dummy
	cubeObject->objects.reserve(cubeObject->objectsNum);
	cubeObject->framesNum = 1;
	cubeObject->pipeLineStateIdx = 1;
	cubeObject->shadowPipeLineStateIdx = 1;
	cubeObject->obb = BoundingOrientedBox(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(12.0f, 12.0f, 12.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	_TCHAR* diffuseTextureName = L"Assets/Image/Object/Rocks.dds";
	_TCHAR* normalTextureName = L"Assets/Image/Object/RocksNormal.dds";

	cubeObject->textureName.diffuse.emplace_back(0, diffuseTextureName);
	cubeObject->textureName.normal.emplace_back(0, normalTextureName);

	RotatingObject* rotatingObject = NULL;
	RotatingObject* dummyCube = new RotatingObject();
	dummyCube->CreateMaterials(1);
	Material* temp = new Material(1);
	temp->textureMask = MATERIAL_DIFFUSE_MAP + MATERIAL_NORMAL_MAP;
	temp->reflection = 0;
	temp->textureIdx = 0000;
	dummyCube->SetMaterial(0, temp);

	cubeObject->objects.emplace_back(dummyCube);
	// dummy object

	for (int i = 0; i < cubeObject->objectsNum - 1; ++i)
	{
		rotatingObject = new RotatingObject();
		float xPosition = (float)i * 30 + 100;
		float zPosition = 100;
		float fHeight = 0.0f; // terrain->GetHeight(xPosition, zPosition) + 6.0f;
		rotatingObject->SetPosition(xPosition, fHeight, zPosition);
		rotatingObject->SetRotationAxis(XMFLOAT3(0.0f, 1.0f, 0.0f));
		rotatingObject->SetRotationSpeed(10.0f*(i % 10));
		cubeObject->objects.emplace_back(rotatingObject);
	}
	//인스턴싱을 사용하여 렌더링하기 위하여 하나의 게임 객체만 메쉬를 가진다.
	CubeMeshNormalMapTextured *cubeMesh = new CubeMeshNormalMapTextured(device, commandList, 12.0f, 12.0f, 12.0f);
	cubeObject->objects[0]->SetMesh(0, cubeMesh);
	instanceBuffer.emplace_back(cubeObject);

	int iter = 1;
	for (int i = 0; i < hObjectSize -1 ; ++i) {
		InstanceBuffer* tempBuffer = new InstanceBuffer();
		GameObject* tempObjcet = (GameObject*)context[iter++];
		BuildInstanceObject(device, commandList, tempBuffer, 0, tempObjcet->GetPipeLineStatesNum(), tempObjcet, 1, XMFLOAT3{ 2.0f , 2.0f , 2.0f }, XMFLOAT3(0, 0, 0), XMFLOAT3(0 + i * 10, 0, 0));
	}

	LoadBoundingBoxInfo();

	for (int i = 0; i < instanceBuffer.size(); ++i)
	{
		CreateShaderVariables(device, commandList, i);
	}
	SetTextures(device, commandList);
}

void InstancingShader::BuildInstanceObject(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
	InstanceBuffer * instanceObject, int objectNum, int pipeLineStateIdx, GameObject * objectModel, int modelNumber, XMFLOAT3 boundingSize, XMFLOAT3 rotate, XMFLOAT3 position)
{
	objects.emplace_back(objectModel);
	instanceObject->useShadow = objectModel->useShadow;
	instanceObject->objectsNum = objectNum + 1; //dummy
	instanceObject->objects.reserve(instanceObject->objectsNum);
	instanceObject->framesNum = objectModel->GetFramesNum() + 1;
	instanceObject->pipeLineStateIdx = pipeLineStateIdx;
	instanceObject->shadowPipeLineStateIdx = pipeLineStateIdx;
	instanceObject->useBillboard = -1;
	instanceObject->textureName = objectModel->textureName;
	instanceObject->obb = BoundingOrientedBox(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(20.0f, 20.0f, 20.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));

	BoundingBoxMesh *tempBounding = new BoundingBoxMesh(device, commandList, boundingSize.x, boundingSize.y, boundingSize.z);

	GameObject* dummy = new GameObject();
	dummy->SetChild(objectModel);
	dummy->AddRef();
	dummy->SetBoundingMesh(tempBounding);
	dummy->allocateBvec();

	instanceObject->objects.emplace_back(dummy);

	for (int i = 0; i < instanceObject->objectsNum - 1; ++i)
	{
		GameObject* gameobject = new GameObject();
		gameobject->SetChild(objectModel);

		gameobject->SetPosition(position.x, position.y, position.z);
		gameobject->Rotate(rotate.x, rotate.y, rotate.z);
		gameobject->SetScale(1.0f, 1.0f, 1.0f);
		instanceObject->objects.emplace_back(gameobject);
	}
	instanceBuffer.emplace_back(instanceObject);
}


void InstancingShader::AnimateObjects(float timeElapsed, Camera* camera)
{
	elapsedTime = timeElapsed;
	for (int i = 1; i < instanceBuffer.back()->objects.size(); ++i)
	{
		GameObject* obj = instanceBuffer.back()->objects[i];

		obj->arrowAge += timeElapsed;
		if (obj->arrowAge > 10.0f)
		{
			obj->isLive = false;
			obj->isMove = false;
			auto p = instanceBuffer.back()->objects.begin();
			delete obj;
			instanceBuffer.back()->objects.erase(p + i);
		}
		else if (obj->isMove == true) 
		{
			float time = (1.0f - (obj->arrowAge / 10.0f));
			float theta = obj->arrowTheta * PI / 180.0f;
			float speed = obj->arrowSpeed;
			XMFLOAT3 beforePos = obj->GetTransformPosition();
			XMFLOAT3 velocity = Vector3::ScalarProduct(obj->GetVelocity(), elapsedTime * speed * cos(theta) * time, false);
			XMFLOAT3 gravity = XMFLOAT3(0.0f, -9.8 * elapsedTime * 0.5f * cos(theta), 0.0f);
			XMFLOAT3 afterPos = Vector3::Add(Vector3::Add(beforePos, velocity), gravity);
			obj->SetPosition(afterPos);
			obj->GetobbVector()[0].Center = afterPos;
			obj->Rotate(timeElapsed, 0.0f, 0.0f);

			if (obj->arrowAge - obj->arrowTrailCoolTime > 0.1f)
			{
				obj->prePos = obj->curPos;
				XMFLOAT3 pos0 = Vector3::Add(obj->GetPosition(), XMFLOAT3(-0.01f, -0.01f, -0.01f));
				XMFLOAT3 pos1 = Vector3::Add(obj->GetPosition(), XMFLOAT3(+0.01f, +0.01f, +0.01f));
				obj->curPos.start._41 = pos0.x; obj->curPos.start._42 = pos0.y; obj->curPos.start._43 = pos0.z;
				obj->curPos.end._41 = pos1.x; obj->curPos.end._42 = pos1.y; obj->curPos.end._43 = pos1.z;
				XMFLOAT3 prePos0 = XMFLOAT3(obj->prePos.end._41, obj->prePos.end._42, obj->prePos.end._43);
				XMFLOAT3 prePos1 = XMFLOAT3(obj->prePos.start._41, obj->prePos.start._42, obj->prePos.start._43);
				XMFLOAT3 curPos0 = XMFLOAT3(obj->curPos.end._41, obj->curPos.end._42, obj->curPos.end._43);
				XMFLOAT3 curPos1 = XMFLOAT3(obj->curPos.start._41, obj->curPos.start._42, obj->curPos.start._43);
				if (Vector3::Length(prePos0) != 0)
				{
					XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 0.75f);
					float age = 0.3f;
					wtShader->AddBuffer(prePos0, prePos1, curPos0, curPos1, color, age, 0);

				}
			}
		}
	}

}

void InstancingShader::Animate()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		for (int j = 1; j < instanceBuffer[k]->objects.size(); ++j)
		{
			if (instanceBuffer[k]->objects[j])
			{
				instanceBuffer[k]->objects[j]->Animate(elapsedTime);
				instanceBuffer[k]->objects[j]->UpdateTransform(instanceBuffer[k]->worldCashe, j - 1, NULL);
			}

			// 바운딩 박스 메쉬에 대한 월드변환행렬.
			if (showBounding == true) {
				for (int m = 0; m < instanceBuffer[k]->vbMappedBounding.size(); ++m) {
					if (instanceBuffer[k]->objects[0]->GetBoundingObject() != NULL) {
						auto world = (*instanceBuffer[k]->objects[0]->GetBoundingObject())[m]->GetWorld();
						instanceBuffer[k]->objects[j]->UpdateBoundingTransForm(instanceBuffer[k]->vbMappedBounding[m], j - 1, world, m);
					}
				}
			}
		}
	}
}

void InstancingShader::ReleaseObjects()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (!instanceBuffer[k]->objects.empty())
		{
			if (instanceBuffer[k]->objects[0]->GetBoundingObject()) {
				for (int j = 0; j < instanceBuffer[k]->objects[0]->GetBoundingObject()->size(); ++j) {
					instanceBuffer[k]->objects[0]->GetBoundingObject()[0][j]->Release();
				}
			}
		}
		instanceBuffer[k]->objects[0]->Release();

		for (int i = 0; i < SKINNED_BUFFERSIZE; ++i)
			delete[] instanceBuffer[k]->isVisable[i];
		delete[] instanceBuffer[k]->isVisable;
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->Release();
	}
}

void InstancingShader::ReleaseUploadBuffers()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->objects[0])
			instanceBuffer[k]->objects[0]->ReleaseUploadBuffers();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if(textures[i])
			textures[i]->ReleaseUploadBuffers();
	}
}

void InstancingShader::SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{

	std::vector<std::pair<int, std::wstring>> dtname;
	std::vector<std::pair<int, std::wstring>> nlname;
	std::vector<std::pair<int, std::wstring>> spname;
	std::vector<std::pair<int, std::wstring>> mtname;


	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		bool isEqual = false;
		int equalidx = -1;
		if (instanceBuffer[k]->textureName.diffuse.size() > 0)
		{
			//dtname.emplace_back(instanceBuffer[k]->textureName.diffuse);
			Texture* diffuseTexture = NULL;
			for (int i = 0; i < dtname.size(); ++i) {
				for (int j = 0; j < instanceBuffer[k]->textureName.diffuse.size(); ++j) {
					if (dtname[i].second.compare(instanceBuffer[k]->textureName.diffuse[j].second) == 0) {
						isEqual = true;
						equalidx = dtname[i].first;
						break;
					}
				}
				if (isEqual)
					break;
			}
			if (!isEqual) {
				diffuseTexture = new Texture((int)instanceBuffer[k]->textureName.diffuse.size(), ResourceTexture2D, 0);
				for (int i = 0; i < instanceBuffer[k]->textureName.diffuse.size(); ++i)
				{
					dtname.emplace_back(k, instanceBuffer[k]->textureName.diffuse[i].second);
					diffuseTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.diffuse[i].second, instanceBuffer[k]->textureName.diffuse[i].first);

				}

				instanceBuffer[k]->diffuseIdx = Scene::CreateShaderResourceViews(device, commandList, diffuseTexture, GraphicsRootDiffuseTextures);
			}
			else {
				instanceBuffer[k]->diffuseIdx = instanceBuffer[equalidx]->diffuseIdx;
			}

			textures.emplace_back(diffuseTexture);

		}
		if (instanceBuffer[k]->textureName.normal.size() > 0)
		{
			bool isEqual = true;
			int equalidx = -1;
			Texture* normalTexture = NULL;
			for (int i = 0; i < nlname.size(); ++i) {
				for (int j = 0; j < instanceBuffer[k]->textureName.normal.size(); ++j) {
					if (nlname[i].second.compare(instanceBuffer[k]->textureName.normal[j].second) == 0) {
						isEqual = false;
						equalidx = nlname[i].first;
						break;
					}
				}
				if (!isEqual)
					break;
			}
			if (isEqual) {
				Texture* normalTexture = new Texture((int)instanceBuffer[k]->textureName.normal.size(), ResourceTexture2D, 0);

				for (int i = 0; i < instanceBuffer[k]->textureName.normal.size(); ++i)
				{
					nlname.emplace_back(k, instanceBuffer[k]->textureName.normal[i].second);
					normalTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.normal[i].second, instanceBuffer[k]->textureName.normal[i].first);
				}
				instanceBuffer[k]->normalIdx = Scene::CreateShaderResourceViews(device, commandList, normalTexture, GraphicsRootNormalTextures);

			}
			else {
				instanceBuffer[k]->normalIdx = instanceBuffer[equalidx]->normalIdx;
			}
			textures.emplace_back(normalTexture);
		}
		if (instanceBuffer[k]->textureName.specular.size() > 0)
		{

			bool isEqual = true;
			int equalidx = -1;
			Texture* specularTexture = NULL;
			for (int i = 0; i < spname.size(); ++i) {
				for (int j = 0; j < instanceBuffer[k]->textureName.normal.size(); ++j) {
					if (spname[i].second.compare(instanceBuffer[k]->textureName.normal[j].second) == 0) {
						isEqual = false;
						equalidx = spname[i].first;
						break;
					}
				}
				if (!isEqual)
					break;
			}
			if (isEqual) {
				specularTexture = new Texture((int)instanceBuffer[k]->textureName.specular.size(), ResourceTexture2D, 0);
				for (int i = 0; i < instanceBuffer[k]->textureName.specular.size(); ++i)
				{
					spname.emplace_back(k, instanceBuffer[k]->textureName.specular[i].second);
					specularTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.specular[i].second, instanceBuffer[k]->textureName.specular[i].first);
				}
				instanceBuffer[k]->specularIdx = Scene::CreateShaderResourceViews(device, commandList, specularTexture, GraphicsRootSpecularTextures);
			}
			else {
				instanceBuffer[k]->specularIdx = instanceBuffer[equalidx]->specularIdx;
			}

			textures.emplace_back(specularTexture);
		}
		if (instanceBuffer[k]->textureName.metallic.size() > 0)
		{
			bool isEqual = true;
			int equalidx = -1;
			Texture* metallicTexture = NULL;
			for (int i = 0; i < mtname.size(); ++i) {
				for (int j = 0; j < instanceBuffer[k]->textureName.normal.size(); ++j) {
					if (mtname[i].second.compare(instanceBuffer[k]->textureName.normal[j].second) == 0) {
						isEqual = false;
						equalidx = mtname[i].first;
						break;
					}
				}
				if (!isEqual)
					break;
			}

			if (isEqual) {
				metallicTexture = new Texture((int)instanceBuffer[k]->textureName.metallic.size(), ResourceTexture2D, 0);
				for (int i = 0; i < instanceBuffer[k]->textureName.metallic.size(); ++i)
				{
					mtname.emplace_back(k, instanceBuffer[k]->textureName.metallic[i].second);
					metallicTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.metallic[i].second, instanceBuffer[k]->textureName.metallic[i].first);
				}
				instanceBuffer[k]->metallicIdx = Scene::CreateShaderResourceViews(device, commandList, metallicTexture, GraphicsRootMetallicTextures);
			}
			else {
				instanceBuffer[k]->metallicIdx = instanceBuffer[equalidx]->metallicIdx;
			}
			textures.emplace_back(metallicTexture);
		}
	}
}

void InstancingShader::DeleteAllObjcet()
{
	for (int i = 0; i < instanceBuffer.size(); ++i) {
		while (true) {
			if (instanceBuffer[i]->objects.size() < 2) {
				break;
			}
			GameObject* temp = instanceBuffer[i]->objects.back();
			instanceBuffer[i]->objects.pop_back();
			delete temp;

		}
		instanceBuffer[i]->objectsNum = 1;
	}
}

void InstancingShader::LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, int objectType)
{
	GameObject* object = new GameObject();
	if (objectType != 0) {
		object->SetChild(objects[objectType]);
	}
	object->SetTransform(world);
	if (objectType == 96)
	{
		object->SetScale(5.0f, 5.0f, 1.0f);
		object->curPos.Clear();
		object->prePos.Clear();
	}
	
	std::vector<GameObject*>* bounding = instanceBuffer[objectType]->objects[0]->GetBoundingObject();

	for (int i = 0; i < bounding->size(); ++i) {
		BoundingOrientedBox tempObb;
		tempObb.Center = XMFLOAT3(0.0f, 0.0f, 0.0f);
		tempObb.Extents = XMFLOAT3(2.0f, 2.0f, 2.0f);
		tempObb.Orientation = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
		tempObb.Transform(tempObb, XMLoadFloat4x4(&(*bounding)[i]->GetWorld()));
		tempObb.Transform(tempObb, XMLoadFloat4x4(&world));
		object->GetobbVector().emplace_back(tempObb);
	}

	instanceBuffer[objectType]->objects.emplace_back(object);
	instanceBuffer[objectType]->objectsNum++;
}

void InstancingShader::SetVelocityTheta(XMFLOAT3 velocity, float theta)
{
	instanceBuffer.back()->objects.back()->SetVelocity(velocity);
	instanceBuffer.back()->objects.back()->isMove = true;
	instanceBuffer.back()->objects.back()->arrowTheta = theta;
}

void InstancingShader::OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx, bool isShadow)
{
	if (!isShadow || instanceBuffer[idx]->pipeLineStateIdx == InstanceShaderPS_Blend || instanceBuffer[idx]->pipeLineStateIdx == InstanceShaderPS_Grass) {
		if (instanceBuffer[idx]->diffuseIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->diffuseIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->diffuseIdx].srvGpuDescriptorHandle);
		if (instanceBuffer[idx]->normalIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->normalIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->normalIdx].srvGpuDescriptorHandle);
		if (instanceBuffer[idx]->specularIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->specularIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->specularIdx].srvGpuDescriptorHandle);
		if (instanceBuffer[idx]->metallicIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->metallicIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->metallicIdx].srvGpuDescriptorHandle);
	}
	if (isShadow)
	{
		if (Shader::curPipelineState != shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx]) {
			commandList->SetPipelineState(shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx]);
			curPipelineState = shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx];
		}
	}
	else
	{
		if (Shader::curPipelineState != pipelineStates[instanceBuffer[idx]->pipeLineStateIdx]){
			commandList->SetPipelineState(pipelineStates[instanceBuffer[idx]->pipeLineStateIdx]);
			curPipelineState = pipelineStates[instanceBuffer[idx]->pipeLineStateIdx];
		}
	}

}

void InstancingShader::Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx)
{
	UpdateShaderVariables(NULL, cameraIdx);

	for (int k = 0; k < instanceBuffer.size(); ++k)
	{	
		OnPrepareRender(commandList, k, isShadow);

		if (isShadow == true && instanceBuffer[k]->useShadow == false) return;
		if (!instanceBuffer[k]->objects.empty())
		{
			if (instanceBuffer[k]->renderObjNum[cameraIdx] > 0) {
				instanceBuffer[k]->objects[0]->Render(commandList, UINT(instanceBuffer[k]->renderObjNum[cameraIdx]), instanceBuffer[k]->instancingGameObjectBufferView[cameraIdx], isShadow);
			}
		}
		// 바운딩 박스 렌더링.
		if (isShadow == false && showBounding == true) {
		
			if (Shader::curPipelineState != pipelineStates[InstanceShaderPS_Bounding]){
				commandList->SetPipelineState(pipelineStates[InstanceShaderPS_Bounding]);
				curPipelineState = pipelineStates[InstanceShaderPS_Bounding];
			}
		
			if (!instanceBuffer[k]->objects.empty())
			{
				for (int m = 0; m < instanceBuffer[k]->instancingBoundingBufferView.size(); ++m)
					instanceBuffer[k]->objects[0]->BoundingRender(commandList, UINT(instanceBuffer[k]->objects.size() - 1), instanceBuffer[k]->instancingBoundingBufferView[m]);
			}
		}
	}
}

void InstancingShader::FrustumCulling(void * frustum, bool isShadow, UINT cameraIdx)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		
		BoundingOrientedBox obb = instanceBuffer[k]->obb;
		for (int i = 1; i < instanceBuffer[k]->objects.size(); ++i)
		{
			bool isVisable = true;

			BoundingOrientedBox resultOBB;
			obb.Transform(resultOBB, XMLoadFloat4x4(&instanceBuffer[k]->objects[i]->GetWorld()));
			if (isShadow)
			{
				BoundingOrientedBox camFrustum = *(BoundingOrientedBox*)frustum;
				isVisable = camFrustum.Intersects(resultOBB);
			}
			else
			{
				BoundingFrustum camFrustum = *(BoundingFrustum*)frustum;
				isVisable = camFrustum.Intersects(resultOBB);
			}

			instanceBuffer[k]->isVisable[i - 1][cameraIdx] = isVisable;

		}
	}
}

void InstancingShader::ApplyLOD(Camera* camera, int idx)
{
	//int billboardObjIdx = -1;
	//int objIdx = -1;

	//// 빌보드가 될 수 있는 오브젝트의 경우
	//if (instanceBuffer[idx]->useBillboard >= 0)
	//{
	//	for (int i = 0; i < instanceBuffer.size(); ++i)
	//	{
	//		if (instanceBuffer[idx]->useBillboard == instanceBuffer[i]->isBillboard)
	//		{
	//			billboardObjIdx = i;
	//			break;
	//		}
	//	}
	//	if (billboardObjIdx == -1)
	//		return;
	//	for (int j = 1; j < instanceBuffer[idx]->objects.size(); ++j)
	//	{
	//		if (instanceBuffer[idx]->objects[j]->IsFar(camera))
	//		{
	//			GameObject* temp = instanceBuffer[idx]->objects[j];
	//			XMFLOAT3 position = temp->GetPosition();
	//			instanceBuffer[idx]->objects[j] = instanceBuffer[idx]->objects.back();
	//			instanceBuffer[idx]->objects.pop_back();
	//			instanceBuffer[idx]->objectsNum--;

	//			BillboardObject* obj = new BillboardObject;
	//			position.x += 7.5, position.z += 7.5, position.y += 10;
	//			obj->billboardVertex.position = position;
	//			obj->billboardVertex.billboardInfo = XMFLOAT3(15, 20, float(instanceBuffer[billboardObjIdx]->isBillboard));

	//			instanceBuffer[billboardObjIdx]->objects.emplace_back(obj);
	//			instanceBuffer[billboardObjIdx]->objectsNum++;
	//		}
	//	}
	//}
	//// 빌보드 오브젝트의 경우
	//if (instanceBuffer[idx]->isBillboard >= 0) {
	//	for (int i = 0; i < instanceBuffer.size(); ++i)
	//	{
	//		if (instanceBuffer[idx]->isBillboard == instanceBuffer[i]->useBillboard)
	//		{
	//			objIdx = i;
	//			break;
	//		}
	//	}
	//	if (objIdx == -1)
	//		return;
	//	for (int j = 1; j < instanceBuffer[idx]->objects.size(); ++j)
	//	{

	//		if (!instanceBuffer[idx]->objects[j]->IsFar(camera))
	//		{
	//			BillboardObject* temp2 = (BillboardObject*)instanceBuffer[idx]->objects[j];
	//			XMFLOAT3 position = temp2->billboardVertex.position;
	//			instanceBuffer[idx]->objects[j] = instanceBuffer[idx]->objects.back();
	//			instanceBuffer[idx]->objects.pop_back();
	//			instanceBuffer[idx]->objectsNum--;

	//			GameObject* obj;
	//			obj = new TreeObject();
	//			obj->SetChild(objects[2]);
	//			position.x -= 7.5, position.z -= 7.5, position.y -= 10;
	//			obj->SetPosition(position);
	//			obj->SetScale(3.0f, 3.0f, 3.0f);
	//			
	//			instanceBuffer[objIdx]->objects.emplace_back(obj);
	//			instanceBuffer[objIdx]->objectsNum++;
	//		}
	//	}
	//}
}

void InstancingShader::LoadBoundingBoxInfo()
{
	//바운딩 박스 정보 읽기 
	std::ifstream in{ "Assets/Bounding/bInform.txt" ,std::ios::out | std::ios::binary };
	std::vector<bImform> bivec;

	for (int i = 0; i < instanceBuffer.size(); ++i) {
		if (instanceBuffer[i]->objects[0]->GetBoundingObject() != NULL) { // 큐브 때문에 이렇게 막음 

			bImform tb;
			in.read((char*)&tb.size, sizeof(int));
			tb.bworld.resize(tb.size);
			tb.tscale.resize(tb.size);
			tb.trotate.resize(tb.size);
			tb.tpos.resize(tb.size);
			if (tb.size == 0)
				continue;
			//instanceBuffer[i]->objects[0]->GetobbVector().resize(tb.size);
			in.read((char*)&tb.bworld[0], sizeof(XMFLOAT4X4) * tb.size);
			in.read((char*)&tb.tscale[0], sizeof(XMFLOAT3) * tb.size);
			in.read((char*)&tb.trotate[0], sizeof(XMFLOAT3) * tb.size);
			in.read((char*)&tb.tpos[0], sizeof(XMFLOAT3) * tb.size);

			for (int j = 0; j < tb.size; ++j) {
				GameObject* BoundingObject = new GameObject();
				BoundingObject->SetWorld(tb.bworld[j]);
				BoundingObject->SetAllScale(tb.tscale[j].x, tb.tscale[j].y, tb.tscale[j].z);
				BoundingObject->GetInputAngle() = tb.trotate[j];
				BoundingObject->SettPos(tb.tpos[j]);
				instanceBuffer[i]->objects[0]->GetBoundingObject()->emplace_back(BoundingObject);
			}
		}
	}
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SkinnedInstancingShader::SkinnedInstancingShader()
{
}

SkinnedInstancingShader::~SkinnedInstancingShader()
{
}

void SkinnedInstancingShader::CreateShader(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature, ID3D12RootSignature* computeRootSignature)
{
	pipelineStatesNum = SkinnedShaderPS_Num;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	shadowPipelineStateNum = 2;
	shadowPipelineStates = new ID3D12PipelineState*[shadowPipelineStateNum];

	Shader::CreateShader(device, graphicsRootSignature);
	auto boundingPipeLineStateDesc = pipelineStateDesc;

	pipelineStateDesc.VS = Shader::CompileShaderFromFile(L"ModelViewer.hlsl", "VSSkinnedAnimationStandard", "vs_5_1", &vertexShaderBlob);

	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[SkinnedShaderPS_Standard]);

	// 그림자
	Shader::CreateShadowShader(device, graphicsRootSignature);

	pipelineStateDesc.VS = Shader::CompileShaderFromFile(L"ShadowCaster.hlsl", "VSCSMSkinnedStandard", "vs_5_1", &vertexShaderBlob);

	hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&shadowPipelineStates[1]);

	// 컴퓨트 스키닝
	computePipelineStatesNum = 1;
	computePipelineStates = new ID3D12PipelineState*[computePipelineStatesNum];

	::ZeroMemory(&computePipelineStateDesc, sizeof(D3D12_COMPUTE_PIPELINE_STATE_DESC));
	computePipelineStateDesc.pRootSignature = computeRootSignature;
	computePipelineStateDesc.CS = CreateComputeShader();

	hResult = device->CreateComputePipelineState(&computePipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&computePipelineStates[0]);

	// 바운딩박스
	UINT inputElementDescsNum = 5;
	D3D12_INPUT_ELEMENT_DESC *BoundinginputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	BoundinginputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	BoundinginputElementDescs[1] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[2] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[3] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[4] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

	boundingPipeLineStateDesc.InputLayout.NumElements = inputElementDescsNum;
	boundingPipeLineStateDesc.InputLayout.pInputElementDescs = BoundinginputElementDescs;

	boundingPipeLineStateDesc.RasterizerState.FillMode = D3D12_FILL_MODE_WIREFRAME;
	boundingPipeLineStateDesc.VS = Shader::CompileShaderFromFile(L"BoundingBox.hlsl", "VS_Bounding", "vs_5_1", &vertexShaderBlob);
	boundingPipeLineStateDesc.PS = Shader::CompileShaderFromFile(L"BoundingBox.hlsl", "PS_Bounding", "ps_5_1", &pixelShaderBlob);

	hResult = device->CreateGraphicsPipelineState(&boundingPipeLineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[SkinnedShaderPS_Bounding]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
	if (boundingPipeLineStateDesc.InputLayout.pInputElementDescs) delete[] boundingPipeLineStateDesc.InputLayout.pInputElementDescs;

}

void SkinnedInstancingShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, std::vector<void*>& context, int hObjectSize)
{
	int iter = hObjectSize;
	bool isZombie = false;
	bool isBoss = false;
	for (int i = hObjectSize; i < context.size(); ++i) {
		LoadedModelInfo* tempModel = (LoadedModelInfo*)context[iter++];
		if (i > hObjectSize + 2) isZombie = true;
		else isZombie = false;
		if (i == context.size() - 1) isBoss = true;
		BuildNewSkinedObject(device, commandList, graphicsRootSignature, tempModel, XMFLOAT3(0.7f, 1.8f, 0.7f), isZombie, isBoss);
	}

	SetTextures(device, commandList);

	for (int i = 0; i < instanceBuffer.size(); ++i)
		CreateShaderVariables(device, commandList, i);

}

void SkinnedInstancingShader::BuildNewSkinedObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, LoadedModelInfo * context, XMFLOAT3 boundingSize , bool isZombie, bool isBoss)
{
	static int contextCount = 0;
	SkinnedInstanceBuffer* tempBuffer = new SkinnedInstanceBuffer;

	tempBuffer->objectsNum = 1;
	tempBuffer->objects.resize(tempBuffer->objectsNum);

	LoadedModelInfo* tempModel = (LoadedModelInfo*)context;
	instanceModels.emplace_back(tempModel);
	tempBuffer->isBoss = isBoss;

	if (isZombie == true && isBoss == false) {
		tempModel->animationSets->animationSets[ZombieDeath1]->animationType = AnimationTypeOnce;
		tempModel->animationSets->animationSets[ZombieDeath2]->animationType = AnimationTypeOnce;
	}

	tempBuffer->textureName = tempModel->modelRootObject->textureName;
	tempBuffer->skinnedMeshNum = tempModel->skinnedMeshNum;
	tempBuffer->standardMeshNum = tempModel->standardMeshNum;
	tempBuffer->boundingMeshNum = tempModel->boundingMeshNum;
	tempBuffer->hp = tempModel->modelRootObject->hp;
	tempBuffer->strikingPower = tempModel->modelRootObject->strikingPower;
	tempBuffer->defencivePower = tempModel->modelRootObject->defencivePower;

	tempBuffer->skinnedMeshes = new SkinnedMesh*[tempBuffer->skinnedMeshNum];
	for (int i = 0; i < tempBuffer->skinnedMeshNum; i++) tempBuffer->skinnedMeshes[i] = tempModel->skinnedMeshes[i];

	for (int i = 0; i < tempBuffer->objectsNum; ++i) {
		bool isDummy = false;
		if (i == 0)
			isDummy = true;
		tempBuffer->objects[i] = new AnimationObject(device, commandList, tempModel, 1, isDummy);
		tempBuffer->objects[i]->skinnedAnimationController->SetTrackAnimationSet(0, 0);
		tempBuffer->objects[i]->skinnedAnimationController->SetTrackSpeed(0, 1);
		tempBuffer->objects[i]->skinnedAnimationController->SetTrackPosition(0.0f, 0.0f);
		tempBuffer->objects[i]->SetPosition(XMFLOAT3(30 + 2 * i, 0, 30 + 2 * contextCount));
		tempBuffer->pipeLineStateIdx = 0;
		tempBuffer->shadowPipeLineStateIdx = 0;
	}
	contextCount++;

	tempBuffer->isZombie = isZombie;
	instanceBuffer.emplace_back(tempBuffer);

}

void SkinnedInstancingShader::DeleteAllObjcet()
{
	for (int i = 0; i < instanceBuffer.size(); ++i) {
		while (true) {
			if (instanceBuffer[i]->objects.size() < 2) {
				break;
			}
			GameObject* temp = instanceBuffer[i]->objects[instanceBuffer[i]->objects.size() - 1];
			instanceBuffer[i]->objects.pop_back();
			delete temp;

		}
		instanceBuffer[i]->objectsNum = 1;
	}
}

void SkinnedInstancingShader::LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType)
{
	AnimationObject* tempObject = new AnimationObject(device, commandList, instanceModels[objectType], 1, false);
	tempObject->skinnedAnimationController->SetTrackAnimationSet(0, 0);
	tempObject->skinnedAnimationController->SetTrackSpeed(0, 1);
	tempObject->skinnedAnimationController->SetTrackPosition(0.0f, 0.0f);
	tempObject->SetTransform(world);
	std::vector<BoundingOrientedBox>& tempObb = tempObject->GetobbVector();
	tempObb.resize(instanceBuffer[objectType]->boundingMeshNum);
	std::vector<GameObject*>* tempVec = new std::vector<GameObject*>;
	tempObject->SetBoundingObject(tempVec);
	tempObject->hp = instanceBuffer[objectType]->hp;
	tempObject->strikingPower = instanceBuffer[objectType]->strikingPower;
	tempObject->defencivePower = instanceBuffer[objectType]->defencivePower;


	for (int i = 0; i < tempObb.size(); ++i)
	{
		tempObb[i].Center = XMFLOAT3(0.0f, 0.0f, 0.0f);
		tempObb[i].Extents = XMFLOAT3(0.5f, 0.5f, 0.5f);
		GameObject* tempObj = new GameObject();
		tempObject->GetBoundingObject()->emplace_back(tempObj);
	}
	

	instanceBuffer[objectType]->objects.emplace_back(tempObject);
}


void SkinnedInstancingShader::CreateShaderVariables(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx)
{
	UINT skinnedStride = 0;
	ID3D12Resource** tempSkinned = NULL;
	XMFLOAT4X4** tempMappedSkinned = NULL;

	UINT objIdxStride = 0;
	ID3D12Resource** tempObjIdx = NULL;
	UINT** tempMappedObjIdx = NULL;
	D3D12_VERTEX_BUFFER_VIEW* tempObjIdxBufferView = NULL;

	UINT standardStride = 0;
	ID3D12Resource*** tempStandard = NULL;
	VS_VB_INSTANCE_OBJECT*** tempMappedStandard = NULL;
	D3D12_VERTEX_BUFFER_VIEW** tempStandardBufferView = NULL;

	UINT boundStride = 0;
	ID3D12Resource** tempbounding = NULL;
	VS_VB_INSTANCE_BOUNDING** tempMappedBounding = NULL;
	D3D12_VERTEX_BUFFER_VIEW* tempBoundingBufferView;

	skinnedStride = sizeof(XMFLOAT4X4) * SKINNED_ANIMATION_BONES;
	tempSkinned = new ID3D12Resource*[instanceBuffer[idx]->skinnedMeshNum];
	tempMappedSkinned = new XMFLOAT4X4*[instanceBuffer[idx]->skinnedMeshNum];

	objIdxStride = sizeof(UINT);
	tempObjIdx = new ID3D12Resource*[CAMERAS_NUM];
	tempMappedObjIdx = new UINT*[CAMERAS_NUM];
	tempObjIdxBufferView = new D3D12_VERTEX_BUFFER_VIEW[CAMERAS_NUM];

	standardStride = sizeof(VS_VB_INSTANCE_OBJECT);
	tempStandard = new ID3D12Resource**[CAMERAS_NUM];
	tempMappedStandard = new VS_VB_INSTANCE_OBJECT**[CAMERAS_NUM];
	tempStandardBufferView = new D3D12_VERTEX_BUFFER_VIEW*[CAMERAS_NUM];

	boundStride = sizeof(VS_VB_INSTANCE_BOUNDING);
	tempbounding = new ID3D12Resource*[instanceBuffer[idx]->boundingMeshNum];
	tempMappedBounding = new VS_VB_INSTANCE_BOUNDING*[instanceBuffer[idx]->boundingMeshNum];
	tempBoundingBufferView = new D3D12_VERTEX_BUFFER_VIEW[instanceBuffer[idx]->boundingMeshNum];

	for (int i = 0; i < CAMERAS_NUM; ++i) {
		tempStandard[i] = new ID3D12Resource*[instanceBuffer[idx]->standardMeshNum];
		tempMappedStandard[i] = new VS_VB_INSTANCE_OBJECT*[instanceBuffer[idx]->standardMeshNum];
		tempStandardBufferView[i] = new D3D12_VERTEX_BUFFER_VIEW[instanceBuffer[idx]->standardMeshNum];
	}

	for (int i = 0; i < instanceBuffer[idx]->skinnedMeshNum; ++i)
	{
		tempSkinned[i] = ::CreateBufferResource(device, commandList, NULL, skinnedStride * SKINNED_BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_GENERIC_READ, NULL);
		tempSkinned[i] -> Map(0, NULL, (void **)&tempMappedSkinned[i]);
	}
	// Skinned Bone Transform Buffers ( SRV )

	for (int i = 0; i < CAMERAS_NUM; ++i) {
		tempObjIdx[i] = ::CreateBufferResource(device, commandList, NULL, objIdxStride * (UINT)SKINNED_BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		tempObjIdx[i]->Map(0, NULL, (void**)&tempMappedObjIdx[i]);
		tempObjIdxBufferView[i].BufferLocation = tempObjIdx[i]->GetGPUVirtualAddress();
		tempObjIdxBufferView[i].StrideInBytes = objIdxStride;
		tempObjIdxBufferView[i].SizeInBytes = objIdxStride * (UINT)SKINNED_BUFFERSIZE;
	}
	// Object Index Bufer ( VertexBuffer )

	for (int j = 0; j < CAMERAS_NUM; ++j) {
		for (int i = 0; i < instanceBuffer[idx]->standardMeshNum; ++i)
		{
			tempStandard[j][i] = ::CreateBufferResource(device, commandList, NULL, standardStride * (UINT)SKINNED_BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
			tempStandard[j][i]->Map(0, NULL, (void **)&tempMappedStandard[j][i]);

			tempStandardBufferView[j][i].BufferLocation = tempStandard[j][i]->GetGPUVirtualAddress();
			tempStandardBufferView[j][i].StrideInBytes = standardStride;
			tempStandardBufferView[j][i].SizeInBytes = standardStride * ((UINT)SKINNED_BUFFERSIZE);
		}
	}
	// Standard Mesh Transform Buffers ( VertexBuffer)

	for (int i = 0; i < instanceBuffer[idx]->boundingMeshNum; ++i)
	{
		tempbounding[i] = ::CreateBufferResource(device, commandList, NULL, boundStride * (UINT)SKINNED_BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		tempbounding[i]->Map(0, NULL, (void**)&tempMappedBounding[i]);

		tempBoundingBufferView[i].BufferLocation = tempbounding[i]->GetGPUVirtualAddress();
		tempBoundingBufferView[i].StrideInBytes = boundStride;
		tempBoundingBufferView[i].SizeInBytes = boundStride * (UINT)SKINNED_BUFFERSIZE;


	}
	// Bounding Mesh Transform Buffers (VertexBuffer)

	XMFLOAT4X4** tempObbCache = new XMFLOAT4X4*[instanceBuffer[idx]->boundingMeshNum];
	
	for (int i = 0; i < instanceBuffer[idx]->boundingMeshNum; ++i)
	{
		tempObbCache[i] = new XMFLOAT4X4[SKINNED_BUFFERSIZE];
	}
	
	VS_VB_INSTANCE_OBJECT** tempCache = new VS_VB_INSTANCE_OBJECT*[instanceBuffer[idx]->standardMeshNum];

	for (int i = 0; i < instanceBuffer[idx]->standardMeshNum; ++i)
	{
		tempCache[i] = new VS_VB_INSTANCE_OBJECT[SKINNED_BUFFERSIZE];
	}
	// Cache

	ID3D12Resource** tempPositionW = NULL;
	ID3D12Resource** tempNormalW = NULL;
	ID3D12Resource** tempTangentW = NULL;
	ID3D12Resource** tempBiTangentW = NULL;

	tempPositionW = new ID3D12Resource*[instanceBuffer[idx]->skinnedMeshNum];
	tempNormalW = new ID3D12Resource*[instanceBuffer[idx]->skinnedMeshNum];
	tempTangentW = new ID3D12Resource*[instanceBuffer[idx]->skinnedMeshNum];
	tempBiTangentW = new ID3D12Resource*[instanceBuffer[idx]->skinnedMeshNum];

	for (int i = 0; i < instanceBuffer[idx]->skinnedMeshNum; ++i) {
		UINT bytesNum = sizeof(XMFLOAT3) * instanceBuffer[idx]->skinnedMeshes[i]->GetVerticesNum() * SKINNED_BUFFERSIZE;
		tempPositionW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		tempNormalW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		tempTangentW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		tempBiTangentW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

		::ResourceBarrier(commandList, tempPositionW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, tempNormalW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, tempTangentW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, tempBiTangentW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	}
	// ComputeSkinning

	instanceBuffer[idx]->skinnedBuffer = tempSkinned;
	instanceBuffer[idx]->standardBuffer = tempStandard;
	instanceBuffer[idx]->mappedSkinnedBuffer = tempMappedSkinned;
	instanceBuffer[idx]->mappedStandardBuffer = tempMappedStandard;
	instanceBuffer[idx]->instancingStandardBufferView = tempStandardBufferView;

	instanceBuffer[idx]->objIdxBuffer = tempObjIdx;
	instanceBuffer[idx]->mappedObjIdxBuffer = tempMappedObjIdx;
	instanceBuffer[idx]->instancingObjIdxBufferView = tempObjIdxBufferView;

	instanceBuffer[idx]->boundingBuffer = tempbounding;
	instanceBuffer[idx]->mappedBoundingBuffer = tempMappedBounding;
	instanceBuffer[idx]->instancingBoundingBufferView = tempBoundingBufferView;
	//to draw Bounding Box make world transFormBuffer 
	instanceBuffer[idx]->casheBoundingBuffer = tempObbCache;

	instanceBuffer[idx]->casheStandardBuffer = tempCache;

	instanceBuffer[idx]->positionW = tempPositionW;
	instanceBuffer[idx]->normalW = tempNormalW;
	instanceBuffer[idx]->tangentW = tempTangentW;
	instanceBuffer[idx]->biTangentW = tempBiTangentW;

	instanceBuffer[idx]->isVisable = new bool*[SKINNED_BUFFERSIZE];

	for (int i = 0; i < SKINNED_BUFFERSIZE; ++i)
	{
		instanceBuffer[idx]->isVisable[i] = new bool[CAMERAS_NUM];

		for (int j = 0; j < CAMERAS_NUM; ++j)
			instanceBuffer[idx]->isVisable[i][j] = true;
	}


	for (int i = 0; i < CAMERAS_NUM; ++i)
	{
		instanceBuffer[idx]->renderObjNum[i] = 0;
	}

}

void SkinnedInstancingShader::UpdateShaderVariable(int idx, int objIdx)
{
	for (int i = 0; i < instanceBuffer[idx]->skinnedMeshNum; ++i)
	{
		for (int j = 0; j < instanceBuffer[idx]->skinnedMeshes[i]->skinningBones; ++j)
		{
			XMStoreFloat4x4(&instanceBuffer[idx]->mappedSkinnedBuffer[i][(objIdx * SKINNED_ANIMATION_BONES) + j],
				XMMatrixTranspose(XMLoadFloat4x4(&instanceBuffer[idx]->skinnedMeshes[i]->skinningBoneFrameCaches[j]->GetWorld())));
		}
	}
}

void SkinnedInstancingShader::UpdateShaderVariables(ID3D12GraphicsCommandList * commandList, UINT cameraIdx)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		int objNum = 0;

		for (int j = 0; j < instanceBuffer[k]->objects.size() - 1; ++j)
		{
			if (instanceBuffer[k]->isVisable[j][cameraIdx])
			{
				if (instanceBuffer[k]->objects[j + 1]->needDelete == false)
				{
					instanceBuffer[k]->mappedObjIdxBuffer[cameraIdx][objNum] = j;
					for (int i = 0; i < instanceBuffer[k]->standardMeshNum; ++i)
						instanceBuffer[k]->mappedStandardBuffer[cameraIdx][i][objNum] = instanceBuffer[k]->casheStandardBuffer[i][j];
					objNum++;
				}
			}
		}
		instanceBuffer[k]->renderObjNum[cameraIdx] = objNum;
	}
}

void SkinnedInstancingShader::ReleaseShaderVariables()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->skinnedBuffer) {
			for (int j = 0; j < instanceBuffer[k]->skinnedMeshNum; ++j)
			{
				instanceBuffer[k]->skinnedBuffer[j]->Unmap(0, NULL);
				instanceBuffer[k]->skinnedBuffer[j]->Release();

				instanceBuffer[k]->positionW[j]->Release();
				instanceBuffer[k]->normalW[j]->Release();
				instanceBuffer[k]->tangentW[j]->Release();
				instanceBuffer[k]->biTangentW[j]->Release();
			}
		}

		if (instanceBuffer[k]->standardBuffer) {
			for (int i = 0; i < CAMERAS_NUM; ++i) {
				for (int j = 0; j < instanceBuffer[k]->standardMeshNum; ++j)
				{

					instanceBuffer[k]->standardBuffer[i][j]->Unmap(0, NULL);
					instanceBuffer[k]->standardBuffer[i][j]->Release();
				}
			}
		}
		if (instanceBuffer[k]->objIdxBuffer) {
			for (int i = 0; i < CAMERAS_NUM; ++i) {
				instanceBuffer[k]->objIdxBuffer[i]->Unmap(0, NULL);
				instanceBuffer[k]->objIdxBuffer[i]->Release();
			}
		}

		if (instanceBuffer[k]->boundingBuffer) {
			for (int i = 0; i < instanceBuffer[k]->boundingMeshNum; ++i)
			{
				instanceBuffer[k]->boundingBuffer[i]->Unmap(0, NULL);
				instanceBuffer[k]->boundingBuffer[i]->Release();
			}
		}

	}
}

void SkinnedInstancingShader::ReleaseObjects()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		
		if (!instanceBuffer[k]->objects.empty())
			instanceBuffer[k]->objects[0]->Release();

		for (int i = 0; i < SKINNED_BUFFERSIZE; ++i)
			delete[] instanceBuffer[k]->isVisable[i];
		delete[] instanceBuffer[k]->isVisable;
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->Release();
	}
}

void SkinnedInstancingShader::ReleaseUploadBuffers()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->objects[0])
			instanceBuffer[k]->objects[0]->ReleaseUploadBuffers();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->ReleaseUploadBuffers();
	}
}


void SkinnedInstancingShader::SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->textureName.diffuse.size() > 0)
		{
			Texture* diffuseTexture = new Texture((int)instanceBuffer[k]->textureName.diffuse.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.diffuse.size(); ++i)
			{
				diffuseTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.diffuse[i].second, instanceBuffer[k]->textureName.diffuse[i].first);
			}
			instanceBuffer[k]->diffuseIdx = Scene::CreateShaderResourceViews(device, commandList, diffuseTexture, GraphicsRootDiffuseTextures);
			textures.emplace_back(diffuseTexture);
		}
		if (instanceBuffer[k]->textureName.normal.size() > 0)
		{
			Texture* normalTexture = new Texture((int)instanceBuffer[k]->textureName.normal.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.normal.size(); ++i)
			{
				normalTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.normal[i].second, instanceBuffer[k]->textureName.normal[i].first);
			}
			instanceBuffer[k]->normalIdx = Scene::CreateShaderResourceViews(device, commandList, normalTexture, GraphicsRootNormalTextures);
			textures.emplace_back(normalTexture);
		}
		if (instanceBuffer[k]->textureName.specular.size() > 0)
		{
			Texture* specularTexture = new Texture((int)instanceBuffer[k]->textureName.specular.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.specular.size(); ++i)
			{
				specularTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.specular[i].second, instanceBuffer[k]->textureName.specular[i].first);
			}
			instanceBuffer[k]->specularIdx = Scene::CreateShaderResourceViews(device, commandList, specularTexture, GraphicsRootSpecularTextures);
			textures.emplace_back(specularTexture);
		}
		if (instanceBuffer[k]->textureName.metallic.size() > 0)
		{
			Texture* metallicTexture = new Texture((int)instanceBuffer[k]->textureName.metallic.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.metallic.size(); ++i)
			{
				metallicTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.metallic[i].second, instanceBuffer[k]->textureName.metallic[i].first);
			}
			instanceBuffer[k]->metallicIdx = Scene::CreateShaderResourceViews(device, commandList, metallicTexture, GraphicsRootMetallicTextures);
			textures.emplace_back(metallicTexture);
		}
	}
}

void SkinnedInstancingShader::SetParticleShader(ParticleShader * particleShader)
{
	this->particleShader = particleShader;
}

void SkinnedInstancingShader::AnimateObjects(float timeElapsed, Camera* camera)
{
	elapsedTime = timeElapsed;
	
	if (activePotal == false) {
		// 중간보스, 보스일 경우 잡았을 경우 맵의 모든 좀비들은 죽어야 한다.
		if (instanceBuffer[middleBossIdx]->objects.size() > 1)
			if (instanceBuffer[middleBossIdx]->objects[1]->isLive == false)
			{
				allKill = true;
			}
		if (instanceBuffer[finalBossIdx]->objects.size() > 1)
			if (instanceBuffer[finalBossIdx]->objects[1]->isLive == false)
			{
				allKill = true;
				victory = true;
			}

		if (allKill == true)
		{
			activePotal = true;

			std::vector<bool>& isRender = particleShader->GetIsRender();
			std::vector<ParticleInfoInCPU> pInfo = particleShader->GetParticles();
			UINT way = Scene::FIndCanGoWay();
			for (int i = 0; i < isRender.size(); ++i)
			{
				// 갈수 있는 길만 포탈 활성화.
				if (isRender[i] == false)
				{
					if ((pInfo[i].position.x < -150.0f) && (way & CanGoWayLeft))
					{
						isRender[i] = true;
					}
					else if ((pInfo[i].position.x > 150.0f) && (way & CanGoWayRight))
					{
						isRender[i] = true;
					}
					else if ((pInfo[i].position.z < -150.0f) && (way & CanGoWayDown))
					{
						isRender[i] = true;
					}
					else if ((pInfo[i].position.z > 150.0f) && (way & CanGoWayUp))
					{
						isRender[i] = true;
					}
				}
			}
		}
	}

	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		for (int j = 1; j < instanceBuffer[k]->objects.size(); ++j)
		{
			GameObject* obj = instanceBuffer[k]->objects[j];

			if (allKill == true && obj->isLive == true)
			{
				obj->isLive = false;
				int random = rand() % 2;
				obj->skinnedAnimationController->animationTracks[0].position = 0.0f;
				obj->skinnedAnimationController->SetTrackAnimationSet(0, ZombieDeath1 + random);
			}

			if (obj->isLive == false && obj->needDelete == false)
				obj->zombieDeadAge += timeElapsed;
			if (obj->zombieDeadAge > 10)
				obj->needDelete = true;

		}
	}

}
void SkinnedInstancingShader::Animate()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		for (int j = 1; j < instanceBuffer[k]->objects.size(); ++j)
		{
			if (instanceBuffer[k]->objects[j]->needDelete == false) 
			{
				instanceBuffer[k]->objects[j]->Animate(elapsedTime);
				UpdateShaderVariable(k, j - 1);
				instanceBuffer[k]->objects[j]->UpdateTransformOnlyStandardMesh(instanceBuffer[k]->casheStandardBuffer, j - 1, instanceBuffer[k]->standardMeshNum, 1, NULL, NULL);
				instanceBuffer[k]->objects[j]->UpdateSkinnedBoundingTransform(instanceBuffer[k]->mappedBoundingBuffer, instanceBuffer[k]->objects[j]->GetobbVector(),
					instanceBuffer[k]->objects[j]->GetBoundingObject(), j - 1, instanceBuffer[k]->boundingMeshNum, NULL);
			}
		}
	}
}

void SkinnedInstancingShader::OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx, bool isShadow)
{
	if (!isShadow) {
		if (instanceBuffer[idx]->diffuseIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->diffuseIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->diffuseIdx].srvGpuDescriptorHandle);
		if (instanceBuffer[idx]->normalIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->normalIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->normalIdx].srvGpuDescriptorHandle);
		if (instanceBuffer[idx]->specularIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->specularIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->specularIdx].srvGpuDescriptorHandle);
		if (instanceBuffer[idx]->metallicIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->metallicIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->metallicIdx].srvGpuDescriptorHandle);
	}
	if (isShadow)
	{
		if (Shader::curPipelineState != shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx]) {
			commandList->SetPipelineState(shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx]);
			curPipelineState = shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx];
		}
	}
	else
	{
		if (Shader::curPipelineState != pipelineStates[instanceBuffer[idx]->pipeLineStateIdx]) {
			commandList->SetPipelineState(pipelineStates[instanceBuffer[idx]->pipeLineStateIdx]);
			curPipelineState = pipelineStates[instanceBuffer[idx]->pipeLineStateIdx];
		}
	}
}

void SkinnedInstancingShader::Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx)
{
	UpdateShaderVariables(NULL, cameraIdx);

	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->renderObjNum > 0) {
			
			OnPrepareRender(commandList, k, isShadow);
			ID3D12Resource** buffers[4] = { instanceBuffer[k]->positionW, instanceBuffer[k]->normalW, instanceBuffer[k]->tangentW, instanceBuffer[k]->biTangentW };

			instanceBuffer[k]->objects[0]->Render(commandList, instanceBuffer[k]->renderObjNum[cameraIdx], instanceBuffer[k]->instancingStandardBufferView[cameraIdx], instanceBuffer[k]->instancingObjIdxBufferView[cameraIdx], buffers, isShadow);

			if (showBounding == true && isShadow == false) {

				if (Shader::curPipelineState != pipelineStates[SkinnedShaderPS_Bounding]) {
					commandList->SetPipelineState(pipelineStates[SkinnedShaderPS_Bounding]);
					curPipelineState = pipelineStates[SkinnedShaderPS_Bounding];
				}

				instanceBuffer[k]->objects[0]->SkinnedBoundingRender(commandList, instanceBuffer[k]->objects.size() - 1, instanceBuffer[k]->instancingBoundingBufferView, -1);
			}
		}
	}
}

void SkinnedInstancingShader::PrepareCompute(ID3D12GraphicsCommandList * commandList)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		for (int i = 0; i < instanceBuffer[k]->skinnedMeshNum; ++i)
		{
			if (instanceBuffer[k]->objects.size() > 1) {

				instanceBuffer[k]->skinnedMeshes[i]->skinningBoneTransforms = instanceBuffer[k]->skinnedBuffer[i];
				instanceBuffer[k]->skinnedMeshes[i]->mappedSkinningBoneTransforms = instanceBuffer[k]->mappedSkinnedBuffer[i];

				commandList->SetPipelineState(computePipelineStates[0]);
				ID3D12Resource* buffers[4] = { instanceBuffer[k]->positionW[i], instanceBuffer[k]->normalW[i], instanceBuffer[k]->tangentW[i], instanceBuffer[k]->biTangentW[i] };

				instanceBuffer[k]->skinnedMeshes[i]->OnPreRender(commandList, instanceBuffer[k]->objects.size() - 1, buffers);
			}
		}
	}
}

void SkinnedInstancingShader::FrustumCulling(void * frustum, bool isShadow, UINT cameraIdx)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		BoundingOrientedBox obb = instanceBuffer[k]->obb;
		for (int i = 1; i < instanceBuffer[k]->objects.size(); ++i)
		{
			BoundingOrientedBox resultOBB;
			obb.Transform(resultOBB, XMLoadFloat4x4(&instanceBuffer[k]->objects[i]->GetWorld()));
			bool isVisable = true;
			if (isShadow)
			{
				BoundingOrientedBox camFrustum = *(BoundingOrientedBox*)frustum;
				isVisable = camFrustum.Intersects(resultOBB);
			}
			else
			{
				BoundingFrustum camFrustum = *(BoundingFrustum*)frustum;
				isVisable = camFrustum.Intersects(resultOBB);
			}

			instanceBuffer[k]->isVisable[i - 1][cameraIdx] = isVisable;
		}
	}

}