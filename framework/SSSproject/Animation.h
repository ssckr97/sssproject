#pragma once
#include "GameObject.h"

//애니메이션 컨트롤러 수정 중

struct CALLBACKKEY
{
	float  							time = 0.0f;
	void  							*callbackData = NULL;
};

#define _WITH_ANIMATION_INTERPOLATION

class AnimationCallbackHandler
{
public:
	AnimationCallbackHandler() { }
	~AnimationCallbackHandler() { }

public:
	virtual void HandleCallback(void *CallbackData) { }
};

class AnimationSet
{
public:
	AnimationSet(float Length, int FramesPerSecond, int nKeyFrameTransforms, int SkinningBones, char *Name);
	~AnimationSet();

public:
	char							animationSetName[64];

	float							length = 0.0f;
	int								framesPerSecond = 0; //m_fTicksPerSecond

	int								keyFrames = 0;
	float							lastPosition = 0.0f;

	bool							lockanimation = false;
	bool							lockupanimation = false;


	float							*keyFrameTimes = NULL;
	XMFLOAT4X4						**keyFrameTransforms = NULL;

	bool							isRelease = false;

#ifdef _WITH_ANIMATION_SRT
	int								m_nKeyFrameScales = 0;
	float							*m_pfKeyFrameScaleTimes = NULL;
	XMFLOAT3						**m_ppxmf3KeyFrameScales = NULL;
	int								m_nKeyFrameRotations = 0;
	float							*m_pfKeyFrameRotationTimes = NULL;
	XMFLOAT4						**m_ppxmf4KeyFrameRotations = NULL;
	int								m_nKeyFrameTranslations = 0;
	float							*m_pfKeyFrameTranslationTimes = NULL;
	XMFLOAT3						**m_ppxmf3KeyFrameTranslations = NULL;
#endif

	float 							position = 0.0f;
	float							upPosition = 0.0f;
	float							loPosition = 0.0f;

	int 							animationType = AnimationTypeLoop; //Once, Loop, PingPong

	int 							callbackKeynum = 0;
	CALLBACKKEY 					*callbackKeys = NULL;

	AnimationCallbackHandler 		*animationCallbackHandler = NULL;

public:
	void SetPosition(float Position);

	XMFLOAT4X4 GetSRT(int Bone);

	void SetCallbackKeys(int CallbackKeys);
	void SetCallbackKey(int KeyIndex, float Time, void *Data);
	void SetAnimationCallbackHandler(AnimationCallbackHandler *callbackHandler);

	void *GetCallbackData();
};

class AnimationSets
{
private:
	int								references = 0;

public:
	void AddRef() { references++; }
	void Release() { if (--references <= 0) delete this; }

public:
	AnimationSets(int AnimationSets);
	~AnimationSets();

public:
	int								animationSetnum = 0;
	AnimationSet					**animationSets = NULL;

	int								animatedBoneFrames = 0;
	GameObject						**animatedBoneFrameCaches = NULL; //[m_nAnimatedBoneFrames]

public:
	void SetCallbackKeys(int AnimationSet, int CallbackKeys);
	void SetCallbackKey(int AnimationSet, int KeyIndex, float Time, void *Data);
	void SetAnimationCallbackHandler(int AnimationSet, AnimationCallbackHandler *CallbackHandler);
};

class AnimationTrack
{
public:
	AnimationTrack() { }
	~AnimationTrack() { }

public:
	BOOL 							enable = true;
	float 							speed = 1.0f;
	float 							position = 0.0f;
	float							upPosition = 0.0f;
	float							loPosition = 0.0f;
	float 							weight = 1.0f;

	int 							animationSet = 0;

	int								upanimationSet = 0;
	int								loanimationSet = 0;

public:
	void SetAnimationSet(int AnimationSet) { animationSet = AnimationSet; }

	void SetEnable(bool Enable) { enable = Enable; }
	void SetSpeed(float Speed) { speed = Speed; }
	void SetWeight(float Weight) { weight = Weight; }
	void SetPosition(float Position) { position = Position; }
};

class LoadedModelInfo
{
public:
	LoadedModelInfo() { }
	~LoadedModelInfo();

	GameObject						*modelRootObject = NULL;

	int 							skinnedMeshNum = 0;
	int								standardMeshNum = 0;
	int								boundingMeshNum = 0;
	SkinnedMesh					**skinnedMeshes = NULL; //[SkinnedMeshes], Skinned Mesh Cache

	AnimationSets					*animationSets = NULL;

public:
	void PrepareSkinning();
};

class AnimationController
{
public:
	AnimationController(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, int AnimationTracks, LoadedModelInfo *Model);
	~AnimationController();

public:
	float							bodyRotate = 0.0f;
	float							upperbodyRotate = 0.0f;

	float 							time = 0.0f;

	int 							animationTracknum = 0;
	AnimationTrack 				    *animationTracks = NULL;

	AnimationSets					*animationSets = NULL;

	bool							middleTransform = false;
	bool							upmiddleTransform = false;
	bool							lomiddleTransform = false;

	XMFLOAT4X4						*beforeTransform = NULL;
	XMFLOAT4X4						*upbeforeTransform = NULL;
	XMFLOAT4X4						*lobeforeTransform = NULL;

	XMFLOAT4X4						*nextTransform = NULL;
	XMFLOAT4X4						*upnextTransform = NULL;
	XMFLOAT4X4						*lonextTransform = NULL;

	int								nextAnimation = 0;
	int								nextUpAnimation = 0;
	int								nextLoAnimation = 0;

	int								nowAnimation = -1;
	int								nowupAnimation = 0;
	int								nowloAnimation = 0;

	float							tempPosition = 0.f;
	float							uptempPosition = 0.f;
	float							lotempPosition = 0.f;

	bool							bowCharacterDraw = true;
	bool							bowCanRelease = true;

	int								playerJob = -1;

	bool							clientCharacter = false;

public:
	void UpdateShaderVariables(ID3D12GraphicsCommandList *commandList);

	void SetTrackAnimationSet(int AnimationTrack, int AnimationSet);
	void SetTrackAnimationSet(int AnimationTrack, int AnimationSet1, int AnimationSet2);
	void SetNextAnimationSet(int AnimationTrack, int AnimationSet1, int AnimationSet2);
	void SetTrackEnable(int AnimationTrack, bool Enable);
	void SetTrackPosition(int AnimationTrack, float Position);
	void SetTrackSpeed(int AnimationTrack, float Speed);
	void SetTrackWeight(int AnimationTrack, float Weight);

	void SetCallbackKeys(int AnimationSet, int CallbackKeys);
	void SetCallbackKey(int AnimationSet, int KeyIndex, float Time, void *Data);
	void SetAnimationCallbackHandler(int AnimationSet, AnimationCallbackHandler *CallbackHandler);

	void AdvanceTime(float ElapsedTime, GameObject *RootGameObject);
	void MiddleAnimation(float ElapsedTime, GameObject *RootGameObject);
	void SetNextTransform(int AnimationSet);
	void SetNextUpTransform(int Animationnum);
	void SetNextLoTransform(int Animationnum);
	void SetBeforeTransform(int AnimationSet);
};