#pragma once
#include "Vertex.h"
class GameObject;
class Mesh
{
public:
	Mesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual ~Mesh();
	int meshnum;

private:
	int references = 0;

public:
	void AddRef() { ++references; }
	void Release() { if (--references <= 0) delete this; }

protected:
	char meshName[256] = { 0 };

	UINT type = 0x00;

	UINT slot = 0;
	UINT offset = 0;

	BoundingBox aabbBox;

	XMFLOAT3 boundingBoxAABBCenter = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 boundingBoxAABBExtents = XMFLOAT3(0.0f, 0.0f, 0.0f);

	D3D12_PRIMITIVE_TOPOLOGY primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

protected:
	// Buffers

	int verticesNum = 0;

	Vertex* vertex;
	ID3D12Resource* vertexBuffer = NULL;
	ID3D12Resource* vertexUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW vertexBufferView;

	int indicesNum = 0;

	UINT* indices;
	ID3D12Resource* indexBuffer = NULL;
	ID3D12Resource* indexUploadBuffer = NULL;
	D3D12_INDEX_BUFFER_VIEW indexBufferView;
	// VertexBuffer has Position, Color ...

	XMFLOAT3* positions;
	ID3D12Resource* positionBuffer = NULL;
	ID3D12Resource* positionUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW positionBufferView;

	int	subMeshesNum = 0;
	int* subSetIndicesNum = NULL;
	UINT** subSetIndices = NULL;

	ID3D12Resource** subSetIndexBuffers = NULL;
	ID3D12Resource** subSetIndexUploadBuffers = NULL;
	D3D12_INDEX_BUFFER_VIEW* subSetIndexBufferViews = NULL;
public:
	
	UINT GetType() { return type; }
	virtual void ReleaseUploadBuffers();

	virtual void Render(ID3D12GraphicsCommandList* commandList);
	virtual void Render(ID3D12GraphicsCommandList *commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews);

	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews)
	{
		Render(commandList, instancesNum, instancingBufferViews); 
	};

	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, int weaponNum)
	{
		Render(commandList, instancesNum, instancingBufferViews);
	};

	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, ID3D12Resource* buffers[4]) {};

	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum) { Render(commandList); };
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, ID3D12Resource* buffers[4]) { Render(commandList); };
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum) { Render(commandList); };
	 
	virtual Diffused2TexturedVertex* GetDiffuseTexturedVertex() { return NULL; }
	
	virtual void OnPreRender(ID3D12GraphicsCommandList *commandList, UINT instancesNum) {}

	virtual int GetVerticesNum() { return verticesNum; }

	char *GetmeshName() { return meshName; }

	virtual ID3D12Resource* GetVertexBuffer() { return vertexBuffer; }
	virtual void SetVertexBuffer(ID3D12Resource* vertexBuffer);

};

class TriangleMesh : public Mesh
{
public:
	TriangleMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual ~TriangleMesh() { }
};

class CubeMeshDiffused : public Mesh
{
public:
	//직육면체의 가로, 세로, 깊이의 길이를 지정하여 직육면체 메쉬를 생성한다.
	CubeMeshDiffused(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
		float width = 2.0f, float height = 2.0f, float depth = 2.0f);
	virtual ~CubeMeshDiffused();
};

class AirplaneMeshDiffused : public Mesh
{
public:
	AirplaneMeshDiffused(ID3D12Device *device, ID3D12GraphicsCommandList* commandList,  
		float width = 20.0f, float height = 20.0f, float depth = 4.0f, XMFLOAT4 color = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f));
	virtual ~AirplaneMeshDiffused();
};

class MeshIlluminated : public Mesh
{
public:
	MeshIlluminated(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual ~MeshIlluminated();
public:
	void CalculateTriangleListVertexNormals(XMFLOAT3 *normals, XMFLOAT3* positions, int nVertices);
	void CalculateTriangleListVertexNormals(XMFLOAT3 *normals, XMFLOAT3* positions, UINT verticesNum, UINT *indices, UINT IndicesNum);
	void CalculateTriangleStripVertexNormals(XMFLOAT3* normals, XMFLOAT3* positions, UINT verticesNum, UINT *indices, UINT IndicesNum);
	void CalculateVertexNormals(XMFLOAT3 *normals, XMFLOAT3 *positions, int verticesNum, UINT *indices, int indicesNum);
};

class CubeMeshIlluminated : public MeshIlluminated
{
public:
	CubeMeshIlluminated(ID3D12Device *device, ID3D12GraphicsCommandList* commandList, float width = 2.0f, float height = 2.0f, float depth = 2.0f);
	virtual ~CubeMeshIlluminated();
};

// Normal값을 CalculateVertexNormals를 사용하여 계산하여 정해줌.
class CubeMeshIlluminatedTextured : public MeshIlluminated
{
public:
	CubeMeshIlluminatedTextured(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width = 2.0f, float height = 2.0f, float depth = 2.0f);
	virtual ~CubeMeshIlluminatedTextured();
};

class SkyBoxMesh : public Mesh
{
private:
	TexturedVertex* texturedVertex;

public:
	SkyBoxMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float width = 10.0f, float height = 10.0f, float depth = 10.0f);
	virtual ~SkyBoxMesh();
};

class UiMesh : public Mesh
{
private:
	UiVertex* texturedVertex;

public:
	UiMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual ~UiMesh();
};

class BoundingBoxMesh : public Mesh 
{
public:
	BoundingBoxMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth);
	BoundingBoxMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, XMFLOAT3 center, float width, float height, float depth);
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, int weaponNum);
	virtual ~BoundingBoxMesh();


};


class HeightMapImage
{
private:
	// 높이 맵 이미지 픽셀(8-비트)들의 이차원 배열이다. 각 픽셀은 0~255 값을 갖는다.
	BYTE*						heightMapPixels;

	// 높이 맵 이미지의 가로와 세로 크기이다.
	int							width;
	int							length;

	// 높이 맵 이미지를 실제로 몇 배 확대하여 사용할 것인가를 나타내는 스케일 벡터이다.
	XMFLOAT3					scale;

public:
	HeightMapImage(LPCTSTR fileName, int width, int length, XMFLOAT3 scale);
	~HeightMapImage();

	// 높이 맵 이미지에서 (x, z) 위치의 픽셀 값에 기반한 지형의 높이를 반환한다.
	float GetHeight(float x, float z);

	// 높이 맵 이미지에서 (x, z) 위치의 법선 벡터를 반환한다.
	XMFLOAT3 GetHeightMapNormal(int x, int z);
	void LoadHeightMapImage(LPCTSTR fileName);

	XMFLOAT3 GetScale() { return scale; }
	BYTE* GetHeightMapPixels() { return (heightMapPixels); }
	int GetHeightMapWidth() { return(width); }
	int GetHeightMapLength() { return (length); }

};

class HeightMapGridMesh : public Mesh
{
protected:
	int							width;
	int							length;
	// 격자의 크기(가로 : x-방향, 세로: z-방향)이다.
	int							boundingWidth;
	int							boundingLength;

	XMFLOAT3					scale;
	/*
	격자의 스케일(가로 : x-방향, 세로: z-방향, 높이: y-방향) 벡터이다. 실제 격자 메쉬의 각 정점의 x-좌표, y-좌표,
	z-좌표는 스케일 벡터의 x-좌표, y-좌표, z-좌표로 곱한 값을 갖는다. 즉 실제 격자의 x-축 방향의 간격은 1이 아니라
	스케일 벡터의 x-좌표가 된다. 이렇게 하면 작은 격자(적은 정점)을 사용하더라도 큰 크기의 격자(지형)을 생성할 수 있다.
	*/
	Diffused2TexturedVertex *diffused2TexturedVerices{NULL};
	Diffused2TexturedVertex* mappedDiffued2TextureVerices{ NULL };

	BoundingBox terrainBoundings[TERRAINBOUNDINGS];
	int			boundingVertexIndex[TERRAINBOUNDINGS];
	int**		boundingTree = NULL;



public:
	HeightMapGridMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
		int xStart, int zStart, int width, int length,
		XMFLOAT3 scale = XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT4 color = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f),
		void* context = NULL);

	virtual ~HeightMapGridMesh();

	XMFLOAT3 GetScale() { return (scale); }
	int GetWidth() { return (width); }
	int GetLength() { return (length); }
	int GetVertexNum() { return verticesNum; }
	// 격자의 좌표가 (x, z)일 때, 교점(정점)의 높이를 반환하는 함수이다.
	virtual float OnGetHeight(int x, int z, void* context);

	XMFLOAT3 calNormal(int index);
	void calALLNormal();

	// 격자의 좌표가 (x, z)일 때 교점(정점)의 색상을 반환하는 함수이다.
	virtual XMFLOAT4 OnGetColor(int x, int z, void* context);
	virtual Diffused2TexturedVertex* GetDiffuseTexturedVertex() { return diffused2TexturedVerices; }
	void ResetVertexBuffer(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, Diffused2TexturedVertex* diffuseVertex);

	BoundingBox* GetBoundings() { return terrainBoundings; }
	int*		 GetBoudingVertexIndex() { return boundingVertexIndex; }

	virtual bool CheckRayIntersect(XMFLOAT3 origin, XMFLOAT3 dir, float& pfNearHitDistance, XMFLOAT4X4 world, int bIdx);


};

class StandardMesh : public Mesh
{
public:
	StandardMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual ~StandardMesh();

protected:
	UINT							vertexSize = 0;

	XMFLOAT4						*colors = NULL;
	XMFLOAT3						*normals = NULL;
	XMFLOAT3						*tangents = NULL;
	XMFLOAT3						*biTangents = NULL;
	XMFLOAT2						*textureCoords0 = NULL;
	XMFLOAT2						*textureCoords1 = NULL;

	ID3D12Resource					*textureCoord0Buffer = NULL;
	ID3D12Resource					*textureCoord0UploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		textureCoord0BufferView;

	ID3D12Resource					*textureCoord1Buffer = NULL;
	ID3D12Resource					*textureCoord1UploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		textureCoord1BufferView;

	ID3D12Resource					*normalBuffer = NULL;
	ID3D12Resource					*normalUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		normalBufferView;

	ID3D12Resource					*tangentBuffer = NULL;
	ID3D12Resource					*tangentUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		tangentBufferView;

	ID3D12Resource					*biTangentBuffer = NULL;
	ID3D12Resource					*biTangentUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		biTangentBufferView;

public:
	void LoadMeshFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, FILE *pInFile);

	virtual void ReleaseUploadBuffers();
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subSet);
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews);
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, int weaponNum);
};

class CubeMeshNormalMapTextured : public StandardMesh
{
public:
	CubeMeshNormalMapTextured(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width = 2.0f, float height = 2.0f, float depth = 2.0f);
	virtual ~CubeMeshNormalMapTextured();

	void CalculateTriangleListTBNs(int verticesNum, XMFLOAT3 *positions, XMFLOAT2 *texCoords, XMFLOAT3 *tangents, XMFLOAT3 *biTangents, XMFLOAT3 *normals);
};

class TexturedRectMesh : public StandardMesh
{
public:
	TexturedRectMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float fWidth = 20.0f, float fHeight = 20.0f, float fDepth = 20.0f, float fxPosition = 0.0f, float fyPosition = 0.0f, float fzPosition = 0.0f);
	virtual ~TexturedRectMesh();
};

class SkinnedMesh : public StandardMesh
{
public:
	SkinnedMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList);
	virtual ~SkinnedMesh();

protected:
	int								bonesPerVertex = 4;

	XMINT4							*boneIndices = NULL;
	XMFLOAT4						*boneWeights = NULL;

	ID3D12Resource					*boneIndexBuffer = NULL;
	ID3D12Resource					*boneIndexUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		boneIndexBufferView;

	ID3D12Resource					*boneWeightBuffer = NULL;
	ID3D12Resource					*boneWeightUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		boneWeightBufferView;

public:
	int								skinningBones = 0;

	char(*skinningBoneNames)[128];
	GameObject						**skinningBoneFrameCaches = NULL; //[m_nSkinningBones]

	XMFLOAT4X4						*xmf4x4BindPoseBoneOffsets = NULL; //Transposed

	ID3D12Resource					*bindPoseBoneOffsets = NULL;
	XMFLOAT4X4*						mappedBindPoseBoneOffsets = NULL;
	ID3D12Resource					*bindPoseBoneOffsetsUploadBuffer = NULL;

	ID3D12Resource					*skinningBoneTransforms = NULL;
	XMFLOAT4X4						*mappedSkinningBoneTransforms = NULL;

public:
	void PrepareSkinning(GameObject *ModelRootObject);
	void LoadSkinInfoFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, FILE *pInFile);

	void ComputeSkinning(ID3D12GraphicsCommandList* commandList, UINT instanceNum, ID3D12Resource* buffers[4]);

	virtual void CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList *commandList, ID3D12Resource* buffers[4]);
	virtual void ReleaseShaderVariables();

	virtual void ReleaseUploadBuffers();

	virtual void OnPreRender(ID3D12GraphicsCommandList *commandList, UINT instancesNum, ID3D12Resource* buffers[4]);
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subSet, ID3D12Resource* buffers[4]);
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingbufferViews, ID3D12Resource* buffers[4]);
		

};