#include "UIShader.h"
#include "Scene.h"
#include "Player.h"

void UIShader::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	objectBuffer->cbGameObjects = new ID3D12Resource*[1];
	objectBuffer->cbMappedGameObjects = new CB_GAMEOBJECT_INFO*[1];
	objectBuffer->cbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수

	objectBuffer->cbGameObjects[0] = ::CreateBufferResource(device, commandList, NULL, objectBuffer->cbGameObjectBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	objectBuffer->cbGameObjects[0]->Map(0, NULL, (void **)&objectBuffer->cbMappedGameObjects[0]);
}

void UIShader::ReleaseObjects()
{
	if (objectBuffer->objects[0])
		objectBuffer->objects[0]->ReleaseShaderVariables();
}

void UIShader::ReleaseUploadBuffer()
{
	if (objectBuffer->objects[0])
		objectBuffer->objects[0]->ReleaseUploadBuffers();
}

void UIShader::ReleaseShaderVariables()
{
	if (objectBuffer->cbGameObjects[0]) {
		objectBuffer->cbGameObjects[0]->Unmap(0, NULL);
		objectBuffer->cbGameObjects[0]->Release();
	}
}

void UIShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, void * context)
{
	objectBuffer = new ObjectBuffer;

	objectBuffer->objectsNum = 1;
	objectBuffer->objects.reserve(objectBuffer->objectsNum);

	objectBuffer->framesNum = 1;

	CreateShaderVariables(device, commandList);
	D3D12_GPU_DESCRIPTOR_HANDLE handle = Scene::CreateConstantBufferViews(device, commandList, objectBuffer->objectsNum, objectBuffer->cbGameObjects[0], objectBuffer->cbGameObjectBytes);

	GameObject *uiObject = NULL;
	uiObject = new GameObject();
	uiObject->SetPosition(0, 0, 0);
	uiObject->CreateCbvGPUDescriptorHandles();
	uiObject->SetCbvGPUDescriptorHandlePtr(handle.ptr);

	UiMesh* uiMesh = new UiMesh(device, commandList);

	uiObject->SetMesh(0, uiMesh);

	objectBuffer->objects.emplace_back(uiObject);
}


D3D12_INPUT_LAYOUT_DESC UIShader::CreateInputLayout()
{
	UINT nInputElementDescs = 3;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "TEXINFO", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE UIShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"UI.hlsl", "VSUiTextured", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE UIShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"UI.hlsl", "PSUiTextured", "ps_5_1", &pixelShaderBlob));
}

D3D12_BLEND_DESC UIShader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = TRUE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(blendDesc);
}

void UIShader::Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx)
{
	if (!isShadow) {
		Shader::OnPrepareRender(commandList, 0, isShadow);
		Shader::UpdateShaderVariables(commandList);
		UpdateShaderVariables(commandList);

		objectBuffer->objects[0]->Render(commandList);
	}
}

MiniMapShader::MiniMapShader()
{
}

MiniMapShader::~MiniMapShader()
{
}

void MiniMapShader::CreateShader(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 2;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	Shader::CreateShader(device, graphicsRootSignature);

	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[2];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pipelineStateDesc.InputLayout.pInputElementDescs = pd3dInputElementDescs;
	pipelineStateDesc.InputLayout.NumElements = 2;

	pipelineStateDesc.VS = Shader::CompileShaderFromFile(L"UI.hlsl", "VSMiniMapBG", "vs_5_1", &vertexShaderBlob);
	pipelineStateDesc.PS = Shader::CompileShaderFromFile(L"UI.hlsl", "PSMiniMapBG", "ps_5_1", &pixelShaderBlob);
	D3D12_SHADER_BYTECODE shaderByteCode;
	shaderByteCode.BytecodeLength = 0;
	shaderByteCode.pShaderBytecode = NULL;

	pipelineStateDesc.GS = shaderByteCode;

	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;

	pipelineStateDesc.BlendState.AlphaToCoverageEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].BlendEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;

	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[1]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (geometryShaderBlob) geometryShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}

D3D12_INPUT_LAYOUT_DESC MiniMapShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE MiniMapShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"UI.hlsl", "VSMiniMap", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE MiniMapShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"UI.hlsl", "PSMiniMap", "ps_5_1", &pixelShaderBlob));
}

D3D12_SHADER_BYTECODE MiniMapShader::CreateGeometryShader()
{
	return(Shader::CompileShaderFromFile(L"UI.hlsl", "GSMiniMap", "gs_5_1", &geometryShaderBlob));
}

D3D12_PRIMITIVE_TOPOLOGY_TYPE MiniMapShader::SetTopologyType()
{
	return D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
}

void MiniMapShader::CreateShaderVariables(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx)
{
	buffer = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_MiniMap) * BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	buffer->Map(0, NULL, (void**)&mappedBuffer);

	vertexBufferView.BufferLocation = buffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = sizeof(VS_VB_MiniMap);
	vertexBufferView.SizeInBytes = sizeof(VS_VB_MiniMap) * BUFFERSIZE;

	mappedBuffer[0] = VS_VB_MiniMap(XMFLOAT2(50, 50), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f));
	// 이 값은 변하지 않는다.
	bufferSize = 1;

	XMFLOAT4 color = XMFLOAT4(0.25f, 0.25f, 0.25f, 0.6f);
	vertex = new VS_VB_MiniMap[6];
	vertex[0] = VS_VB_MiniMap(XMFLOAT2(0, 0), color);
	vertex[1] = VS_VB_MiniMap(XMFLOAT2(0, 200), color);
	vertex[2] = VS_VB_MiniMap(XMFLOAT2(200, 0), color);
	vertex[3] = VS_VB_MiniMap(XMFLOAT2(0, 200), color);
	vertex[4] = VS_VB_MiniMap(XMFLOAT2(200, 200), color);
	vertex[5] = VS_VB_MiniMap(XMFLOAT2(200, 0), color);

	bgBuffer = ::CreateBufferResource(device, commandList, vertex, sizeof(VS_VB_MiniMap) * 6, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &uploadBgBuffer);

	bgVertexBufferView.BufferLocation = bgBuffer->GetGPUVirtualAddress();
	bgVertexBufferView.StrideInBytes = sizeof(VS_VB_MiniMap);
	bgVertexBufferView.SizeInBytes = sizeof(VS_VB_MiniMap) * 6;
}

void MiniMapShader::UpdateShaderVariables(ID3D12GraphicsCommandList * commandList)
{
	for (int i = 1; i < bufferSize; ++i)
	{
		mappedBuffer[i] = info[i - 1];
	}
}

void MiniMapShader::ReleaseShaderVariables()
{
}

void MiniMapShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, void * context)
{
	info = new VS_VB_MiniMap[BUFFERSIZE - 1];

	CreateShaderVariables(device, commandList);
}

void MiniMapShader::Animate()
{
	if (player->GetCamera()->GetMode() == SPACESHIP_CAMERA || player->GetCamera()->GetMode() == ZOOM_MODE_CAMERA) return;

	XMFLOAT3 playerPos = player->GetPosition();
	XMFLOAT3 gab = Vector3::Subtract(center, playerPos);

	std::vector<SkinnedInstanceBuffer*> bufs = skinInsShader->GetBuffer();

	int idx = 0;
	for (int k = 0; k < bufs.size(); ++k)
	{
		std::vector<GameObject*> objs = bufs[k]->objects;
		for (int j = 1; j < objs.size(); ++j)
		{
			if (objs[j]->isLive == true) 
			{
				XMFLOAT3 pos = objs[j]->GetPosition();
				if (IsShowObject(playerPos, pos))
				{
					XMFLOAT3 transformPos = Vector3::Add(gab, pos);
					info[idx].position = XMFLOAT2(transformPos.x, transformPos.z);
					info[idx++].color = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
				}
			}
		}		
	}

	std::vector<ParticleInfoInCPU> particles = particleShader->GetParticles();

	for (int k = 0; k < particles.size(); ++k)
	{
		if (particles[k].type == PARTICLE_TYPE_TORNADO && particleShader->GetIsRender()[k] == true)
		{
			XMFLOAT3 pos = particles[k].position;
			if (IsShowObject(playerPos, pos))
			{
				XMFLOAT3 transformPos = Vector3::Add(gab, pos);
				info[idx].position = XMFLOAT2(transformPos.x, transformPos.z);
				info[idx++].color = XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f);
			}
		}
	}

	bufferSize = idx + 1;
}

bool MiniMapShader::IsShowObject(XMFLOAT3 playerPos, XMFLOAT3 objectPos)
{
	XMFLOAT3 subPos = Vector3::Subtract(playerPos, objectPos);
	if (abs(subPos.x) > 50) return false;
	if (abs(subPos.z) > 50) return false;
	return true;
}

void MiniMapShader::SetObject(Player * player, SkinnedInstancingShader* skinInsShader, ParticleShader* particleShader)
{
	this->player = player;
	this->skinInsShader = skinInsShader;
	this->particleShader = particleShader;
}

void MiniMapShader::OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx, bool isShadow)
{
	if (Shader::curPipelineState != pipelineStates[idx]) {
		commandList->SetPipelineState(pipelineStates[idx]);
		Shader::curPipelineState = pipelineStates[idx];
	}
}

void MiniMapShader::Render(ID3D12GraphicsCommandList * commandList, bool isShadow, UINT cameraIdx)
{
	if (isShadow == true) return;

	if (player->GetCamera()->GetMode() == SPACESHIP_CAMERA || player->GetCamera()->GetMode() == ZOOM_MODE_CAMERA) return;

	UpdateShaderVariables(commandList);
	
	OnPrepareRender(commandList, 0, isShadow);

	commandList->IASetVertexBuffers(0, 1, &vertexBufferView);
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
	
	commandList->DrawInstanced(1, bufferSize, 0, 0);

	OnPrepareRender(commandList, 1, isShadow);

	commandList->IASetVertexBuffers(0, 1, &bgVertexBufferView);
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

	commandList->DrawInstanced(6, 1, 0, 0);
}
