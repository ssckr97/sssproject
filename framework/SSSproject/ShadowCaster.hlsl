#include "Shaders.hlsli"

/////////////////////////////////////////////////////////////////////////////
// CascadeShadowMap

struct VS_CSM_STANDARD_IN
{
    float3 position : POSITION;

};

struct VS_CSM_STANDARD_INSTANCE_IN
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    
    float4x4 transform : WORLDMATRIX;
};

struct VS_CSM_STANDARD_SKINNED_IN
{
    float3 position : POSITION;
    
    uint vertexID : SV_VertexID;
};

struct VS_CSM_INSTANCING_SKINNED_IN
{
    float3 position : POSITION;
    
    uint objIdx : OBJIDX;
    uint vertexID : SV_VertexID;
};

struct PS_CSM_INPUT
{
    float4 posW : SV_POSITION;
    float2 uv : TEXCOORD;
};

PS_CSM_INPUT VSCSMTerrain(VS_CSM_STANDARD_IN input)
{
    PS_CSM_INPUT output;
    output.posW = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    return output;
}

PS_CSM_INPUT VSCSMStandard(VS_CSM_STANDARD_IN input)
{
    PS_CSM_INPUT output;
    output.posW = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    return output;
}

PS_CSM_INPUT VSCSMStandardInstancing(VS_CSM_STANDARD_INSTANCE_IN input)
{
    PS_CSM_INPUT output;
    output.posW = mul(mul(mul(float4(input.position, 1.0f), input.transform), gmtxView), gmtxProjection);
    output.uv = input.uv;
    return output;
}

PS_CSM_INPUT VSCSMSkinnedStandard(VS_CSM_STANDARD_SKINNED_IN input)
{
    PS_CSM_INPUT output;
    output.posW = mul(mul(float4(gsbSkinnedPosition[input.vertexID], 1.0f), gmtxView), gmtxProjection);
    return output;
}

PS_CSM_INPUT VSCSMSkinnedInstancing(VS_CSM_INSTANCING_SKINNED_IN input)
{
    PS_CSM_INPUT output;
    output.posW = mul(mul(float4(gsbSkinnedPosition[input.objIdx * gnVerticesNum + input.vertexID], 1.0f), gmtxView), gmtxProjection);
    return output;
}

float4 PSCSMStandardInstancing(PS_CSM_INPUT input) : SV_TARGET
{
    uint textureIdx = gnTextureIdx;
    uint uTextureIdx[5];
    
    uTextureIdx[0] = textureIdx % 100;
    textureIdx = textureIdx / 100;
    
    float4 cDiffuseColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_DIFFUSE_MAP)
        cDiffuseColor = gtxtDiffuseTexture[uTextureIdx[0]].Sample(gssWrap, input.uv);
    
    return cDiffuseColor;
}