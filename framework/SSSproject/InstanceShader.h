#pragma once
#include "Shader.h"

class WeaponTrailerShader;
class ParticleShader;

struct InstanceBuffer
{
	std::vector<GameObject*> objects;
	int objectsNum = 0;
	bool useShadow = true;

	int useBillboard = -1;
	// 빌보드 사각형의 idx정보

	int framesNum;

	VS_VB_INSTANCE_OBJECT** worldCashe = NULL;
	bool** isVisable = NULL;
	int renderObjNum[CAMERAS_NUM]{};
	// [][] : Objects, Frames
	// FrustumCulling을 위한 데이터

	ID3D12Resource*** vbGameObjects = NULL;
	VS_VB_INSTANCE_OBJECT*** vbMappedGameObjects = NULL;
	// [][][] : Camera, Frames, Objects
	D3D12_VERTEX_BUFFER_VIEW** instancingGameObjectBufferView = NULL;
	UINT vbGameObjectBytes = 0;

	std::vector<ID3D12Resource*> vbBounding;
	std::vector<VS_VB_INSTANCE_BOUNDING*> vbMappedBounding;
	// [][] : Frames, Objects
	std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBoundingBufferView;
	UINT vbBoundingBytes = 0;

	UINT pipeLineStateIdx = 0;
	UINT shadowPipeLineStateIdx = 0;

	TEXTURENAME textureName;
	int diffuseIdx = -1;
	int normalIdx = -1;
	int specularIdx = -1;
	int metallicIdx = -1;
	// Scene::rootArgument의 idx 값.

	BoundingOrientedBox obb;
};

struct bImform {
	int size;
	std::vector<XMFLOAT4X4> bworld;
	std::vector<XMFLOAT3> tscale;
	std::vector<XMFLOAT3> trotate;
	std::vector<XMFLOAT3> tpos;
};

struct SkinnedInstanceBuffer
{
	std::vector<GameObject*> objects;
	bool isZombie = false;
	bool isBoss = false;
	int objectsNum = 0;

	int skinnedMeshNum = 0;
	int standardMeshNum = 0;
	int boundingMeshNum = 0;
	float hp = 100.0f;
	float strikingPower = 10.0f;
	float defencivePower = 10.0f;

	SkinnedMesh** skinnedMeshes = NULL;

	bool** isVisable = NULL;
	int renderObjNum[CAMERAS_NUM]{};

	ID3D12Resource** skinnedBuffer = NULL;
	XMFLOAT4X4** mappedSkinnedBuffer = NULL;	//[][][] skinNum, objectNum, bone

	ID3D12Resource*** standardBuffer = NULL;
	VS_VB_INSTANCE_OBJECT*** mappedStandardBuffer = NULL;	//[][][] standardNum, objectNum
	D3D12_VERTEX_BUFFER_VIEW** instancingStandardBufferView = NULL;

	VS_VB_INSTANCE_OBJECT** casheStandardBuffer = NULL;

	ID3D12Resource** objIdxBuffer = NULL;
	UINT** mappedObjIdxBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW* instancingObjIdxBufferView;

	XMFLOAT4X4** casheBoundingBuffer = NULL;

	ID3D12Resource** boundingBuffer = NULL;
	VS_VB_INSTANCE_BOUNDING** mappedBoundingBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW* instancingBoundingBufferView;

	UINT pipeLineStateIdx = 0;
	UINT shadowPipeLineStateIdx = 0;

	TEXTURENAME textureName{};
	int diffuseIdx = -1;
	int normalIdx = -1;
	int specularIdx = -1;
	int metallicIdx = -1;

	BoundingOrientedBox obb;

	ID3D12Resource** positionW = NULL;
	ID3D12Resource** normalW = NULL;
	ID3D12Resource** tangentW = NULL;
	ID3D12Resource** biTangentW = NULL;

};

class InstancingShader : public StandardInstanceShader
{
protected:

	WeaponTrailerShader* wtShader;

	std::vector<GameObject*> objects;
	// terrain, helicoptor, tree ... 오브젝트 데이터를 관리하는 벡터.
	std::vector<InstanceBuffer*> instanceBuffer;
	// 오브젝트들과 오브젝트들의 버퍼 등을 관리하는 벡터.
	std::vector<Texture*> textures;

	ID3DBlob*			geometryShaderBlob = NULL;
	bool showBounding = false;

public:

	InstancingShader();
	virtual ~InstancingShader();

	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx = 0);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, UINT cameraIdx);
	virtual void ReleaseShaderVariables();

	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void Animate();

	void SetWTShader(WeaponTrailerShader* wtShader);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context, int hObjectSize);
	virtual void ReleaseObjects();
	virtual void ReleaseUploadBuffers();
	void BuildInstanceObject(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
		InstanceBuffer * instanceObject, int objectNum, int pipeLineStateIdx, GameObject * objectModel, int modelNumber, XMFLOAT3 boundingSize, XMFLOAT3 rotate, XMFLOAT3 position);

	std::vector<InstanceBuffer*> GetBuffer() { return instanceBuffer; }

	void SetTextures(ID3D12Device* deivce, ID3D12GraphicsCommandList* commandList);

	virtual void DeleteAllObjcet();
	virtual void LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, int objectType);
	virtual void SetVelocityTheta(XMFLOAT3 velocity, float theta);

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx, bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList* commandList, bool isShadow, UINT cameraIdx);

	virtual void FrustumCulling(void* frustum, bool isShadow, UINT cameraIdx);
	
	virtual void ApplyLOD(Camera* camera, int idx);

	void LoadBoundingBoxInfo();

	void SetShowBounding(bool showBounding) { this->showBounding = showBounding; }

};


class SkinnedInstancingShader :public StandardSkinnedAnimationInstanceShader
{
protected:
	std::vector<SkinnedInstanceBuffer*> instanceBuffer;
	std::vector<LoadedModelInfo*> instanceModels;
	std::vector<Texture*> textures;

	bool showBounding = false;

	ParticleShader* particleShader;


public:
	bool allKill = false;
	bool victory = false;
	bool activePotal = false;

	int middleBossIdx;
	int finalBossIdx;

	SkinnedInstancingShader();
	virtual ~SkinnedInstancingShader();
	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context, int hObjectSize = 0);
	void BuildNewSkinedObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, LoadedModelInfo* context, XMFLOAT3 boundingSize , bool isZombie, bool isBoss);
	virtual void DeleteAllObjcet();
	virtual void LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType);

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx);
	virtual void UpdateShaderVariable(int idx, int objIdx);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, UINT cameraIdx);

	virtual void ReleaseShaderVariables();

	virtual void ReleaseObjects();
	virtual void ReleaseUploadBuffers();

	std::vector<SkinnedInstanceBuffer*>& GetBuffer() { return instanceBuffer; }
	void SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);

	void SetParticleShader(ParticleShader* particleShader);
	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void Animate();
	virtual void OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx, bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx);

	virtual void PrepareCompute(ID3D12GraphicsCommandList* commandList);

	virtual void FrustumCulling(void* frustum, bool isShadow, UINT cameraIdx);

	void SetShowBounding(bool showBounding) { this->showBounding = showBounding; }
	
};