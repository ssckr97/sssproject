#include "WeaponTrailerShader.h"

WeaponTrailerShader::WeaponTrailerShader()
{
}

WeaponTrailerShader::~WeaponTrailerShader()
{
}

D3D12_INPUT_LAYOUT_DESC WeaponTrailerShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 5;
	D3D12_INPUT_ELEMENT_DESC *inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[2] = { "TEXTUREIDX", 0, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[3] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[4] = { "AGE", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };


	D3D12_INPUT_LAYOUT_DESC inputLayout;
	inputLayout.NumElements = inputElementDescsNum;
	inputLayout.pInputElementDescs = inputElementDescs;
	return inputLayout;
}

D3D12_DEPTH_STENCIL_DESC WeaponTrailerShader::CreateDepthStencilState()
{

	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = TRUE;
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return(depthStencilDesc);
}

D3D12_RASTERIZER_DESC WeaponTrailerShader::CreateRasterizerState()
{
	D3D12_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(rasterizerDesc);
}

D3D12_BLEND_DESC WeaponTrailerShader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;

	blendDesc.AlphaToCoverageEnable = FALSE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;

	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	UINT colorMask = D3D12_COLOR_WRITE_ENABLE_RED + D3D12_COLOR_WRITE_ENABLE_GREEN + D3D12_COLOR_WRITE_ENABLE_BLUE;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = colorMask;

	return blendDesc;
}

D3D12_SHADER_BYTECODE WeaponTrailerShader::CreateVertexShader()
{
	return Shader::CompileShaderFromFile(L"WeaponTrail.hlsl", "VSWeaponTrail", "vs_5_1", &vertexShaderBlob);
}

D3D12_SHADER_BYTECODE WeaponTrailerShader::CreatePixelShader()
{
	return Shader::CompileShaderFromFile(L"WeaponTrail.hlsl", "PSWeaponTrail", "ps_5_1", &pixelShaderBlob);
}

void WeaponTrailerShader::CreateShader(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	Shader::CreateShader(device, graphicsRootSignature);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}

void WeaponTrailerShader::CreateShaderVariables(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx)
{
	buffer = new ID3D12Resource*[BUFFERSIZE];
	mappedBuffer = new VS_VB_INSTANCE_TRAIL*[BUFFERSIZE];

	bufferView = new D3D12_VERTEX_BUFFER_VIEW[BUFFERSIZE];
	info = new WeaponTrailInfo*[BUFFERSIZE];

	for (int i = 0; i < BUFFERSIZE; ++i) {
		buffer[i] = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTANCE_TRAIL) * 6, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		buffer[i]->Map(0, NULL, (void**)&mappedBuffer[i]);

		bufferView[i].BufferLocation = buffer[i]->GetGPUVirtualAddress();
		bufferView[i].StrideInBytes = sizeof(VS_VB_INSTANCE_TRAIL);
		bufferView[i].SizeInBytes = sizeof(VS_VB_INSTANCE_TRAIL) * 6;
		info[i] = new WeaponTrailInfo[6];

	}
}

void WeaponTrailerShader::UpdateShaderVariables(int idx)
{
	for (int i = 0; i < 6; ++i) {
		mappedBuffer[idx][i].position = info[idx][i].position;
		mappedBuffer[idx][i].uv = info[idx][i].uv;
		mappedBuffer[idx][i].textureIdx = info[idx][i].textureIdx;
		mappedBuffer[idx][i].color = info[idx][i].color;
		mappedBuffer[idx][i].age = info[idx][i].age;
	}
}

void WeaponTrailerShader::ReleaseShaderVariables()
{
	if (buffer) {
		for (int i = 0; i < BUFFERSIZE; ++i) {
			buffer[i]->Unmap(0, NULL);
			buffer[i]->Release();
		}
	}
}

void WeaponTrailerShader::ReleaseObjects()
{
	if (info) {
		for (int i = 0; i < BUFFERSIZE; ++i) {
			delete[] info[i];
		}
		delete[] info;
	}

}

void WeaponTrailerShader::AddBuffer(XMFLOAT3 start0, XMFLOAT3 end0, XMFLOAT3 start1, XMFLOAT3 end1, XMFLOAT4 color, float age, UINT textureIdx)
{
	info[currentIdx][0].position = end0;
	info[currentIdx][0].uv = XMFLOAT2(0.0f, 0.0f);
	info[currentIdx][1].position = end1;
	info[currentIdx][1].uv = XMFLOAT2(1.0f, 0.0f);
	info[currentIdx][2].position = start1;
	info[currentIdx][2].uv = XMFLOAT2(1.0f, 1.0f);

	info[currentIdx][3].position = end0;
	info[currentIdx][3].uv = XMFLOAT2(0.0f, 0.0f);
	info[currentIdx][4].position = start1;
	info[currentIdx][4].uv = XMFLOAT2(1.0f, 1.0f);
	info[currentIdx][5].position = start0;
	info[currentIdx][5].uv = XMFLOAT2(0.0f, 1.0f);

	for (int i = 0; i < 6; ++i)
	{
		info[currentIdx][i].color = color;
		info[currentIdx][i].age = age;
		info[currentIdx][i].textureIdx = textureIdx;
		info[currentIdx][i].isActive = true;
	}
	renderBuffer.emplace_back(currentIdx);
	currentIdx = (currentIdx + 1) % BUFFERSIZE;
}

void WeaponTrailerShader::AnimateObjects(float timeElapsed, Camera * camera)
{
	for (int i=0; i< renderBuffer.size(); ++i)
	{
		for (int j = 0; j < 6; ++j) {
			info[renderBuffer[i]][j].age -= timeElapsed;
			if (info[renderBuffer[i]][j].age <= 0)
			{
				info[renderBuffer[i]][j].isActive = false;
			}
			else
				UpdateShaderVariables(renderBuffer[i]);
		}
	}

	for (int i = 0; i < renderBuffer.size(); ++i)
	{
		if (info[renderBuffer[i]][0].isActive == false) {
			int temp = renderBuffer[i];
			renderBuffer[i] = renderBuffer.back();
			renderBuffer.back() = temp;
			renderBuffer.pop_back();
		}
	}

}

void WeaponTrailerShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, void* context)
{
	renderBuffer.reserve(BUFFERSIZE);

	CreateShaderVariables(device, commandList);

}

void WeaponTrailerShader::Render(ID3D12GraphicsCommandList * commandList, bool isShadow, UINT cameraIdx)
{
	if (!isShadow) 
	{
		for (int i=0; i<renderBuffer.size(); ++i)
		{
			if (Shader::curPipelineState != pipelineStates[0]) {
				commandList->SetPipelineState(pipelineStates[0]);
				Shader::curPipelineState = pipelineStates[0];
			}

		
			commandList->IASetVertexBuffers(0, 1, &bufferView[renderBuffer[i]]);
			commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST);

			commandList->DrawInstanced(6, 1, 0, 0);
		}
	}
}