#pragma once
#include "Shader.h"

class RenderInfo
{
private:
	ID3D12CommandAllocator* shadowAllocator[COMMAND_IDX_NUM];
	ID3D12GraphicsCommandList* shadowCommandList[COMMAND_IDX_NUM];
	ID3D12CommandAllocator* renderAllocator[COMMAND_IDX_NUM];
	ID3D12GraphicsCommandList* renderCommandList[COMMAND_IDX_NUM];
	std::vector<Shader*> shaders;
	Player* player = NULL;

	BoundingFrustum frustum;
	BoundingOrientedBox shadowFrustum[MAX_CASCADE_SIZE];
	Camera* lightCamera[MAX_CASCADE_SIZE];
	Camera* camera = NULL;

	D3D12_CPU_DESCRIPTOR_HANDLE renderTargetHandle[2];
	D3D12_CPU_DESCRIPTOR_HANDLE depthBufferHandle[COMMAND_IDX_NUM];
	D3D12_CPU_DESCRIPTOR_HANDLE shadowBufferHandle[COMMAND_IDX_NUM];

public:
	RenderInfo(ID3D12Device* commandList);
	~RenderInfo();

	void SetCamera(Camera* camera, Camera* lightCamera[MAX_CASCADE_SIZE]);
	void SetHandles(D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle, D3D12_CPU_DESCRIPTOR_HANDLE depthHandle, D3D12_CPU_DESCRIPTOR_HANDLE shadowHandle, int dx);
	void SetShader(Shader* shader);
	void SetPlayer(Player* player);

	ID3D12CommandAllocator* GetShadowAllocator(int idx);
	ID3D12GraphicsCommandList* GetShadowCommandList(int idx);
	ID3D12CommandAllocator* GetRenderAllocator(int idx);
	ID3D12GraphicsCommandList* GetRenderCommandList(int idx);
	std::vector<Shader*> GetShaders();
	Player* GetPlayer();
	Camera* GetCamera();
	Camera* GetLightCamera(int idx);
	BoundingFrustum GetFrustum();
	BoundingOrientedBox GetShadowFrustum(int idx);
	D3D12_CPU_DESCRIPTOR_HANDLE GetRenderTargetHandle(int idx);
	D3D12_CPU_DESCRIPTOR_HANDLE GetDepthBufferHandle(int idx);
	D3D12_CPU_DESCRIPTOR_HANDLE GetShadowBufferHandle(int idx);
};

class ComputeInfo
{
private:

	ID3D12CommandAllocator* allocator[COMMAND_IDX_NUM];
	ID3D12GraphicsCommandList* commandList[COMMAND_IDX_NUM];

	std::vector<Shader*> shaders;

	ID3D12Resource* renderTargetBuffer[2];
	D3D12_CPU_DESCRIPTOR_HANDLE renderTargetHandle[2];
	D3D12_CPU_DESCRIPTOR_HANDLE depthBufferHandle[COMMAND_IDX_NUM];
	D3D12_CPU_DESCRIPTOR_HANDLE shadowBufferHandle[COMMAND_IDX_NUM];

public:
	ComputeInfo(ID3D12Device* device);
	~ComputeInfo();


	ID3D12CommandAllocator* GetAllocator(int idx);
	ID3D12GraphicsCommandList* GetCommandList(int idx);

	void SetHandles(D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle, D3D12_CPU_DESCRIPTOR_HANDLE depthHandle, D3D12_CPU_DESCRIPTOR_HANDLE shadowHandle, int idx);
	void SetRtBuffer(ID3D12Resource* renderTargetBuffers, int idx);

	D3D12_CPU_DESCRIPTOR_HANDLE GetRenderTargetHandle(int idx);
	D3D12_CPU_DESCRIPTOR_HANDLE GetDepthBufferHandle(int idx);
	D3D12_CPU_DESCRIPTOR_HANDLE GetShadowBufferHandle(int idx);
	ID3D12Resource* GetRtBuffer(int idx);

};