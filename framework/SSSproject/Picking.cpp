#include "Picking.h"

bool Picking::CheckPicking(POINT& cursur, XMFLOAT3 rayOrigin, XMFLOAT3 rayDirection, int windowClientWidth, int windowClientHeight, BoundingOrientedBox obb, XMFLOAT3& pickPlace, float& distance)
{
	XMVECTOR tempOrigin = XMLoadFloat3(&rayOrigin);
	XMVECTOR tempDirection = XMLoadFloat3(&rayDirection);

	float hitDistance = FLT_MAX;
	BoundingOrientedBox tempObb = obb;
	if (tempObb.Intersects(tempOrigin, tempDirection, hitDistance))
	{
		if (hitDistance < distance)
			distance = hitDistance;
		pickPlace = Vector3::Add(rayOrigin, Vector3::ScalarProduct(rayDirection, distance));
		return true;
	}
	return false;
}

bool Picking::CheckPicking(POINT& cursur, XMFLOAT3 rayOrigin, XMFLOAT3 rayDirection, int windowClientWidth, int windowClientHeight, BoundingBox obb, XMFLOAT3& pickPlace, float& distance)
{
	XMVECTOR tempOrigin = XMLoadFloat3(&rayOrigin);
	XMVECTOR tempDirection = XMLoadFloat3(&rayDirection);

	float hitDistance = FLT_MAX;
	BoundingBox tempObb = obb;
	if (tempObb.Intersects(tempOrigin, tempDirection, hitDistance))
	{
		if (hitDistance < distance)
			distance = hitDistance;
		pickPlace = Vector3::Add(rayOrigin, Vector3::ScalarProduct(rayDirection, distance));
		return true;
	}
	return false;
}
