// MAX ShadowMap

float GetShadowFactor(float4 shadowPosition[3], float4 positionW)
{
    float fShadowFactor = 1.0f, fsDepth = 1.0f, fShadowOffset = 0;
    int idx = -1;
    
    float toCamLen = distance(positionW.xyz, gvCameraPosition.xyz);
 
    [unroll]
    for (int i = 0; i < int(gfCasacdeNum); ++i)
    {
        if (toCamLen <= gfCascadeLength[i].x)
        {
            idx = i;
            break;
        }
    }
    if (idx == -1)
        fShadowFactor = 1.0f;
    else if ((saturate(shadowPosition[idx].x) == shadowPosition[idx].x) &&
        (saturate(shadowPosition[idx].y) == shadowPosition[idx].y) &&
        (saturate(shadowPosition[idx].z) == shadowPosition[idx].z))
    {
        fShadowOffset = idx / gfCasacdeNum;
       
        float depth = shadowPosition[idx].z;
        float2 shadowUV = shadowPosition[idx].xy;
        shadowUV.x /= gfCasacdeNum;
        shadowUV.x += fShadowOffset;
        
        const float Dilation = 2.0 / gfCascadeLength[idx].x / 1.5f;
        float d1 = Dilation * gfShadowTexelSize * 0.125;
        float d2 = Dilation * gfShadowTexelSize * 0.875;
        float d3 = Dilation * gfShadowTexelSize * 0.625;
        float d4 = Dilation * gfShadowTexelSize * 0.375;
        float result = (
       2.0 * gtxtShadowMapTexture[gnSwapchainIdx].SampleCmpLevelZero(gssCmpShadowClamp, shadowUV, depth) +
       gtxtShadowMapTexture[gnSwapchainIdx].SampleCmpLevelZero(gssCmpShadowClamp, shadowUV + float2(-d2, d1), depth) +
       gtxtShadowMapTexture[gnSwapchainIdx].SampleCmpLevelZero(gssCmpShadowClamp, shadowUV + float2(-d1, -d2), depth) +
       gtxtShadowMapTexture[gnSwapchainIdx].SampleCmpLevelZero(gssCmpShadowClamp, shadowUV + float2(d2, -d1), depth) +
       gtxtShadowMapTexture[gnSwapchainIdx].SampleCmpLevelZero(gssCmpShadowClamp, shadowUV + float2(d1, d2), depth) +
                                                                            
       gtxtShadowMapTexture[gnSwapchainIdx].SampleCmpLevelZero(gssCmpShadowClamp, shadowUV + float2(-d4, d3), depth) +
       gtxtShadowMapTexture[gnSwapchainIdx].SampleCmpLevelZero(gssCmpShadowClamp, shadowUV + float2(-d3, -d4), depth) +
       gtxtShadowMapTexture[gnSwapchainIdx].SampleCmpLevelZero(gssCmpShadowClamp, shadowUV + float2(d4, -d3), depth) +
       gtxtShadowMapTexture[gnSwapchainIdx].SampleCmpLevelZero(gssCmpShadowClamp, shadowUV + float2(d3, d4), depth)
       ) / 14.0f;
       
        result = result * result;
        fShadowFactor = (1 - result);
    }
    return fShadowFactor;
}