#include "Shaders.hlsli"

//////////////////////////////////////////////////////////////////////////////////////////////
// BillboardTexture(GS) Shader Code

struct VS_BILLBOARD_OUTPUT
{
    float3 centerW : POSITION;
    float2 size : SIZE;
    uint textureIdx : TEXTUREIDX;
};

struct VS_BILLBOARD_INPUT
{
    float3 positionW : POSITION;
    float3 billboardInfo : BILLBOARDINFO; //(width, height, textureIdx)
};

struct GS_BILLBOARD_OUTPUT
{
    float4 posH : SV_POSITION;
    float3 posW : POSITION;
    float3 normalW : NORMAL;
    float2 uv : TEXCOORD;
    uint primID : SV_PrimitiveID;
};



VS_BILLBOARD_OUTPUT VSBillboard(VS_BILLBOARD_INPUT input)
{
    VS_BILLBOARD_OUTPUT output;

    output.centerW = input.positionW;

    output.size.xy = input.billboardInfo.xy;
    output.textureIdx = (uint) input.billboardInfo.z;

    return (output);
}

[maxvertexcount(4)]
void GSBillboard(point VS_BILLBOARD_OUTPUT input[1], uint primID : SV_PrimitiveID, inout TriangleStream<GS_BILLBOARD_OUTPUT> outStream)
{
    float3 vUp = float3(0.0f, 1.0f, 0.0f);
    float3 vLook = gvCameraPosition.xyz - input[0].centerW;
    vLook = normalize(vLook);
    float3 vRight = cross(vUp, vLook);
    float fHalfW = input[0].size.x * 0.5f;
    float fHalfH = input[0].size.y * 0.5f;
    float4 pVertices[4];
    
    pVertices[0] = float4(input[0].centerW + fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[1] = float4(input[0].centerW + fHalfW * vRight + fHalfH * vUp, 1.0f);
    pVertices[2] = float4(input[0].centerW - fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[3] = float4(input[0].centerW - fHalfW * vRight + fHalfH * vUp, 1.0f);

    float2 pUVs[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };
    GS_BILLBOARD_OUTPUT output;
    
    for (int j = 0; j < 4; ++j)
    {
        output.posW = pVertices[j].xyz;
        output.posH = mul(mul(pVertices[j], gmtxView), gmtxProjection);
        output.normalW = vLook;
        output.uv = pUVs[j];
        output.primID = input[0].textureIdx;
        outStream.Append(output);
    }
}

float4 PSBillboard(GS_BILLBOARD_OUTPUT input) : SV_TARGET
{
    
    float4 cColor = gtxtBillboardTexture[NonUniformResourceIndex(input.primID)].Sample(gssWrap, input.uv);
    
    return (cColor);
}