cbuffer cbTimeInfo : register(b0)
{
    float gfTotalTime : packoffset(c0.x);
    float gfTimeElapsed : packoffset(c0.y);
}

cbuffer cbParticleInfo : register(b1)
{
    int gnParticleType : packoffset(c0);
    float gfParticleAcc : packoffset(c0.y);
    uint gnParticleSize : packoffset(c0.z);
    float gfParticleAge : packoffset(c0.w);
    float gfRadius : packoffset(c1.x);
    float gfYgab : packoffset(c1.y);
    float gfRotateAcc : packoffset(c1.z);
}

cbuffer cbCameraInfo : register(b2)
{
    matrix gmtxView : packoffset(c0);
    matrix gmtxProjection : packoffset(c4);
    float3 gvCameraPosition : packoffset(c8);
};

cbuffer cbParticleSortInfo : register(b3)
{
    uint giLevel : packoffset(c0);
}

struct ParticleData
{
    float3 position;
    float3 velocity;
    float age;
};

#define BLOCKSIZE 256
#define MAX_PARTICLESIZE 1024

#define PARTICLE_TYPE_DISAPPEAR		0
#define PARTICLE_TYPE_FOREVER		1
#define PARTICLE_TYPE_TORNADO		2
#define PARTICLE_TYPE_SMALLER		3

#define PI 3.141592

groupshared float3 sharedPos[BLOCKSIZE];

StructuredBuffer<ParticleData> gsbReadParticleData : register(t0);
StructuredBuffer<ParticleData> gsbCopyParticleData : register(t1);
RWStructuredBuffer<ParticleData> gsbWriteParticleData : register(u0);