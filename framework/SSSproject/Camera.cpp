#include "stdafx.h"
#include "Camera.h"
#include "Player.h"


Camera::Camera()
{
	view = Matrix4x4::Identity();
	projection = Matrix4x4::Identity();
	viewport = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT, 0.0f, 1.0f };
	scissorRect = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT };
	position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	right = XMFLOAT3(1.0f, 0.0f, 0.0f);
	look = XMFLOAT3(0.0f, 0.0f, 1.0f);
	up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	pitch = 0.0f;
	roll = 0.0f;
	yaw = 0.0f;
	offset = XMFLOAT3(0.0f, 0.0f, 0.0f);
	timeLag = 0.0f;
	lookAtWorld = XMFLOAT3(0.0f, 0.0f, 0.0f);
	mode = 0x00;
	player = NULL;
}

Camera::~Camera()
{
}

Camera::Camera(Camera *camera)
{
	if (camera)
	{
		//카메라가 이미 있으면 기존 카메라의 정보를 새로운 카메라에 복사한다.
		*this = *camera;
	}
	else
	{
		//카메라가 없으면 기본 정보를 설정한다.
		view = Matrix4x4::Identity();
		projection = Matrix4x4::Identity();
		viewport = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT, 0.0f, 1.0f };
		scissorRect = { 0, 0, FRAME_BUFFER_WIDTH , FRAME_BUFFER_HEIGHT };
		position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		right = XMFLOAT3(1.0f, 0.0f, 0.0f);
		look = XMFLOAT3(0.0f, 0.0f, 1.0f);
		up = XMFLOAT3(0.0f, 1.0f, 0.0f);
		pitch = 0.0f;
		roll = 0.0f;
		yaw = 0.0f;
		offset = XMFLOAT3(0.0f, 0.0f, 0.0f);
		timeLag = 0.0f;
		lookAtWorld = XMFLOAT3(0.0f, 0.0f, 0.0f);
		mode = 0x00;
		player = NULL;
	}
}

void Camera::SetOrthoInfo(XMFLOAT3 center, float radius)
{
	this->orthoCenter = center;
	this->orthoRadius = radius;
}

void Camera::SetViewport(int topLeftX, int topLeftY, int width, int height, float minZ, float maxZ)
{
	viewport.TopLeftX = float(topLeftX);
	viewport.TopLeftY = float(topLeftY);
	viewport.Width = float(width);
	viewport.Height = float(height);
	viewport.MinDepth = minZ;
	viewport.MaxDepth = maxZ;
}

void Camera::SetScissorRect(LONG leftX, LONG topY, LONG rightX, LONG bottomY)
{
	scissorRect.left = leftX;
	scissorRect.top = topY;
	scissorRect.right = rightX;
	scissorRect.bottom = bottomY;
}

void Camera::GenerateProjectionMatrix(float nearPlaneDistance, float farPlaneDistance, float aspectRatio, float fovAngle)
{
	this->nearPlaneDistance = nearPlaneDistance;
	this->farPlaneDistance = farPlaneDistance;
	this->aspectRatio = aspectRatio;
	this->fovAngle = fovAngle;
	projection = Matrix4x4::PerspectiveFovLH(XMConvertToRadians(fovAngle), aspectRatio, nearPlaneDistance, farPlaneDistance);
}

void Camera::GenerateOrthograhpicsMatrix(float w, float h, float zn, float zf)
{
	projection = Matrix4x4::OrthographicLH(w, h, zn, zf);
}

void Camera::GenerateOrthograhpicsOffCenterMatrix(float l, float r, float b, float t, float zn, float zf)
{
	projection = Matrix4x4::OrthographicOffCenterLH(l, r, b, t, zn, zf);
}

void Camera::GenerateViewMatrix(XMFLOAT3 position, XMFLOAT3 lookAt, XMFLOAT3 up)
{
	this->position = position;
	lookAtWorld = lookAt;
	this->up = up;
	GenerateViewMatrix();
}

/*카메라 변환 행렬을 생성한다. 카메라의 위치 벡터, 카메라가 바라보는 지점, 카메라의 Up 벡터(로컬 y-축 벡터)를
파라메터로 사용하는 XMMatrixLookAtLH() 함수를 사용한다.*/
void Camera::GenerateViewMatrix()
{
	view = Matrix4x4::LookAtLH(position, lookAtWorld, up);
	look.x = view._13; look.y = view._23; look.z = view._33;
	up.x = view._12; up.y = view._22; up.z = view._32;
	right.x = view._11; right.y = view._21; right.z = view._31;

}

void Camera::RegenerateViewMatrix()
{
	//카메라의 z-축을 기준으로 카메라의 좌표축들이 직교하도록 카메라 변환 행렬을 갱신한다.
	//카메라의 z-축 벡터를 정규화한다.
	look = Vector3::Normalize(look);
	//카메라의 z-축과 y-축에 수직인 벡터를 x-축으로 설정한다.
	right = Vector3::CrossProduct(up, look, true);
	//카메라의 z-축과 x-축에 수직인 벡터를 y-축으로 설정한다.
	up = Vector3::CrossProduct(look, right, true);
	view._11 = right.x; view._12 = up.x; view._13 = look.x;
	view._21 = right.y; view._22 = up.y; view._23 = look.y;
	view._31 = right.z; view._32 = up.z; view._33 = look.z;
	view._41 = -Vector3::DotProduct(position, right);
	view._42 = -Vector3::DotProduct(position, up);
	view._43 = -Vector3::DotProduct(position, look);
}

void Camera::GenerateFrustum()
{
	XMFLOAT4X4 frustumProj = Matrix4x4::PerspectiveFovLH(XMConvertToRadians(fovAngle + 10), aspectRatio, nearPlaneDistance, farPlaneDistance);
	frustum.CreateFromMatrix(frustum, XMLoadFloat4x4(&frustumProj));
	XMFLOAT4X4 frustumView = view;
	frustumView._43 += 10;
	XMMATRIX viewInv = XMMatrixInverse(NULL, XMLoadFloat4x4(&frustumView));
	frustum.Transform(frustum, viewInv);
	// World 좌표계로 표현된 절두체 생성
}

void Camera::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	UINT cbElementBytes = ((sizeof(VS_CB_CAMERA_INFO) + 255) & ~255); //256의 배수
	cbCamera = ::CreateBufferResource(device, commandList, NULL, cbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

	cbCamera->Map(0, NULL, (void **)&cbMappedCamera);
}
void Camera::UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, bool isShadow)
{
	XMFLOAT4X4 view;
	XMStoreFloat4x4(&view, XMMatrixTranspose(XMLoadFloat4x4(&this->view)));
	::memcpy(&cbMappedCamera->view, &view, sizeof(XMFLOAT4X4));

	XMFLOAT4X4 projection;
	XMStoreFloat4x4(&projection, XMMatrixTranspose(XMLoadFloat4x4(&this->projection)));
	::memcpy(&cbMappedCamera->projection, &projection, sizeof(XMFLOAT4X4));

	if (!isShadow)
	{
		XMFLOAT4X4 invView;
		XMStoreFloat4x4(&invView, XMMatrixTranspose(XMLoadFloat4x4(&::Matrix4x4::Inverse(this->view))));
		::memcpy(&cbMappedCamera->invView, &invView, sizeof(XMFLOAT4X4));

		XMFLOAT4X4 invProjection;
		XMStoreFloat4x4(&invProjection, XMMatrixTranspose(XMLoadFloat4x4(&::Matrix4x4::Inverse(this->projection))));
		::memcpy(&cbMappedCamera->invProjection, &invProjection, sizeof(XMFLOAT4X4));
	}

	::memcpy(&cbMappedCamera->position, &position, sizeof(XMFLOAT3));

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = cbCamera->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootCamera, d3dGpuVirtualAddress);

	if(!isShadow)
		commandList->SetGraphicsRootConstantBufferView(ComputeRootCamera, d3dGpuVirtualAddress);

}

void Camera::ReleaseShaderVariables()
{
	if (cbCamera)
	{
		cbCamera->Unmap(0, NULL);
		cbCamera->Release();
	}
}

void Camera::SetViewportsAndScissorRects(ID3D12GraphicsCommandList* commandList)
{
	commandList->RSSetViewports(1, &viewport);
	commandList->RSSetScissorRects(1, &scissorRect);
}

void Camera::SetWorldMatrix(XMFLOAT4X4 world)
{
	right.x = world._11, right.y = world._12, right.z = world._13;
	up.x = world._21, up.y = world._22, up.z = world._23;
	look.x = world._31, look.y = world._32, look.z = world._33;
	position.x = world._41, position.y = world._42, position.z = world._43;
}

void Camera::SetProjectionMatrix(XMFLOAT4X4 projection)
{
	this->projection = projection;
}

XMFLOAT4X4 Camera::GetWorldMatrix()
{
	// Right , UP , Look
	XMFLOAT4X4 world;
	world._11 = right.x, world._12 = right.y, world._13 = right.z;
	world._21 = up.x, world._22 = up.y, world._23 = up.z;
	world._31 = look.x, world._32 = look.y, world._33 = look.z;
	world._41 = position.x, world._42 = position.y, world._43 = position.z;

	return world;
}

void Camera::SetLookAt(const XMFLOAT3& lookAt)
{
	//현재 카메라의 위치에서 플레이어를 바라보기 위한 카메라 변환 행렬을 생성한다.
	XMFLOAT4X4 tempLookAt = Matrix4x4::LookAtLH(position, lookAt, XMFLOAT3(0.0f, 1.0f, 0.0f));
	//카메라 변환 행렬에서 카메라의 x-축, y-축, z-축을 구한다.
	right = XMFLOAT3(tempLookAt._11, tempLookAt._21, tempLookAt._31);
	up = XMFLOAT3(tempLookAt._12, tempLookAt._22, tempLookAt._32);
	look = XMFLOAT3(tempLookAt._13, tempLookAt._23, tempLookAt._33);
	lookAtWorld = lookAt;
}

bool Camera::IsInFrustum(BoundingOrientedBox& boundingBox, bool isOrthograhpics)
{
	if (isOrthograhpics)
	{
		SetOrthoFrustum(boundingBox);
		return orthoFrustum.Intersects(boundingBox);
	}
	else
	{
		GenerateFrustum();
		return frustum.Intersects(boundingBox);
	}
}

void Camera::SetOrthoFrustum(BoundingOrientedBox& boundingBox)
{	
	XMFLOAT3 center = orthoCenter;
	
	XMFLOAT3 size = XMFLOAT3(orthoRadius + 10, orthoRadius + 10, orthoRadius + 10);
	orthoFrustum = BoundingOrientedBox(center, size, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	
	XMFLOAT4X4 viewInv = Matrix4x4::Inverse(view);
	orthoFrustum.Transform(orthoFrustum, XMLoadFloat4x4(&viewInv));

	// World 좌표계로 표현된 절두체 생성
}

BoundingFrustum Camera::GetBoundingFrustum()
{
	BoundingFrustum result;

	XMFLOAT4X4 frustumProj = Matrix4x4::PerspectiveFovLH(XMConvertToRadians(fovAngle + 10), aspectRatio, nearPlaneDistance, farPlaneDistance);
	result.CreateFromMatrix(result, XMLoadFloat4x4(&frustumProj));
	XMFLOAT4X4 frustumView = view;
	frustumView._43 += 10;
	XMMATRIX viewInv = XMMatrixInverse(NULL, XMLoadFloat4x4(&frustumView));
	result.Transform(result, viewInv);
	
	return result;
}

BoundingOrientedBox Camera::GetBoundingOrientedBox()
{
	BoundingOrientedBox result;

	XMFLOAT3 center = orthoCenter;
	XMFLOAT3 size = XMFLOAT3(orthoRadius + 10, orthoRadius + 10, orthoRadius + 10);
	result = BoundingOrientedBox(center, size, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));
	XMFLOAT4X4 viewInv = Matrix4x4::Inverse(view);
	result.Transform(result, XMLoadFloat4x4(&viewInv));

	return result;
}

/////////////////////////////////////////////////////////////////////////

SpaceShipCamera::SpaceShipCamera(Camera *camera) : Camera(camera)
{
	mode = SPACESHIP_CAMERA;
}

//스페이스-쉽 카메라를 플레이어의 로컬 x-축(Right), y-축(Up), z-축(Look)을 기준으로 회전하는 함수이다.
void SpaceShipCamera::Rotate(float x, float y, float z)
{
	if (player && (x != 0.0f))
	{
		//플레이어의 로컬 x-축에 대한 x 각도의 회전 행렬을 계산한다.
		XMFLOAT3 right = player->GetRightVector();
		XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&right), XMConvertToRadians(x));
		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		this->right = Vector3::TransformNormal(this->right, rotate);
		up = Vector3::TransformNormal(up, rotate);
		look = Vector3::TransformNormal(look, rotate);
		//카메라의 위치 벡터에서 플레이어의 위치 벡터를 뺀다. 결과는 플레이어 위치를 기준(원점)으로 한 카메라의 위치벡터이다.
		position = Vector3::Subtract(position, player->GetPosition());
		//플레이어의 위치를 중심으로 카메라의 위치 벡터(플레이어를 기준으로 한)를 회전한다.
		position = Vector3::TransformCoord(position, rotate);
		//회전시킨 카메라의 위치 벡터에 플레이어의 위치를 더하여 카메라의 위치 벡터를 구한다.
		position = Vector3::Add(position, player->GetPosition());
	}
	if (player && (y != 0.0f))
	{
		XMFLOAT3 up = player->GetUpVector();
		XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&up), XMConvertToRadians(y));
		right = Vector3::TransformNormal(right, rotate);
		this->up = Vector3::TransformNormal(this->up, rotate);
		look = Vector3::TransformNormal(look, rotate);
		position = Vector3::Subtract(position, player->GetPosition());
		position = Vector3::TransformCoord(position, rotate);
		position = Vector3::Add(position, player->GetPosition());
	}
	if (player && (z != 0.0f))
	{
		XMFLOAT3 look = player->GetLookVector();
		XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&look), XMConvertToRadians(z));
		right = Vector3::TransformNormal(right, rotate);
		up = Vector3::TransformNormal(up, rotate);
		this->look = Vector3::TransformNormal(this->look, rotate);
		position = Vector3::Subtract(position, player->GetPosition());
		position = Vector3::TransformCoord(position, rotate);
		position = Vector3::Add(position, player->GetPosition());
	}
}

FirstPersonCamera::FirstPersonCamera(Camera *camera) : Camera(camera)
{
	mode = FIRST_PERSON_CAMERA;
	if (camera)
	{
		/*1인칭 카메라로 변경하기 이전의 카메라가 스페이스-쉽 카메라이면 카메라의 Up 벡터를 월드좌표의 y-축이 되도록
		한다. 이것은 스페이스-쉽 카메라의 로컬 y-축 벡터가 어떤 방향이든지 1인칭 카메라(대부분 사람인 경우)의 로컬 y-
		축 벡터가 월드좌표의 y-축이 되도록 즉, 똑바로 서있는 형태로 설정한다는 의미이다. 그리고 로컬 x-축 벡터와 로컬
		z-축 벡터의 y-좌표가 0.0f가 되도록 한다. 이것은 다음 그림과 같이 로컬 x-축 벡터와 로컬 z-축 벡터를 xz-평면(지
		면)으로 투영하는 것을 의미한다. 즉, 1인칭 카메라의 로컬 x-축 벡터와 로컬 z-축 벡터는 xz-평면에 평행하다.*/
		if (camera->GetMode() == SPACESHIP_CAMERA)
		{
			up = XMFLOAT3(0.0f, 1.0f, 0.0f);
			right.y = 0.0f;
			look.y = 0.0f;
			right = Vector3::Normalize(right);
			look = Vector3::Normalize(look);
		}
	}
}

void FirstPersonCamera::Rotate(float x, float y, float z)
{
	if (x != 0.0f)
	{
		//카메라의 로컬 x-축을 기준으로 회전하는 행렬을 생성한다. 사람의 경우 고개를 끄떡이는 동작이다.
		XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&right), XMConvertToRadians(x));
		//카메라의 로컬 x-축, y-축, z-축을 회전 행렬을 사용하여 회전한다.
		look = Vector3::TransformNormal(look, rotate);
		up = Vector3::TransformNormal(up, rotate);
		right = Vector3::TransformNormal(right, rotate);
	}
	if (player && (y != 0.0f))
	{
		//플레이어의 로컬 y-축을 기준으로 회전하는 행렬을 생성한다.
		XMFLOAT3 up = player->GetUpVector();
		XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&up),
			XMConvertToRadians(y));
		//카메라의 로컬 x-축, y-축, z-축을 회전 행렬을 사용하여 회전한다.
		look = Vector3::TransformNormal(look, rotate);
		this->up = Vector3::TransformNormal(this->up, rotate);
		right = Vector3::TransformNormal(right, rotate);
	}
	if (player && (z != 0.0f))
	{
		//플레이어의 로컬 z-축을 기준으로 회전하는 행렬을 생성한다.
		XMFLOAT3 look = player->GetLookVector();
		XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&look),
			XMConvertToRadians(z));
		//카메라의 위치 벡터를 플레이어 좌표계로 표현한다(오프셋 벡터).
		position = Vector3::Subtract(position, player->GetPosition());
		//오프셋 벡터 벡터를 회전한다.
		position = Vector3::TransformCoord(position, rotate);
		//회전한 카메라의 위치를 월드 좌표계로 표현한다.
		position = Vector3::Add(position, player->GetPosition());
		//카메라의 로컬 x-축, y-축, z-축을 회전한다.
		this->look = Vector3::TransformNormal(this->look, rotate);
		up = Vector3::TransformNormal(up, rotate);
		right = Vector3::TransformNormal(right, rotate);
	}
}

void FirstPersonCamera::Update(XMFLOAT3& lookAt, float timeElapsed)
{
	//플레이어가 있으면 플레이어의 회전에 따라 3인칭 카메라도 회전해야 한다.
	if (player)
	{
		XMFLOAT4X4 rotate = Matrix4x4::Identity();
		XMFLOAT3 right = player->GetRightVector();
		XMFLOAT3 up = player->GetUpVector();
		XMFLOAT3 look = player->GetLookVector();
		//플레이어의 로컬 x-축, y-축, z-축 벡터로부터 회전 행렬(플레이어와 같은 방향을 나타내는 행렬)을 생성한다.
		rotate._11 = right.x; rotate._21 = up.x; rotate._31 = look.x;
		rotate._12 = right.y; rotate._22 = up.y; rotate._32 = look.y;
		rotate._13 = right.z; rotate._23 = up.z; rotate._33 = look.z;
		//카메라 오프셋 벡터를 회전 행렬로 변환(회전)한다.
		XMFLOAT3 offset = Vector3::TransformCoord(this->offset, rotate);
		//회전한 카메라의 위치는 플레이어의 위치에 회전한 카메라 오프셋 벡터를 더한 것이다.
		XMFLOAT3 position = Vector3::Add(player->GetPosition(), offset);
		//현재의 카메라의 위치에서 회전한 카메라의 위치까지의 방향과 거리를 나타내는 벡터이다.
		XMFLOAT3 direction = Vector3::Subtract(position, this->position);
		float length = Vector3::Length(direction);
		direction = Vector3::Normalize(direction);
		/*3인칭 카메라의 래그(Lag)는 플레이어가 회전하더라도 카메라가 동시에 따라서 회전하지 않고 약간의 시차를 두고
		회전하는 효과를 구현하기 위한 것이다. m_fTimeLag가 1보다 크면 fTimeLagScale이 작아지고 실제 회전(이동)이 적
		게 일어날 것이다. m_fTimeLag가 0이 아닌 경우 fTimeElapsed를 곱하고 있으므로 3인칭 카메라는 1초의 시간동안
		(1.0f / m_fTimeLag)의 비율만큼 플레이어의 회전을 따라가게 될 것이다.*/
		float timeLagScale = (timeLag) ? timeElapsed * (1.0f / timeLag) : 1.0f;
		float distance = length * timeLagScale;
		if (distance > length) distance = length;
		if (length < 0.01f) distance = length;
		if (distance > 0)
		{
			//실제로 카메라를 회전하지 않고 이동을 한다(회전의 각도가 작은 경우 회전 이동은 선형 이동과 거의 같다).
			this->position = Vector3::Add(this->position, direction, distance);
			//카메라가 플레이어를 바라보도록 한다.
			//SetLookAt(lookAt);
		}
	}
}

ThirdPersonCamera::ThirdPersonCamera(Camera* camera) : Camera(camera)
{
	mode = THIRD_PERSON_CAMERA;
	if (camera)
	{
		/*3인칭 카메라로 변경하기 이전의 카메라가 스페이스-쉽 카메라이면 카메라의 Up 벡터를 월드좌표의 y-축이 되도록
		한다. 이것은 스페이스-쉽 카메라의 로컬 y-축 벡터가 어떤 방향이든지 3인칭 카메라(대부분 사람인 경우)의 로컬 y
		축 벡터가 월드좌표의 y-축이 되도록 즉, 똑바로 서있는 형태로 설정한다는 의미이다. 그리고 로컬 x-축 벡터와 로컬
		z-축 벡터의 y-좌표가 0.0f가 되도록 한다. 이것은 로컬 x-축 벡터와 로컬 z-축 벡터를 xz-평면(지면)으로 투영하는
		것을 의미한다. 즉, 3인칭 카메라의 로컬 x-축 벡터와 로컬 z-축 벡터는 xz-평면에 평행하다.*/
		if (camera->GetMode() == SPACESHIP_CAMERA)
		{
			up = XMFLOAT3(0.0f, 1.0f, 0.0f);
			right.y = 0.0f;
			look.y = 0.0f;
			right = Vector3::Normalize(right);
			look = Vector3::Normalize(look);
		}
	}
}

void ThirdPersonCamera::Update(XMFLOAT3& lookAt, float timeElapsed)
{
	//플레이어가 있으면 플레이어의 회전에 따라 3인칭 카메라도 회전해야 한다.
	if (player)
	{
		XMFLOAT4X4 rotate = Matrix4x4::Identity();
		XMFLOAT3 right = player->GetRightVector();
		XMFLOAT3 up = player->GetUpVector();
		XMFLOAT3 look = player->GetLookVector();
		//플레이어의 로컬 x-축, y-축, z-축 벡터로부터 회전 행렬(플레이어와 같은 방향을 나타내는 행렬)을 생성한다.
		rotate._11 = right.x; rotate._21 = up.x; rotate._31 = look.x;
		rotate._12 = right.y; rotate._22 = up.y; rotate._32 = look.y;
		rotate._13 = right.z; rotate._23 = up.z; rotate._33 = look.z;
		//카메라 오프셋 벡터를 회전 행렬로 변환(회전)한다.
		XMFLOAT3 offset = Vector3::TransformCoord(this->offset, rotate);
		//회전한 카메라의 위치는 플레이어의 위치에 회전한 카메라 오프셋 벡터를 더한 것이다.
		XMFLOAT3 position = Vector3::Add(player->GetPosition(), offset);
		//현재의 카메라의 위치에서 회전한 카메라의 위치까지의 방향과 거리를 나타내는 벡터이다.
		XMFLOAT3 direction = Vector3::Subtract(position, this->position);
		float length = Vector3::Length(direction);
		direction = Vector3::Normalize(direction);
		/*3인칭 카메라의 래그(Lag)는 플레이어가 회전하더라도 카메라가 동시에 따라서 회전하지 않고 약간의 시차를 두고
		회전하는 효과를 구현하기 위한 것이다. m_fTimeLag가 1보다 크면 fTimeLagScale이 작아지고 실제 회전(이동)이 적
		게 일어날 것이다. m_fTimeLag가 0이 아닌 경우 fTimeElapsed를 곱하고 있으므로 3인칭 카메라는 1초의 시간동안
		(1.0f / m_fTimeLag)의 비율만큼 플레이어의 회전을 따라가게 될 것이다.*/
		float timeLagScale = (timeLag) ? timeElapsed * (1.0f / timeLag) : 1.0f;
		float distance = length * timeLagScale;
		if (distance > length) distance = length;
		if (length < 0.01f) distance = length;
		if (distance > 0)
		{
			//실제로 카메라를 회전하지 않고 이동을 한다(회전의 각도가 작은 경우 회전 이동은 선형 이동과 거의 같다).
			this->position = Vector3::Add(this->position, direction, distance);
			//카메라가 플레이어를 바라보도록 한다.
			//SetLookAt(lookAt);
		}
	}
}

void ThirdPersonCamera::SetLookAt(const XMFLOAT3& lookAt)
{
	//현재 카메라의 위치에서 플레이어를 바라보기 위한 카메라 변환 행렬을 생성한다.
	//여기서 카메라의 회전 변경을 주어야 한다
	XMFLOAT3 tempRight = player->GetRightVector();
	XMFLOAT3 tempUp = player->GetUpVector();
	XMFLOAT3 tempLook = player->GetLookVector();

	XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&tempRight), XMConvertToRadians(rotateData));
	//카메라의 로컬 x-축, y-축, z-축을 회전 행렬을 사용하여 회전한다.
	tempLook = Vector3::TransformNormal(tempLook, rotate);
	tempUp = Vector3::TransformNormal(tempUp, rotate);
	tempRight = Vector3::TransformNormal(tempRight, rotate);

	XMFLOAT3 camLookPos = Vector3::Add(position, Vector3::ScalarProduct(tempLook, 10.0f));

	XMFLOAT4X4 tempLookAt = Matrix4x4::LookAtLH(position, camLookPos, player->GetUpVector());
	//카메라 변환 행렬에서 카메라의 x-축, y-축, z-축을 구한다.
	right = XMFLOAT3(tempLookAt._11, tempLookAt._21, tempLookAt._31);
	up = XMFLOAT3(tempLookAt._12, tempLookAt._22, tempLookAt._32);
	look = XMFLOAT3(tempLookAt._13, tempLookAt._23, tempLookAt._33);
	lookAtWorld = camLookPos;
}

ZoomCamera::ZoomCamera(Camera* camera) : Camera(camera)
{
	mode = ZOOM_MODE_CAMERA;
	if (camera)
	{
		if (player)
		{
			up = XMFLOAT3(0.0f, 1.0f, 0.0f);
			right.y = 0.0f;
			look.y = 0.0f;
			right = Vector3::Normalize(right);
			look = Vector3::Normalize(look);
		}
	}
}

void ZoomCamera::Update()
{

	if (player)
	{
		XMFLOAT4X4 rotate = Matrix4x4::Identity();
		XMFLOAT3 right = player->GetRightVector();
		XMFLOAT3 up = player->GetUpVector();
		XMFLOAT3 look = player->GetLookVector();
		rotate._11 = right.x; rotate._21 = up.x; rotate._31 = look.x;
		rotate._12 = right.y; rotate._22 = up.y; rotate._32 = look.y;
		rotate._13 = right.z; rotate._23 = up.z; rotate._33 = look.z;
		XMFLOAT3 offset = Vector3::TransformCoord(this->offset, rotate);
		XMFLOAT3 position = Vector3::Add(player->GetPosition(), offset);
	}
}
void ZoomCamera::SetLookAt(const XMFLOAT3& lookAt)
{
	//현재 카메라의 위치에서 플레이어를 바라보기 위한 카메라 변환 행렬을 생성한다.
	//여기서 카메라의 회전 변경을 주어야 한다
	XMFLOAT3 tempRight = player->GetRightVector();
	XMFLOAT3 tempUp = player->GetUpVector();
	XMFLOAT3 tempLook = player->GetLookVector();

	XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&tempRight), XMConvertToRadians(rotateData));
	//카메라의 로컬 x-축, y-축, z-축을 회전 행렬을 사용하여 회전한다.
	tempLook = Vector3::TransformNormal(tempLook, rotate);
	tempUp = Vector3::TransformNormal(tempUp, rotate);
	tempRight = Vector3::TransformNormal(tempRight, rotate);

	XMFLOAT3 camLookPos = Vector3::Add(position, Vector3::ScalarProduct(tempLook, 10.0f));

	XMFLOAT4X4 tempLookAt = Matrix4x4::LookAtLH(position, camLookPos, player->GetUpVector());
	//카메라 변환 행렬에서 카메라의 x-축, y-축, z-축을 구한다.
	right = XMFLOAT3(tempLookAt._11, tempLookAt._21, tempLookAt._31);
	up = XMFLOAT3(tempLookAt._12, tempLookAt._22, tempLookAt._32);
	look = XMFLOAT3(tempLookAt._13, tempLookAt._23, tempLookAt._33);
	lookAtWorld = camLookPos;
}