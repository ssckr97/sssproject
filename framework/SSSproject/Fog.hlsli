
float GetFogFactor(float distance)
{
    float result;
    result = saturate((gvFogInfo.z - distance) / (gvFogInfo.z - gvFogInfo.y));
    return result;
}

float4 BlendFog(float4 color, float factor)
{
    float4 result;
    result = factor * color + (1.0f - factor) * gvFogColor;
    return result;
}
