#include "Shaders.hlsli"
#include "Fog.hlsli"

struct VS_SKYED_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
};

struct VS_SKYED_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float3 positionL : TEXCOORD;
    
    //float fogFactor : FACTOR;
};
VS_SKYED_OUTPUT VSSkyBoxed(VS_SKYED_INPUT input)
{
    VS_SKYED_OUTPUT output;

    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.positionL = input.position;
    output.positionW = mul(input.position, 30);
    
    //float4 cameraPos = mul(mul(float4(output.positionW, 1.0f), gmtxWorld), gmtxView);
    //output.fogFactor = GetFogFactor(cameraPos.z);

    return (output);
}

float4 PSSkyBoxed(VS_SKYED_OUTPUT input) : SV_TARGET
{
    float dist = length(input.positionW - gvCameraPosition);
    float fogFactor = GetFogFactor(dist);
    float4 cColor = gtxtSkyBoxTexture.Sample(gssClamp, input.positionL);
    cColor = BlendFog(cColor, fogFactor);
    return (cColor);
}

struct VS_UI_INPUT
{
	float3 position : POSITION;
	float2 uv : TEXCOORD;
};

struct VS_UI_OUTPUT
{
	float4 position : POSITION;
	float2 uv : TEXCOORD;
};
