#include "stdafx.h"
#include "Mesh.h"
#include "GameObject.h"
#define _WITH_APPROXIMATE_OPPOSITE_CORNER

Mesh::Mesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
}
Mesh::~Mesh()
{
	if (vertexBuffer) vertexBuffer->Release();
	if (indexBuffer) indexBuffer->Release();
	if (positionBuffer) positionBuffer->Release();

	if (subMeshesNum > 0)
	{
		for (int i = 0; i < subMeshesNum; i++)
		{
			if (subSetIndexBuffers[i]) {
				subSetIndexBuffers[i]->Release();
				subSetIndexBuffers[i] = NULL;
			}
			if (subSetIndices[i]) {
				delete[] subSetIndices[i];
				subSetIndices[i] = NULL;
			}
		}
		if (subSetIndexBuffers) {
			delete[] subSetIndexBuffers;
			subSetIndexBuffers = NULL;
		}
		if (subSetIndexBufferViews) {
			delete[] subSetIndexBufferViews;
			subSetIndexBufferViews = NULL;
		}

		if (subSetIndicesNum) {
			delete[] subSetIndicesNum;
			subSetIndicesNum = NULL;
		}
		if (subSetIndices) { 
			delete[] subSetIndices; 
			subSetIndices = NULL;
		}
		subMeshesNum = 0;
	}
}

void Mesh::ReleaseUploadBuffers()
{
	if (vertexUploadBuffer) vertexUploadBuffer->Release();
	if (indexUploadBuffer) indexUploadBuffer->Release();
	if (positionUploadBuffer) positionUploadBuffer->Release();
	vertexUploadBuffer = NULL;
	indexUploadBuffer = NULL;
	positionUploadBuffer = NULL;
	if (subMeshesNum > 0)
	{
		for (int i = 0; i < subMeshesNum; i++)
		{
			if (subSetIndicesNum[i] > 0 )
				if (subSetIndexUploadBuffers[i]) subSetIndexUploadBuffers[i]->Release();
		}
		subSetIndexUploadBuffers = NULL;

		if (subSetIndexUploadBuffers) delete[] subSetIndexUploadBuffers;
	}
}

void Mesh::Render(ID3D12GraphicsCommandList* commandList)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);
	commandList->IASetVertexBuffers(slot, 1, &vertexBufferView);
	if (indexBuffer)
	{
		commandList->IASetIndexBuffer(&indexBufferView);
		commandList->DrawIndexedInstanced(indicesNum, 1, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, 1, offset, 0);
	}
}

void Mesh::Render(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);

	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[2];
	vertexBufferViews[0] = vertexBufferView;
	vertexBufferViews[1] = instancingBufferViews;

	//정점 버퍼 뷰와 인스턴싱 버퍼 뷰를 입력-조립 단계에 설정한다.

	commandList->IASetVertexBuffers(slot, 2, vertexBufferViews);
	if (indexBuffer)
	{
		commandList->IASetIndexBuffer(&indexBufferView);
		commandList->DrawIndexedInstanced(indicesNum, instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}
	delete[] vertexBufferViews;
}

void Mesh::SetVertexBuffer(ID3D12Resource* vertexBuffer)
{
	this->vertexBuffer = vertexBuffer;
}





TriangleMesh::TriangleMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList) : Mesh(device, commandList)
{
	verticesNum = 3;
	int stride = sizeof(DiffusedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	//삼각형 메쉬를 정의한다.

	DiffusedVertex vertices[3];
	vertices[0] = DiffusedVertex(XMFLOAT3(0.0f, 0.5f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[1] = DiffusedVertex(XMFLOAT3(0.5f, -0.5f, 0.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f));
	vertices[2] = DiffusedVertex(XMFLOAT3(-0.5f, -0.5f, 0.0f), XMFLOAT4(Colors::Blue));
	/*정점(삼각형의 꼭지점)의 색상은 시계방향 순서대로 빨간색, 녹색, 파란색으로 지정한다. RGBA(Red, Green, Blue,
	Alpha) 4개의 파라메터를 사용하여 색상을 표현한다. 각 파라메터는 0.0~1.0 사이의 실수값을 가진다.*/

	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	//삼각형 메쉬를 리소스(정점 버퍼)로 생성한다.

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
	//정점 버퍼 뷰를 생성한다.
}

CubeMeshDiffused::CubeMeshDiffused(ID3D12Device* device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth) : Mesh(device, commandList)
{
	verticesNum = 8;
	int stride = sizeof(DiffusedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	float halfX = width * 0.5f, halfY = height * 0.5f, halfZ = depth * 0.5f;

	DiffusedVertex vertices[8];
	vertices[0] = DiffusedVertex(XMFLOAT3(-halfX, +halfY, -halfZ), RANDOM_COLOR);
	vertices[1] = DiffusedVertex(XMFLOAT3(+halfX, +halfY, -halfZ), RANDOM_COLOR);
	vertices[2] = DiffusedVertex(XMFLOAT3(+halfX, +halfY, +halfZ), RANDOM_COLOR);
	vertices[3] = DiffusedVertex(XMFLOAT3(-halfX, +halfY, +halfZ), RANDOM_COLOR);
	vertices[4] = DiffusedVertex(XMFLOAT3(-halfX, -halfY, -halfZ), RANDOM_COLOR);
	vertices[5] = DiffusedVertex(XMFLOAT3(+halfX, -halfY, -halfZ), RANDOM_COLOR);
	vertices[6] = DiffusedVertex(XMFLOAT3(+halfX, -halfY, +halfZ), RANDOM_COLOR);
	vertices[7] = DiffusedVertex(XMFLOAT3(-halfX, -halfY, +halfZ), RANDOM_COLOR);
	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum,
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;

	indicesNum = 36;
	UINT indices[36];
	indices[0] = 3; indices[1] = 1; indices[2] = 0;
	indices[3] = 2; indices[4] = 1; indices[5] = 3;
	indices[6] = 0; indices[7] = 5; indices[8] = 4;
	indices[9] = 1; indices[10] = 5; indices[11] = 0;
	indices[12] = 3; indices[13] = 4; indices[14] = 7;
	indices[15] = 0; indices[16] = 4; indices[17] = 3;
	indices[18] = 1; indices[19] = 6; indices[20] = 5;
	indices[21] = 2; indices[22] = 6; indices[23] = 1;
	indices[24] = 2; indices[25] = 7; indices[26] = 6;
	indices[27] = 3; indices[28] = 7; indices[29] = 2;
	indices[30] = 6; indices[31] = 4; indices[32] = 5;
	indices[33] = 7; indices[34] = 4; indices[35] = 6;

	indexBuffer = ::CreateBufferResource(device, commandList, indices, sizeof(UINT) * indicesNum, 
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &indexUploadBuffer);

	indexBufferView.BufferLocation = indexBuffer->GetGPUVirtualAddress();
	indexBufferView.Format = DXGI_FORMAT_R32_UINT;
	indexBufferView.SizeInBytes = sizeof(UINT) * indicesNum;
}

CubeMeshDiffused::~CubeMeshDiffused()
{
}

CubeMeshIlluminatedTextured::CubeMeshIlluminatedTextured(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth) : MeshIlluminated(device, commandList)
{
	verticesNum = 36;
	int stride = sizeof(IlluminatedTexturedVertex);
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float x = width * 0.5f, y = height * 0.5f, z = depth * 0.5f;

	IlluminatedTexturedVertex vertices[36];
	XMFLOAT3 position[36];
	XMFLOAT3 nomals[36];
	int i = 0;

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));
			
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 1.0f));
			
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));
		
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 1.0f));
			
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));
			
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 1.0f));
			
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
									
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));
									
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));
									
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));
									
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
										
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	vertices[i] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(0.0f, 1.0f));

 	for (int j = 0; j < 36; ++j) nomals[j] = XMFLOAT3(0.0f, 0.0f, 0.0f);

	CalculateVertexNormals(nomals, position, verticesNum, NULL, 0);

	for (int j = 0; j < 36; ++j) vertices[j].normal = nomals[j];
	

	vertexBuffer = CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}

CubeMeshIlluminatedTextured::~CubeMeshIlluminatedTextured()
{
}

CubeMeshNormalMapTextured::CubeMeshNormalMapTextured(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth) : StandardMesh(device, commandList)
{
	verticesNum = 36;
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float x = width * 0.5f, y = height * 0.5f, z = depth * 0.5f;
	
	positions = new XMFLOAT3[verticesNum];
	textureCoords0 = new XMFLOAT2[verticesNum];
	normals = new XMFLOAT3[verticesNum];
	biTangents = new XMFLOAT3[verticesNum];
	tangents = new XMFLOAT3[verticesNum];
	
	int i = 0;

	positions[i] = XMFLOAT3(-x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
											
	positions[i] = XMFLOAT3(-x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
													
	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
												 
	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
												 
	positions[i] = XMFLOAT3(-x, -y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
												 	
	positions[i] = XMFLOAT3(-x, -y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
												 		
	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
												 		
	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, -y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
												 	
	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(-x, +y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
												 	
	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, -y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
												 	
	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
												 	
	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(+x, -y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);

	CalculateTriangleListTBNs(verticesNum, positions, textureCoords0, tangents, biTangents, normals);

	positionBuffer = CreateBufferResource(device, commandList, positions,  sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &positionUploadBuffer);
	positionBufferView.BufferLocation = positionBuffer->GetGPUVirtualAddress();
	positionBufferView.StrideInBytes = sizeof(XMFLOAT3);
	positionBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	type |= VertexPosition;
	vertexSize++;
	
	textureCoord0Buffer = CreateBufferResource(device, commandList, textureCoords0, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord0UploadBuffer);
	textureCoord0BufferView.BufferLocation = textureCoord0Buffer->GetGPUVirtualAddress();
	textureCoord0BufferView.StrideInBytes = sizeof(XMFLOAT2);
	textureCoord0BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
	type |= VertexTexCoord0;
	vertexSize++;

	normalBuffer = CreateBufferResource(device, commandList, normals, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &normalUploadBuffer);
	normalBufferView.BufferLocation = normalBuffer->GetGPUVirtualAddress();
	normalBufferView.StrideInBytes = sizeof(XMFLOAT3);
	normalBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	type |= VertexNormal;
	vertexSize++;

	biTangentBuffer = CreateBufferResource(device, commandList, biTangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &biTangentUploadBuffer);
	biTangentBufferView.BufferLocation = biTangentBuffer->GetGPUVirtualAddress();
	biTangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
	biTangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	type |= VertexTangent;
	vertexSize++;

	tangentBuffer = CreateBufferResource(device, commandList, tangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &tangentUploadBuffer);
	tangentBufferView.BufferLocation = tangentBuffer->GetGPUVirtualAddress();
	tangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
	tangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	vertexSize++;

}

CubeMeshNormalMapTextured::~CubeMeshNormalMapTextured()
{
}

void CubeMeshNormalMapTextured::CalculateTriangleListTBNs(int verticesNum, XMFLOAT3 *positions, XMFLOAT2 *texCoords, XMFLOAT3 *tangents, XMFLOAT3 *biTangents, XMFLOAT3 *normals)
{
	float u10, v10, u20, v20, x10, x20, y10, y20, z10, z20;
	for (int i = 0; i < verticesNum; i += 3)
	{
		u10 = texCoords[i + 1].x - texCoords[i].x;
		v10 = texCoords[i + 1].y - texCoords[i].y;
		u20 = texCoords[i + 2].x - texCoords[i].x;
		v20 = texCoords[i + 2].y - texCoords[i].y;
		x10 = positions[i + 1].x - positions[i].x;
		y10 = positions[i + 1].y - positions[i].y;
		z10 = positions[i + 1].z - positions[i].z;
		x20 = positions[i + 2].x - positions[i].x;
		y20 = positions[i + 2].y - positions[i].y;
		z20 = positions[i + 2].z - positions[i].z;
		float invDeterminant = 1.0f / (u10 * v20 - u20 * v10);
		tangents[i] = tangents[i + 1] = tangents[i + 2] = XMFLOAT3(invDeterminant * (v20 * x10 - v10 * x20), invDeterminant * (v20 * y10 - v10 * y20), invDeterminant * (v20 * z10 - v10 * z20));
		biTangents[i] = biTangents[i + 1] = biTangents[i + 2] = XMFLOAT3(invDeterminant * (-u20 * x10 + u10 * x20), invDeterminant * (-u20 * y10 + u10 * y20), invDeterminant * (-u20 * z10 + u10 * z20));
		normals[i] = Vector3::CrossProduct(tangents[i], biTangents[i], true);
		normals[i + 1] = Vector3::CrossProduct(tangents[i + 1], biTangents[i + 1], true);
		normals[i + 2] = Vector3::CrossProduct(tangents[i + 2], biTangents[i + 2], true);

	}
}

AirplaneMeshDiffused::AirplaneMeshDiffused(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, 
	float width, float height, float depth, XMFLOAT4 color) : Mesh(device, commandList)
{
	verticesNum = 24 * 3;
	int stride = sizeof(DiffusedVertex);
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	float halfX = width * 0.5f, halfY = height * 0.5f, halfZ = depth * 0.5f;
	//위의 그림과 같은 비행기 메쉬를 표현하기 위한 정점 데이터이다.
	DiffusedVertex vertices[24 * 3];
	float x1 = halfX * 0.2f, y1 = halfY * 0.2f, x2 = halfX * 0.1f, y3 = halfY * 0.3f, y2 = ((y1 - (halfY - y3)) / x1) * x2 + (halfY - y3);
	int i = 0;
	//비행기 메쉬의 위쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));

	//비행기 메쉬의 아래쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));

	//비행기 메쉬의 오른쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));

	//비행기 메쉬의 뒤쪽/오른쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	
	//비행기 메쉬의 왼쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));

	//비행기 메쉬의 뒤쪽/왼쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum, 
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}
AirplaneMeshDiffused::~AirplaneMeshDiffused()
{
}

HeightMapImage::HeightMapImage(LPCTSTR fileName, int width, int length, XMFLOAT3 scale)
{
	this->width = width;
	this->length = length;
	this->scale = scale;

	BYTE* heightMapPixels = new BYTE[this->width * this->length];
	// 파일을 열고 읽는다. 높이 맵 이미지는 파일 헤더가 없는 RAW 이미지다.
	HANDLE file = ::CreateFile(fileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_READONLY, NULL);
	DWORD bytesRead;
	::ReadFile(file, heightMapPixels, (width * length), &bytesRead, NULL);
	::CloseHandle(file);
	// 이미지의 y축과 지형의 z축이 방향이 반대이므로 이미지를 상하대칭 시켜 저장한다.
	
	this->heightMapPixels = new BYTE[this->width * this->length];
	for (int y = 0; y < this->length; ++y)
	{
		for (int x = 0; x < this->width; ++x)
		{
			this->heightMapPixels[x + ((this->length - 1 - y) * this->width)] = heightMapPixels[x + y * this->width];
		}
	}
	if (heightMapPixels) delete[] heightMapPixels;
}

HeightMapImage::~HeightMapImage()
{
	if (heightMapPixels) delete[] heightMapPixels;
	heightMapPixels = NULL;
}

XMFLOAT3 HeightMapImage::GetHeightMapNormal(int x, int z)
{
	if ((x < 0.0f) || (z < 0.0f) || (x >= width) || (z >= length))
		return(XMFLOAT3(0.0f, 1.0f, 0.0f));

	int nHeightMapIndex = x + (z * width);
	int xHeightMapAdd = (x < (width - 1)) ? 1 : -1;
	int zHeightMapAdd = (z < (length - 1)) ? width : -width;

	float y1 = (float)heightMapPixels[nHeightMapIndex] * scale.y;
	float y2 = (float)heightMapPixels[nHeightMapIndex + xHeightMapAdd] * scale.y;
	float y3 = (float)heightMapPixels[nHeightMapIndex + zHeightMapAdd] * scale.y;

	XMFLOAT3 edge1 = XMFLOAT3(0.0f, y3 - y1, scale.z);

	XMFLOAT3 edge2 = XMFLOAT3(scale.x, y2 - y1, 0.0f);

	XMFLOAT3 normal = Vector3::CrossProduct(edge1, edge2, true);

	return(normal);
}

void HeightMapImage::LoadHeightMapImage(LPCTSTR fileName)
{
	BYTE* heightMapPixels = new BYTE[this->width * this->length];
	// 파일을 열고 읽는다. 높이 맵 이미지는 파일 헤더가 없는 RAW 이미지다.
	HANDLE file = ::CreateFile(fileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_READONLY, NULL);
	DWORD bytesRead;
	::ReadFile(file, heightMapPixels, (width * length), &bytesRead, NULL);
	::CloseHandle(file);
	// 이미지의 y축과 지형의 z축이 방향이 반대이므로 이미지를 상하대칭 시켜 저장한다.

	delete[] this->heightMapPixels;


	this->heightMapPixels = new BYTE[this->width * this->length];
	for (int y = 0; y < this->length; ++y)
	{
		for (int x = 0; x < this->width; ++x)
		{
			this->heightMapPixels[x + ((this->length - 1 - y) * this->width)] = heightMapPixels[x + y * this->width];
		}
	}
	if (heightMapPixels) delete[] heightMapPixels;


}

float HeightMapImage::GetHeight(float fx, float fz)
{
	/*지형의 좌표 (fx, fz)는 이미지 좌표계이다. 높이 맵의 x-좌표와 z-좌표가 높이 맵의 범위를 벗어나면 지형의 높이는 0이다.*/
	fx = fx + (width) / 2;
	fz = fz + (length) / 2;

	if ((fx < 0.0f) || (fz < 0.0f) || (fx >= width) || (fz >= length)) return(0.0f);
	//높이 맵의 좌표의 정수 부분과 소수 부분을 계산한다.
	int x = (int)fx;
	int z = (int)fz;
	float xPercent = fx - x;
	float zPercent = fz - z;
	float bottomLeft = (float)heightMapPixels[x + (z*width)];
	float bottomRight = (float)heightMapPixels[(x + 1) + (z*width)];
	float topLeft = (float)heightMapPixels[x + ((z + 1)*width)];
	float topRight = (float)heightMapPixels[(x + 1) + ((z + 1)*width)];

#ifdef _WITH_APPROXIMATE_OPPOSITE_CORNER
	//z-좌표가 1, 3, 5, ...인 경우 인덱스가 오른쪽에서 왼쪽으로 나열된다.

	bool bRightToLeft = ((z % 2) != 0);
	if (bRightToLeft)
	{
		/*지형의 삼각형들이 오른쪽에서 왼쪽 방향으로 나열되는 경우이다. 다음 그림의 오른쪽은 (fzPercent < fxPercent)
		인 경우이다. 이 경우 TopLeft의 픽셀 값은 (fTopLeft = fTopRight + (fBottomLeft - fBottomRight))로 근사한다.
		다음 그림의 왼쪽은 (fzPercent ≥ fxPercent)인 경우이다. 이 경우 BottomRight의 픽셀 값은 (fBottomRight =
		fBottomLeft + (fTopRight - fTopLeft))로 근사한다.*/

		if (zPercent >= xPercent)
			bottomRight = bottomLeft + (topRight - topLeft);
		else
			topLeft = topRight + (bottomLeft - bottomRight);
	}
	else
	{
		/*지형의 삼각형들이 왼쪽에서 오른쪽 방향으로 나열되는 경우이다. 다음 그림의 왼쪽은 (fzPercent < (1.0f -
		fxPercent))인 경우이다. 이 경우 TopRight의 픽셀 값은 (fTopRight = fTopLeft + (fBottomRight - fBottomLeft))로
		근사한다. 다음 그림의 오른쪽은 (fzPercent ≥ (1.0f - fxPercent))인 경우이다. 이 경우 BottomLeft의 픽셀 값은
		(fBottomLeft = fTopLeft + (fBottomRight - fTopRight))로 근사한다.*/
		if (zPercent < (1.0f - xPercent))
			topRight = topLeft + (bottomRight - bottomLeft);
		else
			bottomLeft = topLeft + (bottomRight - topRight);
	}
#endif
	//사각형의 네 점을 보간하여 높이(픽셀 값)를 계산한다.
	float topHeight = topLeft * (1 - xPercent) + topRight * xPercent;
	float bottomHeight = bottomLeft * (1 - xPercent) + bottomRight * xPercent;
	float height = bottomHeight * (1 - zPercent) + topHeight * zPercent;
	return(height);
}


HeightMapGridMesh::HeightMapGridMesh(ID3D12Device *device,
	ID3D12GraphicsCommandList *commandList, int xStart, int zStart, int width, int
	length , XMFLOAT3 scale, XMFLOAT4 color, void *context) : Mesh(device, commandList)
{
	verticesNum = width * length;
	offset = 0;
	slot = 0;
	int stride = sizeof(Diffused2TexturedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP;
	this->width = width;
	this->length = length;
	this->scale = scale;
	bool isCreateBoundingZ = false;
	int boundingCount{ 0 };
	boundingWidth = 16;
	boundingLength = 16;

	HeightMapImage *heightMapImage = (HeightMapImage *)context;
	int cxHeightMap = heightMapImage->GetHeightMapWidth();
	int czHeightMap = heightMapImage->GetHeightMapLength();

	diffused2TexturedVerices = new Diffused2TexturedVertex[verticesNum];
	float fHeight = 0.0f, fMinHeight = +FLT_MAX, fMaxHeight = -FLT_MAX;
	for (int i = 0, z = zStart; z < (zStart + this->length); z++)
	{
		if (z % boundingLength == 0) isCreateBoundingZ = true;

		for (int x = xStart; x < (xStart + this->width); x++, i++)
		{
			fHeight = OnGetHeight(x, z, context);
			XMFLOAT3 position = XMFLOAT3(( x * this->scale.x), 0.0f, (z * this->scale.z));
			XMFLOAT3 normal = heightMapImage->GetHeightMapNormal(x, z);
			XMFLOAT4 diffuseColor = XMFLOAT4(normal.x, normal.y, normal.z, 1.0f);
			XMFLOAT2 texCoord0 = XMFLOAT2(float(x) / float(cxHeightMap - 1), float(czHeightMap - 1 - z) / float(czHeightMap - 1));

			XMFLOAT2 texCoord1 = XMFLOAT2(float(x) / float(this->scale.x*0.5f), float(z) / float(this->scale.z*0.5f));

			diffused2TexturedVerices[i] = Diffused2TexturedVertex(position, diffuseColor, texCoord0, texCoord1);
			if (fHeight < fMinHeight) fMinHeight = fHeight;
			if (fHeight > fMaxHeight) fMaxHeight = fHeight;

			if (isCreateBoundingZ) {
				if (x == (xStart + this->width) - 1) {
					isCreateBoundingZ = false;
					continue;
				}
				if (x % boundingWidth == 0 && z != (zStart + this->length) - 1) {
					terrainBoundings[boundingCount].Center = XMFLOAT3((x * scale.x) + scale.x * ((float)boundingWidth / 2), 0.0f, (z * scale.z) + scale.z * ((float)boundingLength / 2));
					terrainBoundings[boundingCount].Extents = XMFLOAT3(scale.x * ((float)boundingWidth / 2), 300.0f, scale.z * ((float)boundingLength / 2));
					boundingVertexIndex[boundingCount] = i;
					boundingCount++;
				}

			}
		}
	}
	calALLNormal();

	boundingTree = new int*[TERRAINBOUNDINGS];

	for (int i = 0; i < TERRAINBOUNDINGS; ++i) {
		boundingTree[i] = new int[8];
		//위 
		if (i + boundingWidth < TERRAINBOUNDINGS)
			boundingTree[i][UPTREE] = boundingVertexIndex[i + boundingWidth];
		else
			boundingTree[i][UPTREE] = NULL;
		//아래
		if (i - boundingWidth >= 0)
			boundingTree[i][DOWNTREE] = boundingVertexIndex[i - boundingWidth];
		else
			boundingTree[i][DOWNTREE] = NULL;

		//왼쪽
		if (i % boundingWidth != 0)
			boundingTree[i][LEFTTREE] = boundingVertexIndex[i - 1];
		else
			boundingTree[i][LEFTTREE] = NULL;
		//오른쪽
		if ((i + 1) % boundingWidth != 0)
			boundingTree[i][RIGHTTREE] = boundingVertexIndex[i + 1];
		else
			boundingTree[i][RIGHTTREE] = NULL;
		//왼쪽위 대각선
		if (i + (boundingWidth - 1) < TERRAINBOUNDINGS && (i % boundingWidth) != 0)
			boundingTree[i][UPLEFTTREE] = boundingVertexIndex[i + (boundingWidth - 1)];
		else
			boundingTree[i][UPLEFTTREE] = NULL;
		//오른위 대각선
		if (((i + 1) % boundingWidth != 0) && i + (boundingWidth + 1) < TERRAINBOUNDINGS)
			boundingTree[i][UPRIGHTTREE] = boundingVertexIndex[i + (boundingWidth + 1)];
		else
			boundingTree[i][UPRIGHTTREE] = NULL;
		//왼쪽아래 대각선
		if (i - (boundingWidth + 1) >= 0 && (i % boundingWidth) != 0)
			boundingTree[i][DOWNLEFTTREE] = boundingVertexIndex[i - (boundingWidth + 1)];
		else
			boundingTree[i][DOWNLEFTTREE] = NULL;
		//오른아래 대각선 
		if ((i + 1) % boundingWidth != 0 && i - (boundingWidth - 1) >= 0)
			boundingTree[i][DOWNRIGHTTREE] = boundingVertexIndex[i - (boundingWidth - 1)];
		else
			boundingTree[i][DOWNRIGHTTREE] = NULL;
	}


	aabbBox.Center = XMFLOAT3(xStart * scale.x + (width * scale.x / 2), 0.0f, zStart * scale.z + (length * scale.z / 2));
	aabbBox.Extents = XMFLOAT3(width * scale.x / 2, 300.0f, length * scale.z / 2);


	vertexBuffer = CreateBufferResource(device, commandList, diffused2TexturedVerices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;


	indicesNum = ((this->width * 2)*(this->length - 1)) + ((this->length - 1) - 1);
	UINT *indices = new UINT[indicesNum];

	for (int j = 0, z = 0; z < this->length - 1; z++)
	{
		if ((z % 2) == 0)
		{
			for (int x = 0; x < this->width; x++)
			{
				if ((x == 0) && (z > 0)) indices[j++] = (UINT)(x + (z * this->width));
				indices[j++] = (UINT)(x + (z * this->width));
				indices[j++] = (UINT)((x + (z * this->width)) + this->width);
			}
		}
		else
		{
			for (int x = this->width - 1; x >= 0; x--)
			{
				if (x == (this->width - 1)) indices[j++] = (UINT)(x + (z * this->width));
				indices[j++] = (UINT)(x + (z * this->width));
				indices[j++] = (UINT)((x + (z * this->width)) + this->width);
			}
		}
	}
	indexBuffer = CreateBufferResource(device, commandList, indices, sizeof(UINT) * indicesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &indexUploadBuffer);
	indexBufferView.BufferLocation = indexBuffer->GetGPUVirtualAddress();
	indexBufferView.Format = DXGI_FORMAT_R32_UINT;
	indexBufferView.SizeInBytes = sizeof(UINT) * indicesNum;
	delete[] indices;
}
HeightMapGridMesh::~HeightMapGridMesh()
{
}

void HeightMapGridMesh::ResetVertexBuffer(ID3D12Device *device,ID3D12GraphicsCommandList *commandList , Diffused2TexturedVertex * diffuseVertex)
{
	int stride = sizeof(Diffused2TexturedVertex);
	vertexBuffer = CreateBufferResource(device, commandList, diffused2TexturedVerices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}


float HeightMapGridMesh::OnGetHeight(int x, int z, void *context)
{
	HeightMapImage *heightMapImage = (HeightMapImage *)context;
	BYTE *heightMapPixels = heightMapImage->GetHeightMapPixels();
	XMFLOAT3 scale = heightMapImage->GetScale();
	int width = heightMapImage->GetHeightMapWidth();

	float height = heightMapPixels[x + (z*width)] * scale.y;
	return(height);
}

XMFLOAT3 HeightMapGridMesh::calNormal(int index)
{
	bool isleftup = false;
	bool isupright = false;
	bool isrightdown = false;
	bool isdownleft = false;

	XMFLOAT3 leftupNormal{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 uprightNormal{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 rightdownNormal{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 downleftNormal{ 0.0f , 0.0f , 0.0f };

	float bias = 1.0;

	//4개의 삼각형의 노멀을 구한다 
	auto& d2t = diffused2TexturedVerices;
	if (index % width != 0 && index + width < verticesNum) // 왼쪽 위 
	{
		//왼
		auto v1 = Vector3::Subtract(d2t[index - 1].position, d2t[index].position);
		//위
		auto v2 = Vector3::Subtract(d2t[index + width].position, d2t[index].position);
		leftupNormal = Vector3::CrossProduct(v1, v2);
		leftupNormal.y = (leftupNormal.y * bias);
		leftupNormal = (leftupNormal);
		isleftup = true;
	}
	if (index + width < verticesNum && (index + 1) % width != 0) // 위 오른쪽  
	{
		//위
		auto v1 = Vector3::Subtract(d2t[index + width].position, d2t[index].position);
		//오른
		auto v2 = Vector3::Subtract(d2t[index + 1].position, d2t[index].position);

		uprightNormal = Vector3::CrossProduct(v1, v2);
		uprightNormal.y = (uprightNormal.y * bias);
		uprightNormal = (uprightNormal);
		isupright = true;
	}
	if ((index + 1) % width != 0 && index - width >= 0) // 오른쪽 아래   
	{
		//오른
		auto v1 = Vector3::Subtract(d2t[index + 1].position, d2t[index].position);
		//아래
		auto v2 = Vector3::Subtract(d2t[index - width].position, d2t[index].position);

		rightdownNormal = Vector3::CrossProduct(v1, v2);
		rightdownNormal.y = (rightdownNormal.y * bias);
		rightdownNormal = (rightdownNormal);
		isrightdown = true;
	}
	if (index - width >= 0 && index % width != 0) // 아래 왼쪽 
	{
		//아래
		auto v1 = Vector3::Subtract(d2t[index - width].position, d2t[index].position);
		//왼쪽
		auto v2 = Vector3::Subtract(d2t[index - 1].position, d2t[index].position);

		downleftNormal = Vector3::CrossProduct(v1, v2);
		downleftNormal.y = (downleftNormal.y * bias);
		downleftNormal = (downleftNormal);
		isdownleft = true;
	}

	XMFLOAT3 finalnormal = (Vector3::Add(Vector3::Add(Vector3::Add(leftupNormal, uprightNormal), rightdownNormal), downleftNormal));
	finalnormal.x = (finalnormal.x / (isleftup + isupright + isrightdown + isdownleft));
	finalnormal.y = (finalnormal.y / (isleftup + isupright + isrightdown + isdownleft));
	finalnormal.z = (finalnormal.z / (isleftup + isupright + isrightdown + isdownleft));
	//if (finalnormal.y <= 0) {
	//	std::cout << finalnormal.y << std::endl;
	//}

	return Vector3::Normalize(finalnormal);
}

void HeightMapGridMesh::calALLNormal()
{
	for (int i = 0; i < verticesNum; ++i) {
		XMFLOAT3 normal = calNormal(i);
		diffused2TexturedVerices[i].normal = normal;
	}
}

XMFLOAT4 HeightMapGridMesh::OnGetColor(int x, int z, void *context)
{
	XMFLOAT3 lightDirection = XMFLOAT3(-1.0f, 1.0f, 1.0f);

	lightDirection = Vector3::Normalize(lightDirection);
	HeightMapImage *heightMapImage = (HeightMapImage *)context;
	XMFLOAT3 scale = heightMapImage->GetScale();
	XMFLOAT4 incidentLightColor(0.9f, 0.8f, 0.4f, 1.0f);

	float lightScale = Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x, z), lightDirection);
	lightScale += Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x + 1, z), lightDirection);
	lightScale += Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x + 1, z + 1), lightDirection);
	lightScale += Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x, z + 1), lightDirection);
	lightScale = (lightScale / 4.0f) + 0.05f;


	if (lightScale > 1.0f) lightScale = 1.0f;
	if (lightScale < 0.25f) lightScale = 0.25f;

	XMFLOAT4 color = Vector4::Multiply(lightScale, incidentLightColor);
   	return(color);
}

bool HeightMapGridMesh::CheckRayIntersect(XMFLOAT3 origin, XMFLOAT3 dir, float & pfNearHitDistance, XMFLOAT4X4 world, int bIdx)
{
	int nIntersections = 0;

	BYTE *pbPositions = (BYTE *)diffused2TexturedVerices;
	XMVECTOR tempOrigin = XMLoadFloat3(&origin);
	XMVECTOR tempDir = XMLoadFloat3(&dir);

	for (int z = 0; z < boundingLength; ++z) {
		for (int x = 0; x < boundingWidth; ++x) {
			//삼각형을 구성해야한다. 점하나당 삼각형 2개가 나온다 .
			auto v0 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + z * (width)].position, world));
			auto v1 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + (z + 1) * (width)].position, world));
			auto v2 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + z * (width)+1].position, world));

			float fHitDistance = FLT_MAX;
			BOOL bIntersected = TriangleTests::Intersects(tempOrigin, tempDir, v0,
				v1, v2, fHitDistance);
			if (bIntersected)
			{
				if (fHitDistance < pfNearHitDistance)
				{
					pfNearHitDistance = fHitDistance;
				}
				nIntersections++;
			}


			v0 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + (z + 1) * (width)].position, world));
			v1 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + (z + 1) * (width)+1].position, world));
			v2 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + z * (width)+1].position, world));

			fHitDistance = FLT_MAX;
			bIntersected = TriangleTests::Intersects(tempOrigin, tempDir, v0,
				v1, v2, fHitDistance);
			if (bIntersected)
			{
				if (fHitDistance < pfNearHitDistance)
				{
					pfNearHitDistance = fHitDistance;
				}
				nIntersections++;
			}

		}
	}

	if (pfNearHitDistance < FLT_MAX - 1.0f) {
		return true;
	}

	else {
		return false;
	}
}

MeshIlluminated::MeshIlluminated(ID3D12Device *device, ID3D12GraphicsCommandList* commandList) : Mesh(device, commandList)
{
}
MeshIlluminated::~MeshIlluminated()
{
}

void MeshIlluminated::CalculateTriangleListVertexNormals(XMFLOAT3 *normals, XMFLOAT3 *positions, int verticesNum)
{
	int primitives = verticesNum / 3;
	UINT index0, index1, index2;
	for (int i = 0; i < primitives; i++)
	{
		index0 = i * 3 + 0;
		index1 = i * 3 + 1;
		index2 = i * 3 + 2;
		XMFLOAT3 edge01 = Vector3::Subtract(positions[index1], positions[index0]);
		XMFLOAT3 edge02 = Vector3::Subtract(positions[index2], positions[index0]);
		normals[index0] = normals[index1] = normals[index2] = Vector3::CrossProduct(edge01, edge02, true);
	}
}

void MeshIlluminated::CalculateTriangleListVertexNormals(XMFLOAT3 *normals, XMFLOAT3 *positions, UINT verticesNum, UINT *indices, UINT indicesNum)
{
	UINT primitives = (indices) ? (indicesNum / 3) : (verticesNum / 3);
	XMFLOAT3 sumOfNormal, edge01, edge02, normal;
	UINT index0, index1, index2;
	for (UINT j = 0; j < verticesNum; j++)
	{
		sumOfNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
		for (UINT i = 0; i < primitives; i++)
		{
			index0 = indices[i * 3 + 0];
			index1 = indices[i * 3 + 1];
			index2 = indices[i * 3 + 2];
			if (indices && ((index0 == j) || (index1 == j) || (index2 == j)))
			{
				edge01 = Vector3::Subtract(positions[index1], positions[index0]);
				edge02 = Vector3::Subtract(positions[index2],
				positions[index0]);
				normal = Vector3::CrossProduct(edge01, edge02, false);
				sumOfNormal = Vector3::Add(sumOfNormal, normal);
			}
		}
		normals[j] = Vector3::Normalize(sumOfNormal);
	}
}

void MeshIlluminated::CalculateTriangleStripVertexNormals(XMFLOAT3 *normals, XMFLOAT3 *positions, UINT verticesNum, UINT *indices, UINT indicesNum)
{
	UINT primitives = (indices) ? (indicesNum - 2) : (verticesNum - 2);
	XMFLOAT3 sumOfNormal(0.0f, 0.0f, 0.0f);
	UINT index0, index1, index2;
	for (UINT j = 0; j < verticesNum; j++)
	{
		sumOfNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
		for (UINT i = 0; i < primitives; i++)
		{
			index0 = ((i % 2) == 0) ? (i + 0) : (i + 1);
			if (indices) index0 = indices[index0];
			index1 = ((i % 2) == 0) ? (i + 1) : (i + 0);
			if (indices) index1 = indices[index1];
			index2 = (indices) ? indices[i + 2] : (i + 2);
			if ((index0 == j) || (index1 == j) || (index2 == j))
			{
				XMFLOAT3 edge01 = Vector3::Subtract(positions[index1], positions[index0]);
				XMFLOAT3 edge02 = Vector3::Subtract(positions[index2], positions[index0]);
				XMFLOAT3 normal = Vector3::CrossProduct(edge01, edge02, true);
				sumOfNormal = Vector3::Add(sumOfNormal, normal);
			}
		}
		normals[j] = Vector3::Normalize(sumOfNormal);
	}
}

void MeshIlluminated::CalculateVertexNormals(XMFLOAT3* normals, XMFLOAT3* positions, int verticesNum, UINT *indices, int indicesNum)
{
	switch (primitiveTopology)
	{
	case D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST:
		if (indices)
			CalculateTriangleListVertexNormals(normals, positions, verticesNum, indices, indicesNum);
		else
			CalculateTriangleListVertexNormals(normals, positions, verticesNum);
		break;
	case D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP:
		CalculateTriangleStripVertexNormals(normals, positions, verticesNum, indices, indicesNum);
		break;
	default:
		break;
	}
}

CubeMeshIlluminated::CubeMeshIlluminated(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth) : MeshIlluminated(device, commandList)
{
	verticesNum = 8;
	int stride = sizeof(IlluminatedVertex);
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	indicesNum = 36;
	UINT indices[36];
	indices[0] = 3; indices[1] = 1; indices[2] = 0;
	indices[3] = 2; indices[4] = 1; indices[5] = 3;
	indices[6] = 0; indices[7] = 5; indices[8] = 4;
	indices[9] = 1; indices[10] = 5; indices[11] = 0;
	indices[12] = 3; indices[13] = 4; indices[14] = 7;
	indices[15] = 0; indices[16] = 4; indices[17] = 3;
	indices[18] = 1; indices[19] = 6; indices[20] = 5;
	indices[21] = 2; indices[22] = 6; indices[23] = 1;
	indices[24] = 2; indices[25] = 7; indices[26] = 6;
	indices[27] = 3; indices[28] = 7; indices[29] = 2;
	indices[30] = 6; indices[31] = 4; indices[32] = 5;
	indices[33] = 7; indices[34] = 4; indices[35] = 6;
	indexBuffer = ::CreateBufferResource(device, commandList, indices, sizeof(UINT) * indicesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &indexUploadBuffer);
	indexBufferView.BufferLocation = indexBuffer->GetGPUVirtualAddress();
	indexBufferView.Format = DXGI_FORMAT_R32_UINT;
	indexBufferView.SizeInBytes = sizeof(UINT) * indicesNum;
	XMFLOAT3 positions[8];
	float fx = width * 0.5f, fy = height * 0.5f, fz = depth * 0.5f;
	positions[0] = XMFLOAT3(-fx, +fy, -fz);
	positions[1] = XMFLOAT3(+fx, +fy, -fz);
	positions[2] = XMFLOAT3(+fx, +fy, +fz);
	positions[3] = XMFLOAT3(-fx, +fy, +fz);
	positions[4] = XMFLOAT3(-fx, -fy, -fz);
	positions[5] = XMFLOAT3(+fx, -fy, -fz);
	positions[6] = XMFLOAT3(+fx, -fy, +fz);
	positions[7] = XMFLOAT3(-fx, -fy, +fz);
	XMFLOAT3 normals[8];
	for (int i = 0; i < 8; i++) normals[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	CalculateVertexNormals(normals, positions, verticesNum, indices, indicesNum);
	
	IlluminatedVertex vertices[8];
	for (int i = 0; i < 8; i++) 
		vertices[i] = IlluminatedVertex(positions[i],normals[i]);
	vertexBuffer = CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}
CubeMeshIlluminated::~CubeMeshIlluminated()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//
StandardMesh::StandardMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList) : Mesh(device, commandList)
{
}

StandardMesh::~StandardMesh()
{
	if (textureCoord0Buffer) 
		textureCoord0Buffer->Release();
	if (textureCoord1Buffer)
		textureCoord1Buffer->Release();
	if (normalBuffer) 
		normalBuffer->Release();
	if (tangentBuffer) 
		tangentBuffer->Release();
	if (biTangentBuffer) 
		biTangentBuffer->Release();

	if (colors) { 
		delete[] colors; 
		colors = NULL;
	}
	if (normals) {
		delete[] normals;
		normals = NULL;
	}
	if (tangents) { 
		delete[] tangents; 
		tangents = NULL;
	}
	if (biTangents) {
		delete[] biTangents;
		biTangents = NULL;
	}
	if (textureCoords0) {
		delete[] textureCoords0;
		textureCoords0 = NULL;
	}
	if (textureCoords1) {
		delete[] textureCoords1;
		textureCoords1 = NULL;
	}
}

void StandardMesh::ReleaseUploadBuffers()
{
	Mesh::ReleaseUploadBuffers();

	if (positionUploadBuffer) {
		positionUploadBuffer->Release();
		positionUploadBuffer = NULL;
	}

	if (textureCoord0UploadBuffer) {
		textureCoord0UploadBuffer->Release();
		textureCoord0UploadBuffer = NULL;
	}

	if (textureCoord1UploadBuffer) {
		textureCoord1UploadBuffer->Release();
		textureCoord1UploadBuffer = NULL;
	}

	if (normalUploadBuffer) {
		normalUploadBuffer->Release();
		normalUploadBuffer = NULL;
	}

	if (tangentUploadBuffer) {
		tangentUploadBuffer->Release();
		tangentUploadBuffer = NULL;
	}

	if (biTangentUploadBuffer) {
		biTangentUploadBuffer->Release();
		biTangentUploadBuffer = NULL;
	}


}

void StandardMesh::LoadMeshFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, FILE *inFile)
{
	char token[64] = { '\0' };
	BYTE strLength = 0;

	int nPositions = 0, nColors = 0, nNormals = 0, nTangents = 0, nBiTangents = 0, nTextureCoords = 0, nIndices = 0, nSubMeshes = 0, nSubIndices = 0;

	UINT nReads = (UINT)::fread(&verticesNum, sizeof(int), 1, inFile);
	nReads = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
	nReads = (UINT)::fread(meshName, sizeof(char), strLength, inFile);
	meshName[strLength] = '\0';

	for (; ; )
	{
		nReads = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
		nReads = (UINT)::fread(token, sizeof(char), strLength, inFile);
		token[strLength] = '\0';

		if (!strcmp(token, "<Bounds>:"))
		{
			nReads = (UINT)::fread(&boundingBoxAABBCenter, sizeof(XMFLOAT3), 1, inFile);
			nReads = (UINT)::fread(&boundingBoxAABBExtents, sizeof(XMFLOAT3), 1, inFile);
		}
		else if (!strcmp(token, "<Positions>:"))
		{
			nReads = (UINT)::fread(&nPositions, sizeof(int), 1, inFile);
			if (nPositions > 0)
			{
				type |= VertexPosition;
				positions = new XMFLOAT3[nPositions];
				nReads = (UINT)::fread(positions, sizeof(XMFLOAT3), nPositions, inFile);

				positionBuffer = ::CreateBufferResource(device, commandList, positions, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &positionUploadBuffer);

				positionBufferView.BufferLocation = positionBuffer->GetGPUVirtualAddress();
				positionBufferView.StrideInBytes = sizeof(XMFLOAT3);
				positionBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<Colors>:"))
		{
			nReads = (UINT)::fread(&nColors, sizeof(int), 1, inFile);
			if (nColors > 0)
			{
				type |= VertexColor;
				colors = new XMFLOAT4[nColors];
				nReads = (UINT)::fread(colors, sizeof(XMFLOAT4), nColors, inFile);
			}
		}
		else if (!strcmp(token, "<TextureCoords0>:"))
		{
			nReads = (UINT)::fread(&nTextureCoords, sizeof(int), 1, inFile);
			if (nTextureCoords > 0)
			{
				type |= VertexTexCoord0;
				textureCoords0 = new XMFLOAT2[nTextureCoords];
				nReads = (UINT)::fread(textureCoords0, sizeof(XMFLOAT2), nTextureCoords, inFile);

				textureCoord0Buffer = ::CreateBufferResource(device, commandList, textureCoords0, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord0UploadBuffer);

				textureCoord0BufferView.BufferLocation = textureCoord0Buffer->GetGPUVirtualAddress();
				textureCoord0BufferView.StrideInBytes = sizeof(XMFLOAT2);
				textureCoord0BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<TextureCoords1>:"))
		{
			nReads = (UINT)::fread(&nTextureCoords, sizeof(int), 1, inFile);
			if (nTextureCoords > 0)
			{
				type |= VertexTexCoord1;
				textureCoords1 = new XMFLOAT2[nTextureCoords];
				nReads = (UINT)::fread(textureCoords1, sizeof(XMFLOAT2), nTextureCoords, inFile);

				textureCoord1Buffer = ::CreateBufferResource(device, commandList, textureCoords1, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord1UploadBuffer);

				textureCoord1BufferView.BufferLocation = textureCoord1Buffer->GetGPUVirtualAddress();
				textureCoord1BufferView.StrideInBytes = sizeof(XMFLOAT2);
				textureCoord1BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<Normals>:"))
		{
			nReads = (UINT)::fread(&nNormals, sizeof(int), 1, inFile);
			if (nNormals > 0)
			{
				type |= VertexNormal;
				normals = new XMFLOAT3[nNormals];
				nReads = (UINT)::fread(normals, sizeof(XMFLOAT3), nNormals, inFile);

				normalBuffer = ::CreateBufferResource(device, commandList, normals, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &normalUploadBuffer);

				normalBufferView.BufferLocation = normalBuffer->GetGPUVirtualAddress();
				normalBufferView.StrideInBytes = sizeof(XMFLOAT3);
				normalBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<Tangents>:"))
		{
			nReads = (UINT)::fread(&nTangents, sizeof(int), 1, inFile);
			if (nTangents > 0)
			{
				type |= VertexTangent;
				tangents = new XMFLOAT3[nTangents];
				nReads = (UINT)::fread(tangents, sizeof(XMFLOAT3), nTangents, inFile);

				tangentBuffer = ::CreateBufferResource(device, commandList, tangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &tangentUploadBuffer);

				tangentBufferView.BufferLocation = tangentBuffer->GetGPUVirtualAddress();
				tangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
				tangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<BiTangents>:"))
		{
			nReads = (UINT)::fread(&nBiTangents, sizeof(int), 1, inFile);
			if (nBiTangents > 0)
			{
				biTangents = new XMFLOAT3[nBiTangents];
				nReads = (UINT)::fread(biTangents, sizeof(XMFLOAT3), nBiTangents, inFile);

				biTangentBuffer = ::CreateBufferResource(device, commandList,biTangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &biTangentUploadBuffer);

				biTangentBufferView.BufferLocation = biTangentBuffer->GetGPUVirtualAddress();
				biTangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
				biTangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<SubMeshes>:"))
		{
			nReads = (UINT)::fread(&(subMeshesNum), sizeof(int), 1, inFile);
			if (subMeshesNum > 0)
			{
				subSetIndicesNum = new int[subMeshesNum];
				subSetIndices = new UINT*[subMeshesNum];

				subSetIndexBuffers = new ID3D12Resource*[subMeshesNum];
				subSetIndexUploadBuffers = new ID3D12Resource*[subMeshesNum];
				subSetIndexBufferViews = new D3D12_INDEX_BUFFER_VIEW[subMeshesNum];

				for (int i = 0; i < subMeshesNum; i++)
				{
					nReads = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
					nReads = (UINT)::fread(token, sizeof(char), strLength, inFile);
					token[strLength] = '\0';
					if (!strcmp(token, "<SubMesh>:"))
					{
						int nIndex = 0;
						nReads = (UINT)::fread(&nIndex, sizeof(int), 1, inFile);
						nReads = (UINT)::fread(&(subSetIndicesNum[i]), sizeof(int), 1, inFile);
						if (subSetIndicesNum[i] > 0)
						{
							subSetIndices[i] = new UINT[subSetIndicesNum[i]];
							nReads = (UINT)::fread(subSetIndices[i], sizeof(UINT)*subSetIndicesNum[i], 1, inFile);

							subSetIndexBuffers[i] = ::CreateBufferResource(device, commandList, subSetIndices[i], sizeof(UINT) * subSetIndicesNum[i], D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &subSetIndexUploadBuffers[i]);

							subSetIndexBufferViews[i].BufferLocation = subSetIndexBuffers[i]->GetGPUVirtualAddress();
							subSetIndexBufferViews[i].Format = DXGI_FORMAT_R32_UINT;
							subSetIndexBufferViews[i].SizeInBytes = sizeof(UINT) * subSetIndicesNum[i];
						}
					}
				}
			}
		}
		else if (!strcmp(token, "</Mesh>"))
		{
			break;
		}
	}
}

void StandardMesh::Render(ID3D12GraphicsCommandList *commandList, int subSet)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);

	D3D12_VERTEX_BUFFER_VIEW pVertexBufferViews[5] = { positionBufferView, textureCoord0BufferView, normalBufferView, tangentBufferView, biTangentBufferView };
	commandList->IASetVertexBuffers(slot, 5, pVertexBufferViews);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], 1, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, 1, offset, 0);
	}
}

void StandardMesh::Render(ID3D12GraphicsCommandList *commandList, int nSubSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);
	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[6];
	vertexBufferViews[0] = positionBufferView;
	vertexBufferViews[1] = textureCoord0BufferView;
	vertexBufferViews[2] = normalBufferView;
	vertexBufferViews[3] = tangentBufferView;
	vertexBufferViews[4] = biTangentBufferView;
	vertexBufferViews[5] = instancingBufferViews;

	commandList->IASetVertexBuffers(slot, 6, vertexBufferViews);

	if ((subMeshesNum > 0) && (nSubSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[nSubSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[nSubSet], instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}
	delete[] vertexBufferViews;
}

void StandardMesh::Render(ID3D12GraphicsCommandList *commandList, int nSubSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, int weaponNum)
{
	if (weaponNum == this->meshnum)
	{
		commandList->IASetPrimitiveTopology(primitiveTopology);
		D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
		vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[6];
		vertexBufferViews[0] = positionBufferView;
		vertexBufferViews[1] = textureCoord0BufferView;
		vertexBufferViews[2] = normalBufferView;
		vertexBufferViews[3] = tangentBufferView;
		vertexBufferViews[4] = biTangentBufferView;
		vertexBufferViews[5] = instancingBufferViews;

		commandList->IASetVertexBuffers(slot, 6, vertexBufferViews);

		if ((subMeshesNum > 0) && (nSubSet < subMeshesNum))
		{
			commandList->IASetIndexBuffer(&(subSetIndexBufferViews[nSubSet]));
			commandList->DrawIndexedInstanced(subSetIndicesNum[nSubSet], instancesNum, 0, 0, 0);
		}
		else
		{
			commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
		}
		delete[] vertexBufferViews;
	}
}


TexturedRectMesh::TexturedRectMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth, float positionX, float positionY, float positionZ) : StandardMesh(device, commandList)
{
	verticesNum = 6;
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	positions = new XMFLOAT3[verticesNum];
	textureCoords0 = new XMFLOAT2[verticesNum];

	float fx = (width * 0.5f) + positionX, fy = (height * 0.5f) + positionY, fz = (depth * 0.5f) + positionZ;
	int i;
	if (width == 0.0f)
	{
		i = 0;
		if (positionX > 0.0f)
		{								
			positions[i] = XMFLOAT3(fx, +fy, -fz),  textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(fx, -fy, -fz),  textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, -fy, +fz),  textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, -fy, +fz),  textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, +fy, +fz),  textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(fx, +fy, -fz),  textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
		else
		{
			positions[i] = XMFLOAT3(fx, +fy, +fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(fx, -fy, +fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, -fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, -fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, +fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(fx, +fy, +fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
	}
	else if (height == 0.0f)
	{
		i = 0;
		if (positionY > 0.0f)
		{
			positions[i] = XMFLOAT3(+fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, fy, -fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
		else
		{
			positions[i] = XMFLOAT3(+fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, fy, +fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
	}
	else if (depth == 0.0f)
	{
		i = 0;
		if (positionZ > 0.0f)
		{
			positions[i] = XMFLOAT3(+fx, +fy, fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, +fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, +fy, fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
		else
		{
			positions[i] = XMFLOAT3(-fx, +fy, fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(-fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(+fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(+fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(+fx, +fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(-fx, +fy, fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
	}

	positionBuffer = CreateBufferResource(device, commandList, positions, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &positionUploadBuffer);
	positionBufferView.BufferLocation = positionBuffer->GetGPUVirtualAddress();
	positionBufferView.StrideInBytes = sizeof(XMFLOAT3);
	positionBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	type |= VertexPosition;
	vertexSize++;

	textureCoord0Buffer = CreateBufferResource(device, commandList, textureCoords0, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord0UploadBuffer);
	textureCoord0BufferView.BufferLocation = textureCoord0Buffer->GetGPUVirtualAddress();
	textureCoord0BufferView.StrideInBytes = sizeof(XMFLOAT2);
	textureCoord0BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
	type |= VertexTexCoord0;
	vertexSize++;

	XMFLOAT3* dummy = new XMFLOAT3;

	normalBuffer = CreateBufferResource(device, commandList, dummy, sizeof(XMFLOAT3), D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &normalUploadBuffer);
	normalBufferView.BufferLocation = normalBuffer->GetGPUVirtualAddress();
	normalBufferView.StrideInBytes = sizeof(XMFLOAT3);
	normalBufferView.SizeInBytes = sizeof(XMFLOAT3);

	tangentBuffer = CreateBufferResource(device, commandList, dummy, sizeof(XMFLOAT3), D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &tangentUploadBuffer);
	tangentBufferView.BufferLocation = tangentBuffer->GetGPUVirtualAddress();
	tangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
	tangentBufferView.SizeInBytes = sizeof(XMFLOAT3);

	biTangentBuffer = CreateBufferResource(device, commandList, dummy, sizeof(XMFLOAT3), D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &biTangentUploadBuffer);
	biTangentBufferView.BufferLocation = biTangentBuffer->GetGPUVirtualAddress();
	biTangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
	biTangentBufferView.SizeInBytes = sizeof(XMFLOAT3);
}

TexturedRectMesh::~TexturedRectMesh()
{
}

SkinnedMesh::SkinnedMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList) : StandardMesh(device, commandList)
{
}

SkinnedMesh::~SkinnedMesh()
{
	if (boneIndices) delete[] boneIndices;
	if (boneWeights) delete[] boneWeights;

	if (skinningBoneFrameCaches) delete[] skinningBoneFrameCaches;
	if (skinningBoneNames) delete[] skinningBoneNames;

	if (xmf4x4BindPoseBoneOffsets) delete[] xmf4x4BindPoseBoneOffsets;
	if (bindPoseBoneOffsets) bindPoseBoneOffsets->Release();

	if (boneIndexBuffer) boneIndexBuffer->Release();
	if (boneWeightBuffer) boneWeightBuffer->Release();

	ReleaseShaderVariables();
}

void SkinnedMesh::CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList)
{
}

void SkinnedMesh::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList, ID3D12Resource* buffers[4])
{
	D3D12_GPU_VIRTUAL_ADDRESS positionWorldGpuVirtualAddress = buffers[0]->GetGPUVirtualAddress();
	commandList->SetGraphicsRootShaderResourceView(GraphicsRootSkinnedPositionWorld, positionWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS normalWorldGpuVirtualAddress = buffers[1]->GetGPUVirtualAddress();
	commandList->SetGraphicsRootShaderResourceView(GraphicsRootSkinnedNormalWorld, normalWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS tangentWorldGpuVirtualAddress = buffers[2]->GetGPUVirtualAddress();
	commandList->SetGraphicsRootShaderResourceView(GraphicsRootSkinnedTangentWorld, tangentWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS biTangentWorldGpuVirtualAddress = buffers[3]->GetGPUVirtualAddress();
	commandList->SetGraphicsRootShaderResourceView(GraphicsRootSkinnedBiTangentWorld, biTangentWorldGpuVirtualAddress);

	commandList->SetGraphicsRoot32BitConstant(GraphicsRootSkinnedVerticesNum, verticesNum, 0);
}

void SkinnedMesh::ReleaseShaderVariables()
{
}

void SkinnedMesh::ReleaseUploadBuffers()
{
	StandardMesh::ReleaseUploadBuffers();

	if (boneIndexUploadBuffer) boneIndexUploadBuffer->Release();
	boneIndexUploadBuffer = NULL;

	if (boneWeightUploadBuffer) boneWeightUploadBuffer->Release();
	boneWeightUploadBuffer = NULL;

	if (bindPoseBoneOffsetsUploadBuffer) bindPoseBoneOffsetsUploadBuffer->Release();
	bindPoseBoneOffsetsUploadBuffer = NULL;
}

void SkinnedMesh::PrepareSkinning(GameObject *pModelRootObject)
{
	for (int j = 0; j < skinningBones; j++)
	{
		skinningBoneFrameCaches[j] = pModelRootObject->FindFrame(skinningBoneNames[j]);
	}
}


void SkinnedMesh::LoadSkinInfoFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, FILE *pInFile)
{
	char pstrToken[64] = { '\0' };
	UINT nReads = 0;

	::ReadStringFromFile(pInFile, meshName);

	for (; ; )
	{
		::ReadStringFromFile(pInFile, pstrToken);
		if (!strcmp(pstrToken, "<BonesPerVertex>:"))
		{
			bonesPerVertex = ::ReadIntegerFromFile(pInFile);
		}
		else if (!strcmp(pstrToken, "<Bounds>:"))
		{
			nReads = (UINT)::fread(&boundingBoxAABBCenter, sizeof(XMFLOAT3), 1, pInFile);
			nReads = (UINT)::fread(&boundingBoxAABBExtents, sizeof(XMFLOAT3), 1, pInFile);
		}
		else if (!strcmp(pstrToken, "<BoneNames>:"))
		{
			skinningBones = ::ReadIntegerFromFile(pInFile);
			if (skinningBones > 0)
			{
				skinningBoneNames = new char[skinningBones][128];
				skinningBoneFrameCaches = new GameObject*[skinningBones];
				for (int i = 0; i < skinningBones; i++)
				{
					::ReadStringFromFile(pInFile, skinningBoneNames[i]);
					skinningBoneFrameCaches[i] = NULL;
				}
			}
		}
		else if (!strcmp(pstrToken, "<BoneOffsets>:"))
		{
			skinningBones = ::ReadIntegerFromFile(pInFile);
			if (skinningBones > 0)
			{
				xmf4x4BindPoseBoneOffsets = new XMFLOAT4X4[skinningBones];
				nReads = (UINT)::fread(xmf4x4BindPoseBoneOffsets, sizeof(XMFLOAT4X4), skinningBones, pInFile);

				XMFLOAT4X4 boneOffset[SKINNED_ANIMATION_BONES];
				for (int i = 0; i < skinningBones; i++)
				{
					XMStoreFloat4x4(&boneOffset[i], XMMatrixTranspose(XMLoadFloat4x4(&xmf4x4BindPoseBoneOffsets[i])));
				}

				UINT ncbElementBytes = (((sizeof(XMFLOAT4X4) * SKINNED_ANIMATION_BONES) + 255) & ~255); //256의 배수
				bindPoseBoneOffsets = ::CreateBufferResource(device, commandList, boneOffset, ncbElementBytes, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &bindPoseBoneOffsetsUploadBuffer);

			}
		}
		else if (!strcmp(pstrToken, "<BoneIndices>:"))
		{
			type |= VERTEXT_BONE_INDEX_WEIGHT;

			verticesNum = ::ReadIntegerFromFile(pInFile);
			if (verticesNum > 0)
			{
				boneIndices = new XMINT4[verticesNum];

				nReads = (UINT)::fread(boneIndices, sizeof(XMINT4), verticesNum, pInFile);
				boneIndexBuffer = ::CreateBufferResource(device, commandList, boneIndices, sizeof(XMINT4) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &boneIndexUploadBuffer);

				boneIndexBufferView.BufferLocation = boneIndexBuffer->GetGPUVirtualAddress();
				boneIndexBufferView.StrideInBytes = sizeof(XMINT4);
				boneIndexBufferView.SizeInBytes = sizeof(XMINT4) * verticesNum;
			}
		}
		else if (!strcmp(pstrToken, "<BoneWeights>:"))
		{
			type |= VERTEXT_BONE_INDEX_WEIGHT;

			verticesNum = ::ReadIntegerFromFile(pInFile);
			if (verticesNum > 0)
			{
				boneWeights = new XMFLOAT4[verticesNum];

				nReads = (UINT)::fread(boneWeights, sizeof(XMFLOAT4), verticesNum, pInFile);
				boneWeightBuffer = ::CreateBufferResource(device, commandList, boneWeights, sizeof(XMFLOAT4) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &boneWeightUploadBuffer);

				boneWeightBufferView.BufferLocation = boneWeightBuffer->GetGPUVirtualAddress();
				boneWeightBufferView.StrideInBytes = sizeof(XMFLOAT4);
				boneWeightBufferView.SizeInBytes = sizeof(XMFLOAT4) * verticesNum;
			}
		}
		else if (!strcmp(pstrToken, "</SkinningInfo>"))
		{
			break;
		}
	}
}

void SkinnedMesh::ComputeSkinning(ID3D12GraphicsCommandList * commandList, UINT instancesNum, ID3D12Resource* buffers[4])
{
	// CBV, SRV
	if (bindPoseBoneOffsets)
	{
		D3D12_GPU_VIRTUAL_ADDRESS boneOffsetsGpuVirtualAddress = bindPoseBoneOffsets->GetGPUVirtualAddress();
		commandList->SetComputeRootConstantBufferView(ComputeRootBoneOffset, boneOffsetsGpuVirtualAddress); //Skinned Bone Offsets
	}
	if (skinningBoneTransforms)
	{
		D3D12_GPU_VIRTUAL_ADDRESS boneTransformsGpuVirtualAddress = skinningBoneTransforms->GetGPUVirtualAddress();
		commandList->SetComputeRootShaderResourceView(ComputeRootBoneTransform, boneTransformsGpuVirtualAddress); //Skinned Bone Transforms
	}
	::ResourceBarrier(commandList, positionBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, normalBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, tangentBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, biTangentBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, boneIndexBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, boneWeightBuffer, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);

	D3D12_GPU_VIRTUAL_ADDRESS positionGpuVirtualAddress = positionBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedPosition, positionGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS normalGpuVirtualAddress = normalBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedNormal, normalGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS tangentGpuVirtualAddress = tangentBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedTangent, tangentGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS biTangentGpuVirtualAddress = biTangentBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedBiTangent, biTangentGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS boneIndexGpuVirtualAddress = boneIndexBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedBoneIndex, boneIndexGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS boneWeightGpuVirtualAddress = boneWeightBuffer->GetGPUVirtualAddress();
	commandList->SetComputeRootShaderResourceView(ComputeRootSkinnedBoneWeight, boneWeightGpuVirtualAddress);

	::ResourceBarrier(commandList, buffers[0], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
	::ResourceBarrier(commandList, buffers[1], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
	::ResourceBarrier(commandList, buffers[2], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
	::ResourceBarrier(commandList, buffers[3], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);

	// UAV
	D3D12_GPU_VIRTUAL_ADDRESS positionWorldGpuVirtualAddress = buffers[0]->GetGPUVirtualAddress();
	commandList->SetComputeRootUnorderedAccessView(ComputeRootSkinnedPositionWorld, positionWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS normalWorldGpuVirtualAddress = buffers[1]->GetGPUVirtualAddress();
	commandList->SetComputeRootUnorderedAccessView(ComputeRootSkinnedNormalWorld, normalWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS tangentWorldGpuVirtualAddress = buffers[2]->GetGPUVirtualAddress();
	commandList->SetComputeRootUnorderedAccessView(ComputeRootSkinnedTangentWorld, tangentWorldGpuVirtualAddress);

	D3D12_GPU_VIRTUAL_ADDRESS biTangentWorldGpuVirtualAddress = buffers[3]->GetGPUVirtualAddress();
	commandList->SetComputeRootUnorderedAccessView(ComputeRootSkinnedBiTangentWorld, biTangentWorldGpuVirtualAddress);

	commandList->SetComputeRoot32BitConstant(ComputeRootSkinnedVerticesNum, verticesNum, 0);

	commandList->Dispatch(static_cast<int>(ceil(verticesNum / BLOCKSIZE)), instancesNum, 1);

	::ResourceBarrier(commandList, buffers[0], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, buffers[1], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, buffers[2], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, buffers[3], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);


	::ResourceBarrier(commandList, positionBuffer, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, normalBuffer, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, tangentBuffer, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, biTangentBuffer, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, boneIndexBuffer, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
	::ResourceBarrier(commandList, boneWeightBuffer, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER);
}

void SkinnedMesh::OnPreRender(ID3D12GraphicsCommandList *commandList, UINT instancesNum, ID3D12Resource* buffers[4])
{
	// ComputeShader Skinning
	ComputeSkinning(commandList, instancesNum, buffers);

}

void SkinnedMesh::Render(ID3D12GraphicsCommandList *commandList, int subSet, ID3D12Resource* buffers[4])
{
	// Set SRV, CBV
	UpdateShaderVariables(commandList, buffers);

	D3D12_VERTEX_BUFFER_VIEW pVertexBufferViews[7] = { positionBufferView, textureCoord0BufferView, normalBufferView, tangentBufferView, biTangentBufferView, boneIndexBufferView, boneWeightBufferView };
	commandList->IASetVertexBuffers(slot, 7, pVertexBufferViews);

	commandList->IASetPrimitiveTopology(primitiveTopology);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], 1, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, 1, offset, 0);
	}
}

void SkinnedMesh::Render(ID3D12GraphicsCommandList *commandList, int subSet, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, ID3D12Resource* buffers[4])
{
	// Set SRV, CBV
	UpdateShaderVariables(commandList, buffers);

	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[8];
	vertexBufferViews[0] = positionBufferView;
	vertexBufferViews[1] = textureCoord0BufferView;
	vertexBufferViews[2] = normalBufferView;
	vertexBufferViews[3] = tangentBufferView;
	vertexBufferViews[4] = biTangentBufferView;
	vertexBufferViews[5] = boneIndexBufferView;
	vertexBufferViews[6] = boneWeightBufferView;
	vertexBufferViews[7] = instancingBufferViews;

	commandList->IASetVertexBuffers(slot, 8, vertexBufferViews);
	commandList->IASetPrimitiveTopology(primitiveTopology);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}
	delete[] vertexBufferViews;
}

SkyBoxMesh::SkyBoxMesh(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, float width, float height, float depth) : Mesh(device, commandList)
{
	verticesNum = 36;
	int stride = sizeof(TexturedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	float x = width * 0.5f, y = height * 0.5f, z = depth * 0.5f;

	int i = 0;
	texturedVertex = new TexturedVertex[36];

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i] = TexturedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT2(0.0f, 1.0f));

	vertexBuffer = ::CreateBufferResource(device, commandList, texturedVertex, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}

SkyBoxMesh::~SkyBoxMesh()
{
}

UiMesh::UiMesh(ID3D12Device * device, ID3D12GraphicsCommandList * commandList) : Mesh(device, commandList)
{
	verticesNum = 300;
	int stride = sizeof(UiVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	float w, h, x, y;

	int i = 0;
	float textureidx = 0.0f;
	float sceneidx = 0.0f;
	float playeridx = 0.0f;
	texturedVertex = new UiVertex[verticesNum];

	// 인게임 UI
	// 아이템 획득시 나올 화면
	// 36 ~ 46 악세서리 텍스쳐 번호
	// 총 캐릭터 장비 획득 UI :47 ~ 52 검 : 53 ~ 58 활 : 59 ~ 63
	// 왼쪽 현재 장착중인 아이템
	textureidx = 36.0f;
	sceneidx = SCENE_INGAME_ITEM;
	w = 0.3664;
	h = 0.807;
	x = -0.4f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 오른쪽 스테이지 클리어 후 얻은 아이템
	textureidx = 37.0f;
	x = 0.4f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));


	// other player icon 
	// 플레이어 1
	sceneidx = SCENE_INGAME;
	textureidx = 0.0f;
	w = 0.38 * 0.5;
	h = 0.24 * 0.5;
	x = 0.8f;
	y = -0.39f;
	playeridx = 1.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 플레이어 2
	y = -0.63f;
	playeridx = 2.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));            
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));     

	// 플레이어 3
	y = -0.87f;
	playeridx = 3.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	
	// 다른 플레이어 이름
	w = 0.38 * 0.5;
	h = 0.24 * 0.5;
	x = 0.8f;
	y = -0.39f;
	playeridx = 1.0f;
	textureidx = 79.f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 플레이어 2
	y = -0.63f;
	playeridx = 2.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 플레이어 3
	y = -0.87f;
	playeridx = 3.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// hp, stamina bar
	// hp bar
	playeridx = 0.0f;
	textureidx = 3.0f;
	w = 1 * 0.5;
	h = 0.2 * 0.5;
	x = 0.0f;
	y = -0.88f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 다른 플레이어 체력
	playeridx = 1.0f;
	textureidx = 3.0f;
	w = 0.113;
	h = 0.0244;
	x = 0.737f;
	y = -0.45f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 2.0f;
	textureidx = 3.0f;
	x = 0.737f;
	y = -0.69f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 3.0f;
	textureidx = 3.0f;
	x = 0.737f;
	y = -0.93f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// stamina bar
	textureidx = 4.0f;
	w = 1 * 0.5;
	h = 0.05 * 0.5;
	x = 0.0f;
	y = -0.73f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// Map(5) , 지나간 맵 위치 6 , 현재 위치 7
	w = 0.18 * 0.5 * 0.2725;
	h = 0.32 * 0.5 * 0.2725;
	textureidx = 5.0f;

	playeridx = 0.0f;
	x = 0.84f;
	y = 0.931f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 1.0f;
	x = 0.9f;
	y = 0.931f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 2.0f;
	x = 0.961f;
	y = 0.931f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 3.0f;
	x = 0.84f;
	y = 0.825f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 4.0f;
	x = 0.9f;
	y = 0.825f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 5.0f;
	x = 0.961f;
	y = 0.825f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 6.0f;
	x = 0.84f;
	y = 0.717f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 7.0f;
	x = 0.9f;
	y = 0.717f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 8.0f;
	x = 0.961f;
	y = 0.717f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// All Map
	textureidx = 5.0f;
	playeridx = 9.0f;
	w = 0.18 * 0.5;
	h = 0.32 * 0.5;
	x = 0.901f;
	y = 0.824f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 총 캐릭터 뒷 탄창 + 남은 탄창
	textureidx = 84.0f;
	w = 0.0375;
	h = 0.0958;
	x = -0.6f;
	y = -0.9f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	textureidx = 83.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// accessories and weapon
	// accessories 8 ~ 18
	textureidx = 8.0f;
	w = 0.18 * 0.5;
	h = 0.36 * 0.5;
	x = -0.89f;
	y = -0.8f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// weapon 총 19 ~ 24, 칼 25 ~ 30, 활 31 ~ 35
	textureidx = 19.0f;
	w = 0.18 * 0.5;
	h = 0.36 * 0.5;
	x = -0.69f;
	y = -0.8f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 조준선
	textureidx = 72.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));


	// 줌화면
	textureidx = 73.0f;
	sceneidx = 3.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 대기화면 유아이 16 : 9
	// 각 플레이어 별로 있어야함 총 4개
	// 선택한 플레이어 아이콘 총 65 검 66 활 67
	sceneidx = SCENE_LOBBY;

	textureidx = 65.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	playeridx = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	y = -0.315f;
	playeridx = 1.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	y -= 0.315f;
	playeridx = 2.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	y -= 0.315f;
	playeridx = 3.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 준비 버튼 눌리면 해당 이미지로 변경, textureidx = 69 <- 로비 내의 모든 플레이어가 준비 되면 게임 시작 버튼
	playeridx = 0.0f;
	textureidx = 68.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 준비 완료 아이콘
	// 모든 플레이어에게 있어야함 총 4개 
	textureidx = 70.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	playeridx = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	y = -0.315f;
	playeridx = 1.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	y = -0.632f;
	playeridx = 2.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	y = -0.947f;
	playeridx = 3.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));


	// 캐릭터 초상화 위 테두리
	playeridx = 0.0f;
	textureidx = 71.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	// 3개 중 1개 위에 있어야함 x 값만 변화시키면 됨
	x = -0.597f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 1.0f;
	x = -0.298f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	playeridx = 2.0f;
	x = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 로비 화면 플레이어
	sceneidx = SCENE_LOBBY;
	textureidx = 75.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	textureidx = 76.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	textureidx = 77.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	textureidx = 78.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 베이스 대기화면
	textureidx = 64.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 시작화면
	sceneidx = SCENE_START;
	textureidx = 74.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 로딩화면
	sceneidx = SCENE_LOADING;
	textureidx = 85.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));


	// 게임 오버
	sceneidx = SCENE_GAMEOVER;
	textureidx = 86.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	// 승리화면
	sceneidx = SCENE_VICTORY;
	textureidx = 87.0f;
	w = 2.0 * 0.5;
	h = 2.0 * 0.5;
	x = 0.0f;
	y = 0.0f;
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y + h, 0), XMFLOAT2(1.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y + h, 0), XMFLOAT2(0.0f, 0.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x + w, y - h, 0), XMFLOAT2(1.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));
	texturedVertex[i++] = UiVertex(XMFLOAT3(x - w, y - h, 0), XMFLOAT2(0.0f, 1.0f), XMFLOAT3(textureidx, sceneidx, playeridx));

	vertexBuffer = ::CreateBufferResource(device, commandList, texturedVertex, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}

UiMesh::~UiMesh()
{
}


BoundingBoxMesh::BoundingBoxMesh(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, float width, float height, float depth) : Mesh(device, commandList)
{
	verticesNum = 36;//버텍스 개수 6개
	int stride = sizeof(DiffusedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	DiffusedVertex* vertices = new DiffusedVertex[36];
	float x = (width), y = (height), z = (depth);
	int i = 0;

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	//삼각형 메쉬를 리소스(정점 버퍼)로 생성한다.

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;

}

BoundingBoxMesh::BoundingBoxMesh(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT3 center, float width, float height, float depth) : Mesh(device, commandList)
{
	verticesNum = 36;//버텍스 개수 6개
	int stride = sizeof(DiffusedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	DiffusedVertex* vertices = new DiffusedVertex[36];
	float x = (width), y = (height), z = (depth);
	int i = 0;

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	//삼각형 메쉬를 리소스(정점 버퍼)로 생성한다.

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;

}


void BoundingBoxMesh::Render(ID3D12GraphicsCommandList * commandList, int subsetNum, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferViews, int weaponNum)
{
	if (weaponNum != -1)
		if ((this->meshnum >= MODEL_WEAPON_START && this->meshnum <= MODEL_WEAPON_END) && (this->meshnum != (MODEL_WEAPON_START + weaponNum))) {
			return;
		}
	commandList->IASetPrimitiveTopology(primitiveTopology);
	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[2];
	vertexBufferViews[0] = vertexBufferView;
	vertexBufferViews[1] = instancingBufferViews;

	commandList->IASetVertexBuffers(slot, 2, vertexBufferViews);

	if ((subMeshesNum > 0) && (subsetNum < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subsetNum]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subsetNum], instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}

	delete[] vertexBufferViews;
}

BoundingBoxMesh::~BoundingBoxMesh()
{
}