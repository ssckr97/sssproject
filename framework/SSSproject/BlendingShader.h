#pragma once
#include "Shader.h"

struct BlendingBuffer
{
	std::vector<GameObject*> objects;
	int objectsNum = 0;

	int useBillboard = -1;
	int isBillboard = -1;
	// 빌보드 사각형의 idx정보

	int framesNum;

	VS_VB_INSTANCE_OBJECT** worldCashe = NULL;
	bool* isVisable = NULL;
	int renderObjNum = 0;
	// [][] : Objects, Frames

	ID3D12Resource*** vbGameObjects = NULL;
	VS_VB_INSTANCE_OBJECT*** vbMappedGameObjects = NULL;
	// [][][] : Camera, Frames, Objects
	D3D12_VERTEX_BUFFER_VIEW** instancingGameObjectBufferView = NULL;
	UINT vbGameObjectBytes = 0;

	UINT pipeLineStateIdx = 0;
	UINT shadowPipeLineStateIdx = 0;

	TEXTURENAME textureName;
	int diffuseIdx = -1;
	int normalIdx = -1;
	int specularIdx = -1;
	int metallicIdx = -1;
	int emissionIdx = -1;
	// Scene::rootArgument의 idx 값.

	BoundingOrientedBox obb;
};

struct BillboardBuffer
{
	std::vector<GameObject*> objects;
	int objectsNum = 0;

	int billboardIdx = -1;
	// 빌보드 사각형의 idx정보 (BillboardLOD를 할 때 이 값을 통해 찾아낸다.)

	ID3D12Resource* vbBillboardObjects = NULL;
	VS_VB_INSTANCE_BILLBOARD* vbMappedBillboardObjects = NULL;
	D3D12_VERTEX_BUFFER_VIEW instancingBillboardObjectBufferView;
	UINT vbBillboardObjectBytes = 0;

	UINT pipeLineStateIdx = 0;
	UINT shadowPipeLineStateIdx = 0;

	TEXTURENAME textureName;
	int billboardTexIdx = -1;
	// Scene::rootArgument의 idx 값.
};

class BlendingShader : public StandardInstanceShader
{
protected:

	std::vector<GameObject*> objects;
	// terrain, helicoptor, tree ... 오브젝트 데이터를 관리하는 벡터.
	std::vector<BlendingBuffer*> instanceBuffer;
	// 오브젝트들과 오브젝트들의 버퍼 등을 관리하는 벡터.
	std::vector<Texture*> textures;


public:
	BlendingShader();
	virtual ~BlendingShader();

	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual D3D12_SHADER_BYTECODE CreateShadowPixelShader();
	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx = 0);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, UINT cameraIdx);
	virtual void ReleaseShaderVariables();

	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context, int hObjectSize = 0);
	virtual void ReleaseObjects();
	virtual void ReleaseUploadBuffers();
	void BuildInstanceObject(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
		BlendingBuffer * instanceObject, int objectNum, int pipeLineStateIdx, GameObject * objectModel, int modelNumber, XMFLOAT3 boundingSize, XMFLOAT3 rotate, XMFLOAT3 position);

	std::vector<BlendingBuffer*> GetBuffer() { return instanceBuffer; }

	void SetTextures(ID3D12Device* deivce, ID3D12GraphicsCommandList* commandList);

	virtual void DeleteAllObjcet();
	virtual void LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType);

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx, bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList* commandList, bool isShadow, UINT cameraIdx);

	virtual void ApplyLOD(Camera* camera, int idx);

};

class BillboardShader : public Shader
{
	std::vector<BillboardBuffer*> instanceBuffer;
	// 오브젝트들과 오브젝트들의 버퍼 등을 관리하는 벡터.
	std::vector<Texture*> textures;


public:
	BillboardShader();
	virtual ~BillboardShader();

	virtual D3D12_BLEND_DESC CreateBlendState();

	D3D12_INPUT_LAYOUT_DESC CreateInputLayout();

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateGeomtryShader();

	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowPixelShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowGeomtryShader();

	D3D12_PRIMITIVE_TOPOLOGY_TYPE SetTopologyType();

	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx = 0);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, UINT cameraIdx);
	virtual void ReleaseShaderVariables();

	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context);
	virtual void ReleaseObjects();
	virtual void ReleaseUploadBuffers();

	std::vector<BillboardBuffer*> GetBuffer() { return instanceBuffer; }

	void SetTextures(ID3D12Device* deivce, ID3D12GraphicsCommandList* commandList);

	virtual void DeleteAllObjcet();

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx, bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList* commandList, bool isShadow, UINT cameraIdx);

	virtual void ApplyLOD(Camera* camera, int idx);
};