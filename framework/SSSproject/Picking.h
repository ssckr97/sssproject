#pragma once
#include "ComputeShader.h"

class Picking
{
private:
	XMFLOAT3 direction;
	int pickingIndex;
	GameObject* oldObject = NULL;
	GameObject* selectObject = NULL;
	bool isPicking = true;


public:
	bool Getpicking() { return isPicking; }
	void Setpicking(bool isPicking) { this->isPicking = isPicking; }
	//피킹 성공 여부 
	bool CheckPicking(POINT& cursur, XMFLOAT3 rayOrigin, XMFLOAT3 rayDirection, int windowClientWidth, int windowClientHeight, BoundingOrientedBox obb, XMFLOAT3& pickPlace, float& distance);
	bool CheckPicking(POINT& cursur, XMFLOAT3 rayOrigin, XMFLOAT3 rayDirection, int windowClientWidth, int windowClientHeight, BoundingBox obb, XMFLOAT3& pickPlace, float& distance);

	XMFLOAT3 GetDirection() const { return direction; }
	GameObject* GetOldObject() { return oldObject; }
	GameObject* GetSelectObject() { return selectObject; }
	int GetSelectIndex() { return pickingIndex; }

	void SetOldObject(GameObject* oldObject) { this->oldObject = oldObject; }
	void SetSelectObject(GameObject *selectObject) { this->selectObject = selectObject; }
	void SetSelectIndex(int index) { pickingIndex = index; }
	void SetDirection(XMFLOAT3& direction) { this->direction = direction; }
};
