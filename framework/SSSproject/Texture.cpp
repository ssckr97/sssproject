#include "stdafx.h"
#include "Texture.h"


Texture::Texture(int texturesNum, UINT textureType, int samplers)
{
	this->textureType = textureType;
	this->texturesNum = texturesNum;
	this->samplers = samplers;

	if (texturesNum > 0)
	{
		rootArgumentInfos = new SRVROOTARGUMENTINFO[this->texturesNum];
		textureUploadBuffers = new ID3D12Resource*[this->texturesNum];
		textures = new ID3D12Resource*[this->texturesNum];
		for (int i = 0; i < texturesNum; ++i)
			textureUploadBuffers[i] = NULL;
	}

	if (this->samplers > 0)
		samplerGpuDescriptorHandles = new D3D12_GPU_DESCRIPTOR_HANDLE[this->samplers];
}

Texture::~Texture()
{
	if (textures)
	{
		for (int i = 0; i < texturesNum; i++)
			if (textures[i])
			{
				textures[i]->Release();
				textures[i] = NULL;
			}
	}

	if (rootArgumentInfos)
	{
		delete[] rootArgumentInfos;
	}

	if (samplerGpuDescriptorHandles)
		delete[] samplerGpuDescriptorHandles;
}

void Texture::SetRootArgument(int idx, UINT rootParameterIndex, D3D12_GPU_DESCRIPTOR_HANDLE srvGpuDescriptorHandle)
{
	rootArgumentInfos[idx].rootParameterIndex = rootParameterIndex;
	rootArgumentInfos[idx].srvGpuDescriptorHandle = srvGpuDescriptorHandle;
}

void Texture::SetSampler(int idx, D3D12_GPU_DESCRIPTOR_HANDLE samplerGpuDescriptorHandle)
{
	this->samplerGpuDescriptorHandles[idx] = samplerGpuDescriptorHandle;
}

void Texture::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList)
{
	if (textureType == ResourceTexture2D_Array)
	{
		commandList->SetGraphicsRootDescriptorTable(rootArgumentInfos[0].rootParameterIndex, rootArgumentInfos[0].srvGpuDescriptorHandle);
	}
	else
	{
#ifdef SEPARATE_DESCRIPTOR_RANGE
		for (int i = 0; i < textures; i++)
		{
			commandList->SetGraphicsRootDescriptorTable(m_pRootArgumentInfos[i].m_nRootParameterIndex, m_pRootArgumentInfos[i].m_d3dSrvGpuDescriptorHandle);
		}
#else
		commandList->SetGraphicsRootDescriptorTable(rootArgumentInfos[0].rootParameterIndex, rootArgumentInfos[0].srvGpuDescriptorHandle);
#endif
	}
}

void Texture::UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, int nIndex)
{
	commandList->SetGraphicsRootDescriptorTable(rootArgumentInfos[nIndex].rootParameterIndex, rootArgumentInfos[nIndex].srvGpuDescriptorHandle);
}

void Texture::ReleaseUploadBuffers()
{
	if (textureUploadBuffers)
	{
		for (int i = 0; i < texturesNum; i++)
			if (textureUploadBuffers[i])
				textureUploadBuffers[i]->Release();
		delete[] textureUploadBuffers;
		textureUploadBuffers = NULL;
	}
}

void Texture::ReleaseShaderVariables()
{
}

void Texture::LoadTextureFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, wchar_t *fileName, UINT nIndex, bool isDDSFile)
{
	if (isDDSFile)
		textures[nIndex] = ::CreateTextureResourceFromFile(device, commandList, fileName, &textureUploadBuffers[nIndex], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);
}

void Texture::SetTexture(ID3D12Resource * resource, int idx)
{
	textures[idx] = resource;
}
