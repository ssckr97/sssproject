#pragma once
#include "Mesh.h"
#include "Camera.h"

class Shader;
class StandardShader;
class Texture;
class GameObject;
class AnimationController;
class LoadedModelInfo;
class Material;

struct MATERIAL;
struct VS_VB_INSTANCE_OBJECT;
struct VS_VB_INSTANCE_MATERIAL;
struct VS_VB_INSTANCE_BILLBOARD;
struct VS_VB_INSTANCE_PARTICLE;
struct VS_VB_INSTANCE_BOUNDING;

struct CB_GAMEOBJECT_INFO;

struct TEXTURENAME
{
	std::vector<std::pair<int, _TCHAR*>> diffuse;
	std::vector<std::pair<int, _TCHAR*>> normal;
	std::vector<std::pair<int, _TCHAR*>> specular;
	std::vector<std::pair<int, _TCHAR*>> metallic;

	int GetSize() {
		return (int)diffuse.size() + (int)normal.size() + (int)specular.size() + (int)metallic.size();
	}
};

struct WeaponPoint {
	XMFLOAT4X4 start;
	XMFLOAT4X4 end;

	void Clear()
	{
		start._11 = 0.0f; start._12 = 0.0f; start._13 = 0.0f; start._14 = 1.0f;
		start._21 = 0.0f; start._22 = 0.0f; start._23 = 0.0f; start._24 = 1.0f;
		start._31 = 0.0f; start._32 = 0.0f; start._33 = 0.0f; start._34 = 1.0f;
		start._41 = 0.0f; start._42 = 0.0f; start._43 = 0.0f; start._44 = 1.0f;

		end._11 = 0.0f; end._12 = 0.0f; end._13 = 0.0f; end._14 = 1.0f;
		end._21 = 0.0f; end._22 = 0.0f; end._23 = 0.0f; end._24 = 1.0f;
		end._31 = 0.0f; end._32 = 0.0f; end._33 = 0.0f; end._34 = 1.0f;
		end._41 = 0.0f; end._42 = 0.0f; end._43 = 0.0f; end._44 = 1.0f;
	}
};

class GameObject
{
private:
	int references = 0;

public:
	GameObject(int meshesNum = 1);
	virtual ~GameObject();
	void AddRef();
	void Release();

	int animateSpeed = 0;

	GameObject* toParent;

	float hp = 100.0f;
	float stemina = 100.0f;
	float strikingPower = 10.0f;
	float defencivePower = 10.0f;
	float arrowSpeed = 80.0f;
	float arrowAge = 0.0f;
	float arrowTheta = 0.0f;
	float arrowTrailCoolTime = -1.0f;
	float zombieDeadAge = 0.0f;				// 이 값을 통해 좀비가 죽은 시간을 확인하여 10초이상이 지났을 경우 맵에서 사라지게 만들어준다.
	float zombieAttackCoolTime = -1.0f;
	int id;
	WeaponPoint prePos;
	WeaponPoint curPos;

	bool isLive = true;							// 이 값을 통해 좀비가 죽었는지를 확인한다. (hp가 0이하일 경우 false)
	bool needDelete = false;					// 이 값을 통해 좀비를 렌더링할지 말지 여부를 결정한다. (isLive가 false가 된 후 10초가 지났을 경우 true)

	bool useShadow = true;
	bool isMove = false;
	bool isCollisionSkinnedModel = false;

protected:

	char frameName[128] = { '\0' };

	Mesh **meshes = NULL;
	int meshesNum = 0;

	Material** materials = NULL;
	int materialsNum = 1;

	XMFLOAT4X4 transform;
	XMFLOAT4X4 world;
	UINT pipeLineStatesNum = 0;
	int framesNum = 0;
	int maximumMaterialsNum = 0;

	GameObject 					*parent = NULL;
	GameObject 					*child = NULL;
	GameObject 					*sibling = NULL;

	D3D12_GPU_DESCRIPTOR_HANDLE*	cbvGPUDescriptorHandle = NULL;
	int handlesNum = 0;

	ID3D12Resource					*cbGameObject = NULL;
	CB_GAMEOBJECT_INFO				*cbMappedGameObject = NULL;

	int								type = -1;
	// Object Type
	UINT							reflection = 0;
	// Material Number
	UINT							textureMask = 0x00;
	// Texture Number
	UINT							textureIdx = -1;

	UINT							shaderType = 0;

	XMFLOAT3 inputangle{ 0,0,0 };
	XMFLOAT3 rotatingAngle = { 0 , 0  , 0 };
	XMFLOAT3 allScale = { 1.0f  , 1.0f , 1.0f };
	XMFLOAT3 tPos{ 0.0f , 0.0f , 0.0f };

	BoundingOrientedBox				obbBox;
	BoundingOrientedBox				colObbBox;
	BoundingBox						aabbBox;

	BoundingBoxMesh *boundingMesh = NULL;
	std::vector<GameObject*>* boundingObject = NULL;
	std::vector<BoundingOrientedBox> obbvec;
	int direction = DirectionNoMove;
	int state;
	float mass = 6.0f;
	XMFLOAT3 acc;
	XMFLOAT3 velocity;
	XMFLOAT3 gravity;
	XMFLOAT3 colPos;
	XMFLOAT3 finalvec;
	
	//플레이어에 작용하는 중력을 나타내는 벡터이다.
	float maxVelocityXZ = 6.0f;
	//xz-평면에서 (한 프레임 동안) 플레이어의 이동 속력의 최대값을 나타낸다.
	float maxVelocityY;
	//y-축 방향으로 (한 프레임 동안) 플레이어의 이동 속력의 최대값을 나타낸다.
	float friction = 20.0f;

	std::chrono::high_resolution_clock::time_point ctime;

public:
	TEXTURENAME						textureName;
	AnimationController 			*skinnedAnimationController = NULL;

	int animationStatus = MainCharacterIdle;
	int upAnimationStatus = MainCharacterIdle;
	int loAnimationStatus = MainCharacterIdle;

	void ReleaseUploadBuffers();
	
	virtual void SetMesh(int idx, Mesh *mesh);
	virtual void SetBoundingMesh(BoundingBoxMesh* mesh);
	virtual void SetShader(Shader *shader);
	virtual void SetMaterial(int idx, Material *material);
	virtual void SetMaterial(int idx, UINT reflection);
	virtual void SetReflection(UINT reflection);
	virtual void SetTextureMask(UINT texturemask);
	virtual void SetTextureIdx(UINT idx);
	virtual void SetChild(GameObject* child);
	virtual void SetChild(GameObject* child, bool referenceUpdate);
	virtual void SetPipelineStatesNum(UINT type);
	virtual void AddFramesNum() { framesNum++; }
	virtual void GetHierarchyMaterial(std::vector<MATERIAL>& materialReflection, int& idx);
	virtual UINT GetReflection();
	virtual UINT GetTextureMask();
	virtual int GetFramesNum();
	virtual GameObject* GetChild() { return child; }
	virtual int GetMaterialsNum() { return materialsNum; }
	virtual int GetMaximumMaterialsNum() { return maximumMaterialsNum; }
	virtual int GetPipeLineStatesNum() { return pipeLineStatesNum; }
	virtual void CreateMaterials(UINT materialsNum);
	virtual BoundingOrientedBox& GetOBB() { return obbBox; }
	virtual BoundingBox& GetAABB() { return aabbBox; }
	
	BoundingOrientedBox& GetColOBB() { return colObbBox; }

	std::vector<GameObject*>* GetBoundingObject() { return boundingObject; }
	void SetBoundingObject(std::vector<GameObject*>* tempObject) { this->boundingObject = tempObject; }
	void allocateBvec() { boundingObject = new std::vector<GameObject*>; }

	virtual Mesh* GetMesh(int idx) { return meshes[idx]; };


	void CreateCbvGPUDescriptorHandles(int handlesNum = 1);
	void SetCbvGPUDescriptorHandle(D3D12_GPU_DESCRIPTOR_HANDLE cbvGPUDescriptorHandle, int idx = 0) { this->cbvGPUDescriptorHandle[idx] = cbvGPUDescriptorHandle; }
	void SetCbvGPUDescriptorHandlePtr(UINT64 cbvGPUDescriptorHandlePtr, int idx = 0) { cbvGPUDescriptorHandle[idx].ptr = cbvGPUDescriptorHandlePtr; }
	void SetCbvGPUDescriptorHandlePtrHierarchy(UINT64 cbvGPUDescriptorHandlePtr, int idx, int objectsNum);
	void SetMaterialCbvGPUDescriptorHandlePtrHierarchy(UINT64 cbvGPUDescriptorHandlePtr, int maximum);
	void SetCbvGPUDescriptorHandlesOnlyStandardMesh(D3D12_GPU_DESCRIPTOR_HANDLE handle, int objectsNum = 1, int framesNum = 1);


	D3D12_GPU_DESCRIPTOR_HANDLE GetCbvGPUDescriptorHandle(int idx = 0) { return(cbvGPUDescriptorHandle[idx]); }

	virtual void PrepareAnimate() { }
	virtual void Animate(float timeElapsed, XMFLOAT4X4* parent = NULL);

	virtual std::vector<BoundingOrientedBox>& GetobbVector();

	virtual void OnPrepareRender();
	virtual void Render(ID3D12GraphicsCommandList *commandList, int idx = 0);
	virtual void Render(ID3D12GraphicsCommandList *commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList *commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, ID3D12Resource** buffers[4], bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList *commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, ID3D12Resource** buffers[4], bool isShadow, int weapon0, int weapon1 = -1);
	virtual void Render(ID3D12GraphicsCommandList* commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingStandardBufferView, D3D12_VERTEX_BUFFER_VIEW instancingObjBufferView, ID3D12Resource** buffers[4], bool isShadow);

	virtual void BillboardRender(ID3D12GraphicsCommandList* commandList, Camera* camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferView);
	virtual void BoundingRender(ID3D12GraphicsCommandList * commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingGameObjectBufferView);
	virtual void SkinnedBoundingRender(ID3D12GraphicsCommandList * commandList, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, int weaponNum);

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);

	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, int idx);
	virtual void ReleaseShaderVariables();

	virtual void BuildMaterials(ID3D12Device *device, ID3D12GraphicsCommandList *commandList) { }

	virtual void ChangePlayerAnimation(int status1, int status2, int character);


	XMFLOAT3 GetVelocity();
	XMFLOAT3 GetPosition();
	XMFLOAT3 GetTransformPosition();
	XMFLOAT3 GetLook();
	XMFLOAT3 GetUp();
	XMFLOAT3 GetRight();
	XMFLOAT3& GetLookRefernce();
	XMFLOAT3  GetFinalVec(GameObject* ob, float force, XMFLOAT3 dir , float elapsedTime);
	int Getid() { return id; }
	XMFLOAT4X4 GetWorld();
	XMFLOAT4X4 GetTransform();
	float	   GetDir() { return direction; }
	int		   GetState() { return state; }
	Material* GetMaterial(int idx);
	UINT GetTextureIdx() { return textureIdx; }
	XMFLOAT3 GetAllScale();
	XMFLOAT3& GetInputAngle();
	
	std::chrono::high_resolution_clock::time_point GetTime() { return ctime; }
	int CompareName(char name[]) { 	return strcmp(frameName, name); };

	void SetVelocity(XMFLOAT3 velocity);
	void SetPosition(float x, float y, float z);
	void SetPosition(XMFLOAT3 position);
	void SetScale(float x, float y, float z);
	void SetWorld(XMFLOAT4X4 world);
	void SetTransform(XMFLOAT4X4 transform);
	void SetLook(XMFLOAT3 look);
	void SetRight(XMFLOAT3 right);
	void SetUp(XMFLOAT3 up);
	void SetAllScale(float x, float y, float z);
	void SettPos(XMFLOAT3 position);
	void SetVel(XMFLOAT3 vel);
	void Sethp(int hp);
	void Setid(int id);
	void SetDir(int dir);
	void SetTime(std::chrono::high_resolution_clock::time_point time);
	void SetColPos(XMFLOAT3 pos) { colPos = pos; }
	void MoveStrafe(float distance = 1.0f);
	void MoveUp(float distance = 1.0f);
	void MoveForward(float distance = 1.0f);
	void Move(int direction, float force, bool updateVelocity , float elapsedTime , std::vector<GameObject*>& tZombies);
	void RealMove();
	void SetState(int state) { this->state = state; }


	void Rotate(XMFLOAT3* axis, float angle);
	void Rotate(float pitch = 10.0f, float yaw = 10.0f, float roll = 10.0f);
	void Rotate(XMFLOAT4 *quaternion);

	GameObject *GetParent() { return(parent); }
	int  GetParentFrame(char name[]);
	void UpdateTransform(XMFLOAT4X4 *parent);
	void UpdateTransform(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, XMFLOAT4X4 *parent = NULL);
	void UpdateTransform(VS_VB_INSTANCE_OBJECT** vbMappedGameObjects, int idx, XMFLOAT4X4 * parent = NULL);
	void UpdateTransformOnlyStandardMesh(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, int framesNum, XMFLOAT4X4 *parent);
	WeaponPoint UpdateTransformOnlyStandardMesh(VS_VB_INSTANCE_OBJECT** vbMappedGameObjects, int idx, int framesNum, int weaponNum, XMFLOAT4X4 *parent, XMFLOAT4X4* toArrow);

	void UpdateBoundingTransForm(VS_VB_INSTANCE_BOUNDING * vbMappedBoudnigs, int idx, XMFLOAT4X4 pWorld, int bidx);
	void UpdateSkinnedBoundingTransform(VS_VB_INSTANCE_BOUNDING** vbMappedBoundings, std::vector<BoundingOrientedBox>& obbs, std::vector<GameObject*>* objVec, int idx, int framesNum, XMFLOAT4X4* parent);


	virtual void UpdateTransform(VS_VB_INSTANCE_BILLBOARD* vbMappedBillboardObject, int idx) {}
	virtual void UpdateTransform(VS_VB_INSTANCE_PARTICLE* vbMappedBillboardObject, int idx) {}

	GameObject *FindFrame(char *pstrFrameName);
	
	_TCHAR* FindReplicatedTexture(_TCHAR* textureName);

	UINT GetMeshType(int idx = 0) { return((meshes[idx]) ? meshes[idx]->GetType() : 0x00); }
	UINT GetShaderType() { return shaderType; }

	void FindAndSetSkinnedMesh(SkinnedMesh **SkinnedMeshes, int *SkinnedMesh);
	 
	void LoadMaterialsFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, GameObject *parent, FILE *inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5]);

	static LoadedModelInfo *LoadGeometryAndAnimationFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, char *fileName, char* folderName);
	static GameObject *LoadFrameHierarchyFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, GameObject *parent, FILE *inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5], int& frameCounter, int& materialCounter, int* skinnedMeshesNum, int* standardMeshCounter, int* boundingMeshNum);
	static void LoadAnimationFromFile(FILE *inFile, LoadedModelInfo *loadedModel);
	static GameObject *LoadGeometryFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, char *fileName, char* folderName);
	static void PrintFrameInfo(GameObject *gameObject, GameObject *parent);

	virtual bool IsFar(Camera* camera) { return false; };

	char* GetName() { return frameName; };
};

class RotatingObject : public GameObject
{
public:
	RotatingObject(int meshesNum = 1);
	virtual ~RotatingObject();
private:
	XMFLOAT3 rotationAxis;
	float rotationSpeed;
public:
	void SetRotationSpeed(float rotationSpeed) { this->rotationSpeed = rotationSpeed; }
	void SetRotationAxis(XMFLOAT3 rotationAxis) 
	{
		this->rotationAxis = rotationAxis;
	}
	virtual void Animate(float timeElapsed, XMFLOAT4X4* parent = NULL);

};



class HeightMapTerrain : public GameObject
{
public:
	HeightMapTerrain(ID3D12Device *device, ID3D12GraphicsCommandList
		*commandList, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength, 
		XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color);
	virtual ~HeightMapTerrain();
private:
	HeightMapImage *heightMapImage;
	int width;
	int length;

	XMFLOAT3 scale;



public:
	float GetHeight(float x, float z) 
	{
		return(heightMapImage->GetHeight(x / scale.x, z / scale.z) * scale.y);
	}
	XMFLOAT3 GetNormal(float x, float z) 
	{
		return(heightMapImage->GetHeightMapNormal(int(x / scale.x), int(z / scale.z)));
	}
	float GetTerrainHeight(float x, float y);

	HeightMapImage* GetImage() { return heightMapImage; }
	int GetHeightMapWidth() { return(heightMapImage->GetHeightMapWidth()); }
	int GetHeightMapLength() { return(heightMapImage->GetHeightMapLength()); }
	XMFLOAT3 GetScale() { return(scale); }
	float GetWidth() { return(width * scale.x); }
	float GetLength() { return(length * scale.z); }
	virtual Mesh* GetMesh(int idx) { return meshes[idx]; };
	void SetBoundingTransform();

};

class SuperCobraObject : public GameObject
{
public:
	SuperCobraObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature);
	virtual ~SuperCobraObject();

private:
	GameObject					*mainRotorFrame = NULL;
	GameObject					*tailRotorFrame = NULL;

public:
	virtual void PrepareAnimate();
	virtual void Animate(float fTimeElapsed, XMFLOAT4X4 *pxmf4x4Parent = NULL);
};

class TreeObject : public GameObject
{
public:
	TreeObject();
	virtual ~TreeObject();

	virtual void Animate(float timeElapsed, XMFLOAT4X4* parent = NULL) {};

	virtual bool IsFar(Camera* camera);
};

class BillboardObject : public GameObject
{
public:

	BillboardGSVertex billboardVertex;


	BillboardObject();
	virtual ~BillboardObject();

	virtual bool IsFar(Camera* camera);
	virtual void UpdateTransform(VS_VB_INSTANCE_BILLBOARD* vbMappedBillboardObjects, int idx);

};

class AnimationObject : public GameObject
{
public:
	AnimationObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, LoadedModelInfo *model, int animationTracks, bool isDummy);
	AnimationObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature);
	virtual ~AnimationObject();

	virtual void UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, XMFLOAT4X4 *xmf4x4World);

};