#pragma once
#include "stdafx.h"
const ULONG MAX_SAMPLE_COUNT = 50; // 50회의 프레임 처리시간을 누적하여 평균한다.
class GameTimer
{
public:
	GameTimer();
	virtual ~GameTimer();

	void Tick(float lockFPS = 0.0f);
	void Start();
	void Stop();
	void Reset();

	unsigned long GetFrameRate(LPTSTR string = NULL, int characters = 0);

	float GetTimeElapsed();
	float GetTotalTime();

private:
	bool hardwareHasPerformanceCounter;
	bool stopped;

	double timeScale;
	float timeElapsed;

	__int64 basePerformanceCounter;
	__int64	pausedPerformanceCounter;
	__int64	stopPerformanceCounter;
	__int64	currentPerformanceCounter;
	__int64	lastPerformanceCounter;

	__int64 performanceFrequencyPerSec;

	float frameTime[MAX_SAMPLE_COUNT];
	ULONG sampleCount;

	unsigned long currentFrameRate;
	unsigned long framesPerSecond;
	float fpsTimeElapsed;
};