#include "stdafx.h"
#include "Player.h"
#include "ObjectShader.h"
#include "Material.h"


Player::Player(int meshesNum)
{
	this->meshesNum = meshesNum;
	camera = NULL;
	position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	right = XMFLOAT3(1.0f, 0.0f, 0.0f);
	up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	look = XMFLOAT3(0.0f, 0.0f, 1.0f);
	acc = XMFLOAT3(0.0f, 0.0f, 0.0f);
	velocity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	gravity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	maxVelocityXZ = 0.0f;
	minVelocityXZ = -6.0f;
	maxVelocityY = 0.0f;
	friction = 0.0f;
	pitch = 0.0f;
	roll = 0.0f;
	yaw = 0.0f;
	playerUpdatedContext = NULL;
	cameraUpdatedContext = NULL;
	obbBox.Center = position;
	obbBox.Extents = XMFLOAT3(0.4f, 2.0f, 0.4f);
	obbBox.Orientation = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	preWeaponPoint.Clear();
	curWeaponPoint.Clear();

}
Player::~Player()
{
	if (camera) delete camera;
}

void Player::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	UINT skinnedStride = 0;
	ID3D12Resource** tempSkinned = NULL;
	XMFLOAT4X4** tempMappedSkinned = NULL;

	skinnedStride = sizeof(XMFLOAT4X4) * SKINNED_ANIMATION_BONES;
	tempSkinned = new ID3D12Resource*[skinnedMeshNum];
	tempMappedSkinned = new XMFLOAT4X4*[skinnedMeshNum];

	for (int i = 0; i < skinnedMeshNum; ++i)
	{
		tempSkinned[i] = ::CreateBufferResource(device, commandList, NULL, skinnedStride, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_GENERIC_READ, NULL);
		tempSkinned[i]->Map(0, NULL, (void **)&tempMappedSkinned[i]);
	}
	// Skinned Bone Transform Buffers ( SRV )

	UINT standardStride = 0;
	ID3D12Resource** tempGameObject = NULL;
	VS_VB_INSTANCE_OBJECT** tempMappedGameObject = NULL;
	D3D12_VERTEX_BUFFER_VIEW* tempGameObjectView = NULL;

	standardStride = ((sizeof(VS_VB_INSTANCE_OBJECT) + 255) & ~255); //256의 배수
	tempGameObject = new ID3D12Resource*[standardMeshNum];
	tempMappedGameObject = new VS_VB_INSTANCE_OBJECT*[standardMeshNum];
	tempGameObjectView = new D3D12_VERTEX_BUFFER_VIEW[standardMeshNum];

	for (int j = 0; j < standardMeshNum; ++j)
	{
		tempGameObject[j] = ::CreateBufferResource(device, commandList, NULL, standardStride, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		tempGameObject[j]->Map(0, NULL, (void **)&tempMappedGameObject[j]);

		tempGameObjectView[j].BufferLocation = tempGameObject[j]->GetGPUVirtualAddress();
		tempGameObjectView[j].StrideInBytes = standardStride;
		tempGameObjectView[j].SizeInBytes = standardStride;
	}
	// Standard Mesh Transform Buffer ( Vertex Buffer )

	UINT boundStride = 0;
	ID3D12Resource** tempbounding = NULL;
	VS_VB_INSTANCE_BOUNDING** tempMappedBounding = NULL;
	D3D12_VERTEX_BUFFER_VIEW* tempBoundingBufferView;

	boundStride = sizeof(VS_VB_INSTANCE_BOUNDING);
	tempbounding = new ID3D12Resource*[boundingMeshNum];
	tempMappedBounding = new VS_VB_INSTANCE_BOUNDING*[boundingMeshNum];
	tempBoundingBufferView = new D3D12_VERTEX_BUFFER_VIEW[boundingMeshNum];

	for (int i = 0; i < boundingMeshNum; ++i)
	{
		tempbounding[i] = ::CreateBufferResource(device, commandList, NULL, boundStride, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		tempbounding[i]->Map(0, NULL, (void**)&tempMappedBounding[i]);

		tempBoundingBufferView[i].BufferLocation = tempbounding[i]->GetGPUVirtualAddress();
		tempBoundingBufferView[i].StrideInBytes = boundStride;
		tempBoundingBufferView[i].SizeInBytes = boundStride;
	}
	XMFLOAT4X4** tempObbCache = NULL;
	tempObbCache = new XMFLOAT4X4*[boundingMeshNum];
	for (int i = 0; i < boundingMeshNum; ++i)
	{
		tempObbCache[i] = new XMFLOAT4X4;
	}

	ID3D12Resource** tempPositionW = NULL;
	ID3D12Resource** tempNormalW = NULL;
	ID3D12Resource** tempTangentW = NULL;
	ID3D12Resource** tempBiTangentW = NULL;

	tempPositionW = new ID3D12Resource*[skinnedMeshNum];
	tempNormalW = new ID3D12Resource*[skinnedMeshNum];
	tempTangentW = new ID3D12Resource*[skinnedMeshNum];
	tempBiTangentW = new ID3D12Resource*[skinnedMeshNum];

	for (int i = 0; i < skinnedMeshNum; ++i) {
		UINT bytesNum = sizeof(XMFLOAT3) * skinnedMeshes[i]->GetVerticesNum();
		tempPositionW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		tempNormalW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		tempTangentW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		tempBiTangentW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

		::ResourceBarrier(commandList, tempPositionW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, tempNormalW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, tempTangentW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, tempBiTangentW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	}

	skinnedBuffer = tempSkinned;
	mappedSkinnedBuffer = tempMappedSkinned;

	vbGameObject = tempGameObject;
	vbMappedGameObject = tempMappedGameObject;
	vbGameObjectView = tempGameObjectView;
	
	boundingBuffer = tempbounding;
	mappedBoundingBuffer = tempMappedBounding;
	instancingBoundingBufferView = tempBoundingBufferView;

	obbWorldCache = tempObbCache;

	positionW = tempPositionW;
	normalW = tempNormalW;
	tangentW = tempTangentW;
	biTangentW = tempBiTangentW;

}
void Player::ReleaseShaderVariables()
{
	if (camera) camera->ReleaseShaderVariables();
	if (materials)
		materials[0]->shader->ReleaseShaderVariables();
}
void Player::UpdateShaderVariables()
{
	for (int i = 0; i < skinnedMeshNum; ++i)
	{
		for (int j = 0; j < skinnedMeshes[i]->skinningBones; ++j)
		{
			XMStoreFloat4x4(&mappedSkinnedBuffer[i][j], XMMatrixTranspose(XMLoadFloat4x4(&skinnedMeshes[i]->skinningBoneFrameCaches[j]->GetWorld())));
		}
	}
}

/*플레이어의 위치를 변경하는 함수이다. 플레이어의 위치는 기본적으로 사용자가 플레이어를 이동하기 위한 키보드를
누를 때 변경된다. 플레이어의 이동 방향(dwDirection)에 따라 플레이어를 fDistance 만큼 이동한다.*/
void Player::Move(DWORD direction, float force, bool updateVelocity , float elapsedTime)
{
		XMFLOAT3 vForce = XMFLOAT3(0, 0, 0);
		if (direction == DirectionForword) 
			vForce = Vector3::Add(vForce, look, force);
		if (direction == DirectionBackword) 
			vForce = Vector3::Add(vForce, look, -force);
		//화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.

		if (direction == DirectionRight) vForce = Vector3::Add(vForce, right, force);
		if (direction == DirectionLeft) vForce = Vector3::Add(vForce, right, -force);
		//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.

		XMFLOAT3 opRight = XMFLOAT3(-right.x, -right.y, -right.z);
		XMFLOAT3 lrVec = Vector3::Normalize(Vector3::Add(look, right));
		XMFLOAT3 mlrVec = Vector3::Normalize(Vector3::Add(look, opRight));

		//대각선 이동 
		if (direction == DirectionFR) vForce = Vector3::Add(vForce, lrVec, force);
		if (direction == DirectionFL) vForce = Vector3::Add(vForce, mlrVec, force);
		if (direction == DirectionBR) vForce = Vector3::Add(vForce, mlrVec, -force);
		if (direction == DirectionBL) vForce = Vector3::Add(vForce, lrVec, -force);


		if (direction == DirectionUp) vForce = Vector3::Add(vForce, up, force);
		if (direction == DirectionDown) vForce = Vector3::Add(vForce, up, -force);
		//‘Page Up’을 누르면 로컬 y-축 방향으로 이동한다. ‘Page Down’을 누르면 반대 방향으로 이동한다.

		acc = XMFLOAT3(vForce.x / mass, 0, vForce.z / mass); // 가속도 
		float vlength = Vector3::Length(velocity);

		/*if (direction == DirectionForword) {
			std::cout << "VL : " << Vector3::Length(velocity) << std::endl;
		}*/
		// 어느 순간 이부분에 계산이 안들어감??? 즉 이미 값 자체는 넘었다고 계산되어버림 그럼 velocity가 다른곳에서 따로 계산이 된다.
		if (vlength < maxVelocityXZ)
			velocity = Vector3::Add(velocity, XMFLOAT3((acc.x * elapsedTime), 0, (acc.z *elapsedTime)));

		Move(vForce, updateVelocity , elapsedTime);
		//플레이어를 현재 위치 벡터에서 Shift 벡터만큼 이동한다.
	
}
void Player::Move(const XMFLOAT3& shift, bool isSetting , float elapsedTime)
{
	//bUpdateVelocity가 참이면 플레이어를 이동하지 않고 속도 벡터를 변경한다.
	if (isSetting)
	{
		position = Vector3::Add(position, shift);

		obbBox.Center = position;

		camera->Move(shift);

	}
	else
	{

		finalvec = Vector3::Add(XMFLOAT3(velocity.x * elapsedTime, shift.y, velocity.z * elapsedTime),
			XMFLOAT3(acc.x * elapsedTime * elapsedTime * 0.5f, 0.0f, acc.z * elapsedTime * elapsedTime * 0.5f));
		
		if (direction == DirectionForword) {
			
		}
		//플레이어를 현재 위치 벡터에서 Shift 벡터만큼 이동한다.
		
		colPos = position;
		colPos = Vector3::Add(position, finalvec);

		//real move를 넣어줘야함
		colObbBox = obbBox;
		colObbBox.Center = colPos;

		float length = sqrtf(velocity.x * velocity.x + velocity.z * velocity.z);
		if (direction == DirectionNoMove)
		{
			length = Vector3::Length(velocity);
			float deceleration = (friction * elapsedTime);
			if (deceleration > length) deceleration = length;
			velocity = Vector3::Add(velocity, Vector3::ScalarProduct(velocity, -deceleration, false));
		}
		else {
			length = Vector3::Length(velocity);
			float deceleration = ((friction - 21.0f) * elapsedTime);
			if (deceleration > length) deceleration = length;
			velocity = Vector3::Add(velocity, Vector3::ScalarProduct(velocity, -deceleration, false));
		}

	}

}
void Player::RealMove()
{
	position = Vector3::Add(position, finalvec);
	colPos = position;

	//플레이어의 위치가 변경되었으므로 카메라의 위치도 Shift 벡터만큼 이동한다.
	obbBox.Center = position;
	colObbBox = obbBox;
	camera->Move(finalvec);

}
//플레이어를 로컬 x-축, y-축, z-축을 중심으로 회전한다.
void Player::Rotate(float x, float y, float z)
{
	DWORD cameraMode = camera->GetMode();
	//1인칭 카메라 또는 3인칭 카메라의 경우 플레이어의 회전은 약간의 제약이 따른다.
	if ((cameraMode == FIRST_PERSON_CAMERA) || (cameraMode == THIRD_PERSON_CAMERA) || (cameraMode == ZOOM_MODE_CAMERA))
	{
		/*로컬 x-축을 중심으로 회전하는 것은 고개를 앞뒤로 숙이는 동작에 해당한다. 그러므로 x-축을 중심으로 회전하는
		각도는 -89.0~+89.0도 사이로 제한한다. x는 현재의 fPitch에서 실제 회전하는 각도이므로 x만큼 회전한 다음
		Pitch가 +89도 보다 크거나 -89도 보다 작으면 fPitch가 +89도 또는 -89도가 되도록 회전각도(x)를 수정한다.*/
		if (x != 0.0f)
		{
			pitch += x;
			//camera->rotateDataX = 0.f;
			if (pitch > +20.0f) { x -= (pitch - 20.0f); pitch = +20.0f; }
			if (pitch < -20.0f) { x -= (pitch + 20.0f); pitch = -20.0f; }
		}
		if (y != 0.0f)
		{
			//로컬 y-축을 중심으로 회전하는 것은 몸통을 돌리는 것이므로 회전 각도의 제한이 없다.
			yaw += y;
			camera->rotateDataY = 0.f;
			if (yaw > 360.0f) yaw -= 360.0f;
			if (yaw < 0.0f) yaw += 360.0f;
		}
		if (z != 0.0f)
		{
			/*로컬 z-축을 중심으로 회전하는 것은 몸통을 좌우로 기울이는 것이므로 회전 각도는 -20.0~+20.0도 사이로 제한된
			다. z는 현재의 fRoll에서 실제 회전하는 각도이므로 z만큼 회전한 다음 fRoll이 +20도 보다 크거나 -20도보다
			작으면 fRoll이 +20도 또는 -20도가 되도록 회전각도(z)를 수정한다.*/
			roll += z;
			camera->rotateDataX = 0.f;
			if (roll > +20.0f) { z -= (roll - 20.0f); roll = +20.0f; }
			if (roll < -20.0f) { z -= (roll + 20.0f); roll = -20.0f; }
		}
		//카메라를 x, y, z 만큼 회전한다. 플레이어를 회전하면 카메라가 회전하게 된다.
		camera->Rotate(x, y, z);
		/*플레이어를 회전한다. 1인칭 카메라 또는 3인칭 카메라에서 플레이어의 회전은 로컬 y-축에서만 일어난다. 플레이어
		의 로컬 y-축(Up 벡터)을 기준으로 로컬 z-축(Look 벡터)와 로컬 x-축(Right 벡터)을 회전시킨다. 기본적으로 Up 벡
		터를 기준으로 회전하는 것은 플레이어가 똑바로 서있는 것을 가정한다는 의미이다.*/
		if (y != 0.0f)
		{
			XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&up), XMConvertToRadians(y));
			look = Vector3::TransformNormal(look, rotate);
			right = Vector3::TransformNormal(right, rotate);
		}

		if (x != 0.0f)
		{
			camera->rotateData += x;
		}
	}
	else if (cameraMode == SPACESHIP_CAMERA)
	{
		/*스페이스-쉽 카메라에서 플레이어의 회전은 회전 각도의 제한이 없다. 그리고 모든 축을 중심으로 회전을 할 수 있
		다.*/
		camera->Rotate(x, y, z);
		if (x != 0.0f)
		{
			XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&right), XMConvertToRadians(x));
			look = Vector3::TransformNormal(look, rotate);
			up = Vector3::TransformNormal(up, rotate);
		}
		if (y != 0.0f)
		{
			XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&up), XMConvertToRadians(y));
			look = Vector3::TransformNormal(look, rotate);
			right = Vector3::TransformNormal(right, rotate);
		}
		if (z != 0.0f)
		{
			XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&look), XMConvertToRadians(z));
			up = Vector3::TransformNormal(up, rotate);
			right = Vector3::TransformNormal(right, rotate);
		}
	}
	/*회전으로 인해 플레이어의 로컬 x-축, y-축, z-축이 서로 직교하지 않을 수 있으므로 z-축(LookAt 벡터)을 기준으
	로 하여 서로 직교하고 단위벡터가 되도록 한다.*/
	look = Vector3::Normalize(look);
	right = Vector3::CrossProduct(up, look, true);
	up = Vector3::CrossProduct(look, right, true);
}

void Player::PlayerGetHeal(float time)
{
	hpcool += time;
	steminacool += time;

	if (hpcool >= 5.0f)
	{
		SetPlayerHp(maxHp / 10);
		hpcool = 0.f;;
		isHill = true;
	}

	if (steminacool >= 2.0f)
	{
		SetPlayerStemina(maxStemina / 10);
		steminacool = 0.f;;
	}
}

//이 함수는 매 프레임마다 호출된다. 플레이어의 속도 벡터에 중력과 마찰력 등을 적용한다.
void Player::Update(float timeElapsed)
{
	// 플레이어의 속도 벡터를 중력 벡터와 더한다. 중력 벡터에 fTimeElapsed를 곱하는 것은 중력을 시간에 비례하도록 적용한다는 의미이다.
	//velocity = Vector3::Add(velocity, Vector3::ScalarProduct(gravity, timeElapsed, false));
	// 플레이어의 속도 벡터의 XZ-성분의 크기를 구한다. 이것이 XZ-평면의 최대 속력보다 크면 속도 벡터의 x와 z-방향 성분을 조정한다.
//	//float maxVelocityXZ = this->maxVelocityXZ * timeElapsed;
//
//	//if (length > this->maxVelocityXZ)
//	//{
//	//	velocity.x *= (maxVelocityXZ / length);
//	//	velocity.z *= (maxVelocityXZ / length);
//	//}
////	플레이어의 속도 벡터의 y-성분의 크기를 구한다. 이것이 y-축 방향의 최대 속력보다 크면 속도 벡터의 y-방향 성분을 조정한다.
//	float maxVelocityY = this->maxVelocityY * timeElapsed;
//	length = sqrtf(velocity.y * velocity.y);
//	if (length > this->maxVelocityY) velocity.y *= (maxVelocityY / length);
//	플레이어를 속도 벡터 만큼 실제로 이동한다(카메라도 이동될 것이다).
//	Move(velocity, false);
	/*플레이어의 위치가 변경될 때 추가로 수행할 작업을 수행한다. 플레이어의 새로운 위치가 유효한 위치가 아닐 수도
	있고 또는 플레이어의 충돌 검사 등을 수행할 필요가 있다. 이러한 상황에서 플레이어의 위치를 유효한 위치로 다시 변경할 수 있다.*/

//	if (playerUpdatedContext) OnPlayerUpdateCallback(timeElapsed);

	DWORD cameraMode = camera->GetMode();
//	플레이어의 위치가 변경되었으므로 3인칭 카메라를 갱신한다.
	if (cameraMode == THIRD_PERSON_CAMERA || cameraMode == FIRST_PERSON_CAMERA || (cameraMode == ZOOM_MODE_CAMERA)) camera->Update(position, timeElapsed);
//	카메라의 위치가 변경될 때 추가로 수행할 작업을 수행한다.

//	if (cameraUpdatedContext) OnCameraUpdateCallback(timeElapsed);

//	카메라가 3인칭 카메라이면 카메라가 변경된 플레이어 위치를 바라보도록 한다.
	XMFLOAT3 camLookPos = Vector3::Add(position, Vector3::ScalarProduct(look, 10.0f));
	if (cameraMode == THIRD_PERSON_CAMERA || cameraMode == FIRST_PERSON_CAMERA || (cameraMode == ZOOM_MODE_CAMERA)) camera->SetLookAt(camLookPos);
//  카메라의 카메라 변환 행렬을 다시 생성한다.
	camera->RegenerateViewMatrix();
	/*플레이어의 속도 벡터가 마찰력 때문에 감속이 되어야 한다면 감속 벡터를 생성한다. 속도 벡터의 반대 방향 벡터를
	구하고 단위 벡터로 만든다. 마찰 계수를 시간에 비례하도록 하여 마찰력을 구한다. 단위 벡터에 마찰력을 곱하여 감
	속 벡터를 구한다. 속도 벡터에 감속 벡터를 더하여 속도 벡터를 줄인다. 마찰력이 속력보다 크면 속력은 0이 될 것이다.*/

	PlayerGetHeal(timeElapsed);

}
/*카메라를 변경할 때 ChangeCamera() 함수에서 호출되는 함수이다. nCurrentCameraMode는 현재 카메라의 모드
이고 nNewCameraMode는 새로 설정할 카메라 모드이다.*/
Camera *Player::OnChangeCamera(DWORD newCameraMode, DWORD currentCameraMode)
{
	//새로운 카메라의 모드에 따라 카메라를 새로 생성한다.
	Camera *newCamera = NULL;
	switch (newCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		newCamera = new FirstPersonCamera(camera);
		break;
	case THIRD_PERSON_CAMERA:
		newCamera = new ThirdPersonCamera(camera);
		break;
	case SPACESHIP_CAMERA:
		newCamera = new SpaceShipCamera(camera);
		break;
	case ZOOM_MODE_CAMERA:
		newCamera = new ZoomCamera(camera);
		break;
	}
	/*현재 카메라의 모드가 스페이스-쉽 모드의 카메라이고 새로운 카메라가 1인칭 또는 3인칭 카메라이면 플레이어의
	Up 벡터를 월드좌표계의 y-축 방향 벡터(0, 1, 0)이 되도록 한다. 즉, 똑바로 서도록 한다. 그리고 스페이스-쉽 카메
	라의 경우 플레이어의 이동에는 제약이 없다. 특히, y-축 방향의 움직임이 자유롭다. 그러므로 플레이어의 위치는 공
	중(위치 벡터의 y-좌표가 0보다 크다)이 될 수 있다. 이때 새로운 카메라가 1인칭 또는 3인칭 카메라이면 플레이어의
	위치는 지면이 되어야 한다. 그러므로 플레이어의 Right 벡터와 Look 벡터의 y 값을 0으로 만든다. 이제 플레이어의
	Right 벡터와 Look 벡터는 단위벡터가 아니므로 정규화한다.*/
	if (currentCameraMode == SPACESHIP_CAMERA)
	{
		right = Vector3::Normalize(XMFLOAT3(right.x, 0.0f, right.z));
		up = Vector3::Normalize(XMFLOAT3(0.0f, 1.0f, 0.0f));
		look = Vector3::Normalize(XMFLOAT3(look.x, 0.0f, look.z));
		pitch = 0.0f;
		roll = 0.0f;
		/*Look 벡터와 월드좌표계의 z-축(0, 0, 1)이 이루는 각도(내적=cos)를 계산하여 플레이어의 y-축의 회전 각도 yaw로 설정한다.*/
		yaw = Vector3::Angle(XMFLOAT3(0.0f, 0.0f, 1.0f), look);
		if (look.x < 0.0f) yaw = -yaw;
	}
	else if ((newCameraMode == SPACESHIP_CAMERA) && camera)
	{
		/*새로운 카메라의 모드가 스페이스-쉽 모드의 카메라이고 현재 카메라 모드가 1인칭 또는 3인칭 카메라이면 플레이
		어의 로컬 축을 현재 카메라의 로컬 축과 같게 만든다.*/
		right = camera->GetRightVector();
		up = camera->GetUpVector();
		look = camera->GetLookVector();
	}
	if (newCamera)
	{
		newCamera->SetMode(newCameraMode);
		//현재 카메라를 사용하는 플레이어 객체를 설정한다.
		newCamera->SetPlayer(this);
	}
	if (camera) delete camera;
	return(newCamera);
}

void Player::Animate()
{
	transform._11 = right.x; transform._12 = right.y; transform._13 = right.z;
	transform._21 = up.x; transform._22 = up.y; transform._23 = up.z;
	transform._31 = look.x; transform._32 = look.y; transform._33 = look.z;
	transform._41 = position.x; transform._42 = position.y; transform._43 = position.z;

	transform = Matrix4x4::Multiply(XMMatrixScaling(scale.x, scale.y, scale.z), transform);

	GameObject::Animate(elapsedTime);
	UpdateShaderVariables();
	curWeaponPoint = UpdateTransformOnlyStandardMesh(vbMappedGameObject, 0, standardMeshNum, weaponNum, NULL, toArrow);
	UpdateSkinnedBoundingTransform(mappedBoundingBuffer, obbvec, NULL, 0, boundingMeshNum, NULL);
}

/*플레이어의 위치와 회전축으로부터 월드 변환 행렬을 생성하는 함수이다. 플레이어의 Right 벡터가 월드 변환 행렬
의 첫 번째 행 벡터, Up 벡터가 두 번째 행 벡터, Look 벡터가 세 번째 행 벡터, 플레이어의 위치 벡터가 네 번째 행
벡터가 된다.*/
void Player::OnPrepareRender(ID3D12GraphicsCommandList* commandList, bool isShadow)
{
	if (!isShadow)
	{
		if (diffuseIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[diffuseIdx].rootParameterIndex, Scene::rootArgumentInfos[diffuseIdx].srvGpuDescriptorHandle);
		if (normalIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[normalIdx].rootParameterIndex, Scene::rootArgumentInfos[normalIdx].srvGpuDescriptorHandle);
		if (specularIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[specularIdx].rootParameterIndex, Scene::rootArgumentInfos[specularIdx].srvGpuDescriptorHandle);
		if (metallicIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[metallicIdx].rootParameterIndex, Scene::rootArgumentInfos[metallicIdx].srvGpuDescriptorHandle);
		if (emissionIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[emissionIdx].rootParameterIndex, Scene::rootArgumentInfos[emissionIdx].srvGpuDescriptorHandle);
	}

	if (isShadow)
	{
		if (Shader::curPipelineState != shader->GetShadowPipelineStates()[shadowPipeLineStateIdx]) {
			commandList->SetPipelineState(shader->GetShadowPipelineStates()[shadowPipeLineStateIdx]);
			Shader::curPipelineState = shader->GetShadowPipelineStates()[shadowPipeLineStateIdx];
		}
	}
	else
	{
		if (Shader::curPipelineState != shader->GetPipelineStates()[pipeLineStateIdx]) {
			commandList->SetPipelineState(shader->GetPipelineStates()[pipeLineStateIdx]);
			Shader::curPipelineState = shader->GetPipelineStates()[pipeLineStateIdx];
		}
	}

}
void Player::PrepareCompute(ID3D12GraphicsCommandList* commandList)
{
	for (int i = 0; i < skinnedMeshNum; ++i)
	{
		skinnedMeshes[i]->skinningBoneTransforms = skinnedBuffer[i];
		skinnedMeshes[i]->mappedSkinningBoneTransforms = mappedSkinnedBuffer[i];

		commandList->SetPipelineState(shader->GetComputePiplineStates()[0]);
		ID3D12Resource* buffers[4] = { positionW[i], normalW[i], tangentW[i], biTangentW[i] };

		skinnedMeshes[i]->OnPreRender(commandList, 1, buffers);
	}
}

void Player::SetModel(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int modelIdx)
{
	playerJob = modelIdx;
	LoadedModelInfo* playerModel = (LoadedModelInfo*)objects[hObjectSize + modelIdx];

	if (playerJob == PlayerJobBow)
		toArrow = new XMFLOAT4X4();

	SetPlayerUpdatedContext(objects[0]);
	SetCameraUpdatedContext(objects[0]);

	textureName = playerModel->modelRootObject->textureName;
	skinnedMeshNum = playerModel->skinnedMeshNum;
	standardMeshNum = playerModel->standardMeshNum;
	boundingMeshNum = playerModel->boundingMeshNum;

	obbvec.resize(boundingMeshNum);

	skinnedMeshes = new SkinnedMesh*[playerModel->skinnedMeshNum];
	pipeLineStateIdx = 0;
	shadowPipeLineStateIdx = 0;

	for (int i = 0; i < skinnedMeshNum; i++)
		skinnedMeshes[i] = playerModel->skinnedMeshes[i];

	SetChild(playerModel->modelRootObject, true);
	skinnedAnimationController = new AnimationController(device, commandList, 1, playerModel);

	if (playerJob == PlayerJobRifle)
	{
		skinnedAnimationController->animationSets->animationSets[MainCharacterReload]->animationType = AnimationTypeOnce;
	}
	else if (playerJob == PlayerJobBow)
	{
		skinnedAnimationController->animationSets->animationSets[BowCharacterDrawArrow]->animationType = AnimationTypeOnce;
		skinnedAnimationController->animationSets->animationSets[BowCharacterRelease]->animationType = AnimationTypeOnce;
	}
	skinnedAnimationController->playerJob = playerJob;
	skinnedAnimationController->SetTrackAnimationSet(0, 0);
	skinnedAnimationController->SetBeforeTransform(0);
	skinnedAnimationController->SetTrackSpeed(0, 1);
	skinnedAnimationController->SetTrackPosition(0, 0.0f);

	CreateShaderVariables(device, commandList);
	// Skinned: BoneTransform, Standard : InstanceBuffer 할당.

	SetTextures(device, commandList);
	
	haveModel = true;
}

void Player::Render(ID3D12GraphicsCommandList* commandList, Camera* camera, bool isShadow)
{
	DWORD cameraMode = (camera) ? camera->GetMode() : 0x00;

	if (cameraMode == THIRD_PERSON_CAMERA || cameraMode == FIRST_PERSON_CAMERA || isShadow)
	{
		OnPrepareRender(commandList, isShadow);

		ID3D12Resource** buffers[4] = { positionW, normalW, tangentW, biTangentW };
		int weaponNum1 = -1;
		if (playerJob == PlayerJobBow)
			weaponNum1 = 5;
		GameObject::Render(commandList, 1, vbGameObjectView, buffers, isShadow, weaponNum, weaponNum1);
		if (isShadow == false && showBounding == true)
		{
			if (Shader::curPipelineState != shader->GetPipelineStates()[1]) {
				commandList->SetPipelineState(shader->GetPipelineStates()[1]);
				Shader::curPipelineState = shader->GetPipelineStates()[1];
			}
			GameObject::SkinnedBoundingRender(commandList, 1, instancingBoundingBufferView, weaponNum);
		}
	}
	//카메라 모드가 3인칭이면 플레이어 객체를 렌더링한다.
}

void Player::AnimateObject(float elapsedTime)
{
	this->elapsedTime = elapsedTime;
}

void Player::SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	if (textureName.diffuse.size() > 0)
	{
		Texture* diffuseTexture = new Texture((int)textureName.diffuse.size(), ResourceTexture2D, 0);
		for (int i = 0; i < textureName.diffuse.size(); ++i)
		{
			diffuseTexture->LoadTextureFromFile(device, commandList, textureName.diffuse[i].second, textureName.diffuse[i].first);
		}
		diffuseIdx = Scene::CreateShaderResourceViews(device, commandList, diffuseTexture, GraphicsRootDiffuseTextures);
	}
	if (textureName.normal.size() > 0)
	{
		Texture* normalTexture = new Texture((int)textureName.normal.size(), ResourceTexture2D, 0);
		for (int i = 0; i < textureName.normal.size(); ++i)
		{
			normalTexture->LoadTextureFromFile(device, commandList, textureName.normal[i].second, textureName.normal[i].first);
		}
		normalIdx = Scene::CreateShaderResourceViews(device, commandList, normalTexture, GraphicsRootNormalTextures);
	}
	if (textureName.specular.size() > 0)
	{
		Texture* specularTexture = new Texture((int)textureName.specular.size(), ResourceTexture2D, 0);
		for (int i = 0; i < textureName.specular.size(); ++i)
		{
			specularTexture->LoadTextureFromFile(device, commandList, textureName.specular[i].second, textureName.specular[i].first);
		}
		specularIdx = Scene::CreateShaderResourceViews(device, commandList, specularTexture, GraphicsRootSpecularTextures);
	}
	if (textureName.metallic.size() > 0)
	{
		Texture* metallicTexture = new Texture((int)textureName.metallic.size(), ResourceTexture2D, 0);
		for (int i = 0; i < textureName.metallic.size(); ++i)
		{
			metallicTexture->LoadTextureFromFile(device, commandList, textureName.metallic[i].second, textureName.metallic[i].first);
		}
		metallicIdx = Scene::CreateShaderResourceViews(device, commandList, metallicTexture, GraphicsRootMetallicTextures);
	}
}

void Player::ChangePlayerAnimation(int status)
{
	if (animationStatus != status)
	{
		skinnedAnimationController->SetTrackAnimationSet(0, status);
		animationStatus = status;
	}
}

void Player::ChangePlayerAnimation(int status1, int status2, int character)
{
	if (skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockanimation) return;

	if (playerJob == PlayerJobBow)
	{
		if (status1 == BowCharacterDrawArrow)
		{
			if (upAnimationStatus == BowCharacterDrawArrow)
			{
				status1 = BowCharacterRelease;
				status2 = BowCharacterRelease;
			}
		}
	}

	if (skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation == false)
	{
		if (upAnimationStatus != status1 && loAnimationStatus != status2)
		{
			skinnedAnimationController->SetTrackAnimationSet(0, status1, status2);
			upAnimationStatus = status1;
			loAnimationStatus = status2;
		}
		else if (upAnimationStatus != status1)
		{
			skinnedAnimationController->SetTrackAnimationSet(0, status1, loAnimationStatus);
			upAnimationStatus = status1;
		}
		else if (loAnimationStatus != status2)
		{
			skinnedAnimationController->SetTrackAnimationSet(0, upAnimationStatus, status2);
			loAnimationStatus = status2;
		}
	}
	else
	{
		if (loAnimationStatus != status2)
		{
			skinnedAnimationController->SetTrackAnimationSet(0, upAnimationStatus, status2);
			loAnimationStatus = status2;
		}
	}

	if (skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation == false)
	{
		if (character == PlayerJobBow)
		{
			if (status1 == BowCharacterPunch || status1 == BowCharacterKick)
			{
				skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockanimation = true;
				skinnedAnimationController->animationTracks[0].position = 0.0f;
			}
			else if (status1 == BowCharacterDrawArrow || status1 == BowCharacterRelease)
			{
				skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockanimation = true;
				skinnedAnimationController->animationTracks[0].position = 0.0f;
			}
		}
		else if (character == PlayerJobSword)
		{
			if (status1 == SwordCharacterAttack || status1 == SwordCharacterAttack2 || status1 == SwordCharacterAttack3)
			{
				skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockanimation = true;
				skinnedAnimationController->animationTracks[0].position = 0.0f;
				SetPlayerStemina(-15.0f);
			}
		}
		else if (character == PlayerJobRifle)
		{
			if (status1 == MainCharacterReload || status1 == MainCharacterWalkingReload)
			{
				skinnedAnimationController->animationSets->animationSets[skinnedAnimationController->animationTracks[0].upanimationSet]->lockupanimation = true;
				skinnedAnimationController->animationTracks[0].position = 0.0f;
			}
		}
	}
}	

void Player::GunRebound()
{
	if (playerJob == PlayerJobRifle)
	{
		switch (weaponNum){
			// scar 반동
			case 0 :
				camera->rotateDataX -= 0.05;
				camera->rotateDataY += rand() % 2 - 0.5;
				break;
			// G28 반동
			case 1:
				camera->rotateDataX -= 5;
				camera->rotateDataY += rand() % 4 - 2;
				break;
			// AK 반동
			case 2:
				camera->rotateDataX -= 0.15;
				camera->rotateDataY += rand() % 3 - 1;
				break;
			case 3:
				camera->rotateDataX -= 0.05;
				camera->rotateDataY += rand() % 3 - 1;
				break;
			case 4:
				camera->rotateDataX -= 10;
				camera->rotateDataY += rand() % 6 - 3;
				break;
			case 5:
				camera->rotateDataX -= 0.1;
				camera->rotateDataY += rand() % 3 - 1;
				break;
			
		}
	}
}

float Player::GetWeaponDamage()
{
	float damage = 0.f;
	if (playerJob == PlayerJobRifle)
	{
		switch (weaponNum) {
		case 0:
			damage = 6.f;
			break;
		case 1:
			damage = 15.f;
			break;
		case 2:
			damage = 10.f;
			break;
		case 3:
			damage = 8.f;
			break;
		case 4:
			damage = 55.f;
			break;
		case 5:
			damage = 10.f;
			break;
		}
		damage += damage * 2;
	}
	else if (playerJob == PlayerJobSword)
	{
		switch (weaponNum) {
		case 0:
			damage = 10.f;
			break;
		case 1:
			damage = 13.f;
			break;
		case 2:
			damage = 16.f;
			break;
		case 3:
			damage = 19.f;
			break;
		case 4:
			damage = 22.f;
			break;
		case 5:
			damage = 25.f;
			break;
		}
		damage += damage;
	}
	else if (playerJob == PlayerJobBow)
	{
		switch (weaponNum) {
		case 0:
			damage = 15.f;
			break;
		case 1:
			damage = 20.f;
			break;
		case 2:
			damage = 25.f;
			break;
		case 3:
			damage = 30.f;
			break;
		case 4:
			damage = 40.f;
			break;
		}
		damage += damage * 3;
	}

	switch (accessoryNum) {
	case 1:
		damage -= (damage / 20);
		break;
	case 3:
		damage += (damage / 10);
		break;
	case 4:
		damage += (damage / 4);
		break;
	case 6:
		damage += (damage / 20);
		break;
	case 8:
		damage -= (damage * 3 / 20);
		break;
	default:
		break;
	}

	return damage;
}

void Player::SetPlayerHp(float damage)
{
	hp += damage;

	float max = maxHp;
	
	switch (accessoryNum) {
		// 20
	case 5:
		max += max / 5;
		break;
		// 5
	case 6:
		max += max / 20;
		break;
		// -10
	case 8:
		max -= max / 10;
		break;
		// 15
	case 9:
		max += max * (3 / 20);
		break;
	default:
		break;
	}

	if (hp > max)
		hp = max;
}

void Player::SetPlayerStemina(float changedata)
{
	stemina += changedata;

	float max = 100.f;

	switch (accessoryNum) {
	case 2:
		max += max / 10;
		break;
	case 5:
		max -= max / 10;
		break;
	case 6:
		max += max / 20;
		break;
	case 7:
		max += max * (3 / 20);
		break;
	case 10:
		max += max / 20;
		break;
	default:
		break;
	}
	maxStemina = max;

	if (stemina > max)
		stemina = max;
	else if (stemina < 0)
		stemina = 0.f;
}

float Player::GetPlayerdefencivePower()
{
	float dfp = defencivePower;

	switch (accessoryNum) {
	case 0:
		dfp += dfp / 20;
		break;
	case 1:
		dfp += dfp * (3 / 20);
		break;
	case 3:
		dfp -= dfp / 20;
		break;
	case 4:
		dfp -= dfp / 5;
		break;
	case 6:
		dfp += dfp / 20;
		break;
	case 7:
		dfp -= dfp / 20;
		break;
	case 10:
		dfp += dfp / 10;
		break;
	default:
		break;
	}

	return dfp;
}

void Player::SetAttackCoolTime(float time)
{
	float cooltime;
	switch (weaponNum) {
	case 0:
		cooltime = time;
		break;
	case 1:
		cooltime = time + 0.5f;
		break;
	case 2:
		cooltime = time + 0.1;
		break;
	case 3:
		cooltime = time;
		break;
	case 4:
		cooltime = time + 1.f;
		break;
	case 5:
		cooltime = time + 0.1;
		break;
	}

	attackCoolTime = cooltime;
}

void Player::SetPlayerAmmo()
{
	switch (weaponNum) {
	case 0:
		ammo = SCAR_AMMO;
		break;
	case 1:
		ammo = G28_AMMO;
		break;
	case 2:
		ammo = AK_AMMO;
		break;
	case 3:
		ammo = M4_AMMO;
		break;
	case 4:
		ammo = SNIPER_AMMO;
		break;
	case 5:
		ammo = BLACKAK_AMMO;
		break;
	}
}

RiflePlayer::RiflePlayer(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature, std::vector<void*> objects, 
	ParticleShader* particleShader, WeaponTrailerShader* weaponTrailerShader, int hObjectSize, int meshesNum)
{
	this->particleShader = particleShader;
	this->weaponTrailerShader = weaponTrailerShader;
	this->meshesNum = meshesNum;
	this->hObjectSize = hObjectSize;
	this->objects = objects;

	ammo = 40;

	shader = new SkinnedAnimationObjectsShader();
	shader->CreateShader(device, graphicsRootSignature, computeRootSignature);

	camera = ChangeCamera(SPACESHIP_CAMERA, 0.0f);

	haveModel = false;

	SetPosition(XMFLOAT3(1.0f, 2.0f, 3.0f), true);
	Rotate(20, 180, 0);

	camera->SetPosition(Vector3::Add(position, camera->GetOffset()));
	Update(0);

	if (camera) camera->CreateShaderVariables(device, commandList);
}

RiflePlayer::~RiflePlayer()
{
}

void RiflePlayer::OnPrepareRender(ID3D12GraphicsCommandList* commandList, bool isShadow)
{
	Player::OnPrepareRender(commandList, isShadow);
	XMMATRIX rotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(90.0f), 0.0f, 0.0f);
	world = Matrix4x4::Multiply(rotate, world);
}
/*3인칭 카메라일 때 플레이어 메쉬를 로컬 x-축을 중심으로 +90도 회전하고 렌더링한다. 왜냐하면 비행기 모델 메쉬
는 다음 그림과 같이 y-축 방향이 비행기의 앞쪽이 되도록 모델링이 되었기 때문이다. 그리고 이 메쉬를 카메라의 z-
축 방향으로 향하도록 그릴 것이기 때문이다.*/

//카메라를 변경할 때 호출되는 함수이다. nNewCameraMode는 새로 설정할 카메라 모드이다.
Camera *RiflePlayer::ChangeCamera(DWORD newCameraMode, float timeElapsed)
{
	DWORD currentCameraMode = (camera) ? camera->GetMode() : 0x00;
	if (currentCameraMode == newCameraMode) return(camera);
	switch (newCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		//플레이어의 특성을 1인칭 카메라 모드에 맞게 변경한다. 중력은 적용하지 않는다.
		SetFriction(200.0f);
		SetGravity(XMFLOAT3(0.0f, -9.8f, 0.0f));
		SetMaxVelocityXZ(125.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(FIRST_PERSON_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.0f);
		camera->SetOffset(XMFLOAT3(0.0f, 1.7f, 0.18f));
		camera->GenerateProjectionMatrix(0.1f, 500.0f, ASPECT_RATIO, 60.0f);
		camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	case SPACESHIP_CAMERA:
		//플레이어의 특성을 스페이스-쉽 카메라 모드에 맞게 변경한다. 중력은 적용하지 않는다.
		SetFriction(125.0f);
		SetGravity(XMFLOAT3(0.0f, 0.0f, 0.0f));
		SetMaxVelocityXZ(10.0f);
		SetMaxVelocityY(10.0f);
		camera = OnChangeCamera(SPACESHIP_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.0f);
		camera->SetOffset(XMFLOAT3(0.0f, 0.0f, 0.0f));
		camera->GenerateProjectionMatrix(1.01f, 1000.0f, ASPECT_RATIO, 90.0f);
		camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	case THIRD_PERSON_CAMERA:
		//플레이어의 특성을 3인칭 카메라 모드에 맞게 변경한다. 지연 효과와 카메라 오프셋을 설정한다.
		SetFriction(30.0f);
		SetGravity(XMFLOAT3(0.0f, -9.8f, 0.0f));
		SetMaxVelocityXZ(6.0f);
		SetMinVelocityXZ(-6.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(THIRD_PERSON_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.25f);
		//3인칭 카메라의 지연 효과를 설정한다. 값을 0.25f 대신에 0.0f와 1.0f로 설정한 결과를 비교하기 바란다.
		camera->SetOffset(XMFLOAT3(0.5f, 2.0f, -2.5f));
		camera->GenerateProjectionMatrix(1.01f, 500.0f, ASPECT_RATIO, 60.0f);
		camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	case ZOOM_MODE_CAMERA:
		SetFriction(30.0f);
		SetGravity(XMFLOAT3(0.0f, 0.0f, 0.0f));
		SetMaxVelocityXZ(6.0f);
		SetMinVelocityXZ(-6.0f);
		SetMaxVelocityY(10.0f);
		camera = OnChangeCamera(ZOOM_MODE_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.0f);
		camera->SetOffset(XMFLOAT3(0.0f, 1.0f, 2.0f));
		camera->GenerateProjectionMatrix(1.01f, 500.0f, ASPECT_RATIO, 30.0f);
		camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	default:
		break;
	}
	//플레이어를 시간의 경과에 따라 갱신(위치와 방향을 변경: 속도, 마찰력, 중력 등을 처리)한다.
	camera->SetPosition(Vector3::Add(position, camera->GetOffset()));
	Update(timeElapsed);
	return(camera);
}

// 플레이어의 위치가 바뀔 때 마다 호출되는 함수
void RiflePlayer::OnPlayerUpdateCallback(float fTimeElapsed)
{
	XMFLOAT3 playerPosition = GetPosition();
	HeightMapTerrain *terrain = (HeightMapTerrain *)playerUpdatedContext;
	/*지형에서 플레이어의 현재 위치 (x, z)의 지형 높이(y)를 구한다. 그리고 플레이어 메쉬의 높이가 12이고 플레이어의
	중심이 직육면체의 가운데이므로 y 값에 메쉬의 높이의 절반을 더하면 플레이어의 위치가 된다.*/
	float height = terrain->GetHeight(playerPosition.x, playerPosition.z);
	/*플레이어의 위치 벡터의 y-값이 음수이면(예를 들어, 중력이 적용되는 경우) 플레이어의 위치 벡터의 y-값이 점점
	작아지게 된다. 이때 플레이어의 현재 위치 벡터의 y 값이 지형의 높이(실제로 지형의 높이 + 6)보다 작으면 플레이어
	의 일부가 지형 아래에 있게 된다. 이러한 경우를 방지하려면 플레이어의 속도 벡터의 y 값을 0으로 만들고 플레이어
	의 위치 벡터의 y-값을 지형의 높이(실제로 지형의 높이 + 6)로 설정한다. 그러면 플레이어는 항상 지형 위에 있게 된다.*/
	if (playerPosition.y < height)
	{
		XMFLOAT3 xmf3PlayerVelocity = GetVelocity();
		xmf3PlayerVelocity.y = 0.0f;
		SetVelocity(xmf3PlayerVelocity);
		playerPosition.y = height;
		SetPosition(playerPosition);
	}
}

void RiflePlayer::OnCameraUpdateCallback(float timeElapsed)
{
	XMFLOAT3 cameraPosition = camera->GetPosition();
	/*높이 맵에서 카메라의 현재 위치 (x, z)에 대한 지형의 높이(y 값)를 구한다. 이 값이 카메라의 위치 벡터의 y-값 보
	다 크면 카메라가 지형의 아래에 있게 된다. 이렇게 되면 다음 그림의 왼쪽과 같이 지형이 그려지지 않는 경우가 발생
	한다(카메라가 지형 안에 있으므로 삼각형의 와인딩 순서가 바뀐다). 이러한 경우가 발생하지 않도록 카메라의 위치 벡
	터의 y-값의 최소값은 (지형의 높이 + 5)로 설정한다. 카메라의 위치 벡터의 y-값의 최소값은 지형의 모든 위치에서
	카메라가 지형 아래에 위치하지 않도록 설정해야 한다.*/
	HeightMapTerrain *terrain = (HeightMapTerrain *)cameraUpdatedContext;
	float height = terrain->GetHeight(cameraPosition.x, cameraPosition.z);
	if (cameraPosition.y <= height)
	{
		cameraPosition.y = height;
		camera->SetPosition(cameraPosition);
		if (camera->GetMode() == THIRD_PERSON_CAMERA || camera->GetMode() == FIRST_PERSON_CAMERA || (camera->GetMode() == ZOOM_MODE_CAMERA))
		{
			//3인칭 카메라의 경우 카메라 위치(y-좌표)가 변경되었으므로 카메라가 플레이어를 바라보도록 한다.
			ThirdPersonCamera *thirdPersonCamera = (ThirdPersonCamera *)camera;
			thirdPersonCamera->SetLookAt(GetPosition());
		}
	}
}


bool RiflePlayer::ShowEffect(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float totalTime)
{
	if (camera->GetMode() != SPACESHIP_CAMERA) {
		// ParticleInfo : particleSize, copyIdx, textureIdx, type, pos, size, color, age, acc, alpha
		float lerp = ::RandomPercent();
		XMFLOAT4 color = ::Vector4::Add(::Vector4::Multiply(lerp, XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f)), ::Vector4::Multiply((1 - lerp), XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f)));

		if (playerJob == PlayerJobRifle) {
			if (upAnimationStatus == MainCharacterFire)
			{
				if (totalTime - fireCoolTime > 0.1f) {
					float age = 0.01f;

					particleShader->CreateParticle(device, commandList,
						ParticleInfoInCPU{ particleShader->copyBufferMap["GunFire"], particleShader->textureMap["GunFire"], PARTICLE_TYPE_DISAPPEAR,
						XMFLOAT3(curWeaponPoint.end._41, curWeaponPoint.end._42, curWeaponPoint.end._43), XMFLOAT2(0.1f, 0.1f), color, age, age, 0.01f, 0.3f });
					fireCoolTime = totalTime;
				}
				isHit = false;
			}
			if (loAnimationStatus >= MainCharacterRun && loAnimationStatus <= MainCharacterRunRight)
			{
				if (totalTime - runCoolTime > 0.2f) {
					float age = 1.0f;
					XMFLOAT4 rColor{ 0.6, 0.5f, 0.25f, 1.0f };

					particleShader->CreateParticle(device, commandList,
						ParticleInfoInCPU{ particleShader->copyBufferMap["RunDust"], particleShader->textureMap["Dust"], PARTICLE_TYPE_SMALLER,
						Vector3::Add(Vector3::Add(this->position, this->look, 0.5f), XMFLOAT3(0, 0.3f, 0)), XMFLOAT2(0.5f, 0.5f), rColor, age, age, 1.0f, 0.3f });
					runCoolTime = totalTime;
				}
			}
			if (upAnimationStatus == MainCharacterHit)
			{
				if (isHit == false)
				{
					float age = 0.3f;

					particleShader->CreateParticle(device, commandList,
						ParticleInfoInCPU{ particleShader->copyBufferMap["Blood"], particleShader->textureMap["Blood"], PARTICLE_TYPE_DISAPPEAR,
						Vector3::Add(Vector3::Add(this->position, this->look, 0.3f), XMFLOAT3(0, 1.5f, 0)), XMFLOAT2(0.3f, 0.3f), color, age, age, 1.0f, 0.3f });
					isHit = true;
				}
			}
		}

		if (playerJob == PlayerJobSword)
		{
			if (loAnimationStatus >= SwordCharacterRun && loAnimationStatus <= SwordCharacterRunRight)
			{
				if (totalTime - runCoolTime > 0.2f) {
					float age = 1.0f;
					XMFLOAT4 rColor{ 0.6, 0.5f, 0.25f, 1.0f };

					particleShader->CreateParticle(device, commandList,
						ParticleInfoInCPU{ particleShader->copyBufferMap["RunDust"], particleShader->textureMap["Dust"], PARTICLE_TYPE_SMALLER,
						Vector3::Add(Vector3::Add(this->position, this->look, 0.5f), XMFLOAT3(0, 0.3f, 0)), XMFLOAT2(0.5f, 0.5f), rColor, age, age, 1.0f, 0.3f });
					runCoolTime = totalTime;
				}
			}
			if (upAnimationStatus == SwordCharacterHit)
			{
				if (isHit == false)
				{
					float age = 0.3f;

					particleShader->CreateParticle(device, commandList,
						ParticleInfoInCPU{ particleShader->copyBufferMap["Blood"], particleShader->textureMap["Blood"], PARTICLE_TYPE_DISAPPEAR,
						Vector3::Add(Vector3::Add(this->position, this->look, 0.3f), XMFLOAT3(0, 1.5f, 0)), XMFLOAT2(0.3f, 0.3f), color, age, age, 1.0f, 0.3f });
					isHit = true;
				}
			}
			if (upAnimationStatus >= SwordCharacterAttack && upAnimationStatus <= SwordCharacterAttack3)
			{
				if (totalTime - attackCoolTime > 0.016f)
				{
					// Effect
					float age = 0.3f;
					XMFLOAT4 rColor{ 0.75, 0.0f, 0.0f, 0.7f };
					XMFLOAT3 prePos = XMFLOAT3(preWeaponPoint.end._41, preWeaponPoint.end._42, preWeaponPoint.end._43);
					if (Vector3::Length(prePos) > 0) {
						XMFLOAT3 start0 = XMFLOAT3(preWeaponPoint.start._41, preWeaponPoint.start._42, preWeaponPoint.start._43);
						XMFLOAT3 end0 = XMFLOAT3(preWeaponPoint.end._41, preWeaponPoint.end._42, preWeaponPoint.end._43);

						XMFLOAT3 start1 = XMFLOAT3(curWeaponPoint.start._41, curWeaponPoint.start._42, curWeaponPoint.start._43);
						XMFLOAT3 end1 = XMFLOAT3(curWeaponPoint.end._41, curWeaponPoint.end._42, curWeaponPoint.end._43);

						weaponTrailerShader->AddBuffer(start0, end0, start1, end1, rColor, age, particleShader->textureMap["GunFire"]);
					}
					preWeaponPoint = curWeaponPoint;


					attackCoolTime = totalTime;
					return true;
				}
			}
			else {
				preWeaponPoint.Clear();
			}
		}
	}
	return false;
}


TerrainPlayer::TerrainPlayer(ID3D12Device *device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature *graphicsRootSignature, void *context, int meshesNum) : Player(meshesNum)
{
	camera = ChangeCamera(SPACESHIP_CAMERA, 0.0f);

	HeightMapTerrain *terrain = (HeightMapTerrain *)context;
	// 플레이어의 위치를 지형의 가운데(y-축 좌표는 지형의 높이보다 1500 높게)로 설정한다. 
	// 플레이어 위치 벡터의 y-좌표가 지형의 높이보다 크고 중력이 작용하도록 플레이어를
	// 설정하였으므로 플레이어는 점차적으로 하강하게 된다.
	float fHeight = terrain->GetHeight(terrain->GetWidth()*0.5f, terrain->GetLength()*0.5f);
	SetPosition(XMFLOAT3(terrain->GetWidth()*0.5f, fHeight + 1500.0f, terrain->GetLength()*0.5f));
	//플레이어의 위치가 변경될 때 지형의 정보에 따라 플레이어의 위치를 변경할 수 있도록 설정한다.

	SetPlayerUpdatedContext(terrain);
	//카메라의 위치가 변경될 때 지형의 정보에 따라 카메라의 위치를 변경할 수 있도록 설정한다.
	SetCameraUpdatedContext(terrain);
	CubeMeshDiffused *pCubeMesh = new CubeMeshDiffused(device, commandList, 4.0f, 12.0f, 4.0f);
	SetMesh(0, pCubeMesh);
	//플레이어를 렌더링할 셰이더를 생성한다.
	CreateShaderVariables(device, commandList);
}

TerrainPlayer::~TerrainPlayer()
{
}

Camera *TerrainPlayer::ChangeCamera(DWORD newCameraMode, float timeElapsed)
{
	DWORD currentCameraMode = (camera) ? camera->GetMode() : 0x00;
	if (currentCameraMode == newCameraMode) return(camera);
	switch (newCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		SetFriction(20.0f);
		//1인칭 카메라일 때 플레이어에 y-축 방향으로 중력이 작용한다.
		SetGravity(XMFLOAT3(0.0f, -250.0f, 0.0f));
		SetMaxVelocityXZ(300.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(FIRST_PERSON_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.0f);
		camera->SetOffset(XMFLOAT3(0.0f, 20.0f, 0.0f));
		camera->GenerateProjectionMatrix(1.01f, 50000.0f, ASPECT_RATIO, 60.0f);
		break;
	case SPACESHIP_CAMERA:
		SetFriction(20.0f);
		//스페이스 쉽 카메라일 때 플레이어에 중력이 작용하지 않는다.
		SetGravity(XMFLOAT3(0.0f, 0.0f, 0.0f));
		SetMaxVelocityXZ(300.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(SPACESHIP_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.0f);
		camera->SetOffset(XMFLOAT3(0.0f, 0.0f, 0.0f));
		camera->GenerateProjectionMatrix(1.01f, 50000.0f, ASPECT_RATIO, 60.0f);
		break;
	case THIRD_PERSON_CAMERA:
		SetFriction(20.0f);
		//3인칭 카메라일 때 플레이어에 y-축 방향으로 중력이 작용한다.
		SetGravity(XMFLOAT3(0.0f, -250.0f, 0.0f));
		SetMaxVelocityXZ(300.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(THIRD_PERSON_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.25f);
		camera->SetOffset(XMFLOAT3(0.0f, 20.0f, -50.0f));
		camera->GenerateProjectionMatrix(1.01f, 5000.0f, ASPECT_RATIO, 60.0f);
		break;
	default:
		break;
	}
	Update(timeElapsed);
	return(camera);
}

// 플레이어의 위치가 바뀔 때 마다 호출되는 함수
void TerrainPlayer::OnPlayerUpdateCallback(float fTimeElapsed)
{
	XMFLOAT3 playerPosition = GetPosition();
	HeightMapTerrain *terrain = (HeightMapTerrain *)playerUpdatedContext;
	/*지형에서 플레이어의 현재 위치 (x, z)의 지형 높이(y)를 구한다. 그리고 플레이어 메쉬의 높이가 12이고 플레이어의
	중심이 직육면체의 가운데이므로 y 값에 메쉬의 높이의 절반을 더하면 플레이어의 위치가 된다.*/
	float height = terrain->GetHeight(playerPosition.x, playerPosition.z) + 6.0f;
	/*플레이어의 위치 벡터의 y-값이 음수이면(예를 들어, 중력이 적용되는 경우) 플레이어의 위치 벡터의 y-값이 점점
	작아지게 된다. 이때 플레이어의 현재 위치 벡터의 y 값이 지형의 높이(실제로 지형의 높이 + 6)보다 작으면 플레이어
	의 일부가 지형 아래에 있게 된다. 이러한 경우를 방지하려면 플레이어의 속도 벡터의 y 값을 0으로 만들고 플레이어
	의 위치 벡터의 y-값을 지형의 높이(실제로 지형의 높이 + 6)로 설정한다. 그러면 플레이어는 항상 지형 위에 있게 된다.*/
	if (playerPosition.y < height)
	{
		XMFLOAT3 xmf3PlayerVelocity = GetVelocity();
		xmf3PlayerVelocity.y = 0.0f;
		SetVelocity(xmf3PlayerVelocity);
		playerPosition.y = height;
		SetPosition(playerPosition);
	}
}

void TerrainPlayer::OnCameraUpdateCallback(float timeElapsed)
{
	XMFLOAT3 cameraPosition = camera->GetPosition();
	/*높이 맵에서 카메라의 현재 위치 (x, z)에 대한 지형의 높이(y 값)를 구한다. 이 값이 카메라의 위치 벡터의 y-값 보
	다 크면 카메라가 지형의 아래에 있게 된다. 이렇게 되면 다음 그림의 왼쪽과 같이 지형이 그려지지 않는 경우가 발생
	한다(카메라가 지형 안에 있으므로 삼각형의 와인딩 순서가 바뀐다). 이러한 경우가 발생하지 않도록 카메라의 위치 벡
	터의 y-값의 최소값은 (지형의 높이 + 5)로 설정한다. 카메라의 위치 벡터의 y-값의 최소값은 지형의 모든 위치에서
	카메라가 지형 아래에 위치하지 않도록 설정해야 한다.*/
	HeightMapTerrain *terrain = (HeightMapTerrain *)cameraUpdatedContext;
	float height = terrain->GetHeight(cameraPosition.x, cameraPosition.z) + 5.0f;
	if (cameraPosition.y <= height)
	{
		cameraPosition.y = height;
		camera->SetPosition(cameraPosition);
		if (camera->GetMode() == THIRD_PERSON_CAMERA || camera->GetMode() == FIRST_PERSON_CAMERA)
		{
			//3인칭 카메라의 경우 카메라 위치(y-좌표)가 변경되었으므로 카메라가 플레이어를 바라보도록 한다.
			ThirdPersonCamera *thirdPersonCamera = (ThirdPersonCamera *)camera;
			thirdPersonCamera->SetLookAt(GetPosition());
		}
	}
}

void RealMove()
{
}
