#include "stdafx.h"
#include "BlendingShader.h"
#include "Material.h"
#include "Scene.h"

BlendingShader::BlendingShader()
{
}

BlendingShader::~BlendingShader()
{
}

D3D12_BLEND_DESC BlendingShader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = TRUE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(blendDesc);
}

D3D12_SHADER_BYTECODE BlendingShader::CreateShadowPixelShader()
{
	return Shader::CompileShaderFromFile(L"ShadowCaster.hlsl", "PSCSMStandardInstancing", "ps_5_1", &pixelShaderBlob);
}

void BlendingShader::CreateShader(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	shadowPipelineStateNum = 1;
	shadowPipelineStates = new ID3D12PipelineState*[shadowPipelineStateNum];

	Shader::CreateShader(device, graphicsRootSignature);
	Shader::CreateShadowShader(device, graphicsRootSignature);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();
	if (geometryShaderBlob) geometryShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}

void BlendingShader::CreateShaderVariables(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx)
{
	UINT tempObjectByte = 0;
	ID3D12Resource*** tempObject = NULL;
	VS_VB_INSTANCE_OBJECT*** tempMappedObject = NULL;
	D3D12_VERTEX_BUFFER_VIEW** tempObjectBufferView = NULL;

	tempObjectByte = sizeof(VS_VB_INSTANCE_OBJECT);

	tempObject = new ID3D12Resource**[CAMERAS_NUM];
	tempMappedObject = new VS_VB_INSTANCE_OBJECT**[CAMERAS_NUM];
	tempObjectBufferView = new D3D12_VERTEX_BUFFER_VIEW*[CAMERAS_NUM];

	for (int i = 0; i < CAMERAS_NUM; ++i)
	{
		tempObject[i] = new ID3D12Resource*[instanceBuffer[idx]->framesNum];
		tempMappedObject[i] = new VS_VB_INSTANCE_OBJECT*[instanceBuffer[idx]->framesNum];
		tempObjectBufferView[i] = new D3D12_VERTEX_BUFFER_VIEW[instanceBuffer[idx]->framesNum];
	}

	for (int j = 0; j < CAMERAS_NUM; ++j) {
		for (int i = 0; i < instanceBuffer[idx]->framesNum; ++i)
		{
			tempObject[j][i] = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTANCE_OBJECT) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
			//인스턴스 정보를 저장할 정점 버퍼를 업로드 힙 유형으로 생성한다.

			tempObject[j][i]->Map(0, NULL, (void **)&tempMappedObject[j][i]);
			//정점 버퍼(업로드 힙)에 대한 포인터를 저장한다.

			tempObjectBufferView[j][i].BufferLocation = tempObject[j][i]->GetGPUVirtualAddress();
			tempObjectBufferView[j][i].StrideInBytes = tempObjectByte;
			tempObjectBufferView[j][i].SizeInBytes = tempObjectByte * ((UINT)BUFFERSIZE);
			//정점 버퍼에 대한 뷰를 생성한다.
		}
	}
	instanceBuffer[idx]->vbGameObjectBytes = tempObjectByte;

	instanceBuffer[idx]->vbGameObjects = tempObject;
	instanceBuffer[idx]->vbMappedGameObjects = tempMappedObject;
	instanceBuffer[idx]->instancingGameObjectBufferView = tempObjectBufferView;

	// Create Cashe	
	instanceBuffer[idx]->worldCashe = new VS_VB_INSTANCE_OBJECT*[instanceBuffer[idx]->framesNum];
	instanceBuffer[idx]->isVisable = new bool[BUFFERSIZE];
	for (int i = 0; i < instanceBuffer[idx]->framesNum; ++i)
	{
		instanceBuffer[idx]->worldCashe[i] = new VS_VB_INSTANCE_OBJECT[BUFFERSIZE];
	}

}

void BlendingShader::UpdateShaderVariables(ID3D12GraphicsCommandList * commandList, UINT cameraIdx)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		int objNum = 0;
		for (int j = 0; j < instanceBuffer[k]->objects.size() - 1; ++j)
		{
			if (instanceBuffer[k]->isVisable[j])
			{
				for (int i = 0; i < instanceBuffer[k]->framesNum; ++i)
				{
					instanceBuffer[k]->vbMappedGameObjects[cameraIdx][i][objNum] = instanceBuffer[k]->worldCashe[i][j];
				}
				objNum++;
			}
		}
		instanceBuffer[k]->renderObjNum = objNum;
	}
}

void BlendingShader::ReleaseShaderVariables()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->vbGameObjects)
		{
			for (int i = 0; i < CAMERAS_NUM; ++i) {
				for (int j = 0; j < instanceBuffer[k]->framesNum; ++j)
				{
					instanceBuffer[k]->vbGameObjects[i][j]->Unmap(0, NULL);
					instanceBuffer[k]->vbGameObjects[i][j]->Release();
				}
			}
		}
	}
}

void BlendingShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, std::vector<void*>& context, int hObjectSize)
{
	objects.reserve(100);

	HeightMapTerrain *terrain = (HeightMapTerrain *)context[0];
	objects.emplace_back(terrain);
	float terrainWidth = terrain->GetWidth(), terrainLength = terrain->GetLength();

	// Tree Object Build
	BlendingBuffer* treeObject = new BlendingBuffer();
	BuildInstanceObject(device, commandList, treeObject, 0, 0, (GameObject*)context[2], 1, XMFLOAT3{ 2.0f , 4.0f , 2.0f }, XMFLOAT3(0, 0, 0), XMFLOAT3(0, 0, 0));

	BlendingBuffer* wireFenceObject = new BlendingBuffer();
	BuildInstanceObject(device, commandList, wireFenceObject, 0, 0, (GameObject*)context[10], 1, XMFLOAT3(0.3f, 3.5f, 2.5f), XMFLOAT3(0, 0, 0), XMFLOAT3(400, 0, 0));

	BlendingBuffer* treeObject1 = new BlendingBuffer();
	BuildInstanceObject(device, commandList, treeObject1, 0, 0, (GameObject*)context[14], 1, XMFLOAT3(1.5f, 4.0f, 1.5f), XMFLOAT3(0, 0, 0), XMFLOAT3(16, 0, 5));

	BlendingBuffer* treeObject2 = new BlendingBuffer();
	BuildInstanceObject(device, commandList, treeObject2, 1, 0, (GameObject*)context[15], 1, XMFLOAT3(1.2f, 2.5f, 1.2f), XMFLOAT3(0, 0, 0), XMFLOAT3(16, 0, 5));

	BlendingBuffer* treeObject3 = new BlendingBuffer();
	BuildInstanceObject(device, commandList, treeObject3, 0, 0, (GameObject*)context[16], 1, XMFLOAT3(1.2f, 4.2f, 1.2f), XMFLOAT3(0, 0, 0), XMFLOAT3(16, 0, 5));

	for (int i = 0; i < instanceBuffer.size(); ++i)
	{
		CreateShaderVariables(device, commandList, i);
	}
	SetTextures(device, commandList);
}

void BlendingShader::BuildInstanceObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, BlendingBuffer * instanceObject, int objectNum, int pipeLineStateIdx, GameObject * objectModel, int modelNumber, XMFLOAT3 boundingSize, XMFLOAT3 rotate, XMFLOAT3 position)
{

	objects.emplace_back(objectModel);
	instanceObject->objectsNum = objectNum + 1; //dummy
	instanceObject->objects.reserve(instanceObject->objectsNum);
	instanceObject->framesNum = objectModel->GetFramesNum() + 1;
	instanceObject->pipeLineStateIdx = pipeLineStateIdx;
	instanceObject->shadowPipeLineStateIdx = pipeLineStateIdx;
	instanceObject->useBillboard = -1;
	instanceObject->textureName = objectModel->textureName;
	instanceObject->obb = BoundingOrientedBox(XMFLOAT3(0.0f, 0.0f, 0.0f), boundingSize, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f));


	GameObject* dummy = new GameObject();
	dummy->SetChild(objectModel);
	dummy->AddRef();

	instanceObject->objects.emplace_back(dummy);

	for (int i = 0; i < instanceObject->objectsNum - 1; ++i)
	{
		GameObject* gameobject = new GameObject();
		gameobject->SetChild(objectModel);

		gameobject->SetPosition(position.x, position.y, position.z);
		gameobject->Rotate(rotate.x, rotate.y, rotate.z);
		gameobject->SetScale(1.0f, 1.0f, 1.0f);
		instanceObject->objects.emplace_back(gameobject);
	}
	instanceBuffer.emplace_back(instanceObject);
}

void BlendingShader::AnimateObjects(float timeElapsed, Camera * camera)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		//ApplyLOD(camera, k);
	}
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		for (int j = 1; j < instanceBuffer[k]->objects.size(); ++j)
		{
			if (instanceBuffer[k]->objects[j])
			{
				instanceBuffer[k]->objects[j]->Animate(timeElapsed);
				instanceBuffer[k]->objects[j]->UpdateTransform(instanceBuffer[k]->worldCashe, j - 1, NULL);
			}
		}
	}
}

void BlendingShader::ReleaseObjects()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (!instanceBuffer[k]->objects.empty())
			instanceBuffer[k]->objects[0]->Release();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->Release();
	}
}

void BlendingShader::ReleaseUploadBuffers()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->objects[0])
			instanceBuffer[k]->objects[0]->ReleaseUploadBuffers();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->ReleaseUploadBuffers();
	}
}

void BlendingShader::SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->textureName.diffuse.size() > 0)
		{
			Texture* diffuseTexture = new Texture((int)instanceBuffer[k]->textureName.diffuse.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.diffuse.size(); ++i)
			{
				diffuseTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.diffuse[i].second, instanceBuffer[k]->textureName.diffuse[i].first);
			}
			instanceBuffer[k]->diffuseIdx = Scene::CreateShaderResourceViews(device, commandList, diffuseTexture, GraphicsRootDiffuseTextures);
			textures.emplace_back(diffuseTexture);
		}
		if (instanceBuffer[k]->textureName.normal.size() > 0)
		{
			Texture* normalTexture = new Texture((int)instanceBuffer[k]->textureName.normal.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.normal.size(); ++i)
			{
				normalTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.normal[i].second, instanceBuffer[k]->textureName.normal[i].first);
			}
			instanceBuffer[k]->normalIdx = Scene::CreateShaderResourceViews(device, commandList, normalTexture, GraphicsRootNormalTextures);
			textures.emplace_back(normalTexture);
		}
		if (instanceBuffer[k]->textureName.specular.size() > 0)
		{
			Texture* specularTexture = new Texture((int)instanceBuffer[k]->textureName.specular.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.specular.size(); ++i)
			{
				specularTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.specular[i].second, instanceBuffer[k]->textureName.specular[i].first);
			}
			instanceBuffer[k]->specularIdx = Scene::CreateShaderResourceViews(device, commandList, specularTexture, GraphicsRootSpecularTextures);
			textures.emplace_back(specularTexture);
		}
		if (instanceBuffer[k]->textureName.metallic.size() > 0)
		{
			Texture* metallicTexture = new Texture((int)instanceBuffer[k]->textureName.metallic.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.metallic.size(); ++i)
			{
				metallicTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.metallic[i].second, instanceBuffer[k]->textureName.metallic[i].first);
			}
			instanceBuffer[k]->metallicIdx = Scene::CreateShaderResourceViews(device, commandList, metallicTexture, GraphicsRootMetallicTextures);
			textures.emplace_back(metallicTexture);
		}
	}
}

void BlendingShader::DeleteAllObjcet()
{
	for (int i = 0; i < instanceBuffer.size(); ++i) {
		while (true) {
			if (instanceBuffer[i]->objects.size() < 2) {
				break;
			}
			GameObject* temp = instanceBuffer[i]->objects.back();
			instanceBuffer[i]->objects.pop_back();
			delete temp;

		}
		instanceBuffer[i]->objectsNum = 1;
	}
}

void BlendingShader::LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType)
{
	GameObject* object = new GameObject();
	if (objectType != 0) {
		object->SetChild(objects[objectType]);
	}
	object->SetTransform(world);

	instanceBuffer[objectType]->objects.emplace_back(object);
	instanceBuffer[objectType]->objectsNum++;
}

void BlendingShader::OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx, bool isShadow)
{

	if (instanceBuffer[idx]->diffuseIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->diffuseIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->diffuseIdx].srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->normalIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->normalIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->normalIdx].srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->specularIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->specularIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->specularIdx].srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->metallicIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->metallicIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->metallicIdx].srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->emissionIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->emissionIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->emissionIdx].srvGpuDescriptorHandle);

	if (isShadow)
	{
		if (Shader::curPipelineState != shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx]) {
			commandList->SetPipelineState(shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx]);
			curPipelineState = shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx];
		}
	}
	else
	{
		if (Shader::curPipelineState != pipelineStates[instanceBuffer[idx]->pipeLineStateIdx]) {
			commandList->SetPipelineState(pipelineStates[instanceBuffer[idx]->pipeLineStateIdx]);
			curPipelineState = pipelineStates[instanceBuffer[idx]->pipeLineStateIdx];
		}
	}
}

void BlendingShader::Render(ID3D12GraphicsCommandList * commandList, bool isShadow, UINT cameraIdx)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		BoundingOrientedBox obb = instanceBuffer[k]->obb;
		for (int i = 1; i < instanceBuffer[k]->objects.size(); ++i)
		{
			BoundingOrientedBox resultOBB;
			obb.Transform(resultOBB, XMLoadFloat4x4(&instanceBuffer[k]->objects[i]->GetWorld()));
			bool isVisable = true;
			/*if (isShadow)
			{
				BoundingOrientedBox camFrustum = *(BoundingOrientedBox*)frustum;
				isVisable = camFrustum.Intersects(resultOBB);
			}
			else
			{
				BoundingFrustum camFrustum = *(BoundingFrustum*)frustum;
				isVisable = camFrustum.Intersects(resultOBB);
			}*/

			instanceBuffer[k]->isVisable[i - 1] = isVisable;
		}

	}

	UpdateShaderVariables(commandList, cameraIdx);

	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		OnPrepareRender(commandList, k, isShadow);

		if (!instanceBuffer[k]->objects.empty())
		{
			if (instanceBuffer[k]->renderObjNum > 0) {
				instanceBuffer[k]->objects[0]->Render(commandList, UINT(instanceBuffer[k]->renderObjNum),
					instanceBuffer[k]->instancingGameObjectBufferView[cameraIdx], isShadow);
			}
		}
	}
}

void BlendingShader::ApplyLOD(Camera * camera, int idx)
{
}

///////////////////////////////////////////////////////////////
//  BillboardShader

BillboardShader::BillboardShader()
{
}

BillboardShader::~BillboardShader()
{
}

D3D12_BLEND_DESC BillboardShader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = TRUE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(blendDesc);
}

D3D12_SHADER_BYTECODE BillboardShader::CreateVertexShader()
{
	return Shader::CompileShaderFromFile(L"Billboard.hlsl", "VSBillboard", "vs_5_1", &vertexShaderBlob);
}

D3D12_SHADER_BYTECODE BillboardShader::CreatePixelShader()
{
	return Shader::CompileShaderFromFile(L"Billboard.hlsl", "PSBillboard", "ps_5_1", &pixelShaderBlob);
}

D3D12_SHADER_BYTECODE BillboardShader::CreateGeomtryShader()
{
	return Shader::CompileShaderFromFile(L"Billboard.hlsl", "GSBillboard", "gs_5_1", &geometryShaderBlob);
}

D3D12_SHADER_BYTECODE BillboardShader::CreateShadowVertexShader()
{
	return Shader::CompileShaderFromFile(L"Billboard.hlsl", "VSBillboard", "vs_5_1", &vertexShaderBlob);
}

D3D12_SHADER_BYTECODE BillboardShader::CreateShadowPixelShader()
{
	return Shader::CompileShaderFromFile(L"Billboard.hlsl", "PSBillboard", "ps_5_1", &pixelShaderBlob);
}

D3D12_SHADER_BYTECODE BillboardShader::CreateShadowGeomtryShader()
{
	return Shader::CompileShaderFromFile(L"Billboard.hlsl", "GSBillboard", "gs_5_1", &geometryShaderBlob);
}

D3D12_INPUT_LAYOUT_DESC BillboardShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 2;
	D3D12_INPUT_ELEMENT_DESC *inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[1] = { "BILLBOARDINFO", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	return(inputLayoutDesc);
}

D3D12_PRIMITIVE_TOPOLOGY_TYPE BillboardShader::SetTopologyType()
{
	return D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
}

void BillboardShader::CreateShader(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	shadowPipelineStateNum = 1;
	shadowPipelineStates = new ID3D12PipelineState*[shadowPipelineStateNum];

	Shader::CreateShader(device, graphicsRootSignature);
	Shader::CreateShadowShader(device, graphicsRootSignature);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();
	if (geometryShaderBlob) geometryShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}

void BillboardShader::CreateShaderVariables(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx)
{
	ID3D12Resource* tempObject = NULL;
	VS_VB_INSTANCE_BILLBOARD* tempMappedObject = NULL;
	D3D12_VERTEX_BUFFER_VIEW tempObjectBufferView;
	UINT bytes = sizeof(VS_VB_INSTANCE_BILLBOARD);

	tempObject = ::CreateBufferResource(device, commandList, NULL, bytes * BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	tempObject->Map(0, NULL, (void**)tempMappedObject);

	tempObjectBufferView.BufferLocation = tempObject->GetGPUVirtualAddress();
	tempObjectBufferView.StrideInBytes = bytes;
	tempObjectBufferView.SizeInBytes = bytes * BUFFERSIZE;

	instanceBuffer[idx]->vbBillboardObjectBytes = bytes;
	instanceBuffer[idx]->vbBillboardObjects = tempObject;
	instanceBuffer[idx]->vbMappedBillboardObjects = tempMappedObject;
	instanceBuffer[idx]->instancingBillboardObjectBufferView = tempObjectBufferView;
}

void BillboardShader::UpdateShaderVariables(ID3D12GraphicsCommandList * commandList, UINT cameraIdx)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		for (int j = 0; j < instanceBuffer[k]->objects.size() - 1; ++j)
		{
			instanceBuffer[k]->vbMappedBillboardObjects[j].position = instanceBuffer[k]->objects[j]->GetPosition();
		}
	}
}

void BillboardShader::ReleaseShaderVariables()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->vbBillboardObjects)
		{
			instanceBuffer[k]->vbBillboardObjects->Unmap(0, NULL);
			instanceBuffer[k]->vbBillboardObjects->Release();
		}
	}
}

void BillboardShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, std::vector<void*>& context)
{
	for (int i = 0; i < instanceBuffer.size(); ++i)
	{
		CreateShaderVariables(device, commandList, i);
	}
	SetTextures(device, commandList);
}

void BillboardShader::AnimateObjects(float timeElapsed, Camera * camera)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		//ApplyLOD(camera, k);
	}
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		for (int j = 1; j < instanceBuffer[k]->objects.size(); ++j)
		{
			if (instanceBuffer[k]->objects[j])
			{
				instanceBuffer[k]->objects[j]->Animate(timeElapsed);
				instanceBuffer[k]->objects[j]->UpdateTransform(instanceBuffer[k]->vbMappedBillboardObjects, j - 1);
			}
		}
	}
}

void BillboardShader::ReleaseObjects()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (!instanceBuffer[k]->objects.empty())
			instanceBuffer[k]->objects[0]->Release();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->Release();
	}
}

void BillboardShader::ReleaseUploadBuffers()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->objects[0])
			instanceBuffer[k]->objects[0]->ReleaseUploadBuffers();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->ReleaseUploadBuffers();
	}
}

void BillboardShader::SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	//for (int k = 0; k < instanceBuffer.size(); ++k)
	//{
	//	if (instanceBuffer[k]->textureName.diffuse.size() > 0)
	//	{
	//		Texture* billboardTexture = new Texture((int)instanceBuffer[k]->textureName.diffuse.size(), ResourceTexture2D, 0);
	//		for (int i = 0; i < instanceBuffer[k]->textureName.diffuse.size(); ++i)
	//		{
	//			billboardTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.diffuse[i].second, instanceBuffer[k]->textureName.diffuse[i].first);
	//		}
	//		instanceBuffer[k]->diffuseIdx = Scene::CreateShaderResourceViews(device, commandList, billboardTexture, GraphicsRootDiffuseTextures);
	//		textures.emplace_back(billboardTexture);
	//	}
	//}
}

void BillboardShader::DeleteAllObjcet()
{
	for (int i = 0; i < instanceBuffer.size(); ++i) {
		while (true) {
			if (instanceBuffer[i]->objects.size() < 2) {
				break;
			}
			GameObject* temp = instanceBuffer[i]->objects.back();
			instanceBuffer[i]->objects.pop_back();
			delete temp;

		}
		instanceBuffer[i]->objectsNum = 1;
	}
}

void BillboardShader::OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx, bool isShadow)
{

	if (instanceBuffer[idx]->billboardTexIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[instanceBuffer[idx]->billboardTexIdx].rootParameterIndex, Scene::rootArgumentInfos[instanceBuffer[idx]->billboardTexIdx].srvGpuDescriptorHandle);

	if (isShadow)
	{
		if (Shader::curPipelineState != shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx]) {
			commandList->SetPipelineState(shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx]);
			curPipelineState = shadowPipelineStates[instanceBuffer[idx]->shadowPipeLineStateIdx];
		}
	}
	else
	{
		if (Shader::curPipelineState != pipelineStates[instanceBuffer[idx]->pipeLineStateIdx]) {
			commandList->SetPipelineState(pipelineStates[instanceBuffer[idx]->pipeLineStateIdx]);
			curPipelineState = pipelineStates[instanceBuffer[idx]->pipeLineStateIdx];
		}
	}
}

void BillboardShader::Render(ID3D12GraphicsCommandList * commandList, bool isShadow, UINT cameraIdx)
{
	UpdateShaderVariables(commandList, cameraIdx);

	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		OnPrepareRender(commandList, k, isShadow);

		if (!instanceBuffer[k]->objects.empty())
		{
			//instanceBuffer[k]->objects[0]->Render(commandList, camera, UINT(instanceBuffer[k]->objects.size()),
			//	instanceBuffer[k]->instancingBillboardObjectBufferView, isShadow);
		}
	}
}

void BillboardShader::ApplyLOD(Camera * camera, int idx)
{
}
