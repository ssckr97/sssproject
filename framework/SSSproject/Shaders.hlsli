//////////////////////////////////////////////////////////////////////////////////////////////
// Define
// Texture resource
#define MATERIAL_DIFFUSE_MAP		0x01
#define MATERIAL_NORMAL_MAP			0x02
#define MATERIAL_SPECULAR_MAP		0x04
#define MATERIAL_METALLIC_MAP		0x08
#define MATERIAL_EMISSION_MAP		0x10

// MAX ShadowMap
#define MAX_CASCADE_SIZE            3

// Skinned Animation
#define MAX_VERTEX_INFLUENCES		4
#define SKINNED_ANIMATION_BONES		128

// Fog Mode
#define FOG_LINEAR                  1.0f
#define FOG_EXP                     2.0f
#define FOG_EXP2                    3.0f

// Particle Type
#define PARTICLE_TYPE_DISAPPEAR		0
#define PARTICLE_TYPE_FOREVER		1
#define PARTICLE_TYPE_TORNADO		2
#define PARTICLE_TYPE_SMALLER		3

//////////////////////////////////////////////////////////////////////////////////////////////
// cBuffer
cbuffer cbPlayerInfo : register(b0)
{
    matrix gmtxPlayerWorld : packoffset(c0);
};
cbuffer cbCameraInfo : register(b1)
{
    matrix gmtxView : packoffset(c0);
    matrix gmtxProjection : packoffset(c4);
    matrix gmtxInvView : packoffset(c8);
    matrix gmtxInvProjection : packoffset(c12);
    float3 gvCameraPosition : packoffset(c16);
};
cbuffer cbGameObjectInfo : register(b2)
{
    matrix gmtxWorld : packoffset(c0);
};
cbuffer cbMaterialInfo : register(b3)
{
    uint gnMaterial : packoffset(c0.x);
    uint gnTextureMask : packoffset(c0.y);
    uint gnTextureIdx : packoffset(c0.z);
};
cbuffer cbTimeInfo : register(b4)
{
    float gfTotalTime : packoffset(c0.x);
    float gfTimeElapsed : packoffset(c0.y);
    uint gnSwapchainIdx : packoffset(c0.z);
}

// b5 : material, b6 : light

cbuffer cbParticleInfo : register(b7)
{
    float gfParticleAge : packoffset(c0);
    int gnParticleType : packoffset(c0.y);
    
}

cbuffer cbLightCamera : register(b8)
{
    matrix gmtxLightViewProjection[MAX_CASCADE_SIZE];
    float4 gfCascadeLength[MAX_CASCADE_SIZE];
 
    float gfShadowTexelSize;
    float gfCasacdeNum;
}

cbuffer cbGameObjectInfo : register(b9)
{
    uint gnVerticesNum : packoffset(c0);
};

cbuffer cbFogInfo : register(b10)
{
    float4 gvFogColor : packoffset(c0);
    float4 gvFogInfo : packoffset(c1);  //Mode, Start, End, Density
};

//////////////////////////////////////////////////////////////////////////////////////////////
// Texture
Texture2D gtxtTerrainDetailTexture[12] : register(t0);
Texture2D gtxtTerrainNormalTexture[12] : register(t12);
Texture2D gtxtTerrainAlphaTexture[3] : register(t24);

Texture2D gtxtDiffuseTexture[10] : register(t27);
Texture2D gtxtNormalTexture[10] : register(t37);
Texture2D gtxtSpecularTexture[10] : register(t47);
Texture2D gtxtMetallicTexture[10] : register(t57);

Texture2D gtxtBillboardTexture[10] : register(t67);
Texture2D gtxtParticleTexture[10] : register(t77);

Texture2D gtxtDepthTexture[2] : register(t87);
Texture2D gtxtShadowMapTexture[2] : register(t89);

TextureCube gtxtSkyBoxTexture : register(t91);

Texture2D gtxtUiTexture[88] : register(t110);

//////////////////////////////////////////////////////////////////////////////////////////////
// Sampler
SamplerState gssWrap : register(s0);
SamplerState gssClamp : register(s1);
SamplerState gssDepthClamp : register(s2);
SamplerComparisonState gssCmpShadowClamp : register(s3);
SamplerState gssAnisotropic : register(s4);


//////////////////////////////////////////////////////////////////////////////////////////////
// Struct
struct ParticleData
{
    float3 position;
    float3 velocity;
    float age;
};

//////////////////////////////////////////////////////////////////////////////////////////////
// Structured Buffer
StructuredBuffer<ParticleData> gsbParticleData : register(t100);

StructuredBuffer<float3> gsbSkinnedPosition : register(t101);
StructuredBuffer<float3> gsbSkinnedNormal : register(t102);
StructuredBuffer<float3> gsbSkinnedTangent : register(t103);
StructuredBuffer<float3> gsbSkinnedBiTangent : register(t104);

//////////////////////////////////////////////////////////////////////////////////////////////
// Static
static matrix gmtxProjectToTexture = { 0.5f, 0.0f, 0.0f, 0.0f, 0.0f, -0.5f, 0.0f, 0.0f, 0.0f, 0.0f, 1.0f, 0.0f, 0.5f, 0.5f, 0.0f, 1.0f };
static float3 gf3BillboardPos[4] = { float3(-1.0f, 1.0f, 0.0f), float3(1.0f, 1.0f, 0.0f), float3(-1.0f, -1.0f, 0.0f), float3(1.0f, -1.0f, 0.0f) };
static float2 gf2BillboardUVs[4] = { float2(0.0f, 0.0f), float2(1.0f, 0.0f), float2(0.0f, 1.0f), float2(1.0f, 1.0f) };
