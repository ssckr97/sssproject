#pragma once

#include "ObjectShader.h"
#include "GameObject.h"
#include "Camera.h"
#include "Scene.h"

class Player : public GameObject
{
protected:
	XMFLOAT3 colPos;
	XMFLOAT3 position;
	XMFLOAT3 right;
	XMFLOAT3 up;
	XMFLOAT3 look;
	//플레이어의 위치 벡터, x-축(Right), y-축(Up), z-축(Look) 벡터이다.
	int direction = DirectionNoMove;
	int potal = NOPOTAL;
	XMFLOAT3 scale = XMFLOAT3(1.0f, 1.0f, 1.0f);

	float pitch;
	float yaw;
	float roll;
	//플레이어가 로컬 x-축(Right), y-축(Up), z-축(Look)으로 얼마만큼 회전했는가를 나타낸다.

	float mass = 5.0f;
	XMFLOAT3 acc;
	XMFLOAT3 velocity;
	XMFLOAT3 finalvec;
	//플레이어의 이동 속도를 나타내는 벡터이다.

	XMFLOAT3 gravity;
	//플레이어에 작용하는 중력을 나타내는 벡터이다.

	float maxVelocityXZ;
	float minVelocityXZ;
	//xz-평면에서 (한 프레임 동안) 플레이어의 이동 속력의 최대값을 나타낸다.

	float maxVelocityY;
	//y-축 방향으로 (한 프레임 동안) 플레이어의 이동 속력의 최대값을 나타낸다.

	float friction;
	//플레이어에 작용하는 마찰력을 나타낸다.

	LPVOID playerUpdatedContext;
	//플레이어의 위치가 바뀔 때마다 호출되는 OnPlayerUpdateCallback() 함수에서 사용하는 데이터이다.

	LPVOID cameraUpdatedContext;
	//카메라의 위치가 바뀔 때마다 호출되는 OnCameraUpdateCallback() 함수에서 사용하는 데이터이다.

	Camera *camera = NULL;
	//플레이어에 현재 설정된 카메라이다.

	SkinnedAnimationObjectsShader* shader = NULL;

	int skinnedMeshNum = 0;
	int standardMeshNum = 0;
	int boundingMeshNum = 0;

	SkinnedMesh** skinnedMeshes = NULL;

	ID3D12Resource** skinnedBuffer = NULL;
	XMFLOAT4X4** mappedSkinnedBuffer = NULL;	//[][][] skinNum, bone

	ID3D12Resource** vbGameObject = NULL;
	VS_VB_INSTANCE_OBJECT** vbMappedGameObject = 0;
	D3D12_VERTEX_BUFFER_VIEW* vbGameObjectView = NULL;
	// 무기와 같은 StandardMesh를 위한 버퍼

	ID3D12Resource** boundingBuffer = NULL;
	VS_VB_INSTANCE_BOUNDING** mappedBoundingBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW* instancingBoundingBufferView;
	// 바운딩 박스를 위한 버퍼

	XMFLOAT4X4** obbWorldCache = NULL;

	ID3D12Resource** positionW = NULL;
	ID3D12Resource** normalW = NULL;
	ID3D12Resource** tangentW = NULL;
	ID3D12Resource** biTangentW = NULL;

	UINT pipeLineStateIdx = 0;
	UINT shadowPipeLineStateIdx = 0;

	TEXTURENAME textureName{};
	int diffuseIdx = -1;
	int normalIdx = -1;
	int specularIdx = -1;
	int metallicIdx = -1;
	int emissionIdx = -1;

	std::vector<void*> objects;

	float elapsedTime = 0;

	int weaponNum = 0;
	int accessoryNum = 0;
	//int weaponNum1 = 5;

	WeaponPoint preWeaponPoint;
	WeaponPoint curWeaponPoint;

	ParticleShader* particleShader;
	WeaponTrailerShader* weaponTrailerShader;

	bool showBounding = false;

	int hObjectSize;

	float hpcool = 0.0f;
	float steminacool = 0.0f;
public:

	bool isHill = false;

	float attackCoolTime = -1.0f;
	float fireCoolTime = -1.0f;
	float runCoolTime = -1.0f;
	float cameraRotate = 0.0f;

	bool isHit = false;
	bool haveModel;
	int playerJob = -1;
	int ammo = 0.f;

	float maxHp = 100.0f;
	float maxStemina = 100.0f;

	XMFLOAT3 cameraright;
	XMFLOAT3 cameraup;
	XMFLOAT3 cameralook;

	XMFLOAT4X4* toArrow = NULL;

	bool	upBodyAniLock = false;
	bool	loBodyAniLock = false;

	Player(int meshesNum = 1);
	virtual ~Player();

	XMFLOAT3 GetPosition() { return(position); }
	XMFLOAT3 GetLookVector() { return(look); }
	XMFLOAT3 GetUpVector() { return(up); }
	XMFLOAT3 GetRightVector() { return(right); }

	void SetLookVector(XMFLOAT3 look) { this->look = look; }
	void SetUpVector(XMFLOAT3 up) { this->up = up; }
	void SetRightVector(XMFLOAT3 right) { this->right = right; }

	void SetFriction(float friction) { this->friction = friction; }
	void SetGravity(const XMFLOAT3& gravity) { this->gravity = gravity; }
	void SetMaxVelocityXZ(float maxVelocity) { this->maxVelocityXZ = maxVelocity; }
	void SetMinVelocityXZ(float minVelocity) { this->minVelocityXZ = minVelocity; }
	void SetMaxVelocityY(float maxVelocity) { this->maxVelocityY = maxVelocity; }
	void SetVelocity(const XMFLOAT3& velocity) { this->velocity = velocity; }
	void SetPosition(const XMFLOAT3& position, bool isSetting = false)
	{
		Move(XMFLOAT3(position.x - this->position.x, position.y - this->position.y, position.z - this->position.z), isSetting, elapsedTime);
	}
	void SetHeight(const float& height) {
		position.y = height;
	}
	
	/*플레이어의 위치를 xmf3Position 위치로 설정한다. xmf3Position 벡터에서 현재 플레이어의 위치 벡터를 빼면 현
	재 플레이어의 위치에서 xmf3Position 방향으로의 벡터가 된다. 현재 플레이어의 위치에서 이 벡터 만큼 이동한다.*/

	XMFLOAT3& GetVelocity() { return(velocity); }
	float GetYaw() { return(yaw); }
	float GetPitch() { return(pitch); }
	float GetRoll() { return(roll); }
	Camera *GetCamera() { return(camera); }
	int   GetDir() { return direction; }
	int	  GetPotal(){ return potal; }
	XMFLOAT3 GetColpos(){ return colPos; }

	void SetDir(int direction) { this->direction = direction; }
	void SetCamera(Camera* camera) { this->camera = camera; }
	void SetPotal(int potal) { this->potal = potal; }

	void Move(DWORD direction, float distance, bool velocity , float elapsedTime);
	void Move(const XMFLOAT3& shift, bool velocity , float elapsedTime);
	void Move(float offsetX = 0.0f, float offsetY = 0.0f, float offsetZ = 0.0f);
	void RealMove();

	//플레이어를 이동하는 함수이다.

	void Rotate(float x, float y, float z);
	//플레이어를 회전하는 함수이다.
	void SetScale(float x, float y, float z) { scale.x = x; scale.y = y; scale.z = z; }

	void Update(float timeElapsed);
	//플레이어의 위치와 회전 정보를 경과 시간에 따라 갱신하는 함수이다.

	virtual void OnPlayerUpdateCallback(float timeElapsed) { }
	void SetPlayerUpdatedContext(LPVOID context) { playerUpdatedContext = context; }
	//플레이어의 위치가 바뀔 때마다 호출되는 함수와 그 함수에서 사용하는 정보를 설정하는 함수이다.

	virtual void OnCameraUpdateCallback(float timeElapsed) { }
	void SetCameraUpdatedContext(LPVOID context) { cameraUpdatedContext = context; }
	//카메라의 위치가 바뀔 때마다 호출되는 함수와 그 함수에서 사용하는 정보를 설정하는 함수이다.

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseShaderVariables();
	virtual void UpdateShaderVariables();
	
	Camera *OnChangeCamera(DWORD newCameraMode, DWORD currentCameraMode);
	virtual Camera *ChangeCamera(DWORD newCameraMode, float timeElapsed) { return(NULL); }
	//카메라를 변경하기 위하여 호출하는 함수이다.

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, bool isShadow);
	//플레이어의 위치와 회전축으로부터 월드 변환 행렬을 생성하는 함수이다.

	virtual void Render(ID3D12GraphicsCommandList* commandList, Camera* camera = NULL, bool isShadow = false);
	//플레이어의 카메라가 3인칭 카메라일 때 플레이어(메쉬)를 렌더링한다.

	virtual void GunRebound();

	virtual void SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);

	virtual void ChangePlayerAnimation(int status);
	virtual void ChangePlayerAnimation(int status1, int status2, int characeter);

	virtual void AnimateObject(float elapsedTime);
	virtual void Animate();

	virtual void PrepareCompute(ID3D12GraphicsCommandList* commandList);

	virtual bool ShowEffect(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float totalTime) { return false; };

	void SetShowBounding(bool showBounding) { this->showBounding = showBounding; }

	WeaponPoint GetPreWeaponPoint() { return preWeaponPoint; }
	WeaponPoint GetCurWeaponPoint() { return curWeaponPoint; }

	virtual void SetModel(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int modelIdx);
	virtual int GetWeaponNum() { return weaponNum; }
	virtual void SetWeaponNum(int newweapon) { weaponNum = newweapon; };
	virtual void SetAccessoryNum(int newaccesory) { accessoryNum = newaccesory; };

	virtual float GetWeaponDamage();
	virtual void SetPlayerHp(float damage);
	virtual void SetPlayerStemina(float changedata);
	virtual float GetPlayerdefencivePower();
	virtual void SetAttackCoolTime(float time);
	virtual void SetPlayerAmmo();
	virtual void PlayerGetHeal(float time);
};

class RiflePlayer : public Player
{
public:
	RiflePlayer(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature, ID3D12RootSignature* computeRootSignature, std::vector<void*> objects, 
		ParticleShader* particleShader, WeaponTrailerShader* weaponTrailerShader, int hObjectSize, int meshesNum = 1);
	virtual ~RiflePlayer();

	virtual Camera *ChangeCamera(DWORD newCameraMode, float timeElapsed);
	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, bool isShadow);
	virtual void OnPlayerUpdateCallback(float timeElapsed);
	virtual void OnCameraUpdateCallback(float timeElapsed);

	virtual bool ShowEffect(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, float totalTime);
};

class TerrainPlayer : public Player
{
public:
	TerrainPlayer(ID3D12Device *device, ID3D12GraphicsCommandList *commandList,
		ID3D12RootSignature *graphicsRootSignature, void *context, int meshesNum = 1);
	virtual ~TerrainPlayer();
	virtual Camera *ChangeCamera(DWORD newCameraMode, float timeElapsed);
	virtual void OnPlayerUpdateCallback(float timeElapsed);
	virtual void OnCameraUpdateCallback(float timeElapsed);
};
