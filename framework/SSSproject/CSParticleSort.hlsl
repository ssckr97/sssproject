#include "ParticleUtility.hlsli"

groupshared ParticleData gShaderedCashe[BLOCKSIZE];

[numthreads(BLOCKSIZE, 1, 1)]
void main(uint3 DTid : SV_DispatchThreadID, uint GI : SV_GroupIndex)
{
    gShaderedCashe[GI] = gsbReadParticleData[DTid.x];
    GroupMemoryBarrierWithGroupSync();
    // Load
  
    for (unsigned int j = giLevel >> 1; j > 0; j >>= 1)
    {
        ParticleData result;
        
        float4 pos1 = float4(gShaderedCashe[GI & ~j].position, 1);
        float4 pos2 = float4(gShaderedCashe[GI | j].position, 1);
      
        float4 depth1 = mul(mul(pos1, gmtxView), gmtxProjection);
        float4 depth2 = mul(mul(pos2, gmtxView), gmtxProjection);
      
        
        if ((depth1.w >= depth2.w) == (bool) (giLevel & DTid.x))
            result = gShaderedCashe[GI ^ j];
        else
            result = gShaderedCashe[GI];
       
        GroupMemoryBarrierWithGroupSync();
        gShaderedCashe[GI] = result;
        // 각 쓰레드의 결과를 캐시에 담는다.
        GroupMemoryBarrierWithGroupSync();
    }
    // Sort
    
    gsbWriteParticleData[DTid.x] = gShaderedCashe[GI];
    // Store
}