#include "Shaders.hlsli"
#include "Fog.hlsli"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Particle Shader Code

struct VS_PARTICLE_INPUT
{
    uint textureIdx : TEXTUREIDX;
    float2 size : SIZE;
    float3 position : POSITION;
    uint id : SV_VERTEXID;
    float4 color : COLOR;
    float alphaDegree : ALPHA;
};

struct GS_PARTICLE_INPUT
{
    float3 position : POSITION;
    float2 size : SIZE;
    uint textureIdx : TEXTUREIDX;
    float age : AGE;
    float4 color : COLOR;
    float alhpaDegree : ALPHA;
    
};

struct GS_PARTICLE_OUTPUT
{
    float4 position : SV_Position;
    float2 uv : TEXCOORD0;
    float2 screenTex : TEXCOORD1;
    float2 depth : TEXCOORD2;
    float age : AGE;
    float4 color : COLOR;
    uint textureIdx : TEXTUREIDX;
    float alphaDegree : ALPHA;
    
    float fogFactor : FACTOR;
 
};

GS_PARTICLE_INPUT VSParticle(VS_PARTICLE_INPUT input)
{
    GS_PARTICLE_INPUT output;
    
    output.position = gsbParticleData[input.id].position.xyz + input.position;
    output.age = gsbParticleData[input.id].age;
    output.size = input.size;
    output.textureIdx = input.textureIdx;
    output.color = input.color;
    output.alhpaDegree = input.alphaDegree;

    return output;
}

[maxvertexcount(4)]
void GSParticleDraw(point GS_PARTICLE_INPUT input[1], inout TriangleStream<GS_PARTICLE_OUTPUT> outStream)
{
    if (input[0].age < 0 || input[0].age > gfParticleAge)
        return;
   
    GS_PARTICLE_OUTPUT output;
    
    float size = input[0].size.x;
    if (gnParticleType == PARTICLE_TYPE_SMALLER)
    {
        size -= (1 - (input[0].age) / gfParticleAge) * input[0].size.x;
    }
    
    [unroll]
    for (int j = 0; j < 4; ++j)
    {
        float3 position = gf3BillboardPos[j] * size;
        position = mul(position, (float3x3) gmtxInvView) + input[0].position;
        float4 cameraPos = mul(float4(position, 1.0f), gmtxView);
        output.position = mul(cameraPos, gmtxProjection);
        output.uv = gf2BillboardUVs[j];
        output.age = input[0].age;
        output.color = input[0].color;
        output.screenTex = output.position.xy / output.position.w;
        output.depth = output.position.zw;
        output.textureIdx = input[0].textureIdx;
        output.alphaDegree = input[0].alhpaDegree;
        output.fogFactor = GetFogFactor(cameraPos.z);
        
        outStream.Append(output);
    }
    outStream.RestartStrip();
}

float4 PSParticle(GS_PARTICLE_OUTPUT input) : SV_TARGET
{
    float2 screenTex = ((input.screenTex) + float2(1.0f, 1.0f)) * 0.5f;
    screenTex.y = 1 - screenTex.y;
    
    float depthSample = gtxtDepthTexture[gnSwapchainIdx].Sample(gssDepthClamp, screenTex);
    // depth버퍼의 depth값
    float4 particleSample = gtxtParticleTexture[input.textureIdx].Sample(gssWrap, input.uv);

    float particleDepth = input.depth.x;
    particleDepth /= input.depth.y;
    // 파티클 입자의 depth값
    
    float depthFade = 0.0f;

    float4 depthViewSample = mul(float4(input.screenTex, depthSample, 1), gmtxInvProjection);
    float4 depthViewParticle = mul(float4(input.screenTex, particleDepth, 1), gmtxInvProjection);
    
    float depthDiff = (depthViewSample.z / depthViewSample.w) - (depthViewParticle.z / depthViewParticle.w);
    if (depthDiff < 0)
        discard;
    
    depthFade = saturate(depthDiff / 10.0f);
    
    float4 light = float4(0.992f, 1.0f, 0.880f, 1.0f) + float4(0.525f, 0.474f, 0.474f, 1.0f);
    particleSample.rgb *= light.xyz * input.color.rgb;

    float4 color = float4(particleSample.rgb, 1.0f);
    color = BlendFog(color, input.fogFactor);
    
    float age;
    if (gnParticleType == PARTICLE_TYPE_DISAPPEAR)
    {
        age = input.age / gfParticleAge;
        age = saturate(age);
    }
    else
    {
        age = 0.0f;
    }
    
   float4 finalColor = float4(color.rgb, particleSample.a);
    finalColor.a *= (age + input.alphaDegree);
    finalColor.a *= depthFade;
    
    return finalColor;
}
