#pragma once
#include"stdafx.h"
constexpr int MAX_ID_LEN = 50;
constexpr int MAX_STR_LEN = 255;

#define WORLD_WIDTH		514
#define WORLD_HEIGHT	514
#define SECTOR_SIZE		64
#define SECTOR_WIDTH    (WORLD_WIDTH / SECTOR_SIZE) + 1
#define SERVER_PORT		9000

#define OBJSECTOR_SIZE		16
#define OBJSECTOR_WIDTH    (WORLD_WIDTH / OBJSECTOR_SIZE) + 1
#define C2S_LOGIN			1
#define C2S_CHANGE_JOB		2
#define C2S_MOVE			3
#define C2S_LOADMAP			4
#define C2S_WEAPON			5
#define C2S_ATTACK			6
#define C2S_ZOMBIEATTACK	7
#define C2S_HILL			8

#define S2C_LOGIN_OK		1
#define S2C_ENTER_LOBBY		2
#define S2C_CHANGE_JOB		3
#define S2C_ENTER_INGAME	4
#define S2C_MOVE			5
#define S2C_WEAPON			6
#define S2C_LEAVE			7
#define S2C_ZOMBIE			8
#define S2C_MAP				9
#define S2C_EFFECT			10
#define S2C_ENDGAME			11

enum EffectType {
	EffectTypeDust = 0,
	EffectTypeBlood
};

#pragma pack(push ,1)

struct sc_packet_ingame_enter {
	char size;
	char type;
	short id;
	char cJob;
	short hp;
	XMFLOAT3 look;
	XMFLOAT3 pos;
};

struct sc_packet_move {
	char size;
	char type;
	short id;
	XMFLOAT3 pos;
	XMFLOAT3 look;
	XMFLOAT3 velocity;
	char  direction;
	bool jump;
	short hp;
	char aniTrackUp;
	char aniTrackLo;
};

struct sc_packet_leave {
	char size;
	char type;
	int id;
};
struct sc_packet_zombie {
	char size;
	char type;
	int id;
	short oidx;
	char bidx;
	char state;
	short hp;
	XMFLOAT3 look;
	XMFLOAT3 pos;
	XMFLOAT3 velocity;
	char aniTrack;
};

struct sc_packet_lobby {
	char size;
	char type;
	short id;
	char cJob;
	char isReady;
};

struct sc_packet_loadmap {
	char size;
	char type;
	short id;
	char mapNum;
};
struct sc_packet_weapon {
	char size;
	char type;
	short id;
	char weaponNum;
};

struct sc_packet_effect {
	char size;
	char type;
	char effectType;
	XMFLOAT3 pos;

};
struct sc_packet_endgame {
	char size;
	char type;
};
constexpr unsigned char D_UP = 0;
constexpr unsigned char D_DOWN = 1;
constexpr unsigned char D_LEFT = 2;
constexpr unsigned char D_RIGHT = 3;
constexpr unsigned char D_UPRIGHT = 4;
constexpr unsigned char D_UPLEFT = 5;
constexpr unsigned char D_DOWNRIGHT = 6;
constexpr unsigned char D_DOWNLEFT = 7;

struct cs_packet_move {
	char	size;
	char	type;
	char	direction;
	char	aniTrack;
	char	potal;
	char	aniTrackUp;
	char	aniTrackLo;
	XMFLOAT3 pos;
	XMFLOAT3 velocity;
	XMFLOAT3 look;
};

struct cs_packet_login {
	char	size;
	char	type;
};

struct cs_packet_loadmap {
	char	size;
	char	type;
	char    mapNum;
};
struct cs_packet_attack {
	char size;
	char type;
	int  id;
	float damage;
	XMFLOAT3 pos;
};
struct cs_packet_zombieattack {
	char size;
	char type;
	float damage;
};
struct cs_packet_lobby {
	char size;
	char type;
	char cJob;
	char isReady;
};

struct cs_packet_weapon {
	char size;
	char type;
	char weaponNum;
};

struct cs_packet_hill {
	char size;
	char type;
	short hp;
};

#pragma pack (pop)