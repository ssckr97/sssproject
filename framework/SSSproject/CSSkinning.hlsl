#include "Skinning.hlsli"

[numthreads(BLOCK_SIZE, 1, 1)]
void main(uint3 Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID)
{
    if (DTid.x < gnVerticesNum)
    {
        float4x4 mtxVertexToBoneWorld = (float4x4) 0.0f;
        for (int i = 0; i < MAX_VERTEX_INFLUENCES; ++i)
        {
            mtxVertexToBoneWorld += gsbBoneWeight[DTid.x][i] * mul(gpmtxBoneOffsets[gsbBoneIndex[DTid.x][i]], gsbBoneTransforms[(Gid.y * SKINNED_ANIMATION_BONES) + gsbBoneIndex[DTid.x][i]]);
        }
        float3 positionW = mul(float4(gsbPosition[DTid.x], 1.0f), mtxVertexToBoneWorld).xyz;
        float3 normalW = mul(gsbNormal[DTid.x], (float3x3) mtxVertexToBoneWorld).xyz;
        float3 tangentW = mul(gsbTangent[DTid.x], (float3x3) mtxVertexToBoneWorld).xyz;
        float3 bitangentW = mul(gsbBiTangent[DTid.x], (float3x3) mtxVertexToBoneWorld).xyz;
    
        grwsbPositionW[Gid.y * gnVerticesNum + DTid.x] = positionW;
        grwsbNormalW[Gid.y * gnVerticesNum + DTid.x] = normalW;
        grwsbTangentW[Gid.y * gnVerticesNum + DTid.x] = tangentW;
        grwsbBiTangentW[Gid.y * gnVerticesNum + DTid.x] = bitangentW;
    }
    return;
}