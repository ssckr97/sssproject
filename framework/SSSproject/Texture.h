#pragma once

struct SRVROOTARGUMENTINFO
{
	UINT							rootParameterIndex;
	D3D12_GPU_DESCRIPTOR_HANDLE		srvGpuDescriptorHandle;

	SRVROOTARGUMENTINFO() { rootParameterIndex = 0; }
	SRVROOTARGUMENTINFO(UINT idx, D3D12_GPU_DESCRIPTOR_HANDLE handle)
	{
		rootParameterIndex = idx;
		srvGpuDescriptorHandle = handle;
	}
};


class Texture
{
public:
	Texture(int textureResources = 1, UINT resourceType = ResourceTexture2D, int samplers = 0);
	virtual ~Texture();


private:
	int								references = 0;

	UINT							textureType = ResourceTexture2D;
	int								texturesNum = 0;
	ID3D12Resource					**textures = NULL;
	ID3D12Resource					**textureUploadBuffers;

	SRVROOTARGUMENTINFO				*rootArgumentInfos = NULL;

	int								samplers = 0;
	D3D12_GPU_DESCRIPTOR_HANDLE		*samplerGpuDescriptorHandles = NULL;

public:
	void AddRef() { references++; }
	void Release() { if (--references <= 0) delete this; }

	void SetRootArgument(int nIndex, UINT nRootParameterIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dsrvGpuDescriptorHandle);
	void SetSampler(int nIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dSamplerGpuDescriptorHandle);

	void UpdateShaderVariables(ID3D12GraphicsCommandList *commandList);
	void UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, int nIndex);
	void ReleaseShaderVariables();

	void LoadTextureFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, wchar_t *pszFileName, UINT nIndex, bool isDDSFile = true);

	void SetTexture(ID3D12Resource * resource, int idx);

	int GetTextures() { return(texturesNum); }
	ID3D12Resource *GetTexture(int nIndex) { return(textures[nIndex]); }
	UINT GetTextureType() { return(textureType); }

	void ReleaseUploadBuffers();
};
