#include "ProcessFile.h"

SaveFile::SaveFile()
{
}

void SaveFile::SetInstanceBuffer(std::vector<InstanceBuffer*> instanceBuffer)
{
	for (int i = 0; i < instanceBuffer.size(); ++i)
	{
		instanceBufferSize = instanceBuffer.size();
		saveInstanceBuffer[i].objectNumber = instanceBuffer[i]->objects.size();
		for (int j = 0; j < saveInstanceBuffer[i].objectNumber; ++j) {
			saveInstanceBuffer[i].objectWorld.emplace_back(instanceBuffer[i]->objects[j]->GetWorld());
		}

	}
}

void SaveFile::SetSkinedInstanceBuffer(std::vector<SkinnedInstanceBuffer*> instanceBuffer)
{
	for (int i = 0; i < instanceBuffer.size(); ++i)
	{
		instanceBufferSize = instanceBuffer.size();
		saveInstanceBuffer[i].objectNumber = instanceBuffer[i]->objects.size();
		for (int j = 0; j < saveInstanceBuffer[i].objectNumber; ++j) {
			saveInstanceBuffer[i].objectWorld.emplace_back(instanceBuffer[i]->objects[j]->GetWorld());
		}

	}
}




void ProcessFile::LoadMap(std::string filePath, ID3D12Device * device, ID3D12GraphicsCommandList * commandList)
{
	SaveFile file;


	std::vector<Shader*> shaders = scene->Getshaders();

	InstancingShader* iShader = static_cast<InstancingShader*>(shaders[1]);
	SkinnedInstancingShader* skiShader = static_cast<SkinnedInstancingShader*>(shaders[2]);
	TerrainShader* terrainShader = reinterpret_cast<TerrainShader*>(scene->Getshaders()[0]);

	std::vector<InstanceBuffer*>	    insBuffer = iShader->GetBuffer();
	std::vector<SkinnedInstanceBuffer*> skinsBuffer = skiShader->GetBuffer();
	HeightMapTerrain* terrain = scene->GetTerrain();
	HeightMapGridMesh* mesh = (HeightMapGridMesh*)terrain->GetMesh(0);

	std::string mapInfrom;
	std::string alphatexture0;
	std::string alphatexture1;
	std::string alphatexture2;

	std::string heightMaptexture;
	std::string NormalTexture;


	mapInfrom = filePath + ".txt";
	alphatexture0 = filePath + "_alphaTexture0.dds";
	alphatexture1 = filePath + "_alphaTexture1.dds";
	alphatexture2 = filePath + "_alphaTexture2.dds";

	alphatexture0 = filePath + "_alphaTexture0_hightTexture.PNG";


	std::ifstream in(mapInfrom, std::ios_base::in | std::ios::binary);
	if (in.is_open() == false) {
		std::cout << "파일 열기 실패 " << std::endl;
	}
	int bufferSize;
	in.read((char*)&bufferSize, 4);

	file.GetSaveInstanceBuffer().resize(bufferSize);
	file.SetInstanceBufferSize(bufferSize);


	for (int i = 0; i < bufferSize; ++i) {
		int objectNumber;
		in.read((char*)&objectNumber, 4);
		file.GetSaveInstanceBuffer()[i].objectNumber = objectNumber;
		file.GetSaveInstanceBuffer()[i].objectWorld.resize(objectNumber);
		in.read((char*)&file.GetSaveInstanceBuffer()[i].objectWorld[0], objectNumber * sizeof(XMFLOAT4X4));
		//오브젝트 정보 로드 
	}
	iShader->DeleteAllObjcet();
	terrainShader->DeleteAllBounding(terrainShader->GetQuadRoot());

	XMFLOAT4X4 world = Matrix4x4::Identity();

	for (int i = 0; i < bufferSize; ++i) {
		int objectNumber = file.GetSaveInstanceBuffer()[i].objectNumber;
		for (int j = 1; /*except dummy*/ j < objectNumber; ++j) {
			iShader->LoadGameObject(device, commandList,
				file.GetSaveInstanceBuffer()[i].objectWorld[j], i);
		}
	}

	bufferSize;
	in.read((char*)&bufferSize, 4);

	file.GetSaveInstanceBuffer().clear();
	file.GetSaveInstanceBuffer().resize(bufferSize);
	file.SetInstanceBufferSize(bufferSize);


	for (int i = 0; i < bufferSize; ++i) {
		int objectNumber;
		in.read((char*)&objectNumber, 4);
		file.GetSaveInstanceBuffer()[i].objectNumber = objectNumber;
		file.GetSaveInstanceBuffer()[i].objectWorld.resize(objectNumber);
		in.read((char*)&file.GetSaveInstanceBuffer()[i].objectWorld[0], objectNumber * sizeof(XMFLOAT4X4));
		//캐릭터 
	}

	skiShader->DeleteAllObjcet();
	for (int i = 0; i < bufferSize; ++i) {
		int objectNumber = file.GetSaveInstanceBuffer()[i].objectNumber;
		for (int j = 1; /*except dummy*/ j < objectNumber; ++j) {
			skiShader->LoadGameObject(device, commandList,
				file.GetSaveInstanceBuffer()[i].objectWorld[j], scene->GetGraphicsRootSignature(), i);
		}
	}
	auto sbuffer = skiShader->GetBuffer();

	int jCount = 0;
	for (int i = ZOMBIE_MODEL_START; i < sbuffer.size(); ++i) {
		for (int j = 1; j < sbuffer[i]->objects.size(); ++j) {
			sbuffer[i]->objects[j]->Setid(ZOMBIE_START_NUMBER + jCount);
			++jCount;
		}
		
	}

	LIGHTS* light = scene->GetLights();
	in.read((char*)&light->lights[0], sizeof(LIGHT) * MAX_LIGHTS);


	ParticleShader* pShader = static_cast<ParticleShader*>(shaders[4]);
	pShader->ClearParticle();
	int pSize;
	int blobSize;

	in.read((char*)&pSize, 4);
	in.read((char*)&blobSize, 4);

	std::vector<ParticleInfoInCPU> tempParticle;
	std::vector<ParticleBlobModify> tempblob;
	tempParticle.resize(pSize);
	tempblob.resize(blobSize);

	if (pSize > 0) in.read((char*)&tempParticle[0], pSize * sizeof(ParticleInfoInCPU));
	if (blobSize > 0) in.read((char*)&tempblob[0], blobSize * sizeof(ParticleBlobModify));

	for (int i = 0; i < blobSize; ++i) {
		std::string buf = std::to_string(i);
		pShader->CreateCopyBuffer(device, commandList, tempblob[i], buf);
	}

	for (int i = 0; i < pSize; ++i) {
		tempParticle[i].copyIdx += pShader->remainCopyBuffers;
		pShader->CreateParticle(device, commandList, tempParticle[i]);
	}
	if (pSize >= 4) {
		for (int i = pSize - 4; i < pSize; ++i)
			pShader->GetIsRender()[i] = false;
	}

	int vertexNum = mesh->GetVertexNum();
	file.GetSaveDiffuseVertex().resize(vertexNum);
	for (int i = 0; i < 1; ++i) {
		HeightMapGridMesh* tempMesh = (HeightMapGridMesh*)terrain->GetMesh(i);

		if (in.read((char*)&file.GetSaveDiffuseVertex()[0], vertexNum * sizeof(SaveDiffuseVertex))) {
			for (int j = 0; j < vertexNum; ++j) {
				tempMesh->GetDiffuseTexturedVertex()[j].position = file.GetSaveDiffuseVertex()[j].postion;
				tempMesh->GetDiffuseTexturedVertex()[j].diffuse = file.GetSaveDiffuseVertex()[j].diffuse;
			}
		}
		else {
			std::cout << "Error code: " << strerror(errno);
		}
		tempMesh->calALLNormal();
	}
	// vertexBuffer 삭제
	if (mesh->GetVertexBuffer())
	{
		ID3D12Resource* temp = mesh->GetVertexBuffer();
		mesh->SetVertexBuffer(terrainShader->releaseBuffer);
		terrainShader->releaseBuffer = temp;
	}

	mesh->ResetVertexBuffer(device, commandList, mesh->GetDiffuseTexturedVertex());
	HeightMapImage* tImage = terrain->GetImage();
	for (int i = 0; i < insBuffer.size(); ++i)
	{
		auto objects = insBuffer[i]->objects;
		for (int j = 1; j < objects.size(); ++j)
		{
			auto obbs = objects[j]->GetobbVector();
			for (int k = 0; k < obbs.size(); ++k)
			{
				BoundingOrientedBox temp;
				obbs[k].Transform(temp, XMLoadFloat4x4(&Matrix4x4::Inverse(terrain->GetWorld())));
				terrainShader->SetQuadTreeObject(terrainShader->GetQuadRoot(), temp);
			}
		}
	}
	in.close();
}
