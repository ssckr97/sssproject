#include "Shaders.hlsli"
#include "Light.hlsli"
#include "Shadow.hlsli"
#include "Fog.hlsli"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Terrain Detail Textured Shader

struct VS_TERRAIN_INPUT
{
    float3 position : POSITION;
    float4 color : COLOR;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    float3 normal : NORMAL;
};

struct VS_TERRAIN_OUTPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    float3 normal : NORMAL;

    float4 shadowPosition[3] : TEXCOORD2;
    float4 positionW : POSITION;
    
    float fogFactor : FACTOR;
};

VS_TERRAIN_OUTPUT VSTerrain(VS_TERRAIN_INPUT input)
{
    VS_TERRAIN_OUTPUT output;
    output.positionW = mul(float4(input.position, 1.0f), gmtxWorld);
    float4 cameraPos = mul(output.positionW, gmtxView);
    output.position = mul(cameraPos, gmtxProjection);
    
    output.color = input.color;
    output.uv0 = input.uv0;
    output.uv1 = input.uv1;
    output.normal = input.normal;
    
    matrix shadowProject[MAX_CASCADE_SIZE];
    
    for (int i = 0; i < int(gfCasacdeNum); ++i)
    {
        shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture);
        output.shadowPosition[i] = mul(output.positionW, shadowProject[i]);
    }
    
    output.fogFactor = GetFogFactor(cameraPos.z);
    
    return (output);
}

float4 PSTerrain(VS_TERRAIN_OUTPUT input) : SV_TARGET
{
    float fShadowFactor = 1.0f;
    fShadowFactor = GetShadowFactor(input.shadowPosition, input.positionW);
       
    //float4 cDetailTexColor = gtxtTerrainDetailTexture[0].Sample(gssAnisotropic, input.uv1);
    float4 alphaColor0 = gtxtTerrainAlphaTexture[0].Sample(gssAnisotropic, input.uv0);
    float4 alphaColor1 = gtxtTerrainAlphaTexture[1].Sample(gssAnisotropic, input.uv0);
    float4 alphaColor2 = gtxtTerrainAlphaTexture[2].Sample(gssAnisotropic, input.uv0);
    
    float alphaNumber[12];
    alphaNumber[0] = alphaColor0.x;
    alphaNumber[1] = alphaColor0.y;
    alphaNumber[2] = alphaColor0.z;
    alphaNumber[3] = alphaColor0.a;
    alphaNumber[4] = alphaColor1.x;
    alphaNumber[5] = alphaColor1.y;
    alphaNumber[6] = alphaColor1.z;
    alphaNumber[7] = alphaColor1.a;
    alphaNumber[8] = alphaColor2.x;
    alphaNumber[9] = alphaColor2.y;
    alphaNumber[10] = alphaColor2.z;
    alphaNumber[11] = alphaColor2.a;
    
    
    int maxIndex = 0;
    int secondMaxIndex = 0;
    float maxValue = 0;
    for (int k = 0; k < 12; ++k)
    {
        if (maxValue < alphaNumber[k])
        {
            maxValue = alphaNumber[k];
            maxIndex = k;
        }
    }
    maxValue = 0;
    for (int j = 0; j < 12; ++j)
    {
        if (j != maxIndex && maxValue < alphaNumber[j])
        {
            maxValue = alphaNumber[j];
            secondMaxIndex = j;
        }
    }

    float4 tnormal0 = gtxtTerrainNormalTexture[maxIndex].Sample(gssAnisotropic, input.uv1);
    float4 tnormal1 = gtxtTerrainNormalTexture[secondMaxIndex].Sample(gssAnisotropic, input.uv1);
    float4 tTex0 = gtxtTerrainDetailTexture[maxIndex].Sample(gssAnisotropic, input.uv1);
    float4 tTex1 = gtxtTerrainDetailTexture[secondMaxIndex].Sample(gssAnisotropic, input.uv1);

    float3 dPdx = ddx(input.positionW);
    float3 dPdy = ddy(input.positionW);
    float2 dUVdx = ddx(input.uv0);
    float2 dUVdy = ddy(input.uv0);
    float3 tangent = normalize((dUVdx.y * dPdy - dUVdy.y * dPdx));
    float3 bitangent = cross(tangent, input.normal);
    float3x3 TBN = float3x3((tangent), (bitangent), (input.normal));
    
    float lerpValue = 0.0f;
    if (secondMaxIndex == maxIndex)
    {
        lerpValue = 0.0f;
        tTex1 = float4(0.0f, 0.0f, 0.0f, 0.0f);
    }
    else
        lerpValue = alphaNumber[secondMaxIndex];
    float3 finalNormal = lerp(tnormal0, tnormal1, lerpValue);
    finalNormal = (finalNormal.rgb * 2.0f - 1.0f); //[0, 1] �� [-1, 1]
    finalNormal = normalize(mul(finalNormal, TBN));
    float3 posW = input.positionW.xyz;
    
    float cdist = length(gvCameraPosition - posW);
    if(cdist > 20.0f)
        cdist = 20.0f;
    
    float4 finalColor = lerp(tTex0, tTex1, lerpValue);
   
    float4 cIllumination;
    float4 bumpColor;
    float4 normalColor;
    
    cdist = cdist * 0.05;
   
    bumpColor = Lighting(input.positionW.xyz, finalNormal, 1);

    normalColor = Lighting(input.positionW.xyz, input.normal.xyz, 1);
    
    cIllumination = (bumpColor * (1.0f - cdist)) + (normalColor * cdist);
    
    
    //cIllumination = Lighting(input.positionW.xyz, finalNormal, 1);
    //cIllumination += float4(0.2f, 0.2f, 0.2f, 1.0f);
    //return input.color;
    //return float4(normal, 1.0f);
    
    finalColor = saturate(finalColor * cIllumination);
    
    
    float4 cascadeColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    //if (idx == 0)
    //    cascadeColor = float4(0.5f, 0.0f, 0.0f, 0.0f);
    //else if (idx == 1)
    //    cascadeColor = float4(0.0f, 0.5f, 0.0f, 0.0f);
    //else if (idx == 2)
    //    cascadeColor = float4(0.0f, 0.0f, 0.5f, 0.0f);
    //else if (idx == 3)
    //    cascadeColor = float4(0.5f, 0.5f, 0.0f, 0.0f);
    
    finalColor = finalColor * fShadowFactor + cascadeColor;
    
    finalColor = BlendFog(finalColor, input.fogFactor);
    
    return (finalColor);
}