#include "stdafx.h"
#include "ObjectShader.h"
#include "Scene.h"
#include "Material.h"

QuadTree::QuadTree(int startidx , int width, int length , HeightMapGridMesh* mesh)//width는 항상 0부터 시작해서의 거리다.
{
	int terrainWidth = 257;
	vStartIndex = startidx;
	this->width = width;
	this->length = length;
	vertexNum = 0;
	
	XMFLOAT3 sPos = mesh->GetDiffuseTexturedVertex()[vStartIndex].position;
	XMFLOAT3 scale = mesh->GetScale();

	AABBbOX.Center.x = sPos.x + width;
	AABBbOX.Center.y = 0;
	AABBbOX.Center.z = sPos.z + length;

	AABBbOX.Extents = XMFLOAT3( (width) , 300, (length)) ;

	for (int z = 0; z <= length; ++z) {
		for (int x = 0; x <= width; ++x) {
			vertexNum++;
		}
	}
	primitiveNum = vertexNum - 2;


	parent = NULL;
	for (int i = 0; i < 4; ++i) {
		child[i] = NULL;
	}

}

TerrainShader::TerrainShader()
{
}

TerrainShader::~TerrainShader()
{
}
void TerrainShader::BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature *graphicsRootSignature, void* context)
{

	objectsNum = 1;
	objects.reserve(objectsNum);

	UINT cbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255);

	CreateShaderVariables(device, commandList);

	D3D12_GPU_DESCRIPTOR_HANDLE handle = Scene::CreateConstantBufferViews(device, commandList, objectsNum, cbGameObjects, cbElementBytes);

	XMFLOAT3 scale(2.0f, 2.0f, 2.0f);
	XMFLOAT4 color(0.0f, 0.5f, 0.0f, 0.0f);
#ifdef _WITH_TERRAIN_PARTITION
	HeightMapTerrain* terrain = new CHeightMapTerrain(device, commandList, m_pd3dGraphicsRootSignature, _T("Assets/Image/Terrain/HeightMap.raw"), 257, 257, 17, 17, xmf3Scale, xmf4Color);
#else
	HeightMapTerrain* terrain = new HeightMapTerrain(device, commandList, _T("Assets/Image/Terrain/HeightMap.raw"), 257, 257, 257, 257, scale, color);
#endif
	this->terrain = terrain;

	for (int i = 0; i < objectsNum; ++i)
	{
		objects.emplace_back(terrain);
		objects[i]->CreateCbvGPUDescriptorHandles();
		objects[i]->SetCbvGPUDescriptorHandle(handle);
	}
	Root = new QuadTree(0, 257, 257 ,static_cast<HeightMapGridMesh*>(terrain->GetMesh(0)));
	CreateQuadTree(Root);

}
void TerrainShader::CreateQuadTree(QuadTree *Node) //재귀함수를 통해 쿼드 트리를 만든다.
{
	//0.지형을 가져온다. 
	//1. 프리미티브의 개수를 센다 
	
	//4개로 1000개보다 많다면 4개로 나눈다. 왼쪽 아래 , 오른쪽 아래 , 윈쪽 위 , 오른쪽 위 

	int terrainWidth = 257;
	if (Node->primitiveNum > 100) {
		
		Node->child[0] = new QuadTree(Node->vStartIndex , Node->width / 2, Node->length / 2 ,static_cast<HeightMapGridMesh*>(terrain->GetMesh(0)));//왼쪽 아래
		Node->child[1] = new QuadTree(Node->vStartIndex + ((Node->width) / 2), (Node->width) / 2, (Node->length) / 2 , static_cast<HeightMapGridMesh*>(terrain->GetMesh(0)));//오른쪽 아래 
		Node->child[2] = new QuadTree(Node->vStartIndex + ((terrainWidth)* (Node->length / 2)), Node->width / 2, Node->length / 2, static_cast<HeightMapGridMesh*>(terrain->GetMesh(0)));//왼쪽 위
		Node->child[3] = new QuadTree(Node->vStartIndex + ((terrainWidth) * (Node->length / 2)) + ((Node->width) / 2), (Node->width) / 2, (Node->length) / 2,  static_cast<HeightMapGridMesh*>(terrain->GetMesh(0)));//오른쪽 위 
	

		for (int i = 0; i < 4; ++i) {
			Node->child[i]->parent = Node;
		}

		for (int i = 0; i < 4; ++i) {
			CreateQuadTree(Node->child[i]);
		}
	}
	else {
		
		return;
	}
}

void TerrainShader::SetQuadTreeObject(QuadTree* Node, BoundingOrientedBox obb)
{
	for (int i = 0; i < 4; ++i)
	{
		// 가장 하위 자식이 아니라면
		if (Node->child[i] != NULL)
		{
			// 충돌검사에 성공했을 경우 가장 하위자식 노드에 도달할 때까지 검사해준다.
			if (Node->child[i]->AABBbOX.Intersects(obb) == true)
			{
				SetQuadTreeObject(Node->child[i], obb);
			}
		}
		// 가장 하위 자식이라면 emplace_back()
		else
		{
			Node->obbs.emplace_back(obb);
			break;
		}
	}

}

void TerrainShader::DeleteAllBounding(QuadTree* Node)
{
	for (int i = 0; i < 4; ++i)
	{
		if (Node->child[i] != NULL)
		{
			DeleteAllBounding(Node->child[i]);
		}
		else
		{
			Node->obbs.clear();
			break;
		}
	}
}


D3D12_INPUT_LAYOUT_DESC TerrainShader::CreateInputLayout()
{
	UINT nInputElementDescs = 5;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];

	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[3] = { "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[4] = { "NORMAL" , 0 , DXGI_FORMAT_R32G32B32_FLOAT , 0 , D3D12_APPEND_ALIGNED_ELEMENT , D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA , 0 };

	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE TerrainShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Terrain.hlsl", "VSTerrain", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateShadowVertexShader()
{
	return(Shader::CompileShaderFromFile(L"ShadowCaster.hlsl", "VSCSMTerrain", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"Terrain.hlsl", "PSTerrain", "ps_5_1", &pixelShaderBlob));
}

void TerrainShader::CreateShader(ID3D12Device *device, ID3D12RootSignature *graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	shadowPipelineStateNum = 1;
	shadowPipelineStates = new ID3D12PipelineState*[shadowPipelineStateNum];

	Shader::CreateShader(device, graphicsRootSignature);
	Shader::CreateShadowShader(device, graphicsRootSignature);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}
void TerrainShader::CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList)
{
	UINT cbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
	cbGameObjects = ::CreateBufferResource(device, commandList, NULL, cbGameObjectBytes * objectsNum, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	cbGameObjects->Map(0, NULL, (void **)&cbMappedGameObjects);
}

void TerrainShader::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList)
{
	UINT cbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
	XMFLOAT4X4 world;
	for (int j = 0; j < objects.size(); j++)
	{
		XMStoreFloat4x4(&world, XMMatrixTranspose(XMLoadFloat4x4(&objects[j]->GetWorld())));
		CB_GAMEOBJECT_INFO *mappedcbGameObject = (CB_GAMEOBJECT_INFO*)(cbMappedGameObjects + (j * cbGameObjectBytes));
		::memcpy(&mappedcbGameObject->world, &world, sizeof(XMFLOAT4X4));
	}
}

void TerrainShader::ReleaseShaderVariables()
{
	if (cbGameObjects) {
		cbGameObjects->Unmap(0, NULL);
		cbGameObjects->Release();
	}
}

void TerrainShader::ReleaseObjects()
{
	for (int i = 0; i < objects.size(); ++i)
	{
		objects[i]->Release();
	}
}

void TerrainShader::ReleaseUploadBuffers()
{
	for (int i = 0; i < objects.size(); ++i)
	{
		objects[i]->ReleaseUploadBuffers();
	}
}

void TerrainShader::Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx)
{
	if (!isShadow) {
		Shader::OnPrepareRender(commandList, 0, isShadow);
		Shader::UpdateShaderVariables(commandList);
		UpdateShaderVariables(commandList);

		for (int i = 0; i < 1; ++i)
		{
			objects[0]->Render(commandList);
		}
	}
}

void TerrainShader::GarbageCollection()
{
	if (releaseBuffer) releaseBuffer->Release();
	releaseBuffer = NULL;
}


void SkyBoxShader::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	objectBuffer->cbGameObjects = new ID3D12Resource*[1];
	objectBuffer->cbMappedGameObjects = new CB_GAMEOBJECT_INFO*[1];
	objectBuffer->cbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수

	objectBuffer->cbGameObjects[0] = ::CreateBufferResource(device, commandList, NULL, objectBuffer->cbGameObjectBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	objectBuffer->cbGameObjects[0]->Map(0, NULL, (void **)&objectBuffer->cbMappedGameObjects[0]);
}

void SkyBoxShader::ReleaseObjects()
{
	if (objectBuffer->objects[0])
		objectBuffer->objects[0]->ReleaseShaderVariables();
}

void SkyBoxShader::ReleaseUploadBuffer()
{
	if (objectBuffer->objects[0])
		objectBuffer->objects[0]->ReleaseUploadBuffers();
}

void SkyBoxShader::ReleaseShaderVariables()
{
	if (objectBuffer->cbGameObjects[0]) {
		objectBuffer->cbGameObjects[0]->Unmap(0, NULL);
		objectBuffer->cbGameObjects[0]->Release();
	}
}

void SkyBoxShader::AnimateObjects(float timeElapsed, Camera * camera)
{
	camPos = camera->GetPosition();
}

void SkyBoxShader::Animate()
{
	objectBuffer->objects[0]->SetPosition(camPos);
	objectBuffer->objects[0]->UpdateTransform(objectBuffer->cbMappedGameObjects, objectBuffer->cbGameObjectBytes, 0, NULL);
}

void SkyBoxShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, void * context)
{
	objectBuffer = new ObjectBuffer;

	objectBuffer->objectsNum = 1;
	objectBuffer->objects.reserve(objectBuffer->objectsNum);

	objectBuffer->framesNum = 1;

	CreateShaderVariables(device, commandList);
	D3D12_GPU_DESCRIPTOR_HANDLE handle = Scene::CreateConstantBufferViews(device, commandList, objectBuffer->objectsNum, objectBuffer->cbGameObjects[0], objectBuffer->cbGameObjectBytes);

	GameObject *skyBoxObject = NULL;
	skyBoxObject = new GameObject();
	skyBoxObject->SetPosition(0, 0, 0);
	skyBoxObject->CreateCbvGPUDescriptorHandles();
	skyBoxObject->SetCbvGPUDescriptorHandlePtr(handle.ptr);

	SkyBoxMesh *skyboxMesh = new SkyBoxMesh(device, commandList, 20.0f, 20.0f, 20.0f);

	skyBoxObject->SetMesh(0, skyboxMesh);

	objectBuffer->objects.emplace_back(skyBoxObject);
}



D3D12_INPUT_LAYOUT_DESC SkyBoxShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE SkyBoxShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"SkyBox.hlsl", "VSSkyBoxed", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE SkyBoxShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"SkyBox.hlsl", "PSSkyBoxed", "ps_5_1", &pixelShaderBlob));
}

D3D12_RASTERIZER_DESC SkyBoxShader::CreateRasterizerState()
{
	//FIllMode와 CullMode를 변경하여 그려지는 객체의 내부를 칠할지 말지 등을 결정할 수 있다.
	D3D12_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_FRONT;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(rasterizerDesc);
}

D3D12_DEPTH_STENCIL_DESC SkyBoxShader::CreateDepthStencilState()
{
	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = FALSE;
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return(depthStencilDesc);
}

D3D12_BLEND_DESC SkyBoxShader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = FALSE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = FALSE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(blendDesc);
}

void SkyBoxShader::Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx)
{
	if (!isShadow) {
		Shader::OnPrepareRender(commandList, 0, isShadow);
		Shader::UpdateShaderVariables(commandList);
		UpdateShaderVariables(commandList);

		objectBuffer->objects[0]->Render(commandList);
	}
}

SkinnedAnimationObjectsShader::SkinnedAnimationObjectsShader()
{
}

SkinnedAnimationObjectsShader::~SkinnedAnimationObjectsShader()
{
}

D3D12_BLEND_DESC SkinnedAnimationObjectsShader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = TRUE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(blendDesc);
}

void SkinnedAnimationObjectsShader::CreateShader(ID3D12Device *device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 2;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	shadowPipelineStateNum = 1;
	shadowPipelineStates = new ID3D12PipelineState*[shadowPipelineStateNum];

	computePipelineStatesNum = 1;
	computePipelineStates = new ID3D12PipelineState*[computePipelineStatesNum];

	Shader::CreateShader(device, graphicsRootSignature);

	auto boundingPipeLineStateDesc = pipelineStateDesc;

	// 바운딩박스
	UINT inputElementDescsNum = 5;
	D3D12_INPUT_ELEMENT_DESC *BoundinginputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	BoundinginputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	BoundinginputElementDescs[1] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[2] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[3] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[4] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

	boundingPipeLineStateDesc.InputLayout.NumElements = inputElementDescsNum;
	boundingPipeLineStateDesc.InputLayout.pInputElementDescs = BoundinginputElementDescs;

	boundingPipeLineStateDesc.RasterizerState.FillMode = D3D12_FILL_MODE_WIREFRAME;
	boundingPipeLineStateDesc.VS = Shader::CompileShaderFromFile(L"BoundingBox.hlsl", "VS_Bounding", "vs_5_1", &vertexShaderBlob);
	boundingPipeLineStateDesc.PS = Shader::CompileShaderFromFile(L"BoundingBox.hlsl", "PS_Bounding", "ps_5_1", &pixelShaderBlob);

	HRESULT hResult = device->CreateGraphicsPipelineState(&boundingPipeLineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[1]);

	Shader::CreateShadowShader(device, graphicsRootSignature);

	::ZeroMemory(&computePipelineStateDesc, sizeof(D3D12_COMPUTE_PIPELINE_STATE_DESC));
	computePipelineStateDesc.pRootSignature = computeRootSignature;
	computePipelineStateDesc.CS = CreateComputeShader();

	hResult = device->CreateComputePipelineState(&computePipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&computePipelineStates[0]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}

void SkinnedAnimationObjectsShader::BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context, int hObjectSize)
{
	this->hObjectSize = hObjectSize;
	this->objects = context;
}

void SkinnedAnimationObjectsShader::AddSingleObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int id, int jobIdx, XMFLOAT3 pos)
{
	LoadedModelInfo* playerModel = (LoadedModelInfo*)objects[hObjectSize + jobIdx];

	SkinnedObjectBuffer* playerBuffer = new SkinnedObjectBuffer;

	if (jobIdx == PlayerJobBow)
		playerBuffer->weaponNum1 = 5;

	playerBuffer->textureName = playerModel->modelRootObject->textureName;
	playerBuffer->skinnedMeshNum = playerModel->skinnedMeshNum;
	playerBuffer->standardMeshNum = playerModel->standardMeshNum;
	playerBuffer->boundingMeshNum = playerModel->boundingMeshNum;
	playerBuffer->skinnedMeshes = new SkinnedMesh*[playerModel->skinnedMeshNum];
	playerBuffer->pipeLineStateIdx = 0;
	playerBuffer->shadowPipeLineStateIdx = 0;
	playerBuffer->mainCharacter = true;
	playerBuffer->id = id;
	playerBuffer->playerJob = jobIdx;

	for (int i = 0; i < playerBuffer->skinnedMeshNum; i++) playerBuffer->skinnedMeshes[i] = playerModel->skinnedMeshes[i];

	playerBuffer->object = new AnimationObject(device, commandList, playerModel, 1, true);

	playerBuffer->object->skinnedAnimationController->SetTrackAnimationSet(0, 0);
	playerBuffer->object->skinnedAnimationController->SetTrackSpeed(0, 0.25f);
	playerBuffer->object->skinnedAnimationController->SetTrackPosition(0, 0.0f);
	playerBuffer->object->SetPosition(pos);
	playerBuffer->object->GetobbVector().resize(playerBuffer->boundingMeshNum);
	playerBuffer->object->Rotate(0, -90, 0);
	playerBuffer->object->GetOBB().Extents = XMFLOAT3(0.4f, 2.0f, 0.4f);
	playerBuffer->object->isLive = true;

	objectBuffer.emplace_back(playerBuffer);

	CreateShaderVariables(device, commandList, objectBuffer.size() - 1);

	SetTextures(device, commandList);

}

void SkinnedAnimationObjectsShader::ReleaseObjects()
{
	for (int i = 0; i < objectBuffer.size(); ++i) {
		//if (objectBuffer[i]->object)
		//{
		//	objectBuffer[i]->object->Release();
		//}
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->Release();
	}
}

void SkinnedAnimationObjectsShader::ReleaseShaderVariables()
{
	for (int i = 0; i < objectBuffer.size(); ++i) {
		for (int j = 0; j < objectBuffer[i]->skinnedMeshNum; ++j) {
			if (objectBuffer[i]->skinnedBuffer[j])
				objectBuffer[i]->skinnedBuffer[j]->Release();

			objectBuffer[i]->positionW[j]->Release();
			objectBuffer[i]->normalW[j]->Release();
			objectBuffer[i]->tangentW[j]->Release();
			objectBuffer[i]->biTangentW[j]->Release();
		}
		for (int j = 0; j < objectBuffer[i]->standardMeshNum; ++j)
		{
			if (objectBuffer[i]->vbGameObject[j])
				objectBuffer[i]->vbGameObject[j]->Unmap(0, NULL);
				objectBuffer[i]->vbGameObject[j]->Release();
		}
	}
	
}

void SkinnedAnimationObjectsShader::AnimateObjects(float timeElapsed, Camera* camera)
{
	elapsedTime = timeElapsed;
	for (int i = 0; i < objectBuffer.size(); ++i) {
		objectBuffer[i]->object->GetOBB().Center = objectBuffer[i]->object->GetPosition();
	}
}

void SkinnedAnimationObjectsShader::ReleaseUploadBuffers()
{
	for (int i = 0; i < objectBuffer.size(); ++i) {
		//if (objectBuffer[i]->object)
		//{
		//	objectBuffer[i]->object->ReleaseUploadBuffers();
		//}
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->ReleaseUploadBuffers();
	}
}

void SkinnedAnimationObjectsShader::CreateShaderVariables(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx)
{
	UINT skinnedStride = 0;
	ID3D12Resource** tempSkinned = NULL;
	XMFLOAT4X4** tempMappedSkinned = NULL;
	
	skinnedStride = sizeof(XMFLOAT4X4) * SKINNED_ANIMATION_BONES;
	tempSkinned = new ID3D12Resource*[objectBuffer[idx]->skinnedMeshNum];
	tempMappedSkinned = new XMFLOAT4X4*[objectBuffer[idx]->skinnedMeshNum];

	for (int i = 0; i < objectBuffer[idx]->skinnedMeshNum; ++i)
	{
		tempSkinned[i] = ::CreateBufferResource(device, commandList, NULL, skinnedStride, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_GENERIC_READ, NULL);
		tempSkinned[i]->Map(0, NULL, (void **)&tempMappedSkinned[i]);
	}
	// Skinned Bone Transform Buffers ( SRV )

	UINT standardStride = 0;
	ID3D12Resource** tempGameObject = NULL;
	VS_VB_INSTANCE_OBJECT** tempMappedGameObject = NULL;
	D3D12_VERTEX_BUFFER_VIEW* tempGameObjectView = NULL;

	standardStride = ((sizeof(VS_VB_INSTANCE_OBJECT) + 255) & ~255); //256의 배수
	tempGameObject = new ID3D12Resource*[objectBuffer[idx]->standardMeshNum];
	tempMappedGameObject = new VS_VB_INSTANCE_OBJECT*[objectBuffer[idx]->standardMeshNum];
	tempGameObjectView = new D3D12_VERTEX_BUFFER_VIEW[objectBuffer[idx]->standardMeshNum];

	for (int j = 0; j < objectBuffer[idx]->standardMeshNum; ++j)
	{
		tempGameObject[j] = ::CreateBufferResource(device, commandList, NULL, standardStride, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		tempGameObject[j]->Map(0, NULL, (void **)&tempMappedGameObject[j]);

		tempGameObjectView[j].BufferLocation = tempGameObject[j]->GetGPUVirtualAddress();
		tempGameObjectView[j].StrideInBytes = standardStride;
		tempGameObjectView[j].SizeInBytes = standardStride;
	}
	// Standard Mesh Transform Buffer ( Vertex Buffer )

	UINT boundStride = 0;
	ID3D12Resource** tempbounding = NULL;
	VS_VB_INSTANCE_BOUNDING** tempMappedBounding = NULL;
	D3D12_VERTEX_BUFFER_VIEW* tempBoundingBufferView;

	boundStride = sizeof(VS_VB_INSTANCE_BOUNDING);
	tempbounding = new ID3D12Resource*[objectBuffer[idx]->boundingMeshNum];
	tempMappedBounding = new VS_VB_INSTANCE_BOUNDING*[objectBuffer[idx]->boundingMeshNum];
	tempBoundingBufferView = new D3D12_VERTEX_BUFFER_VIEW[objectBuffer[idx]->boundingMeshNum];

	for (int i = 0; i < objectBuffer[idx]->boundingMeshNum; ++i)
	{
		tempbounding[i] = ::CreateBufferResource(device, commandList, NULL, boundStride * (UINT)BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		tempbounding[i]->Map(0, NULL, (void**)&tempMappedBounding[i]);

		tempBoundingBufferView[i].BufferLocation = tempbounding[i]->GetGPUVirtualAddress();
		tempBoundingBufferView[i].StrideInBytes = boundStride;
		tempBoundingBufferView[i].SizeInBytes = boundStride * (UINT)BUFFERSIZE;
	}

	XMFLOAT4X4** tempObbCache = NULL;
	tempObbCache = new XMFLOAT4X4*[objectBuffer[idx]->boundingMeshNum];
	for (int i = 0; i < objectBuffer[idx]->boundingMeshNum; ++i)
	{
		tempObbCache[i] = new XMFLOAT4X4;
	}


	ID3D12Resource** tempPositionW = NULL;
	ID3D12Resource** tempNormalW = NULL;
	ID3D12Resource** tempTangentW = NULL;
	ID3D12Resource** tempBiTangentW = NULL;

	tempPositionW = new ID3D12Resource*[objectBuffer[idx]->skinnedMeshNum];
	tempNormalW = new ID3D12Resource*[objectBuffer[idx]->skinnedMeshNum];
	tempTangentW = new ID3D12Resource*[objectBuffer[idx]->skinnedMeshNum];
	tempBiTangentW = new ID3D12Resource*[objectBuffer[idx]->skinnedMeshNum];

	for (int i = 0; i < objectBuffer[idx]->skinnedMeshNum; ++i) {
		UINT bytesNum = sizeof(XMFLOAT3) * objectBuffer[idx]->skinnedMeshes[i]->GetVerticesNum() * SKINNED_BUFFERSIZE;
		tempPositionW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		tempNormalW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		tempTangentW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);
		tempBiTangentW[i] = ::CreateBufferResource(device, commandList, NULL, bytesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS);

		::ResourceBarrier(commandList, tempPositionW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, tempNormalW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, tempTangentW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
		::ResourceBarrier(commandList, tempBiTangentW[i], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	}
	// ComputeSkinning

	objectBuffer[idx]->skinnedBuffer = tempSkinned;
	objectBuffer[idx]->mappedSkinnedBuffer = tempMappedSkinned;

	objectBuffer[idx]->vbGameObject = tempGameObject;
	objectBuffer[idx]->vbMappedGameObject = tempMappedGameObject;
	objectBuffer[idx]->vbGameObjectView = tempGameObjectView;

	objectBuffer[idx]->positionW = tempPositionW;
	objectBuffer[idx]->normalW = tempNormalW;
	objectBuffer[idx]->tangentW = tempTangentW;
	objectBuffer[idx]->biTangentW = tempBiTangentW;

	objectBuffer[idx]->boundingBuffer = tempbounding;
	objectBuffer[idx]->mappedBoundingBuffer = tempMappedBounding;
	objectBuffer[idx]->instancingBoundingBufferView = tempBoundingBufferView;

	objectBuffer[idx]->obbWorldCache = tempObbCache;
}

void SkinnedAnimationObjectsShader::UpdateShaderVariables(int idx)
{
	for (int i = 0; i < objectBuffer[idx]->skinnedMeshNum; ++i)
	{
		for (int j = 0; j < objectBuffer[idx]->skinnedMeshes[i]->skinningBones; ++j)
		{
			XMStoreFloat4x4(&objectBuffer[idx]->mappedSkinnedBuffer[i][j],
				XMMatrixTranspose(XMLoadFloat4x4(&objectBuffer[idx]->skinnedMeshes[i]->skinningBoneFrameCaches[j]->GetWorld())));
		}
	}
}

void SkinnedAnimationObjectsShader::OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx, bool isShadow)
{
	if (!isShadow) {
		if (objectBuffer[idx]->diffuseIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[objectBuffer[idx]->diffuseIdx].rootParameterIndex, Scene::rootArgumentInfos[objectBuffer[idx]->diffuseIdx].srvGpuDescriptorHandle);
		if (objectBuffer[idx]->normalIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[objectBuffer[idx]->normalIdx].rootParameterIndex, Scene::rootArgumentInfos[objectBuffer[idx]->normalIdx].srvGpuDescriptorHandle);
		if (objectBuffer[idx]->specularIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[objectBuffer[idx]->specularIdx].rootParameterIndex, Scene::rootArgumentInfos[objectBuffer[idx]->specularIdx].srvGpuDescriptorHandle);
		if (objectBuffer[idx]->metallicIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[objectBuffer[idx]->metallicIdx].rootParameterIndex, Scene::rootArgumentInfos[objectBuffer[idx]->metallicIdx].srvGpuDescriptorHandle);
		if (objectBuffer[idx]->emissionIdx >= 0)
			commandList->SetGraphicsRootDescriptorTable(Scene::rootArgumentInfos[objectBuffer[idx]->emissionIdx].rootParameterIndex, Scene::rootArgumentInfos[objectBuffer[idx]->emissionIdx].srvGpuDescriptorHandle);
	}

	if (isShadow)
	{
		if (Shader::curPipelineState != shadowPipelineStates[objectBuffer[idx]->shadowPipeLineStateIdx]) {
			commandList->SetPipelineState(shadowPipelineStates[objectBuffer[idx]->shadowPipeLineStateIdx]);
			curPipelineState = shadowPipelineStates[objectBuffer[idx]->shadowPipeLineStateIdx];
		}
	}
	else
	{
		if (Shader::curPipelineState != pipelineStates[objectBuffer[idx]->pipeLineStateIdx]) {
			commandList->SetPipelineState(pipelineStates[objectBuffer[idx]->pipeLineStateIdx]);
			curPipelineState = pipelineStates[objectBuffer[idx]->pipeLineStateIdx];
		}
	}
}

void SkinnedAnimationObjectsShader::PrepareCompute(ID3D12GraphicsCommandList * commandList)
{
	for (int k = 0; k < objectBuffer.size(); ++k)
	{
		for (int i = 0; i < objectBuffer[k]->skinnedMeshNum; ++i)
		{
			if (objectBuffer[k]->object) {

				objectBuffer[k]->skinnedMeshes[i]->skinningBoneTransforms = objectBuffer[k]->skinnedBuffer[i];
				objectBuffer[k]->skinnedMeshes[i]->mappedSkinningBoneTransforms = objectBuffer[k]->mappedSkinnedBuffer[i];

				commandList->SetPipelineState(computePipelineStates[0]);
				ID3D12Resource* buffers[4] = { objectBuffer[k]->positionW[i], objectBuffer[k]->normalW[i], objectBuffer[k]->tangentW[i], objectBuffer[k]->biTangentW[i] };

				objectBuffer[k]->skinnedMeshes[i]->OnPreRender(commandList, 1, buffers);
			}
		}
	}
}

void SkinnedAnimationObjectsShader::FrustumCulling(void* frustum, bool isShadow, UINT cameraIdx)
{
	for (int k = 0; k < objectBuffer.size(); k++)
	{
		BoundingOrientedBox obb = objectBuffer[k]->obb;
		BoundingOrientedBox resultOBB;
		obb.Transform(resultOBB, XMLoadFloat4x4(&objectBuffer[k]->object->GetWorld()));
		bool isVisable = true;
		if (isShadow)
		{
			BoundingOrientedBox camFrustum = *(BoundingOrientedBox*)frustum;
			isVisable = camFrustum.Intersects(resultOBB);
		}
		else
		{
			BoundingFrustum camFrustum = *(BoundingFrustum*)frustum;
			isVisable = camFrustum.Intersects(resultOBB);
		}
		objectBuffer[k]->isVisable[cameraIdx] = isVisable;
	}
}

void SkinnedAnimationObjectsShader::Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx)
{
	for (int k = 0; k < objectBuffer.size(); k++)
	{
		OnPrepareRender(commandList, k, isShadow);

		if (objectBuffer[k]->isVisable[cameraIdx]) {
			if (objectBuffer[k]->object->isLive == true)
			{

				objectBuffer[k]->object->Animate(elapsedTime);
				UpdateShaderVariables(k);
				for (int i = 0; i < objectBuffer[k]->skinnedMeshNum; ++i)
				{
					objectBuffer[k]->skinnedMeshes[i]->skinningBoneTransforms = objectBuffer[k]->skinnedBuffer[i];
					objectBuffer[k]->skinnedMeshes[i]->mappedSkinningBoneTransforms = objectBuffer[k]->mappedSkinnedBuffer[i];
				}
				objectBuffer[k]->object->UpdateSkinnedBoundingTransform(objectBuffer[k]->mappedBoundingBuffer, objectBuffer[k]->object->GetobbVector(), NULL, 0, objectBuffer[k]->boundingMeshNum, NULL);
				objectBuffer[k]->object->UpdateTransformOnlyStandardMesh(objectBuffer[k]->vbMappedGameObject, 0, objectBuffer[k]->standardMeshNum, objectBuffer[k]->weaponNum, NULL, NULL);

				ID3D12Resource** buffers[4] = { objectBuffer[k]->positionW, objectBuffer[k]->normalW, objectBuffer[k]->tangentW, objectBuffer[k]->biTangentW };

				objectBuffer[k]->object->Render(commandList, 1, objectBuffer[k]->vbGameObjectView, buffers, isShadow, objectBuffer[k]->weaponNum, -1);

				if (showBounding) {
					
					if (Shader::curPipelineState != pipelineStates[1]) {
						commandList->SetPipelineState(pipelineStates[1]);
						curPipelineState = pipelineStates[1];
					}
					objectBuffer[k]->object->SkinnedBoundingRender(commandList, 1, objectBuffer[k]->instancingBoundingBufferView, objectBuffer[k]->weaponNum);
				}
			}
		}
	}
}

void SkinnedAnimationObjectsShader::SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	for (int k = 0; k < objectBuffer.size(); ++k) {

		if (objectBuffer[k]->textureName.diffuse.size() > 0)
		{
			Texture* diffuseTexture = new Texture((int)objectBuffer[k]->textureName.diffuse.size(), ResourceTexture2D, 0);
			for (int i = 0; i < objectBuffer[k]->textureName.diffuse.size(); ++i)
			{
				diffuseTexture->LoadTextureFromFile(device, commandList, objectBuffer[k]->textureName.diffuse[i].second, objectBuffer[k]->textureName.diffuse[i].first);
			}
			objectBuffer[k]->diffuseIdx = Scene::CreateShaderResourceViews(device, commandList, diffuseTexture, GraphicsRootDiffuseTextures);
			textures.emplace_back(diffuseTexture);
		}
		if (objectBuffer[k]->textureName.normal.size() > 0)
		{
			Texture* normalTexture = new Texture((int)objectBuffer[k]->textureName.normal.size(), ResourceTexture2D, 0);
			for (int i = 0; i < objectBuffer[k]->textureName.normal.size(); ++i)
			{
				normalTexture->LoadTextureFromFile(device, commandList, objectBuffer[k]->textureName.normal[i].second, objectBuffer[k]->textureName.normal[i].first);
			}
			objectBuffer[k]->normalIdx = Scene::CreateShaderResourceViews(device, commandList, normalTexture, GraphicsRootNormalTextures);
			textures.emplace_back(normalTexture);
		}
		if (objectBuffer[k]->textureName.specular.size() > 0)
		{
			Texture* specularTexture = new Texture((int)objectBuffer[k]->textureName.specular.size(), ResourceTexture2D, 0);
			for (int i = 0; i < objectBuffer[k]->textureName.specular.size(); ++i)
			{
				specularTexture->LoadTextureFromFile(device, commandList, objectBuffer[k]->textureName.specular[i].second, objectBuffer[k]->textureName.specular[i].first);
			}
			objectBuffer[k]->specularIdx = Scene::CreateShaderResourceViews(device, commandList, specularTexture, GraphicsRootSpecularTextures);
			textures.emplace_back(specularTexture);
		}
		if (objectBuffer[k]->textureName.metallic.size() > 0)
		{
			Texture* metallicTexture = new Texture((int)objectBuffer[k]->textureName.metallic.size(), ResourceTexture2D, 0);
			for (int i = 0; i < objectBuffer[k]->textureName.metallic.size(); ++i)
			{
				metallicTexture->LoadTextureFromFile(device, commandList, objectBuffer[k]->textureName.metallic[i].second, objectBuffer[k]->textureName.metallic[i].first);
			}
			objectBuffer[k]->metallicIdx = Scene::CreateShaderResourceViews(device, commandList, metallicTexture, GraphicsRootMetallicTextures);
			textures.emplace_back(metallicTexture);
		}
	}
}

void SkinnedAnimationObjectsShader::Move(int direction, float force, float elapsedTime, int bufferidx)
{
	if (direction != 0)
	{
		auto& ob = objectBuffer[bufferidx];

		XMFLOAT3 vForce = XMFLOAT3(0, 0, 0);
		XMFLOAT3 obLook = ob->object->GetLook();
		XMFLOAT3 obRight = ob->object->GetRight();
		XMFLOAT3 obUp = ob->object->GetUp();

		if (direction == DirectionForword)
			vForce = Vector3::Add(vForce, obLook, force);
		if (direction == DirectionBackword)
			vForce = Vector3::Add(vForce, obLook, -force);
		//화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.

		if (direction == DirectionRight) vForce = Vector3::Add(vForce, obRight, force);
		if (direction == DirectionLeft) vForce = Vector3::Add(vForce, obRight, -force);
		//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.

		XMFLOAT3 opRight = XMFLOAT3(-obRight.x, -obRight.y, -obRight.z);
		XMFLOAT3 lrVec = Vector3::Normalize(Vector3::Add(obLook, obRight));
		XMFLOAT3 mlrVec = Vector3::Normalize(Vector3::Add(obLook, opRight));

		//대각선 이동 
		if (direction == DirectionFR) vForce = Vector3::Add(vForce, lrVec, force);
		if (direction == DirectionFL) vForce = Vector3::Add(vForce, mlrVec, force);
		if (direction == DirectionBR) vForce = Vector3::Add(vForce, mlrVec, -force);
		if (direction == DirectionBL) vForce = Vector3::Add(vForce, lrVec, -force);


		if (direction == DirectionUp) vForce = Vector3::Add(vForce, obUp, force);
		if (direction == DirectionDown) vForce = Vector3::Add(vForce, obUp, -force);
		//‘Page Up’을 누르면 로컬 y-축 방향으로 이동한다. ‘Page Down’을 누르면 반대 방향으로 이동한다.
	
		
		ob->acc = XMFLOAT3(vForce.x / ob->mass, 0, vForce.z / ob->mass); // 가속도 
		float vlength = Vector3::Length(ob->velocity);


		if (vlength < ob->maxVelocityXZ)
			ob->velocity = Vector3::Add(ob->velocity, XMFLOAT3((ob->acc.x * elapsedTime), 0, (ob->acc.z * elapsedTime)));

		XMFLOAT3 finalvec = Vector3::Add(XMFLOAT3(ob->velocity.x * elapsedTime, 0.0f, ob->velocity.z * elapsedTime), XMFLOAT3(ob->acc.x * elapsedTime * elapsedTime * 0.5f, 0.0f, ob->acc.z * elapsedTime * elapsedTime * 0.5f));

		ob->object->SetPosition(Vector3::Add(ob->object->GetPosition(), finalvec));
		ob->object->GetOBB().Center = ob->object->GetPosition();
		//플레이어를 현재 위치 벡터에서 Shift 벡터만큼 이동한다.
		float length = sqrtf(ob->velocity.x * ob->velocity.x + ob->velocity.z * ob->velocity.z);

		if (direction == DirectionNoMove)
		{
			length = Vector3::Length(ob->velocity);
			float deceleration = (ob->friction * elapsedTime);
			if (deceleration > length) deceleration = length;
			ob->velocity = Vector3::Add(ob->velocity, Vector3::ScalarProduct(ob->velocity, -deceleration, false));
		}
		else {
			length = Vector3::Length(ob->velocity);
			float deceleration = ((ob->friction - 19.0f) * elapsedTime);
			if (deceleration > length) deceleration = length;
			ob->velocity = Vector3::Add(ob->velocity, Vector3::ScalarProduct(ob->velocity, -deceleration, false));
		}
	}
}


