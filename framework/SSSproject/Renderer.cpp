#include "stdafx.h"
#include "Renderer.h"

RenderInfo::RenderInfo(ID3D12Device* device)
{
	for (int j = 0; j < COMMAND_IDX_NUM; ++j)
	{
		HRESULT hresult = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, __uuidof(ID3D12CommandAllocator), (void**)&shadowAllocator[j]);
		hresult = device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, shadowAllocator[j], NULL, __uuidof(ID3D12GraphicsCommandList), (void**)&shadowCommandList[j]);
		shadowCommandList[j]->Close();

		hresult = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, __uuidof(ID3D12CommandAllocator), (void**)&renderAllocator[j]);
		hresult = device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, renderAllocator[j], NULL, __uuidof(ID3D12GraphicsCommandList), (void**)&renderCommandList[j]);
		renderCommandList[j]->Close();
	}
}

RenderInfo::~RenderInfo()
{
}

void RenderInfo::SetCamera(Camera * camera, Camera* lightCamera[MAX_CASCADE_SIZE])
{
	for (int i = 0; i < MAX_CASCADE_SIZE; ++i)
	{
		this->shadowFrustum[i] = lightCamera[i]->GetBoundingOrientedBox();
		this->lightCamera[i] = lightCamera[i];
	}
	this->frustum = camera->GetBoundingFrustum();
	this->camera = camera;
}

void RenderInfo::SetHandles(D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle, D3D12_CPU_DESCRIPTOR_HANDLE depthHandle, D3D12_CPU_DESCRIPTOR_HANDLE shadowHandle, int idx)
{
	this->renderTargetHandle[idx] = rtvHandle;
	this->depthBufferHandle[idx] = depthHandle;
	this->shadowBufferHandle[idx] = shadowHandle;
}

void RenderInfo::SetShader(Shader * shader)
{
	shaders.emplace_back(shader);
}


void RenderInfo::SetPlayer(Player * player)
{
	this->player = player;
}

ID3D12CommandAllocator* RenderInfo::GetShadowAllocator(int idx)
{
	return shadowAllocator[idx];
}

ID3D12GraphicsCommandList* RenderInfo::GetShadowCommandList(int idx)
{
	return shadowCommandList[idx];
}


ID3D12CommandAllocator* RenderInfo::GetRenderAllocator(int idx)
{
	return renderAllocator[idx];
}


ID3D12GraphicsCommandList* RenderInfo::GetRenderCommandList(int idx)
{
	return renderCommandList[idx];
}

std::vector<Shader*> RenderInfo::GetShaders()
{
	return shaders;
}

Player * RenderInfo::GetPlayer()
{
	return player;
}

Camera * RenderInfo::GetCamera()
{
	return camera;
}

Camera * RenderInfo::GetLightCamera(int idx)
{
	return lightCamera[idx];
}

BoundingFrustum RenderInfo::GetFrustum()
{
	return frustum;
}
BoundingOrientedBox RenderInfo::GetShadowFrustum(int idx)
{
	return shadowFrustum[idx];
}


D3D12_CPU_DESCRIPTOR_HANDLE RenderInfo::GetRenderTargetHandle(int idx)
{
	return renderTargetHandle[idx];
}


D3D12_CPU_DESCRIPTOR_HANDLE RenderInfo::GetDepthBufferHandle(int idx)
{
	return depthBufferHandle[idx];
}


D3D12_CPU_DESCRIPTOR_HANDLE RenderInfo::GetShadowBufferHandle(int idx)
{
	return shadowBufferHandle[idx];
}

ComputeInfo::ComputeInfo(ID3D12Device* device)
{
	for (int i = 0; i < COMMAND_IDX_NUM; ++i) {
		// Direct가 아닌 Compute로 바꿀 필요가 있어보인다. 그러면 CommandQueue 역시 Compute여야 한다. 하나 더 생성하는것이 맞다.
		HRESULT hresult = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, __uuidof(ID3D12CommandAllocator), (void**)&allocator[i]);
		hresult = device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, allocator[i], NULL, __uuidof(ID3D12GraphicsCommandList), (void**)&commandList[i]);
		commandList[i]->Close();
	}
}

ComputeInfo::~ComputeInfo()
{
}

ID3D12CommandAllocator * ComputeInfo::GetAllocator(int idx)
{
	return allocator[idx];
}

ID3D12GraphicsCommandList * ComputeInfo::GetCommandList(int idx)
{
	return commandList[idx];
}

void ComputeInfo::SetHandles(D3D12_CPU_DESCRIPTOR_HANDLE rtvHandle, D3D12_CPU_DESCRIPTOR_HANDLE depthHandle, D3D12_CPU_DESCRIPTOR_HANDLE shadowHandle, int idx)
{
	this->renderTargetHandle[idx] = rtvHandle;
	this->depthBufferHandle[idx] = depthHandle;
	this->shadowBufferHandle[idx] = shadowHandle;
}

void ComputeInfo::SetRtBuffer(ID3D12Resource * renderTargetBuffers, int idx)
{
	this->renderTargetBuffer[idx] = renderTargetBuffers;
}

D3D12_CPU_DESCRIPTOR_HANDLE ComputeInfo::GetRenderTargetHandle(int idx)
{
	return renderTargetHandle[idx];
}

D3D12_CPU_DESCRIPTOR_HANDLE ComputeInfo::GetDepthBufferHandle(int idx)
{
	return depthBufferHandle[idx];
}

D3D12_CPU_DESCRIPTOR_HANDLE ComputeInfo::GetShadowBufferHandle(int idx)
{
	return shadowBufferHandle[idx];
}

ID3D12Resource * ComputeInfo::GetRtBuffer(int idx)
{
	return renderTargetBuffer[idx];
}
