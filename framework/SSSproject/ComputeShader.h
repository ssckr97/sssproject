#pragma once
#include "Shader.h"

struct VS_VB_INSTANCE_PARTICLE {
	UINT textureIdx;
	XMFLOAT2 size;
	XMFLOAT3 position;
	XMFLOAT4 color;
	float alphaDegree;
};
// position, velocity, age
struct ParticleBlobModify {

	XMFLOAT3 positionMin;
	XMFLOAT3 positionMax;
	XMFLOAT3 velocityMin;
	XMFLOAT3 velocityMax;
	float ageMin;
	float ageMax;

	int amount;

	ParticleBlobModify() {};
	ParticleBlobModify(XMFLOAT3 positionMin, XMFLOAT3 positionMax, XMFLOAT3 velocityMin, XMFLOAT3 velocityMax, float ageMin, float ageMax, int amount) {
		this->positionMin = positionMin;
		this->positionMax = positionMax;
		this->velocityMin = velocityMin;
		this->velocityMax = velocityMax;
		this->ageMin = ageMin;
		this->ageMax = ageMax;
		this->amount = amount;
	};

};

struct ParticleInfoInCPU {
	int copyIdx;
	UINT textureIdx;
	int type;

	XMFLOAT3 position;
	XMFLOAT2 size;
	XMFLOAT4 color;

	float divAge;
	float age;
	float acc;
	float alphaDegree;

	float radius;
	float yGab;
	float rotateAcc;

	ParticleInfoInCPU() {};
	ParticleInfoInCPU(int copyIdx, UINT textureIdx, int type, XMFLOAT3 position, XMFLOAT2 size, XMFLOAT4 color, float divAge, float age, float acc, float alphaDegree, float radius = 3.0f, float yGab = 0.3f, float rotateAcc = 1.0f){
		this->copyIdx = copyIdx;
		this->textureIdx = textureIdx;
		this->type = type;

		this->position = position;
		this->size = size;
		this->color = color;

		this->divAge = divAge;
		this->age = age;
		this->acc = acc;
		this->alphaDegree = alphaDegree;
		this->radius = radius;
		this->yGab = yGab;
		this->rotateAcc = rotateAcc;
	}
};


class ParticleShader : public StandardComputeShader
{
	// ParticleShader 설명.
	// 각 파티클 오브젝트(하나의 덩어리)는 PARTICLESIZE개의 정점들을 사용하여 그린다.
	// 파티클 오브젝트의 위치정보는 positionBuffer를 통해 관리한다. BUFFERSIZE만큼 갖고 있으며, 순환하여 사용한다.
	// 파티클 입자들의 움직임은 ShaderResourceView를 통해 StructuredBuffer를 사용하고, 컴퓨트 쉐이더를 통해 움직임을 계산한다.
	// 파티클 입자들의 정보는 particleBuffer를 통해 관리하며 BUFFERSIZE만큼 갖고 있으며 순환(currentParticleIdx)하여 사용한다.

protected:

	ID3DBlob* geometryShaderBlob = NULL;

	std::vector<bool> isRender;
	std::vector<ParticleInfoInCPU> particles;
	UINT particlesNum = 0;

	ID3D12Resource* instanceBuffer = NULL;
	VS_VB_INSTANCE_PARTICLE* mappedInstanceBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW* instanceBufferView;

	std::vector<ID3D12Resource*> copyBuffer;
	std::vector<VS_SB_PARTICLE*> mappedCopyBuffer;
	
	std::vector<ID3D12Resource*> copyBufferCS;

	std::vector<ID3D12Resource*> particleBuffer;
	std::vector<ID3D12Resource*> releaseBuffer;

	std::vector<std::pair<int, float>> particleDepths;

public:

	std::vector<ParticleBlobModify> blobList;
	std::unordered_map<std::string, UINT> textureMap;
	std::unordered_map<std::string, int> copyBufferMap;
	std::vector<int> particleSizes;
	int currentParticleIdx = 0;
	int copyBufferIdx = 0;
	int remainCopyBuffers = 0;

	ParticleShader();
	virtual ~ParticleShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateGeometryShader();
	virtual D3D12_SHADER_BYTECODE CreateComputeShader(bool isMove);

	virtual D3D12_PRIMITIVE_TOPOLOGY_TYPE SetTopologyType();

	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx = 0);
	virtual void CreateCopyBuffer(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ParticleBlobModify blob, std::string name);

	virtual void UpdateShaderVariables(int idx);
	virtual void ReleaseShaderVariables();
	virtual void ReleaseUploadBuffers();

	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context, int hObjectSize = 0);

	virtual void ReleaseObjects();

	virtual void ReleaseParticle(int idx);
	virtual void GarbageCollection();

	virtual void Compute(ID3D12GraphicsCommandList* commandList, int idx);
	virtual void Render(ID3D12GraphicsCommandList* commandList, bool isShadow, UINT cameraIdx);
	virtual void CreateParticle(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ParticleInfoInCPU particleInfo);
	virtual void ModifyParticle(ID3D12GraphicsCommandList * commandList, int idx, int copyIdx, ParticleBlobModify blob);
	virtual void SortParticleBuffers(Camera* camera);

	std::vector<ParticleInfoInCPU>& GetParticles() { return particles; }
	std::vector<bool>& GetIsRender() { return isRender; }
	virtual void ClearParticle();

};
