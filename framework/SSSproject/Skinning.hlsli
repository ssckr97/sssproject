#define BLOCK_SIZE 256
#define MAX_VERTEX_INFLUENCES 4
#define SKINNED_ANIMATION_BONES 128

cbuffer cbBoneOffset : register(b4)
{
    float4x4 gpmtxBoneOffsets[SKINNED_ANIMATION_BONES];
}

cbuffer cbVertices : register(b5)
{
    uint gnVerticesNum : packoffset(c0);
}

StructuredBuffer<float4x4> gsbBoneTransforms : register(t2);
StructuredBuffer<float3> gsbPosition : register(t3);
StructuredBuffer<float3> gsbNormal : register(t4);
StructuredBuffer<float3> gsbTangent : register(t5);
StructuredBuffer<float3> gsbBiTangent : register(t6);
StructuredBuffer<int4> gsbBoneIndex : register(t7);
StructuredBuffer<float4> gsbBoneWeight : register(t8);

RWStructuredBuffer<float3> grwsbPositionW : register(u1);
RWStructuredBuffer<float3> grwsbNormalW : register(u2);
RWStructuredBuffer<float3> grwsbTangentW : register(u3);
RWStructuredBuffer<float3> grwsbBiTangentW : register(u4);