#include "Shaders.hlsli"
#include "Light.hlsli"
#include "Shadow.hlsli"
#include "Fog.hlsli"

//////////////////////////////////////////////////////////////////////////////////////////////
// Lighing Shader Code (Using pixel light)

struct VS_STANDARD_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
};

struct VS_STANDARD_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float2 uv : TEXCOORD;
    float3 normalW : NORMAL;
    float3 tangentW : TANGENT;
    float3 bitangentW : BITANGENT;
    
    float4 shadowPosition[3] : TEXCOORD1;
    
    float fogFactor : FACTOR;
};

VS_STANDARD_OUTPUT VSStandard(VS_STANDARD_INPUT input)
{
    VS_STANDARD_OUTPUT output;
    output.positionW = (float3) mul(float4(input.position, 1.0f), gmtxWorld);
    float4 cameraPos = mul(float4(output.positionW, 1.0f), gmtxView);
    output.position = mul(cameraPos, gmtxProjection);
    output.normalW = (float3) mul(float4(input.normal, 1.0f), gmtxWorld);
    output.tangentW = (float3) mul(float4(input.tangent, 1.0f), gmtxWorld);
    output.bitangentW = (float3) mul(float4(input.bitangent, 1.0f), gmtxWorld);

    output.uv = input.uv;
    
    matrix shadowProject[MAX_CASCADE_SIZE];
    
    for (int i = 0; i < int(gfCasacdeNum); ++i)
    {
        shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture);
        output.shadowPosition[i] = mul(float4(output.positionW, 1.0f), shadowProject[i]);
    }
    
    output.fogFactor = GetFogFactor(cameraPos.z);
    
    return output;
}

float4 PSStandard(VS_STANDARD_OUTPUT input) : SV_TARGET
{
    float fShadowFactor = 1.0f;
    fShadowFactor = GetShadowFactor(input.shadowPosition, float4(input.positionW, 1.0f));
    
    uint textureIdx = gnTextureIdx;
    uint uTextureIdx[5];
    
    for (int i = 0; i < 5; ++i)
    {
        uTextureIdx[i] = textureIdx % 100;
        textureIdx = textureIdx / 100;
    }
    
    float4 cDiffuseColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_DIFFUSE_MAP)
        cDiffuseColor = gtxtDiffuseTexture[uTextureIdx[0]].Sample(gssWrap, input.uv);
    float4 cNormalColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_NORMAL_MAP)
        cNormalColor = gtxtNormalTexture[uTextureIdx[1]].Sample(gssWrap, input.uv);
    float4 cSpecularColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_SPECULAR_MAP)
        cSpecularColor = gtxtSpecularTexture[uTextureIdx[2]].Sample(gssWrap, input.uv);
    float4 cMetallicColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_METALLIC_MAP)
        cMetallicColor = gtxtMetallicTexture[uTextureIdx[3]].Sample(gssWrap, input.uv);

    float3 normalW;
    float4 cColor = cDiffuseColor + cSpecularColor + cMetallicColor;
    if (gnTextureMask & MATERIAL_NORMAL_MAP)
    {
        float3x3 TBN = float3x3(normalize(input.tangentW), normalize(input.bitangentW), normalize(input.normalW));
        float3 vNormal = normalize(cNormalColor.rgb * 2.0f - 1.0f); //[0, 1] �� [-1, 1]
        normalW = normalize(mul(vNormal, TBN));
    }
    else
    {
        return BlendFog(float4(cColor.xyz * fShadowFactor, cColor.w), input.fogFactor);
        normalW = normalize(input.normalW);
    }

    uint materialNum = gnMaterial;
    float4 cIllumination = Lighting(input.positionW, normalW, materialNum);
    cIllumination += float4(0.2f, 0.2f, 0.2f, 1.0f);
    
    float4 finalColor = saturate(cColor * cIllumination);
    return BlendFog(float4(finalColor.xyz * fShadowFactor, finalColor.w), input.fogFactor);
    
    
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Instancing Illumination Shader Code

struct VS_INSTANCING_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    
    float4x4 transform : WORLDMATRIX;
};

VS_STANDARD_OUTPUT VSStandardInstancing(VS_INSTANCING_INPUT input)
{
    VS_STANDARD_OUTPUT output;
    output.positionW = mul(float4(input.position, 1.0f), input.transform).xyz;
    float4 cameraPos = mul(float4(output.positionW, 1.0f), gmtxView);
    output.position = mul(cameraPos, gmtxProjection);
    output.normalW = mul(input.normal, (float3x3) input.transform);
    output.tangentW = mul(input.tangent, (float3x3) input.transform);
    output.bitangentW = mul(input.bitangent, (float3x3) input.transform);

    output.uv = input.uv;
    
    matrix shadowProject[MAX_CASCADE_SIZE];
    
    for (int i = 0; i < int(gfCasacdeNum); ++i)
    {
        shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture);
        output.shadowPosition[i] = mul(float4(output.positionW, 1.0f), shadowProject[i]);
    }
    output.fogFactor = GetFogFactor(cameraPos.z);
 
    return (output);
}

/////////////////////////////////////////////////////////////////////////////

struct VS_SKINNED_STANDARD_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    int4 indices : BONEINDEX;
    float4 weights : BONEWEIGHT;
    
    uint vertexID : SV_VertexID;
};

VS_STANDARD_OUTPUT VSSkinnedAnimationStandard(VS_SKINNED_STANDARD_INPUT input)
{
    VS_STANDARD_OUTPUT output;

    output.positionW = gsbSkinnedPosition[input.vertexID];
    output.normalW = gsbSkinnedNormal[input.vertexID];
    output.tangentW = gsbSkinnedTangent[input.vertexID];
    output.bitangentW = gsbSkinnedBiTangent[input.vertexID];
    
    float4 cameraPos = mul(float4(output.positionW, 1.0f), gmtxView);
    output.position = mul(cameraPos, gmtxProjection);
    
    output.uv = input.uv;
    
    matrix shadowProject[MAX_CASCADE_SIZE];
    
    for (int i = 0; i < int(gfCasacdeNum); ++i)
    {
        shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture);
        output.shadowPosition[i] = mul(float4(output.positionW, 1.0f), shadowProject[i]);
    }
    output.fogFactor = GetFogFactor(cameraPos.z);

    return (output);
}

/////////////////////////////////////////////////////////////////////////////

struct VS_SKINNED_INSTANCING_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    int4 indices : BONEINDEX;
    float4 weights : BONEWEIGHT;
    uint objIdx : OBJIDX;
    
    uint vertexID : SV_VertexID;
};

VS_STANDARD_OUTPUT VSSkinnedAnimationInstancing(VS_SKINNED_INSTANCING_INPUT input)
{
    VS_STANDARD_OUTPUT output;

    output.positionW = gsbSkinnedPosition[input.objIdx * gnVerticesNum + input.vertexID];
    output.normalW = gsbSkinnedNormal[input.objIdx * gnVerticesNum + input.vertexID];
    output.tangentW = gsbSkinnedTangent[input.objIdx * gnVerticesNum + input.vertexID];
    output.bitangentW = gsbSkinnedBiTangent[input.objIdx * gnVerticesNum + input.vertexID];
    
    float4 cameraPos = mul(float4(output.positionW, 1.0f), gmtxView);
    output.position = mul(cameraPos, gmtxProjection);
    
    output.uv = input.uv;
    
    matrix shadowProject[MAX_CASCADE_SIZE];
    
    for (int i = 0; i < int(gfCasacdeNum); ++i)
    {
        shadowProject[i] = mul(gmtxLightViewProjection[i], gmtxProjectToTexture);
        output.shadowPosition[i] = mul(float4(output.positionW, 1.0f), shadowProject[i]);
    }
    output.fogFactor = GetFogFactor(cameraPos.z);
    
    return (output);
}