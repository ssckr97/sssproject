#pragma once
#include "GameObject.h"
#include "Camera.h"
#include "Animation.h"
//인스턴스 정보(게임 객체의 월드 변환 행렬과 객체의 색상)를 위한 구조체이다.
struct VS_VB_INSTANCE_OBJECT
{
	XMFLOAT4X4 transform;
};

struct VS_VB_INSTANCE_MATERIAL
{
	UINT reflection;	// 재질의 번호
	UINT textureMask;	// 텍스쳐 종류
	UINT textureIdx;	// 텍스쳐 인덱스 diffuse, normal, specular, metallic, emissive
};

struct VS_VB_INSTANCE_BILLBOARD
{
	XMFLOAT3 position;
	XMFLOAT3 instanceInfo;	// x : width, y : height, z : textureIdx
};

struct CB_GAMEOBJECT_INFO
{
	XMFLOAT4X4 world;
};

struct CB_PLAYER_INFO
{
	XMFLOAT4X4 world;
};

struct VS_SB_PARTICLE
{
	XMFLOAT3 position;
	XMFLOAT3 velocity;
	float age;
};

struct VS_VB_INSTANCE_BOUNDING {
	XMFLOAT4X4 transform;
};

//셰이더 소스 코드를 컴파일하고 그래픽스 상태 객체를 생성한다.
class Shader
{
public:
	Shader();
	virtual ~Shader();
private:
	int references = 0;

public:
	void AddRef() { ++references; }
	void Release() { if (--references <= 0) delete this; }

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_RASTERIZER_DESC CreateRasterizerState();
	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateGeometryShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowGeometryShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowPixelShader();

	virtual D3D12_PRIMITIVE_TOPOLOGY_TYPE SetTopologyType();

	D3D12_SHADER_BYTECODE CompileShaderFromFile(const WCHAR* fileName, LPCSTR shaderName, LPCSTR shaderProfile, ID3DBlob** shaderBlob);

	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
	virtual void CreateShadowShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx = 0);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseShaderVariables();

	virtual void UpdateShaderVariable(ID3D12GraphicsCommandList* commandList, XMFLOAT4X4* world);
	virtual void UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, MATERIAL* material) { }
	virtual void ReleaseUploadBuffers() {}

	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context = NULL) {}
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context, int hObjectSize = 0) {}
	virtual void AddSingleObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context , int id , XMFLOAT3 pos) {}
	virtual void ReleaseObjects() {}
	virtual void AnimateObjects(float timeElapsed, Camera* camera) {}
	virtual void Animate() {};
	virtual void DeleteAllObjcet() {}
	virtual void LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType) {}
	virtual void GarbageCollection() {};

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx, bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList* commandList, bool isShadow, UINT cameraIdx);
	virtual void PrepareCompute(ID3D12GraphicsCommandList* commandList);

	virtual void FrustumCulling(void* frustum, bool isShadow, UINT cameraIdx) {};

	virtual GameObject* GetObjects(int idx) { return NULL; }

	virtual void ApplyLOD(Camera* camera, int idx) {}

	static ID3D12PipelineState* curPipelineState;

	ID3D12PipelineState** GetPipelineStates() { return pipelineStates; }
	ID3D12PipelineState** GetShadowPipelineStates() { return shadowPipelineStates; }
	ID3D12PipelineState** GetComputePiplineStates() { return computePipelineStates; }

protected:

	ID3DBlob*							vertexShaderBlob = NULL;
	ID3DBlob*							pixelShaderBlob = NULL;
	ID3DBlob*							geometryShaderBlob = NULL;
	ID3DBlob*							computeShaderBlob = NULL;

	UINT								pipelineStatesNum = 0;
	UINT								computePipelineStatesNum = 0;
	UINT								shadowPipelineStateNum = 0;
	ID3D12PipelineState**				pipelineStates = NULL;
	ID3D12PipelineState**				computePipelineStates = NULL;
	ID3D12PipelineState**				shadowPipelineStates = NULL;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC	pipelineStateDesc;
	D3D12_COMPUTE_PIPELINE_STATE_DESC computePipelineStateDesc;

	float								elapsedTime = 0.0f;
};

class StandardShader : public Shader
{
public:
	StandardShader();
	virtual ~StandardShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();

	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();

	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature *graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
};

class StandardInstanceShader : public StandardShader
{
public:
	StandardInstanceShader();
	virtual ~StandardInstanceShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowPixelShader();

	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature *graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
};

class StandardComputeShader : public Shader
{

public:
	StandardComputeShader();
	virtual ~StandardComputeShader();
	virtual D3D12_SHADER_BYTECODE CreateComputeShader();

};

class StandardSkinnedAnimationShader : public StandardShader
{
public:
	StandardSkinnedAnimationShader();
	virtual ~StandardSkinnedAnimationShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateComputeShader();
};

class StandardSkinnedAnimationInstanceShader : public StandardSkinnedAnimationShader
{
	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature);

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
		
};

