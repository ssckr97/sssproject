#include "Shaders.hlsli"
#include "Fog.hlsli"

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Particle Shader Code

struct VS_WEAPONTRAIL_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    uint textureIdx : TEXTUREIDX;
    float4 color : COLOR;
    float age : AGE;
};

struct VS_WEAPONTRAIL_OUTPUT
{
    float4 position : SV_Position;
    float2 uv : TEXCOORD0;
    uint textureIdx : TEXTUREIDX;
    float4 color : COLOR;
    float age : AGE;

};

VS_WEAPONTRAIL_OUTPUT VSWeaponTrail(VS_WEAPONTRAIL_INPUT input)
{
    VS_WEAPONTRAIL_OUTPUT output;

    output.position = mul(mul(float4(input.position, 1.0f), gmtxView), gmtxProjection);
    output.age = input.age;
    output.uv = input.uv;
    output.textureIdx = input.textureIdx;
    output.color = input.color;

    return output;
}


float4 PSWeaponTrail(VS_WEAPONTRAIL_OUTPUT input) : SV_TARGET
{  
    float bias = 0.2f;
    float4 color = input.color;
    color.a *= input.age;
    color.a += bias;
   
    return color;
}
