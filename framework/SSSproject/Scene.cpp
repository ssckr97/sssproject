#include "stdafx.h"
#include "Scene.h"
#include "Player.h"
#include "Texture.h"
#include "Animation.h"

ID3D12DescriptorHeap*				Scene::cbvSrvUavDescriptorHeap = NULL;

D3D12_CPU_DESCRIPTOR_HANDLE			Scene::cbvCPUDescriptorStartHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::cbvGPUDescriptorStartHandle;
D3D12_CPU_DESCRIPTOR_HANDLE			Scene::srvCPUDescriptorStartHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::srvGPUDescriptorStartHandle;
D3D12_CPU_DESCRIPTOR_HANDLE			Scene::uavCPUDescriptorStartHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::uavGPUDescriptorStartHandle;

D3D12_CPU_DESCRIPTOR_HANDLE			Scene::cbvCPUDescriptorNextHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::cbvGPUDescriptorNextHandle;
D3D12_CPU_DESCRIPTOR_HANDLE			Scene::srvCPUDescriptorNextHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::srvGPUDescriptorNextHandle;
D3D12_CPU_DESCRIPTOR_HANDLE			Scene::uavCPUDescriptorNextHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::uavGPUDescriptorNextHandle;

std::vector<SRVROOTARGUMENTINFO>	Scene::rootArgumentInfos;
int									Scene::rootArgumentCurrentIdx = 0;
int									Scene::mapData[3][3]{};	// 0 : 아직 가보지 않은 곳, 1 : 현재 있는 곳, 2 : 가본 곳
POINT								Scene::curMapPos{};

Scene::Scene(ID3D12Resource* depthStencilBuffer[COMMAND_IDX_NUM], ID3D12Resource* shadowMapBuffer[COMMAND_IDX_NUM], UINT cascadeNum)
{
	for (int i = 0; i < COMMAND_IDX_NUM; ++i)
	{
		this->shadowMapBuffer[i] = shadowMapBuffer[i];
		this->depthStencilBuffer[i] = depthStencilBuffer[i];
	}
	this->cascadeNum = cascadeNum;
	graphicsRootSignature = NULL;

	mapData[0][0] = 1;
}

Scene::~Scene()
{
}

void Scene::BuildLightsAndMaterials()
{
	lights = new LIGHTS;
	::ZeroMemory(lights, sizeof(LIGHTS));
	lights->globalAmbient = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	lights->lights[0].enable = true;
	lights->lights[0].type = POINT_LIGHT;
	lights->lights[0].range = 1000.0f;
	lights->lights[0].ambient = XMFLOAT4(0.1f, 0.0f, 0.0f, 1.0f);
	lights->lights[0].diffuse = XMFLOAT4(0.8f, 0.0f, 0.0f, 1.0f);
	lights->lights[0].specular = XMFLOAT4(0.1f, 0.1f, 0.1f, 0.0f);
	lights->lights[0].position = XMFLOAT3(130.0f, 300.0f, 30.0f);
	lights->lights[0].direction = XMFLOAT3(0.0f, 0.0f, 0.0f);
	lights->lights[0].attenuation = XMFLOAT3(1.0f, 0.001f, 0.0001f);

	lights->lights[1].enable = true;
	lights->lights[1].type = SPOT_LIGHT;
	lights->lights[1].range = 500.0f;
	lights->lights[1].ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	lights->lights[1].diffuse = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	lights->lights[1].specular = XMFLOAT4(0.2f, 0.2f, 0.2f, 0.0f);
	lights->lights[1].position = XMFLOAT3(-50.0f, 20.0f, -5.0f);
	lights->lights[1].direction = XMFLOAT3(0.0f, 0.0f, 1.0f);
	lights->lights[1].attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
	lights->lights[1].falloff = 8.0f;
	lights->lights[1].phi = (float)cos(XMConvertToRadians(40.0f));
	lights->lights[1].theta = (float)cos(XMConvertToRadians(20.0f));

	lights->lights[2].enable = true;
	lights->lights[2].type = DIRECTIONAL_LIGHT;
	lights->lights[2].ambient = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
	lights->lights[2].diffuse = XMFLOAT4(0.3f, 0.3f, 0.3f, 1.0f);
	lights->lights[2].specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	lights->lights[2].direction = XMFLOAT3(1.0f, -1.0f, 0.0f);

	lights->lights[3].enable = true;
	lights->lights[3].type = SPOT_LIGHT;
	lights->lights[3].range = 600.0f;
	lights->lights[3].ambient = XMFLOAT4(0.1f, 0.1f, 0.1f, 1.0f);
	lights->lights[3].diffuse = XMFLOAT4(0.5f, 0.0f, 0.0f, 1.0f);
	lights->lights[3].specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	lights->lights[3].position = XMFLOAT3(-150.0f, 30.0f, 30.0f);
	lights->lights[3].direction = XMFLOAT3(0.0f, 1.0f, 1.0f);
	lights->lights[3].attenuation = XMFLOAT3(1.0f, 0.01f, 0.0001f);
	lights->lights[3].falloff = 8.0f;
	lights->lights[3].phi = (float)cos(XMConvertToRadians(90.0f));
	lights->lights[3].theta = (float)cos(XMConvertToRadians(30.0f));

	materials = new MATERIALS;
	::ZeroMemory(materials, sizeof(MATERIALS));

	materials->reflections[0] = { XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 5.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[1] = { XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f), XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f), XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[2] = { XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 15.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[3] = { XMFLOAT4(0.5f, 0.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.5f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 20.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[4] = { XMFLOAT4(0.0f, 0.5f, 1.0f, 1.0f), XMFLOAT4(0.5f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 25.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[5] = { XMFLOAT4(0.0f, 0.5f, 0.5f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 30.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[6] = { XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f), XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 35.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[7] = { XMFLOAT4(1.0f, 0.5f, 1.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 40.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };

	for (int i = 0; i < materialsNum; ++i)
	{
		materials->reflections[8 + i] = materialReflection[i];
	}
}

void Scene::BuildUIINFO()
{
	uis = new UIINFOINCPU;
	::ZeroMemory(uis, sizeof(UIINFOINCPU));
	uis->scenenum = SCENE_START;
	uis->playerJob[0] = 0;
	uis->playerJob[1] = 0;
	uis->playerJob[2] = 0;
	uis->playerJob[3] = 0;
	uis->playerState[0] = 0;
	uis->playerState[1] = 0;
	uis->playerState[2] = 0;
	uis->playerState[3] = 0;
	uis->myid = 0;

	uis->accessory = 0;
	uis->weapon = 0 + (uis->playerJob[uis->myid] * 6);
	uis->nowspot = 0;
	uis->hp[0] = 1.0f;
	uis->hp[1] = 1.0f;
	uis->hp[2] = 1.0f;
	uis->hp[3] = 1.0f;

	uis->stemina = 0.7f;
	uis->isHead = false;
	uis->players = 0;
	uis->ammo = 0.7f;
	uis->newitem = 0.0f;

	// 안가본곳 0 , 지나감 1 , 현재 2
	for (int i = 0; i < 3; ++i)
		for (int j = 0; j < 3; ++j)
			uis->passedmap[i][j] = 0;
	uis->passedmap[0][0] = 2;
}

UIINFOINCPU* Scene::GetUIINFO()
{
	return uis;
}

void Scene::BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{

	graphicsRootSignature = CreateGraphicsRootSignature(device);
	computeRootSignature = CreateComputeRootSignature(device);
	objects.reserve(100);

	Material::PrepareShaders(device, commandList, graphicsRootSignature, computeRootSignature);

	GameObject* treeModel0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Nature/tree_aspen_medium_b.bin", "Nature");
	treeModel0->SetPipelineStatesNum(InstanceShaderPS_Grass);
	GameObject* watchTowerModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WatchTower/WatchTower.bin", "WatchTower");
	GameObject* farmerHouseModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/FarmHouse/FarmHouse.bin", "FarmHouse");
	GameObject* oldHouseModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/OldHouse/OldHouse.bin", "OldHouse");
	GameObject* concreteBuilding0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/ConcreteBuilding0/Building_Example_1.bin", "UnityPack0");
	GameObject* factoryModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Factory/factory.bin", "Factory");
	GameObject* redRoofModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/RedRoofHouse/RedRoofHouse.bin", "RedRoofHouse");
	GameObject* ruins0Model = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Ruins0/ruins.bin", "Ruins0");
	GameObject* wireFenceModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WireFence/wireFence.bin", "WireFence");
	wireFenceModel->SetPipelineStatesNum(InstanceShaderPS_Blend);
	GameObject* rustStoreModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/RustStoreHouse/rustStoreHouse.bin", "RustStoreHouse");
	GameObject* castleModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Castle/castle.bin", "Castle");
	GameObject* stoneModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Stone/Stone_Pack1_Stone_1.bin", "Stone");
	GameObject* treeModel1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Nature/tree_aspen_medium_b.bin", "Nature");
	treeModel1->SetPipelineStatesNum(InstanceShaderPS_Blend);
	GameObject* treeModel2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Nature/tree_aspen_small_b.bin", "Nature");
	treeModel2->SetPipelineStatesNum(InstanceShaderPS_Blend);
	GameObject* treeModel3 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Nature/TreeAbundantLeaf.bin", "Nature");
	treeModel3->SetPipelineStatesNum(InstanceShaderPS_Blend);
	GameObject* treeModel4 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Nature/TreeWithOutLeaf.bin", "Nature");
	GameObject* containerModel1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Containers_1A2.bin", "Container");
	GameObject* wareHouseModel = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WareHouse/wareHouse.bin", "WareHouse");
	GameObject* busModel = GameObject::LoadGeometryFromFile(device, commandList,  "Assets/Model/Car/Bus.bin", "Car");
	GameObject* militaryCar = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Car/MilitaryCar.bin", "Car");
	GameObject* redCar = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Car/RedCar.bin", "Car");

	// Unity pack
	GameObject* woodBeam0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WoodBeam/Wooden_Beam_1A1.bin", "UnityPack0");
	GameObject* woodBeam1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WoodBeam/Wooden_Beam_1A2.bin", "UnityPack0");
	GameObject* woodPallet = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WoodPallet/Pallet_Stack_1B2.bin", "UnityPack0");
	GameObject* woodPlatform0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WoodPlatform/Wooden_Platform_1A1.bin", "UnityPack0");
	GameObject* woodPlatform1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WoodPlatform/Wooden_Platform_1A3.bin", "UnityPack0");
	GameObject* woodStack0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WoodStack/Log_Stack_1A1.bin", "UnityPack0");
	GameObject* woodStack1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/WoodStack/Log_Stack_1A2.bin", "UnityPack0");
	GameObject* smallFence = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/SmallFence/Railing_Small_1A1.bin", "UnityPack0");
	GameObject* scaffold = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Scaffold/Scaffolding_1A2.bin", "UnityPack0");
	GameObject* brickStack0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Concrete/Brick_Stack_1A1.bin", "UnityPack0");
	GameObject* brickStack1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Concrete/Brick_Stack_1A2.bin", "UnityPack0");
	GameObject* brickStack2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Concrete/Brick_Stack_1A3.bin", "UnityPack0");
	GameObject* concreteBarrier = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Concrete/Concrete_Barrier_1A1.bin", "UnityPack0");
	GameObject* concreteBrickGroup0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Concrete/Concrete_Brick_Group_1A1.bin", "UnityPack0");
	GameObject* concreteBrickGroup1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Concrete/Concrete_Brick_Group_1A2.bin", "UnityPack0");
	GameObject* concreteBrickGroup2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Concrete/Concrete_Brick_Group_1A3.bin", "UnityPack0");
	GameObject* concreteBrickGroup3 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Concrete/Concrete_Brick_Group_1A4.bin", "UnityPack0");

	GameObject* brokenWall0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/SmallWall/Broken_Wall_1A1.bin", "UnityPack0");
	GameObject* brokenWall1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/SmallWall/Broken_Wall_1A2.bin", "UnityPack0");
	GameObject* brokenWall2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/SmallWall/Broken_Wall_1B1.bin", "UnityPack0");
	GameObject* brokenWall3 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/SmallWall/Broken_Wall_1B2.bin", "UnityPack0");
	GameObject* brokenWall4 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/SmallWall/Broken_Wall_1C1.bin", "UnityPack0");

	GameObject* cityWall0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/SmallWall/City_Wall_1A1.bin", "UnityPack0");
	GameObject* cityWall1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/SmallWall/City_Wall_1A2.bin", "UnityPack0");
	GameObject* cityWallPillar = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/SmallWall/City_Wall_Pillar_1A1.bin", "UnityPack0");

	GameObject* lamp = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Lamp/Light_1A1.bin", "UnityPack0");
	GameObject* tapestry0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Tapestry/Tapestry_1A1.bin", "UnityPack0");
	GameObject* tapestry1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Tapestry/Tapestry_1A2.bin", "UnityPack0");
	GameObject* awning0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Awning/Awning_1A1.bin", "UnityPack0");
	GameObject* awning1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Awning/Awning_1A2.bin", "UnityPack0");

	GameObject* containerClose0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Closed_1A1.bin", "UnityPack0");
	GameObject* containerClose1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Closed_1A2.bin", "UnityPack0");
	GameObject* containerClose2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Closed_1A3.bin", "UnityPack0");
	GameObject* containerClose3 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Closed_1A4.bin", "UnityPack0");
	GameObject* containerFill0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Filled_1A1.bin", "UnityPack0");
	GameObject* containerFill1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Filled_1A2.bin", "UnityPack0");
	GameObject* containerFill2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Filled_1A3.bin", "UnityPack0");
	GameObject* containerFill3 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Filled_1A4.bin", "UnityPack0");
	GameObject* containerOpen0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Open_1A1_2.bin", "UnityPack0");
	GameObject* containerOpen1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Open_1A2_2.bin", "UnityPack0");
	GameObject* containerOpen2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Open_1A3_2.bin", "UnityPack0");
	GameObject* containerOpen3 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Container/Container_Open_1A4_2.bin", "UnityPack0");

	GameObject* bigWallMain0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/BigWall/Fortification_Main_Gate_1A1.bin", "UnityPack0");
	GameObject* bigWallMain1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/BigWall/Fortification_Main_Gate_1A2.bin", "UnityPack0");
	GameObject* bigWallPillar0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/BigWall/Fortification_Pillar_1A1.bin", "UnityPack0");
	GameObject* bigWallPillar1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/BigWall/Fortification_Pillar_1A2.bin", "UnityPack0");
	GameObject* bigWallPillar2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/BigWall/Fortification_Pillar_1A3.bin", "UnityPack0");
	GameObject* bigWall0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/BigWall/Fortification_Wall_1A1.bin", "UnityPack0");
	GameObject* bigWall1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/BigWall/Fortification_Wall_1A2.bin", "UnityPack0");
	GameObject* tire = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Tire/Tire_1A1.bin", "UnityPack0");
	GameObject* barrel0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Barrel/Barrel_Group_1A1.bin", "UnityPack0");
	GameObject* barrel1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Barrel/Barrel_Group_1A2.bin", "UnityPack0");
	GameObject* barrel2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Barrel/Barrel_Group_1A3.bin", "UnityPack0");
	GameObject* barrelOpen = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Barrel/Barrel_Open_1A2.bin", "UnityPack0");

	GameObject* pipe0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Pipe/Concrete_Pipe_1A1.bin", "UnityPack0");
	GameObject* pipe1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Pipe/Concrete_Pipe_1B1.bin", "UnityPack0");

	GameObject* pipeGroup0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Pipe/Concrete_Pipe_Group_1A1.bin", "UnityPack0");
	GameObject* pipeGroup1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Pipe/Concrete_Pipe_Group_1B1.bin", "UnityPack0");
	GameObject* sign0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A1.bin", "UnityPack0");
	GameObject* sign1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A2.bin", "UnityPack0");
	GameObject* sign2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A3.bin", "UnityPack0");
	GameObject* sign3 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A4.bin", "UnityPack0");
	GameObject* sign4 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A5.bin", "UnityPack0");
	GameObject* sign5 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A6.bin", "UnityPack0");
	GameObject* sign6 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A7.bin", "UnityPack0");
	GameObject* sign7 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A8.bin", "UnityPack0");
	GameObject* sign8 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A9.bin", "UnityPack0");
	GameObject* sign9 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Sign_1A10.bin", "UnityPack0");

	GameObject* metalBorad0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Metal_Board_1A4.bin", "UnityPack0");
	GameObject* metalBorad1 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Metal_Board_1B1.bin", "UnityPack0");
	GameObject* metalBorad2 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Sign/Metal_Board_1B2.bin", "UnityPack0");
	GameObject* vent = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Vent/Vent_1A1.bin", "UnityPack0");

	// No Unity Pack 2
	GameObject* grass0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Grass/free_grass.bin", "Grass");
	grass0->SetPipelineStatesNum(InstanceShaderPS_Grass);
	GameObject* lamp0 = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Lamp/StreetLamp.bin", "Lamp");
	lamp0->useShadow = false;
	GameObject* arrow = GameObject::LoadGeometryFromFile(device, commandList, "Assets/Model/Human/Arrow.bin", "Human");

	// 스킨 메쉬
	LoadedModelInfo* rifleModel = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Human/6GunMan.bin", "Human");
	LoadedModelInfo* swordModel = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Human/6SwordMan.bin", "Human");
	LoadedModelInfo* bowModel = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Human/6BowGirl.bin", "Human");

	LoadedModelInfo* bigPoliceZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/CopZombie.bin", "Zombie");
	bigPoliceZombie->modelRootObject->hp = 100;
	bigPoliceZombie->modelRootObject->strikingPower = 20;
	bigPoliceZombie->modelRootObject->defencivePower = 15;
	LoadedModelInfo* derrickZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/DerrickZombie.bin", "Zombie");
	derrickZombie->modelRootObject->hp = 100;
	derrickZombie->modelRootObject->strikingPower = 15;
	derrickZombie->modelRootObject->defencivePower = 15;
	LoadedModelInfo* jillZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/JillZombie.bin", "Zombie");
	jillZombie->modelRootObject->hp = 100;
	jillZombie->modelRootObject->strikingPower = 15;
	jillZombie->modelRootObject->defencivePower = 10;
	LoadedModelInfo* starkieZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/ParasiteZombie.bin", "Zombie");
	starkieZombie->modelRootObject->hp = 100;
	starkieZombie->modelRootObject->strikingPower = 20;
	starkieZombie->modelRootObject->defencivePower = 5;
	LoadedModelInfo* womanZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/GirlZombie.bin", "Zombie");
	womanZombie->modelRootObject->hp = 100;
	womanZombie->modelRootObject->strikingPower = 10;
	womanZombie->modelRootObject->defencivePower = 15;
	LoadedModelInfo* pumpKinZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/HulkZombie.bin", "Zombie");
	pumpKinZombie->modelRootObject->hp = 100;
	pumpKinZombie->modelRootObject->strikingPower = 20;
	pumpKinZombie->modelRootObject->defencivePower = 70;
	LoadedModelInfo* warZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/WarZombie.bin", "Zombie");
	warZombie->modelRootObject->hp = 100;
	warZombie->modelRootObject->strikingPower = 20;
	warZombie->modelRootObject->defencivePower = 20;
	LoadedModelInfo* whiteclownZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/WhiteclownZombie.bin", "Zombie");
	whiteclownZombie->modelRootObject->hp = 100;
	whiteclownZombie->modelRootObject->strikingPower = 10;
	whiteclownZombie->modelRootObject->defencivePower = 20;
	LoadedModelInfo* igniteZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/YakuzaZombie.bin", "Zombie");
	igniteZombie->modelRootObject->hp = 100;
	igniteZombie->modelRootObject->strikingPower = 20;
	igniteZombie->modelRootObject->defencivePower = 10;
	LoadedModelInfo* baseZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/BaseZombie.bin", "Zombie");
	baseZombie->modelRootObject->hp = 100;
	baseZombie->modelRootObject->strikingPower = 10;
	baseZombie->modelRootObject->defencivePower = 10;
	LoadedModelInfo* baldZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/BaldZombie.bin", "Zombie");
	baldZombie->modelRootObject->hp = 100;
	baldZombie->modelRootObject->strikingPower = 10;
	baldZombie->modelRootObject->defencivePower = 10;
	LoadedModelInfo* bossZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, "Assets/Model/Zombie/Mutant.bin", "Zombie");
	bossZombie->modelRootObject->hp = 100;
	bossZombie->modelRootObject->strikingPower = 30;
	bossZombie->modelRootObject->defencivePower = 100;

	treeModel0->GetHierarchyMaterial(materialReflection, materialsNum);
	watchTowerModel->GetHierarchyMaterial(materialReflection, materialsNum);
	farmerHouseModel->GetHierarchyMaterial(materialReflection, materialsNum);
	oldHouseModel->GetHierarchyMaterial(materialReflection, materialsNum);
	concreteBuilding0->GetHierarchyMaterial(materialReflection, materialsNum);
	factoryModel->GetHierarchyMaterial(materialReflection, materialsNum);
	redRoofModel->GetHierarchyMaterial(materialReflection, materialsNum);
	ruins0Model->GetHierarchyMaterial(materialReflection, materialsNum);
	wireFenceModel->GetHierarchyMaterial(materialReflection, materialsNum);
	rustStoreModel->GetHierarchyMaterial(materialReflection, materialsNum);
	castleModel->GetHierarchyMaterial(materialReflection, materialsNum);
	stoneModel->GetHierarchyMaterial(materialReflection, materialsNum);
	treeModel1->GetHierarchyMaterial(materialReflection, materialsNum);
	treeModel2->GetHierarchyMaterial(materialReflection, materialsNum);
	treeModel3->GetHierarchyMaterial(materialReflection, materialsNum);
	treeModel4->GetHierarchyMaterial(materialReflection, materialsNum);
	containerModel1->GetHierarchyMaterial(materialReflection, materialsNum);
	wareHouseModel->GetHierarchyMaterial(materialReflection, materialsNum);
	busModel->GetHierarchyMaterial(materialReflection, materialsNum);
	militaryCar->GetHierarchyMaterial(materialReflection, materialsNum);
	redCar->GetHierarchyMaterial(materialReflection, materialsNum);

	woodBeam0->GetHierarchyMaterial(materialReflection, materialsNum);
	woodBeam1->GetHierarchyMaterial(materialReflection, materialsNum);
	woodPallet->GetHierarchyMaterial(materialReflection, materialsNum);
	woodPlatform0->GetHierarchyMaterial(materialReflection, materialsNum);
	woodPlatform1->GetHierarchyMaterial(materialReflection, materialsNum);
	woodStack0->GetHierarchyMaterial(materialReflection, materialsNum);
	woodStack1->GetHierarchyMaterial(materialReflection, materialsNum);
	smallFence->GetHierarchyMaterial(materialReflection, materialsNum);
	scaffold->GetHierarchyMaterial(materialReflection, materialsNum);
	brickStack0->GetHierarchyMaterial(materialReflection, materialsNum);
	brickStack1->GetHierarchyMaterial(materialReflection, materialsNum);
	brickStack2->GetHierarchyMaterial(materialReflection, materialsNum);
	concreteBarrier->GetHierarchyMaterial(materialReflection, materialsNum);
	concreteBrickGroup0->GetHierarchyMaterial(materialReflection, materialsNum);
	concreteBrickGroup1->GetHierarchyMaterial(materialReflection, materialsNum);
	concreteBrickGroup2->GetHierarchyMaterial(materialReflection, materialsNum);
	concreteBrickGroup3->GetHierarchyMaterial(materialReflection, materialsNum);

	brokenWall0->GetHierarchyMaterial(materialReflection, materialsNum);
	brokenWall1->GetHierarchyMaterial(materialReflection, materialsNum);
	brokenWall2->GetHierarchyMaterial(materialReflection, materialsNum);
	brokenWall3->GetHierarchyMaterial(materialReflection, materialsNum);
	brokenWall4->GetHierarchyMaterial(materialReflection, materialsNum);
	cityWall0->GetHierarchyMaterial(materialReflection, materialsNum);
	cityWall1->GetHierarchyMaterial(materialReflection, materialsNum);
	cityWallPillar->GetHierarchyMaterial(materialReflection, materialsNum);

	lamp->GetHierarchyMaterial(materialReflection, materialsNum);
	tapestry0->GetHierarchyMaterial(materialReflection, materialsNum);
	tapestry1->GetHierarchyMaterial(materialReflection, materialsNum);
	awning0->GetHierarchyMaterial(materialReflection, materialsNum);
	awning1->GetHierarchyMaterial(materialReflection, materialsNum);
	containerClose0->GetHierarchyMaterial(materialReflection, materialsNum);
	containerClose1->GetHierarchyMaterial(materialReflection, materialsNum);
	containerClose2->GetHierarchyMaterial(materialReflection, materialsNum);
	containerClose3->GetHierarchyMaterial(materialReflection, materialsNum);
	containerFill0->GetHierarchyMaterial(materialReflection, materialsNum);
	containerFill1->GetHierarchyMaterial(materialReflection, materialsNum);
	containerFill2->GetHierarchyMaterial(materialReflection, materialsNum);
	containerFill3->GetHierarchyMaterial(materialReflection, materialsNum);
	containerOpen0->GetHierarchyMaterial(materialReflection, materialsNum);
	containerOpen1->GetHierarchyMaterial(materialReflection, materialsNum);
	containerOpen2->GetHierarchyMaterial(materialReflection, materialsNum);
	containerOpen3->GetHierarchyMaterial(materialReflection, materialsNum);
	bigWallMain0->GetHierarchyMaterial(materialReflection, materialsNum);
	bigWallMain1->GetHierarchyMaterial(materialReflection, materialsNum);
	bigWallPillar0->GetHierarchyMaterial(materialReflection, materialsNum);
	bigWallPillar1->GetHierarchyMaterial(materialReflection, materialsNum);
	bigWallPillar2->GetHierarchyMaterial(materialReflection, materialsNum);
	bigWall0->GetHierarchyMaterial(materialReflection, materialsNum);
	bigWall1->GetHierarchyMaterial(materialReflection, materialsNum);
	tire->GetHierarchyMaterial(materialReflection, materialsNum);
	barrel0->GetHierarchyMaterial(materialReflection, materialsNum);
	barrel1->GetHierarchyMaterial(materialReflection, materialsNum);
	barrel2->GetHierarchyMaterial(materialReflection, materialsNum);
	barrelOpen->GetHierarchyMaterial(materialReflection, materialsNum);
	pipe0->GetHierarchyMaterial(materialReflection, materialsNum);
	pipe1->GetHierarchyMaterial(materialReflection, materialsNum);
	pipeGroup0->GetHierarchyMaterial(materialReflection, materialsNum);
	pipeGroup1->GetHierarchyMaterial(materialReflection, materialsNum);
	sign0->GetHierarchyMaterial(materialReflection, materialsNum);
	sign1->GetHierarchyMaterial(materialReflection, materialsNum);
	sign2->GetHierarchyMaterial(materialReflection, materialsNum);
	sign3->GetHierarchyMaterial(materialReflection, materialsNum);
	sign4->GetHierarchyMaterial(materialReflection, materialsNum);
	sign5->GetHierarchyMaterial(materialReflection, materialsNum);
	sign6->GetHierarchyMaterial(materialReflection, materialsNum);
	sign7->GetHierarchyMaterial(materialReflection, materialsNum);
	sign8->GetHierarchyMaterial(materialReflection, materialsNum);
	sign9->GetHierarchyMaterial(materialReflection, materialsNum);
	metalBorad0->GetHierarchyMaterial(materialReflection, materialsNum);
	metalBorad1->GetHierarchyMaterial(materialReflection, materialsNum);
	metalBorad2->GetHierarchyMaterial(materialReflection, materialsNum);
	vent->GetHierarchyMaterial(materialReflection, materialsNum);

	// NO Unity Pack
	grass0->GetHierarchyMaterial(materialReflection, materialsNum);
	lamp0->GetHierarchyMaterial(materialReflection, materialsNum);
	arrow->GetHierarchyMaterial(materialReflection, materialsNum);

	// Skinned Model
	rifleModel->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	swordModel->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	bowModel->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);

	bigPoliceZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	derrickZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	jillZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	starkieZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	womanZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	pumpKinZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	warZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	whiteclownZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	igniteZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	baseZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	baldZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);
	bossZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum);

	int terrainObjectsNum = 1;
	int skyboxNum = 1;
	int uiNumber = 10;
	// 인스턴싱하지 않고 오브젝트를 렌더링할 때 사용되는 CBV의 갯수

	int terrainTexturesNum = 20;
	int cubeTexturesNum = 2;
	int objectTexturesNum = 300;
	int billboardTexturesNum = 1;
	int particleTexutresNum = 2;
	int decalTextureNum = 1;
	int skyBoxtextureNum = 18;
	int uiTextureNum = 100;

	objectsNum = terrainObjectsNum + skyboxNum + uiNumber;
	texturesNum = terrainTexturesNum + cubeTexturesNum + objectTexturesNum + billboardTexturesNum + particleTexutresNum + decalTextureNum + skyBoxtextureNum + uiTextureNum;

	CreateCbvSrvUavDescriptorHeaps(device, commandList, objectsNum, texturesNum, 0);
	CreateTextureResource(device, commandList);
	// 게임에 사용되는 텍스쳐들을 모두 Load하는 함수

	shaders.reserve(6);

	TerrainShader* tShader = new TerrainShader();
	tShader->CreateShader(device, graphicsRootSignature);
	tShader->BuildObjects(device, commandList, NULL);
	shaders.emplace_back(tShader);
	// Terrain Build
	terrain = (HeightMapTerrain*)tShader->GetObjects(0);
	POINT terrainSize{ 257.0f , 257.0f };
	XMFLOAT3 terrainScale{ 2.0f , 2.0f , 2.0f };

	terrain->SetPosition((-terrainSize.x * terrainScale.x) / 2, 0, (-terrainSize.y * terrainScale.z) / 2);
	terrain->SetBoundingTransform();
	//terrain Object Get

	objects.emplace_back(terrain);
	objects.emplace_back(treeModel0);
	objects.emplace_back(watchTowerModel);
	objects.emplace_back(farmerHouseModel);
	objects.emplace_back(oldHouseModel);
	objects.emplace_back(concreteBuilding0);
	objects.emplace_back(factoryModel);
	objects.emplace_back(redRoofModel);
	objects.emplace_back(ruins0Model);
	objects.emplace_back(wireFenceModel);
	objects.emplace_back(rustStoreModel);
	objects.emplace_back(castleModel);
	objects.emplace_back(stoneModel);
	objects.emplace_back(treeModel1);
	objects.emplace_back(treeModel2);
	objects.emplace_back(treeModel3);
	objects.emplace_back(treeModel4);
	objects.emplace_back(containerModel1);
	objects.emplace_back(wareHouseModel);
	objects.emplace_back(busModel);
	objects.emplace_back(militaryCar);
	objects.emplace_back(redCar);

	objects.emplace_back(woodBeam0);
	objects.emplace_back(woodBeam1);
	objects.emplace_back(woodPallet);
	objects.emplace_back(woodPlatform0);
	objects.emplace_back(woodPlatform1);
	objects.emplace_back(woodStack0);
	objects.emplace_back(woodStack1);
	objects.emplace_back(smallFence);
	objects.emplace_back(scaffold);
	objects.emplace_back(brickStack0);
	objects.emplace_back(brickStack1);
	objects.emplace_back(brickStack2);
	objects.emplace_back(concreteBarrier);
	objects.emplace_back(concreteBrickGroup0);
	objects.emplace_back(concreteBrickGroup1);
	objects.emplace_back(concreteBrickGroup2);
	objects.emplace_back(concreteBrickGroup3);
	objects.emplace_back(brokenWall0);
	objects.emplace_back(brokenWall1);
	objects.emplace_back(brokenWall2);
	objects.emplace_back(brokenWall3);
	objects.emplace_back(brokenWall4);
	objects.emplace_back(cityWall0);
	objects.emplace_back(cityWall1);
	objects.emplace_back(cityWallPillar);

	objects.emplace_back(lamp);
	objects.emplace_back(tapestry0);
	objects.emplace_back(tapestry1);
	objects.emplace_back(awning0);
	objects.emplace_back(awning1);
	objects.emplace_back(containerClose0);
	objects.emplace_back(containerClose1);
	objects.emplace_back(containerClose2);
	objects.emplace_back(containerClose3);
	objects.emplace_back(containerFill0);
	objects.emplace_back(containerFill1);
	objects.emplace_back(containerFill2);
	objects.emplace_back(containerFill3);
	objects.emplace_back(containerOpen0);
	objects.emplace_back(containerOpen1);
	objects.emplace_back(containerOpen2);
	objects.emplace_back(containerOpen3);
	objects.emplace_back(bigWallMain0);
	objects.emplace_back(bigWallMain1);
	objects.emplace_back(bigWallPillar0);
	objects.emplace_back(bigWallPillar1);
	objects.emplace_back(bigWallPillar2);
	objects.emplace_back(bigWall0);
	objects.emplace_back(bigWall1);

	objects.emplace_back(tire);
	objects.emplace_back(barrel0);
	objects.emplace_back(barrel1);
	objects.emplace_back(barrel2);
	objects.emplace_back(barrelOpen);
	objects.emplace_back(pipe0);
	objects.emplace_back(pipe1);
	objects.emplace_back(pipeGroup0);
	objects.emplace_back(pipeGroup1);
	objects.emplace_back(sign0);
	objects.emplace_back(sign1);
	objects.emplace_back(sign2);
	objects.emplace_back(sign3);
	objects.emplace_back(sign4);
	objects.emplace_back(sign5);
	objects.emplace_back(sign6);
	objects.emplace_back(sign7);
	objects.emplace_back(sign8);
	objects.emplace_back(sign9);
	objects.emplace_back(metalBorad0);
	objects.emplace_back(metalBorad1);
	objects.emplace_back(metalBorad2);
	objects.emplace_back(vent);

	// NO Unity Pack2
	objects.emplace_back(grass0);
	objects.emplace_back(lamp0);
	objects.emplace_back(arrow);

	hObjectSize = objects.size();

	objects.emplace_back(rifleModel);
	objects.emplace_back(swordModel);
	objects.emplace_back(bowModel);

	objects.emplace_back(bigPoliceZombie);
	objects.emplace_back(derrickZombie);
	objects.emplace_back(jillZombie);
	objects.emplace_back(starkieZombie);
	objects.emplace_back(womanZombie);
	objects.emplace_back(pumpKinZombie);
	middleBossIdx = objects.size() - 1;

	objects.emplace_back(warZombie);
	objects.emplace_back(whiteclownZombie);
	objects.emplace_back(igniteZombie);
	objects.emplace_back(baseZombie);
	objects.emplace_back(baldZombie);
	objects.emplace_back(bossZombie);
	finalBossIdx = objects.size() - 1;

	Shader* insShader = new InstancingShader();
	insShader->CreateShader(device, graphicsRootSignature);
	insShader->BuildObjects(device, commandList, graphicsRootSignature, objects, hObjectSize);
	shaders.emplace_back(insShader);

	Shader* skinInsShader = new SkinnedInstancingShader();
	skinInsShader->CreateShader(device, graphicsRootSignature, computeRootSignature);
	skinInsShader->BuildObjects(device, commandList, graphicsRootSignature, objects, hObjectSize);
	reinterpret_cast<SkinnedInstancingShader*>(skinInsShader)->middleBossIdx = middleBossIdx - hObjectSize;
	reinterpret_cast<SkinnedInstancingShader*>(skinInsShader)->finalBossIdx = finalBossIdx - hObjectSize;
	shaders.emplace_back(skinInsShader);

	Shader* skinShader = new SkinnedAnimationObjectsShader();
	skinShader->CreateShader(device, graphicsRootSignature, computeRootSignature);
	skinShader->BuildObjects(device, commandList, graphicsRootSignature, objects, hObjectSize);
	shaders.emplace_back(skinShader);

	Shader* pShader = new ParticleShader();
	pShader->CreateShader(device, graphicsRootSignature, computeRootSignature);
	reinterpret_cast<ParticleShader*>(pShader)->textureMap = particleTextureMap;
	pShader->BuildObjects(device, commandList, graphicsRootSignature, objects);
	shaders.emplace_back(pShader);
	reinterpret_cast<SkinnedInstancingShader*>(shaders[ShaderOrderSkinnedInstancing])->SetParticleShader(reinterpret_cast<ParticleShader*>(pShader));

	Shader* wtShader = new WeaponTrailerShader();
	wtShader->CreateShader(device, graphicsRootSignature);
	wtShader->BuildObjects(device, commandList, graphicsRootSignature, NULL);
	shaders.emplace_back(wtShader);
	reinterpret_cast<InstancingShader*>(shaders[ShaderOrderInstancing])->SetWTShader(reinterpret_cast<WeaponTrailerShader*>(wtShader));

	Shader* skyBoxShader = new SkyBoxShader();
	skyBoxShader->CreateShader(device, graphicsRootSignature);
	skyBoxShader->BuildObjects(device, commandList, graphicsRootSignature, NULL);
	shaders.emplace_back(skyBoxShader);

	Shader* uiShader = new UIShader();
	uiShader->CreateShader(device, graphicsRootSignature);
	uiShader->BuildObjects(device, commandList, graphicsRootSignature, NULL);
	shaders.emplace_back(uiShader);

	Shader* miniMapShader = new MiniMapShader();
	miniMapShader->CreateShader(device, graphicsRootSignature);
	miniMapShader->BuildObjects(device, commandList, graphicsRootSignature, NULL);
	shaders.emplace_back(miniMapShader);

	BuildLightsAndMaterials();
	BuildUIINFO();
	CreateShaderVariables(device, commandList);
}

void Scene::ReleaseObjects()
{
	if (graphicsRootSignature) graphicsRootSignature->Release();
	if (computeRootSignature) computeRootSignature->Release();
	if (cbvSrvUavDescriptorHeap) cbvSrvUavDescriptorHeap->Release();

	if (!shaders.empty())
	{
		for (int i = 0; i < shaders.size(); i++)
		{
			if (shaders[i])
			{
				shaders[i]->ReleaseObjects();
				shaders[i]->Release();
			}
		}
	}

	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->Release();
	}

	if (lights) delete lights;
	if (materials) delete materials;
}

void Scene::ReleaseUploadBuffers()
{
	if (!shaders.empty())
	{
		for (int i = 0; i < shaders.size(); i++)
		{
			if (shaders[i])
			{
				shaders[i]->ReleaseUploadBuffers();
			}
		}
	}

	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->ReleaseUploadBuffers();
	}

	if (terrain) terrain->ReleaseUploadBuffers();
	if (materialUploadBuffer) materialUploadBuffer->Release();
	if (fogUploadBuffer) fogUploadBuffer->Release();
}

ID3D12RootSignature *Scene::GetGraphicsRootSignature()
{
	return(graphicsRootSignature);
}

ID3D12RootSignature *Scene::GetComputeRootSignature()
{
	return(computeRootSignature);
}

ID3D12RootSignature *Scene::CreateGraphicsRootSignature(ID3D12Device* device)
{
	ID3D12RootSignature* graphicsRootSignature = NULL;

	D3D12_DESCRIPTOR_RANGE descriptorRanges[14];

	descriptorRanges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	descriptorRanges[0].NumDescriptors = 1;
	descriptorRanges[0].BaseShaderRegister = 2; // b2 : GameObjectInfo
	descriptorRanges[0].RegisterSpace = 0;
	descriptorRanges[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[1].NumDescriptors = 12;
	descriptorRanges[1].BaseShaderRegister = 0; // t0~11 : gtxtTerrainDetailTexture
	descriptorRanges[1].RegisterSpace = 0;
	descriptorRanges[1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[2].NumDescriptors = 12;
	descriptorRanges[2].BaseShaderRegister = 12; // t12~23: gtxtTerrainNormalTexture
	descriptorRanges[2].RegisterSpace = 0;
	descriptorRanges[2].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[3].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[3].NumDescriptors = 3;
	descriptorRanges[3].BaseShaderRegister = 24; // t24~26: gtxtTerrainAlphaTexture
	descriptorRanges[3].RegisterSpace = 0;
	descriptorRanges[3].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[4].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[4].NumDescriptors = 10;
	descriptorRanges[4].BaseShaderRegister = 27; // t27~36: gtxtDiffuseTexture
	descriptorRanges[4].RegisterSpace = 0;
	descriptorRanges[4].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[5].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[5].NumDescriptors = 10;
	descriptorRanges[5].BaseShaderRegister = 37; // t37~46: gtxtNormalTexture
	descriptorRanges[5].RegisterSpace = 0;
	descriptorRanges[5].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[6].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[6].NumDescriptors = 10;
	descriptorRanges[6].BaseShaderRegister = 47; // t47~56: gtxtSpecularTexture
	descriptorRanges[6].RegisterSpace = 0;
	descriptorRanges[6].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[7].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[7].NumDescriptors = 10;
	descriptorRanges[7].BaseShaderRegister = 57; // t57~66: gtxtMetallicTexture
	descriptorRanges[7].RegisterSpace = 0;
	descriptorRanges[7].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[8].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[8].NumDescriptors = 10;
	descriptorRanges[8].BaseShaderRegister = 67; // t67~76: gtxtBillboardTexture
	descriptorRanges[8].RegisterSpace = 0;
	descriptorRanges[8].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[9].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[9].NumDescriptors = 10;
	descriptorRanges[9].BaseShaderRegister = 77; // t77~86: gtxtParticleTexture
	descriptorRanges[9].RegisterSpace = 0;
	descriptorRanges[9].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[10].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[10].NumDescriptors = 2;
	descriptorRanges[10].BaseShaderRegister = 87; // t87~88: gtxtDepthTexture
	descriptorRanges[10].RegisterSpace = 0;
	descriptorRanges[10].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[11].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[11].NumDescriptors = 2;
	descriptorRanges[11].BaseShaderRegister = 89; // t89~90: gtxtShadowMapTexture
	descriptorRanges[11].RegisterSpace = 0;
	descriptorRanges[11].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[12].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[12].NumDescriptors = 1;
	descriptorRanges[12].BaseShaderRegister = 91; // t91 gtxtSkyBoxTexture
	descriptorRanges[12].RegisterSpace = 0;
	descriptorRanges[12].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[13].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[13].NumDescriptors = 88;
	descriptorRanges[13].BaseShaderRegister = 110; // t110 gtxtUiTexture
	descriptorRanges[13].RegisterSpace = 0;
	descriptorRanges[13].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	// t92 부터 사용
	// t100 ~ t108 까지는 Structured Buffer로 사용중
	// t110 ~ 164 까지 UI 텍스쳐 사용 중


	D3D12_ROOT_PARAMETER rootParameters[GraphicsRootParametersNum];

	rootParameters[GraphicsRootPlayer].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[GraphicsRootPlayer].Descriptor.ShaderRegister = 0;	// b0: Player
	rootParameters[GraphicsRootPlayer].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootPlayer].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootCamera].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[GraphicsRootCamera].Descriptor.ShaderRegister = 1;	// b1: Camera
	rootParameters[GraphicsRootCamera].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootCamera].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootGameobject].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootGameobject].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootGameobject].DescriptorTable.pDescriptorRanges = &descriptorRanges[0];	// b2: GameObjectInfo
	rootParameters[GraphicsRootGameobject].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootMaterialInfo].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[GraphicsRootMaterialInfo].Constants.Num32BitValues = 3;
	rootParameters[GraphicsRootMaterialInfo].Constants.ShaderRegister = 3;		//  b3: MaterialInfo
	rootParameters[GraphicsRootMaterialInfo].Constants.RegisterSpace = 0;
	rootParameters[GraphicsRootMaterialInfo].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootTimeInfo].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[GraphicsRootTimeInfo].Constants.Num32BitValues = 3;
	rootParameters[GraphicsRootTimeInfo].Constants.ShaderRegister = 4;		//  b4: TimeInfo
	rootParameters[GraphicsRootTimeInfo].Constants.RegisterSpace = 0;
	rootParameters[GraphicsRootTimeInfo].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootMaterials].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[GraphicsRootMaterials].Descriptor.ShaderRegister = 5;	// b5: Materials
	rootParameters[GraphicsRootMaterials].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootMaterials].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootLights].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[GraphicsRootLights].Descriptor.ShaderRegister = 6;	// b6: Lights
	rootParameters[GraphicsRootLights].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootLights].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootParticleAge].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[GraphicsRootParticleAge].Constants.Num32BitValues = 2;
	rootParameters[GraphicsRootParticleAge].Constants.ShaderRegister = 7;		//  b7: particleAge
	rootParameters[GraphicsRootParticleAge].Constants.RegisterSpace = 0;
	rootParameters[GraphicsRootParticleAge].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootLightCamera].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[GraphicsRootLightCamera].Descriptor.ShaderRegister = 8;	// b8: LightCamera
	rootParameters[GraphicsRootLightCamera].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootLightCamera].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootSkinnedVerticesNum].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[GraphicsRootSkinnedVerticesNum].Constants.Num32BitValues = 1;
	rootParameters[GraphicsRootSkinnedVerticesNum].Constants.ShaderRegister = 9;		//  b9 VerticesNum
	rootParameters[GraphicsRootSkinnedVerticesNum].Constants.RegisterSpace = 0;
	rootParameters[GraphicsRootSkinnedVerticesNum].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootFogInfo].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[GraphicsRootFogInfo].Descriptor.ShaderRegister = 10;	// b10: FongInfo
	rootParameters[GraphicsRootFogInfo].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootFogInfo].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootUiInfo].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[GraphicsRootUiInfo].Descriptor.ShaderRegister = 11;	// b11: UiInfo
	rootParameters[GraphicsRootUiInfo].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootUiInfo].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootTerrainTexture].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootTerrainTexture].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootTerrainTexture].DescriptorTable.pDescriptorRanges = &descriptorRanges[1]; // t0: gtxtTerrainDetailTexture
	rootParameters[GraphicsRootTerrainTexture].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootTerrainNormalTexture].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootTerrainNormalTexture].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootTerrainNormalTexture].DescriptorTable.pDescriptorRanges = &descriptorRanges[2]; // t12: gtxtTerrainNormalTexture
	rootParameters[GraphicsRootTerrainNormalTexture].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootTerrainAlphaTexture].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootTerrainAlphaTexture].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootTerrainAlphaTexture].DescriptorTable.pDescriptorRanges = &descriptorRanges[3]; // t24: gtxtTerrainNormalTexture
	rootParameters[GraphicsRootTerrainAlphaTexture].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootDiffuseTextures].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootDiffuseTextures].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootDiffuseTextures].DescriptorTable.pDescriptorRanges = &descriptorRanges[4]; // t26: gtxtDiffuseTexture
	rootParameters[GraphicsRootDiffuseTextures].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootNormalTextures].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootNormalTextures].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootNormalTextures].DescriptorTable.pDescriptorRanges = &descriptorRanges[5]; // t36: gtxtNormalTexture
	rootParameters[GraphicsRootNormalTextures].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootSpecularTextures].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootSpecularTextures].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootSpecularTextures].DescriptorTable.pDescriptorRanges = &descriptorRanges[6]; // t46: gtxtSpecularTexture
	rootParameters[GraphicsRootSpecularTextures].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootMetallicTextures].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootMetallicTextures].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootMetallicTextures].DescriptorTable.pDescriptorRanges = &descriptorRanges[7]; // t56: gtxtMetallicTexture
	rootParameters[GraphicsRootMetallicTextures].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootBillboardTextures].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootBillboardTextures].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootBillboardTextures].DescriptorTable.pDescriptorRanges = &descriptorRanges[8]; // t66: gtxtBillboardTexture
	rootParameters[GraphicsRootBillboardTextures].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootParticleTextures].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootParticleTextures].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootParticleTextures].DescriptorTable.pDescriptorRanges = &descriptorRanges[9]; // t76: gtxtParticleTexture
	rootParameters[GraphicsRootParticleTextures].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootDepthTexture].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootDepthTexture].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootDepthTexture].DescriptorTable.pDescriptorRanges = &descriptorRanges[10]; // t86: gtxtDepthTexture
	rootParameters[GraphicsRootDepthTexture].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootShadowTexture].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootShadowTexture].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootShadowTexture].DescriptorTable.pDescriptorRanges = &descriptorRanges[11]; // t88: gtxtShadowMapTexture
	rootParameters[GraphicsRootShadowTexture].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootSkyBoxTexture].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootSkyBoxTexture].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootSkyBoxTexture].DescriptorTable.pDescriptorRanges = &descriptorRanges[12]; // t90: gtxtSkyBoxTexture
	rootParameters[GraphicsRootSkyBoxTexture].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootUiTexture].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[GraphicsRootUiTexture].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[GraphicsRootUiTexture].DescriptorTable.pDescriptorRanges = &descriptorRanges[13]; // t91: gtxtUiTexture
	rootParameters[GraphicsRootUiTexture].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[GraphicsRootParticleInfo].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[GraphicsRootParticleInfo].Descriptor.ShaderRegister = 100;	// t100: particleData
	rootParameters[GraphicsRootParticleInfo].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootParticleInfo].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootSkinnedPositionWorld].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[GraphicsRootSkinnedPositionWorld].Descriptor.ShaderRegister = 101;	// t101: SkinnedPositionWorld
	rootParameters[GraphicsRootSkinnedPositionWorld].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootSkinnedPositionWorld].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootSkinnedNormalWorld].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[GraphicsRootSkinnedNormalWorld].Descriptor.ShaderRegister = 102;	// t102: SkinnedPositionWorld
	rootParameters[GraphicsRootSkinnedNormalWorld].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootSkinnedNormalWorld].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootSkinnedTangentWorld].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[GraphicsRootSkinnedTangentWorld].Descriptor.ShaderRegister = 103;	// t103: SkinnedPositionWorld
	rootParameters[GraphicsRootSkinnedTangentWorld].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootSkinnedTangentWorld].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[GraphicsRootSkinnedBiTangentWorld].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[GraphicsRootSkinnedBiTangentWorld].Descriptor.ShaderRegister = 104;	// t104: SkinnedPositionWorld
	rootParameters[GraphicsRootSkinnedBiTangentWorld].Descriptor.RegisterSpace = 0;
	rootParameters[GraphicsRootSkinnedBiTangentWorld].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	D3D12_STATIC_SAMPLER_DESC samplerDesc[5]{};

	// 지형에 D3D12_FILTER_ANISOTROPIC 필터링을 사용해보자.
	samplerDesc[0].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[0].MipLODBias = 0;
	samplerDesc[0].MaxAnisotropy = 1;
	samplerDesc[0].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	samplerDesc[0].MinLOD = 0;
	samplerDesc[0].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[0].ShaderRegister = 0;
	samplerDesc[0].RegisterSpace = 0;
	samplerDesc[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	samplerDesc[1].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc[1].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[1].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[1].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[1].MipLODBias = 0;
	samplerDesc[1].MaxAnisotropy = 1;
	samplerDesc[1].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	samplerDesc[1].MinLOD = 0;
	samplerDesc[1].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[1].ShaderRegister = 1;
	samplerDesc[1].RegisterSpace = 0;
	samplerDesc[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	samplerDesc[2].Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
	samplerDesc[2].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[2].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[2].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[2].MipLODBias = 0;
	samplerDesc[2].MaxAnisotropy = 1;
	samplerDesc[2].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	samplerDesc[2].MinLOD = 0;
	samplerDesc[2].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[2].ShaderRegister = 2;
	samplerDesc[2].RegisterSpace = 0;
	samplerDesc[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	samplerDesc[3].Filter = D3D12_FILTER_COMPARISON_MIN_MAG_LINEAR_MIP_POINT;
	samplerDesc[3].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[3].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[3].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[3].MipLODBias = 0;
	samplerDesc[3].MaxAnisotropy = 1;
	samplerDesc[3].ComparisonFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
	samplerDesc[3].MinLOD = 0;
	samplerDesc[3].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[3].ShaderRegister = 3;
	samplerDesc[3].RegisterSpace = 0;
	samplerDesc[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	samplerDesc[4].Filter = D3D12_FILTER_ANISOTROPIC;
	samplerDesc[4].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[4].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[4].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[4].MipLODBias = 0;
	samplerDesc[4].MaxAnisotropy = 1;
	samplerDesc[4].ComparisonFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
	samplerDesc[4].MinLOD = 0;
	samplerDesc[4].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[4].ShaderRegister = 4;
	samplerDesc[4].RegisterSpace = 0;
	samplerDesc[4].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;


	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS;

	D3D12_ROOT_SIGNATURE_DESC rootSignatureDesc;
	::ZeroMemory(&rootSignatureDesc, sizeof(D3D12_ROOT_SIGNATURE_DESC));
	rootSignatureDesc.NumParameters = _countof(rootParameters);
	rootSignatureDesc.pParameters = rootParameters;
	rootSignatureDesc.NumStaticSamplers = _countof(samplerDesc);
	rootSignatureDesc.pStaticSamplers = samplerDesc;
	rootSignatureDesc.Flags = rootSignatureFlags;

	ID3DBlob *signatureBlob = NULL;
	ID3DBlob *errorBlob = NULL;
	HRESULT hResult = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signatureBlob, &errorBlob);
	hResult = device->CreateRootSignature(0, signatureBlob->GetBufferPointer(),
		signatureBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void**)&graphicsRootSignature);
	if (signatureBlob) signatureBlob->Release();
	if (errorBlob) errorBlob->Release();

	return(graphicsRootSignature);
}

ID3D12RootSignature * Scene::CreateComputeRootSignature(ID3D12Device * device)
{
	ID3D12RootSignature* computeRootSignature = NULL;

	D3D12_ROOT_PARAMETER rootParameters[ComputeRootParametersNum];

	rootParameters[ComputeRootTimeInfo].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[ComputeRootTimeInfo].Constants.Num32BitValues = 2;
	rootParameters[ComputeRootTimeInfo].Constants.ShaderRegister = 0;		//  b0: TimeInfo
	rootParameters[ComputeRootTimeInfo].Constants.RegisterSpace = 0;
	rootParameters[ComputeRootTimeInfo].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootParticleInfo].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[ComputeRootParticleInfo].Constants.Num32BitValues = 7;
	rootParameters[ComputeRootParticleInfo].Constants.ShaderRegister = 1;		//  b1: ParticleInfo
	rootParameters[ComputeRootParticleInfo].Constants.RegisterSpace = 0;
	rootParameters[ComputeRootParticleInfo].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootParticleInfoSRV].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[ComputeRootParticleInfoSRV].Descriptor.ShaderRegister = 0;	//	t0: particleData
	rootParameters[ComputeRootParticleInfoSRV].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootParticleInfoSRV].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootParticleCopySRV].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[ComputeRootParticleCopySRV].Descriptor.ShaderRegister = 1;	//	t1: copyData
	rootParameters[ComputeRootParticleCopySRV].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootParticleCopySRV].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootParticleInfoUAV].ParameterType = D3D12_ROOT_PARAMETER_TYPE_UAV;
	rootParameters[ComputeRootParticleInfoUAV].Descriptor.ShaderRegister = 0;	//	u0: particleData
	rootParameters[ComputeRootParticleInfoUAV].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootParticleInfoUAV].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootCamera].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[ComputeRootCamera].Descriptor.ShaderRegister = 2;	// b2: Camera
	rootParameters[ComputeRootCamera].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootCamera].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootParticleSortInfo].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[ComputeRootParticleSortInfo].Constants.Num32BitValues = 1;
	rootParameters[ComputeRootParticleSortInfo].Constants.ShaderRegister = 3;		//  b3: particleSortInfo
	rootParameters[ComputeRootParticleSortInfo].Constants.RegisterSpace = 0;
	rootParameters[ComputeRootParticleSortInfo].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootBoneOffset].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[ComputeRootBoneOffset].Descriptor.ShaderRegister = 4;	// b4: BoneOFfset
	rootParameters[ComputeRootBoneOffset].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootBoneOffset].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootBoneTransform].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[ComputeRootBoneTransform].Descriptor.ShaderRegister = 2;	//	t2: SkinnedPosition
	rootParameters[ComputeRootBoneTransform].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootBoneTransform].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedPosition].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[ComputeRootSkinnedPosition].Descriptor.ShaderRegister = 3;	//	t3: SkinnedPosition
	rootParameters[ComputeRootSkinnedPosition].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedPosition].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedNormal].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[ComputeRootSkinnedNormal].Descriptor.ShaderRegister = 4;	//	t4: SkinnedNormal
	rootParameters[ComputeRootSkinnedNormal].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedNormal].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedTangent].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[ComputeRootSkinnedTangent].Descriptor.ShaderRegister = 5;	//	t5: SkinnedTangent
	rootParameters[ComputeRootSkinnedTangent].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedTangent].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedBiTangent].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[ComputeRootSkinnedBiTangent].Descriptor.ShaderRegister = 6;	//	t6: SkinnedBiTangent
	rootParameters[ComputeRootSkinnedBiTangent].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedBiTangent].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedBoneIndex].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[ComputeRootSkinnedBoneIndex].Descriptor.ShaderRegister = 7;	//	t7: SkinnedBoneIndex
	rootParameters[ComputeRootSkinnedBoneIndex].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedBoneIndex].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedBoneWeight].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[ComputeRootSkinnedBoneWeight].Descriptor.ShaderRegister = 8;	//	t8: SkinnedBiTangent
	rootParameters[ComputeRootSkinnedBoneWeight].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedBoneWeight].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedPositionWorld].ParameterType = D3D12_ROOT_PARAMETER_TYPE_UAV;
	rootParameters[ComputeRootSkinnedPositionWorld].Descriptor.ShaderRegister = 1;	//	u1: SkinnedPositionWorld
	rootParameters[ComputeRootSkinnedPositionWorld].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedPositionWorld].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedNormalWorld].ParameterType = D3D12_ROOT_PARAMETER_TYPE_UAV;
	rootParameters[ComputeRootSkinnedNormalWorld].Descriptor.ShaderRegister = 2;	//	u2: SkinnedPositionWorld
	rootParameters[ComputeRootSkinnedNormalWorld].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedNormalWorld].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedTangentWorld].ParameterType = D3D12_ROOT_PARAMETER_TYPE_UAV;
	rootParameters[ComputeRootSkinnedTangentWorld].Descriptor.ShaderRegister = 3;	//	u3: SkinnedPositionWorld
	rootParameters[ComputeRootSkinnedTangentWorld].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedTangentWorld].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedBiTangentWorld].ParameterType = D3D12_ROOT_PARAMETER_TYPE_UAV;
	rootParameters[ComputeRootSkinnedBiTangentWorld].Descriptor.ShaderRegister = 4;	//	u4: SkinnedPositionWorld
	rootParameters[ComputeRootSkinnedBiTangentWorld].Descriptor.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedBiTangentWorld].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[ComputeRootSkinnedVerticesNum].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[ComputeRootSkinnedVerticesNum].Constants.Num32BitValues = 1;
	rootParameters[ComputeRootSkinnedVerticesNum].Constants.ShaderRegister = 5;		//  b5: gnVertiesNum
	rootParameters[ComputeRootSkinnedVerticesNum].Constants.RegisterSpace = 0;
	rootParameters[ComputeRootSkinnedVerticesNum].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	D3D12_ROOT_SIGNATURE_DESC rootSignatureDesc;
	::ZeroMemory(&rootSignatureDesc, sizeof(D3D12_ROOT_SIGNATURE_DESC));
	rootSignatureDesc.NumParameters = _countof(rootParameters);
	rootSignatureDesc.pParameters = rootParameters;
	rootSignatureDesc.NumStaticSamplers = 0;
	rootSignatureDesc.pStaticSamplers = nullptr;
	rootSignatureDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_NONE;

	ID3DBlob *signatureBlob = NULL;
	ID3DBlob *errorBlob = NULL;
	HRESULT hResult = D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signatureBlob, &errorBlob);
	hResult = device->CreateRootSignature(0, signatureBlob->GetBufferPointer(),
		signatureBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void**)&computeRootSignature);
	if (signatureBlob) signatureBlob->Release();
	if (errorBlob) errorBlob->Release();

	return(computeRootSignature);
}

void Scene::CreateCbvSrvUavDescriptorHeaps(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, int nConstantBufferViews, int nShaderResourceViews, int nUnorderedAccessViews)
{
	D3D12_DESCRIPTOR_HEAP_DESC d3dDescriptorHeapDesc;
	d3dDescriptorHeapDesc.NumDescriptors = nConstantBufferViews + nShaderResourceViews + nUnorderedAccessViews; //CBVs + SRVs 
	d3dDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	d3dDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	d3dDescriptorHeapDesc.NodeMask = 0;
	device->CreateDescriptorHeap(&d3dDescriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&cbvSrvUavDescriptorHeap);

	cbvCPUDescriptorNextHandle = cbvCPUDescriptorStartHandle = cbvSrvUavDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	cbvGPUDescriptorNextHandle = cbvGPUDescriptorStartHandle = cbvSrvUavDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
	srvCPUDescriptorNextHandle.ptr = srvCPUDescriptorStartHandle.ptr = cbvCPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nConstantBufferViews);
	srvGPUDescriptorNextHandle.ptr = srvGPUDescriptorStartHandle.ptr = cbvGPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nConstantBufferViews);
	uavCPUDescriptorNextHandle.ptr = uavCPUDescriptorStartHandle.ptr = srvCPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nShaderResourceViews);
	uavGPUDescriptorNextHandle.ptr = uavGPUDescriptorStartHandle.ptr = srvGPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nShaderResourceViews);

}
// Scene에서 View들의 생성을 관리한다. Shader에서 View의 사용은 리턴값인 할당한 핸들의 시작주소로 접근할 수 있다.
D3D12_GPU_DESCRIPTOR_HANDLE Scene::CreateConstantBufferViews(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, int nConstantBufferViews, ID3D12Resource *pd3dConstantBuffers, UINT nStride)
{
	D3D12_GPU_DESCRIPTOR_HANDLE d3dCbvGPUDescriptorHandle = cbvGPUDescriptorNextHandle;
	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = pd3dConstantBuffers->GetGPUVirtualAddress();
	D3D12_CONSTANT_BUFFER_VIEW_DESC d3dCBVDesc;
	d3dCBVDesc.SizeInBytes = nStride;
	for (int j = 0; j < nConstantBufferViews; j++)
	{
		d3dCBVDesc.BufferLocation = d3dGpuVirtualAddress + (nStride * j);
		device->CreateConstantBufferView(&d3dCBVDesc, cbvCPUDescriptorNextHandle);
		cbvCPUDescriptorNextHandle.ptr = cbvCPUDescriptorNextHandle.ptr + ::gnCbvSrvDescriptorIncrementSize;
		cbvGPUDescriptorNextHandle.ptr = cbvGPUDescriptorNextHandle.ptr + ::gnCbvSrvDescriptorIncrementSize;
	}
	return(d3dCbvGPUDescriptorHandle);
}

D3D12_SHADER_RESOURCE_VIEW_DESC GetShaderResourceViewDesc(D3D12_RESOURCE_DESC d3dResourceDesc, UINT nTextureType)
{
	D3D12_SHADER_RESOURCE_VIEW_DESC d3dShaderResourceViewDesc;
	d3dShaderResourceViewDesc.Format = d3dResourceDesc.Format;
	d3dShaderResourceViewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	switch (nTextureType)
	{
	case ResourceTexture2D: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D)(d3dResourceDesc.DepthOrArraySize == 1)
	case ResourceTexture2D_Array:
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		d3dShaderResourceViewDesc.Texture2D.MipLevels = -1;
		d3dShaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
		d3dShaderResourceViewDesc.Texture2D.PlaneSlice = 0;
		d3dShaderResourceViewDesc.Texture2D.ResourceMinLODClamp = 0.0f;
		break;
	case ResourceTexture2DArray: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D)(d3dResourceDesc.DepthOrArraySize != 1)
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DARRAY;
		d3dShaderResourceViewDesc.Texture2DArray.MipLevels = -1;
		d3dShaderResourceViewDesc.Texture2DArray.MostDetailedMip = 0;
		d3dShaderResourceViewDesc.Texture2DArray.PlaneSlice = 0;
		d3dShaderResourceViewDesc.Texture2DArray.ResourceMinLODClamp = 0.0f;
		d3dShaderResourceViewDesc.Texture2DArray.FirstArraySlice = 0;
		d3dShaderResourceViewDesc.Texture2DArray.ArraySize = d3dResourceDesc.DepthOrArraySize;
		break;
	case ResourceTextureCube: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D)(d3dResourceDesc.DepthOrArraySize == 6)
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
		d3dShaderResourceViewDesc.TextureCube.MipLevels = -1;
		d3dShaderResourceViewDesc.TextureCube.MostDetailedMip = 0;
		d3dShaderResourceViewDesc.TextureCube.ResourceMinLODClamp = 0.0f;
		break;
	case ResourceBuffer: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_BUFFER)
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
		d3dShaderResourceViewDesc.Buffer.FirstElement = 0;
		d3dShaderResourceViewDesc.Buffer.NumElements = 0;
		d3dShaderResourceViewDesc.Buffer.StructureByteStride = 0;
		d3dShaderResourceViewDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
		break;
	}
	return(d3dShaderResourceViewDesc);

}

void Scene::CreateTextureResource(ID3D12Device *device, ID3D12GraphicsCommandList *commandList)
{
	rootArgumentInfos.reserve(500);
	rootArgumentIdx.reserve(4);
	int i = 0;

	//디테일 텍스처 8개, 디테일 노말 텍스처 8개 , 알파텍스처2개 , 터레인노말텍스처1개 = 19;

	Texture *terrainDetailTexture = new Texture(12, ResourceTexture2D, 0);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Grass.dds", 0);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Rock.dds", 1);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Mud.dds", 2);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/light Rock.dds", 3);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/snow.dds", 4);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/ice.dds", 5);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Concrete.dds", 6);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Asphalt.dds", 7);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Beach.dds", 8);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/soil.dds", 9);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/FloorStreet.dds", 10);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/SnowRoad.dds", 11);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainDetailTexture, GraphicsRootTerrainTexture));
	textures.emplace_back(terrainDetailTexture);


	Texture *terrianNormal = new Texture(12, ResourceTexture2D, 0);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Grass_n.dds", 0);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Rock_n.dds", 1);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Mud_n.dds", 2);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/light-Rock_n.dds", 3);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/snow_n.dds", 4);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/ice_n.dds", 5);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Concrete_n.dds", 6);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Asphalt_n.dds", 7);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Beach_n.dds", 8);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/soil_n.dds", 9);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/FloorStreet_n.dds", 10);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/SnowRoad_n.dds", 11);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrianNormal, GraphicsRootTerrainNormalTexture));
	textures.emplace_back(terrianNormal);
	// TerrainTexture

	Texture *billboardTexture = new Texture(1, ResourceTexture2D, 0);
	billboardTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Tree/Billboard_Tree.dds", 0);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, billboardTexture, GraphicsRootBillboardTextures));
	textures.emplace_back(billboardTexture);
	// BillboardTexture

	Texture *particleTexture = new Texture(6, ResourceTexture2D, 0);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/SmokeParticle1.dds", 0);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/LightParticle.dds", 1);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/FireParticle.dds", 2);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/GunFire.dds", 3);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/Dust.dds", 4);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/Blood2.dds", 5);

	particleTextureMap["Smoke"] = 0;
	particleTextureMap["Light"] = 1;
	particleTextureMap["Fire"] = 2;
	particleTextureMap["GunFire"] = 3;
	particleTextureMap["Dust"] = 4;
	particleTextureMap["Blood"] = 5;

	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, particleTexture, GraphicsRootParticleTextures));
	textures.emplace_back(particleTexture);
	// ParticleTexture

	Texture *skyBoxTexture = new Texture(1, ResourceTextureCube, 0);
	skyBoxTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/SkyBox/cubeMap.dds", 0);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, skyBoxTexture, GraphicsRootSkyBoxTexture));
	textures.emplace_back(skyBoxTexture);
	// SkyBoxTexture
	Texture *uiTexture = new Texture(88, ResourceTexture2D, 0);
	// 인게임 유아이 들
	// 플레이어 체력, 다른 플레이어 정보
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/OtherGunPlayerIcon.dds", 0);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/OtherSwordPlayerIcon.dds", 1);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/OtherBowPlayerIcon.dds", 2);

	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/HPBar.dds", 3);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/StaminaBar.dds", 4);
	// 맵
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Map.dds", 5);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/MapPassedPosition.dds", 6);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/MapNowPosition.dds", 7);

	// 장신구
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Bracelet.dds", 8);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Bracelet2.dds", 9);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Crystal.dds", 10);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/FireCrystal.dds", 11);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/FireOil.dds", 12);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Food.dds", 13);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Gift.dds", 14);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Gloves.dds", 15);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Hammer.dds", 16);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Portion.dds", 17);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Shoes.dds", 18);

	// 총 든 캐릭터 아이콘
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/ScarGunIcon.dds", 19);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/G28GunIcon.dds", 20);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/AkGunIcon.dds", 21);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/M4GunIcon.dds", 22);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/SniperGunIcon.dds", 23);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/BlackAkGunIcon.dds", 24);

	// 검 든 캐릭터 아이콘
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/AxeIcon.dds", 25);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/CustomSwordIcon.dds", 26);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/HammerIcon.dds", 27);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/KatanaSwordIcon.dds", 28);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/PickaxeIcon.dds", 29);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/RionSwordIcon.dds", 30);

	// 활 든 캐릭터 아이콘
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/ElfBowIcon.dds", 31);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/GoldenBowIcon.dds", 32);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/GriffinBowIcon.dds", 33);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/IronBowIcon.dds", 34);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/NormalBowIcon.dds", 35);

	// 장신구 획득 화면
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/BraceletChoice.dds", 36);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/Bracelet2Choice.dds", 37);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/CrystalChoice.dds", 38);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/FireCrystalChoice.dds", 39);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/FireOilChoice.dds", 40);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/FoodChoice.dds", 41);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/GiftChoice.dds", 42);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/GlovesChoice.dds", 43);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/HammerChoice.dds", 44);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/PortionChoice.dds", 45);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Accessories/ShoesChoice.dds", 46);

	// 총 장비 획득 창
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/ScarChoice.dds", 47);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/G28Choice.dds", 48);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/Ak47Choice.dds", 49);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/M4Choice.dds", 50);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/SniperChoice.dds", 51);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/BlackAkChoice.dds", 52);

	// 검 장비 획득 창
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/AxeChoice.dds", 53);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/CustomSwordChoice.dds", 54);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/HammerChoice.dds", 55);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/KatanaChoice.dds", 56);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/PickaxeChoice.dds", 57);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/RionSwordChoice.dds", 58);

	// 활 장비 획득 창
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/ElfBowChoice.dds", 59);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/GoldenBowChoice.dds", 60);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/GriffinBowChoice.dds", 61);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/IronBowChoice.dds", 62);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/WeaponIcon/NormalBowChoice.dds", 63);

	// 대기화면 유아이
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyUiBase.dds", 64);
	// 선택한 플레이어 아이콘
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyGunIcon.dds", 65);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbySwordIcon.dds", 66);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyBowIcon.dds", 67);
	// 준비 버튼, 준비완료 아이콘, 게임시작 아이콘
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyReadyButton.dds", 68);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyStartButton.dds", 69); 
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyReadyOkIcon.dds", 70);


	// 캐릭터 초싱화 위에 테두리
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyCharacterSelect.dds", 71);

	// 인게임 화면 중앙 조준선
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/CrossHair.dds", 72);
	// 총 캐릭터 줌 할시 줌 화면
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/Scope.dds", 73);

	// 게임 시작 화면
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/StartScene.dds", 74);

	// 로비 'player1 ~ player4' 이름 모음
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyPlayer1.dds", 75);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyPlayer2.dds", 76);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyPlayer3.dds", 77);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LobbyPlayer4.dds", 78);

	// 인게임 'player1 ~ player4' 이름 모음
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/InGamePlayer1.dds", 79);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/InGamePlayer2.dds", 80);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/InGamePlayer3.dds", 81);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/InGamePlayer4.dds", 82);

	// 총 캐릭터 탄창 유아이 2개
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/magazineBase.dds", 83);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/magazineLeft.dds", 84);


	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/LoadingScene.dds", 85);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/GameOverScene.dds", 86);
	uiTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/UI/VictoryScene.dds", 87);

	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, uiTexture, GraphicsRootUiTexture));
	textures.emplace_back(uiTexture);
	// UITexture

	D3D12_SHADER_RESOURCE_VIEW_DESC depthSrvDesc = {};
	depthSrvDesc.Format = DXGI_FORMAT_R24_UNORM_X8_TYPELESS;
	depthSrvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	depthSrvDesc.Texture2D.MipLevels = 1;
	depthSrvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	Texture* depthTexture = new Texture(2, ResourceTexture2D, 0);
	for (int i = 0; i < COMMAND_IDX_NUM; ++i)
		depthTexture->SetTexture(depthStencilBuffer[i], i);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, depthTexture, GraphicsRootDepthTexture, &depthSrvDesc));
	textures.emplace_back(depthTexture);
	// DepthStencilBuffer

#ifdef _WITH_SHADOW_USE

	Texture* shadowMapTexture = new Texture(2, ResourceTexture2D, 0);
	for (int i = 0; i < COMMAND_IDX_NUM; ++i)
		shadowMapTexture->SetTexture(shadowMapBuffer[i], i);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, shadowMapTexture, GraphicsRootShadowTexture, &depthSrvDesc));
	textures.emplace_back(shadowMapTexture);
	// ShadowMapTexture
#endif

	Texture *terrainAlpha0 = new Texture(3, ResourceTexture2D, 0);
	terrainAlpha0->LoadTextureFromFile(device, commandList, L"Assets/Map/map_00_alphaTexture0.dds", 0);
	terrainAlpha0->LoadTextureFromFile(device, commandList, L"Assets/Map/map_00_alphaTexture1.dds", 1);
	terrainAlpha0->LoadTextureFromFile(device, commandList, L"Assets/Map/map_00_alphaTexture2.dds", 2);
	mapTextureIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainAlpha0, GraphicsRootTerrainAlphaTexture));
	textures.emplace_back(terrainAlpha0);

	Texture *terrainAlpha8 = new Texture(3, ResourceTexture2D, 0);
	terrainAlpha8->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture0.dds", 0);
	terrainAlpha8->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture1.dds", 1);
	terrainAlpha8->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture2.dds", 2);
	mapTextureIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainAlpha8, GraphicsRootTerrainAlphaTexture));
	textures.emplace_back(terrainAlpha8);

	Texture *terrainAlpha2 = new Texture(3, ResourceTexture2D, 0);
	terrainAlpha2->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture0.dds", 0);
	terrainAlpha2->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture1.dds", 1);
	terrainAlpha2->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture2.dds", 2);
	mapTextureIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainAlpha2, GraphicsRootTerrainAlphaTexture));
	textures.emplace_back(terrainAlpha8);

	Texture *terrainAlpha3 = new Texture(3, ResourceTexture2D, 0);
	terrainAlpha3->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture0.dds", 0);
	terrainAlpha3->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture1.dds", 1);
	terrainAlpha3->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture2.dds", 2);
	mapTextureIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainAlpha3, GraphicsRootTerrainAlphaTexture));
	textures.emplace_back(terrainAlpha8);

	Texture *terrainAlpha4 = new Texture(3, ResourceTexture2D, 0);
	terrainAlpha4->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture0.dds", 0);
	terrainAlpha4->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture1.dds", 1);
	terrainAlpha4->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture2.dds", 2);
	mapTextureIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainAlpha4, GraphicsRootTerrainAlphaTexture));
	textures.emplace_back(terrainAlpha8);

	Texture *terrainAlpha5 = new Texture(3, ResourceTexture2D, 0);
	terrainAlpha5->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture0.dds", 0);
	terrainAlpha5->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture1.dds", 1);
	terrainAlpha5->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture2.dds", 2);
	mapTextureIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainAlpha5, GraphicsRootTerrainAlphaTexture));
	textures.emplace_back(terrainAlpha8);

	Texture *terrainAlpha6 = new Texture(3, ResourceTexture2D, 0);
	terrainAlpha6->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture0.dds", 0);
	terrainAlpha6->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture1.dds", 1);
	terrainAlpha6->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture2.dds", 2);
	mapTextureIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainAlpha6, GraphicsRootTerrainAlphaTexture));
	textures.emplace_back(terrainAlpha8);

	Texture *terrainAlpha7 = new Texture(3, ResourceTexture2D, 0);
	terrainAlpha7->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture0.dds", 0);
	terrainAlpha7->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture1.dds", 1);
	terrainAlpha7->LoadTextureFromFile(device, commandList, L"Assets/Map/map_08_alphaTexture2.dds", 2);
	mapTextureIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainAlpha7, GraphicsRootTerrainAlphaTexture));
	textures.emplace_back(terrainAlpha8);

	//Texture *terrainAlpha8 = new Texture(3, ResourceTexture2D, 0);
	//terrainAlpha8->LoadTextureFromFile(device, commandList, L"Assets/Map/map_02_alphaTexture0.dds", 0);
	//terrainAlpha8->LoadTextureFromFile(device, commandList, L"Assets/Map/map_02_alphaTexture1.dds", 1);
	//terrainAlpha8->LoadTextureFromFile(device, commandList, L"Assets/Map/map_02_alphaTexture2.dds", 2);
	mapTextureIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainAlpha8, GraphicsRootTerrainAlphaTexture));
	textures.emplace_back(terrainAlpha8);
}

UINT Scene::FIndCanGoWay()
{
	UINT result = 0;
	if (curMapPos.x != 0) result += CanGoWayLeft;
	if (curMapPos.x != 2) result += CanGoWayRight;

	if (curMapPos.y != 0) result += CanGoWayUp;
	if (curMapPos.y != 2) result += CanGoWayDown;

	return result;
}

int Scene::CreateShaderResourceViews(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, Texture *texture, UINT rootParameterStartIndex, D3D12_SHADER_RESOURCE_VIEW_DESC* desc)
{
	int startIdx = rootArgumentCurrentIdx;
	if (texture)
	{
		int nTextures = texture->GetTextures();
		int nTextureType = texture->GetTextureType();
		for (int i = 0; i < nTextures; i++)
		{
			ID3D12Resource *pShaderResource = texture->GetTexture(i);
			D3D12_RESOURCE_DESC	d3dResourceDesc = pShaderResource->GetDesc();
			D3D12_SHADER_RESOURCE_VIEW_DESC d3dShaderResourceViewDesc;
			if (desc)
				d3dShaderResourceViewDesc = *desc;
			else
				d3dShaderResourceViewDesc = GetShaderResourceViewDesc(d3dResourceDesc, nTextureType);
			device->CreateShaderResourceView(pShaderResource, &d3dShaderResourceViewDesc, srvCPUDescriptorNextHandle);
			srvCPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
			SRVROOTARGUMENTINFO temp(rootParameterStartIndex, srvGPUDescriptorNextHandle);
			rootArgumentInfos.emplace_back(temp);
			srvGPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
			rootArgumentCurrentIdx++;
		}
	}

	return startIdx;
}

int Scene::CreateStructuredShaderResourceViews(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, UINT numElements, UINT stride, ID3D12Resource* buffer, UINT rootParameterStartIndex)
{
	int startIdx = rootArgumentCurrentIdx;

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
	srvDesc.Buffer.FirstElement = 0;
	srvDesc.Buffer.NumElements = numElements;
	srvDesc.Buffer.StructureByteStride = stride;
	srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;

	device->CreateShaderResourceView(buffer, &srvDesc, srvCPUDescriptorNextHandle);
	srvCPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
	SRVROOTARGUMENTINFO temp(rootParameterStartIndex, srvGPUDescriptorNextHandle);
	rootArgumentInfos.emplace_back(temp);
	srvGPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
	rootArgumentCurrentIdx++;

	return startIdx;
}

int Scene::CreateStructuredUnorderedAccessViews(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, UINT numElements, UINT stride, ID3D12Resource* buffer, UINT rootParameterStartIndex)
{
	int startIdx = rootArgumentCurrentIdx;

	D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
	uavDesc.Format = DXGI_FORMAT_UNKNOWN;
	uavDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
	uavDesc.Buffer.FirstElement = 0;
	uavDesc.Buffer.NumElements = numElements;
	uavDesc.Buffer.StructureByteStride = stride;
	uavDesc.Buffer.CounterOffsetInBytes = 0;
	uavDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;

	device->CreateUnorderedAccessView(buffer, nullptr, &uavDesc, uavCPUDescriptorNextHandle);
	uavCPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
	SRVROOTARGUMENTINFO temp(rootParameterStartIndex, srvGPUDescriptorNextHandle);
	rootArgumentInfos.emplace_back(temp);
	uavGPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
	rootArgumentCurrentIdx++;

	return startIdx;
}

void Scene::CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList)
{
	UINT cbElementBytes = ((sizeof(LIGHTS) + 255) & ~255);
	cbLights = ::CreateBufferResource(device, commandList, NULL, cbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	cbLights->Map(0, NULL, (void**)& cbMappedLights);

	UINT cbMaterialBytes = ((sizeof(MATERIALS) + 255) & ~255);
	cbMaterials = ::CreateBufferResource(device, commandList, materials, cbMaterialBytes, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &materialUploadBuffer);

	FOG* fog = new FOG;
	fog->color = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	fog->info = XMFLOAT4(FOG_LINEAR, 1.01f, 300.0f, 1.0f);
	UINT cbFogBytes = ((sizeof(FOG) + 255) & ~255);
	cbFog = ::CreateBufferResource(device, commandList, fog, cbFogBytes, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &fogUploadBuffer);

	// 조명은 매 프레임 위치가 바뀔 수 있으므로 UploadHeap에 생성하였고, 재질은 값이 바뀌지 않으므로 DefaultHeap에 생성하였다.
	UINT cbUiBytes = ((sizeof(LIGHTS) + 255) & ~255);
	cbUis = ::CreateBufferResource(device, commandList, NULL, cbUiBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	cbUis->Map(0, NULL, (void**)& cbMappedUis);
}

void Scene::UpdateShaderVariables()
{
	::memcpy(cbMappedLights, lights, sizeof(LIGHTS));

	cbMappedUis->scenenum = uis->scenenum;
	cbMappedUis->playerJob = uis->playerJob[0];
	cbMappedUis->playerJob1 = uis->playerJob[1];
	cbMappedUis->playerJob2 = uis->playerJob[2];
	cbMappedUis->playerJob3 = uis->playerJob[3];
	cbMappedUis->playerState = uis->playerState[0];
	cbMappedUis->playerState1 = uis->playerState[1];
	cbMappedUis->playerState2 = uis->playerState[2];
	cbMappedUis->playerState3 = uis->playerState[3];
	cbMappedUis->accessory = uis->accessory;
	cbMappedUis->weapon = uis->weapon;
	cbMappedUis->nowspot = uis->nowspot;
	cbMappedUis->hp = uis->hp[0];
	cbMappedUis->hp1 = uis->hp[1];
	cbMappedUis->hp2 = uis->hp[2];
	cbMappedUis->hp3 = uis->hp[3];
	cbMappedUis->stemina = uis->stemina;
	cbMappedUis->ammo = uis->ammo;
	cbMappedUis->newitem = uis->newitem;
	cbMappedUis->isHead = uis->isHead;
	cbMappedUis->players = uis->players;
	cbMappedUis->myid = uis->myid;
	cbMappedUis->passedmap0 = uis->passedmap[0][0];
	cbMappedUis->passedmap1 = uis->passedmap[0][1];
	cbMappedUis->passedmap2 = uis->passedmap[0][2];
	cbMappedUis->passedmap3 = uis->passedmap[1][0];
	cbMappedUis->passedmap4 = uis->passedmap[1][1];
	cbMappedUis->passedmap5 = uis->passedmap[1][2];
	cbMappedUis->passedmap6 = uis->passedmap[2][0];
	cbMappedUis->passedmap7 = uis->passedmap[2][1];
	cbMappedUis->passedmap8 = uis->passedmap[2][2];
}

void Scene::SetShaderVariables(ID3D12GraphicsCommandList* commandList)
{
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbLightsGpuVirtualAddress = cbLights->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootLights, d3dcbLightsGpuVirtualAddress);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbMaterialsGpuVirtualAddress = cbMaterials->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootMaterials, d3dcbMaterialsGpuVirtualAddress);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbFogGpuVirtualAddress = cbFog->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootFogInfo, d3dcbFogGpuVirtualAddress);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbUisGpuVirtualAddress = cbUis->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootUiInfo, d3dcbUisGpuVirtualAddress);

	for (int i = 0; i < rootArgumentIdx.size(); ++i)
	{
		commandList->SetGraphicsRootDescriptorTable(rootArgumentInfos[rootArgumentIdx[i]].rootParameterIndex, rootArgumentInfos[rootArgumentIdx[i]].srvGpuDescriptorHandle);
	}
	commandList->SetGraphicsRootDescriptorTable(rootArgumentInfos[mapTextureIdx[mapIdx]].rootParameterIndex, rootArgumentInfos[mapTextureIdx[mapIdx]].srvGpuDescriptorHandle);
	
}

void Scene::ReleaseShaderVariables()
{
	if (cbLights)
	{
		cbLights->Unmap(0, NULL);
		cbLights->Release();
	}
	if (cbMaterials)
	{
		cbMaterials->Release();
	}
	if (cbFog)
	{
		cbFog->Release();
	}
	if (cbUis)
	{
		cbUis->Unmap(0, NULL);
		cbUis->Release();
	}
	for (int i = 0; i < shaders.size(); ++i)
	{
		shaders[i]->ReleaseShaderVariables();
	}
}

void Scene::GarbageCollection()
{
	for (int i = 0; i < shaders.size(); ++i)
	{
		shaders[i]->GarbageCollection();
	}
}

bool Scene::OnProcessingMouseMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{
	return(false);
}
bool Scene::OnProcessingKeyboardMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{
	return(false);
}

bool Scene::ProcessInput(UCHAR* keysBuffer)
{
	return(false);
}

void Scene::AnimateObjects(float timeElapsed, Camera* camera)
{
	if (!shaders.empty())
	{
		for (int i = 0; i < shaders.size(); i++)
		{
			if (shaders[i])
				shaders[i]->AnimateObjects(timeElapsed, camera);
		}
	}
	// 조명의 위치와 방향을 플레이어의 위치와 방향으로 변경한다.
	//if (lights)
	//{
	//	lights->lights[1].position = player->GetPosition();
	//	lights->lights[1].direction = player->GetLookVector();
	//}
}
void Scene::PrepareRender(ID3D12GraphicsCommandList* commandList)
{
	if (graphicsRootSignature) commandList->SetGraphicsRootSignature(graphicsRootSignature);
	if (computeRootSignature) commandList->SetComputeRootSignature(computeRootSignature);
	if (cbvSrvUavDescriptorHeap) commandList->SetDescriptorHeaps(1, &cbvSrvUavDescriptorHeap);

}

void Scene::PrepareSet(ID3D12GraphicsCommandList* commandList, Camera* camera, bool isShadow)
{
	camera->SetViewportsAndScissorRects(commandList);
	camera->UpdateShaderVariables(commandList, isShadow);
}

void Scene::PrepareCompute(ID3D12GraphicsCommandList* commandList)
{
	for (int i = 0; i < shaders.size(); ++i)
	{
		shaders[i]->PrepareCompute(commandList);
	}
}


void Scene::Render(D3D12_CPU_DESCRIPTOR_HANDLE renderTargetHandle, Camera * camera, Camera* lightCamera[MAX_CASCADE_SIZE], HANDLE handle[THREADS_NUM])
{

}

