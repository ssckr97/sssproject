#include "stdafx.h"
#include "Animation.h"


AnimationSets::AnimationSets(int AnimationSets)
{
	animationSetnum = AnimationSets;
	animationSets = new AnimationSet*[AnimationSets];
}

AnimationSets::~AnimationSets()
{
	for (int i = 0; i < animationSetnum; i++) if (animationSets[i]) delete animationSets[i];
	if (animationSets) delete[] animationSets;

	if (animatedBoneFrameCaches) delete[] animatedBoneFrameCaches;
}

void AnimationSets::SetCallbackKeys(int AnimationSet, int CallbackKeys)
{
	animationSets[AnimationSet]->callbackKeynum = CallbackKeys;
	animationSets[AnimationSet]->callbackKeys = new CALLBACKKEY[CallbackKeys];
}

void AnimationSets::SetCallbackKey(int AnimationSet, int KeyIndex, float KeyTime, void *Data)
{
	animationSets[AnimationSet]->callbackKeys[KeyIndex].time = KeyTime;
	animationSets[AnimationSet]->callbackKeys[KeyIndex].callbackData = Data;
}

void AnimationSets::SetAnimationCallbackHandler(int AnimationSet, AnimationCallbackHandler *CallbackHandler)
{
	animationSets[AnimationSet]->SetAnimationCallbackHandler(CallbackHandler);
}

AnimationController::AnimationController(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, int AnimationTracks, LoadedModelInfo *Model)
{
	animationTracknum = AnimationTracks;
	animationTracks = new AnimationTrack[AnimationTracks];

	animationSets = Model->animationSets;
	animationSets->AddRef();

	nextTransform = new XMFLOAT4X4[animationSets->animatedBoneFrames];
	upnextTransform = new XMFLOAT4X4[animationSets->animatedBoneFrames];
	lonextTransform = new XMFLOAT4X4[animationSets->animatedBoneFrames];

	beforeTransform = new XMFLOAT4X4[animationSets->animatedBoneFrames];
	upbeforeTransform = new XMFLOAT4X4[animationSets->animatedBoneFrames];
	lobeforeTransform = new XMFLOAT4X4[animationSets->animatedBoneFrames];
}

AnimationController::~AnimationController()
{
	if (animationTracks) delete[] animationTracks;

	if (animationSets) animationSets->Release();
}

void AnimationController::SetCallbackKeys(int AnimationSet, int CallbackKeys)
{
	if (animationSets) animationSets->SetCallbackKeys(AnimationSet, CallbackKeys);
}

void AnimationController::SetCallbackKey(int nAnimationSet, int nKeyIndex, float fKeyTime, void *pData)
{
	if (animationSets) animationSets->SetCallbackKey(nAnimationSet, nKeyIndex, fKeyTime, pData);
}

void AnimationController::SetAnimationCallbackHandler(int nAnimationSet, AnimationCallbackHandler *pCallbackHandler)
{
	if (animationSets) animationSets->SetAnimationCallbackHandler(nAnimationSet, pCallbackHandler);
}

void AnimationController::SetNextTransform(int Animationnum)
{
	for (int j = 0; j < animationSets->animatedBoneFrames; j++)
	{
		AnimationSet *pAnimationSet = animationSets->animationSets[Animationnum];
		nextTransform[j] = pAnimationSet->keyFrameTransforms[0][j];
	}
}

void AnimationController::SetNextUpTransform(int Animationnum)
{
	for (int j = 0; j < animationSets->animatedBoneFrames; j++)
	{
		AnimationSet *pAnimationSet = animationSets->animationSets[Animationnum];
		upnextTransform[j] = pAnimationSet->keyFrameTransforms[0][j];
	}
	nextUpAnimation = Animationnum;
}

void AnimationController::SetNextLoTransform(int Animationnum)
{
	for (int j = 0; j < animationSets->animatedBoneFrames; j++)
	{
		AnimationSet *pAnimationSet = animationSets->animationSets[Animationnum];
		lonextTransform[j] = pAnimationSet->keyFrameTransforms[0][j];
	}
	nextLoAnimation = Animationnum;
}


void AnimationController::SetBeforeTransform(int Animationnum)
{
	for (int j = 0; j < animationSets->animatedBoneFrames; j++)
	{
		AnimationSet *pAnimationSet = animationSets->animationSets[Animationnum];
		beforeTransform[j] = pAnimationSet->keyFrameTransforms[0][j];
	}
}

void AnimationController::SetTrackAnimationSet(int AnimationTrack, int AnimationSet)
{
	if (animationTracks && (nowAnimation != AnimationSet))
	{
		nowAnimation = AnimationSet;
		animationTracks[AnimationTrack].animationSet = AnimationSet;
		animationTracks[AnimationTrack].upanimationSet = AnimationSet;
		animationTracks[AnimationTrack].loanimationSet = AnimationSet;

		SetNextTransform(AnimationSet);
		SetNextUpTransform(AnimationSet);
		SetNextLoTransform(AnimationSet);
		upmiddleTransform = true;
		lomiddleTransform = true;
		tempPosition = 0.f;
		uptempPosition = 0.f;
		lotempPosition = 0.f;
	}	
}

void AnimationController::SetTrackAnimationSet(int AnimationTrack, int AnimationSet1, int AnimationSet2)
{
	if (animationTracks && ((nowupAnimation != AnimationSet1) || (nowloAnimation != AnimationSet2)))
	{
		if (animationTracks[AnimationTrack].upanimationSet != AnimationSet1)
		{
			nowupAnimation = AnimationSet1;
			animationTracks[AnimationTrack].upanimationSet = AnimationSet1;
			upmiddleTransform = true;
			SetNextUpTransform(AnimationSet1);
			uptempPosition = 0.f;
		}
		if (animationTracks[AnimationTrack].loanimationSet != AnimationSet2)
		{
			nowloAnimation = AnimationSet2;
			animationTracks[AnimationTrack].loanimationSet = AnimationSet2;
			lomiddleTransform = true;
			SetNextLoTransform(AnimationSet2);
			lotempPosition = 0.f;
		}
	}
}

void AnimationController::SetNextAnimationSet(int AnimationTrack, int AnimationSet1, int AnimationSet2)
{
	upmiddleTransform = true;
	SetNextUpTransform(AnimationSet1);
	uptempPosition = 0.f;
	lomiddleTransform = true;
	SetNextLoTransform(AnimationSet2);
	lotempPosition = 0.f;
}


void AnimationController::SetTrackEnable(int AnimationTrack, bool Enable)
{
	if (animationTracks) animationTracks[AnimationTrack].SetEnable(Enable);
}

void AnimationController::SetTrackPosition(int AnimationTrack, float Position)
{
	if (animationTracks) animationTracks[AnimationTrack].SetPosition(Position);
}

void AnimationController::SetTrackSpeed(int AnimationTrack, float Speed)
{
	if (animationTracks) animationTracks[AnimationTrack].SetSpeed(Speed);
}

void AnimationController::SetTrackWeight(int AnimationTrack, float Weight)
{
	if (animationTracks) animationTracks[AnimationTrack].SetWeight(Weight);
}

void AnimationController::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList)
{
}

void AnimationController::AdvanceTime(float TimeElapsed, GameObject *RootGameObject)
{
	time += TimeElapsed;
	uptempPosition += TimeElapsed;
	lotempPosition += TimeElapsed;
	if (animationTracks)
	{
		for (int i = 0; i < animationTracknum; i++) animationTracks[i].position += (TimeElapsed * animationTracks[i].speed);

		for (int j = 0; j < animationSets->animatedBoneFrames; j++)
		{
			XMFLOAT4X4 xmf4x4Transform = Matrix4x4::Zero();
			for (int k = 0; k < animationTracknum; k++)
			{
				if (animationTracks[k].enable)
				{
					AnimationSet *pAnimationSet = NULL;
					if (animationSets->animatedBoneFrameCaches[j]->GetParentFrame("swat:Spine") == 1 || animationSets->animatedBoneFrameCaches[j]->GetParentFrame("Spine") == 1)
					{
						if (!upmiddleTransform)
						{
							pAnimationSet = animationSets->animationSets[animationTracks[k].upanimationSet];
							pAnimationSet->SetPosition(animationTracks[k].position);
							XMFLOAT4X4 xmf4x4TrackTransform = pAnimationSet->GetSRT(j);
							xmf4x4Transform = Matrix4x4::Add(xmf4x4Transform, Matrix4x4::Scale(xmf4x4TrackTransform, animationTracks[k].weight));
							upbeforeTransform[j] = xmf4x4TrackTransform;
						}
						else if (upmiddleTransform)
						{
							if (uptempPosition < 0.2f)
							{
								xmf4x4Transform = Matrix4x4::Add(xmf4x4Transform, (Matrix4x4::Interpolate(upbeforeTransform[j], upnextTransform[j], uptempPosition / 0.2f)));
							}
							else
							{
								xmf4x4Transform = upnextTransform[j];
								upmiddleTransform = false;
								animationTracks[k].upanimationSet = nextUpAnimation;
								animationTracks[k].position = 0.f;
							}
						}

					}
					else
					{
						if (!lomiddleTransform)
						{
							pAnimationSet = animationSets->animationSets[animationTracks[k].loanimationSet];
							pAnimationSet->SetPosition(animationTracks[k].position);
							XMFLOAT4X4 xmf4x4TrackTransform = pAnimationSet->GetSRT(j);
							xmf4x4Transform = Matrix4x4::Add(xmf4x4Transform, Matrix4x4::Scale(xmf4x4TrackTransform, animationTracks[k].weight));
							lobeforeTransform[j] = xmf4x4TrackTransform;
						}
						else if (lomiddleTransform)
						{
							if (lotempPosition < 0.2f)
							{
								xmf4x4Transform = Matrix4x4::Add(xmf4x4Transform, (Matrix4x4::Interpolate(lobeforeTransform[j], lonextTransform[j], lotempPosition / 0.2f)));
							}
							else
							{
								xmf4x4Transform = lonextTransform[j];
								lomiddleTransform = false;
								animationTracks[k].loanimationSet = nextLoAnimation;
								if(nextUpAnimation != MainCharacterWalkingReload)
									animationTracks[k].position = 0.f;
								std::cout << nextUpAnimation << std::endl;
							}
						}
					}
				}
			}

			animationSets->animatedBoneFrameCaches[j]->SetTransform(xmf4x4Transform);
			
			AnimationSet *tempAnimationSet = animationSets->animationSets[animationTracks[0].upanimationSet];

			if (playerJob == PlayerJobBow)
				animationTracks[0].position;

			if (animationTracks[0].position >= tempAnimationSet->keyFrameTimes[tempAnimationSet->keyFrames - 1] && tempAnimationSet->lockupanimation)
			{
				tempAnimationSet->lockupanimation = false;
				SetTrackAnimationSet(0, 0);
			}

			if (animationTracks[0].position >= tempAnimationSet->keyFrameTimes[tempAnimationSet->keyFrames - 1] && tempAnimationSet->lockanimation)
			{
				tempAnimationSet->lockanimation = false;
				SetTrackAnimationSet(0, 0);
			}

			if (playerJob == PlayerJobRifle)
			{
				if (animationTracks[0].upanimationSet == MainCharacterFire && (animationTracks[0].loanimationSet == MainCharacterWalkLeft || animationTracks[0].loanimationSet == MainCharacterWalkRight))
				{
					if (animationSets->animatedBoneFrameCaches[j]->CompareName("swat:Spine") == 0)
					{
						animationSets->animatedBoneFrameCaches[j]->Rotate(0.f, -30.0f, 0.f);
					}
				}

				if (animationTracks[0].upanimationSet == MainCharacterFire && (animationTracks[0].loanimationSet == MainCharacterWalkBack || animationTracks[0].loanimationSet == MainCharacterWalk))
				{
					if (animationSets->animatedBoneFrameCaches[j]->CompareName("swat:Spine") == 0)
					{
						animationSets->animatedBoneFrameCaches[j]->Rotate(0.f, 45.0f, 0.f);
					}
				}

				if (animationSets->animatedBoneFrameCaches[j]->CompareName("swat:Hips") == 0) // 하체 회전을 위한 몸 전체 회전
				{
					animationSets->animatedBoneFrameCaches[j]->Rotate(0.f, bodyRotate, 0.f);
				}

				if (animationSets->animatedBoneFrameCaches[j]->CompareName("swat:Spine") == 0) // 상체 회전되는거 확인함
				{
					animationSets->animatedBoneFrameCaches[j]->Rotate(0.f, -bodyRotate, 0.f);
					animationSets->animatedBoneFrameCaches[j]->Rotate(0.f, upperbodyRotate, 0.f);
				}
			}
		}
		RootGameObject->UpdateTransform(NULL);
	}
}

void AnimationController::MiddleAnimation(float TimeElapsed, GameObject *RootGameObject)
{
	time += TimeElapsed;
	if (middleTransform)
	{
		tempPosition += TimeElapsed * 5;
		for (int i = 0; i < animationSets->animatedBoneFrames; ++i)
		{
			XMFLOAT4X4 xmf4x4Transform = Matrix4x4::Zero();

			if (tempPosition < 1.f)
			{
				xmf4x4Transform = Matrix4x4::Add(xmf4x4Transform, (Matrix4x4::Interpolate(beforeTransform[i], nextTransform[i], tempPosition)));

				animationSets->animatedBoneFrameCaches[i]->SetTransform(xmf4x4Transform);
			}
			else
				animationSets->animatedBoneFrameCaches[i]->SetTransform(nextTransform[i]);

		}

		if (tempPosition >= 1.f)
			middleTransform = false;


		RootGameObject->UpdateTransform(NULL);
	}
}

LoadedModelInfo::~LoadedModelInfo()
{
	if (skinnedMeshes) delete[] skinnedMeshes;
}

void LoadedModelInfo::PrepareSkinning()
{
	int nSkinnedMesh = 0;
	skinnedMeshes = new SkinnedMesh*[skinnedMeshNum];
	modelRootObject->FindAndSetSkinnedMesh(skinnedMeshes, &nSkinnedMesh);

	for (int i = 0; i < skinnedMeshNum; i++) skinnedMeshes[i]->PrepareSkinning(modelRootObject);
}

// 애니메이션 컨트롤러 수정 중

AnimationSet::AnimationSet(float Length, int FramesPerSecond, int KeyFrames, int AnimatedBones, char *Name)
{
	length = Length;
	framesPerSecond = FramesPerSecond;
	keyFrames = KeyFrames;

	strcpy_s(animationSetName, 64, Name);

	keyFrameTimes = new float[keyFrames];
	keyFrameTransforms = new XMFLOAT4X4*[keyFrames];

	//이 부분 변경해야함(뼈마다 키프레임 개 수 다르게 나옴)
	for (int i = 0; i < keyFrames; i++) keyFrameTransforms[i] = new XMFLOAT4X4[AnimatedBones];
}

AnimationSet::~AnimationSet()
{
	if (keyFrameTimes) delete[] keyFrameTimes;
	for (int j = 0; j < keyFrames; j++) if (keyFrameTransforms[j]) delete[] keyFrameTransforms[j];
	if (keyFrameTransforms) delete[] keyFrameTransforms;

	if (callbackKeys) delete[] callbackKeys;
	if (animationCallbackHandler) delete animationCallbackHandler;
}

void *AnimationSet::GetCallbackData()
{
	for (int i = 0; i < callbackKeynum; i++)
	{
		if (::IsEqual(callbackKeys[i].time, position, ANIMATION_CALLBACK_EPSILON)) return(callbackKeys[i].callbackData);
	}
	return(NULL);
}

void AnimationSet::SetPosition(float TrackPosition)
{
	position = TrackPosition;
	switch (animationType)
	{
	case AnimationTypeLoop:
	{
		position = fmod(TrackPosition, keyFrameTimes[keyFrames - 1]); // m_fPosition = fTrackPosition - int(fTrackPosition / m_pfKeyFrameTimes[m_nKeyFrames-1]) * m_pfKeyFrameTimes[m_nKeyFrames-1];
//			m_fPosition = fmod(fTrackPosition, m_fLength); //if (m_fPosition < 0) m_fPosition += m_fLength;
//			m_fPosition = fTrackPosition - int(fTrackPosition / m_fLength) * m_fLength;
		break;
	}
	case AnimationTypeOnce:
		if (TrackPosition >= keyFrameTimes[keyFrames - 1])
		{
			if (strncmp(animationSetName, "AimRealese", 64) == 0)
			{
				isRelease = true;
			}
			position = keyFrameTimes[keyFrames - 1];
		}
		break;
	case AnimationTypePingpong:
		break;
	}

	if (animationCallbackHandler)
	{
		void *callbackData = GetCallbackData();
		if (callbackData) animationCallbackHandler->HandleCallback(callbackData);
	}
}

XMFLOAT4X4 AnimationSet::GetSRT(int nBone)
{
	XMFLOAT4X4 xmf4x4Transform = Matrix4x4::Identity();
#ifdef _WITH_ANIMATION_SRT
	XMVECTOR S, R, T;
	XMFLOAT3 RO = XMFLOAT3(0.0f, 0.0f, 0.0f);
	for (int i = 0; i < (m_nKeyFrameTranslations - 1); i++)
	{
		if ((m_pfKeyFrameTranslationTimes[i] <= position) && (position <= m_pfKeyFrameTranslationTimes[i + 1]))
		{
			float t = (position - m_pfKeyFrameTranslationTimes[i]) / (m_pfKeyFrameTranslationTimes[i + 1] - m_pfKeyFrameTranslationTimes[i]);
			T = XMVectorLerp(XMLoadFloat3(&m_ppxmf3KeyFrameTranslations[i][nBone]), XMLoadFloat3(&m_ppxmf3KeyFrameTranslations[i + 1][nBone]), t);
			break;
		}
	}
	for (UINT i = 0; i < (m_nKeyFrameScales - 1); i++)
	{
		if ((m_pfKeyFrameScaleTimes[i] <= position) && (position <= m_pfKeyFrameScaleTimes[i + 1]))
		{
			float t = (position - m_pfKeyFrameScaleTimes[i]) / (m_pfKeyFrameScaleTimes[i + 1] - m_pfKeyFrameScaleTimes[i]);
			S = XMVectorLerp(XMLoadFloat3(&m_ppxmf3KeyFrameScales[i][nBone]), XMLoadFloat3(&m_ppxmf3KeyFrameScales[i + 1][nBone]), t);
			break;
		}
	}
	for (UINT i = 0; i < (m_nKeyFrameRotations - 1); i++)
	{
		if ((m_pfKeyFrameRotationTimes[i] <= position) && (position <= m_pfKeyFrameRotationTimes[i + 1]))
		{
			float t = (position - m_pfKeyFrameRotationTimes[i]) / (m_pfKeyFrameRotationTimes[i + 1] - m_pfKeyFrameRotationTimes[i]);
			R = XMQuaternionSlerp(XMLoadFloat4(&m_ppxmf4KeyFrameRotations[i][nBone]), XMLoadFloat4(&m_ppxmf4KeyFrameRotations[i + 1][nBone]), t);
			break;
		}
	}

	XMStoreFloat4x4(&xmf4x4Transform, XMMatrixAffineTransformation(S, XMLoadFloat3(&RO), R, T));
#else   
	for (int i = 0; i < (keyFrames - 1); i++)
	{
		if ((keyFrameTimes[i] <= position) && (position <= keyFrameTimes[i + 1]))
		{
			float t = (position - keyFrameTimes[i]) / (keyFrameTimes[i + 1] - keyFrameTimes[i]);
			xmf4x4Transform = Matrix4x4::Interpolate(keyFrameTransforms[i][nBone], keyFrameTransforms[i + 1][nBone], t);
			break;
		}
	}
#endif
	return(xmf4x4Transform);
}

void AnimationSet::SetCallbackKeys(int CallbackKeys)
{
	callbackKeynum = CallbackKeys;
	callbackKeys = new CALLBACKKEY[callbackKeynum];
}

void AnimationSet::SetCallbackKey(int KeyIndex, float KeyTime, void *Data)
{
	callbackKeys[KeyIndex].time = KeyTime;
	callbackKeys[KeyIndex].callbackData = Data;
}

void AnimationSet::SetAnimationCallbackHandler(AnimationCallbackHandler *CallbackHandler)
{
	animationCallbackHandler = CallbackHandler;
}

// 애니메이션 컨트롤러 수정 중