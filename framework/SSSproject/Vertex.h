#pragma once
#include "stdafx.h"

//정점을 표현하기 위한 클래스를 선언한다.
class Vertex
{
public:
	//정점의 위치 벡터이다(모든 정점은 최소한 위치 벡터를 가져야 한다).
	XMFLOAT3 position;

public:
	Vertex() { position = XMFLOAT3(0.0f, 0.0f, 0.0f); }
	Vertex(XMFLOAT3 position) { this->position = position; }
	~Vertex() { }
};

class BillboardGSVertex : public Vertex
{
public:
	XMFLOAT3 billboardInfo;		// x : width, y : height, z : textureIdx

	BillboardGSVertex() {
		position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		billboardInfo = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}
	BillboardGSVertex(XMFLOAT3 pos, XMFLOAT3 info) {
		position = pos;
		billboardInfo = info;
	}
	~BillboardGSVertex() {}

};

class DiffusedVertex : public Vertex
{
public:
	//정점의 색상이다.
	XMFLOAT4 diffuse;
public:
	DiffusedVertex()
	{
		position = XMFLOAT3(0.0f, 0.0f, 0.0f); diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
	}
	DiffusedVertex(float x, float y, float z, XMFLOAT4 diffuse)
	{
		position = XMFLOAT3(x, y, z);
		this->diffuse = diffuse;
	}
	DiffusedVertex(XMFLOAT3 position, XMFLOAT4 diffuse)
	{
		this->position = position;
		this->diffuse = diffuse;
	}
	~DiffusedVertex() { }
};

class TexturedVertex : public Vertex
{
public:
	XMFLOAT2						texCoord;

public:
	TexturedVertex() {
		position = XMFLOAT3(0.0f, 0.0f, 0.0f); texCoord = XMFLOAT2(0.0f, 0.0f);
	}
	TexturedVertex(float x, float y, float z, XMFLOAT2 xmf2TexCoord) {
		position = XMFLOAT3(x, y, z); texCoord = xmf2TexCoord;
	}
	TexturedVertex(XMFLOAT3 xmf3Position, XMFLOAT2 xmf2TexCoord = XMFLOAT2(0.0f, 0.0f)) {
		position = xmf3Position; texCoord = xmf2TexCoord;
	}
	~TexturedVertex() { }
};

class UiVertex : public Vertex
{
public:
	XMFLOAT2		uv;
	XMFLOAT3		texinfo;
public:
	UiVertex() {
		position = XMFLOAT3(0.0f, 0.0f, 0.0f); uv = XMFLOAT2(0.0f, 0.0f); texinfo = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}
	UiVertex(XMFLOAT3 xmf3Position, XMFLOAT2 xmf2UvInfo, XMFLOAT3 xmf3TexInfo){
		position = xmf3Position; uv = xmf2UvInfo; texinfo = xmf3TexInfo;
	}
	~UiVertex() {}
};

class DiffusedTexturedVertex : public DiffusedVertex
{
public:
	XMFLOAT2						texCoord;

public:
	DiffusedTexturedVertex() {
		position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
		texCoord = XMFLOAT2(0.0f, 0.0f);
	}
	DiffusedTexturedVertex(float x, float y, float z, XMFLOAT4 diffuse, XMFLOAT2 texCoord) {
		this->position = XMFLOAT3(x, y, z);
		this->diffuse = diffuse;
		this->texCoord = texCoord;
	}
	DiffusedTexturedVertex(XMFLOAT3 position, XMFLOAT4 diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f), XMFLOAT2 texCoord = XMFLOAT2(0.0f, 0.0f)) {
		this->position = position;
		this->diffuse = diffuse;
		this->texCoord = texCoord;
	}
	~DiffusedTexturedVertex() { }
};

class Diffused2TexturedVertex : public DiffusedVertex
{
public:
	XMFLOAT2						texCoord0;
	XMFLOAT2						texCoord1;	// 디테일 텍스쳐 u, v 좌표
	XMFLOAT3						normal;
public:
	Diffused2TexturedVertex() {
		position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
		texCoord0 = texCoord1 = XMFLOAT2(0.0f, 0.0f);
		normal = XMFLOAT3{ 0.0f, 1.0f ,0.0f };
	}
	Diffused2TexturedVertex(float x, float y, float z, XMFLOAT4 diffuse, XMFLOAT2 texCoord0, XMFLOAT2 texCoord1) {
		this->position = XMFLOAT3(x, y, z);
		this->diffuse = diffuse;
		this->texCoord0 = texCoord0;
		this->texCoord1 = texCoord1;
	}
	Diffused2TexturedVertex(XMFLOAT3 position, XMFLOAT4 diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f), XMFLOAT2 texCoord0 = XMFLOAT2(0.0f, 0.0f), XMFLOAT2 texCoord1 = XMFLOAT2(0.0f, 0.0f)) {
		this->position = position;
		this->diffuse = diffuse;
		this->texCoord0 = texCoord0;
		this->texCoord1 = texCoord1;
	}
	~Diffused2TexturedVertex() { }
};

class Diffused2TexturedNormalVertex : public Diffused2TexturedVertex
{
public:
	XMFLOAT3						normal;

public:

	Diffused2TexturedNormalVertex() {
		position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f);
		normal = XMFLOAT3(0.0f, 0.0f, 0.0f);
		texCoord0 = texCoord1 = XMFLOAT2(0.0f, 0.0f);
	}
	Diffused2TexturedNormalVertex(float x, float y, float z, XMFLOAT4 diffuse, XMFLOAT3 normal, XMFLOAT2 texCoord0, XMFLOAT2 texCoord1) {
		position = XMFLOAT3(x, y, z);
		this->diffuse = diffuse;
		this->normal = normal;
		this->texCoord0 = texCoord0;
		this->texCoord1 = texCoord1;
	}
	Diffused2TexturedNormalVertex(XMFLOAT3 position, XMFLOAT4 diffuse = XMFLOAT4(0.0f, 0.0f, 0.0f, 0.0f), XMFLOAT3 normal = XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT2 texCoord0 = XMFLOAT2(0.0f, 0.0f), XMFLOAT2 texCoord1 = XMFLOAT2(0.0f, 0.0f)) {
		this->position = position;
		this->diffuse = diffuse;
		this->normal = normal;
		this->texCoord0 = texCoord0;
		this->texCoord1 = texCoord1;
	}
	~Diffused2TexturedNormalVertex() { }
};

class IlluminatedVertex : public Vertex
{
protected:
	XMFLOAT3 normal;

public:
	IlluminatedVertex() {
		position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		normal = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}
	IlluminatedVertex(float x, float y, float z, XMFLOAT3 normal = XMFLOAT3(0.0f, 0.0f, 0.0f)) {
		this->position = XMFLOAT3(x, y, z);
		this->normal = normal;
	}
	IlluminatedVertex(XMFLOAT3 position, XMFLOAT3 normal = XMFLOAT3(0.0f, 0.0f, 0.0f)) {
		this->position = position;
		this->normal = normal;
	}
	~IlluminatedVertex() { }
};

class IlluminatedTexturedVertex : public TexturedVertex
{
public:
	XMFLOAT3 normal;

public:
	IlluminatedTexturedVertex() {
		position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		texCoord = XMFLOAT2(0.0f, 0.0f);
		normal = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}
	IlluminatedTexturedVertex(float x, float y, float z, float u, float v, XMFLOAT3 normal = XMFLOAT3(0.0f, 0.0f, 0.0f)) {
		this->position = XMFLOAT3(x, y, z);
		this->texCoord = XMFLOAT2(u, v);
		this->normal = normal;
	}
	IlluminatedTexturedVertex(XMFLOAT3 position, XMFLOAT2 texCoord, XMFLOAT3 normal = XMFLOAT3(0.0f, 0.0f, 0.0f)) {
		this->position = position;
		this->texCoord = texCoord;
		this->normal = normal;
	}
	~IlluminatedTexturedVertex() { }
};

class NormalMapTexturedVertex : public IlluminatedTexturedVertex
{
public:
	XMFLOAT3						tangent;
	XMFLOAT3						biTangent;

public:
	NormalMapTexturedVertex() {
		position = XMFLOAT3(0.0f, 0.0f, 0.0f);
		texCoord = XMFLOAT2(0.0f, 0.0f);
		normal = XMFLOAT3(0.0f, 0.0f, 0.0f);
		tangent = XMFLOAT3(0.0f, 0.0f, 0.0f);
		biTangent = XMFLOAT3(0.0f, 0.0f, 0.0f);
	}
	NormalMapTexturedVertex(float x, float y, float z, XMFLOAT2 texCoord, XMFLOAT3 normal = XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3 tangent = XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3 biTangent = XMFLOAT3(0.0f, 0.0f, 0.0f)) {
		this->position = XMFLOAT3(x, y, z);
		this->normal = normal;
		this->tangent = tangent;
		this->biTangent = biTangent;
		this->texCoord = texCoord;
	}
	NormalMapTexturedVertex(XMFLOAT3 position, XMFLOAT2 texCoord = XMFLOAT2(0.0f, 0.0f), XMFLOAT3 normal = XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3 tangent = XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3 biTangent = XMFLOAT3(0.0f, 0.0f, 0.0f)) {
		this->position = position;
		this->normal = normal;
		this->tangent = tangent;
		this->biTangent = biTangent;
		this->texCoord = texCoord;
	}
	~NormalMapTexturedVertex() { }
};