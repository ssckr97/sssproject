#include "stdafx.h"
#include "protocol.h"
#include "GameFramework.h"

std::mutex GameFramework::locker;

std::atomic<bool> GameFramework::isLive;

std::string serverIP;

GameFramework::GameFramework()
{
	factory = NULL;
	swapChain = NULL;
	device = NULL;
	commandQueue = NULL;
	pipelineState = NULL;
	for (int j = 0; j < COMMAND_IDX_NUM; ++j)
	{
		for (int i = 0; i < CommandListStateNum; ++i)
		{
			commandAllocator[j][i] = NULL;
			commandList[j][i] = NULL;
		}
	}
	for (int i = 0; i < renderTargetBuffersNum; i++)
	{
		renderTargetBuffers[i] = NULL;
		fenceValues[i] = 0;
	}
	rtvDescriptorHeap = NULL;
	rtvDescriptorIncrementSize = 0;

	for (int i = 0; i < COMMAND_IDX_NUM; ++i)
	{
		depthStencilBuffer[i] = NULL;
		shadowMapBuffer[i] = NULL;
	}

	dsvDescriptorHeap = NULL;
	dsvDescriptorIncrementSize = 0;
	swapChainBufferIndex = 0;

	fenceEvent = NULL;
	fence = NULL;

	windowClientWidth = FRAME_BUFFER_WIDTH;
	windowClientHeight = FRAME_BUFFER_HEIGHT;

	_tcscpy_s(frameRate, _T("SSSProject ("));

	scene = NULL;
	isMouseDown = false;
	ingameBuild = false;
}

GameFramework::~GameFramework()
{
}

bool GameFramework::OnCreate(HINSTANCE hinstance, HWND hwnd)
{
	this->hinstance = hinstance;
	this->hwnd = hwnd;

	CreateDirect3DDevice();
	CreateCommandQueueAndList();
	CreateSwapChain();
	CreateRtvAndDsvDescriptorHeaps();
	//Direct3D 디바이스, 명령 큐와 명령 리스트, 스왑 체인 등을 생성하는 함수를 호출한다.

	CreateRenderTargetViews();
	CreateDepthStencilView();
	// 랜더타겟, 뎁스스텐실의 뷰를 생성한다.

	std::cin >> serverIP;


	BuildObjects();
	//렌더링할 객체(게임 월드 객체)를 생성한다.
	
	InitSocketAndConnect(hwnd);

	sendtime = std::chrono::high_resolution_clock::now();

	return(true);
}

void GameFramework::OnDestroy()
{
	WaitForGpuComplete(commandQueue, fence, ++fenceValues[swapChainBufferIndex], fenceEvent);
	GameFramework::isLive = false;

	for (int i = 0; i < RENDER_THREADS_NUM; ++i)
	{
		SetEvent(renderBeginEvent[i]);
	}
	SetEvent(computeBeginEvent);
	SetEvent(presentBeginEvent);

		for (auto& th : renderThreads)
	{
		th.join();
	}

	computeThread.join();
	presentThread.join();

	ReleaseShaderVariables();
	ReleaseObjects();

	::CloseHandle(fenceEvent);
	for (int i = 0; i < RENDER_THREADS_NUM; ++i)
	{
		::CloseHandle(renderBeginEvent[i]);
		::CloseHandle(renderShadowFinishEvent[i]);
		::CloseHandle(renderSceneFinishEvent[i]);
	}
	::CloseHandle(computeBeginEvent);
	::CloseHandle(computeFinishEvent);
	::CloseHandle(presentBeginEvent);
	::CloseHandle(presentFinishEvent);


#if defined(_DEBUG)
	if (debugController) debugController->Release();
#endif

	for (int i = 0; i < renderTargetBuffersNum; i++)
		if (renderTargetBuffers[i]) renderTargetBuffers[i]->Release();
	if (rtvDescriptorHeap) rtvDescriptorHeap->Release();
	if (dsvDescriptorHeap) dsvDescriptorHeap->Release();
	if (commandQueue) commandQueue->Release();
	for (int j = 0; j < 2; ++j) {
		for (int i = 0; i < CommandListStateNum; ++i)
		{
			if (commandAllocator[j][i]) commandAllocator[j][i]->Release();
			if (commandList[j][i]) commandList[j][i]->Release();
		}
	}
	if (fence) fence->Release();
	swapChain->SetFullscreenState(FALSE, NULL);
	if (swapChain) swapChain->Release();
	if (device) device->Release();
	if (factory) factory->Release();
}

void GameFramework::CreateSwapChain()
{
	RECT rcClient;
	::GetClientRect(hwnd, &rcClient);
	windowClientWidth = rcClient.right - rcClient.left;
	windowClientHeight = rcClient.bottom - rcClient.top;

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	::ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferCount = renderTargetBuffersNum;
	swapChainDesc.BufferDesc.Width = windowClientWidth;
	swapChainDesc.BufferDesc.Height = windowClientHeight;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.SampleDesc.Count = (msaa4xEnable) ? 4 : 1;
	swapChainDesc.SampleDesc.Quality = (msaa4xEnable) ? (msaa4xQualityLevels - 1) : 0;
	swapChainDesc.Windowed = TRUE;
	swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	//DXGI_SWAP_CHAIN_FLAG_ALLOW_TEARING;
	//전체화면 모드에서 바탕화면의 해상도를 스왑체인(후면버퍼)의 크기에 맞게 변경한다.

	HRESULT hresult = factory->CreateSwapChain(commandQueue, &swapChainDesc, (IDXGISwapChain **)&swapChain);
	swapChainBufferIndex = swapChain->GetCurrentBackBufferIndex();
	hresult = factory->MakeWindowAssociation(hwnd, DXGI_MWA_NO_ALT_ENTER);

	//#ifndef _WITH_SWAPCHAIN_FULLSCREEN_STATE
	//	CreateRenderTargetViews();
	//#endif
}

void GameFramework::CreateDirect3DDevice()
{
#if defined(_DEBUG)
	D3D12GetDebugInterface(__uuidof(ID3D12Debug), (void **)&debugController);
	debugController->EnableDebugLayer();
#endif

	::CreateDXGIFactory1(__uuidof(IDXGIFactory4), (void **)&factory);
	//DXGI 팩토리를 생성한다.
	HRESULT hResult;
	IDXGIAdapter1* adapter = NULL;
	for (UINT i = 0; DXGI_ERROR_NOT_FOUND != factory->EnumAdapters1(i, &adapter); i++)
	{
		DXGI_ADAPTER_DESC1 adapterDesc;
		adapter->GetDesc1(&adapterDesc);
		if (adapterDesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) continue;
		if (SUCCEEDED(hResult = D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_12_0, _uuidof(ID3D12Device), (void **)&device))) break;
	}
	_com_error err(hResult);
	err.ErrorMessage();
	
	//모든 하드웨어 어댑터 대하여 특성 레벨 12.0을 지원하는 하드웨어 디바이스를 생성한다.

	if (!adapter)
	{
		factory->EnumWarpAdapter(_uuidof(IDXGIFactory4), (void **)&adapter);
		D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_0, _uuidof(ID3D12Device), (void**)&device);
	}
	//특성 레벨 12.0을 지원하는 하드웨어 디바이스를 생성할 수 없으면 WARP 디바이스를 생성한다.

	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS msaaQualityLevels;
	msaaQualityLevels.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	msaaQualityLevels.SampleCount = 4; //Msaa4x 다중 샘플링
	msaaQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
	msaaQualityLevels.NumQualityLevels = 0;
	device->CheckFeatureSupport(D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS, &msaaQualityLevels, sizeof(D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS));
	msaa4xQualityLevels = msaaQualityLevels.NumQualityLevels;
	//디바이스가 지원하는 다중 샘플의 품질 수준을 확인한다.

	msaa4xEnable = (msaa4xQualityLevels > 1) ? true : false;
	//다중 샘플의 품질 수준이 1보다 크면 다중 샘플링을 활성화한다.

	device->CreateFence(0, D3D12_FENCE_FLAG_NONE, __uuidof(ID3D12Fence), (void**)&fence);
	
	for (int i = 0; i < renderTargetBuffersNum; i++)
	{
		fenceValues[i] = 0;
	}
	//펜스를 생성하고 펜스 값을 0으로 설정한다.
	fenceEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	//펜스와 동기화를 위한 이벤트 객체를 생성한다(이벤트 객체의 초기값을 FALSE이다). 이벤트가 실행되면(Signal) 이벤트의 값을 자동적으로 FALSE가 되도록 생성한다.

	::gnCbvSrvDescriptorIncrementSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);


	if (adapter) adapter->Release();
}

void GameFramework::CreateCommandQueueAndList()
{
	D3D12_COMMAND_QUEUE_DESC commandQueueDesc;
	::ZeroMemory(&commandQueueDesc, sizeof(D3D12_COMMAND_QUEUE_DESC));
	commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	commandQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

	HRESULT hresult = device->CreateCommandQueue(&commandQueueDesc, _uuidof(ID3D12CommandQueue), (void **)&commandQueue);

	for (int j = 0; j < COMMAND_IDX_NUM; ++j) {
		for (int i = 0; i < CommandListStateNum; ++i)
		{
			hresult = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, __uuidof(ID3D12CommandAllocator), (void **)&commandAllocator[j][i]);
			hresult = device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, commandAllocator[j][i], NULL, __uuidof(ID3D12GraphicsCommandList), (void**)&commandList[j][i]);
			hresult = commandList[j][i]->Close();
		}
	}

}

void GameFramework::CreateRtvAndDsvDescriptorHeaps()
{
	D3D12_DESCRIPTOR_HEAP_DESC descriptorHeapDesc;
	::ZeroMemory(&descriptorHeapDesc, sizeof(D3D12_DESCRIPTOR_HEAP_DESC));
	descriptorHeapDesc.NumDescriptors = renderTargetBuffersNum;
	descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	descriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	descriptorHeapDesc.NodeMask = 0;

	HRESULT hresult = device->CreateDescriptorHeap(&descriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&rtvDescriptorHeap);
	//렌더 타겟 서술자 힙(서술자의 개수는 스왑체인 버퍼의 개수)을 생성한다.

	rtvDescriptorIncrementSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	//렌더 타겟 서술자 힙의 원소의 크기를 저장한다.


	descriptorHeapDesc.NumDescriptors = 4;
	descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	hresult = device->CreateDescriptorHeap(&descriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&dsvDescriptorHeap);
	//깊이-스텐실 서술자 힙(서술자의 개수는 4)을 생성한다.
	// (깊이 스텐실 버퍼 + 그림자 버퍼) * COMMAND_IDX_NUM
	dsvDescriptorIncrementSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	//깊이-스텐실 서술자 힙의 원소의 크기를 저장한다.
}

void GameFramework::CreateRenderTargetViews()
{
	D3D12_CPU_DESCRIPTOR_HANDLE rtvCPUDescriptorHandle = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	D3D12_CLEAR_VALUE clearValue;
	FLOAT clearColor[4]{ 1.0f, 1.0f, 1.0f, 1.0f };
	clearValue.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

	for (UINT i = 0; i < renderTargetBuffersNum; i++)
	{
		HRESULT hresult = swapChain->GetBuffer(i, __uuidof(ID3D12Resource), (void**)&renderTargetBuffers[i]);
		rtvHandle[i] = rtvCPUDescriptorHandle;
		device->CreateRenderTargetView(renderTargetBuffers[i], NULL, rtvHandle[i]);
		rtvCPUDescriptorHandle.ptr += rtvDescriptorIncrementSize;
	}
}

void GameFramework::CreateDepthStencilView()
{
	D3D12_RESOURCE_DESC resourceDesc;
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resourceDesc.Alignment = 0;
	resourceDesc.Width = windowClientWidth;
	resourceDesc.Height = windowClientHeight;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.MipLevels = 1;
	resourceDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	resourceDesc.SampleDesc.Count = (msaa4xEnable) ? 4 : 1;
	resourceDesc.SampleDesc.Quality = (msaa4xEnable) ? (msaa4xQualityLevels - 1) : 0;
	resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resourceDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_HEAP_PROPERTIES heapProperties;
	::ZeroMemory(&heapProperties, sizeof(D3D12_HEAP_PROPERTIES));
	heapProperties.Type = D3D12_HEAP_TYPE_DEFAULT;
	heapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	heapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	heapProperties.CreationNodeMask = 1;
	heapProperties.VisibleNodeMask = 1;

	D3D12_CLEAR_VALUE clearValue;
	clearValue.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	clearValue.DepthStencil.Depth = 1.0f;
	clearValue.DepthStencil.Stencil = 0;

	for (int i = 0; i < COMMAND_IDX_NUM; ++i)
		device->CreateCommittedResource(&heapProperties, D3D12_HEAP_FLAG_NONE, &resourceDesc, D3D12_RESOURCE_STATE_DEPTH_WRITE, &clearValue,
			__uuidof(ID3D12Resource), (void **)&depthStencilBuffer[i]);
	//깊이-스텐실 버퍼를 생성한다.

	D3D12_DEPTH_STENCIL_VIEW_DESC dsvDesc;
	dsvDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;
	dsvDesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	dsvDesc.Texture2D.MipSlice = 0;
	dsvDesc.Flags = D3D12_DSV_FLAG_NONE;

	D3D12_CPU_DESCRIPTOR_HANDLE dsvCPUDescriptorHandle = dsvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	for (int i = 0; i < COMMAND_IDX_NUM; ++i)
	{
		depthStencilHandle[i] = dsvCPUDescriptorHandle;
		device->CreateDepthStencilView(depthStencilBuffer[i], &dsvDesc, depthStencilHandle[i]);
		dsvCPUDescriptorHandle.ptr += dsvDescriptorIncrementSize;
	}
	//깊이-스텐실 버퍼 뷰를 생성한다.
	cascadeNum = MAX_CASCADE_SIZE;
	cascadeBias = 1.5f;
	cascadePos = new float[cascadeNum + 1];

	resourceDesc.Width = SHADOW_BUFFER_SIZE * cascadeNum;
	resourceDesc.Height = SHADOW_BUFFER_SIZE;
	resourceDesc.Format = DXGI_FORMAT_D24_UNORM_S8_UINT;

	// 쉐도우 맵 버퍼를 생성한다.
	for (int i = 0; i < COMMAND_IDX_NUM; ++i)
		device->CreateCommittedResource(&heapProperties, D3D12_HEAP_FLAG_NONE, &resourceDesc, D3D12_RESOURCE_STATE_DEPTH_WRITE, &clearValue,
			__uuidof(ID3D12Resource), (void **)&shadowMapBuffer[i]);

	for (int i = 0; i < COMMAND_IDX_NUM; ++i)
	{
		shadowMapHandle[i] = dsvCPUDescriptorHandle;
		device->CreateDepthStencilView(shadowMapBuffer[i], &dsvDesc, shadowMapHandle[i]);
		dsvCPUDescriptorHandle.ptr += dsvDescriptorIncrementSize;
	}
	// 쉐도우 맵 버퍼뷰를 생성한다.
}

void GameFramework::ChangeSwapChainState()
{
	WaitForSingleObject(presentFinishEvent, INFINITE);

	::WaitForGpuComplete(commandQueue, fence, ++fenceValues[swapChainBufferIndex], fenceEvent);
	BOOL fullScreenState = FALSE;
	swapChain->GetFullscreenState(&fullScreenState, NULL);
	swapChain->SetFullscreenState(!fullScreenState, NULL);
	DXGI_MODE_DESC targetParameters;
	targetParameters.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	targetParameters.Width = windowClientWidth;
	targetParameters.Height = windowClientHeight;
	targetParameters.RefreshRate.Numerator = 60;
	targetParameters.RefreshRate.Denominator = 1;
	targetParameters.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	targetParameters.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChain->ResizeTarget(&targetParameters);

	for (int i = 0; i < renderTargetBuffersNum; i++)
		if (renderTargetBuffers[i]) renderTargetBuffers[i]->Release();
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	swapChain->GetDesc(&swapChainDesc);
	swapChain->ResizeBuffers(renderTargetBuffersNum, windowClientWidth, windowClientHeight, swapChainDesc.BufferDesc.Format, swapChainDesc.Flags);

	swapChainBufferIndex = swapChain->GetCurrentBackBufferIndex();

	CreateRenderTargetViews();

	for (int j = 0; j < COMMAND_IDX_NUM; ++j) 
	{
		for (int i = 0; i < RENDER_THREADS_NUM; ++i)
		{
			renderInfo[i]->SetHandles(rtvHandle[j], depthStencilHandle[j], shadowMapHandle[j], j);
		}
		computeInfo->SetHandles(rtvHandle[j], depthStencilHandle[j], shadowMapHandle[j], j);
		computeInfo->SetRtBuffer(renderTargetBuffers[j], j);
	}
	SetEvent(presentFinishEvent);
}

void GameFramework::BuildObjects()
{
	commandList[0][CommandListStateMain]->Reset(commandAllocator[0][CommandListStateMain], NULL);

	scene = new Scene(depthStencilBuffer, shadowMapBuffer, cascadeNum);
	scene->BuildObjects(device, commandList[0][CommandListStateMain]);
	//씬 객체를 생성하고 씬에 포함될 게임 객체들을 생성한다.
	


	processFile = new ProcessFile(scene);
	//file I/O를 처리하는 객체를 만든다.
	SetMapFilePath();
	processFile->LoadMap("Assets/Map/lobby_06", device, commandList[0][CommandListStateMain]);
	isLoadMapFinish = false;

	SkinnedInstancingShader* siShader = static_cast<SkinnedInstancingShader*>(scene->Getshaders()[2]);
	std::vector<SkinnedInstanceBuffer*> buffer = siShader->GetBuffer();
	for (int i = 0; i < buffer.size(); ++i) {
		for (int j = 1; j < buffer[i]->objects.size(); ++j) {
			tZombies.emplace_back(buffer[i]->objects[j]);
		}
	}


	RiflePlayer *riflePlayer = new RiflePlayer(device, commandList[0][CommandListStateMain], scene->GetGraphicsRootSignature(), scene->GetComputeRootSignature(), scene->GetObjects(), 
		reinterpret_cast<ParticleShader*>(scene->Getshaders()[ShaderOrderParticle]), reinterpret_cast<WeaponTrailerShader*>(scene->Getshaders()[ShaderOrderWeaponTrail]), scene->hObjectSize);

	reinterpret_cast<MiniMapShader*>(scene->Getshaders()[ShaderOrderMiniMap])->SetObject(riflePlayer, reinterpret_cast<SkinnedInstancingShader*>(scene->Getshaders()[ShaderOrderSkinnedInstancing]),
		reinterpret_cast<ParticleShader*>(scene->Getshaders()[ShaderOrderParticle]));

	scene->player = player = riflePlayer;
	camera = player->GetCamera();

	CreateLightCameras();

	CreateShaderVariables();

	picking = new Picking();

	commandList[0][CommandListStateMain]->Close();
	ID3D12CommandList *pCommandLists[] = { commandList[0][CommandListStateMain] };
	commandQueue->ExecuteCommandLists(1, pCommandLists);
	//씬 객체를 생성하기 위하여 필요한 그래픽 명령 리스트들을 명령 큐에 추가한다.

	::WaitForGpuComplete(commandQueue, fence, ++fenceValues[swapChainBufferIndex], fenceEvent);

	//그래픽 명령 리스트들이 모두 실행될 때까지 기다린다.

	if (scene) scene->ReleaseUploadBuffers();

	//그래픽 리소스들을 생성하는 과정에 생성된 업로드 버퍼들을 소멸시킨다.

	gameTimer.Reset();

	CreateThreadsAndEvents();
}

void GameFramework::ReleaseObjects()
{

	if (player) delete player;
	if (scene) scene->ReleaseObjects();
	if (scene) delete scene;
	scene = NULL;
	if (lightCamera)
	{
		for (int i = 0; i < cascadeNum; ++i)
			delete lightCamera[i];
		delete[] lightCamera;
	}
}

void GameFramework::CreateThreadsAndEvents()
{
	renderInfo.reserve(RENDER_THREADS_NUM);

	for (int i = 0; i < RENDER_THREADS_NUM; ++i)
	{
		RenderInfo* r = new RenderInfo(device);
		renderInfo.emplace_back(r);
	}
	computeInfo = new ComputeInfo(device);

	std::vector<Shader*> shaders = scene->Getshaders();

	renderInfo[0]->SetShader(shaders[ShaderOrderSkyBox]);
	renderInfo[0]->SetShader(shaders[ShaderOrderTerrain]);
	renderInfo[0]->SetShader(shaders[ShaderOrderInstancing]);
	renderInfo[0]->SetShader(shaders[ShaderOrderSkinnedInstancing]);
	renderInfo[0]->SetShader(shaders[ShaderOrderSkinnedObject]);
	renderInfo[0]->SetShader(shaders[ShaderOrderParticle]);
	renderInfo[0]->SetShader(shaders[ShaderOrderWeaponTrail]);
	renderInfo[0]->SetPlayer(player);
	renderInfo[0]->SetShader(shaders[ShaderOrderUI]);
	renderInfo[0]->SetShader(shaders[ShaderOrderMiniMap]);

	for (int i = 0; i < RENDER_THREADS_NUM; i++)
	{
		renderBeginEvent[i] = CreateEvent(
			NULL,	// Event에 대한 설명자
			FALSE,	// TRUE : 수동 재설정 , FALSE : 자동 재설정
			FALSE,	// TRUE : 신호 상태, FALSE : 비신호 상태
			NULL);	// EVENT객체 이름

		renderShadowFinishEvent[i] = CreateEvent(NULL, FALSE, FALSE, NULL);
		renderSceneFinishEvent[i] = CreateEvent(NULL, FALSE, FALSE, NULL);
	}
	computeBeginEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	computeFinishEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	presentBeginEvent = CreateEvent(NULL, FALSE, FALSE, NULL);
	presentFinishEvent = CreateEvent(NULL, FALSE, TRUE, NULL);
	presentChangeIdxEvent = CreateEvent(NULL, FALSE, TRUE, NULL);

	for (int i = 0; i < RENDER_THREADS_NUM; ++i)
		renderThreads.emplace_back(RenderThread, this, i);
	computeThread = std::thread(ComputeThread, this);
	presentThread = std::thread(PresentThread, this);

	GameFramework::isLive = true;

	for (int j = 0; j < COMMAND_IDX_NUM; ++j)
	{
		shadowCommandLists[j][0] = commandList[j][CommandListStateMain];
		shadowCommandLists[j][1] = computeInfo->GetCommandList(j);

		for (int i = 0; i < RENDER_THREADS_NUM; ++i)
		{
			shadowCommandLists[j][2 + i] = renderInfo[i]->GetShadowCommandList(j);
			renderCommandLists[j][i] = renderInfo[i]->GetRenderCommandList(j);

			renderInfo[i]->SetHandles(rtvHandle[j], depthStencilHandle[j], shadowMapHandle[j], j);
		}
		computeInfo->SetHandles(rtvHandle[j], depthStencilHandle[j], shadowMapHandle[j], j);
		computeInfo->SetRtBuffer(renderTargetBuffers[j], j);
		shadowCommandLists[j][2 + RENDER_THREADS_NUM] = commandList[j][CommandListStateMid];
		renderCommandLists[j][RENDER_THREADS_NUM] = commandList[j][CommandListStatePost];

	}

	// shdowCommandList = main, compute, shadow, mid
	// renderCommandList = render, post
}

void GameFramework::CreateLightCameras()
{
	lightCamera = new Camera*[cascadeNum];

	cascadePos[0] = 1.01f;
	cascadePos[1] = 15.0f;
	cascadePos[2] = 50.0f;
	cascadePos[3] = 300.0f;

	for (int i = 0; i < cascadeNum; ++i)
	{
		lightCamera[i] = new Camera();
		lightCamera[i]->SetViewport(SHADOW_BUFFER_SIZE * i, 0, SHADOW_BUFFER_SIZE, SHADOW_BUFFER_SIZE);
		lightCamera[i]->SetScissorRect(SHADOW_BUFFER_SIZE * i + 2, 2, (SHADOW_BUFFER_SIZE * (1 + i)) - 2, SHADOW_BUFFER_SIZE - 2);
		lightCamera[i]->GenerateViewMatrix(XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(1.0f, -1.0f, 0.0f), XMFLOAT3(0.0f, 1.0f, 0.0f));
		lightCamera[i]->CreateShaderVariables(device, commandList[0][CommandListStateMain]);
	}
}

void GameFramework::UpdateLightCameras()
{
	XMFLOAT4X4 camView = camera->GetViewMatrix();
	XMFLOAT4X4 camViewInv = ::Matrix4x4::Inverse(camView);

	float ar = 1.0f / camera->GetAspectRatio();
	float fov = camera->GetFOV() + 30;
	float tanThetaX = tanf(::XMConvertToRadians(fov / 2.0f));
	float tanThetaY = tanf(::XMConvertToRadians(fov * ar / 2.0f));

	for (int i = 0; i < cascadeNum; ++i)
	{
		XMFLOAT4X4 lightView = lightCamera[i]->GetViewMatrix();

		float xn = cascadePos[i] * tanThetaX;
		float xf = cascadePos[i + 1] * tanThetaX;
		float yn = cascadePos[i] * tanThetaY;
		float yf = cascadePos[i + 1] * tanThetaY;

		XMFLOAT3 frustumCornersV[8] = {
			// near plane
			XMFLOAT3(xn, yn, cascadePos[i]),
			XMFLOAT3(-xn, yn, cascadePos[i]),
			XMFLOAT3(xn, -yn, cascadePos[i]),
			XMFLOAT3(-xn, -yn, cascadePos[i]),

			// far plane
			XMFLOAT3(xf, yf, cascadePos[i + 1]),
			XMFLOAT3(-xf, yf, cascadePos[i + 1]),
			XMFLOAT3(xf, -yf, cascadePos[i + 1]),
			XMFLOAT3(-xf, -yf, cascadePos[i + 1])
		};

		XMFLOAT3 fustumCornersL[8];
		float minX = FLT_MAX;
		float maxX = -FLT_MAX;
		float minY = FLT_MAX;
		float maxY = -FLT_MAX;
		float minZ = FLT_MAX;
		float maxZ = -FLT_MAX;

		XMFLOAT4X4 lightCamInv;

		for (int j = 0; j < 8; ++j)
		{
			// CamView -> World
			XMFLOAT3 frustumCornersW = Vector3::TransformCoord(frustumCornersV[j], camViewInv);
			lightCamInv = Matrix4x4::Inverse(lightView);
			// World -> LightView
			fustumCornersL[j] = Vector3::TransformCoord(frustumCornersW, lightView);

			minX = min(minX, fustumCornersL[j].x);
			maxX = max(maxX, fustumCornersL[j].x);
			minY = min(minY, fustumCornersL[j].y);
			maxY = max(maxY, fustumCornersL[j].y);
			minZ = min(minZ, fustumCornersL[j].z);
			maxZ = max(maxZ, fustumCornersL[j].z);
		}
		XMFLOAT3 center = XMFLOAT3((maxX - minX) / 2.0f + minX, (maxY - minY) / 2.0f + minY, (maxZ - minZ) / 2.0f + minZ);
		float xLength = (maxX - minX) / 2.0f;
		float yLength = (maxY - minY) / 2.0f;
		float zLength = (maxZ - minZ) / 2.0f;

		float radius = xLength;
		radius = max(radius, yLength);
		radius = max(radius, zLength);
		radius = std::ceil(radius);
		radius = cascadePos[i + 1];

		XMFLOAT3 ogMin = XMFLOAT3(center.x - radius, center.y - radius, center.z - radius);
		XMFLOAT3 ogMax = XMFLOAT3(center.x + radius, center.y + radius, center.z + radius);

		// Micro Soft Code
		float texel = radius * 2 / SHADOW_BUFFER_SIZE;

		ogMin.x /= texel;
		ogMin.y /= texel;
		ogMin.z /= texel;

		ogMin = Vector3::Floor(ogMin);

		ogMin.x *= texel;
		ogMin.y *= texel;
		ogMin.z *= texel;

		ogMax.x /= texel;
		ogMax.y /= texel;
		ogMax.z /= texel;

		ogMax = Vector3::Floor(ogMax);

		ogMax.x *= texel;
		ogMax.y *= texel;
		ogMax.z *= texel;
		ogMin.z -= 100;

		lightCamera[i]->GenerateOrthograhpicsOffCenterMatrix(ogMin.x, ogMax.x, ogMin.y, ogMax.y, ogMin.z, ogMax.z);
		lightCamera[i]->SetOrthoInfo(center, radius);
	}
}

void GameFramework::CreateShaderVariables()
{
	lightCameraBuffer = ::CreateBufferResource(device, commandList[0][CommandListStateMain], NULL, sizeof(VS_CB_SHADOW_INFO), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	lightCameraBuffer->Map(0, NULL, (void**)&mappedLightCamera);
}

void GameFramework::UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, int idx)
{
	total = gameTimer.GetTotalTime();
	elapsed = gameTimer.GetTimeElapsed();

	for (int i = 0; i < cascadeNum; ++i) {
		XMFLOAT4X4 viewProjection;
		XMStoreFloat4x4(&viewProjection, XMMatrixTranspose(XMLoadFloat4x4(&Matrix4x4::Multiply(lightCamera[i]->GetViewMatrix(), lightCamera[i]->GetProjectionMatrix()))));
		::memcpy(&mappedLightCamera->viewProjection[i], &viewProjection, sizeof(XMFLOAT4X4));

		XMFLOAT3 pos;
		pos = lightCamera[i]->GetPosition();
		mappedLightCamera->cascadeLength[i].x = cascadePos[i + 1] * cascadeBias;
	}

	mappedLightCamera->shadowTexSize = 1.0f / SHADOW_BUFFER_SIZE;
	mappedLightCamera->cascadeNum = float(cascadeNum);

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = lightCameraBuffer->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootLightCamera, d3dGpuVirtualAddress);

	commandList->SetGraphicsRoot32BitConstants(GraphicsRootTimeInfo, 1, &total, 0);
	commandList->SetGraphicsRoot32BitConstants(GraphicsRootTimeInfo, 1, &elapsed, 1);
	commandList->SetGraphicsRoot32BitConstants(GraphicsRootTimeInfo, 1, &idx, 2);

	commandList->SetComputeRoot32BitConstants(ComputeRootTimeInfo, 1, &total, 0);
	commandList->SetComputeRoot32BitConstants(ComputeRootTimeInfo, 1, &elapsed, 1);
}

void GameFramework::ReleaseShaderVariables()
{
	if (scene) scene->ReleaseShaderVariables();
	if (player) player->ReleaseShaderVariables();
	if (lightCamera) {
		for (int i = 0; i < cascadeNum; ++i)
			lightCamera[i]->ReleaseShaderVariables();
	}
	if (lightCameraBuffer) lightCameraBuffer->Release();
}

void GameFramework::processPacket()
{
	int retval = 0;
	switch (cl.packet_buf[1]) {
	case S2C_LOGIN_OK: {
		sc_packet_lobby* sc_lobbyPacket = reinterpret_cast<sc_packet_lobby*>(cl.packet_buf);
		cl.pri_size = 0;
		myid = sc_lobbyPacket->id;
		UIINFOINCPU* info = scene->GetUIINFO();
		info->myid = myid;
		if (myid == 0)
			info->isHead = true;
		info->players++;
	}
					   break;
	case S2C_ENTER_LOBBY: {
		sc_packet_lobby* sc_lobbyPacket = reinterpret_cast<sc_packet_lobby*>(cl.packet_buf);
		cl.pri_size = 0;
		short id = sc_lobbyPacket->id;
		char job = sc_lobbyPacket->cJob;
		char isReady = sc_lobbyPacket->isReady;
		UIINFOINCPU* info = scene->GetUIINFO();
		info->playerJob[id] = job;
		info->playerState[id] = isReady;
		info->players++;
	}
						 break;
	case S2C_CHANGE_JOB:{
		sc_packet_lobby* sc_lobbyPacket = reinterpret_cast<sc_packet_lobby*>(cl.packet_buf);
		cl.pri_size = 0;
		short id = sc_lobbyPacket->id;
		char job = sc_lobbyPacket->cJob;
		char isReady = sc_lobbyPacket->isReady;
		UIINFOINCPU* info = scene->GetUIINFO();
		info->playerJob[id] = job;
		info->playerState[id] = isReady;
	}
						break;
	case S2C_ENTER_INGAME: {
		sc_packet_ingame_enter* sc_loginPacket = reinterpret_cast<sc_packet_ingame_enter*>(cl.packet_buf);
		cl.pri_size = 0;
		short hp = sc_loginPacket->hp;
		char cJob = sc_loginPacket->cJob;
		XMFLOAT3 pos = sc_loginPacket->pos;
		XMFLOAT3 look = sc_loginPacket->look;

		player->SetPosition(pos, true);
		player->SetLook(look);

		ingameBuild = true;
		loadingCoolTime = gameTimer.GetTotalTime();
	}
					   break;

	case S2C_WEAPON: {
		sc_packet_weapon* sc_weaponPacket = reinterpret_cast<sc_packet_weapon*>(cl.packet_buf);
		cl.pri_size = 0;
		char id = sc_weaponPacket->id;
		char weaponNum = sc_weaponPacket->weaponNum;
		SkinnedAnimationObjectsShader* skinObjShader = reinterpret_cast<SkinnedAnimationObjectsShader*>(scene->Getshaders()[ShaderOrderSkinnedObject]);
		std::vector<SkinnedObjectBuffer*> buffer = skinObjShader->GetBuffer();
		if (myid == id)
			player->SetWeaponNum(weaponNum);
		else {
			for (int i = 0; i < buffer.size(); ++i)
			{
				if (buffer[i]->id == id)
				{
					buffer[i]->weaponNum = weaponNum;
					break;
				}
			}
		}
	}
						   break;

	case S2C_MOVE: {
		sc_packet_move* sc_movePacket = reinterpret_cast<sc_packet_move*>(cl.packet_buf);
		cl.pri_size = 0;
		XMFLOAT3 pos = sc_movePacket->pos;
		XMFLOAT3 look = sc_movePacket->look;
		XMFLOAT3 velocity = sc_movePacket->velocity;
		char aniTrackUp = sc_movePacket->aniTrackUp;
		char aniTrackLo = sc_movePacket->aniTrackLo;
		int direction = sc_movePacket->direction;
		short hp = sc_movePacket->hp;
		char id = sc_movePacket->id;
		if (id == myid) {
			player->hp = hp;
		}
		else {
			SkinnedAnimationObjectsShader* saShader = static_cast<SkinnedAnimationObjectsShader*>(scene->Getshaders()[3]);
			std::vector<SkinnedObjectBuffer*>& buffer = saShader->GetObjectBuffer();
			for (int i = 0; i < buffer.size(); ++i) {
				char Bid = buffer[i]->id;
				if ((int)Bid == (int)id) {
					//여기 있는것들도 나중에 animate로 옮겨서 해줘야한다.
					
					buffer[i]->object->hp = hp;
					buffer[i]->object->SetPosition(pos);
					buffer[i]->object->SetLook(look);
					buffer[i]->velocity = velocity;
					buffer[i]->object->ChangePlayerAnimation((int)aniTrackUp, (int)aniTrackLo, buffer[i]->playerJob);
					buffer[i]->direction = direction;
					break;
				}
			}
		}
	}
				   break;
	case S2C_LEAVE: {
		sc_packet_leave* sc_movePacket = reinterpret_cast<sc_packet_leave*>(cl.packet_buf);
		cl.pri_size = 0;
		char id = sc_movePacket->id;

		SkinnedAnimationObjectsShader* saShader = static_cast<SkinnedAnimationObjectsShader*>(scene->Getshaders()[3]);
		std::vector<SkinnedObjectBuffer*> buffer = saShader->GetObjectBuffer();
		int index;
		SkinnedObjectBuffer* temp;
		for (int i = 0; i < buffer.size(); ++i) {
			char Bid = buffer[i]->id;
			if (Bid == id) {
				index = i;
				SkinnedObjectBuffer* temp = buffer[i];
				buffer[i]->id = -1;
				buffer[i]->isActive = false;
				buffer[i]->object->SetPosition(NOPLAYER, NOPLAYER, NOPLAYER);
				break;
			}
		}

	}
					break;
	case S2C_MAP: {
		//로드맵 
		sc_packet_loadmap* sc_loadpacket = reinterpret_cast<sc_packet_loadmap*>(cl.packet_buf);
		scene->mapIdx = sc_loadpacket->mapNum;
		allKill = false;
		isLoadMap = true;
		loadingCoolTime = gameTimer.GetTotalTime();
		std::cout << scene->mapIdx << std::endl;
	}
				  break;

	case S2C_ZOMBIE: {
		if (isLoadMapFinish == false) break;
		sc_packet_zombie* sc_movePacket = reinterpret_cast<sc_packet_zombie*>(cl.packet_buf);
		cl.pri_size = 0;
		SkinnedInstancingShader* siShader = static_cast<SkinnedInstancingShader*>(scene->Getshaders()[2]);
		std::vector<SkinnedInstanceBuffer*> buffer = siShader->GetBuffer();
		int bidx = sc_movePacket->bidx + 3;
		int oidx = sc_movePacket->oidx;
		int anitrack = sc_movePacket->aniTrack;
		auto& obj = buffer[bidx]->objects[oidx];
		obj->skinnedAnimationController->SetTrackAnimationSet(0, anitrack);
		obj->SetPosition(sc_movePacket->pos);
		obj->SetLook(sc_movePacket->look);
		obj->SetVel(sc_movePacket->velocity);
		obj->Sethp(sc_movePacket->hp);
		
		int state = sc_movePacket->state;
		if (state == IDLE) {
			obj->SetDir(DirectionNoMove);
			obj->SetState(IDLE);
		}
		if (state == WANDER) {
			obj->SetDir(DirectionForword);
			obj->SetState(WANDER);
		}
		if (state == CHASE) {
			obj->SetDir(DirectionForword);
			obj->SetState(CHASE);
		}
		if (state == ATTACK) {
			obj->SetDir(DirectionNoMove);
			obj->SetState(ATTACK);
		}
		if (state == DEATH) {
			if (bidx == MIDDLEBOSS || bidx == FINALBOSS)
				allKill = true;
			obj->SetDir(DirectionNoMove);
			obj->SetState(DEATH);
			obj->skinnedAnimationController->animationTracks[0].position = 0.0f;
			obj->isLive = false;
		}
	}
					 break;
	case S2C_EFFECT: {
		sc_packet_effect* sc_effectPacket = reinterpret_cast<sc_packet_effect*>(cl.packet_buf);
		cl.pri_size = 0;
		char effectType = sc_effectPacket->effectType;
		XMFLOAT3 pos = sc_effectPacket->pos;

		ParticleShader* pShader = reinterpret_cast<ParticleShader*>(scene->Getshaders()[ShaderOrderParticle]);
		
		if (effectType == EffectTypeDust)
		{
			float age = 0.2f;
			XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
			pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
				ParticleInfoInCPU{ pShader->copyBufferMap["HitDust"], pShader->textureMap["Dust"], PARTICLE_TYPE_DISAPPEAR,
				pos, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 1.0f });
		}
		else if (effectType == EffectTypeBlood)
		{
			float age = 0.3f;
			XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
			pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
				ParticleInfoInCPU{ pShader->copyBufferMap["Blood"], pShader->textureMap["Blood"], PARTICLE_TYPE_DISAPPEAR,
				pos, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 0.3f });
		}
	}
					 break;
	case S2C_ENDGAME:{
		sc_packet_endgame* sc_effectPacket = reinterpret_cast<sc_packet_endgame*>(cl.packet_buf);
		cl.pri_size = 0;
		closesocket(cSocket);
		UIINFOINCPU* info = scene->GetUIINFO();
		info->scenenum = SCENE_GAMEOVER;
		// 종료씬 띄워야함.
	}
					 break;
	default:
		break;

	

	}

}

void GameFramework::SetSwapchinIdx(int idx)
{
	this->swapChainBufferIndex = idx;
}


void GameFramework::InitSocketAndConnect(HWND hWnd)
{
	
	WSADATA wsaData;
	WSAStartup(MAKEWORD(2, 2), &wsaData);

	cSocket = WSASocket(AF_INET, SOCK_STREAM, IPPROTO_TCP, NULL, 0, 0);

	SOCKADDR_IN serverAddr;
	memset(&serverAddr, 0x00, sizeof(serverAddr));
	serverAddr.sin_family = AF_INET;   //IPv4
	serverAddr.sin_port = htons(SERVERPORT);
	serverAddr.sin_addr.S_un.S_addr = inet_addr(serverIP.c_str());
	
	int retval = WSAConnect(cSocket, (struct sockaddr*) & serverAddr, sizeof(serverAddr), NULL, NULL, NULL, NULL);
	WSAAsyncSelect(cSocket, hWnd, WM_SOCKET, FD_CLOSE | FD_READ);



	if (retval != -1) {
		std::cout << "서버 연결 완료\n";
		isConnect = true;
	}
	else {
		std::cout << "서버 연결 실패\n";
	}

	/*if (retval == -1) {
		std::cout << "서버와 연결실패 프로그램 종료" << std::endl;
	}
	exit(0);*/
	//return;


	cs_packet_login cs_loginPacket;
	cs_loginPacket.size = sizeof(cs_packet_login);
	cs_loginPacket.type = C2S_LOGIN;
	retval = send(cSocket, (char*)&cs_loginPacket, sizeof(cs_packet_login), 0);
	if (retval == SOCKET_ERROR)	std::cout << "socket error" << std::endl;



}

void GameFramework::OnProcessSocketMessage(HWND hWnd, UINT nMessageID, WPARAM wParam, LPARAM lParam)
{
	switch (WSAGETSELECTEVENT(lParam)) {
	case FD_READ: {

		int MaxByte = MAX_BUF_SIZE;
		char		ioBuffer[MAX_BUF_SIZE];
		memset(ioBuffer, 0, MAX_BUF_SIZE);
		int rest_byte = recv(cSocket, (char*)&ioBuffer, MAX_BUF_SIZE, 0);

		char* p = ioBuffer;
		int packetSize = 0;
		if (cl.pri_size != 0) packetSize = cl.packet_buf[0];


		while (rest_byte > 0) {
			if (packetSize == 0) packetSize = p[0];
			if (packetSize == 0) {
				break;
			}

			if (rest_byte >= packetSize - cl.pri_size)
			{
				memcpy(cl.packet_buf + cl.pri_size, p, packetSize - cl.pri_size);
				p += packetSize - cl.pri_size;
				rest_byte -= packetSize - cl.pri_size;
				cl.pri_size = 0;
				processPacket();
				packetSize = 0;
			}
			else {
				memcpy(cl.packet_buf + cl.pri_size, p, rest_byte);
				cl.pri_size += rest_byte;
				rest_byte = 0;
				p += rest_byte;
			}

		}

	}
				  break;
	case FD_CLOSE:
		std::cout << "연결 끊김 " << std::endl;
		closesocket(cSocket);
		break;
	}


}

void GameFramework::PrintFrame()
{
	gameTimer.GetFrameRate(frameRate + 10, 37);

	size_t nLength = _tcslen(frameRate);
	XMFLOAT3 pos, look;
	if (player) {
		pos = player->GetPosition();
		look = camera->GetLookVector();
	}
	_stprintf_s(frameRate + nLength, 90 - nLength, _T("(%.1f, %.1f, %.1f), LOOK : (%.1f, %.1f, %.1f)"), pos.x, pos.y, pos.z, look.x, look.y, look.z);

	::SetWindowText(hwnd, frameRate);
}

void GameFramework::OnProcessingMouseMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{
	switch (messageID)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		//마우스 캡쳐를 하고 현재 마우스 위치를 가져온다.
		isMouseDown = true;
		::SetCapture(hwnd);
		::GetCursorPos(&oldCursorPos);
		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		//마우스 캡쳐를 해제한다.
		::ReleaseCapture();
		isMouseDown = false;
		break;
	}
}
void GameFramework::OnProcessingKeyboardMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{
	switch (messageID)
	{
	case WM_KEYUP:
		switch (wparam)
		{
			/*‘F1’ 키를 누르면 1인칭 카메라, ‘F2’ 키를 누르면 스페이스-쉽 카메라로 변경한다, ‘F3’ 키를 누르면 3인칭 카메라로 변경한다.*/
		case VK_F1:
		case VK_F2:
		case VK_F3:
			if (player) {
				camera = player->ChangeCamera(DWORD(wparam - VK_F1 + 1), gameTimer.GetTimeElapsed());
			}
			break;
		case VK_ESCAPE:
			::PostQuitMessage(0);
			break;
		case VK_F4:
			//if (!isConnect) {
				
				//sendLoadMapPacket(0);
			//}
			break;
			//“F9” 키가 눌려지면 윈도우 모드와 전체화면 모드의 전환을 처리한다.
		case VK_F9:
			ChangeSwapChainState();
		}
		break;
	default:
		break;
	}
}
LRESULT CALLBACK GameFramework::OnProcessingWindowMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{
	switch (messageID)
	{

	case WM_SIZE:
	{
		windowClientWidth = LOWORD(lparam);
		windowClientHeight = HIWORD(lparam);
		break;
	}
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
		OnProcessingMouseMessage(hwnd, messageID, wparam, lparam);
		break;
	case WM_KEYDOWN:
		isKeyDown = true;
		OnProcessingKeyboardMessage(hwnd, messageID, wparam, lparam);
		break;
	case WM_KEYUP:
		isKeyDown = false;
		OnProcessingKeyboardMessage(hwnd, messageID, wparam, lparam);
		break;
	}
	return(0);
}

void GameFramework::ProcessFileIO(ID3D12Device * device, ID3D12GraphicsCommandList * commandList)
{
	processFile->LoadMap(mapFileNames[scene->mapIdx], device, commandList);
	
	player->SetPosition(StartPos(myid), true);
	UIINFOINCPU* info = scene->GetUIINFO();
	SkinnedInstancingShader* skinInsShader = reinterpret_cast<SkinnedInstancingShader*>(scene->Getshaders()[ShaderOrderSkinnedInstancing]);
	SkinnedAnimationObjectsShader* skinObjShader = reinterpret_cast<SkinnedAnimationObjectsShader*>(scene->Getshaders()[ShaderOrderSkinnedObject]);
	for (int i = 0; i < skinObjShader->GetBuffer().size(); ++i) {
		skinObjShader->GetBuffer()[i]->object->SetPosition(StartPos(skinObjShader->GetBuffer()[i]->object->id));
	}
	skinInsShader->activePotal = false;

	isLoadMapFinish = true;

	for (int i = 0; i < 3; ++i)
	{
		for (int j = 0; j < 3; ++j)
		{
			if (i * 3 + j == scene->mapIdx)
			{
				info->passedmap[i][j] = 2;
			}
			else if (info->passedmap[i][j] == 2)
			{
				info->passedmap[i][j] = 1;
			}
		}
	}
}

void GameFramework::GetAnimationNum(UCHAR keys[256], int character)
{
	int statusBody = 0, statusLeg = 0;
	if (character == PlayerJobRifle) {
		if (keys[87] & 0xF0) {
			statusLeg = MainCharacterWalk;
			// 앞	
		}
		if (keys[83] & 0xF0) {
			if (statusLeg == MainCharacterWalk)
				statusLeg = MainCharacterIdle;
			else
				statusLeg = MainCharacterWalkBack;
			// 뒤
		}
		if (keys[65] & 0xF0) {
			if (statusLeg != MainCharacterWalk || statusLeg != MainCharacterWalkBack)
				statusLeg = MainCharacterWalkLeft;
			// 왼
		}
		if (keys[68] & 0xF0) {
			if (statusLeg != MainCharacterWalk || statusLeg != MainCharacterWalkBack)
				statusLeg = MainCharacterWalkRight;
			else if (statusLeg == MainCharacterWalkRight)
				statusLeg = MainCharacterIdle;
			// 오
		}
		statusBody = statusLeg;

		if (keys[VK_RBUTTON] & 0xF0) {
			statusBody = MainCharacterAim;
		}

		if (keys[VK_LBUTTON] & 0xF0) {
			statusBody = MainCharacterFire;
		}

		//R button
		if (keys[82] & 0xF0) {
			if (statusLeg != MainCharacterIdle)
				statusBody = MainCharacterWalkingReload;
			else
			{
				statusLeg = MainCharacterReload;
				statusBody = MainCharacterReload;
			}
		}

		//Shift
		if (keys[160] & 0xF0) {
			if (statusBody == statusLeg) {
				if (statusLeg == MainCharacterWalk)
					statusLeg = MainCharacterRun;
				else if (statusLeg == MainCharacterWalkBack)
					statusLeg = MainCharacterRunBack;
				else if (statusLeg == MainCharacterWalkLeft)
					statusLeg = MainCharacterRunLeft;
				else if (statusLeg == MainCharacterWalkRight)
					statusLeg = MainCharacterRunRight;

				statusBody = statusLeg;
			}
		}

	}

	else if (character == PlayerJobSword)
	{
		if (keys[87] & 0xF0) {
			statusLeg = SwordCharacterWalk;
			// 앞	
		}
		if (keys[83] & 0xF0) {
			if (statusLeg == BowCharacterWalk)
				statusLeg = SwordCharacterIdle;
			else
				statusLeg = SwordCharacterWalkBack;
			// 뒤
		}
		if (keys[65] & 0xF0) {
			if (statusLeg != SwordCharacterWalk || statusLeg != SwordCharacterWalkBack)
				statusLeg = SwordCharacterWalkLeft;
			// 왼
		}
		if (keys[68] & 0xF0) {
			if (statusLeg != SwordCharacterWalk || statusLeg != SwordCharacterWalkBack)
				statusLeg = SwordCharacterWalkRight;
			else if (statusLeg == SwordCharacterWalkRight)
				statusLeg = SwordCharacterIdle;
			// 오
		}

		//Shift
		if (keys[160] & 0xF0) {
			if (statusLeg == SwordCharacterWalk)
				statusLeg = SwordCharacterRun;
			else if (statusLeg == SwordCharacterWalkBack)
				statusLeg = SwordCharacterRunBack;
			else if (statusLeg == SwordCharacterWalkLeft)
				statusLeg = SwordCharacterRunLeft;
			else if (statusLeg == SwordCharacterWalkRight)
				statusLeg = SwordCharacterRunRight;
		}

		if (keys[VK_LBUTTON] & 0xF0 && player->stemina > 0) {
			int random = rand() % 3;
			if (random == 0)
				statusLeg = SwordCharacterAttack;
			else if (random == 1)
				statusLeg = SwordCharacterAttack2;
			else if (random == 2)
				statusLeg = SwordCharacterAttack3;
		}

		statusBody = statusLeg;
	}

	else if (character == PlayerJobBow)
	{
		if (keys[87] & 0xF0) {
			statusLeg = BowCharacterWalk;
			// 앞	
		}
		if (keys[83] & 0xF0) {
			if (statusLeg == BowCharacterWalk)
				statusLeg = BowCharacterIdle;
			else
				statusLeg = BowCharacterWalkBack;
			// 뒤
		}
		if (keys[65] & 0xF0) {
			if (statusLeg != BowCharacterWalk || statusLeg != BowCharacterWalkBack)
				statusLeg = BowCharacterWalkLeft;
			// 왼
		}
		if (keys[68] & 0xF0) {
			if (statusLeg != BowCharacterWalk || statusLeg != BowCharacterWalkBack)
				statusLeg = BowCharacterWalkRight;
			else if (statusLeg == BowCharacterWalkRight)
				statusLeg = BowCharacterIdle;
			// 오
		}
		//Shift
		if (keys[160] & 0xF0) {
			if (statusLeg == BowCharacterWalk)
				statusLeg = BowCharacterRun;
			else if (statusLeg == BowCharacterWalkBack)
				statusLeg = BowCharacterRunBack;
			else if (statusLeg == BowCharacterWalkLeft)
				statusLeg = BowCharacterRunLeft;
			else if (statusLeg == BowCharacterWalkRight)
				statusLeg = BowCharacterRunRight;
		}

		statusBody = statusLeg;

		//F button
		if (keys[70] & 0xF0) {
			int random = rand() % 2;
			if (random == 0) {
				statusLeg = BowCharacterPunch;
				statusBody = statusLeg;
			}
			else {
				statusLeg = BowCharacterKick;
				statusBody = statusLeg;
			}
		}		

		if (keys[VK_LBUTTON] & 0xF0) {
			statusBody = BowCharacterDrawArrow;
			statusLeg = BowCharacterDrawArrow;
		}

	}
	player->ChangePlayerAnimation(statusBody, statusLeg, character);
	UIINFOINCPU *info = scene->GetUIINFO();
	info->stemina = player->stemina / player->maxStemina;

	if (player->stemina > 0)
	{
		bool isSwordPlayerAttack = player->ShowEffect(device, commandList[swapChainBufferIndex][CommandListStateMain], gameTimer.GetTotalTime());

		if (isSwordPlayerAttack)
		{
			bool hitable = false;
			int upset = player->skinnedAnimationController->animationTracks[0].upanimationSet;
			float position = player->skinnedAnimationController->animationSets->animationSets[player->skinnedAnimationController->animationTracks[0].upanimationSet]->position;

			if (upset == 10) {
				if (position > 0.75f && position < 1.15f)
					hitable = true;
			}
			if (upset == 11) {
				if (position > 0.6f && position < 0.9f)
					hitable = true;
			}
			if (upset == 12) {
				if (position > 1.0f && position < 1.2f)
					hitable = true;
			}
			if (hitable == true)
			{
				SwordAttack();
			}
		}
	}

	if (player->playerJob == PlayerJobBow)
	{
		if (player->upAnimationStatus == BowCharacterPunch || player->upAnimationStatus == BowCharacterKick)
		{
			bool hitable = false;
			float position = player->skinnedAnimationController->animationSets->animationSets[player->skinnedAnimationController->animationTracks[0].upanimationSet]->position;
			bool isPunch = (player->upAnimationStatus == BowCharacterPunch) ? true : false;
			if (gameTimer.GetTotalTime() - player->attackCoolTime > 0.1f)
			{
				if (isPunch == true)
				{
					if (position > 0.20f && position < 0.45f)
						hitable = true;
				}
				else
				{
					if (position > 0.40f && position < 0.75f)
						hitable = true;
				}
				if(hitable == true)
					BowNearAttack(isPunch);
			}
		}

		if (player->skinnedAnimationController->animationSets->animationSets[player->skinnedAnimationController->animationTracks[0].upanimationSet]->isRelease == true)
		{
			BowAttack();
			player->skinnedAnimationController->animationSets->animationSets[player->skinnedAnimationController->animationTracks[0].upanimationSet]->isRelease = false;
		}
	}
	
}

void GameFramework::sendLoadMapPacket(int mapNum)
{
	cs_packet_loadmap cs_p_lm;
	cs_p_lm.type = C2S_LOADMAP;
	cs_p_lm.size = sizeof(cs_packet_loadmap);
	cs_p_lm.mapNum = mapNum;
	send(cSocket, (char*)&cs_p_lm, sizeof(cs_packet_loadmap), 0);
}

void GameFramework::sendhitpacket(int id, int damage, XMFLOAT3 pos)
{
	cs_packet_attack cs_p_lm;
	cs_p_lm.type = C2S_ATTACK;
	cs_p_lm.size = sizeof(cs_packet_attack);
	cs_p_lm.id = id;
	cs_p_lm.damage = damage;
	cs_p_lm.pos = pos;
	send(cSocket, (char*)&cs_p_lm, sizeof(cs_packet_attack), 0);

}

void GameFramework::sendZombieHitPacket(int damage)
{
	cs_packet_zombieattack cs_p_za;
	cs_p_za.type = C2S_ZOMBIEATTACK;
	cs_p_za.size = sizeof(cs_packet_zombieattack);
	cs_p_za.damage = damage;
	send(cSocket, (char*)&cs_p_za, sizeof(cs_packet_zombieattack), 0);
}

void GameFramework::sendHillPacket()
{
	cs_packet_hill cs_p_h;
	cs_p_h.type = C2S_HILL;
	cs_p_h.size = sizeof(cs_packet_hill);
	cs_p_h.hp = player->hp;
	send(cSocket, (char*)&cs_p_h, sizeof(cs_packet_hill), 0);
}

float GameFramework::CalcuateDamage(float strPower, float defPower, float hitArea)
{
	return (strPower * 100 / (100 + defPower)) * hitArea;
}

void GameFramework::ProcessInput()
{
	UIINFOINCPU *info = scene->GetUIINFO();
	static UCHAR keyBuffer[256];
	int direction = 0;
	/*키보드의 상태 정보를 반환한다. 화살표 키(‘→’, ‘←’, ‘↑’, ‘↓’)를 누르면 플레이어를 오른쪽/왼쪽(로컬 x-축), 앞/
	뒤(로컬 z-축)로 이동한다. ‘Page Up’과 ‘Page Down’ 키를 누르면 플레이어를 위/아래(로컬 y-축)로 이동한다.*/
	bool move = false, shift = false, reload = false, Lclick = false, Rclick = false, Aup = false, Adown = false, Lmove = false, Rmove = false, Bmove = false, Hit = false, Death = false;
	if (player->haveModel)
		player->skinnedAnimationController->bodyRotate = 0.f;
	if (::GetKeyboardState(keyBuffer))
	{
		if (keyBuffer[87] & 0xF0) {
			direction += DirectionForword;
			move = true;
		}
		if (keyBuffer[83] & 0xF0) {
			direction += DirectionBackword;
			move = true;
			Bmove = true;
		}
		if (keyBuffer[65] & 0xF0) {
			direction += DirectionLeft;
			move = true;
			Lmove = true;
			//player->skinnedAnimationController->bodyRotate = -90.f;
		}
		if (keyBuffer[68] & 0xF0) {
			direction += DirectionRight;
			move = true;
			Rmove = true;
			//player->skinnedAnimationController->bodyRotate = 90.f;
		}
		//Shift
		if (keyBuffer[160] & 0xF0) {
			shift = true;
		}
		//R button
		if (keyBuffer[82] & 0xF0) {
			reload = true;
			player->SetPlayerAmmo();
			info->ammo = player->ammo;
		}

		if (keyBuffer[79] & 0xF0) {
			Aup = true;
		}
		if (keyBuffer[80] & 0xF0) {
			Adown = true;
		}

		if (keyBuffer[VK_PRIOR] & 0xF0) direction |= DirectionUp;
		if (keyBuffer[VK_NEXT] & 0xF0) direction |= DirectionDown;

		if (keyBuffer[49] & 0xF0) {
			Hit = true;
		}
		if (keyBuffer[50] & 0xF0) {
			Death = true;
		}
		if ((keyBuffer[0x62] & 0xF0 || keyBuffer[0x42] & 0xF0) && isKeyDown == true) {
			isClearMap = true;
			isKeyDown = false;
		}
		if (keyBuffer[77] & 0xF0 && gameTimer.GetTotalTime() - showBoundingCoolTime > 0.3f)
		{
			showBoundingCoolTime = gameTimer.GetTotalTime();
			if (showBounding)
				showBounding = false;
			else
				showBounding = true;
			
			if (player->haveModel)
				player->SetShowBounding(showBounding);
			reinterpret_cast<InstancingShader*>(scene->Getshaders()[ShaderOrderInstancing])->SetShowBounding(showBounding);
			reinterpret_cast<SkinnedInstancingShader*>(scene->Getshaders()[ShaderOrderSkinnedInstancing])->SetShowBounding(showBounding);
			reinterpret_cast<SkinnedAnimationObjectsShader*>(scene->Getshaders()[ShaderOrderSkinnedObject])->SetShowBounding(showBounding);

		}
		if (keyBuffer[VK_PRIOR] & 0xF0) direction |= DirectionUp;
		if (keyBuffer[VK_NEXT] & 0xF0) direction |= DirectionDown;
		
	}
	float deltaX = 0.0f, deltaY = 0.0f;
	POINT cursorPos{};
	/*마우스를 캡쳐했으면 마우스가 얼마만큼 이동하였는 가를 계산한다. 마우스 왼쪽 또는 오른쪽 버튼이 눌러질 때의
	메시지(WM_LBUTTONDOWN, WM_RBUTTONDOWN)를 처리할 때 마우스를 캡쳐하였다. 그러므로 마우스가 캡쳐된
	것은 마우스 버튼이 눌려진 상태를 의미한다. 마우스 버튼이 눌려진 상태에서 마우스를 좌우 또는 상하로 움직이면 플
	레이어를 x-축 또는 y-축으로 회전한다.*/
	if (info->scenenum == SCENE_INGAME || info->scenenum == SCENE_INGAME_ZOOM)
	{
		::SetCapture(hwnd);
		::SetCursor(NULL);
		::GetCursorPos(&cursorPos);
		deltaX = (float)(cursorPos.x - oldCursorPos.x) / 3.0f;
		deltaY = (float)(cursorPos.y - oldCursorPos.y) / 3.0f;
		::SetCursorPos(oldCursorPos.x, oldCursorPos.y);
		if (keyBuffer[VK_LBUTTON] & 0xF0)
			Lclick = true;
		else if (keyBuffer[VK_RBUTTON] & 0xF0)
			Rclick = true;
		::ReleaseCapture();
	}
	else if (::GetCapture() == hwnd)
	{
		//마우스 커서를 화면에서 없앤다(보이지 않게 한다).
		::SetCursor(NULL);
		//현재 마우스 커서의 위치를 가져온다.
		::GetCursorPos(&cursorPos);
		//마우스 버튼이 눌린 상태에서 마우스가 움직인 양을 구한다.
		deltaX = (float)(cursorPos.x - oldCursorPos.x) / 3.0f;
		deltaY = (float)(cursorPos.y - oldCursorPos.y) / 3.0f;
		//마우스 커서의 위치를 마우스가 눌려졌던 위치로 설정한다.
		::SetCursorPos(oldCursorPos.x, oldCursorPos.y);

		if (keyBuffer[VK_LBUTTON] & 0xF0)
			Lclick = true;
		else
		{
			if(isMouseDown == false)
				isMouseDown = true;
		}
		
		if (keyBuffer[VK_RBUTTON] & 0xF0 && !Lclick)
			Rclick = true;
	}
	//마우스 또는 키 입력이 있으면 플레이어를 이동하거나(dwDirection) 회전한다(cxDelta 또는 cyDelta).
	auto curTime = std::chrono::high_resolution_clock::now();
	if ((direction != 0) || (deltaX != 0.0f) || (deltaY != 0.0f))
	{
		if (deltaX || deltaY)
		{
			/*deltaX는 y-축의 회전을 나타내고 deltaY는 x-축의 회전을 나타낸다. 오른쪽 마우스 버튼이 눌려진 경우
			cxDelta는 z-축의 회전을 나타낸다.*/
			if (keyBuffer[VK_RBUTTON] & 0xF0)
			{
				player->Rotate(deltaY, 0.0f, -deltaX);
			}
			else
			{
				player->Rotate(deltaY, deltaX, 0.0f);
			}
		}
		/*플레이어를 dwDirection 방향으로 이동한다(실제로는 속도 벡터를 변경한다). 이동 거리는 시간에 비례하도록 한다.
		플레이어의 이동 속력은 (50/초)로 가정한다.*/
	}
	float elpasedTime = gameTimer.GetTimeElapsed();

	if (elpasedTime > 0.1f) {
		elpasedTime = 0.1f;
	}
	
	int etime = int(elpasedTime * 1000);

	if (player->haveModel) {
		if (curTime - cl.prev_time > std::chrono::milliseconds(etime)) { // 프레임 타임에 한번씩 move 함수를 호출한다 .
			float bias = 0;
			if (shift && player->stemina > 0) {
				bias = PLAYERSPEED;
				player->SetPlayerStemina(-(elpasedTime * 5));
				info->stemina = player->stemina / player->maxStemina;
			}

			XMFLOAT3 beforePos = player->GetPosition();
			player->SetDir(direction);
			player->Move(direction, PLAYERSPEED + bias, false , elpasedTime);
			if (CollisionCheck(player , true)) {

			}
			else {
				player->RealMove();
			}
			
			if (camera->GetMode() != SPACESHIP_CAMERA)
				ProcessQuadTree(player, true , false);
			cl.prev_time = std::chrono::high_resolution_clock::now();

		}

		player->Update(gameTimer.GetTimeElapsed());
	}

	//플레이어를 실제로 이동하고 카메라를 갱신한다. 중력과 마찰력의 영향을 속도 벡터에 적용한다.

	if (player->haveModel)
		player->skinnedAnimationController->upperbodyRotate = deltaX;

	int status = MainCharacterIdle;

	float totalTime = gameTimer.GetTotalTime();

	//player->ChangePlayerAnimation(status);
	if (player->haveModel) {
		SkinnedAnimationObjectsShader* skinObjShader = reinterpret_cast<SkinnedAnimationObjectsShader*>(scene->Getshaders()[ShaderOrderSkinnedObject]);
		
		for (int i = 0; i < skinObjShader->GetBuffer().size(); ++i)
		{
			info->hp[skinObjShader->GetBuffer()[i]->id] = skinObjShader->GetBuffer()[i]->object->hp / 100;
		}

		GetAnimationNum(keyBuffer, player->playerJob);

		info->hp[myid] = player->hp / player->maxHp;
		info->stemina = player->stemina / player->maxStemina;
	}
	if (Rclick && gameTimer.GetTotalTime() - zoomCoolTime > 0.3f && player->playerJob == PlayerJobRifle)
	{

		if (info->scenenum == SCENE_INGAME && player->GetCamera()->GetMode() == THIRD_PERSON_CAMERA && info->playerJob[myid] == 0)
		{
			camera = player->ChangeCamera(ZOOM_MODE_CAMERA, gameTimer.GetTimeElapsed());
			info->scenenum = SCENE_INGAME_ZOOM;
		}
		else if (info->scenenum == SCENE_INGAME_ZOOM && player->GetCamera()->GetMode() == ZOOM_MODE_CAMERA && info->playerJob[myid] == 0)
		{
			camera = player->ChangeCamera(THIRD_PERSON_CAMERA, gameTimer.GetTimeElapsed());
			info->scenenum = SCENE_INGAME;
		}
		zoomCoolTime = gameTimer.GetTotalTime();
	}
	if ((isMouseDown == true) && (Lclick == true) && (gameTimer.GetTotalTime() - clickCoolTime > 0.1f))
	{
		clickCoolTime = gameTimer.GetTotalTime();
		if(camera->GetMode() == SPACESHIP_CAMERA)
			ScreenPicking(cursorPos);
		else if((camera->GetMode() == THIRD_PERSON_CAMERA || camera->GetMode() == ZOOM_MODE_CAMERA) && info->scenenum == SCENE_INGAME_ITEM)
			ScreenPicking(cursorPos);
		else {
			ScreenToClient(hwnd, &cursorPos);
			if (player->playerJob == PlayerJobRifle)
				RifleAttack();
		}
	}
	if (player->playerJob == PlayerJobBow)
		ArrowCollision();

	curTime = std::chrono::high_resolution_clock::now();
	
	
	if (isGameStart == true) {
		if (curTime - sendtime > std::chrono::milliseconds(33)) {
			cs_packet_move cs_p_m;
			cs_p_m.type = (char)C2S_MOVE;
			cs_p_m.size = sizeof(cs_packet_move);
			cs_p_m.direction = (char)direction;
			cs_p_m.pos = player->GetPosition();
			cs_p_m.look = player->GetLookVector();
			cs_p_m.velocity = player->GetVelocity();
			cs_p_m.aniTrack = (char)status;
			cs_p_m.potal = player->GetPotal();
			cs_p_m.aniTrackUp = (char)player->skinnedAnimationController->nowupAnimation;
			cs_p_m.aniTrackLo = (char)player->skinnedAnimationController->nowloAnimation;
			send(cSocket, (char*)&cs_p_m, sizeof(cs_packet_move), 0);
			sendtime = std::chrono::high_resolution_clock::now();
		}
	}

	UpdateLightCameras();
}
void GameFramework::AnimateObjects()
{
	if (scene) {
		scene->AnimateObjects(gameTimer.GetTimeElapsed(), camera);
		scene->UpdateShaderVariables();
	}
	if (player) player->AnimateObject(gameTimer.GetTimeElapsed());

	//move other characters
	SkinnedAnimationObjectsShader* saShader = static_cast<SkinnedAnimationObjectsShader*>(scene->Getshaders()[3]);
	std::vector<SkinnedObjectBuffer*> &objectBuffer = saShader->GetObjectBuffer();

	for (int i = 0; i < objectBuffer.size(); ++i) {
		if (objectBuffer[i]->isActive) {
			if (objectBuffer[i]->direction != DirectionNoMove) {
				saShader->Move(objectBuffer[i]->direction, PLAYERSPEED, gameTimer.GetTimeElapsed(), i);
				ProcessQuadTree(objectBuffer[i]->object, false , false);				
			}		
		}
	}
	ParticleShader* pShader = reinterpret_cast<ParticleShader*>(scene->Getshaders()[ShaderOrderParticle]);
	SkinnedInstancingShader* siShader = static_cast<SkinnedInstancingShader*>(scene->Getshaders()[2]);
	std::vector<SkinnedInstanceBuffer*> buffer = siShader->GetBuffer();

	for (int i = 0; i < buffer.size(); ++i) {
		
		for (int j = 0; j < buffer[i]->objects.size(); ++j) {
			auto& siobj =  buffer[i]->objects;
			auto curtime = std::chrono::high_resolution_clock::now();
			int tempTime = int(gameTimer.GetTimeElapsed() * 1000);
			if (curtime - siobj[j]->GetTime() > std::chrono::milliseconds(tempTime) && siobj[j]->GetDir() != DirectionNoMove) {
				int state = siobj[j]->GetState();
				if (state == WANDER || state == IDLE ) {
					siobj[j]->Move(siobj[j]->GetDir(), ZOMBIESPEED, false, gameTimer.GetTimeElapsed() , tZombies);
				}
				if (state == CHASE) {
					siobj[j]->Move(siobj[j]->GetDir(), ZOMBIERUN, false, gameTimer.GetTimeElapsed(), tZombies);
				}
				if (state == ATTACK || state == DEATH) {
					siobj[j]->Move(siobj[j]->GetDir(), 0.0f, false, gameTimer.GetTimeElapsed(), tZombies);
				}
				if (CollisionCheck(siobj[j], false) == false) {
						siobj[j]->RealMove();
					siobj[j]->SetTime(std::chrono::high_resolution_clock::now());
					GetHeight(siobj[j]);
				}
				else {
					siobj[j]->SetTime(std::chrono::high_resolution_clock::now());
				}
			}
			if (isGameStart) {
				// 좀비 공격
				bool isHit = false;
				int aniNum = siobj[j]->skinnedAnimationController->animationTracks[0].upanimationSet;
				float position = siobj[j]->skinnedAnimationController->animationSets->animationSets[siobj[j]->skinnedAnimationController->animationTracks[0].upanimationSet]->position;
				BoundingOrientedBox playerObb = player->GetobbVector()[0];
				if (_isnan(playerObb.Center.x) == true) break;
				XMFLOAT3 pos;

				if (buffer[i]->isBoss == false) 
				{
					if ((aniNum >= ZombieAttack1 && aniNum <= ZombieKick) && gameTimer.GetTotalTime() - siobj[j]->zombieAttackCoolTime > 1.0f)
					{
						std::vector<BoundingOrientedBox> obbs = siobj[j]->GetobbVector();
						if (aniNum == ZombieKick)
						{
							if (position >= 0.23 && position <= 1.08) {
								obbs[2].Extents = Vector3::ScalarProduct(obbs[2].Extents, 1.5f, false);
								if (obbs[2].Intersects(playerObb))
								{
									pos = obbs[2].Center;
									isHit = true;
								}
							}
						}
						else if (aniNum == ZombieAttack1)
						{
							if (position >= 1.18 && position <= 1.4) {
								obbs[6].Extents = Vector3::ScalarProduct(obbs[6].Extents, 1.5f, false);
								obbs[8].Extents = Vector3::ScalarProduct(obbs[8].Extents, 1.5f, false);
								if (obbs[6].Intersects(playerObb) || obbs[8].Intersects(playerObb))
								{
									pos = obbs[6].Center;
									isHit = true;
								}
							}
						}
						else if (aniNum == ZombieAttack2)
						{
							if (position >= 0.51 && position <= 1.09) {
								obbs[6].Extents = Vector3::ScalarProduct(obbs[6].Extents, 1.5f, false);
								if (obbs[6].Intersects(playerObb))
								{
									pos = obbs[6].Center;
									isHit = true;
								}
							}
						}
					}
				}
				else
				{
					if ((aniNum >= BossPunch && aniNum <= BossJumpAttack) && gameTimer.GetTotalTime() - siobj[j]->zombieAttackCoolTime > 1.0f)
					{
						std::vector<BoundingOrientedBox> obbs = siobj[j]->GetobbVector();
						if (aniNum == BossPunch)
						{
							if (position >= 0.13 && position <= 0.3) {
								obbs[6].Extents = Vector3::ScalarProduct(obbs[6].Extents, 1.5f, false);
								if (obbs[6].Intersects(playerObb))
								{
									pos = obbs[6].Center;
									isHit = true;
								}
							}
						}
						else if (aniNum == BossSwiping)
						{
							if (position >= 1.0 && position <= 1.35) {
								obbs[8].Extents = Vector3::ScalarProduct(obbs[8].Extents, 1.5f, false);
								if (obbs[8].Intersects(playerObb))
								{
									pos = obbs[8].Center;
									isHit = true;
								}
							}
						}
						else if (aniNum == BossJumpAttack)
						{
							if (position >= 1.4 && position <= 1.5) {

								if (Vector3::Length(Vector3::Subtract(playerObb.Center, obbs[0].Center)) < 5.0f)
								{
									pos = playerObb.Center;
									isHit = true;
								}
								float age = 1.0f;
								XMFLOAT4 rColor{ 0.75, 0.75f, 0.75f, 1.0f };
								XMFLOAT3 bosPosition = siobj[j]->GetPosition();

								if (gameTimer.GetTotalTime() - jumpAttackEffectCoolTime > 1.0f) {
									jumpAttackEffectCoolTime = gameTimer.GetTotalTime();
									XMFLOAT3 addPos[9]{ XMFLOAT3(1.0f, 0.0f, 0.0f), XMFLOAT3(0.0f, 0.0f, 1.0f), XMFLOAT3(-1.0f, 0.0f, 0.0f),
									XMFLOAT3(0.0f, 0.0f, -1.0f) , XMFLOAT3(1.0f, 0.0f, 1.0f), XMFLOAT3(1.0f, 0.0f, -1.0f), XMFLOAT3(-1.0f, 0.0f, 1.0f), XMFLOAT3(-1.0f, 0.0f, -1.0f) };
									for(int i=0; i<9; ++i)
										pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
											ParticleInfoInCPU{ pShader->copyBufferMap["RunDust"], pShader->textureMap["Dust"], PARTICLE_TYPE_SMALLER,
											Vector3::Add(bosPosition, addPos[i]), XMFLOAT2(1.0f, 1.0f), rColor, age, age, 1.0f, 0.3f });
								}
							}
						}
					}
				}


				if (isHit) {
					siobj[j]->zombieAttackCoolTime = gameTimer.GetTotalTime();
					float strPower = siobj[j]->strikingPower;
					float defPower = player->defencivePower;
					float damage = CalcuateDamage(strPower, defPower, 1.0f);
					sendZombieHitPacket(damage);

					float age = 0.3f;
					XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
					pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
						ParticleInfoInCPU{ pShader->copyBufferMap["Blood"], pShader->textureMap["Blood"], PARTICLE_TYPE_DISAPPEAR,
						pos, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 0.3f });

					sendhitpacket(-1, 0.0f, pos);
				}
			}
		}
	}

	player->SetPotal(NOPOTAL);

	//포탈과 플레이어 위치검사



	ParticleShader* particleShader = static_cast<ParticleShader*>(scene->Getshaders()[4]);

	std::vector<bool>& isRender = particleShader->GetIsRender();
	std::vector<ParticleInfoInCPU> pInfo = particleShader->GetParticles();
	UINT way = Scene::FIndCanGoWay();
	

	if (allKill == true) {
		for (int i = 0; i < isRender.size(); ++i)
		{
			// 갈수 있는 길만 포탈 활성화.
			if (isRender[i] == true && pInfo[i].type == PARTICLE_TYPE_TORNADO)
			{
				if ((pInfo[i].position.x < -150.0f) && (way & CanGoWayLeft))
				{
					if (Vector3::Length(Vector3::Subtract(player->GetPosition(), pInfo[i].position)) < 10.0f)
						player->SetPotal(1);
				}
				else if ((pInfo[i].position.x > 150.0f) && (way & CanGoWayRight))
				{
					if (Vector3::Length(Vector3::Subtract(player->GetPosition(), pInfo[i].position)) < 10.0f)
						player->SetPotal(2);
				}
				else if ((pInfo[i].position.z < -150.0f) && (way & CanGoWayDown))
				{
					if (Vector3::Length(Vector3::Subtract(player->GetPosition(), pInfo[i].position)) < 10.0f)
						player->SetPotal(3);
				}
				else if ((pInfo[i].position.z > 150.0f) && (way & CanGoWayUp))
				{
					if (Vector3::Length(Vector3::Subtract(player->GetPosition(), pInfo[i].position)) < 10.0f)
						player->SetPotal(4);
				}
			}
		}
	}
	UIINFOINCPU* info = scene->GetUIINFO();


	if (siShader->allKill == true) {
		if (info->scenenum == SCENE_INGAME)
		{
			isMouseDown = false;

			info->scenenum = SCENE_INGAME_ITEM;
			int random = rand() % 2;
			if (random == 0)
			{
				info->newitem = rand() % 11 + 8;
				player->SetAccessoryNum(info->newitem - 8);
			}
			else
			{
				if (player->playerJob == PlayerJobRifle)
				{
					info->newitem = rand() % 6 + 19;
					player->SetWeaponNum(info->newitem - 19);
					player->SetPlayerAmmo();
					info->ammo = player->ammo;
				}
				else if (player->playerJob == PlayerJobSword)
				{
					info->newitem = rand() % 6 + 25;
					player->SetWeaponNum(info->newitem - 25);
				}
				else if (player->playerJob == PlayerJobBow)
				{
					info->newitem = rand() % 5 + 31;
					player->SetWeaponNum(info->newitem - 31);
				}
			}
		}
		siShader->allKill = false;
	}
	if (player->isHill == true)
	{
		sendHillPacket();
		player->isHill = false;
	}

	if (info->scenenum == SCENE_LOADING && isGameStart == true && gameTimer.GetTotalTime() - loadingCoolTime > 3.0f)
	{
		info->scenenum = SCENE_INGAME;
	}
	if (siShader->victory == true)
	{
		closesocket(cSocket);
		info->scenenum = SCENE_VICTORY;
	}

}

void GameFramework::MoveToNextFrame()
{
	UINT64 fenceValue = fenceValues[swapChainBufferIndex];
	HRESULT hresult = commandQueue->Signal(fence, fenceValue);
	if (fence->GetCompletedValue() < fenceValue)
	{
		fence->SetEventOnCompletion(fenceValue, fenceEvent);
		::WaitForSingleObject(fenceEvent, INFINITE);
	}
}

void GameFramework::GarbageCollection()
{
	if (scene)
		scene->GarbageCollection();
}

void GameFramework::ProcessQuadTree(GameObject* gobject , bool isPlayer , bool isSkined)
{
	TerrainShader* tShader = static_cast<TerrainShader*>(scene->Getshaders()[0]);
	QuadTree* root = tShader->GetQuadRoot();

	QuadTree* Node = root;
	HeightMapTerrain* terrain = scene->GetTerrain();
	HeightMapGridMesh* tMesh = static_cast<HeightMapGridMesh*>(terrain->GetMesh(0));
	XMFLOAT4X4 itt = Matrix4x4::Inverse(terrain->GetTransform());
	BoundingOrientedBox pWorldbounding;
	if (isSkined == false) {
		pWorldbounding = gobject->GetOBB();
		
	}
	else {
		pWorldbounding = gobject->GetobbVector()[0];
	}
	pWorldbounding.Center = Vector3::TransformCoord(pWorldbounding.Center, itt);
	// Terrain의 모델좌표계로 변환한다.
	bool loop = true;
	bool outside = false;
	if (pWorldbounding.Center.x > 512.0f || pWorldbounding.Center.x < 2.0f || pWorldbounding.Center.z > 512.0f || pWorldbounding.Center.z < 2.0f)
	{
		outside = true;
		// 맵의 크기를 벗어나면 outside가 true
	}

	//플레이어도 바운딩 박스가 있어야함 
	if (outside == false)
	{
		while (1) {

			if (Node->AABBbOX.Intersects(pWorldbounding))
				for (int i = 0; i < 4; ++i) {
					if (Node->child[i] != NULL) {
						if (Node->child[i]->AABBbOX.Intersects(pWorldbounding)) {
							Node = Node->child[i];
							break;
						}
					}
					else {
						loop = false;
						break;
					}
				}
			if (loop == false)
				break;
		}
		//프리미티브의 개수만큼 삼각형을 만들어 검사하자
		int terrainWidth = 257;
		int nVertexNum = Node->vertexNum;
		int nStartIndex = Node->vStartIndex;
		int nPriNum = Node->primitiveNum;
		int count = 0;
		float finalDistance = FLT_MAX;
		pWorldbounding.Center.y += 100.0f;
		int vertexNum = tMesh->GetVertexNum();
		for (int z = 0; z <= Node->length; z++) { // 이건 노드에서의 length 
			for (int x = 0; x <= Node->width; x++) { // 이건 노드에서의 width 

				// 내 맘대로 삼각형 구성할꺼임
				// 한 점당 삼각형 두개다. 그러므로 맨마지막 점은 삼각형 구성 안함 
				// 맨 마지막 Length 점도 삼각형 구성을 안함 
				if (nStartIndex + count > tMesh->GetVertexNum()) {
					break;
				}
				if ((((nStartIndex + x) + ((z + 1) * terrainWidth) + 1)) > tMesh->GetVertexNum()){
					continue;
				}

				if ((((nStartIndex + x) + (z * terrainWidth) / terrainWidth) != terrainWidth - 1)) {

					float distance = FLT_MAX;

					//삼각형 아래 삼각형 구성
					XMFLOAT3 dir = XMFLOAT3(0, -1, 0);

					auto pPos = XMLoadFloat3((XMFLOAT3 *)&pWorldbounding.Center);

					auto direction = XMLoadFloat3((XMFLOAT3 *)&dir);


					auto v0 = XMLoadFloat3(&tMesh->GetDiffuseTexturedVertex()[(nStartIndex + x) + z * terrainWidth].position);
					auto v1 = XMLoadFloat3(&tMesh->GetDiffuseTexturedVertex()[(nStartIndex + x) + (z * terrainWidth) + terrainWidth].position);
					auto v2 = XMLoadFloat3(&tMesh->GetDiffuseTexturedVertex()[(nStartIndex + x) + (z * terrainWidth) + 1].position);


					if (TriangleTests::Intersects(pPos, direction, v0, v1, v2, distance)) {
						if (distance < finalDistance) {
							finalDistance = distance;
						}
					}
					//위에 삼각형 구성 
					auto v3 = XMLoadFloat3(&tMesh->GetDiffuseTexturedVertex()[(nStartIndex + x) + (z + 1) * terrainWidth].position);
					auto v4 = XMLoadFloat3(&tMesh->GetDiffuseTexturedVertex()[(nStartIndex + x) + ((z + 1) * terrainWidth) + 1].position);
					auto v5 = XMLoadFloat3(&tMesh->GetDiffuseTexturedVertex()[(nStartIndex + x) + (z * terrainWidth) + 1].position);

					distance = FLT_MAX;

					if (TriangleTests::Intersects(pPos, direction, v3, v4, v5, distance)) {
						if (distance < finalDistance) {
							finalDistance = distance;
						}
					}
					float pHeight = gobject->GetPosition().y;

				}
				count++;

			}
		}
		XMFLOAT3 finalpPos = gobject->GetPosition();
		if (finalDistance >= FLT_MAX - 1) {
			std::cout << "NO" << std::endl;
		}
		else {
			finalpPos = Vector3::TransformCoord(Vector3::Add(pWorldbounding.Center, Vector3::ScalarProduct(XMFLOAT3(0.0f, -1.0f, 0.0f), finalDistance)), terrain->GetTransform());
		}
		if (isPlayer) {
			this->player->SetPosition(finalpPos , true);
		}
		else {
			gobject->SetPosition(finalpPos);
		}
	}
}

void GameFramework::GetHeight(GameObject * gObject)
{
	GameObject* terrain = scene->GetTerrain();
	HeightMapGridMesh* mesh =static_cast<HeightMapGridMesh*>(terrain->GetMesh(0));
	auto tVertices = mesh->GetDiffuseTexturedVertex();
	
	
	XMFLOAT3 pos = gObject->GetPosition();
	XMFLOAT3 lpos = Vector3::Add(pos, XMFLOAT3(257.0f, 10.0f, 257.0f));
	//위치로 부터 속한 타일을 알아야함
	XMFLOAT3 dir = XMFLOAT3(0.0f, -1.0f, 0.0f);
	float finaldist = FLT_MAX;
	auto vlpos = XMLoadFloat3(&lpos);
	auto vdir = XMLoadFloat3(&dir);
	int tx = int(lpos.x) / 2;
	int tz = int(lpos.z) / 2;
	bool isCrash = false;
	int tileNumber = 0;

	int ftx = tx + 5;
	int ftz = tz + 5;

	if (ftx > 257) {
		ftx = 257;
	}
	if (ftz > 257) {
		ftz = 257;
	}


	for (int z = tz; z < ftz; ++z) {
		for (int x = tx; x < ftx; ++x) {
			float dist = FLT_MAX;

			if ((x + 1) != (ftx - 1) && (z + 1) != (ftz - 1)) {
				int idx = x + (257 * z);
				if (tileNumber != -1) {

					auto lv1 = XMLoadFloat3(&tVertices[idx].position);
					auto lv2 = XMLoadFloat3(&tVertices[idx + 257].position);
					auto lv3 = XMLoadFloat3(&tVertices[idx + 1].position);

					if (TriangleTests::Intersects(vlpos, vdir, lv1, lv2, lv3, dist)) {
						if (dist < finaldist) {
							finaldist = dist;
						}
						isCrash = true;
					}
					float dist = FLT_MAX;
					lv1 = XMLoadFloat3(&tVertices[idx + 257].position);
					lv2 = XMLoadFloat3(&tVertices[idx + 257 + 1].position);
					lv3 = XMLoadFloat3(&tVertices[idx + 1].position);

					if (TriangleTests::Intersects(vlpos, vdir, lv1, lv2, lv3, dist)) {
						if (dist < finaldist) {
							finaldist = dist;
						}
						isCrash = true;
					}

				}
				else {

				}

				if (isCrash) break;
			}
			if (isCrash) break;

		}

	}
	if (isCrash) {
		auto finalPos = Vector3::Add(Vector3::Add(lpos, Vector3::ScalarProduct(dir, finaldist)), XMFLOAT3(-257.0f, 0.0f, -257.0f));
		gObject->SetPosition(finalPos);
	}
	else {
		std::cout << "타일 넘버 찾기 실패" << std::endl;
	}

}

void GameFramework::ScreenPicking(POINT cursorPos)
{
	UIINFOINCPU* info = scene->GetUIINFO();
	// GameStart, Lobby
	if (camera->GetMode() == SPACESHIP_CAMERA)
	{
		auto tempCursorPos = cursorPos;
		ScreenToClient(hwnd, &tempCursorPos);
		int playerNum = 0;
		if (info->scenenum == SCENE_START)
		{
			info->scenenum = SCENE_LOBBY;
		}
		else if (info->scenenum == SCENE_LOBBY)
		{
			bool changeState = false;
			
			if ((730 < tempCursorPos.x && tempCursorPos.x < 1260) && (610 < tempCursorPos.y && tempCursorPos.y < 690))
			{
				// 방장이라면 시작버튼을 누른 것이 된다.
				if(info->isHead == true)
					info->playerState[myid] = 2;
				else {
					if (info->playerState[myid] == 0)
						info->playerState[myid] = 1;
					else
						info->playerState[myid] = 0;
				}

				changeState = true;
			}
			else if ((91 < tempCursorPos.x && tempCursorPos.x < 280) && (180 < tempCursorPos.y && tempCursorPos.y < 510))
			{
				if(info->playerJob[myid] != PlayerJobRifle)
					changeState = true;
				info->playerJob[myid] = PlayerJobRifle;
			}
			else if ((281 < tempCursorPos.x && tempCursorPos.x < 470) && (180 < tempCursorPos.y && tempCursorPos.y < 510))
			{
				if (info->playerJob[myid] != PlayerJobSword)
					changeState = true;
				info->playerJob[myid] = PlayerJobSword;
			}
			else if ((471 < tempCursorPos.x && tempCursorPos.x < 660) && (180 < tempCursorPos.y && tempCursorPos.y < 510))
			{
				if (info->playerJob[myid] != PlayerJobBow)
					changeState = true;
				info->playerJob[myid] = PlayerJobBow;
			}
			if (changeState == true)
			{
				// 방장이라면 시작이 가능한지 서버에 물어봐야한다.
				cs_packet_lobby cs_lobby;
				cs_lobby.type = (char)C2S_CHANGE_JOB;
				cs_lobby.size = sizeof(cs_packet_lobby);
				cs_lobby.cJob = info->playerJob[myid];
				cs_lobby.isReady = info->playerState[myid];
				send(cSocket, (char*)&cs_lobby, sizeof(cs_packet_lobby), 0);
			}
			info->weapon = 0 + (info->playerJob[myid] * 6);

		}
	}
	else if (camera->GetMode() == THIRD_PERSON_CAMERA || camera->GetMode() == ZOOM_MODE_CAMERA)
	{
		auto tempCursorPos = cursorPos;
		ScreenToClient(hwnd, &tempCursorPos);
		int playerNum = 0;

		if (info->scenenum == SCENE_INGAME_ITEM)
		{
			//왼쪽 아이템 선택 범위
			if ((149 < tempCursorPos.x && tempCursorPos.x < 619) && (70 < tempCursorPos.y && tempCursorPos.y < 650))
			{
				info->scenenum = SCENE_INGAME;
			}
			//오른쪽 아이템 선택
			else if ((661 < tempCursorPos.x && tempCursorPos.x < 1131) && (70 < tempCursorPos.y && tempCursorPos.y < 650))
			{
				info->scenenum = SCENE_INGAME;

				if (info->newitem < 19)
					info->accessory = info->newitem - 8;
				else if(info->newitem < 25)
					info->weapon = info->newitem - 19;
				else if (info->newitem < 31)
					info->weapon = info->newitem - 19;
				else if (info->newitem < 36)
					info->weapon = info->newitem - 19;

				// 무기가 0 ~ 16 임
				// 그럼 인포에 무기 값이 어케 해야 할까?

			}
			if (info->newitem >= 19) {
				cs_packet_weapon cs_p_w;
				cs_p_w.type = (char)C2S_WEAPON;
				cs_p_w.size = sizeof(cs_packet_move);
				cs_p_w.weaponNum = info->newitem - 19 - (6 * player->playerJob);
				send(cSocket, (char*)&cs_p_w, sizeof(cs_packet_move), 0);
			}
		}
	}
}

void GameFramework::RifleAttack()
{
	// InGame
	UIINFOINCPU *info = scene->GetUIINFO();
	POINT cursor = POINT{ long(windowClientWidth / 2), long(windowClientHeight / 2) };
	if (isMouseDown == true)
	{
		if (player->GetCurWeaponPoint().end._42 - player->GetPosition().y > 1.3f)
		{
			if (player->playerJob == PlayerJobRifle && gameTimer.GetTotalTime() - player->attackCoolTime > 0.1f && player->ammo > 0 && player->upAnimationStatus == MainCharacterFire) {
				player->ammo -= 1;
				info->ammo = (float)player->ammo;
				player->SetAttackCoolTime(gameTimer.GetTotalTime());
				player->GunRebound();
				player->Rotate(camera->rotateDataX, camera->rotateDataY, 0.f);
				camera->rotateDataX = 0.f;
				camera->rotateDataY = 0.f;
				XMFLOAT3 pickingPoint{ 0.0f, 0.0f, 0.0f };
				float nearDistance = FLT_MAX;
				int bIdx = -1;


				float pointX = ((2.0f * (float)cursor.x) / (float)windowClientWidth) - 1.0f;
				float pointY = ((2.0f * (float)cursor.y) / (float)windowClientHeight) - 1.0f;

				XMFLOAT4X4 projectionMartix = camera->GetProjectionMatrix();

				pointX = pointX / projectionMartix._11;
				pointY = -pointY / projectionMartix._22;

				XMFLOAT3 rayOrigin = XMFLOAT3(0.0f, 0.0f, 0.0f);
				XMFLOAT3 rayDirection = XMFLOAT3(pointX, pointY, 1.0f);
				XMFLOAT4X4 inverseViewMatrix = Matrix4x4::Inverse(camera->GetViewMatrix());
				rayOrigin = Vector3::TransformCoord(rayOrigin, inverseViewMatrix);
				rayDirection = Vector3::TransformCoord(rayDirection, inverseViewMatrix);
				rayDirection = Vector3::Normalize(Vector3::Subtract(rayDirection, rayOrigin));

				ParticleShader* pShader = reinterpret_cast<ParticleShader*>(scene->Getshaders()[ShaderOrderParticle]);
				WeaponTrailerShader* wtShader = reinterpret_cast<WeaponTrailerShader*>(scene->Getshaders()[ShaderOrderWeaponTrail]);
				InstancingShader* insShader = reinterpret_cast<InstancingShader*>(scene->Getshaders()[ShaderOrderInstancing]);
				SkinnedInstancingShader* skinInsShader = reinterpret_cast<SkinnedInstancingShader*>(scene->Getshaders()[ShaderOrderSkinnedInstancing]);
				GameObject* terrain = scene->GetTerrain();
				HeightMapGridMesh* terrainMesh = (HeightMapGridMesh*)terrain->GetMesh(0);

				std::map<float, UINT> obbMap = SortBoundingBox(terrain, insShader, skinInsShader);

				for (auto p : obbMap)
				{
					float dist = p.first;
					UINT info = p.second;
					UINT shaderType, terrainIdx, bufferIndex, objectIndex, boundingIndex;
					shaderType = info % 10;
					info /= 10;
					if (shaderType == ShaderTypeForBoundingTerrain)
					{
						terrainIdx = info;
					}
					else
					{
						bufferIndex = info % 1000;
						info /= 1000;
						objectIndex = info % 1000;
						info /= 1000;
						boundingIndex = info;
					}

					if (shaderType == ShaderTypeForBoundingTerrain)
					{
						BoundingBox obb = terrainMesh->GetBoundings()[terrainIdx];
						if (picking->CheckPicking(cursor, rayOrigin, rayDirection, windowClientWidth, windowClientHeight, obb, pickingPoint, dist))
						{
							bIdx = terrainMesh->GetBoudingVertexIndex()[terrainIdx];
							if (terrainMesh->CheckRayIntersect(rayOrigin, rayDirection, nearDistance, terrain->GetWorld(), bIdx))
							{
								pickingPoint = Vector3::Add(rayOrigin, Vector3::ScalarProduct(rayDirection, nearDistance));

								int id = 0;
								float damage = 0.0f;
								sendhitpacket(id, damage, pickingPoint);

								float age = 0.2f;
								XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
								pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
									ParticleInfoInCPU{ pShader->copyBufferMap["HitDust"], pShader->textureMap["Dust"], PARTICLE_TYPE_DISAPPEAR,
									pickingPoint, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 1.0f });

								// Laser
								age = 0.1f;
								XMFLOAT4 rColor = XMFLOAT4{ 0.75, 0.75f, 0.75f, 0.7f };

								WeaponPoint temp = player->GetCurWeaponPoint();
								XMFLOAT3 start0 = Vector3::Add(XMFLOAT3(temp.end._41, temp.end._42, temp.end._43), XMFLOAT3(-0.005f, -0.005f, -0.005f));
								XMFLOAT3 end0 = Vector3::Add(XMFLOAT3(temp.end._41, temp.end._42, temp.end._43), XMFLOAT3(0.005f, 0.005f, 0.005f));

								XMFLOAT3 start1 = Vector3::Add(pickingPoint, XMFLOAT3(-0.005f, -0.005f, -0.005f));
								XMFLOAT3 end1 = Vector3::Add(pickingPoint, XMFLOAT3(0.005f, 0.005f, 0.005f));

								wtShader->AddBuffer(start0, end0, start1, end1, rColor, age, pShader->textureMap["GunFire"]);

								break;
							}
						}
					}
					else
					{
						BoundingOrientedBox obb;
						bool objLive = true;
						if (shaderType == ShaderTypeForBoundingInstancing)
						{
							objLive = insShader->GetBuffer()[bufferIndex]->objects[objectIndex]->isLive;
							obb = insShader->GetBuffer()[bufferIndex]->objects[objectIndex]->GetobbVector()[boundingIndex];
						}
						if (shaderType == ShaderTypeForBoundingSkinnedInstancing)
						{
							objLive = skinInsShader->GetBuffer()[bufferIndex]->objects[objectIndex]->isLive;
							obb = skinInsShader->GetBuffer()[bufferIndex]->objects[objectIndex]->GetobbVector()[boundingIndex];
						}
						if (objLive == true) {
							if (picking->CheckPicking(cursor, rayOrigin, rayDirection, windowClientWidth, windowClientHeight, obb, pickingPoint, dist))
							{
								if (shaderType == ShaderTypeForBoundingSkinnedInstancing)						{
									// Calcuate Damage
									std::vector<GameObject*>& object = skinInsShader->GetBuffer()[bufferIndex]->objects;
									float strPower = player->strikingPower + (player->GetWeaponDamage());
									float defPower = object[objectIndex]->defencivePower;
									float damage = CalcuateDamage(strPower, defPower, GetBoundingDamage(boundingIndex));
									int id = object[objectIndex]->Getid();
									sendhitpacket(id, damage, pickingPoint);

									// Effect
									float age = 0.3f;
									XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
									pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
										ParticleInfoInCPU{ pShader->copyBufferMap["Blood"], pShader->textureMap["Blood"], PARTICLE_TYPE_DISAPPEAR,
										pickingPoint, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 0.3f });


									// Laser
									age = 0.1f;
									XMFLOAT4 rColor = XMFLOAT4{ 0.75, 0.75f, 0.75f, 0.7f };

									WeaponPoint temp = player->GetCurWeaponPoint();
									XMFLOAT3 start0 = Vector3::Add(XMFLOAT3(temp.end._41, temp.end._42, temp.end._43), XMFLOAT3(-0.005f, -0.005f, -0.005f));
									XMFLOAT3 end0 = Vector3::Add(XMFLOAT3(temp.end._41, temp.end._42, temp.end._43), XMFLOAT3(0.005f, 0.005f, 0.005f));

									XMFLOAT3 start1 = Vector3::Add(pickingPoint, XMFLOAT3(-0.005f, -0.005f, -0.005f));
									XMFLOAT3 end1 = Vector3::Add(pickingPoint, XMFLOAT3(0.005f, 0.005f, 0.005f));

									wtShader->AddBuffer(start0, end0, start1, end1, rColor, age, pShader->textureMap["GunFire"]);

								}
								else
								{
									int id = 0;
									float damage = 0.0f;
									sendhitpacket(id, damage, pickingPoint);

									// Effect
									float age = 0.2f;
									XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
									pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
										ParticleInfoInCPU{ pShader->copyBufferMap["HitDust"], pShader->textureMap["Dust"], PARTICLE_TYPE_DISAPPEAR,
										pickingPoint, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 1.0f });

									// Laser
									age = 0.1f;
									XMFLOAT4 rColor = XMFLOAT4{ 0.75, 0.75f, 0.75f, 0.7f };

									WeaponPoint temp = player->GetCurWeaponPoint();
									XMFLOAT3 start0 = Vector3::Add(XMFLOAT3(temp.end._41, temp.end._42, temp.end._43), XMFLOAT3(-0.005f, -0.005f, -0.005f));
									XMFLOAT3 end0 = Vector3::Add(XMFLOAT3(temp.end._41, temp.end._42, temp.end._43), XMFLOAT3(0.005f, 0.005f, 0.005f));

									XMFLOAT3 start1 = Vector3::Add(pickingPoint, XMFLOAT3(-0.005f, -0.005f, -0.005f));
									XMFLOAT3 end1 = Vector3::Add(pickingPoint, XMFLOAT3(0.005f, 0.005f, 0.005f));

									wtShader->AddBuffer(start0, end0, start1, end1, rColor, age, pShader->textureMap["GunFire"]);
								}
								break;
							}
						}
					}

				}
			}

		}
	}
}

void GameFramework::SwordAttack()
{
	ParticleShader* pShader = reinterpret_cast<ParticleShader*>(scene->Getshaders()[ShaderOrderParticle]);
	InstancingShader* insShader = reinterpret_cast<InstancingShader*>(scene->Getshaders()[ShaderOrderInstancing]);
	SkinnedInstancingShader* skinInsShader = reinterpret_cast<SkinnedInstancingShader*>(scene->Getshaders()[ShaderOrderSkinnedInstancing]);
	GameObject* terrain = scene->GetTerrain();
	HeightMapGridMesh* terrainMesh = (HeightMapGridMesh*)terrain->GetMesh(0);

	std::map<float, UINT> obbMap = SortBoundingBox(terrain, insShader, skinInsShader);

	for (auto p : obbMap)
	{
		float dist = p.first;
		UINT info = p.second;
		UINT shaderType, terrainIdx, bufferIndex, objectIndex, boundingIndex;
		shaderType = info % 10;
		info /= 10;
		if (shaderType == ShaderTypeForBoundingTerrain)
		{
			terrainIdx = info;
		}
		else
		{
			bufferIndex = info % 1000;
			info /= 1000;
			objectIndex = info % 1000;
			info /= 1000;
			boundingIndex = info;
		}


		bool objLive = true;
		BoundingOrientedBox obb;
		BoundingOrientedBox weaponObb = player->GetobbVector()[GetBoundingWeapon(player->GetWeaponNum() + MODEL_WEAPON_START)];
		if (shaderType == ShaderTypeForBoundingInstancing)
		{
			objLive = insShader->GetBuffer()[bufferIndex]->objects[objectIndex]->isLive;
			obb = insShader->GetBuffer()[bufferIndex]->objects[objectIndex]->GetobbVector()[boundingIndex];
		}
		if (shaderType == ShaderTypeForBoundingSkinnedInstancing)
		{
			objLive = skinInsShader->GetBuffer()[bufferIndex]->objects[objectIndex]->isLive;
			obb = skinInsShader->GetBuffer()[bufferIndex]->objects[objectIndex]->GetobbVector()[boundingIndex];
		}

		if (shaderType != ShaderTypeForBoundingTerrain) {
			if (objLive == true) {
				if (obb.Intersects(weaponObb))
				{

					if (shaderType == ShaderTypeForBoundingSkinnedInstancing)
					{

						// Effect
						float age = 0.3f;
						XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
						XMFLOAT3 pos = XMFLOAT3(player->GetCurWeaponPoint().end._41, player->GetCurWeaponPoint().end._42, player->GetCurWeaponPoint().end._43);

						pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
							ParticleInfoInCPU{ pShader->copyBufferMap["Blood"], pShader->textureMap["Blood"], PARTICLE_TYPE_DISAPPEAR,
							pos, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 0.3f });

						// Calcuate Damage
						std::vector<GameObject*>& object = skinInsShader->GetBuffer()[bufferIndex]->objects;
						float strPower = player->strikingPower + (player->GetWeaponDamage());
						float defPower = object[objectIndex]->defencivePower;
						float damage = CalcuateDamage(strPower, defPower, GetBoundingDamage(boundingIndex));
						int id = object[objectIndex]->Getid();
						sendhitpacket(id, damage, pos);
					}
					else
					{
						float age = 0.2f;
						XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
						XMFLOAT3 pos = XMFLOAT3(player->GetCurWeaponPoint().end._41, player->GetCurWeaponPoint().end._42, player->GetCurWeaponPoint().end._43);
						pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
							ParticleInfoInCPU{ pShader->copyBufferMap["HitDust"], pShader->textureMap["Dust"], PARTICLE_TYPE_DISAPPEAR,
							pos, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 1.0f });

						float damage = 0.0f;
						int id = 0;
						sendhitpacket(id, damage, pos);
					}
					break;
				}
			}
		}

	}
}

void GameFramework::BowAttack()
{
	POINT cursor{ windowClientWidth / 2, windowClientHeight / 2 };
	XMFLOAT3 pickingPoint{ 0.0f, 0.0f, 0.0f };
	float nearDistance = FLT_MAX;
	int bIdx = -1;

	float pointX = ((2.0f * (float)cursor.x) / (float)windowClientWidth) - 1.0f;
	float pointY = ((2.0f * (float)cursor.y) / (float)windowClientHeight) - 1.0f;

	XMFLOAT4X4 projectionMartix = camera->GetProjectionMatrix();

	pointX = pointX / projectionMartix._11;
	pointY = -pointY / projectionMartix._22;

	XMFLOAT4X4 world = *(player->toArrow);
	XMFLOAT3 rayOrigin = XMFLOAT3(world._41, world._42, world._43);
	XMFLOAT3 rayDirection = XMFLOAT3(pointX, pointY, 100.0f);
	XMFLOAT4X4 inverseViewMatrix = Matrix4x4::Inverse(camera->GetViewMatrix());
	rayDirection = Vector3::TransformCoord(rayDirection, inverseViewMatrix);
	rayDirection = Vector3::Normalize(Vector3::Subtract(rayDirection, rayOrigin));

	InstancingShader* insShader = reinterpret_cast<InstancingShader*>(scene->Getshaders()[ShaderOrderInstancing]);

	XMFLOAT3 arrowLook = player->GetLook();
	world._31 = -arrowLook.x;
	world._32 = -arrowLook.y;
	world._33 = -arrowLook.z;
		
	float theta = -camera->rotateData;
	
	insShader->LoadGameObject(device, commandList[swapChainBufferIndex][CommandListStateMain], world, scene->hObjectSize - 1);
	insShader->SetVelocityTheta(rayDirection, theta);
}

void GameFramework::BowNearAttack(bool isPunch)
{
	ParticleShader* pShader = reinterpret_cast<ParticleShader*>(scene->Getshaders()[ShaderOrderParticle]);
	InstancingShader* insShader = reinterpret_cast<InstancingShader*>(scene->Getshaders()[ShaderOrderInstancing]);
	SkinnedInstancingShader* skinInsShader = reinterpret_cast<SkinnedInstancingShader*>(scene->Getshaders()[ShaderOrderSkinnedInstancing]);
	GameObject* terrain = scene->GetTerrain();
	HeightMapGridMesh* terrainMesh = (HeightMapGridMesh*)terrain->GetMesh(0);

	std::map<float, UINT> obbMap = SortBoundingBox(terrain, insShader, skinInsShader);

	for (auto p : obbMap)
	{
		float dist = p.first;
		UINT info = p.second;
		UINT shaderType, terrainIdx, bufferIndex, objectIndex, boundingIndex;
		shaderType = info % 10;
		info /= 10;
		if (shaderType == ShaderTypeForBoundingTerrain)
		{
			terrainIdx = info;
		}
		else
		{
			bufferIndex = info % 1000;
			info /= 1000;
			objectIndex = info % 1000;
			info /= 1000;
			boundingIndex = info;
		}


		BoundingOrientedBox obb;
		BoundingOrientedBox weaponObb;
		bool objLive = true;
		if (isPunch)
		{
			weaponObb = player->GetobbVector()[GetBoundingWeapon(player->GetWeaponNum() + MODEL_WEAPON_START)];
		}
		else
			weaponObb = player->GetobbVector()[2];
		if (shaderType == ShaderTypeForBoundingInstancing)
		{
			objLive = insShader->GetBuffer()[bufferIndex]->objects[objectIndex]->isLive;
			obb = insShader->GetBuffer()[bufferIndex]->objects[objectIndex]->GetobbVector()[boundingIndex];
		}
		if (shaderType == ShaderTypeForBoundingSkinnedInstancing)
		{
			objLive = skinInsShader->GetBuffer()[bufferIndex]->objects[objectIndex]->isLive;
			obb = skinInsShader->GetBuffer()[bufferIndex]->objects[objectIndex]->GetobbVector()[boundingIndex];
		}
		if (shaderType != ShaderTypeForBoundingTerrain) {
			if (objLive == true) {
				if (obb.Intersects(weaponObb))
				{

					if (shaderType == ShaderTypeForBoundingSkinnedInstancing)
					{
						// Effect
						float age = 0.3f;
						XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
						XMFLOAT3 pos = XMFLOAT3(player->GetCurWeaponPoint().end._41, player->GetCurWeaponPoint().end._42, player->GetCurWeaponPoint().end._43);

						pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
							ParticleInfoInCPU{ pShader->copyBufferMap["Blood"], pShader->textureMap["Blood"], PARTICLE_TYPE_DISAPPEAR,
							pos, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 0.3f });

						// Calcuate Damage
						std::vector<GameObject*>& object = skinInsShader->GetBuffer()[bufferIndex]->objects;
						float strPower = player->strikingPower + (player->GetWeaponDamage());
						float defPower = object[objectIndex]->defencivePower;
						float damage = CalcuateDamage(strPower, defPower, GetBoundingDamage(boundingIndex));
						int id = object[objectIndex]->Getid();
						sendhitpacket(id, damage, pos);
					}
					else
					{
						float age = 0.2f;
						XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
						XMFLOAT3 pos = XMFLOAT3(player->GetCurWeaponPoint().end._41, player->GetCurWeaponPoint().end._42, player->GetCurWeaponPoint().end._43);
						pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
							ParticleInfoInCPU{ pShader->copyBufferMap["HitDust"], pShader->textureMap["Dust"], PARTICLE_TYPE_DISAPPEAR,
							pos, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 1.0f });

						float damage = 0.0f;
						int id = 0;
						sendhitpacket(id, damage, pos);
					}
					break;
				}
			}
		}
	}
}

void GameFramework::ArrowCollision()
{
	ParticleShader* pShader = reinterpret_cast<ParticleShader*>(scene->Getshaders()[ShaderOrderParticle]);
	InstancingShader* insShader = reinterpret_cast<InstancingShader*>(scene->Getshaders()[ShaderOrderInstancing]);
	SkinnedInstancingShader* skinInsShader = reinterpret_cast<SkinnedInstancingShader*>(scene->Getshaders()[ShaderOrderSkinnedInstancing]);
	WeaponTrailerShader* wtShader = reinterpret_cast<WeaponTrailerShader*>(scene->Getshaders()[ShaderOrderWeaponTrail]);
	GameObject* terrain = scene->GetTerrain();
	HeightMapGridMesh* terrainMesh = (HeightMapGridMesh*)terrain->GetMesh(0);

	std::map<float, UINT> obbMap = SortBoundingBox(terrain, insShader, skinInsShader);
	std::vector<GameObject*>& arrowObjects = insShader->GetBuffer().back()->objects;

	for (auto p : obbMap)
	{
		float dist = p.first;
		UINT info = p.second;
		UINT shaderType, terrainIdx, bufferIndex, objectIndex, boundingIndex;
		shaderType = info % 10;
		info /= 10;
		if (shaderType == ShaderTypeForBoundingTerrain)
		{
			terrainIdx = info;
		}
		else
		{
			bufferIndex = info % 1000;
			info /= 1000;
			objectIndex = info % 1000;
			info /= 1000;
			boundingIndex = info;
		}

		for (int i = 1; i < arrowObjects.size(); ++i) {

			if (arrowObjects[i]->isMove == true)
			{
				bool objLive = true;
				BoundingOrientedBox weaponObb = arrowObjects[i]->GetobbVector()[0];
				BoundingOrientedBox obb;

				if (shaderType != ShaderTypeForBoundingTerrain) 
				{
					if (shaderType == ShaderTypeForBoundingInstancing)
					{
						objLive = insShader->GetBuffer()[bufferIndex]->objects[objectIndex]->isLive;
						obb = insShader->GetBuffer()[bufferIndex]->objects[objectIndex]->GetobbVector()[boundingIndex];
					}
					else if (shaderType == ShaderTypeForBoundingSkinnedInstancing)
					{
						objLive = skinInsShader->GetBuffer()[bufferIndex]->objects[objectIndex]->isLive;
						obb = skinInsShader->GetBuffer()[bufferIndex]->objects[objectIndex]->GetobbVector()[boundingIndex];
					}
					if (objLive == true)
					{
						if (obb.Intersects(weaponObb))
						{
							arrowObjects[i]->isMove = false;
							if (shaderType == ShaderTypeForBoundingSkinnedInstancing)
							{
								std::vector<GameObject*>& object = skinInsShader->GetBuffer()[bufferIndex]->objects;
								std::vector<GameObject*>* targetObj = skinInsShader->GetBuffer()[bufferIndex]->objects[objectIndex]->GetBoundingObject();
								XMFLOAT3 arrowPos = Vector3::Subtract(arrowObjects[i]->GetTransformPosition(), (*targetObj)[boundingIndex]->GetPosition());
								arrowObjects[i]->SetPosition(arrowPos);
								arrowObjects[i]->toParent = (*targetObj)[boundingIndex];
								arrowObjects[i]->isCollisionSkinnedModel = true;

								// Effect
								float age = 0.3f;
								XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
								XMFLOAT3 pos = XMFLOAT3(weaponObb.Center);

								pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
									ParticleInfoInCPU{ pShader->copyBufferMap["Blood"], pShader->textureMap["Blood"], PARTICLE_TYPE_DISAPPEAR,
									pos, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 0.3f });

								// Calcuate Damage
								float strPower = player->strikingPower + (player->GetWeaponDamage());
								float defPower = object[objectIndex]->defencivePower;
								float damage = CalcuateDamage(strPower, defPower, GetBoundingDamage(boundingIndex));
								int id = object[objectIndex]->Getid();
								sendhitpacket(id, damage, pos);
							}
							else
							{
								float age = 0.2f;
								XMFLOAT4 color = XMFLOAT4(0.7f, 0.7f, 0.7f, 1.0f);
								XMFLOAT3 pos = XMFLOAT3(weaponObb.Center);
								pShader->CreateParticle(device, commandList[swapChainBufferIndex][CommandListStateMain],
									ParticleInfoInCPU{ pShader->copyBufferMap["HitDust"], pShader->textureMap["Dust"], PARTICLE_TYPE_DISAPPEAR,
									pos, XMFLOAT2(0.15f, 0.15f), color, age, age, 1.0f, 1.0f });

								float damage = 0.0f;
								int id = 0;
								sendhitpacket(id, damage, pos);
							}
							break;
						}
					}
				}
			}
		}

	}
}

bool GameFramework::CollisionCheck(GameObject* gobj , bool isPlayer)
{
	TerrainShader* tShader = static_cast<TerrainShader*>(scene->Getshaders()[0]);
	HeightMapTerrain* terrain = scene->GetTerrain();
	HeightMapGridMesh* tMesh = static_cast<HeightMapGridMesh*>(terrain->GetMesh(0));

	QuadTree* root = tShader->GetQuadRoot();
	QuadTree* Node = root;

	XMFLOAT4X4 itt = Matrix4x4::Inverse(terrain->GetTransform());

	BoundingOrientedBox pWorldbounding;
	BoundingOrientedBox pTerrainbounding;

	if (isPlayer) {
		pWorldbounding = player->GetColOBB();
	}
	else {
		pWorldbounding = gobj->GetColOBB();
	}
	if (_isnan(pWorldbounding.Orientation.x) == true) {
		return true;
	}
	pWorldbounding.Transform(pTerrainbounding, XMLoadFloat4x4(&itt));

	bool loop = true;
	while (1) {

		if (Node->AABBbOX.Intersects(pTerrainbounding))
			for (int i = 0; i < 4; ++i) {
				if (Node->child[i] != NULL) {
					if (Node->child[i]->AABBbOX.Intersects(pTerrainbounding)) {
						Node = Node->child[i];
						break;
					}
					else if(i == 3){
						loop = false; 
						break;
					}
				}
				else {
					loop = false;
					break;
				}
			}
		else loop = false;
		
		if (loop == false)
			break;
	}
	for (int i = 0; i < Node->obbs.size(); ++i)
	{
		if (pTerrainbounding.Intersects(Node->obbs[i]))
		{
			return true;
		}
	}
	if (isPlayer == true) 
	{
		SkinnedInstancingShader* skinInsShader = reinterpret_cast<SkinnedInstancingShader*>(scene->Getshaders()[ShaderOrderSkinnedInstancing]);
		std::vector<SkinnedInstanceBuffer*> insBuffer = skinInsShader->GetBuffer();
		for (int i = 0; i < insBuffer.size(); ++i)
		{
			std::vector<GameObject*> objects = insBuffer[i]->objects;
			for (int j = 1; j < objects.size(); ++j)
			{
				if (objects[j]->isLive == true) 
				{
					BoundingOrientedBox obb = objects[j]->GetobbVector()[0];
					if (_isnan(obb.Orientation.x) == true) {
						return true;
					}
					
					if (pWorldbounding.Intersects(obb))
					{
						return true;
					}
				}
			}
		}
		SkinnedAnimationObjectsShader* skinObjShader = reinterpret_cast<SkinnedAnimationObjectsShader*>(scene->Getshaders()[ShaderOrderSkinnedObject]);
		std::vector<SkinnedObjectBuffer*> objBuffer = skinObjShader->GetBuffer();
		for (int i = 0; i < objBuffer.size(); ++i)
		{
			if (objBuffer[i]->object->isLive == true) {
				BoundingOrientedBox obb = objBuffer[i]->object->GetobbVector()[0];
				if (_isnan(obb.Orientation.x) == true) {
					return true;
				}
				if (pWorldbounding.Intersects(obb))
				{
					return true;
				}
			}
		}
	}
	return false;
}

std::map<float, UINT> GameFramework::SortBoundingBox(GameObject* terrain, InstancingShader* insShader, SkinnedInstancingShader* skinInsShader)
{
	std::map<float, UINT> obbMap;

	HeightMapGridMesh* terrainMesh = (HeightMapGridMesh*)terrain->GetMesh(0);
	// UINT : 0 ~ 10 : ShaderType, 10~ BoundingIndex
	for (int i = 0; i < TERRAINBOUNDINGS; ++i)
	{
		float dist = Vector3::Length(Vector3::Subtract(terrainMesh->GetBoundings()[i].Center, player->GetPosition()));
		obbMap[dist] = ShaderTypeForBoundingTerrain + 10 * i;
	}

	// UINT : 0 ~ 10 : Shader Type, 10 ~ 10,000 : Buffer Index, 10,000 ~ 10,000,000 : Object Index, 10,000,000 ~ 1,000,000,000 Bounding Index
	for (int i = 0; i < insShader->GetBuffer().size() - 1; ++i)
	{
		std::vector<GameObject*> objects = insShader->GetBuffer()[i]->objects;
		for (int j = 1; j < objects.size(); ++j)
		{
			std::vector<BoundingOrientedBox> obbs = objects[j]->GetobbVector();
			for (int k = 0; k < obbs.size(); ++k)
			{
				float dist = Vector3::Length(Vector3::Subtract(obbs[k].Center, player->GetPosition())); //- Vector3::Length(obbs[k].Extents);
				UINT info = ShaderTypeForBoundingInstancing + 10 * i + 10000 * j + 10000000 * k;
				obbMap[dist] = info;
			}
		}
	}

	for (int i = 3; i < skinInsShader->GetBuffer().size(); ++i)
	{
		std::vector<GameObject*> objects = skinInsShader->GetBuffer()[i]->objects;
		for (int j = 1; j < objects.size(); ++j)
		{
			std::vector<BoundingOrientedBox> obbs = objects[j]->GetobbVector();
			for (int k = 1; k < obbs.size(); ++k)
			{
				float dist = Vector3::Length(Vector3::Subtract(obbs[k].Center, player->GetPosition()));// - Vector3::Length(obbs[k].Extents);
				UINT info = ShaderTypeForBoundingSkinnedInstancing + 10 * i + 10000 * j + 10000000 * k;
				obbMap[dist] = info;
			}
		}
	}
	return obbMap;
}

void GameFramework::SetMapFilePath()
{
	mapFileNames.emplace_back("Assets/Map/map_00");
	mapFileNames.emplace_back("Assets/Map/map_08");
	mapFileNames.emplace_back("Assets/Map/map_08");
	mapFileNames.emplace_back("Assets/Map/map_08");
	mapFileNames.emplace_back("Assets/Map/map_08");
	mapFileNames.emplace_back("Assets/Map/map_08");
	mapFileNames.emplace_back("Assets/Map/map_08");
	mapFileNames.emplace_back("Assets/Map/map_08");
	mapFileNames.emplace_back("Assets/Map/map_08");
}

XMFLOAT3 GameFramework::StartPos(int idx)
{
	XMFLOAT3 position{};
	if (idx == 0) position = XMFLOAT3(2.0f, 0.0f, 2.0f);
	if (idx == 1) position = XMFLOAT3(-2.0f, 0.0f, 2.0f);
	if (idx == 2) position = XMFLOAT3(-2.0f, 0.0f, -2.0f);
	if (idx == 3) position = XMFLOAT3(2.0f, 0.0f, -2.0f);
	return position;
}

void GameFramework::BuildInGame(ID3D12GraphicsCommandList* commandList)
{
	UIINFOINCPU* info = scene->GetUIINFO();
	if (ingameBuild == true)
	{
		info->scenenum = SCENE_LOADING;
		SkinnedAnimationObjectsShader* skinObjShader = reinterpret_cast<SkinnedAnimationObjectsShader*>(scene->Getshaders()[ShaderOrderSkinnedObject]);
		for (int i = 0; i < info->players; ++i) {
			if (i != myid)
				skinObjShader->AddSingleObjects(device, commandList, i, info->playerJob[i], StartPos(i));
		}

		player->SetModel(device, commandList, info->playerJob[myid]);
		ProcessFileIO(device, commandList);
		camera = player->ChangeCamera(THIRD_PERSON_CAMERA, gameTimer.GetTimeElapsed());
		ingameBuild = false;
		isGameStart = true;
	}

	if (isLoadMap == true) {
		info->scenenum = SCENE_LOADING;
		ProcessFileIO(device, commandList);
		isLoadMap = false;
	}

}

void GameFramework::FrameAdvance()
{
	HRESULT hresult;
	Shader::curPipelineState = NULL;

	gameTimer.Tick(0.0f);

	WaitForSingleObject(presentChangeIdxEvent, INFINITE);

	commandAllocator[swapChainBufferIndex][CommandListStateMain]->Reset();
	commandList[swapChainBufferIndex][CommandListStateMain]->Reset(commandAllocator[swapChainBufferIndex][CommandListStateMain], NULL);

	ProcessInput();

	BuildInGame(commandList[swapChainBufferIndex][CommandListStateMain]);

	AnimateObjects();

	commandList[swapChainBufferIndex][CommandListStateMain]->Close();

	BeginRender();
	// Reset, ResourceBarrier, SetGraphicsRootSignature, ComputeSkinning, ClearRenderTargetView, ClearDepthStencilView
	Render();
	// Rendering MultiThread (Animate, ShadowCaster, Render)
	EndRender();
	// ResourceBarrier, Close commandLists and Execute

	PrintFrame();
}

void GameFramework::BeginRender()
{
	SetEvent(computeBeginEvent);

	for (int i = 1; i < CommandListStateNum; ++i)
	{
		commandAllocator[swapChainBufferIndex][i]->Reset();
		commandList[swapChainBufferIndex][i]->Reset(commandAllocator[swapChainBufferIndex][i], NULL);
	}
}

void GameFramework::Render()
{
	for (int i = 0; i < RENDER_THREADS_NUM; ++i)
	{
		renderInfo[i]->SetCamera(camera, lightCamera);
		SetEvent(renderBeginEvent[i]);
	}

	::ResourceBarrier(commandList[swapChainBufferIndex][CommandListStateMid], shadowMapBuffer[swapChainBufferIndex], D3D12_RESOURCE_STATE_DEPTH_WRITE, D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE);

	commandList[swapChainBufferIndex][CommandListStateMid]->Close();

}

void GameFramework::EndRender()
{
	WaitForSingleObject(computeFinishEvent, INFINITE);

	WaitForMultipleObjects(RENDER_THREADS_NUM, renderShadowFinishEvent, TRUE, INFINITE);

	WaitForGpuComplete(commandQueue, fence, fenceValues[swapChainBufferIndex], fenceEvent);

	commandQueue->ExecuteCommandLists(_countof(shadowCommandLists[swapChainBufferIndex]), shadowCommandLists[swapChainBufferIndex]);

	::ResourceBarrier(commandList[swapChainBufferIndex][CommandListStatePost], renderTargetBuffers[swapChainBufferIndex], D3D12_RESOURCE_STATE_RENDER_TARGET, D3D12_RESOURCE_STATE_PRESENT);
	::ResourceBarrier(commandList[swapChainBufferIndex][CommandListStatePost], shadowMapBuffer[swapChainBufferIndex], D3D12_RESOURCE_STATE_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_DEPTH_WRITE);

	commandList[swapChainBufferIndex][CommandListStatePost]->Close();

	WaitForMultipleObjects(RENDER_THREADS_NUM, renderSceneFinishEvent, TRUE, INFINITE);
	commandQueue->ExecuteCommandLists(_countof(renderCommandLists[swapChainBufferIndex]), renderCommandLists[swapChainBufferIndex]);

	SetEvent(presentBeginEvent);

}


void GameFramework::RenderThread(GameFramework* framework,  int idx)
{
	while (true) {

		WaitForSingleObject(framework->renderBeginEvent[idx], INFINITE);

		if (false == GameFramework::isLive) break;

		RenderInfo* re = framework->renderInfo[idx];

		// Animate
		for (int i = 1; i < re->GetShaders().size(); ++i)
		{
			re->GetShaders()[i]->Animate();
		}

		for (int j = 0; j < MAX_CASCADE_SIZE; ++j)
		{
			for (auto& sh : re->GetShaders())
				sh->FrustumCulling(&re->GetShadowFrustum(j), true, j);
		}
		for (auto& sh : re->GetShaders())
			sh->FrustumCulling(&re->GetFrustum(), false, MAX_CASCADE_SIZE);

		UINT scIdx = framework->swapChainBufferIndex;

		// Shadow Render
		re->GetShadowAllocator(scIdx)->Reset();
		re->GetShadowCommandList(scIdx)->Reset(re->GetShadowAllocator(scIdx), NULL);
		framework->scene->PrepareRender(re->GetShadowCommandList(scIdx));
		re->GetShadowCommandList(scIdx)->OMSetRenderTargets(0, nullptr, false, &(re->GetShadowBufferHandle(scIdx)));		

		WaitForSingleObject(framework->presentFinishEvent, INFINITE);

		for (int j = 0; j < MAX_CASCADE_SIZE; ++j)
		{
			framework->scene->PrepareSet(re->GetShadowCommandList(scIdx), re->GetLightCamera(j), true);
			for (auto& sh : re->GetShaders())
				sh->Render(re->GetShadowCommandList(scIdx), true, j);
		}

		if (re->GetPlayer())
		{
			if (re->GetPlayer()->haveModel) {
				re->GetPlayer()->Animate();
				framework->scene->PrepareSet(re->GetShadowCommandList(scIdx), re->GetLightCamera(0), true);
				re->GetPlayer()->Render(re->GetShadowCommandList(scIdx), re->GetLightCamera(0), true);
			}
		}

		re->GetShaders()[0]->Animate();
		// SkyBox Animate

		re->GetShadowCommandList(scIdx)->Close();

		SetEvent(framework->renderShadowFinishEvent[idx]);

		// Scene Render
		re->GetRenderAllocator(scIdx)->Reset();
		re->GetRenderCommandList(scIdx)->Reset(re->GetRenderAllocator(scIdx), NULL);
		framework->scene->PrepareRender(re->GetRenderCommandList(scIdx));
		framework->UpdateShaderVariables(re->GetRenderCommandList(scIdx), scIdx);

		re->GetRenderCommandList(scIdx)->OMSetRenderTargets(1, &(re->GetRenderTargetHandle(scIdx)), TRUE, &(re->GetDepthBufferHandle(scIdx)));

		framework->scene->PrepareSet(re->GetRenderCommandList(scIdx), re->GetCamera(), false);
		framework->scene->SetShaderVariables(re->GetRenderCommandList(scIdx));
		for (auto& sh : re->GetShaders())
			sh->Render(re->GetRenderCommandList(scIdx), false, MAX_CASCADE_SIZE);

		if (re->GetPlayer()) {
			if (re->GetPlayer()->haveModel)
				re->GetPlayer()->Render(re->GetRenderCommandList(scIdx), re->GetCamera(), false);
		}

		re->GetRenderCommandList(scIdx)->Close();

		SetEvent(framework->renderSceneFinishEvent[idx]);
	}
}

void GameFramework::ComputeThread(GameFramework* framework)
{
    while (true) {

		WaitForSingleObject(framework->computeBeginEvent, INFINITE);

        if (false == GameFramework::isLive) break;

        ComputeInfo* com = framework->computeInfo;

        UINT scIdx = framework->swapChainBufferIndex;


        com->GetAllocator(scIdx)->Reset();
        com->GetCommandList(scIdx)->Reset(com->GetAllocator(scIdx), NULL);

        ::ResourceBarrier(com->GetCommandList(scIdx), com->GetRtBuffer(scIdx), D3D12_RESOURCE_STATE_PRESENT, D3D12_RESOURCE_STATE_RENDER_TARGET);

        float pfClearColor[4] = { 0.0f, 1.0f, 1.0f, 1.0f };
        com->GetCommandList(scIdx)->ClearRenderTargetView(com->GetRenderTargetHandle(scIdx), pfClearColor/*Colors::Azure*/, 0, NULL);
        com->GetCommandList(scIdx)->ClearDepthStencilView(com->GetDepthBufferHandle(scIdx), D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
        com->GetCommandList(scIdx)->ClearDepthStencilView(com->GetShadowBufferHandle(scIdx), D3D12_CLEAR_FLAG_DEPTH, 1.0f, 0, 0, NULL);

        framework->scene->PrepareRender(com->GetCommandList(scIdx));
        framework->scene->PrepareCompute(com->GetCommandList(scIdx));
		if (framework->player->haveModel)
			framework->player->PrepareCompute(com->GetCommandList(scIdx));

        com->GetCommandList(scIdx)->Close();

		SetEvent(framework->computeFinishEvent);

    }
}

void GameFramework::PresentThread(GameFramework* framework)
{
	while (true) {

		WaitForSingleObject(framework->presentBeginEvent, INFINITE);

		if (false == GameFramework::isLive) break;

		int scIdx = framework->swapChainBufferIndex;

		framework->SetSwapchinIdx(1 - scIdx);
		SetEvent(framework->presentChangeIdxEvent);

		WaitForGpuComplete(framework->commandQueue, framework->fence, ++framework->fenceValues[scIdx], framework->fenceEvent);

		framework->swapChain->Present(0, 0);

		framework->GarbageCollection();

		SetEvent(framework->presentFinishEvent);
	}
}
