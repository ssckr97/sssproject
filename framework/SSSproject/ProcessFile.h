#pragma once
#include "Scene.h"


struct NaviInform {
	int bIdx;
	int x;
	int z;
	int width;
	XMFLOAT3 dtriv1; // �Ʒ��ﰢ�� 
	XMFLOAT3 dtriv2; // �Ʒ��ﰢ�� 
	XMFLOAT3 dtriv3; // �Ʒ��ﰢ�� 

	XMFLOAT3 utriv1; // ���� �ﰢ�� 
	XMFLOAT3 utriv2; // ���� �ﰢ�� 
	XMFLOAT3 utriv3; // ���� �ﰢ�� 
	XMFLOAT3 center;
};

class SaveInstanceBuffer {
public:
	int objectNumber;
	std::vector<XMFLOAT4X4> objectWorld;

};

class SaveDiffuseVertex {
public:
	XMFLOAT3 postion;
	XMFLOAT4 diffuse;
	int    texNumber;
};

class SaveFile
{
private:
	int instanceBufferSize = 0;
	std::vector<SaveInstanceBuffer> saveInstanceBuffer;
	std::vector<SaveDiffuseVertex> saveDiffuseVertex;
public:
	SaveFile();
	int GetInstanceBufferSize() { return instanceBufferSize; }
	void SetInstanceBufferSize(int size) { instanceBufferSize; }

	std::vector<SaveInstanceBuffer>& GetSaveInstanceBuffer() { return saveInstanceBuffer; }
	void SetInstanceBuffer(std::vector<InstanceBuffer*> instanceBuffer);
	void SetSkinedInstanceBuffer(std::vector<SkinnedInstanceBuffer*> instanceBuffer);

	std::vector<SaveDiffuseVertex> &GetSaveDiffuseVertex() { return saveDiffuseVertex; }

};

class ProcessFile
{
private:
	Scene* scene;
public:
	ProcessFile(Scene* scene) : scene(scene) {}

	void LoadMap(std::string filePath , ID3D12Device * device, ID3D12GraphicsCommandList * commandList);


};

