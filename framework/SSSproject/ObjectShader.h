#pragma once
#include "Shader.h"

struct ObjectBuffer
{
	std::vector<GameObject*> objects;
	int objectsNum = 0;

	int framesNum = 0;
	int materialsNum = 0;

	ID3D12Resource** cbGameObjects = NULL;
	CB_GAMEOBJECT_INFO** cbMappedGameObjects = NULL;
	UINT cbGameObjectBytes = 0;

	UINT pipeLineStateIdx = 0;
};

struct SkinnedObjectBuffer
{
	GameObject* object;

	bool isVisable[CAMERAS_NUM];
	char id = 255;
	int skinnedMeshNum = 0;
	int standardMeshNum = 0;
	int boundingMeshNum = 0;
	bool mainCharacter = false;
	bool isActive = false;
	int direction;
	float mass = 5.0f;
	XMFLOAT3 acc;
	XMFLOAT3 velocity;
	XMFLOAT3 gravity;
	//플레이어에 작용하는 중력을 나타내는 벡터이다.
	float maxVelocityXZ = 6.0f;
	float minVelocityXZ = -6.0f;
	//xz-평면에서 (한 프레임 동안) 플레이어의 이동 속력의 최대값을 나타낸다.
	float maxVelocityY;
	//y-축 방향으로 (한 프레임 동안) 플레이어의 이동 속력의 최대값을 나타낸다.
	float friction = 20.0f;

	std::chrono::high_resolution_clock::time_point ctime;

	int playerJob = 0;

	int weaponNum = 0;
	int weaponNum1 = -1;

	SkinnedMesh** skinnedMeshes = NULL;

	ID3D12Resource** skinnedBuffer = NULL;
	XMFLOAT4X4** mappedSkinnedBuffer = NULL;	//[][][] skinNum, bone

	ID3D12Resource** vbGameObject = NULL;
	VS_VB_INSTANCE_OBJECT** vbMappedGameObject = 0;
	D3D12_VERTEX_BUFFER_VIEW* vbGameObjectView = NULL;
	// 무기와 같은 StandardMesh를 위한 버퍼

	ID3D12Resource** boundingBuffer = NULL;
	VS_VB_INSTANCE_BOUNDING** mappedBoundingBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW* instancingBoundingBufferView;
	// 바운딩박스를 위한 버퍼

	XMFLOAT4X4** obbWorldCache = NULL;

	ID3D12Resource** positionW = NULL;
	ID3D12Resource** normalW = NULL;
	ID3D12Resource** tangentW = NULL;
	ID3D12Resource** biTangentW = NULL;

	UINT pipeLineStateIdx = 0;
	UINT shadowPipeLineStateIdx = 0;

	TEXTURENAME textureName{};
	int diffuseIdx = -1;
	int normalIdx = -1;
	int specularIdx = -1;
	int metallicIdx = -1;
	int emissionIdx = -1;

	BoundingOrientedBox obb;

};
class QuadTree {
public:
	BoundingBox AABBbOX;
	int vStartIndex; // 점이 시작하는 인덱스
	int width;
	int length;
	int vertexNum;
	int primitiveNum;
	bool isCheck = false;

	QuadTree* parent;
	QuadTree* child[4];
	std::vector<BoundingOrientedBox> obbs;

public:
	QuadTree(int startidx , int abwidth , int ablength , HeightMapGridMesh* mesh);
};

class TerrainShader : public Shader
{
protected:
	ID3D12Resource *cbGameObjects = NULL;
	CB_GAMEOBJECT_INFO *cbMappedGameObjects = NULL;
	UINT cbGameObjectBytes = 0;
	QuadTree* Root;
	std::vector<GameObject*> objects;
	int objectsNum = 0;

public:
	ID3D12Resource* releaseBuffer = NULL;

	TerrainShader();
	virtual ~TerrainShader();
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context = NULL);

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseShaderVariables();

	virtual void ReleaseObjects();

	virtual void ReleaseUploadBuffers();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateShadowVertexShader();
	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature *grapicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);

	virtual void Render(ID3D12GraphicsCommandList* commandList, bool isShadow, UINT cameraIdx);
	virtual void GarbageCollection();

	HeightMapTerrain* GetTerrain() { return terrain; }

	virtual GameObject* GetObjects(int idx) { return objects[idx]; }

	void CreateQuadTree(QuadTree* Node);
	void SetQuadTreeObject(QuadTree* Node, BoundingOrientedBox obb);
	QuadTree* GetQuadRoot() { return Root; }

	void DeleteAllBounding(QuadTree* Node);

protected:

	HeightMapTerrain* terrain = NULL;
};

class SkyBoxShader : public StandardShader {

private:
	ObjectBuffer* objectBuffer;
	XMFLOAT3 camPos;

public:
	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void Animate();
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context);
	virtual void Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseObjects();
	virtual void ReleaseUploadBuffer();
	virtual void ReleaseShaderVariables();
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_RASTERIZER_DESC CreateRasterizerState();
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();
	virtual D3D12_BLEND_DESC CreateBlendState();
};

class SkinnedAnimationObjectsShader : public StandardSkinnedAnimationShader
{
public:
	SkinnedAnimationObjectsShader();
	virtual ~SkinnedAnimationObjectsShader();

	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature = NULL);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context, int hObjectSize);
	virtual void AddSingleObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int id, int jobIdx, XMFLOAT3 pos);
	virtual void AnimateObjects(float timeElapsed, Camera* camera);

	virtual void ReleaseUploadBuffers();
	virtual void ReleaseObjects();
	virtual void ReleaseShaderVariables();

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx);
	virtual void UpdateShaderVariables(int idx);

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx, bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList *commandList, bool isShadow, UINT cameraIdx);

	virtual void PrepareCompute(ID3D12GraphicsCommandList * commandList);

	virtual void FrustumCulling(void* frustum, bool isShadow, UINT cameraIdx);

	virtual void SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);

	void Move(int direction, float distance, float elapsedTime , int bufferidx);

	std::vector<SkinnedObjectBuffer*>& GetBuffer() { return objectBuffer; }

protected:
	std::vector<SkinnedObjectBuffer*> objectBuffer;
	std::vector<Texture*> textures;
	std::vector<void*> objects;
	int hObjectSize = 0;

	bool showBounding = false;

public:
	std::vector<SkinnedObjectBuffer*>& GetObjectBuffer() { return objectBuffer; }
	void SetShowBounding(bool showBounding) { this->showBounding = showBounding; }
};