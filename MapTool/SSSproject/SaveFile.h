#pragma once
#include "Scene.h"

class SaveInstanceBuffer {
public:
	int objectNumber;
	std::vector<XMFLOAT4X4> objectWorld;
	
};

class SaveDiffuseVertex {
public:
	XMFLOAT3 position;
	XMFLOAT4 diffuse;
	//XMFLOAT3 normal;
	int texNumber;
};

class SaveFile
{
private:
	int instanceBufferSize = 0;
	std::vector<SaveInstanceBuffer> saveInstanceBuffer;
	std::vector<SaveDiffuseVertex> saveDiffuseVertex;
	std::vector<XMFLOAT3> saveTerrainPos;

public:
	SaveFile();
	int GetInstanceBufferSize() { return instanceBufferSize; }
	void SetInstanceBufferSize(int size) {instanceBufferSize;}

	std::vector<SaveInstanceBuffer>& GetSaveInstanceBuffer() { return saveInstanceBuffer;	}
	void SetInstanceBuffer(std::vector<InstanceBuffer*> instanceBuffer);
	void SetSkinedInstanceBuffer(std::vector<SkinnedInstanceBuffer*> instanceBuffer);

	std::vector<SaveDiffuseVertex> &GetSaveDiffuseVertex() { return saveDiffuseVertex; }
	std::vector<XMFLOAT3> &GetSaveTerrainPos() { return saveTerrainPos; }
};

