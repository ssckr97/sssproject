#pragma once
#include "Shader.h"
//랜덤 엔진 

struct bImform {
	int size;
	std::vector<XMFLOAT4X4> bworld;
	std::vector<XMFLOAT3> tscale;
	std::vector<XMFLOAT3> trotate;
	std::vector<XMFLOAT3> tpos;
};

class InstancingShader : public StandardInstanceShader
{
protected:

	std::vector<GameObject*> objects;
	// terrain, helicoptor, tree ... 오브젝트 데이터를 관리하는 벡터.// 모델들을 관리한다. 

	std::vector<InstanceBuffer*> instanceBuffer;
	// 오브젝트들과 오브젝트들의 버퍼 등을 관리하는 벡터.
	std::vector<Texture*> textures;

	ID3DBlob*			geometryShaderBlob = NULL;

public:
	InstancingShader();
	virtual ~InstancingShader();

	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature = NULL);
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx);
	virtual void ReleaseShaderVariables();
	virtual void ReleaseUploadBuffers();
	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void DeleteGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int bufferIndex, int objectindex);
	virtual void DeleteAllObjcet();
	virtual void AddLight(XMFLOAT3 pointxyz);
	virtual void ModifyLight(int idx, XMFLOAT3 pointxyz);

	virtual void AddGameObject(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, XMFLOAT3 pointxyz, ID3D12RootSignature* graphicsRootSignature, int objectType , bool noiseFlag);
	virtual void LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context , int fobjectSize);
	virtual void ReleaseObjects();
	virtual void BuildInstanceObject(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
		InstanceBuffer* instanceBuffer, int objectNum, int pipeLineStateIdx, GameObject* objectModel,
		int modelNumber, XMFLOAT3 boundingSize, XMFLOAT3 rotate, HeightMapTerrain *terrain, XMFLOAT3 position);
	void SetTextures(ID3D12Device* deivce, ID3D12GraphicsCommandList* commandList);

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx);
	virtual void Render(ID3D12GraphicsCommandList* commandList, Camera* camera);
	virtual void SetDummyFarAway();
	virtual void ApplyLOD(Camera* camera, int idx);
	virtual std::vector<InstanceBuffer*> GetBuffer() { return instanceBuffer; }
	virtual std::vector<GameObject*> GetGameObjectIdx(int idx) {
		return instanceBuffer[idx]->objects;
	}
	virtual GameObject* GetGameObject(int idx , int objectidx) { return instanceBuffer[idx]->objects[objectidx]; }

	bool renderBoundingBox = false;
	bool renderLight = false;


	void CreateNewBounding(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , int idx);
	void DeleteBounding(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx , int bIdx);


};
class SkinnedInstancingShader :public SkinnedAnimationStandardInstanceShader
{
protected:
	std::vector<LoadedModelInfo*> instanceModels;
	std::vector<SkinnedInstanceBuffer*> instanceBuffer;
	std::vector<Texture*> textures;

public:

	SkinnedInstancingShader();
	virtual ~SkinnedInstancingShader();
	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context , int fobjectSize);

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx);
	void UpdateShaderVariables(int idx, int objIdx);
	virtual void ReleaseShaderVariables();

	virtual void ReleaseObjects();
	virtual void ReleaseUploadBuffers();

	void SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void SetDummyFarAway();
	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx);
	virtual void Render(ID3D12GraphicsCommandList *commandList, Camera *pCamera);
	virtual GameObject* GetGameObject(int idx, int objectidx) { return instanceBuffer[idx]->objects[objectidx]; }
	virtual void AddGameObject(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, XMFLOAT3 pointxyz, ID3D12RootSignature* graphicsRootSignature, int objectType , bool noiseFlag);
	virtual void LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType);
	virtual void DeleteGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int bufferIndex, int objectindex);
	virtual void DeleteAllObjcet();
	void BuildNewSkinedObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, LoadedModelInfo* context, XMFLOAT3 boundingSize);

	virtual std::vector< SkinnedInstanceBuffer*> GetSkinedBuffer() { return instanceBuffer; }
	virtual std::vector<GameObject*> GetGameObjectIdx(int idx) {return instanceBuffer[idx]->objects; }
};