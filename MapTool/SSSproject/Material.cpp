#include "stdafx.h"
#include "Material.h"
#include "GameObject.h"

Material::Material(int texturesNum)
{
	this->texturesNum = texturesNum;

	textures = new Texture*[texturesNum];
	textureNames = new _TCHAR[texturesNum][128];
	for (int i = 0; i < texturesNum; i++) textures[i] = NULL;
	for (int i = 0; i < texturesNum; i++) textureNames[i][0] = '\0';
}

Material::~Material()
{
	if (shader) shader->Release();

	if (texturesNum > 0)
	{
		for (int i = 0; i < texturesNum; i++)
			if (textures[i]) textures[i]->Release();
		delete[] textures;

		if (textureNames) delete[] textureNames;
	}
}

void Material::SetTexture(Texture *texture, UINT idx)
{
	if (textures[idx])
		textures[idx]->Release();
	textures[idx] = texture;
	if (textures[idx])
		textures[idx]->AddRef();
}

void Material::SetShader(Shader *shader)
{
	if (this->shader)
		this->shader->Release();
	this->shader = shader;
	if (this->shader)
		this->shader->AddRef();
}

void Material::SetTextureIdx(UINT type, int idx)
{
	if (type == MATERIAL_DIFFUSE_MAP)
		textureIdx += idx;

	if (type == MATERIAL_NORMAL_MAP)
		textureIdx += idx * 100;

	if (type == MATERIAL_SPECULAR_MAP)
		textureIdx += idx * 10000;

	if (type == MATERIAL_METALLIC_MAP)
		textureIdx += idx * 1000000;

	if (type == MATERIAL_EMISSION_MAP)
		textureIdx += idx * 100000000;
}

void Material::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList)
{
	commandList->SetGraphicsRoot32BitConstants(GraphicsRootMaterialInfo, 1, &reflection, 0);
	commandList->SetGraphicsRoot32BitConstants(GraphicsRootMaterialInfo, 1, &textureMask, 1);
	commandList->SetGraphicsRoot32BitConstants(GraphicsRootMaterialInfo, 1, &textureIdx, 2);
}

void Material::ReleaseShaderVariables()
{
	if (shader)
		shader->ReleaseShaderVariables();

}

void Material::ReleaseUploadBuffers()
{
	if (textures)
		for (int i = 0; i < texturesNum; i++)
		{
			if (textures[i]) textures[i]->ReleaseUploadBuffers();
		}
}

void Material::LoadTextureFromFile(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, UINT type, UINT rootParameter, _TCHAR * textureName, Texture ** textures, GameObject * parent, FILE * inFile, char* folderName, std::vector<std::pair<int, _TCHAR*>>& textureMapNames, int& idx)
{
	char pstrTextureName[64] = { '\0' };

	BYTE nStrLength = 64;
	UINT nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
	nReads = (UINT)::fread(pstrTextureName, sizeof(char), nStrLength, inFile);
	pstrTextureName[nStrLength] = '\0';

	bool bDuplicated = false;
	if (strcmp(pstrTextureName, "null"))
	{
		SetTextureMask(type);

		char typeFolder[] = "Assets/Image/";
		char slash[] = "/";

		char folderFullName[128] = { '\0' };
		char pstrFilePath[128] = { '\0' };

		strcpy_s(folderFullName, typeFolder);
		strcat_s(folderFullName, folderName);
		strcat_s(folderFullName, slash);

		strcpy_s(pstrFilePath, 128, folderFullName);

		bDuplicated = (pstrTextureName[0] == '@');
		strcpy_s(pstrFilePath + strlen(folderFullName), 128 - strlen(folderFullName), (bDuplicated) ? (pstrTextureName + 1) : pstrTextureName);
		strcpy_s(pstrFilePath + strlen(folderFullName) + ((bDuplicated) ? (nStrLength - 1) : nStrLength), 128 - strlen(folderFullName) - ((bDuplicated) ? (nStrLength - 1) : nStrLength), ".dds");

		size_t nConverted = 0;
		mbstowcs_s(&nConverted, textureName, 128, pstrFilePath, 128);

#define _WITH_DISPLAY_TEXTURE_NAME

#ifdef _WITH_DISPLAY_TEXTURE_NAME
		static int nTextures = 0, nRepeatedTextures = 0;
		TCHAR pstrDebug[256] = { 0 };
		_stprintf_s(pstrDebug, 256, _T("Texture Name: %d %c %s\n"), (pstrTextureName[0] == '@') ? nRepeatedTextures++ : nTextures++, (pstrTextureName[0] == '@') ? '@' : ' ', textureName);
		OutputDebugString(pstrDebug);
#endif


		if (!bDuplicated)
		{
			SetTextureIdx(type, idx);
			textureMapNames.emplace_back(idx, textureName);
			idx++;
		}
		else
		{
			for (int i = 0; i < textureMapNames.size(); ++i)
			{
				if (!_tcsncmp(textureMapNames[i].second, textureName, _tcslen(textureName)))
				{
					SetTextureIdx(type, textureMapNames[i].first);
					break;
				}
			}

		}

	}
}

Shader *Material::pSkinnedAnimationShader = NULL;
Shader *Material::pStandardShader = NULL;

void Material::PrepareShaders(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature)
{
	pStandardShader = new StandardInstanceShader();
	pStandardShader->CreateShader(device, graphicsRootSignature);
	pStandardShader->CreateShaderVariables(device, commandList);

	pSkinnedAnimationShader = new SkinnedAnimationStandardShader();
	pSkinnedAnimationShader->CreateShader(device, graphicsRootSignature);
	pSkinnedAnimationShader->CreateShaderVariables(device, commandList);
}