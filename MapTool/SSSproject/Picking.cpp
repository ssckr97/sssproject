#include "Picking.h"

int Picking::CheckPickingObjects(POINT & CursorPos, Camera & camera, int windowClientWidth, int windowClientHeight, std::vector<GameObject*> obj, int objNum, float& nearDistance , int& index)
{

	XMFLOAT3 rayOrigin, rayDirection;
	float oldDistance = nearDistance;

	//마우스 커서 좌표를 -1 과 1 사이의 범위로 이동시킨다. 
	float pointx = ((2.0f * (float)CursorPos.x) / (float)windowClientWidth) - 1.0f;
	float pointy = ((2.0f * (float)CursorPos.y) / (float)windowClientHeight) - 1.0f;

	//뷰포트의 종횡비를 고려하여 투영행렬을 사용한다음 점을 조정 
	XMFLOAT4X4 projectionMartix = camera.GetProjectionMatrix();


	pointx = pointx / projectionMartix._11;
	pointy = -pointy / projectionMartix._22;

	//카메라 공간에서 카메라의 위치
	rayOrigin = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//카메라 공간에서 광선의 방향 
	rayDirection = XMFLOAT3(pointx, pointy, 1.0f);

	//카메라의 역변환행렬 
	XMFLOAT4X4 inverseViewMatrix = Matrix4x4::Inverse(camera.GetViewMatrix());
	//월드공간에서 광선의 시작점	
	rayOrigin = Vector3::TransformCoord(rayOrigin, inverseViewMatrix);
	//월드공간에서 광선의 방향 
	rayDirection = Vector3::TransformCoord(rayDirection, inverseViewMatrix);
	//auto tempoutDirection = Vector3::Subtract(rayDirection, rayOrigin);
	rayDirection = Vector3::Normalize(Vector3::Subtract(rayDirection, rayOrigin));
	//오브젝트의 월드 변환행렬 
	XMVECTOR tempOrigin = XMLoadFloat3(&rayOrigin);
	XMVECTOR tempDirection = XMLoadFloat3(&rayDirection);
	

	GameObject* selectobj = NULL;
	for (int i = 0; i < objNum; ++i) {
		bool intersect = false;
		float hitDistance = FLT_MAX;
		if (!obj[i]->GetBoxType()) {
			if (obj[i]->GetOBB().Intersects(tempOrigin, tempDirection, hitDistance)) {
				intersect = true;
				oldDistance = Vector3::Length(Vector3::Subtract(rayOrigin, obj[i]->GetPosition()));
				if (oldDistance < nearDistance) {
					nearDistance = hitDistance;
					selectobj = obj[i];
					index = i;
				}
			}
		}
		else {
			if (obj[i]->GetAABB().Intersects(tempOrigin, tempDirection, hitDistance)) {
				intersect = true;
				nearDistance = Vector3::Length(Vector3::Subtract(rayOrigin, obj[i]->GetPosition()));
				if (hitDistance < nearDistance) {
					nearDistance = hitDistance;
					selectobj = obj[i];
					index = i;
				}
			}
		}
	}
	//바운딩 박스와 교차하면 더 정확한 검사를 위해 메쉬와의 교차검사를 진행
	if (!selectobj) 
		return -1;
	else 
		return index;
	
}

bool Picking::CheckPickingTerrain(POINT & CursorPos, Camera & camera, int windowClientWidth, int windowClientHeight,XMFLOAT3& pickPlace, GameObject & terrain, float & nearDistance, int& bIdx)
{

	XMFLOAT3 rayOrigin, rayDirection;
	
	//마우스 커서 좌표를 -1 과 1 사이의 범위로 이동시킨다. 
	double pointx = ((2.0f * (double)CursorPos.x) / (double)camera.GetViewport().Width) - 1.0f;
	double pointy = ((2.0f * (double)CursorPos.y) / (double)camera.GetViewport().Height) - 1.0f;

	//뷰포트의 종횡비를 고려하여 투영행렬을 사용한다음 점을 조정 
	XMFLOAT4X4 projectionMartix = camera.GetProjectionMatrix();

	pointx = pointx / (projectionMartix._11);
	pointy = -pointy / (projectionMartix._22);

	//카메라 공간에서 카메라의 위치
	rayOrigin = XMFLOAT3(0.0f, 0.0f, 0.0f);
	//카메라 공간에서 광선의 방향 
	rayDirection = XMFLOAT3(pointx, pointy, 1.0f);

	//카메라의 역변환행렬 
	XMFLOAT4X4 inverseViewMatrix = Matrix4x4::Inverse(camera.GetViewMatrix());


	//월드공간에서 광선의 시작점	
	rayOrigin = Vector3::TransformCoord(rayOrigin, inverseViewMatrix);
	//월드공간에서 광선의 방향 
	rayDirection = Vector3::TransformCoord(rayDirection, inverseViewMatrix);
	auto tempoutDirection = Vector3::Subtract(rayDirection, rayOrigin);
	rayDirection = Vector3::Normalize(Vector3::Subtract(rayDirection, rayOrigin));
	//오브젝트의 월드 변환행렬 
	XMVECTOR tempOrigin = XMLoadFloat3(&rayOrigin);
	XMVECTOR tempDirection = XMLoadFloat3(&rayDirection);
	int index{ NULL };
	
	bool intersect = false;

	
	float boxDistance = FLT_MAX;

	BoundingBox tempBox;
	HeightMapGridMesh* terrainMesh = (HeightMapGridMesh*)terrain.GetMesh(0);
	std::map<float, int> boundingsMap;
	std::vector<BoundingBox> boundingVec;

	for (int i = 0; i < TERRAINBOUNDINGS; ++i)
	{
		terrainMesh->GetBoundings()[i].Transform(tempBox, XMLoadFloat4x4(&terrain.GetWorld()));
		float dist = Vector3::Length(Vector3::Subtract(tempBox.Center, rayOrigin));
		boundingVec.emplace_back(tempBox);
		boundingsMap[dist] = i;
	}

	for (auto p : boundingsMap) {
		float hitDistance = FLT_MAX;

		if (boundingVec[p.second].Intersects(tempOrigin, tempDirection, hitDistance)) {
			intersect = true;
			bIdx = terrainMesh->GetBoudingVertexIndex()[p.second];

			if (terrainMesh->CheckRayIntersect(tempOrigin, tempDirection, nearDistance, terrain.GetWorld(), bIdx))
				break;
		}
	}

	if (!intersect)
		return false;

	pickPlace = Vector3::Add(rayOrigin, Vector3::ScalarProduct(rayDirection, nearDistance));
	return true;
}

void Picking::MoveObjectToDirection(std::vector<GameObject*> ArrowObject, float deltaX, float deltaY)
{
	isPicking = false;
	if (pickingIndex == LOOKARROW) {
		XMFLOAT3 look = ArrowObject[LOOKARROW]->GetUp();
		deltaX = abs(look.x) * deltaX + deltaX * abs(look.z);
		deltaY = abs(look.y) * deltaY + deltaY * abs(look.z);
		oldObject->SetPosition(Vector3::Add(oldObject->GetPosition(), Vector3::ScalarProduct(look, (-deltaX) + (-deltaY))));
		if (oldObject->GetBoxType() == OBBBOX) {
			oldObject->SetOBBPos(oldObject->GetPosition());
			oldObject->SetOBBBoundingBox();
		}
		else if (oldObject->GetBoxType() == AABBBOX) {
			oldObject->SetAABBPos(oldObject->GetPosition());
		}
		for (int i = 0; i < 3; ++i) {
			ArrowObject[i]->SetPosition(Vector3::Add(ArrowObject[i]->GetPosition(), Vector3::ScalarProduct(look, (-deltaX) + (-deltaY))));
			ArrowObject[i]->SetOBBPos(ArrowObject[i]->GetPosition());
		}
	}
	if (pickingIndex == RIGHTARROW) {
		XMFLOAT3 right = ArrowObject[RIGHTARROW]->GetUp();
		deltaX = abs(right.x) * deltaX + deltaX * abs(right.z);
		deltaY = abs(right.y) * deltaY + deltaY * abs(right.z);
		oldObject->SetPosition(Vector3::Add(oldObject->GetPosition(), Vector3::ScalarProduct(right, (deltaX)+(deltaY))));
		if (oldObject->GetBoxType() == OBBBOX) {
			oldObject->SetOBBPos(oldObject->GetPosition());
			oldObject->SetOBBBoundingBox();
		}
		else if (oldObject->GetBoxType() == AABBBOX) {
			oldObject->SetAABBPos(oldObject->GetPosition());
		}
		for (int i = 0; i < ARROWNUM; ++i) {
			ArrowObject[i]->SetPosition(Vector3::Add(ArrowObject[i]->GetPosition(), Vector3::ScalarProduct(right, (deltaX)+(deltaY))));
			ArrowObject[i]->SetOBBPos(ArrowObject[i]->GetPosition());
		}
	}
	if (pickingIndex == UPARRROW) {
		XMFLOAT3 up = ArrowObject[UPARRROW]->GetUp();
		deltaX = abs(up.x) * deltaX + deltaX * abs(up.z);
		deltaY = abs(up.y) * deltaY + deltaY * abs(up.z);
		oldObject->SetPosition(Vector3::Add(oldObject->GetPosition(), Vector3::ScalarProduct(up, (-deltaX) + (-deltaY))));
		if (oldObject->GetBoxType() == OBBBOX) {
			oldObject->SetOBBPos(oldObject->GetPosition());
			oldObject->SetOBBBoundingBox();
		}
		else if (oldObject->GetBoxType() == AABBBOX) {
			oldObject->SetAABBPos(oldObject->GetPosition());
		}
		for (int i = 0; i < ARROWNUM; ++i) {
			ArrowObject[i]->SetPosition(Vector3::Add(ArrowObject[i]->GetPosition(), Vector3::ScalarProduct(up, (-deltaX) + (-deltaY))));
			ArrowObject[i]->SetOBBPos(ArrowObject[i]->GetPosition());
		}
	}

}

void Picking::SetArrowsPosition(GameObject *object, int index)
{
	object->SetPosition(selectObject->GetPosition());
	//노말라이즈된 Look , Right , up을 가져온다.
	object->SetLook(selectObject->GetLook());
	object->SetRight(selectObject->GetRight());
	object->SetUp(selectObject->GetUp());
	if (index == LOOKARROW) {
		object->SetPosition(Vector3::Add(object->GetPosition() , Vector3::ScalarProduct(selectObject->GetLook(), 20)));
		object->Rotate(90.0f, 0.0f, 0.0f);
		object->SetOBB(object->GetPosition(), XMFLOAT3(12.0f, 18.0f, 6.0f), XMFLOAT4(0, 0, 0, 0));
		object->SetOBBBoundingBox();
	}
	else if (index == RIGHTARROW) {		
		object->SetPosition(Vector3::Add(object->GetPosition(), Vector3::ScalarProduct(selectObject->GetRight(), 20)));
		object->Rotate(0.0, 0.0f, -90.0f);
		object->SetOBB(object->GetPosition(), XMFLOAT3(12.0f, 18.0f, 6.0f), XMFLOAT4(0, 0, 0, 0));
		object->SetOBBBoundingBox();
	}
	else if (index == UPARRROW) {
		object->SetPosition(Vector3::Add(object->GetPosition(), Vector3::ScalarProduct(selectObject->GetUp(), 20)));
		object->Rotate(0.0, -90.0f, 0.0f);
		object->SetOBB(object->GetPosition(), XMFLOAT3(12.0f, 18.0f, 6.0f), XMFLOAT4(0, 0, 0, 0));
		object->SetOBBBoundingBox();
	}
}
