#include "stdafx.h"
#include "ObjectShader.h"
#include "Scene.h"
#include "Material.h"

PlayerShader::PlayerShader()
{
}
PlayerShader::~PlayerShader()
{
}

D3D12_INPUT_LAYOUT_DESC PlayerShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 2;
	D3D12_INPUT_ELEMENT_DESC *inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	return(inputLayoutDesc);
}

D3D12_SHADER_BYTECODE PlayerShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSPlayer", "vs_5_1", &vertexShaderBlob));
}
D3D12_SHADER_BYTECODE PlayerShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSPlayer", "ps_5_1", &pixelShaderBlob));
}

void PlayerShader::CreateShader(ID3D12Device *device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];
	Shader::CreateShader(device, graphicsRootSignature);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}
void PlayerShader::CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList)
{
	UINT cbPlayerBytes = ((sizeof(CB_PLAYER_INFO) + 255) & ~255); //256의 배수
	cbPlayer = ::CreateBufferResource(device, commandList, NULL, cbPlayerBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	cbPlayer->Map(0, NULL, (void **)&cbMappedPlayer);
}

void PlayerShader::UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, XMFLOAT4X4* world)
{
	XMFLOAT4X4 playerWorld;
	XMStoreFloat4x4(&playerWorld, XMMatrixTranspose(XMLoadFloat4x4(world)));
	::memcpy(&cbMappedPlayer->world, &playerWorld, sizeof(XMFLOAT4X4));
}

void PlayerShader::ReleaseShaderVariables()
{
	if (cbPlayer) cbPlayer->Unmap(0, NULL);
	if (cbPlayer) cbPlayer->Release();
}

void PlayerShader::Render(ID3D12GraphicsCommandList *commandList, Camera *pCamera)
{
	Shader::OnPrepareRender(commandList, 0);

	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = cbPlayer->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootPlayer, d3dGpuVirtualAddress);
}


void AssistShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature* graphicsRootSignature, void * context)
{
	HeightMapTerrain *terrain = (HeightMapTerrain *)context;
	float terrainWidth = terrain->GetWidth(), terrainLength = terrain->GetLength();
	objectBuffer = new ObjectBuffer;

	objectBuffer->objectsNum = ARROWNUM;
	objectBuffer->objects.reserve(objectBuffer->objectsNum);

	objectBuffer->framesNum = 1;

	CreateShaderVariables(device, commandList);

	//다른 루트파라매터로 여러개의 쉐이더 리소스 뷰를 만든다.
	D3D12_GPU_DESCRIPTOR_HANDLE handle = Scene::CreateConstantBufferViews(device, commandList, objectBuffer->objectsNum, objectBuffer->cbGameObjects[0], objectBuffer->cbGameObjectBytes);

	RotatingObject *rotatingObject = NULL;
	float Red = 0.0f;
	float Green = 0.0f;
	float Blue = 0.0f;
	for (int i = 0; i < ARROWNUM; ++i) {
		rotatingObject = new RotatingObject();
		float xPosition = 100.0f + 100.0f * i;
		float zPosition = 0.0f;
		float yPosition = 0.0f;

		rotatingObject->SetPosition(xPosition, yPosition, zPosition);
		rotatingObject->SetRotationAxis(XMFLOAT3(0.0f, 1.0f, 0.0f));
		rotatingObject->SetRotationSpeed(0);
		rotatingObject->CreateCbvGPUDescriptorHandles();
		rotatingObject->SetCbvGPUDescriptorHandlePtr(handle.ptr + (::gnCbvSrvDescriptorIncrementSize * objectBuffer->objects.size()));
		rotatingObject->SetBoxType(false);
		rotatingObject->SetObjectType(ARROWTYPE);
		ArrowMesh *arrowMesh = new ArrowMesh(device, commandList, 12.0f, 12.0f, 12.0f, Red = (i == 0) ? 0.5f : 0.0f, Green = (i == 1) ? 0.5f : 0.0f, Blue = (i == 2) ? 0.5f : 0.0f);
		rotatingObject->SetMesh(0, arrowMesh);
		rotatingObject->SetOBB(rotatingObject->GetPosition(), XMFLOAT3(12.0f, 18.0f, 6.0f), XMFLOAT4(0, 0, 0, 0));
		objectBuffer->objects.emplace_back(rotatingObject);
	}

}

void AssistShader::CreateShader(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	ID3DBlob* vertexShaderBlob = NULL, *pixelShaderBlob = NULL;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
	::ZeroMemory(&pipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	pipelineStateDesc.pRootSignature = graphicsRootSignature;
	pipelineStateDesc.VS = CreateVertexShader();
	pipelineStateDesc.PS = CreatePixelShader();
	pipelineStateDesc.RasterizerState = CreateRasterizerState();
	pipelineStateDesc.BlendState = CreateBlendState();
	pipelineStateDesc.DepthStencilState = CreateDepthStencilState();
	pipelineStateDesc.InputLayout = CreateInputLayout();
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[0]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[]
		pipelineStateDesc.InputLayout.pInputElementDescs;
}

D3D12_DEPTH_STENCIL_DESC AssistShader::CreateDepthStencilState()
{

	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = TRUE; // 뎁스 검사하지 않는다.
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL; // 뎁스값 무조껀 쓴다 
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS; // 뎁스 비교 무조껀 통과 
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return(depthStencilDesc);
}
D3D12_INPUT_LAYOUT_DESC AssistShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 2;
	D3D12_INPUT_ELEMENT_DESC *inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	return(inputLayoutDesc);

}

D3D12_SHADER_BYTECODE AssistShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSDiffused", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE AssistShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSDiffused", "ps_5_1", &pixelShaderBlob));
}

ScreenSpaceDecalShader::ScreenSpaceDecalShader(ID3D12Resource* depthStencilBuffer)
{
	this->depthStencilBuffer = depthStencilBuffer;
}

void ScreenSpaceDecalShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, void * context)
{
	ID3D12Resource *terrainDepthResource = (ID3D12Resource *)context;
	objectBuffer = new ObjectBuffer;

	objectBuffer->objectsNum = 1;

	objectBuffer->objects.reserve(objectBuffer->objectsNum);

	objectBuffer->framesNum = 1;

	CreateShaderVariables(device, commandList);
	//다른 루트파라매터로 여러개의 쉐이더 리소스 뷰를 만든다.
	D3D12_GPU_DESCRIPTOR_HANDLE handle = Scene::CreateConstantBufferViews(device, commandList, objectBuffer->objectsNum, objectBuffer->cbGameObjects[0], objectBuffer->cbGameObjectBytes);

	RotatingObject *rotatingObject = NULL;
	rotatingObject = new RotatingObject();
	rotatingObject->SetPosition(0, 0, 0);
	rotatingObject->SetRotationAxis(XMFLOAT3(0.0f, 1.0f, 0.0f));
	rotatingObject->SetRotationSpeed(0);
	rotatingObject->CreateCbvGPUDescriptorHandles();
	rotatingObject->SetCbvGPUDescriptorHandlePtr(handle.ptr);
	rotatingObject->SetBoxType(false);
	rotatingObject->SetObjectType(RECTANGLETYPE);
	TexturedCubeMesh *rectangle = new TexturedCubeMesh(device, commandList, 1.0f, 1.0f, 1.0f);
	rotatingObject->SetMesh(0, rectangle);
	rotatingObject->SetOBB(rotatingObject->GetPosition(), XMFLOAT3(0.5, 0.5f, 0.5f), XMFLOAT4(0, 0, 0, 0));
	objectBuffer->objects.emplace_back(rotatingObject);
}

void ScreenSpaceDecalShader::CreateShader(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	ID3DBlob* vertexShaderBlob = NULL, *pixelShaderBlob = NULL;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
	::ZeroMemory(&pipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	pipelineStateDesc.pRootSignature = graphicsRootSignature;
	pipelineStateDesc.VS = CreateVertexShader();
	pipelineStateDesc.PS = CreatePixelShader();
	pipelineStateDesc.RasterizerState = CreateRasterizerState();
	pipelineStateDesc.BlendState = CreateBlendState();
	pipelineStateDesc.DepthStencilState = CreateDepthStencilState();
	pipelineStateDesc.InputLayout = CreateInputLayout();
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[0]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[]
		pipelineStateDesc.InputLayout.pInputElementDescs;
}

D3D12_BLEND_DESC ScreenSpaceDecalShader::CreateBlendState()
{
	D3D12_BLEND_DESC d3dBlendDesc;
	::ZeroMemory(&d3dBlendDesc, sizeof(D3D12_BLEND_DESC));
	d3dBlendDesc.AlphaToCoverageEnable = FALSE;
	d3dBlendDesc.IndependentBlendEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].BlendEnable = TRUE;
	d3dBlendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	d3dBlendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	d3dBlendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	d3dBlendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	d3dBlendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	d3dBlendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	d3dBlendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;

	return(d3dBlendDesc);


}

D3D12_DEPTH_STENCIL_DESC ScreenSpaceDecalShader::CreateDepthStencilState() {
	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = TRUE; // 뎁스 검사하지 않는다.
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL; // 뎁스값 무조껀 쓴다 
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_ALWAYS; // 뎁스 비교 무조껀 통과 
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return(depthStencilDesc);
}

D3D12_INPUT_LAYOUT_DESC ScreenSpaceDecalShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];

	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_RASTERIZER_DESC ScreenSpaceDecalShader::CreateRasterizerState()
{
	D3D12_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(rasterizerDesc);
}



D3D12_SHADER_BYTECODE ScreenSpaceDecalShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSDecalShader", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE ScreenSpaceDecalShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSDecalShader", "ps_5_1", &pixelShaderBlob));
}

void BoundingBoxShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, void * context)
{
	HeightMapTerrain *terrain = (HeightMapTerrain *)context;
	float terrainWidth = terrain->GetWidth(), terrainLength = terrain->GetLength();

	objectBuffer = new ObjectBuffer;

	HeightMapGridMesh* heightMapMesh = (HeightMapGridMesh*)terrain->GetMesh(0);
	BoundingBox* boundings = heightMapMesh->GetBoundings();

	objectBuffer->objectsNum = TERRAINBOUNDINGS;
	objectBuffer->objects.reserve(objectBuffer->objectsNum);

	objectBuffer->framesNum = 1;

	CreateShaderVariables(device, commandList);
	//다른 루트파라매터로 여러개의 쉐이더 리소스 뷰를 만든다.
	D3D12_GPU_DESCRIPTOR_HANDLE handle = Scene::CreateConstantBufferViews(device, commandList, objectBuffer->objectsNum, objectBuffer->cbGameObjects[0], objectBuffer->cbGameObjectBytes);

	RotatingObject *rotatingObject = NULL;
	for (int i = 0; i < TERRAINBOUNDINGS; ++i) {
		rotatingObject = new RotatingObject();
		float xPosition = 0.0f + 100.0f * i;
		float zPosition = 0.0f;
		float yPosition = 0.0f;
		//auto tempMesh = terrain->GetMesh(i);
		XMFLOAT3 pos = Vector3::TransformCoord(boundings[i].Center, terrain->GetWorld());
		rotatingObject->SetPosition(pos);
		rotatingObject->SetRotationAxis(XMFLOAT3(0.0f, 1.0f, 0.0f));
		rotatingObject->SetRotationSpeed(0);
		rotatingObject->CreateCbvGPUDescriptorHandles();
		rotatingObject->SetCbvGPUDescriptorHandlePtr(handle.ptr + (::gnCbvSrvDescriptorIncrementSize * i));
		rotatingObject->SetBoxType(false);
		rotatingObject->SetObjectType(BOUNDINGBOXTYPE);
		BoundingBoxMesh *boundingMesh = new BoundingBoxMesh(device, commandList, 64.0f, 300.0f, 64.0f);
		rotatingObject->SetMesh(0, boundingMesh);
		rotatingObject->SetOBB(rotatingObject->GetPosition(), XMFLOAT3(12.0f, 18.0f, 6.0f), XMFLOAT4(0, 0, 0, 0));
		objectBuffer->objects.emplace_back(rotatingObject);
	}

}

void BoundingBoxShader::CreateShader(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	Shader::CreateShader(device, graphicsRootSignature);

}

D3D12_RASTERIZER_DESC BoundingBoxShader::CreateRasterizerState()
{
	D3D12_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_WIREFRAME;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(rasterizerDesc);
}

void SkyBoxShader::AnimateObjects(float timeElapsed, Camera * camera)
{
	objectBuffer->objects[0]->SetPosition(camera->GetPosition());
	objectBuffer->objects[0]->UpdateTransform(objectBuffer->cbMappedGameObjects, objectBuffer->cbGameObjectBytes, 0, NULL);
}

void SkyBoxShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, void * context)
{
	objectBuffer = new ObjectBuffer;

	objectBuffer->objectsNum = 1;
	objectBuffer->objects.reserve(objectBuffer->objectsNum);

	objectBuffer->framesNum = 1;

	CreateShaderVariables(device, commandList);
	D3D12_GPU_DESCRIPTOR_HANDLE handle = Scene::CreateConstantBufferViews(device, commandList, objectBuffer->objectsNum, objectBuffer->cbGameObjects[0], objectBuffer->cbGameObjectBytes);

	GameObject *skyBoxObject = NULL;
	skyBoxObject = new GameObject();
	skyBoxObject->SetPosition(0, 0, 0);
	skyBoxObject->CreateCbvGPUDescriptorHandles();
	skyBoxObject->SetCbvGPUDescriptorHandlePtr(handle.ptr);
	skyBoxObject->SetBoxType(false);
	skyBoxObject->SetObjectType(SKYBOXTYPE);

	TexturedCubeMesh *texturedCubeMesh = new TexturedCubeMesh(device, commandList, 20.0f, 20.0f, 20.0f);

	skyBoxObject->SetMesh(0, texturedCubeMesh);

	skyBoxObject->SetAABB(skyBoxObject->GetPosition(), XMFLOAT3(0.5, 0.5f, 0.5f));
	objectBuffer->objects.emplace_back(skyBoxObject);
}



D3D12_INPUT_LAYOUT_DESC SkyBoxShader::CreateInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];
	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE SkyBoxShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSSkyBoxed", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE SkyBoxShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSSkyBoxed", "ps_5_1", &pixelShaderBlob));
}

D3D12_RASTERIZER_DESC SkyBoxShader::CreateRasterizerState()
{
	//FIllMode와 CullMode를 변경하여 그려지는 객체의 내부를 칠할지 말지 등을 결정할 수 있다.
	D3D12_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_FRONT;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(rasterizerDesc);
}

D3D12_DEPTH_STENCIL_DESC SkyBoxShader::CreateDepthStencilState()
{
	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = FALSE;
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return(depthStencilDesc);
}

D3D12_BLEND_DESC SkyBoxShader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = FALSE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = FALSE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(blendDesc);
}

SkinnedAnimationObjectsShader::SkinnedAnimationObjectsShader()
{
}

SkinnedAnimationObjectsShader::~SkinnedAnimationObjectsShader()
{
}

D3D12_BLEND_DESC SkinnedAnimationObjectsShader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = TRUE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(blendDesc);
}

void SkinnedAnimationObjectsShader::CreateShader(ID3D12Device *device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	Shader::CreateShader(device, graphicsRootSignature);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}

void SkinnedAnimationObjectsShader::BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context, int fobjectSize)
{
	SkinnedObjectBuffer* playerBuffer = new SkinnedObjectBuffer;

	LoadedModelInfo* playerModel = (LoadedModelInfo*)context[fobjectSize];

	playerBuffer->textureName = playerModel->modelRootObject->textureName;
	playerBuffer->skinnedMeshNum = playerModel->skinnedMeshNum;
	playerBuffer->standardMeshNum = playerModel->standardMeshNum;
	playerBuffer->skinnedMeshes = new SkinnedMesh*[playerModel->skinnedMeshNum];
	for (int i = 0; i < playerBuffer->skinnedMeshNum; i++) playerBuffer->skinnedMeshes[i] = playerModel->skinnedMeshes[i];


	playerBuffer->object = new AnimationObject(device, commandList, graphicsRootSignature, playerModel, 1);
	playerBuffer->object->skinnedAnimationController->SetTrackAnimationSet(0, 0);
	playerBuffer->object->skinnedAnimationController->SetTrackSpeed(0, 0.25);
	playerBuffer->object->skinnedAnimationController->SetTrackPosition(0, 0.0f);
	playerBuffer->object->SetPosition(XMFLOAT3(200, 250, 300));

	objectBuffer.emplace_back(playerBuffer);

	for (int i = 0; i < objectBuffer.size(); ++i)
		CreateShaderVariables(device, commandList, i);

	// StandardMesh cbvHandle 할당.

	SetTextures(device, commandList);

	if (playerModel) delete playerModel;
	playerModel = NULL;
}

void SkinnedAnimationObjectsShader::ReleaseObjects()
{
	for (int i = 0; i < objectBuffer.size(); ++i) {
		if (objectBuffer[i]->object)
		{
			objectBuffer[i]->object->Release();
		}
	}

}

void SkinnedAnimationObjectsShader::AnimateObjects(float timeElapsed, Camera* camera)
{
	elapsedTime = timeElapsed;
}

void SkinnedAnimationObjectsShader::ReleaseUploadBuffers()
{
	for (int i = 0; i < objectBuffer.size(); ++i) {
		if (objectBuffer[i]->object)
		{
			objectBuffer[i]->object->ReleaseUploadBuffers();
		}
	}
}

void SkinnedAnimationObjectsShader::CreateShaderVariables(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx)
{
	UINT skinnedStride = 0;
	ID3D12Resource** tempSkinned = NULL;
	XMFLOAT4X4** tempMappedSkinned = NULL;

	skinnedStride = sizeof(XMFLOAT4X4) * SKINNED_ANIMATION_BONES;
	tempSkinned = new ID3D12Resource*[objectBuffer[idx]->skinnedMeshNum];
	tempMappedSkinned = new XMFLOAT4X4*[objectBuffer[idx]->skinnedMeshNum];

	for (int i = 0; i < objectBuffer[idx]->skinnedMeshNum; ++i)
	{
		tempSkinned[i] = ::CreateBufferResource(device, commandList, NULL, skinnedStride, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_GENERIC_READ, NULL);
		tempSkinned[i]->Map(0, NULL, (void **)&tempMappedSkinned[i]);
	}
	// Skinned Bone Transform Buffers ( SRV )

	UINT standardStride = 0;
	ID3D12Resource** tempGameObject = NULL;
	VS_VB_INSTANCE_OBJECT** tempMappedGameObject = NULL;
	D3D12_VERTEX_BUFFER_VIEW* tempGameObjectView = NULL;

	standardStride = ((sizeof(VS_VB_INSTANCE_OBJECT) + 255) & ~255); //256의 배수
	tempGameObject = new ID3D12Resource*[objectBuffer[idx]->standardMeshNum];
	tempMappedGameObject = new VS_VB_INSTANCE_OBJECT*[objectBuffer[idx]->standardMeshNum];
	tempGameObjectView = new D3D12_VERTEX_BUFFER_VIEW[objectBuffer[idx]->standardMeshNum];

	for (int j = 0; j < objectBuffer[idx]->standardMeshNum; ++j)
	{
		tempGameObject[j] = ::CreateBufferResource(device, commandList, NULL, standardStride, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		tempGameObject[j]->Map(0, NULL, (void **)&tempMappedGameObject[j]);

		tempGameObjectView[j].BufferLocation = tempGameObject[j]->GetGPUVirtualAddress();
		tempGameObjectView[j].StrideInBytes = standardStride;
		tempGameObjectView[j].SizeInBytes = standardStride;
	}
	// Standard Mesh Transform Buffer ( Vertex Buffer )

	objectBuffer[idx]->skinnedBuffer = tempSkinned;
	objectBuffer[idx]->mappedSkinnedBuffer = tempMappedSkinned;

	objectBuffer[idx]->vbGameObject = tempGameObject;
	objectBuffer[idx]->vbMappedGameObject = tempMappedGameObject;
	objectBuffer[idx]->vbGameObjectView = tempGameObjectView;
}

void SkinnedAnimationObjectsShader::UpdateShaderVariables(int idx)
{
	for (int i = 0; i < objectBuffer[idx]->skinnedMeshNum; ++i)
	{
		for (int j = 0; j < objectBuffer[idx]->skinnedMeshes[i]->skinningBones; ++j)
		{
			XMStoreFloat4x4(&objectBuffer[idx]->mappedSkinnedBuffer[i][j],
				XMMatrixTranspose(XMLoadFloat4x4(&objectBuffer[idx]->skinnedMeshes[i]->skinningBoneFrameCaches[j]->GetWorld())));
		}
	}
}

void SkinnedAnimationObjectsShader::OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx, bool isShadow)
{
	if (objectBuffer[idx]->diffuseIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(objectBuffer[idx]->diffuseIdx).rootParameterIndex, Scene::GetRootArgument(objectBuffer[idx]->diffuseIdx).srvGpuDescriptorHandle);
	if (objectBuffer[idx]->normalIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(objectBuffer[idx]->normalIdx).rootParameterIndex, Scene::GetRootArgument(objectBuffer[idx]->normalIdx).srvGpuDescriptorHandle);
	if (objectBuffer[idx]->specularIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(objectBuffer[idx]->specularIdx).rootParameterIndex, Scene::GetRootArgument(objectBuffer[idx]->specularIdx).srvGpuDescriptorHandle);
	if (objectBuffer[idx]->metallicIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(objectBuffer[idx]->metallicIdx).rootParameterIndex, Scene::GetRootArgument(objectBuffer[idx]->metallicIdx).srvGpuDescriptorHandle);
	if (objectBuffer[idx]->emissionIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(objectBuffer[idx]->emissionIdx).rootParameterIndex, Scene::GetRootArgument(objectBuffer[idx]->emissionIdx).srvGpuDescriptorHandle);

	if (pipelineStates) commandList->SetPipelineState(pipelineStates[idx]);
}

void SkinnedAnimationObjectsShader::Render(ID3D12GraphicsCommandList *commandList, Camera *pCamera, bool isShadow)
{
	OnPrepareRender(commandList, 0, isShadow);

	for (int j = 0; j < objectBuffer.size(); j++)
	{
		if (objectBuffer[j]->object)
		{
			objectBuffer[j]->object->Animate(elapsedTime);
			UpdateShaderVariables(j);
			for (int i = 0; i < objectBuffer[j]->skinnedMeshNum; ++i)
			{
				objectBuffer[j]->skinnedMeshes[i]->skinningBoneTransforms = objectBuffer[j]->skinnedBuffer[i];
				objectBuffer[j]->skinnedMeshes[i]->mappedSkinningBoneTransforms = objectBuffer[j]->mappedSkinnedBuffer[i];
			}
			objectBuffer[j]->object->UpdateTransformOnlyStandardMesh(objectBuffer[j]->vbMappedGameObject, 0, objectBuffer[j]->standardMeshNum, NULL);
			objectBuffer[j]->object->Render(commandList, pCamera, 1, objectBuffer[j]->vbGameObjectView, true);
		}
	}
}

void SkinnedAnimationObjectsShader::SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	for (int k = 0; k < objectBuffer.size(); ++k) {

		if (objectBuffer[k]->textureName.diffuse.size() > 0)
		{
			Texture* diffuseTexture = new Texture((int)objectBuffer[k]->textureName.diffuse.size(), ResourceTexture2D, 0);
			for (int i = 0; i < objectBuffer[k]->textureName.diffuse.size(); ++i)
			{
				diffuseTexture->LoadTextureFromFile(device, commandList, objectBuffer[k]->textureName.diffuse[i].second, objectBuffer[k]->textureName.diffuse[i].first);
			}
			objectBuffer[k]->diffuseIdx = Scene::CreateShaderResourceViews(device, commandList, diffuseTexture, GraphicsRootDiffuseTextures);
		}
		if (objectBuffer[k]->textureName.normal.size() > 0)
		{
			Texture* normalTexture = new Texture((int)objectBuffer[k]->textureName.normal.size(), ResourceTexture2D, 0);
			for (int i = 0; i < objectBuffer[k]->textureName.normal.size(); ++i)
			{
				normalTexture->LoadTextureFromFile(device, commandList, objectBuffer[k]->textureName.normal[i].second, objectBuffer[k]->textureName.normal[i].first);
			}
			objectBuffer[k]->normalIdx = Scene::CreateShaderResourceViews(device, commandList, normalTexture, GraphicsRootNormalTextures);
		}
		if (objectBuffer[k]->textureName.specular.size() > 0)
		{
			Texture* specularTexture = new Texture((int)objectBuffer[k]->textureName.specular.size(), ResourceTexture2D, 0);
			for (int i = 0; i < objectBuffer[k]->textureName.specular.size(); ++i)
			{
				specularTexture->LoadTextureFromFile(device, commandList, objectBuffer[k]->textureName.specular[i].second, objectBuffer[k]->textureName.specular[i].first);
			}
			objectBuffer[k]->specularIdx = Scene::CreateShaderResourceViews(device, commandList, specularTexture, GraphicsRootSpecularTextures);
		}
		if (objectBuffer[k]->textureName.metallic.size() > 0)
		{
			Texture* metallicTexture = new Texture((int)objectBuffer[k]->textureName.metallic.size(), ResourceTexture2D, 0);
			for (int i = 0; i < objectBuffer[k]->textureName.metallic.size(); ++i)
			{
				metallicTexture->LoadTextureFromFile(device, commandList, objectBuffer[k]->textureName.metallic[i].second, objectBuffer[k]->textureName.metallic[i].first);
			}
			objectBuffer[k]->metallicIdx = Scene::CreateShaderResourceViews(device, commandList, metallicTexture, GraphicsRootMetallicTextures);
		}
		if (objectBuffer[k]->textureName.emission.size() > 0)
		{
			Texture* emissionTexture = new Texture((int)objectBuffer[k]->textureName.emission.size(), ResourceTexture2D, 0);
			for (int i = 0; i < objectBuffer[k]->textureName.emission.size(); ++i)
			{
				emissionTexture->LoadTextureFromFile(device, commandList, objectBuffer[k]->textureName.emission[i].second, objectBuffer[k]->textureName.emission[i].first);
			}
			objectBuffer[k]->emissionIdx = Scene::CreateShaderResourceViews(device, commandList, emissionTexture, GraphicsRootEmissionTextures);
		}
	}
}