#include "HeightMapLayer.h"



HeightMapLayer::HeightMapLayer()
{
}


HeightMapLayer::~HeightMapLayer()
{
}

void HeightMapLayer::BuildLayer(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int width , int length, Diffused2TexturedVertex* diffusevertex, std::vector<SubIndex> subIndex , int vertexNum  , int indexNum)
{
	stride = sizeof(Diffused2TexturedVertex);
	std::vector<SubIndex> LayerSubIndex;
	LayerSubIndex.reserve(vertexNum);
	isBuild = true;

	diffused2TexturedVerices.clear();
	diffused2TexturedVerices.reserve(vertexNum);

	float fHeight = 0.0f, fMinHeight = +FLT_MAX, fMaxHeight = -FLT_MAX;
	int texNumber = 0;
	for (int i = 0; i < vertexNum; ++i) {
		diffused2TexturedVerices.emplace_back(diffusevertex[i]);
	}
	for (int i = 0, indexZ = 0; indexZ < length; indexZ++)
	{
		for (int indexX = 0; indexX < width; indexX++, i++)
		{
			texNumber = diffused2TexturedVerices[i].texNumber;
			if (texNumber == indexNum && indexX !=  width - 1 && indexZ !=  length - 1) {
				LayerSubIndex.emplace_back(indexX + indexZ * width, indexX + (indexZ * width) + width, indexX + (indexZ * width) + 1);
				LayerSubIndex.emplace_back(indexX + indexZ * width + width, indexX + indexZ * width + width + 1, indexX + indexZ * width + 1);
			}
		}
	}

	if (indexBuffer != NULL) {
		for (int i = 0; i < indicesNum; ++i) {
			indices[i] = NULL;
		}
	}

	if (vertexBuffer == NULL) {
		vertexBuffer = CreateBufferResource(device, commandList, NULL, sizeof(Diffused2TexturedVertex) * diffused2TexturedVerices.size(), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		vertexBuffer->Map(0, NULL, (void**)&mappedDiffued2TextureVerices);
		
		vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
		vertexBufferView.StrideInBytes = stride;
		vertexBufferView.SizeInBytes = stride * diffused2TexturedVerices.size();
	}
	for (int i = 0; i < diffused2TexturedVerices.size(); ++i) {
		mappedDiffued2TextureVerices[i] = diffused2TexturedVerices[i];
	}

	
	indicesNum = subIndex.size() * 3;
	if (indexBuffer == NULL) {
		if (indicesNum > 0) primitiveNum = (primitiveTopology ==
			D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ? (indicesNum / 3) : (indicesNum - 2);
		indexBuffer = CreateBufferResource(device, commandList, NULL, sizeof(UINT) * indicesNum, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_INDEX_BUFFER, NULL);
		indexBuffer->Map(0, NULL, (void**)&indices);
		indexBufferView.BufferLocation = indexBuffer->GetGPUVirtualAddress();
		indexBufferView.Format = DXGI_FORMAT_R32_UINT;
		indexBufferView.SizeInBytes = sizeof(UINT) * indicesNum;
	}
	
	for (int i = 0; i < LayerSubIndex.size(); ++i) {
		indices[i * 3 + 0] = LayerSubIndex[i].GetIndex()[0];
		indices[i * 3 + 1] = LayerSubIndex[i].GetIndex()[1];
		indices[i * 3 + 2] = LayerSubIndex[i].GetIndex()[2];
	}
}

void HeightMapLayer::ChangeLayerHeight(Diffused2TexturedVertex * diffusevertex , int verticeNum)
{
	for (int i = 0; i < verticeNum; ++i) {
		mappedDiffued2TextureVerices[i].position.y = diffusevertex[i].position.y;
		mappedDiffued2TextureVerices[i].normal = diffusevertex[i].normal;
	}
}

void HeightMapLayer::ChangeLayerTexNum(Diffused2TexturedVertex* diffusevertex, int verticeNum)
{
	for (int i = 0; i < verticeNum; ++i) {
		mappedDiffued2TextureVerices[i].texNumber = diffusevertex[i].texNumber;
	}
}

void HeightMapLayer::ChangeLayer(int width , int length , Diffused2TexturedVertex * diffusevertex, std::vector<SubIndex> subIndex, int vertexNum, int indexNum)
{
	std::vector<SubIndex> LayerSubIndex;

	diffused2TexturedVerices.clear();
	diffused2TexturedVerices.reserve(vertexNum);


	float fHeight = 0.0f, fMinHeight = +FLT_MAX, fMaxHeight = -FLT_MAX;
	int texNumber = 0;
	for (int i = 0; i < vertexNum; ++i) {
		diffused2TexturedVerices.emplace_back(diffusevertex[i]);
	}
	for (int i = 0, indexZ = 0; indexZ < length; indexZ++)
	{
		for (int indexX = 0; indexX < width; indexX++, i++)
		{
			texNumber = diffused2TexturedVerices[i].texNumber;
			if (texNumber == indexNum && indexX != width - 1 && indexZ != length - 1) {
				LayerSubIndex.emplace_back(indexX + indexZ * width, indexX + (indexZ * width) + width, indexX + (indexZ * width) + 1);
				LayerSubIndex.emplace_back(indexX + indexZ * width + width, indexX + indexZ * width + width + 1, indexX + indexZ * width + 1);
			}
		}
	}

	for (int i = 0; i < diffused2TexturedVerices.size(); ++i) {
		mappedDiffued2TextureVerices[i] = diffused2TexturedVerices[i];
	}

	indicesNum = subIndex.size() * 3;

	for (int i = 0; i < LayerSubIndex.size(); ++i) {
		indices[i * 3 + 0] = LayerSubIndex[i].GetIndex()[0];
		indices[i * 3 + 1] = LayerSubIndex[i].GetIndex()[1];
		indices[i * 3 + 2] = LayerSubIndex[i].GetIndex()[2];
	}
}


void HeightMapLayer::Render(ID3D12GraphicsCommandList * commandList)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);
	commandList->IASetVertexBuffers(slot, 1, &vertexBufferView);
	if (indexBuffer)
	{
		commandList->IASetIndexBuffer(&indexBufferView);
		commandList->DrawIndexedInstanced(indicesNum, 1, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(diffused2TexturedVerices.size(), 1, offset, 0);
	}
}

float HeightMapLayer::OnGetHeight(int x, int z, void *context)
{
	HeightMapImage *heightMapImage = (HeightMapImage *)context;
	BYTE *heightMapPixels = heightMapImage->GetHeightMapPixels();
	XMFLOAT3 scale = heightMapImage->GetScale();
	int width = heightMapImage->GetHeightMapWidth();

	float height = heightMapPixels[x + (z*width)] * scale.y;
	return(height);
}

void HeightMapLayer::ReleaseBuffers()
{
	if(vertexBuffer)
	vertexBuffer->Release();
	if(indexBuffer)
	indexBuffer->Release();
}

XMFLOAT4 HeightMapLayer::OnGetColor(int x, int z, void *context)
{
	XMFLOAT3 lightDirection = XMFLOAT3(-1.0f, 1.0f, 1.0f);

	lightDirection = Vector3::Normalize(lightDirection);
	HeightMapImage *heightMapImage = (HeightMapImage *)context;
	XMFLOAT3 scale = heightMapImage->GetScale();
	XMFLOAT4 incidentLightColor(0.9f, 0.8f, 0.4f, 1.0f);

	float lightScale = Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x, z), lightDirection);
	lightScale += Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x + 1, z), lightDirection);
	lightScale += Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x + 1, z + 1), lightDirection);
	lightScale += Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x, z + 1), lightDirection);
	lightScale = (lightScale / 4.0f) + 0.05f;
	if (lightScale > 1.0f) lightScale = 1.0f;
	if (lightScale < 0.25f) lightScale = 0.25f;

	XMFLOAT4 color = Vector4::Multiply(lightScale, incidentLightColor);
	return(color);
}




HeightMapImage::HeightMapImage(LPCTSTR fileName, int width, int length, XMFLOAT3 scale)
{
	this->width = width;
	this->length = length;
	this->scale = scale;

	BYTE* heightMapPixels = new BYTE[this->width * this->length];
	// 파일을 열고 읽는다. 높이 맵 이미지는 파일 헤더가 없는 RAW 이미지다.
	HANDLE file = ::CreateFile(fileName, GENERIC_READ, 0, NULL, OPEN_EXISTING, FILE_ATTRIBUTE_NORMAL | FILE_ATTRIBUTE_READONLY, NULL);
	DWORD bytesRead;
	::ReadFile(file, heightMapPixels, (width * length), &bytesRead, NULL);
	::CloseHandle(file);
	// 이미지의 y축과 지형의 z축이 방향이 반대이므로 이미지를 상하대칭 시켜 저장한다.

	this->heightMapPixels = new BYTE[this->width * this->length];
	for (int y = 0; y < this->length; ++y)
	{
		for (int x = 0; x < this->width; ++x)
		{
			this->heightMapPixels[x + ((this->length - 1 - y) * this->width)] = heightMapPixels[x + y * this->width];
		}
	}
	if (heightMapPixels) delete[] heightMapPixels;
}

HeightMapImage::~HeightMapImage()
{
	if (heightMapPixels) delete[] heightMapPixels;
	heightMapPixels = NULL;
}

XMFLOAT3 HeightMapImage::GetHeightMapNormal(int x, int z)
{
	if ((x < 0.0f) || (z < 0.0f) || (x >= width) || (z >= length))
		return(XMFLOAT3(0.0f, 1.0f, 0.0f));

	int nHeightMapIndex = x + (z * width);
	int xHeightMapAdd = (x < (width - 1)) ? 1 : -1;
	int zHeightMapAdd = (z < (length - 1)) ? width : -width;

	float y1 = (float)heightMapPixels[nHeightMapIndex] * scale.y;
	float y2 = (float)heightMapPixels[nHeightMapIndex + xHeightMapAdd] * scale.y;
	float y3 = (float)heightMapPixels[nHeightMapIndex + zHeightMapAdd] * scale.y;

	XMFLOAT3 edge1 = XMFLOAT3(0.0f, y3 - y1, scale.z);

	XMFLOAT3 edge2 = XMFLOAT3(scale.x, y2 - y1, 0.0f);

	XMFLOAT3 normal = Vector3::CrossProduct(edge1, edge2, true);

	return(normal);
}

float HeightMapImage::GetHeight(float fx, float fz)
{
	fx = fx + (width) / 2;
	fz = fz + (length) / 2;

	/*지형의 좌표 (fx, fz)는 이미지 좌표계이다. 높이 맵의 x-좌표와 z-좌표가 높이 맵의 범위를 벗어나면 지형의 높이는 0이다.*/

	if ((fx < 0.0f) || (fz < 0.0f) || (fx >= width) || (fz >= length)) return(0.0f);
	//높이 맵의 좌표의 정수 부분과 소수 부분을 계산한다.
	int x = (int)fx;
	int z = (int)fz;
	float xPercent = fx - x;
	float zPercent = fz - z;
	float bottomLeft = (float)heightMapPixels[x + (z*width)];
	float bottomRight = (float)heightMapPixels[(x + 1) + (z*width)];
	float topLeft = (float)heightMapPixels[x + ((z + 1)*width)];
	float topRight = (float)heightMapPixels[(x + 1) + ((z + 1)*width)];

#ifdef _WITH_APPROXIMATE_OPPOSITE_CORNER
	//z-좌표가 1, 3, 5, ...인 경우 인덱스가 오른쪽에서 왼쪽으로 나열된다.

	bool bRightToLeft = ((z % 2) != 0);
	if (bRightToLeft)
	{
		/*지형의 삼각형들이 오른쪽에서 왼쪽 방향으로 나열되는 경우이다. 다음 그림의 오른쪽은 (fzPercent < fxPercent)
		인 경우이다. 이 경우 TopLeft의 픽셀 값은 (fTopLeft = fTopRight + (fBottomLeft - fBottomRight))로 근사한다.
		다음 그림의 왼쪽은 (fzPercent ≥ fxPercent)인 경우이다. 이 경우 BottomRight의 픽셀 값은 (fBottomRight =
		fBottomLeft + (fTopRight - fTopLeft))로 근사한다.*/

		if (zPercent >= xPercent)
			bottomRight = bottomLeft + (topRight - topLeft);
		else
			topLeft = topRight + (bottomLeft - bottomRight);
	}
	else
	{
		/*지형의 삼각형들이 왼쪽에서 오른쪽 방향으로 나열되는 경우이다. 다음 그림의 왼쪽은 (fzPercent < (1.0f -
		fxPercent))인 경우이다. 이 경우 TopRight의 픽셀 값은 (fTopRight = fTopLeft + (fBottomRight - fBottomLeft))로
		근사한다. 다음 그림의 오른쪽은 (fzPercent ≥ (1.0f - fxPercent))인 경우이다. 이 경우 BottomLeft의 픽셀 값은
		(fBottomLeft = fTopLeft + (fBottomRight - fTopRight))로 근사한다.*/
		if (zPercent < (1.0f - xPercent))
			topRight = topLeft + (bottomRight - bottomLeft);
		else
			bottomLeft = topLeft + (bottomRight - topRight);
	}
#endif
	//사각형의 네 점을 보간하여 높이(픽셀 값)를 계산한다.
	float topHeight = topLeft * (1 - xPercent) + topRight * xPercent;
	float bottomHeight = bottomLeft * (1 - xPercent) + bottomRight * xPercent;
	float height = bottomHeight * (1 - zPercent) + topHeight * zPercent;
	return(height);
}
