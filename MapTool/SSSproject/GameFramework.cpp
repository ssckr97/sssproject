#include "stdafx.h"
#include "GameFramework.h"


static int const  NUM_FRAMES_IN_FLIGHT = 3;

GameFramework::GameFramework()
{
	factory = NULL;
	swapChain = NULL;
	device = NULL;
	commandAllocator = NULL;
	commandQueue = NULL;
	pipelineState = NULL;
	commandList = NULL;
	for (int i = 0; i < renderTargetBuffersNum; i++)
	{
		renderTargetBuffers[i] = NULL;
		fenceValues[i] = 0;
	}
	rtvDescriptorHeap = NULL;
	rtvDescriptorIncrementSize = 0;
	depthStencilBuffer = NULL;
	dsvDescriptorHeap = NULL;
	dsvDescriptorIncrementSize = 0;
	swapChainBufferIndex = 0;

	fenceEvent = NULL;
	fence = NULL;

	windowClientWidth = FRAME_BUFFER_WIDTH;
	windowClientHeight = FRAME_BUFFER_HEIGHT;

	_tcscpy_s(frameRate, _T("SSS Project MapTool ("));

	scene = NULL;
}

GameFramework::~GameFramework()
{
}

//다음 함수는 응용 프로그램이 실행되어 주 윈도우가 생성되면 호출된다는 것에 유의하라.
bool GameFramework::OnCreate(HINSTANCE hinstance, HWND hwnd)
{
	this->hinstance = hinstance;
	this->hwnd = hwnd;

	CreateDirect3DDevice();
	CreateCommandQueueAndList();
	CreateSwapChain();
	CreateRtvAndDsvDescriptorHeaps();
	//Direct3D 디바이스, 명령 큐와 명령 리스트, 스왑 체인 등을 생성하는 함수를 호출한다.
	CreateRenderTargetViews();
	CreateDepthStencilView();
	CreateDetphRtvSrvView();

	// 랜더타겟, 뎁스스텐실의 뷰를 생성한다.
	BuildObjects();
	//렌더링할 객체(게임 월드 객체)를 생성한다.
	BuildGUI();
	//렌더링할 GUI 객체들을 생성한다. 
	BuildPicking();

	processinput = new ProcessInput(scene, guiNum, gui, optiongui, objectgui, terraingui,boundinggui ,player, picking, camera, windowClientWidth, windowClientHeight);
	//사용자 입력에 대한 처리를 한다.
	optiongui->SetCameraSpeed(&processinput->GetCameraSpeed());
	renderer = new Renderer(scene, camera, terraingui ,depthRtvCPUDescriptorHandle, alphaRtvCPUDescriptorHandle0, dsvCPUDescriptorHandle , heightRtvCPUDescriptorHandle, terraindsvCPUDescriptorHandle , alphaRtvCPUDescriptorHandle1 , normalRtvCPUDescriptorHandle , alphaRtvCPUDescriptorHandle2);
	//렌더링에 대한 처리를 한다.
	return(true);
}
void GameFramework::OnDestroy()
{
	ReleaseShaderVariables();
	ReleaseObjects();
	::CloseHandle(fenceEvent);

#if defined(_DEBUG)
	if (debugController) debugController->Release();
#endif

	for (int i = 0; i < renderTargetBuffersNum; i++)
	if (renderTargetBuffers[i]) renderTargetBuffers[i]->Release();
	if (rtvDescriptorHeap) rtvDescriptorHeap->Release();
	if (dsvDescriptorHeap) dsvDescriptorHeap->Release();
	if (commandAllocator) commandAllocator->Release();
	if (commandQueue) commandQueue->Release();
	if (commandList) commandList->Release();
	if (fence) fence->Release();
	swapChain->SetFullscreenState(FALSE, NULL);
	if (swapChain) swapChain->Release();
	if (device) device->Release();
	if (factory) factory->Release();
}

void GameFramework::CreateSwapChain()
{
	RECT rcClient;
	::GetClientRect(hwnd, &rcClient);
	windowClientWidth = rcClient.right - rcClient.left;
	windowClientHeight = rcClient.bottom - rcClient.top;

	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	::ZeroMemory(&swapChainDesc, sizeof(swapChainDesc));
	swapChainDesc.BufferCount = renderTargetBuffersNum;
	swapChainDesc.BufferDesc.Width = windowClientWidth;
	swapChainDesc.BufferDesc.Height = windowClientHeight;
	swapChainDesc.BufferDesc.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	swapChainDesc.BufferDesc.RefreshRate.Numerator = 60;
	swapChainDesc.BufferDesc.RefreshRate.Denominator = 1;
	swapChainDesc.BufferUsage = DXGI_USAGE_RENDER_TARGET_OUTPUT;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;
	swapChainDesc.OutputWindow = hwnd;
	swapChainDesc.SampleDesc.Count = (msaa4xEnable) ? 4 : 1;
	swapChainDesc.SampleDesc.Quality = (msaa4xEnable) ? (msaa4xQualityLevels - 1) : 0;
	swapChainDesc.Windowed = TRUE;
	swapChainDesc.Flags = DXGI_SWAP_CHAIN_FLAG_ALLOW_MODE_SWITCH;
	swapChainDesc.SwapEffect = DXGI_SWAP_EFFECT_FLIP_DISCARD;;

	//전체화면 모드에서 바탕화면의 해상도를 스왑체인(후면버퍼)의 크기에 맞게 변경한다.

	HRESULT hresult = factory->CreateSwapChain(commandQueue, &swapChainDesc, (IDXGISwapChain **)&swapChain);
	swapChainBufferIndex = swapChain->GetCurrentBackBufferIndex();
	hresult = factory->MakeWindowAssociation(hwnd, DXGI_MWA_NO_ALT_ENTER);

	//#ifndef _WITH_SWAPCHAIN_FULLSCREEN_STATE
	//	CreateRenderTargetViews();
	//#endif
}

void GameFramework::CreateDirect3DDevice()
{
#if defined(_DEBUG)
	D3D12GetDebugInterface(__uuidof(ID3D12Debug), (void **)&debugController);
	debugController->EnableDebugLayer();
#endif

	::CreateDXGIFactory1(__uuidof(IDXGIFactory4), (void **)&factory);
	//DXGI 팩토리를 생성한다.

	IDXGIAdapter1* adapter = NULL;
	for (UINT i = 0; DXGI_ERROR_NOT_FOUND != factory->EnumAdapters1(i,
		&adapter); i++)
	{
		DXGI_ADAPTER_DESC1 adapterDesc;
		adapter->GetDesc1(&adapterDesc);
		if (adapterDesc.Flags & DXGI_ADAPTER_FLAG_SOFTWARE) continue;
		if (SUCCEEDED(D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_12_0, _uuidof(ID3D12Device), (void **)&device))) break;
	}
	//모든 하드웨어 어댑터 대하여 특성 레벨 12.0을 지원하는 하드웨어 디바이스를 생성한다.

	if (!adapter)
	{
		factory->EnumWarpAdapter(_uuidof(IDXGIFactory4), (void **)&adapter);
		D3D12CreateDevice(adapter, D3D_FEATURE_LEVEL_11_1, _uuidof(ID3D12Device), (void**)&device);
	}
	//특성 레벨 12.0을 지원하는 하드웨어 디바이스를 생성할 수 없으면 WARP 디바이스를 생성한다.

	D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS msaaQualityLevels;
	msaaQualityLevels.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	msaaQualityLevels.SampleCount = 4; //Msaa4x 다중 샘플링
	msaaQualityLevels.Flags = D3D12_MULTISAMPLE_QUALITY_LEVELS_FLAG_NONE;
	msaaQualityLevels.NumQualityLevels = 0;
	device->CheckFeatureSupport(D3D12_FEATURE_MULTISAMPLE_QUALITY_LEVELS, &msaaQualityLevels, sizeof(D3D12_FEATURE_DATA_MULTISAMPLE_QUALITY_LEVELS));
	msaa4xQualityLevels = msaaQualityLevels.NumQualityLevels;
	//디바이스가 지원하는 다중 샘플의 품질 수준을 확인한다.
	msaa4xEnable = (msaa4xQualityLevels > 1) ? true : false;
	//다중 샘플의 품질 수준이 1보다 크면 다중 샘플링을 활성화한다.

	device->CreateFence(0, D3D12_FENCE_FLAG_NONE, __uuidof(ID3D12Fence), (void**)&fence);
	for (int i = 0; i < renderTargetBuffersNum; i++)
	{
		fenceValues[i] = 0;
	}
	//펜스를 생성하고 펜스 값을 0으로 설정한다.

	fenceEvent = ::CreateEvent(NULL, FALSE, FALSE, NULL);
	//펜스와 동기화를 위한 이벤트 객체를 생성한다(이벤트 객체의 초기값을 FALSE이다). 이벤트가 실행되면(Signal) 이벤트의 값을 자동적으로 FALSE가 되도록 생성한다.

	::gnCbvSrvDescriptorIncrementSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV);

	if (adapter) adapter->Release();
}

void GameFramework::CreateCommandQueueAndList()
{
	D3D12_COMMAND_QUEUE_DESC commandQueueDesc;
	::ZeroMemory(&commandQueueDesc, sizeof(D3D12_COMMAND_QUEUE_DESC));
	commandQueueDesc.Flags = D3D12_COMMAND_QUEUE_FLAG_NONE;
	commandQueueDesc.Type = D3D12_COMMAND_LIST_TYPE_DIRECT;

	HRESULT hresult = device->CreateCommandQueue(&commandQueueDesc, _uuidof(ID3D12CommandQueue), (void **)&commandQueue);
	//직접(Direct) 명령 큐를 생성한다.

	hresult = device->CreateCommandAllocator(D3D12_COMMAND_LIST_TYPE_DIRECT, __uuidof(ID3D12CommandAllocator), (void **)&commandAllocator);
	//직접(Direct) 명령 할당자를 생성한다.

	hresult = device->CreateCommandList(0, D3D12_COMMAND_LIST_TYPE_DIRECT, commandAllocator, NULL, __uuidof(ID3D12GraphicsCommandList), (void**)&commandList);
	//직접(Direct) 명령 리스트를 생성한다.

	hresult = commandList->Close();
	//명령 리스트는 생성되면 열린(Open) 상태이므로 닫힌(Closed) 상태로 만든다.
}

void GameFramework::CreateRtvAndDsvDescriptorHeaps()
{
	D3D12_DESCRIPTOR_HEAP_DESC descriptorHeapDesc;
	::ZeroMemory(&descriptorHeapDesc, sizeof(D3D12_DESCRIPTOR_HEAP_DESC));
	descriptorHeapDesc.NumDescriptors = renderTargetBuffersNum + 6; // depthTexture , alphaTexture0 , heightmapTexture , alphaTexture1 , terrainnormalTexture , alphaTexture2 
	descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_RTV;
	descriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	descriptorHeapDesc.NodeMask = 0;

	HRESULT hresult = device->CreateDescriptorHeap(&descriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&rtvDescriptorHeap);
	//렌더 타겟 서술자 힙(서술자의 개수는 스왑체인 버퍼의 개수)을 생성한다.

	rtvDescriptorIncrementSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_RTV);
	//렌더 타겟 서술자 힙의 원소의 크기를 저장한다.

	descriptorHeapDesc.NumDescriptors = 2;
	descriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_DSV;
	hresult = device->CreateDescriptorHeap(&descriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&dsvDescriptorHeap);
	//깊이-스텐실 서술자 힙(서술자의 개수는 1)을 생성한다.

	dsvDescriptorIncrementSize = device->GetDescriptorHandleIncrementSize(D3D12_DESCRIPTOR_HEAP_TYPE_DSV);
	//깊이-스텐실 서술자 힙의 원소의 크기를 저장한다.
}

//스왑체인의 각 후면 버퍼에 대한 렌더 타겟 뷰를 생성한다.
void GameFramework::CreateRenderTargetViews()
{
	POINT terrainSize{ 257.0f , 257.0f };
	terrainScale = { 8.0f , 2.0f , 8.0f };
	D3D12_CLEAR_VALUE clearValue;

	FLOAT clear[4]{ 1.0f , 1.0f , 1.0f , 1.0f };
	clearValue.Format = DXGI_FORMAT_R32G32B32A32_FLOAT;

	depthRenderResource = ::CreateTexture2DResource(device, commandList, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET, D3D12_RESOURCE_STATE_GENERIC_READ, &clearValue);
	D3D12_CPU_DESCRIPTOR_HANDLE rtvCPUDescriptorHandle = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	terrainAlphaResource0 = CreateTexture2DResource(device, commandList, 256.0f, 256.0f, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET, D3D12_RESOURCE_STATE_GENERIC_READ, &clearValue);
	terrainAlphaResource1 = CreateTexture2DResource(device, commandList, 256.0f, 256.0f, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET, D3D12_RESOURCE_STATE_GENERIC_READ, &clearValue);
	terrainAlphaResource2 = CreateTexture2DResource(device, commandList, 256.0f, 256.0f, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET, D3D12_RESOURCE_STATE_GENERIC_READ, &clearValue);
	terrainHeightResource = CreateTexture2DResource(device, commandList, 256.0f, 256.0f, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET, D3D12_RESOURCE_STATE_GENERIC_READ, &clearValue);
	terrainNormalResource = CreateTexture2DResource(device, commandList , 256.0f, 256.0f, 1, 1, DXGI_FORMAT_R32G32B32A32_FLOAT, D3D12_RESOURCE_FLAG_ALLOW_RENDER_TARGET, D3D12_RESOURCE_STATE_GENERIC_READ, &clearValue);

	for (UINT i = 0; i < renderTargetBuffersNum; i++)
	{
		HRESULT hresult = swapChain->GetBuffer(i, __uuidof(ID3D12Resource), (void**)&renderTargetBuffers[i]);
		device->CreateRenderTargetView(renderTargetBuffers[i], NULL, rtvCPUDescriptorHandle);
		rtvCPUDescriptorHandle.ptr += rtvDescriptorIncrementSize;
	}
	device->CreateRenderTargetView(depthRenderResource, NULL, rtvCPUDescriptorHandle);
	rtvCPUDescriptorHandle.ptr += rtvDescriptorIncrementSize;
	device->CreateRenderTargetView(terrainAlphaResource0, NULL, rtvCPUDescriptorHandle);
	rtvCPUDescriptorHandle.ptr += rtvDescriptorIncrementSize;
	device->CreateRenderTargetView(terrainHeightResource, NULL, rtvCPUDescriptorHandle);
	rtvCPUDescriptorHandle.ptr += rtvDescriptorIncrementSize;
	device->CreateRenderTargetView(terrainAlphaResource1, NULL, rtvCPUDescriptorHandle);
	rtvCPUDescriptorHandle.ptr += rtvDescriptorIncrementSize;
	device->CreateRenderTargetView(terrainNormalResource, NULL, rtvCPUDescriptorHandle);
	rtvCPUDescriptorHandle.ptr += rtvDescriptorIncrementSize;
	device->CreateRenderTargetView(terrainAlphaResource2, NULL, rtvCPUDescriptorHandle);

	depthRtvCPUDescriptorHandle = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	depthRtvCPUDescriptorHandle.ptr += (renderTargetBuffersNum * rtvDescriptorIncrementSize);

	alphaRtvCPUDescriptorHandle0 = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	alphaRtvCPUDescriptorHandle0.ptr += ((renderTargetBuffersNum + 1) * rtvDescriptorIncrementSize);

	heightRtvCPUDescriptorHandle = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	heightRtvCPUDescriptorHandle.ptr += ((renderTargetBuffersNum + 2) * rtvDescriptorIncrementSize);

	alphaRtvCPUDescriptorHandle1 = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	alphaRtvCPUDescriptorHandle1.ptr += ((renderTargetBuffersNum + 3) * rtvDescriptorIncrementSize);

	normalRtvCPUDescriptorHandle = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	normalRtvCPUDescriptorHandle.ptr += ((renderTargetBuffersNum + 4) * rtvDescriptorIncrementSize);

	alphaRtvCPUDescriptorHandle2 = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	alphaRtvCPUDescriptorHandle2.ptr += ((renderTargetBuffersNum + 5) * rtvDescriptorIncrementSize);

}
void GameFramework::CreateDepthStencilView()
{
	D3D12_RESOURCE_DESC resourceDesc;
	resourceDesc.Dimension = D3D12_RESOURCE_DIMENSION_TEXTURE2D;
	resourceDesc.Alignment = 0;
	resourceDesc.Width = windowClientWidth;
	resourceDesc.Height = windowClientHeight;
	resourceDesc.DepthOrArraySize = 1;
	resourceDesc.MipLevels = 1;
	resourceDesc.Format = DXGI_FORMAT_R32G8X24_TYPELESS;
	resourceDesc.SampleDesc.Count = (msaa4xEnable) ? 4 : 1;
	resourceDesc.SampleDesc.Quality = (msaa4xEnable) ? (msaa4xQualityLevels - 1) : 0;
	resourceDesc.Layout = D3D12_TEXTURE_LAYOUT_UNKNOWN;
	resourceDesc.Flags = D3D12_RESOURCE_FLAG_ALLOW_DEPTH_STENCIL;

	D3D12_HEAP_PROPERTIES heapProperties;
	::ZeroMemory(&heapProperties, sizeof(D3D12_HEAP_PROPERTIES));
	heapProperties.Type = D3D12_HEAP_TYPE_DEFAULT;
	heapProperties.CPUPageProperty = D3D12_CPU_PAGE_PROPERTY_UNKNOWN;
	heapProperties.MemoryPoolPreference = D3D12_MEMORY_POOL_UNKNOWN;
	heapProperties.CreationNodeMask = 1;
	heapProperties.VisibleNodeMask = 1;

	D3D12_CLEAR_VALUE clearValue;
	clearValue.Format = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	clearValue.DepthStencil.Depth = 1.0f;
	clearValue.DepthStencil.Stencil = 0;
	device->CreateCommittedResource(&heapProperties, D3D12_HEAP_FLAG_NONE, &resourceDesc, D3D12_RESOURCE_STATE_DEPTH_WRITE, &clearValue,
		__uuidof(ID3D12Resource), (void **)&depthStencilBuffer);
	//깊이-스텐실 버퍼를 생성한다.

	resourceDesc.Width = 256.0f * 8.0f;
	resourceDesc.Height = 256.0f * 8.0f;

	clearValue.DepthStencil.Stencil = 0;
	device->CreateCommittedResource(&heapProperties, D3D12_HEAP_FLAG_NONE, &resourceDesc, D3D12_RESOURCE_STATE_DEPTH_WRITE, &clearValue,
		__uuidof(ID3D12Resource), (void **)&terrainDepthStencilBuffer);

	D3D12_DEPTH_STENCIL_VIEW_DESC depthstencilviewdesc = {};
	depthstencilviewdesc.Format = DXGI_FORMAT_D32_FLOAT_S8X24_UINT; 	// From the example in the book of Frank-Luna, is the stencil-buffer really necessary? 
	depthstencilviewdesc.ViewDimension = D3D12_DSV_DIMENSION_TEXTURE2D;
	depthstencilviewdesc.Flags = D3D12_DSV_FLAG_NONE;
	depthstencilviewdesc.Texture2D.MipSlice = 0;
	dsvCPUDescriptorHandle = dsvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	device->CreateDepthStencilView(depthStencilBuffer, &depthstencilviewdesc, dsvCPUDescriptorHandle);
	terraindsvCPUDescriptorHandle.ptr = dsvDescriptorHeap->GetCPUDescriptorHandleForHeapStart().ptr + dsvDescriptorIncrementSize;

	device->CreateDepthStencilView(terrainDepthStencilBuffer, &depthstencilviewdesc, terraindsvCPUDescriptorHandle);
	//깊이-스텐실 버퍼 뷰를 생성한다.
	//뎁스값을 렌더링할 버퍼를 만든다. 

}

void GameFramework::CreateDetphRtvSrvView()
{
	int nConstantBufferViews = 0;
	D3D12_DESCRIPTOR_HEAP_DESC d3dDescriptorHeapDesc;
	d3dDescriptorHeapDesc.NumDescriptors = 1; //CBVs + SRVs 
	d3dDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	d3dDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_NONE;
	d3dDescriptorHeapDesc.NodeMask = 0;
	device->CreateDescriptorHeap(&d3dDescriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&srvdepthbufferHeap);

	cbvCPUDescriptorStartHandle = srvdepthbufferHeap->GetCPUDescriptorHandleForHeapStart();
	cbvGPUDescriptorStartHandle = srvdepthbufferHeap->GetGPUDescriptorHandleForHeapStart();
	srvCPUDescriptorStartHandle.ptr = cbvCPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nConstantBufferViews);
	srvGPUDescriptorStartHandle.ptr = cbvGPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nConstantBufferViews);

	

}

void GameFramework::ChangeSwapChainState()
{
	WaitForGpuComplete();
	BOOL fullScreenState = FALSE;
	swapChain->GetFullscreenState(&fullScreenState, NULL);
	swapChain->SetFullscreenState(!fullScreenState, NULL);
	DXGI_MODE_DESC targetParameters;
	targetParameters.Format = DXGI_FORMAT_R8G8B8A8_UNORM;
	targetParameters.Width = windowClientWidth;
	targetParameters.Height = windowClientHeight;
	targetParameters.RefreshRate.Numerator = 60;
	targetParameters.RefreshRate.Denominator = 1;
	targetParameters.Scaling = DXGI_MODE_SCALING_UNSPECIFIED;
	targetParameters.ScanlineOrdering = DXGI_MODE_SCANLINE_ORDER_UNSPECIFIED;
	swapChain->ResizeTarget(&targetParameters);

	for (int i = 0; i < renderTargetBuffersNum; i++)
		if (renderTargetBuffers[i]) renderTargetBuffers[i]->Release();
	DXGI_SWAP_CHAIN_DESC swapChainDesc;
	swapChain->GetDesc(&swapChainDesc);
	swapChain->ResizeBuffers(renderTargetBuffersNum, windowClientWidth, windowClientHeight, swapChainDesc.BufferDesc.Format, swapChainDesc.Flags);

	swapChainBufferIndex = swapChain->GetCurrentBackBufferIndex();

	CreateRenderTargetViews();
}

void GameFramework::BuildObjects()
{
	commandList->Reset(commandAllocator, NULL);

	scene = new Scene(depthRenderResource, terrainAlphaResource0, terrainScale , terrainHeightResource , terrainAlphaResource1 , terrainAlphaResource2, depthStencilBuffer);
	scene->BuildObjects(device, commandList);
	//씬 객체를 생성하고 씬에 포함될 게임 객체들을 생성한다.

	AirplanePlayer *airplanePlayer = new AirplanePlayer(device, commandList, scene->GetGraphicsRootSignature());
	scene->player = player = airplanePlayer;
	camera = player->GetCamera();

	commandList->Close();
	ID3D12CommandList *commandLists[] = { commandList };
	commandQueue->ExecuteCommandLists(1, commandLists);
	//씬 객체를 생성하기 위하여 필요한 그래픽 명령 리스트들을 명령 큐에 추가한다.

	WaitForGpuComplete();
	//그래픽 명령 리스트들이 모두 실행될 때까지 기다린다.

	if (scene) scene->ReleaseUploadBuffers();
	if (player) player->ReleaseUploadBuffers();

	//그래픽 리소스들을 생성하는 과정에 생성된 업로드 버퍼들을 소멸시킨다.

	gameTimer.Reset();
}

void GameFramework::ReleaseObjects()
{
	if (player) delete player;
	if (scene) scene->ReleaseObjects();
	if (scene) delete scene;
	if (processinput) delete processinput;
	if (renderer) delete renderer;
	if (gui) {
		for (int i = 0; i < guiNum; ++i) {
			delete gui[i];
		}
		delete[] gui;

	}
	if (mangergui) delete mangergui;
	if (picking) delete picking;
}
void GameFramework::BuildGUI()
{
	gui = new GUI*[6];
	gui[OBJECTGUI] = new ObjectGUI;
	gui[TERRAINGUI] = new TerrainGUI(scene->GetShaders()[SCREENSPACEDECALSHADER]->GetObjects(0), scene->GetTerrain(), scene);
	gui[LIGHTGUI] = new LightGUI(scene);
	gui[PARTICLEGUI] = new ParticleGUI(scene ,device, commandList);
	gui[OPTIONGUI] = new OptionGUI(scene, device, commandList , commandQueue , terrainAlphaResource0 , terrainAlphaResource1 ,
		terrainAlphaResource2,	terrainHeightResource , terrainNormalResource,(LightGUI*)gui[LIGHTGUI] , (ParticleGUI*)gui[PARTICLEGUI]);
	gui[BOUNDINGGUI] = new BoundingGUI(&cbFlag , &dbFlag);


	this->mangergui = new MangerGUI;
	this->optiongui = static_cast<OptionGUI*>(gui[OPTIONGUI]);
	this->objectgui = static_cast<ObjectGUI*>(gui[OBJECTGUI]);
	this->terraingui = static_cast<TerrainGUI*>(gui[TERRAINGUI]);
	this->boundinggui = static_cast<BoundingGUI*>(gui[BOUNDINGGUI]);

	IMGUI_CHECKVERSION();
	ImGui::CreateContext();
	ImGuiIO& io = ImGui::GetIO(); (void)io;
	io.ConfigFlags |= ImGuiConfigFlags_NavEnableKeyboard;       // Enable Keyboard Controls
  //io.ConfigFlags |= ImGuiConfigFlags_NavEnableGamepad;      // Enable Gamepad Controls
	io.ConfigFlags |= ImGuiConfigFlags_DockingEnable;           // Enable Docking
	io.ConfigFlags |= ImGuiConfigFlags_ViewportsEnable;
	// Setup Dear ImGui style
	ImGui::StyleColorsDark();
	//ImGui::StyleColorsClassic();
	ImGuiStyle& style = ImGui::GetStyle();
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		style.WindowRounding = 0.0f;
		style.Colors[ImGuiCol_WindowBg].w = 1.0f;
	}

	D3D12_DESCRIPTOR_HEAP_DESC desc = {};
	desc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	desc.NumDescriptors = 1;
	desc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	device->CreateDescriptorHeap(&desc, IID_PPV_ARGS(&SrvDescHeap));
	// Setup Platform/Renderer bindings
	ImGui_ImplWin32_Init(hwnd);
	ImGui_ImplDX12_Init(device, NUM_FRAMES_IN_FLIGHT,
		DXGI_FORMAT_R8G8B8A8_UNORM, SrvDescHeap,
		SrvDescHeap->GetCPUDescriptorHandleForHeapStart(),
		SrvDescHeap->GetGPUDescriptorHandleForHeapStart());

}
void GameFramework::BuildPicking()
{
	picking = new Picking;
}

void GameFramework::GarbageCollection()
{
	if (scene)
		scene->GarbageCollection();
}

void GameFramework::UpdateShaderVariables()
{
	float total = gameTimer.GetTotalTime();
	float elapsed = gameTimer.GetTimeElapsed();

	commandList->SetGraphicsRoot32BitConstants(GraphicsRootTimeInfo, 1, &total, 0);
	commandList->SetGraphicsRoot32BitConstants(GraphicsRootTimeInfo, 1, &elapsed, 1);

	commandList->SetComputeRoot32BitConstants(ComputeRootTimeInfo, 1, &total, 0);
	commandList->SetComputeRoot32BitConstants(ComputeRootTimeInfo, 1, &elapsed, 1);

}

void GameFramework::ReleaseShaderVariables()
{
	if (scene) scene->ReleaseShaderVariables();
	if (player) player->ReleaseShaderVariables();
}

void GameFramework::OnProcessingMouseMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{
	switch (messageID)
	{
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
		//마우스 캡쳐를 하고 현재 마우스 위치를 가져온다.
		::SetCapture(hwnd);
		POINT tempCursor;
		::GetCursorPos(&tempCursor);
		::GetCursorPos(&tempCursor);
		processinput->SetOldCursorPos(tempCursor);
		processinput->SetPickCursorPos(tempCursor);
		processinput->SetButtonDown(true);

		break;
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
		//마우스 캡쳐를 해제한다.
		::ReleaseCapture();
		picking->SetSelectObject(NULL);
		picking->Setpicking(true);
		processinput->SetButtonDown(false);
		break;
	}
}
void GameFramework::OnProcessingKeyboardMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{
	switch (messageID)
	{
	case WM_KEYUP:
		switch (wparam)
		{
			/*‘F1’ 키를 누르면 1인칭 카메라, ‘F2’ 키를 누르면 스페이스-쉽 카메라로 변경한다, ‘F3’ 키를 누르면 3인칭 카메라
			로 변경한다.*/
		case VK_F1:
		case VK_F2:
		case VK_F3:
			if (player)
			{
				camera = player->ChangeCamera(DWORD(wparam - VK_F1 + 1), gameTimer.GetTimeElapsed());
			}
			break;
		case VK_ESCAPE:
			::PostQuitMessage(0);
			break;
		case VK_RETURN:
			break;
			//“F9” 키가 눌려지면 윈도우 모드와 전체화면 모드의 전환을 처리한다.
		case VK_F9:
			ChangeSwapChainState();
		}
		break;
	default:
		break;
	}
}
LRESULT CALLBACK GameFramework::OnProcessingWindowMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{

	switch (messageID)
	{
	case WM_SIZE:
	{
		windowClientWidth = LOWORD(lparam);
		windowClientHeight = HIWORD(lparam);
		break;
	}
	case WM_LBUTTONDOWN:
	case WM_RBUTTONDOWN:
	case WM_LBUTTONUP:
	case WM_RBUTTONUP:
	case WM_MOUSEMOVE:
		OnProcessingMouseMessage(hwnd, messageID, wparam, lparam);
		break;
	case WM_KEYDOWN:
	case WM_KEYUP:
		OnProcessingKeyboardMessage(hwnd, messageID, wparam, lparam);
		break;
	}
	return(0);
}
void GameFramework::ProcessUserInput()
{
	bool dummyFlag = 0;


	switch (mangergui->GetinputGuiType()) {
	case OPTIONGUI:
		bFlag = true;
		processinput->ProcessBassicCameraKeyboardInput(hwnd, gameTimer); 
		break;
	case OBJECTGUI:
	{
		bFlag = true;
		auto tempFlag = objectgui->GetNewOrEditFlags();
		if (tempFlag == 0) {
			dummyFlag = true;
			processinput->ProcessObjectNewInput(hwnd, gameTimer, device, commandList , dummyFlag); 
		}
		if (tempFlag == 1) { processinput->ProcessObjectEditInput(device, commandList, hwnd, gameTimer); }
	}
	break;
	case TERRAINGUI:
		bFlag = true;
		processinput->ProcessChangeTerrain(device, commandList, hwnd, gameTimer);
		break;
	case LIGHTGUI:
		bFlag = true;
		processinput->ProcessLightInput(hwnd, gameTimer);
		break;
	case PARTICLEGUI:
		bFlag = true;
		processinput->ProcessParticleInput(device, commandList, hwnd , gameTimer);
		break;
	case BOUNDINGGUI:
		if (bFlag == true) {
			player->SetPosition(XMFLOAT3(0.0f, 0.0f, -50.0f));
			player->SetLook(XMFLOAT3(0.0, 0.0, 1.0f));

			bFlag = false;
		}
		int tempFlag = boundinggui->RadioButtonState();
		
		dummyFlag = true;
		
		processinput->ProcessBoundingInput(device, commandList, hwnd, gameTimer , dummyFlag , fobIdx);
		break;
	}
	if (!dummyFlag) {
		Shader** shaders = scene->GetShaders();
		shaders[INSTANCSHADER]->SetDummyFarAway();
		shaders[SKINEDINSTANCSHADER]->SetDummyFarAway();
	}
	
}
void GameFramework::AnimateObjects()
{
	if (scene) scene->AnimateObjects(gameTimer.GetTimeElapsed(), camera);
}

void GameFramework::WaitForGpuComplete()
{
	UINT64 fenceValue = ++fenceValues[swapChainBufferIndex];
	HRESULT hresult = commandQueue->Signal(fence, fenceValue);
	if (fence->GetCompletedValue() < fenceValue)
	{
		hresult = fence->SetEventOnCompletion(fenceValue, fenceEvent);
		::WaitForSingleObject(fenceEvent, INFINITE);
	}
}

void GameFramework::MoveToNextFrame()
{
	swapChainBufferIndex = swapChain->GetCurrentBackBufferIndex();
	UINT64 fenceValue = ++fenceValues[swapChainBufferIndex];
	HRESULT hresult = commandQueue->Signal(fence, fenceValue);
	if (fence->GetCompletedValue() < fenceValue)
	{
		hresult = fence->SetEventOnCompletion(fenceValue, fenceEvent);
		::WaitForSingleObject(fenceEvent, INFINITE);
	}
}

void GameFramework::FrameAdvance()
{
	gameTimer.Tick(0.0f);

	HRESULT hresult = commandAllocator->Reset();
	hresult = commandList->Reset(commandAllocator, NULL);
	//명령 할당자와 명령 리스트를 리셋한다.

	if (cbFlag) {
		InstancingShader* is = static_cast<InstancingShader*>(scene->GetShaders()[INSTANCSHADER]);
		is->CreateNewBounding(device, commandList , fobIdx);
		cbFlag = false;
	}
	

	ProcessUserInput();
	AnimateObjects();

	if (terraingui->GetcbFlag()) {
		HeightMapTerrain* terrain = scene->GetTerrain();
		float scaleY = terrain->GetScale().y;
		float width = 32.0f;
		int scale = int((514.0f / (width)));
		XMFLOAT3 nscale = XMFLOAT3((float)scale, (float)scaleY, (float)scale);

		InstancingShader* is = static_cast<InstancingShader*>(scene->GetShaders()[INSTANCSHADER]);
		std::vector<InstanceBuffer*> isBuffer = is->GetBuffer();

		std::vector<BoundingOrientedBox> tobbvec;

		for (int i = 0; i < isBuffer.size(); ++i) {
			for (int j = 1; j < isBuffer[i]->objects.size(); ++j) {
				for (int k = 0; k < isBuffer[i]->objects[j]->GetobbVector().size(); ++k) {
					tobbvec.emplace_back(isBuffer[i]->objects[j]->GetobbVector()[k]);
				}

			}

		}
		
		int(*navibuffer)[257] = terrain->GetNaviBuffer();
		for (int i = 0; i < 257; ++i) {
			memset(navibuffer[i], -1, sizeof(int) * 257);
		}
		terrain->Getntvec().clear();

		//terraingui->GetNaviSize() 3번째 4번째 매개변수에 들어가야함
		terrain->GetNaviMesh()[0].CreateNaviMesh(device, commandList, width + 1, width + 1, nscale, tobbvec , terrain->GetInverseWorld() , terrain->GetMesh(0)->GetDiffuseTexturedVertex() , navibuffer, terrain->Getntvec());

		for (int i = 0; i < 3; ++i) {
			width = width * 2;
			scale = int((514.0f / (width)));
			nscale = XMFLOAT3((float)scale, (float)scaleY, (float)scale);

			terrain->GetNaviMesh()[i + 1].ReCreateNaviMesh(device, commandList, width + 1, width + 1, nscale, tobbvec, terrain->GetInverseWorld(), terrain->GetMesh(0)->GetDiffuseTexturedVertex(), terrain->GetNaviMesh()[i], navibuffer, terrain->Getntvec());
		}
		

		terraingui->GetcbFlag() = false;
	}

	D3D12_RESOURCE_BARRIER resourceBarrier;
	::ZeroMemory(&resourceBarrier, sizeof(D3D12_RESOURCE_BARRIER));
	resourceBarrier.Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
	resourceBarrier.Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
	resourceBarrier.Transition.pResource = renderTargetBuffers[swapChainBufferIndex];
	resourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_PRESENT;
	resourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_RENDER_TARGET;
	resourceBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;

	commandList->ResourceBarrier(1, &resourceBarrier);

	D3D12_CPU_DESCRIPTOR_HANDLE rtvCPUDescriptorHandle = rtvDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	rtvCPUDescriptorHandle.ptr += (swapChainBufferIndex * rtvDescriptorIncrementSize);



	//깊이 스텐실 , 알파맵 , 렌더타겟 , 뎁스 맵을 지운다. 
	renderer->ClearRtvDepthAlpha(device, commandList, rtvCPUDescriptorHandle, optiongui->clear_color);
	//지형의 뎁스버퍼를 렌더링한다 .
	renderer->RenderTerrainDepthMap(device, commandList);
	//알파맵을 랜더링 한다.
	renderer->RenderTerrainAlphaTexture(device, commandList);
	// 모든 객체를 그린다.

	//하이트맵을 렌더링 한다.
	renderer->RenderTerrainHeightMap(device, commandList);

	renderer->OnPrepareRender(commandList);
	UpdateShaderVariables();
	if (mangergui->GetinputGuiType() != BOUNDINGGUI) {
		renderer->RenderScene(device, commandList, rtvCPUDescriptorHandle);
	}
	else {
		commandList->OMSetRenderTargets(1, &rtvCPUDescriptorHandle, TRUE, &dsvCPUDescriptorHandle);
		scene->BoundingRender(commandList, camera , boundinggui->GetbIdx());
	}
	//3인칭 카메라일 때 플레이어가 항상 보이도록 렌더링한다.
#ifdef _WITH_PLAYER_TOP
//렌더 타겟은 그대로 두고 깊이 버퍼를 1.0으로 지우고 플레이어를 렌더링하면 플레이어는 무조건 그려질 것이다.
	commandList->ClearDepthStencilView(dsvCPUDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
#endif
	//3인칭 카메라일 때 플레이어를 렌더링한다.
	if (player) player->Render(commandList, camera);

	commandList->SetDescriptorHeaps(1, &SrvDescHeap);
	ImGui_ImplDX12_NewFrame();
	ImGui_ImplWin32_NewFrame();
	ImGui::NewFrame();
	mangergui->Render();
	int selectedIndex = mangergui->GetinputGuiType();
	if (selectedIndex != -1) {
		gui[selectedIndex]->Render();
	}
	ImGui::EndFrame();
	ImGui::Render();
	ImGui_ImplDX12_RenderDrawData(ImGui::GetDrawData(), commandList);

	resourceBarrier.Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
	resourceBarrier.Transition.StateAfter = D3D12_RESOURCE_STATE_PRESENT;
	resourceBarrier.Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
	commandList->ResourceBarrier(1, &resourceBarrier);
	//현재 렌더 타겟에 대한 렌더링이 끝나기를 기다린다. GPU가 렌더 타겟(버퍼)을 더 이상 사용하지 않으면 렌더 타겟의 상태는 프리젠트 상태로 바뀔 것이다.

	hresult = commandList->Close();
	//명령 리스트를 닫힌 상태로 만든다.

	ID3D12CommandList *pCommandLists[] = { commandList };
	commandQueue->ExecuteCommandLists(_countof(pCommandLists), pCommandLists);
	//명령 리스트를 명령 큐에 추가하여 실행한다.
	WaitForGpuComplete();

	GarbageCollection();
	if (dbFlag) {
		InstancingShader* is = static_cast<InstancingShader*>(scene->GetShaders()[INSTANCSHADER]);
		is->DeleteBounding(device, commandList, fobIdx , boundinggui->GetbIdx());
		boundinggui->GetbIdx() = -1;
		dbFlag = false;	
	}

	



	ImGuiIO& io = ImGui::GetIO(); (void)io;
	if (io.ConfigFlags & ImGuiConfigFlags_ViewportsEnable)
	{
		ImGui::UpdatePlatformWindows();
		ImGui::RenderPlatformWindowsDefault(NULL, (void*)commandList);
	}

	swapChain->Present(0, 0);
	MoveToNextFrame();

	gameTimer.GetFrameRate(frameRate + 10, 37);

	size_t nLength = _tcslen(frameRate);
	XMFLOAT3 pos, look;
	if (player) {
		pos = player->GetPosition();
		look = camera->GetLookVector();
	}
	_stprintf_s(frameRate + nLength, 90 - nLength, _T("(%.1f, %.1f, %.1f), LOOK : (%.1f, %.1f, %.1f)"), pos.x, pos.y, pos.z, look.x, look.y, look.z);

	::SetWindowText(hwnd, frameRate);
}




