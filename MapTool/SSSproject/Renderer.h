#pragma once
#include "Scene.h"
#include "Camera.h"
#include "stdafx.h"
#include "GUI.h"
class Renderer
{
private:
	Scene* scene = NULL;
	Camera* camera = NULL;
	TerrainGUI* terrainGUI;

	D3D12_CPU_DESCRIPTOR_HANDLE dsvCPUDescriptorHandle;
	D3D12_CPU_DESCRIPTOR_HANDLE depthRtvCPUDescriptorHandle;
	D3D12_CPU_DESCRIPTOR_HANDLE alphaRtvCPUDescriptorHandle0;
	D3D12_CPU_DESCRIPTOR_HANDLE alphaRtvCPUDescriptorHandle1;
	D3D12_CPU_DESCRIPTOR_HANDLE alphaRtvCPUDescriptorHandle2;
	D3D12_CPU_DESCRIPTOR_HANDLE heightRtvCPUDescriptorHandle;

	D3D12_CPU_DESCRIPTOR_HANDLE terraindsvCPUDescriptorHandle;
	D3D12_CPU_DESCRIPTOR_HANDLE normalRtvCPUDescriptorHandle;


	D3D12_CPU_DESCRIPTOR_HANDLE* alphaTextureArr;
	D3D12_CPU_DESCRIPTOR_HANDLE* NormalTextureArr;


public:
	Renderer();
	Renderer(Scene* scene, Camera* camera,TerrainGUI* terrainGUI, D3D12_CPU_DESCRIPTOR_HANDLE depthRtvCPUDescriptorHandle,
		D3D12_CPU_DESCRIPTOR_HANDLE alphaRtvCPUDescriptorHandle0 , D3D12_CPU_DESCRIPTOR_HANDLE dsvCPUDescriptorHandle ,
		D3D12_CPU_DESCRIPTOR_HANDLE heightRtvCPUDescriptorHandle , D3D12_CPU_DESCRIPTOR_HANDLE terraindsvCPUDescriptorHandle , 
		D3D12_CPU_DESCRIPTOR_HANDLE alphaRtvCPUDescriptorHandle1 , D3D12_CPU_DESCRIPTOR_HANDLE normalRtvCPUDescriptorHandle ,
		D3D12_CPU_DESCRIPTOR_HANDLE alphaRtvCPUDescriptorHandle2 )
		: scene(scene) , camera(camera) ,terrainGUI(terrainGUI) , depthRtvCPUDescriptorHandle(depthRtvCPUDescriptorHandle) , alphaRtvCPUDescriptorHandle0(alphaRtvCPUDescriptorHandle0) , 
		dsvCPUDescriptorHandle(dsvCPUDescriptorHandle) , heightRtvCPUDescriptorHandle(heightRtvCPUDescriptorHandle) ,
		terraindsvCPUDescriptorHandle(terraindsvCPUDescriptorHandle) , alphaRtvCPUDescriptorHandle1(alphaRtvCPUDescriptorHandle1), normalRtvCPUDescriptorHandle(normalRtvCPUDescriptorHandle) , alphaRtvCPUDescriptorHandle2(alphaRtvCPUDescriptorHandle2)
	{
		alphaTextureArr = new D3D12_CPU_DESCRIPTOR_HANDLE[3];
		alphaTextureArr[0] = this->alphaRtvCPUDescriptorHandle0;
		alphaTextureArr[1] = this->alphaRtvCPUDescriptorHandle1;
		alphaTextureArr[2] = this->alphaRtvCPUDescriptorHandle2;

		NormalTextureArr = new D3D12_CPU_DESCRIPTOR_HANDLE[2];
		NormalTextureArr[1] = this->normalRtvCPUDescriptorHandle;

	}
	

	void RenderTerrainAlphaTexture(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void ClearRtvDepthAlpha(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, D3D12_CPU_DESCRIPTOR_HANDLE RtvCPUDescriptorHandle , float* RtvClearColor);
	void RenderTerrainDepthMap(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void RenderTerrainNormalMap(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);

	void RenderTerrainHeightMap(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void OnPrepareRender(ID3D12GraphicsCommandList* commandList);
	void RenderScene(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , D3D12_CPU_DESCRIPTOR_HANDLE rtvCPUDescriptorHandle);
	~Renderer();


};

