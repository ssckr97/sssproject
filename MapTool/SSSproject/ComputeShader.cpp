#include "stdafx.h"
#include "ComputeShader.h"


ParticleShader::ParticleShader()
{
}

ParticleShader::~ParticleShader()
{
}

D3D12_INPUT_LAYOUT_DESC ParticleShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 5;
	D3D12_INPUT_ELEMENT_DESC *inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "TEXTUREIDX", 0, DXGI_FORMAT_R32_UINT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[1] = { "SIZE", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[2] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[3] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[4] = { "ALPHA", 0, DXGI_FORMAT_R32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };


	D3D12_INPUT_LAYOUT_DESC inputLayout;
	inputLayout.NumElements = inputElementDescsNum;
	inputLayout.pInputElementDescs = inputElementDescs;
	return inputLayout;
}

D3D12_DEPTH_STENCIL_DESC ParticleShader::CreateDepthStencilState()
{

	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = TRUE;
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS_EQUAL;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return(depthStencilDesc);
}

D3D12_BLEND_DESC ParticleShader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;

	blendDesc.AlphaToCoverageEnable = FALSE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = TRUE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;

	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	UINT colorMask = D3D12_COLOR_WRITE_ENABLE_RED + D3D12_COLOR_WRITE_ENABLE_GREEN + D3D12_COLOR_WRITE_ENABLE_BLUE;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = colorMask;

	return blendDesc;
}

D3D12_SHADER_BYTECODE ParticleShader::CreateVertexShader()
{
	return Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSParticle", "vs_5_1", &vertexShaderBlob);
}

D3D12_SHADER_BYTECODE ParticleShader::CreatePixelShader()
{
	return Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSParticle", "ps_5_1", &pixelShaderBlob);
}

D3D12_SHADER_BYTECODE ParticleShader::CreateGeometryShader()
{
	return Shader::CompileShaderFromFile(L"Shaders.hlsl", "GSParticleDraw", "gs_5_1", &geometryShaderBlob);
}

D3D12_SHADER_BYTECODE ParticleShader::CreateComputeShader()
{
	return Shader::CompileShaderFromFile(L"ComputeShader.hlsl", "main", "cs_5_1", &computeShaderBlob);
}

D3D12_PRIMITIVE_TOPOLOGY_TYPE ParticleShader::SetTopologyType()
{
	return D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;
}

void ParticleShader::CreateShader(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 1;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	::ZeroMemory(&pipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	pipelineStateDesc.pRootSignature = graphicsRootSignature;
	pipelineStateDesc.VS = CreateVertexShader();
	pipelineStateDesc.GS = CreateGeometryShader();
	pipelineStateDesc.PS = CreatePixelShader();
	pipelineStateDesc.RasterizerState = CreateRasterizerState();
	pipelineStateDesc.BlendState = CreateBlendState();
	pipelineStateDesc.DepthStencilState = CreateDepthStencilState();
	pipelineStateDesc.InputLayout = CreateInputLayout();
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = SetTopologyType();
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;

	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[0]);
	// 빌보드 사각형

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();
	if (geometryShaderBlob) geometryShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;

	computePipelineStatesNum = 1;
	computePipelineStates = new ID3D12PipelineState*[computePipelineStatesNum];

	::ZeroMemory(&computePipelineStateDesc, sizeof(D3D12_COMPUTE_PIPELINE_STATE_DESC));
	computePipelineStateDesc.pRootSignature = computeRootSignature;
	computePipelineStateDesc.CS = CreateComputeShader();

	hResult = device->CreateComputePipelineState(&computePipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&computePipelineStates[0]);

	if (computeShaderBlob) computeShaderBlob->Release();
}

void ParticleShader::CreateShaderVariables(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx)
{
	// 인스턴스버퍼 생성
	instanceBuffer = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTANCE_PARTICLE) * BUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	instanceBuffer->Map(0, NULL, (void**)&mappedInstanceBuffer);

	instanceBufferView = new D3D12_VERTEX_BUFFER_VIEW[BUFFERSIZE];

	for (int i = 0; i < BUFFERSIZE; ++i)
	{
		instanceBufferView[i].BufferLocation = instanceBuffer->GetGPUVirtualAddress() + (sizeof(VS_VB_INSTANCE_PARTICLE) * i);
		instanceBufferView[i].StrideInBytes = sizeof(VS_VB_INSTANCE_PARTICLE);
		instanceBufferView[i].SizeInBytes = sizeof(VS_VB_INSTANCE_PARTICLE);
	}

	particleBuffer.reserve(BUFFERSIZE);
}

void ParticleShader::ModifyCopyBuffer(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ParticleBlobModify blob, int idx)
{

	ID3D12Resource* tempCopyBuffer = copyBuffer[idx];
	ID3D12Resource* tempCopyBufferCS = copyBufferCS[idx];
	CreateCopyBuffer(device, commandList, blob);

	copyBuffer[idx] = copyBuffer.back();
	copyBufferCS[idx] = copyBufferCS.back();
	copyBuffer.back() = tempCopyBuffer;
	copyBufferCS.back() = tempCopyBufferCS;

	releaseBuffer.emplace_back(copyBuffer.back());
	releaseBuffer.emplace_back(copyBufferCS.back());

	copyBuffer.pop_back();
	copyBufferCS.pop_back();

	ParticleBlobModify tempBlob = blobList[idx];
	blobList[idx] = blobList.back();
	blobList.back() = blobList[idx];
	blobList.pop_back();

	int pSize = particles.size();

	for (int i = 0; i < pSize; ++i)
	{
		if (particles[i].copyIdx == idx)
		{
			CreateParticle(device, commandList, particles[i]);

			particles[i].age = -1;
		}
	}

}

void ParticleShader::DeleteCopyBuffer(int idx)
{
	ID3D12Resource* tempCopyBuffer = copyBuffer[idx];
	copyBuffer[idx] = copyBuffer.back();
	copyBuffer.back() = tempCopyBuffer;
	releaseBuffer.emplace_back(copyBuffer.back());
	copyBuffer.pop_back();

	ID3D12Resource* tempCopyBufferCS = copyBufferCS[idx];
	copyBufferCS[idx] = copyBufferCS.back();
	copyBufferCS.back() = tempCopyBufferCS;
	releaseBuffer.emplace_back(copyBufferCS.back());
	copyBufferCS.pop_back();

	ParticleBlobModify tempBlob = blobList[idx];
	blobList[idx] = blobList.back();
	blobList.back() = tempBlob;
	blobList.pop_back();

	for (int i = 0; i < particles.size(); ++i)
	{
		if (particles[i].copyIdx > idx)
		{
			particles[i].copyIdx = particles[i].copyIdx - 1;
		}
	}
}

void ParticleShader::CreateCopyBuffer(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ParticleBlobModify blob)
{
	VS_SB_PARTICLE* particleLump = new VS_SB_PARTICLE[blob.amount];

	std::default_random_engine dre;
	std::uniform_real_distribution<float> urdPosX(blob.positionMin.x, blob.positionMax.x);
	std::uniform_real_distribution<float> urdPosY(blob.positionMin.y, blob.positionMax.y);
	std::uniform_real_distribution<float> urdPosZ(blob.positionMin.z, blob.positionMax.z);

	std::uniform_real_distribution<float> urdVelX(blob.velocityMin.x, blob.velocityMax.x);
	std::uniform_real_distribution<float> urdVelY(blob.velocityMin.y, blob.velocityMax.y);
	std::uniform_real_distribution<float> urdVelZ(blob.velocityMin.z, blob.velocityMax.z);

	std::uniform_real_distribution<float> urdAge(blob.ageMin, blob.ageMax);


	for (int i = 0; i < blob.amount; ++i) {

		particleLump[i].position = XMFLOAT3(urdPosX(dre), urdPosY(dre), urdPosZ(dre));
		particleLump[i].velocity = XMFLOAT3(urdVelX(dre), urdVelY(dre), urdVelZ(dre));
		particleLump[i].age = urdAge(dre);
	}

	copyBuffer.emplace_back(::CreateBufferResource(device, commandList, NULL, sizeof(VS_SB_PARTICLE) * blob.amount, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_GENERIC_READ, NULL));
	copyBufferCS.emplace_back(::CreateBufferResource(device, commandList, NULL, sizeof(VS_SB_PARTICLE) * blob.amount, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS));
	::ResourceBarrier(commandList, copyBufferCS.back(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	mappedCopyBuffer.emplace_back();
	copyBuffer.back()->Map(0, NULL, (void**)&mappedCopyBuffer.back());


	for (int i = 0; i < blob.amount; ++i)
	{
		mappedCopyBuffer.back()[i] = particleLump[i];
	}

	copyBufferIdx++;
	blobList.emplace_back(blob);

	delete[] particleLump;
}

// 파티클 덩어리의 위치 변환.
void ParticleShader::UpdateShaderVariables(int idx)
{
	mappedInstanceBuffer[idx].textureIdx = particles[idx].textureIdx;
	mappedInstanceBuffer[idx].position = XMFLOAT3(particles[idx].position);
	mappedInstanceBuffer[idx].size = XMFLOAT2(particles[idx].size);
	mappedInstanceBuffer[idx].color = XMFLOAT4(particles[idx].color);
	mappedInstanceBuffer[idx].alphaDegree = particles[idx].alphaDegree;
}

void ParticleShader::ReleaseShaderVariables()
{
	if (instanceBuffer) {
		instanceBuffer->Unmap(0, NULL);
		instanceBuffer->Release();
	}

	if (!copyBuffer.empty()) {
		for (int i = 0; i < copyBuffer.size(); ++i)
		{
			copyBuffer[i]->Release();
			copyBufferCS[i]->Release();
		}

	}

	if (!particleBuffer.empty()) {
		for (int i = 0; i < particleBuffer.size(); ++i)
			if (particleBuffer[i]) particleBuffer[i]->Release();
	}

}

void ParticleShader::ReleaseUploadBuffers()
{
}

void ParticleShader::AnimateObjects(float timeElapsed, Camera * camera)
{
	// particle의 Age를 최댓값만 CPU에서 계산하여 최댓값을 다 지났을 경우 지워준다.
	for (int i = 0; i < particles.size(); ++i)
	{
		if (particles[i].age == INFINITE)
		{
			UpdateShaderVariables(i);
		}
		else{
			if (particles[i].age > 0.0f)
			{
				particles[i].age -= timeElapsed;
				UpdateShaderVariables(i);

			}
			else
			{
				ReleaseParticle(i);
			}
		}
	}
	SortParticleBuffers(camera);
}

void ParticleShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, std::vector<void*>& context, int fobjectSize)
{
	particles.reserve(BUFFERSIZE);

	CreateShaderVariables(device, commandList);

	CreateCopyBuffer(device, commandList, ParticleBlobModify{ XMFLOAT3{ -1, -1, -1 }, XMFLOAT3{ 1,1,1 }, XMFLOAT3{ -1, 1, -1 }, XMFLOAT3{ 1, 2, 1 }, 4.0f, 7.0f , PARTICLESIZE_DEFAULT});

}

void ParticleShader::ReleaseObjects()
{
}

void ParticleShader::Render(ID3D12GraphicsCommandList * commandList, Camera * camera)
{
	for (int i = 0; i < particles.size(); ++i)
	{
		int idx = particleDepths[i].first;
		if (particles[idx].age > 0)
		{
			Compute(commandList, idx);   // ComputeShader를 사용하여 리소스에 값을 쓴다.
			commandList->SetPipelineState(pipelineStates[0]);
			commandList->SetGraphicsRootShaderResourceView(GraphicsRootParticleInfo, particleBuffer[idx]->GetGPUVirtualAddress());
			commandList->SetGraphicsRoot32BitConstants(GraphicsRootParticleAge, 1, &particles[idx].divAge,  0);
			commandList->SetGraphicsRoot32BitConstants(GraphicsRootParticleAge, 1, &particles[idx].type, 1);

			commandList->IASetVertexBuffers(0, 1, &instanceBufferView[idx]);
			commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);

			commandList->DrawInstanced(blobList[particles[idx].copyIdx].amount, 1, 0, 0);
		}
	}
}

void ParticleShader::CreateParticle(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ParticleInfoInCPU particleInfo)
{
	
	particleBuffer.emplace_back(::CreateBufferResource(device, commandList, NULL, sizeof(VS_SB_PARTICLE) * blobList[particleInfo.copyIdx].amount, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, NULL, D3D12_RESOURCE_FLAG_ALLOW_UNORDERED_ACCESS));

	::ResourceBarrier(commandList, copyBufferCS[particleInfo.copyIdx], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_COPY_DEST);
	commandList->CopyResource(copyBufferCS[particleInfo.copyIdx], copyBuffer[particleInfo.copyIdx]);
	commandList->CopyResource(particleBuffer.back(), copyBuffer[particleInfo.copyIdx]);
	::ResourceBarrier(commandList, copyBufferCS[particleInfo.copyIdx], D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);
	::ResourceBarrier(commandList, particleBuffer.back(), D3D12_RESOURCE_STATE_COPY_DEST, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);

	particles.emplace_back(particleInfo);

	particleDepths.emplace_back(std::make_pair(particleBuffer.size() - 1, 0.0f));

}
// type : 0 : position, 1 : velocity, 2 : age

void ParticleShader::ReleaseParticle(int idx)
{
	ParticleInfoInCPU tempInfo = particles[idx];
	particles[idx] = particles.back();
	particles.back() = tempInfo;
	particles.pop_back();

	ID3D12Resource* tempBuf = particleBuffer[idx];
	particleBuffer[idx] = particleBuffer.back();
	particleBuffer.back() = tempBuf;
	releaseBuffer.emplace_back(particleBuffer.back());
	particleBuffer.pop_back();

	std::pair<int, float> tempDepth = particleDepths[idx];
	particleDepths[idx] = particleDepths.back();
	particleDepths.back() = tempDepth;
	particleDepths.pop_back();
}

void ParticleShader::GarbageCollection()
{
	for (int i = 0; i < releaseBuffer.size(); ++i)
	{
		releaseBuffer[i]->Release();
	}
	if(releaseBuffer.size() > 0)
		releaseBuffer.clear();
}

int ParticleShader::AddParticle(ID3D12Device* device, ID3D12GraphicsCommandList * commandList, XMFLOAT3 pointxyz, int copyIdx, UINT textureIdx)
{
	ParticleInfoInCPU info{};
	info.size = XMFLOAT2(3.0f, 3.0f);
	info.position = pointxyz;
	info.type = 0;
	info.textureIdx = textureIdx;
	info.copyIdx = copyIdx;

	CreateParticle(device, commandList, info);

	return particles.size() - 1;
}

void ParticleShader::Compute(ID3D12GraphicsCommandList* commandList, int idx)
{
	commandList->SetPipelineState(computePipelineStates[0]);

	commandList->SetComputeRoot32BitConstants(ComputeRootParticleTypeAcc, 1, &particles[idx].type, 0);
	commandList->SetComputeRoot32BitConstants(ComputeRootParticleTypeAcc, 1, &particles[idx].acc, 1);
	commandList->SetComputeRoot32BitConstants(ComputeRootParticleTypeAcc, 1, &blobList[particles[idx].copyIdx].amount, 2);
	commandList->SetComputeRoot32BitConstants(ComputeRootParticleTypeAcc, 1, &particles[idx].divAge, 3);
	commandList->SetComputeRoot32BitConstants(ComputeRootParticleTypeAcc, 1, &particles[idx].radius, 4);
	commandList->SetComputeRoot32BitConstants(ComputeRootParticleTypeAcc, 1, &particles[idx].yGab, 5);
	commandList->SetComputeRoot32BitConstants(ComputeRootParticleTypeAcc, 1, &particles[idx].rotateAcc, 6);

	commandList->SetComputeRootShaderResourceView(ComputeRootParticleCopySRV, copyBufferCS[particles[idx].copyIdx]->GetGPUVirtualAddress());

	commandList->SetComputeRootShaderResourceView(ComputeRootParticleInfoSRV, particleBuffer[idx]->GetGPUVirtualAddress());
	::ResourceBarrier(commandList, particleBuffer[idx], D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE, D3D12_RESOURCE_STATE_UNORDERED_ACCESS);
	commandList->SetComputeRootUnorderedAccessView(ComputeRootParticleInfoUAV, particleBuffer[idx]->GetGPUVirtualAddress());
	::ResourceBarrier(commandList, particleBuffer[idx], D3D12_RESOURCE_STATE_UNORDERED_ACCESS, D3D12_RESOURCE_STATE_NON_PIXEL_SHADER_RESOURCE);

	commandList->Dispatch(static_cast<int>(ceil(blobList[particles[idx].copyIdx].amount / BLOCKSIZE)), 1, 1);
}
void ParticleShader::SortParticleBuffers(Camera* camera)
{
	// Particle 덩어리의 Sort

	XMFLOAT3 cPos = camera->GetPosition();
	XMFLOAT3 cLook = camera->GetLookVector();

	for (int i = 0; i < particleDepths.size(); ++i)
	{
		XMFLOAT3 pPos = particles[i].position;
		XMFLOAT3 toParticle = ::Vector3::Subtract(pPos, cPos);
		particleDepths[i].first = i;
		particleDepths[i].second = Vector3::DotProduct(cLook, toParticle);
	}
	std::sort(particleDepths.begin(), particleDepths.end(),
		[](std::pair<int, float> a, std::pair<int, float> b) {
		return a.second > b.second;
	});
}

void ParticleShader::ClearParticle()
{
	for (int i = 0; i < particleBuffer.size(); ++i)
	{
		releaseBuffer.emplace_back(particleBuffer[i]);
	}
	for (int i = 0; i < copyBuffer.size(); ++i)
	{
		releaseBuffer.emplace_back(copyBuffer[i]);
		releaseBuffer.emplace_back(copyBufferCS[i]);
	}
	particleDepths.clear();
	particles.clear();
	particleBuffer.clear();
	copyBuffer.clear();
	copyBufferCS.clear();
	blobList.clear();
}
