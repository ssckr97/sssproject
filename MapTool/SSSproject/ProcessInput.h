#pragma once
#include "GUI.h"
#include "Picking.h"

class ProcessInput
{
private:
	Scene* scene;
	int guiNum = 2;
	GUI **gui;
	OptionGUI *optiongui;
	ObjectGUI* objectgui;
	TerrainGUI* terraingui;
	BoundingGUI* boundinggui;

	Picking *picking;
	Player *player;
	Camera* camera = NULL;

	POINT oldCursorPos;
	POINT pickCursorPos;

	bool buttonDown{ false };
	int windowClientWidth;
	int windowClientHeight;
	int oldObjectIdx = -1;
	int oldShaderIdx = -1;

	int oldbIdx = -1;
	int oldsIdx = -1;


	float cameraSpeed = 150.0f;


public:
	ProcessInput(Scene* scene, int guiNum, GUI **gui, OptionGUI* optiongui, ObjectGUI* objectgui , TerrainGUI* terraingui,BoundingGUI* boundinggui , Player* player , Picking *picking , Camera* camera , int windowWidth , int windowHeight);


	void ProcessBassicCameraKeyboardInput( HWND hwnd, GameTimer& gameTimer);
	void ProcessChangeTerrain(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, HWND hwnd, GameTimer& gameTimer);
	void ProcessObjectEditInput(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , HWND hwnd, GameTimer& gameTimer);
	void ProcessObjectNewInput(HWND hwnd, GameTimer& gameTimer , ID3D12Device* device, ID3D12GraphicsCommandList* commandList, bool& dummyFlag);
	void ProcessArrowPicking(int& index, POINT& tempCursorPos, float& nearDistance);
	void ProcessObjectPicking(std::vector<GameObject*> objects, int & index, POINT & tempCursorPos, float & nearDistance);
	bool ProcessTerrainPicking(GameObject* objects, XMFLOAT3& PickingPoint, POINT & tempCursorPos, float & nearDistance , int& bIdx);
	void ProcessChangeTerrainHeight(HeightMapTerrain* objects, XMFLOAT3 PickingPoint , int bIdx , float frameTime , float CircleSize , int RadioFlags , float maxHeight, float minHeight);
	void ProcessSplattingTextureTerrain(ID3D12Device * device, ID3D12GraphicsCommandList * commandList ,GameObject* objects, XMFLOAT3 PickingPoint, int bIdx, float frameTime, float CircleSize , int textuerType);
	void ProcessLightInput(HWND hwnd, GameTimer& gameTimer);
	void ProcessParticleInput(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, HWND hwnd , GameTimer& gameTimer);
	void ProcessBoundingInput(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, HWND hwnd, GameTimer& gameTimer , bool &dummyFlag , int& fobIdx);


	void SetPickCursorPos(POINT& point) { pickCursorPos = point; }
	void SetOldCursorPos(POINT& point) { oldCursorPos = point; }
	void HideArrowObject(std::vector<GameObject*> &depthshaderObject);

	void SetButtonDown(bool buttonDown) { this->buttonDown = buttonDown; }
	bool GetButtonDown() { return buttonDown; }

	float& GetCameraSpeed() { return cameraSpeed; }
	ProcessInput();
	~ProcessInput();
};

