#pragma once
#include "Timer.h"
#include "SaveFile.h"
#include "Camera.h"
#include "Player.h"
#include "imgui.h"
#include "imgui_impl_win32.h"
#include "imgui_impl_dx12.h"
#include <wincodec.h>
enum {
	OPTIONGUI,
	OBJECTGUI,
	TERRAINGUI,
	LIGHTGUI,
	PARTICLEGUI,
	BOUNDINGGUI
};
class LightGUI;
class ParticleGUI;
//오브젝트를 담당하는 GUI이다.
struct ObjectInform {
	int shaderIdx = -1;
	int objectIdx = -1;
};
struct NaviInform {
	int idx;
	int x;
	int z;
	int width;
	XMFLOAT3 dtriv1; // 아래삼각형 
	XMFLOAT3 dtriv2; // 아래삼각형 
	XMFLOAT3 dtriv3; // 아래삼각형 

	XMFLOAT3 utriv1; // 위에 삼각형 
	XMFLOAT3 utriv2; // 위에 삼각형 
	XMFLOAT3 utriv3; // 위에 삼각형 
	XMFLOAT3 center;
};


class GUI {
private:
protected:
	//라디오 버튼 flag이다 
	ImGuiColorEditFlags Radioflags = 0;
	ImGuiColorEditFlags objectflags = 0;
	ImGuiColorEditFlags transflags = 0;
	
	bool unitNoise = false;

	XMFLOAT2 CursorPos;

	int shapeIdx = 0;

public:
	GUI();
	virtual ImGuiColorEditFlags RadioButtonState();
	virtual ImGuiColorEditFlags GetParticleType() { return 0; }
	virtual ImGuiColorEditFlags GetShapeIdx() { return shapeIdx; }
	virtual void AddParticleIndex(int idx , int copyidx) {}
	virtual void useShortCut(char c) {};
	virtual void Render() {};
	virtual ObjectInform GetLocatedObjectType() { return ObjectInform(); }
	bool GetNoise() { return unitNoise; }

};

class MangerGUI : public GUI {
private:
	int selectedGui = -1;
public:
	int GetinputGuiType() { return selectedGui; }
	virtual void Render();

};

//옵션을 담당하는 GUI이다.
class OptionGUI : public GUI {
	Scene* scene;
	Camera* camera;

	ID3D12Device* device;
	ID3D12GraphicsCommandList* commandList;
	ID3D12CommandQueue* commandQueue;
	std::vector<std::string> mapName;
	ID3D12Resource* alphaResource0;
	ID3D12Resource* alphaResource1;
	ID3D12Resource* alphaResource2;
	ID3D12Resource* heightmapResource;
	ID3D12Resource* normalResource;

	const char* currentMap = NULL;
	int mapNumber = -1;
	LightGUI* lightGUI;
	ParticleGUI* particleGUI;

	float* cameraSpeed;



	bool modifiygui = 0;

	bool renderBoundingBox = false;
	bool renderLight = false;
	bool renderNavi = false;

public:
	OptionGUI(Scene* scene, ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12CommandQueue* commandQueue,
		ID3D12Resource* alphaResource0, ID3D12Resource* alphaResource1, ID3D12Resource* terrainAlphaResource2, ID3D12Resource* heightmapResource, ID3D12Resource* normalResource, LightGUI* lightGUI , ParticleGUI* particleGUI);
	float  clear_color[4] = { 0.0f, 0.0f, 0.0f, 0.00f };
	bool Getmodifygui() { return modifiygui; }
	virtual void Render();
	virtual void useShortCut(char c);
	virtual void SetCameraSpeed(float* speed) { cameraSpeed = speed; }
};


class ObjectGUI : public GUI  {
	
	int PickingObjectIndex;
	int PickingShaderIndex;

	XMFLOAT3 angle = { 0,0,0 };
	XMFLOAT3 oldRoateAngle = { 0,0,0 };
	XMFLOAT3 rotateAngle = { 0,0,0 };
	bool isNewOrEditInit{ false };

	bool isGravity;
	bool InitNewOrEdit = 0;

	ObjectInform objectinform;
	ObjectInform characeterinform;
	ImGuiColorEditFlags NewOrEditFlags = 0;
	GameObject *pickingObject = NULL;
	GameObject *dummyObject = NULL;
	XMFLOAT4X4 tempTransform;

	
public:
	ObjectGUI();

	virtual void Render();
	//카메라 모드 일때는 피킹이 불가능 
	void SetDummy(GameObject* dummyObject);

	//오브젝트 정보 세팅 
	void SetPickObjectImformation(int bufferIndex, int objectIndex , GameObject* pickingObject) {
		this->PickingShaderIndex = bufferIndex;
		this->PickingObjectIndex = objectIndex;
		this->pickingObject = pickingObject;
	}
	//카메라 이동 , 오브젝트 움직임 , 로테이트 키입력 받기
	virtual void useShortCut(char c);

	int GetPickingObjectIndex() { return PickingObjectIndex; }
	int GetPickingShaderIndex() { return PickingShaderIndex; }
	ImGuiColorEditFlags GetNewOrEditFlags() { return NewOrEditFlags; }
	virtual ObjectInform GetLocatedObjectType();

	void EditObject();
	void NewObject();
};

class TerrainGUI : public GUI {
private:
	Scene* scene;
	GameObject* circleObject;
	HeightMapTerrain* terrain;
	float maxHeight{ 0.0f };
	float minHeight{ 0.0f };
	float flatHeight{ 0.0f };
	int splittingTextureNumber{-1};

	ImGuiColorEditFlags terrainShapeFlags = 0;

	int autot0{-1};
	int autot1{-1};
	int autot2{-1};

	bool cbFlag = false;
	int naviSize = 0;



public:
	TerrainGUI(GameObject* CircleObject , HeightMapTerrain* terrain , Scene* scene) : circleObject(CircleObject) , terrain(terrain) , scene(scene) {}
	
	float GetCircleScale() { return circleObject->GetInputScale(); }
	float GetMaxHeight() { return maxHeight; }
	float GetMinHeight() { return minHeight; }
	float GetFlatHeight() { return flatHeight; }
	int GetTerrainShapeFlags() { return terrainShapeFlags; }
	int GetSplittingTextureNumber() { return splittingTextureNumber; }
	virtual void useShortCut(char c);
	virtual void Render();

	bool& GetcbFlag() { return cbFlag; }
	int GetNaviSize() { return naviSize; }

};

class LightGUI : public GUI {
private:
	Scene* scene;
	ImGuiColorEditFlags lightFlags = 0;
	XMFLOAT3 pos{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 direction{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 diffuse{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 specular{ 0.0f , 0.0f , 0.0f };
	float specFactor = 0.0f;
	XMFLOAT3 ambient{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 attenuation{ 0.0f , 0.0f , 0.0f };
	float falloff = 0;
	float phi = 0;
	float theta = 0;
	float range = 0;

	const char* current_item = NULL;
	const char* current_deleteitem = NULL;
	//light의 이름을 가지고 있는 벡터 
	std::vector<std::string> vec;

	int lightnumber = -1;
	int deleteLightNumber = -1;

public:

	LightGUI(Scene* scene);
	virtual void useShortCut(char c);
	virtual void Render();
	void AddLight(int idx, LIGHT light);
	void InitLight(int idx, int type, LIGHT& light);

	void DeleteAllLight();

};

struct ParticleInForm {
	std::string name;
	int index;
	int copyidx;

};

class ParticleGUI : public GUI {
private:
	//파티클들의 이름을 가지고 있는 벡터 
	std::vector<ParticleInForm> vec;
	
	// 파티클들의 모양이름을 가지고 있는 벡터
	std::vector<std::string> shapeVec;

	ParticleBlobModify shapeData{};
	
	ParticleShader* particleShader;
	Scene* scene;
	ID3D12Device* device;
	ID3D12GraphicsCommandList* commandList;
	int textureNumber = 0;
	int typeNumber = 0;

	ImGuiColorEditFlags particleType = 0;
	const char* current_item = NULL;
	const char* delete_item = NULL;

	int curShapeIdx = 0;
	int shapeNumber = -1;
	int shapeDeleteNumber = -1;
	int shapeDeleteIdx = -1;

	int particleNumber = -1;
	int deleteNumber = -1;
	int deleteIdx = -1;
	bool deleteFail = false;

public:
	ParticleGUI(Scene* scene ,ID3D12Device*, ID3D12GraphicsCommandList* commandList);
	virtual void InitShape();
	virtual void AddParticleIndex(int idx , int copyidx);

	virtual void useShortCut(char c);
	virtual ImGuiColorEditFlags GetParticleType() { return particleType; }
	virtual void Render();
	virtual void ClearVec();
};

class BoundingGUI : public GUI {
private:
	std::vector<std::string> nameList;
	ObjectInform objectinform;
	ObjectInform characeterinform;

	GameObject* selectedobject = NULL;
	int bIdx = FAILPICKING;
	XMFLOAT4X4 tempTransform;


	bool* cbFlag;
	bool* dbFlag;
public:
	BoundingGUI(bool* cbFlag , bool* dbFlag);
	virtual void Render();
	virtual void useShortCut(char c);
	virtual ObjectInform GetLocatedObjectType();
	int& GetbIdx() { return bIdx; }
	void SetSelectedObject(GameObject* selectedbox) { this->selectedobject = selectedbox; };



};