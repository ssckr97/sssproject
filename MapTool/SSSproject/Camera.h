#pragma once

#define FIRST_PERSON_CAMERA 0x01
#define SPACESHIP_CAMERA 0x02
#define THIRD_PERSON_CAMERA 0x03
//카메라의 종류(모드: Mode)를 나타내는 상수를 다음과 같이 선언한다.

#define ASPECT_RATIO (float(FRAME_BUFFER_WIDTH) / float(FRAME_BUFFER_HEIGHT))
//프레임 버퍼의 크기와 종횡비(Aspect Ratio)를 나타내는 상수를 다음과 같이 선언한다.
class Player;

struct VS_CB_CAMERA_INFO
{
	XMFLOAT4X4 view;
	XMFLOAT4X4 inverseView;
	XMFLOAT4X4 projection;
	XMFLOAT4X4 inversePro;
	XMFLOAT3   position;

};

class Camera
{
protected:
	XMFLOAT3 position;
	//카메라의 위치(월드좌표계) 벡터이다.

	XMFLOAT3 right;
	XMFLOAT3 up;
	XMFLOAT3 look;
	//카메라의 로컬 x-축(Right), y-축(Up), z-축(Look)을 나타내는 벡터이다.

	float pitch;
	float roll;
	float yaw;
	//카메라가 x-축, z-축, y-축으로 얼마만큼 회전했는 가를 나타내는 각도이다.

	DWORD mode;
	//카메라의 종류(1인칭 카메라, 스페이스-쉽 카메라, 3인칭 카메라)를 나타낸다.

	XMFLOAT3 lookAtWorld;
	//플레이어가 바라볼 위치 벡터이다. 주로 3인칭 카메라에서 사용된다.

	XMFLOAT3 offset;
	//플레이어와 카메라의 오프셋을 나타내는 벡터이다. 주로 3인칭 카메라에서 사용된다.

	float timeLag;
	//플레이어가 회전할 때 얼마만큼의 시간을 지연시킨 후 카메라를 회전시킬 것인가를 나타낸다.

	XMFLOAT4X4 view;
	XMFLOAT4X4 inverseView;
	XMFLOAT4X4 projection;
	XMFLOAT4X4 inversePro;
	D3D12_VIEWPORT viewport;
	D3D12_RECT scissorRect;

	Player *player = NULL;
	//카메라를 가지고 있는 플레이어에 대한 포인터이다.

	ID3D12Resource					*cbCamera = NULL;
	VS_CB_CAMERA_INFO				*cbMappedCamera = NULL;
	// CBV로 사용하기 위한 정보들.

public:
	Camera();
	Camera(Camera *pCamera);
	virtual ~Camera();

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	//카메라의 정보를 셰이더 프로그램에게 전달하기 위한 상수 버퍼를 생성하고 갱신한다.

	virtual void ReleaseShaderVariables();
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);

	void GenerateViewMatrix();
	void GenerateViewMatrix(XMFLOAT3 position, XMFLOAT3 lookAt, XMFLOAT3 up);
	//카메라 변환 행렬을 생성한다.

	void RegenerateViewMatrix();
	/*카메라가 여러번 회전을 하게 되면 누적된 실수 연산의 부정확성 때문에 카메라의 로컬 x-축(Right), y-축(Up), z-
	축(LookAt)이 서로 직교하지 않을 수 있다. 카메라의 로컬 x-축(Right), y-축(Up), z-축(LookAt)이 서로 직교하도록
	만들어준다.*/
	
	void GenerateProjectionMatrix(float nearPlaneDistance, float farPlaneDistance, float aspectRatio, float fovAngle);
	//투영 변환 행렬을 생성한다.

	void SetViewport(int topLeftX, int topLeftY, int width, int height, float minZ = 0.0f, float maxZ = 1.0f);
	void SetScissorRect(LONG leftX, LONG topY, LONG rightX, LONG bottomY);
	virtual void SetViewportsAndScissorRects(ID3D12GraphicsCommandList* commandList);

	void SetPlayer(Player* player) { this->player = player; }
	Player *GetPlayer() { return(player); }

	void SetMode(DWORD mode) { this->mode = mode; }
	DWORD GetMode() { return(mode); }

	void SetPosition(XMFLOAT3 position) { this->position = position; }
	XMFLOAT3& GetPosition() { return(position); }
	void SetLookAtPosition(XMFLOAT3 lookAtWorld) { this->lookAtWorld = lookAtWorld; }

	XMFLOAT3& GetLookAtPosition() { return(lookAtWorld); }
	XMFLOAT3& GetRightVector() { return(right); }
	XMFLOAT3& GetUpVector() { return(up); }
	XMFLOAT3& GetLookVector() { return(look); }

	float& GetPitch() { return(pitch); }
	float& GetRoll() { return(roll); }
	float& GetYaw() { return(yaw); }

	void SetOffset(XMFLOAT3 offset) { this->offset = offset; }
	XMFLOAT3& GetOffset() { return(offset); }

	void SetTimeLag(float timeLag) { this->timeLag = timeLag; }
	float GetTimeLag() { return(timeLag); }

	XMFLOAT4X4 GetViewMatrix() { return(view); }
	XMFLOAT4X4 GetProjectionMatrix() { return(projection); }

	D3D12_VIEWPORT GetViewport() { return(viewport); }
	D3D12_RECT GetScissorRect() { return(scissorRect); }

	virtual void Move(const XMFLOAT3& shift) 
	{
		position.x += shift.x;
		position.y += shift.y; 
		position.z += shift.z;
	}
	//카메라를 Shift 만큼 이동한다.

	virtual void Rotate(float pitch = 0.0f, float yaw = 0.0f, float roll = 0.0f) { }
	//카메라를 x-축, y-축, z-축으로 회전하는 가상함수이다.
	virtual void Update(XMFLOAT3& lookAt, float timeElapsed) { }
	//카메라의 이동, 회전에 따라 카메라의 정보를 갱신하는 가상함수이다.
	virtual void SetLookAt(const XMFLOAT3& lookAt) { }
	//3인칭 카메라에서 카메라가 바라보는 지점을 설정한다. 일반적으로 플레이어를 바라보도록 설정한다.
};

class SpaceShipCamera : public Camera
{
public:
	SpaceShipCamera(Camera *camera);
	virtual ~SpaceShipCamera() { }
	virtual void Rotate(float pitch = 0.0f, float yaw = 0.0f, float roll = 0.0f);
};

class FirstPersonCamera : public Camera
{
public:
	FirstPersonCamera(Camera *pCamera);
	virtual ~FirstPersonCamera() { }
	virtual void Rotate(float fPitch = 0.0f, float fYaw = 0.0f, float fRoll = 0.0f);
};

class ThirdPersonCamera : public Camera
{
public:
	ThirdPersonCamera(Camera *pCamera);
	virtual ~ThirdPersonCamera() { }
	virtual void Update(XMFLOAT3& LookAt, float fTimeElapsed);
	virtual void SetLookAt(const XMFLOAT3& vLookAt);
};