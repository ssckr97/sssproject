#pragma once
#include"stdafx.h"
#include"Vertex.h"
#include"HeightMapLayer.h"

//정점을 표현하기 위한 클래스를 선언한다.

class GameObject;
class HeightMapTerrain;
struct Tri {
	XMFLOAT3 v1;
	XMFLOAT3 v2;
	XMFLOAT3 v3;
	
	Tri() {}
	//이웃하는삼각형은 무조껀 3개 존재
	Tri(XMFLOAT3 v1, XMFLOAT3 v2, XMFLOAT3 v3) {
		this->v1 = v1;
		this->v2 = v2;
		this->v3 = v3;
		
	}
};
struct NaviNode {
	XMFLOAT3 position;

};
struct NaviTile {
	int idx = 0;
	int x;
	int z;
	int width;
	Tri dtri; // 아래삼각형 
	Tri utri; // 위에 삼각형 
	XMFLOAT3 center;

	std::vector<int> leftTile;
	std::vector<int> rightTile;
	std::vector<int> upTile;
	std::vector<int> downTile;
	std::vector<int> leftupTile;
	std::vector<int> rightupTile;
	std::vector<int> leftDownTile;
	std::vector<int> rightDownTile;



	NaviTile() {}
	NaviTile(Tri dtri, Tri utri) {
		this->dtri = dtri;
		this->utri = utri;
		int xd = dtri.v3.x - dtri.v1.x;
		int zd = dtri.v2.z - dtri.v1.z;

		center.x = dtri.v1.x + xd;
		center.y = dtri.v1.y;
		center.z = dtri.v1.z + zd;

	}

};


struct LeftIndex {
	int x;
	int z;
	int index; // 왼쪽아래 , 오른쪽 , 왼쪽 위 , 오른쪽 위 
	

	int GetIndex() {
		return index;
	}
	int Getx() {
		return x;
	}
	int Getz() {
		return z;
	}

};

class Mesh
{
public:
	Mesh() {}
	Mesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual ~Mesh();

private:
	int references = 0;

public:
	void AddRef() { ++references; }
	void Release() { if (--references <= 0) delete this; }

protected:
	char meshName[256] = { 0 };

	UINT type = 0x00;

	UINT slot = 0;
	UINT offset = 0;

	BoundingBox aabbBox;

	XMFLOAT3 boundingBoxAABBCenter = XMFLOAT3(0.0f, 0.0f, 0.0f);
	XMFLOAT3 boundingBoxAABBExtents = XMFLOAT3(0.0f, 0.0f, 0.0f);

	D3D12_PRIMITIVE_TOPOLOGY primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

protected:
	// Buffers
	int indicesNum = 0;

	Vertex* vertex;
	ID3D12Resource* vertexBuffer = NULL;
	ID3D12Resource* vertexUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW vertexBufferView;
	UINT verticesNum = 0;
	UINT stride = 0;
	UINT primitiveNum;
	DiffusedVertex* vertices{ NULL };
	Vertex* linevertices;
	UINT lineVn;
	UINT linestride;
	UINT lineprimit;
	UINT lineprimitN;



	TexturedVertex* Texturedvertices{ NULL };

	UINT * indices{ NULL };
	//피킹 시 필요한 offset 
	UINT pickOffset;
	ID3D12Resource* indexBuffer = NULL;
	ID3D12Resource* indexUploadBuffer = NULL;
	D3D12_INDEX_BUFFER_VIEW indexBufferView;
	// VertexBuffer has Position, Color ...

	XMFLOAT3* positions;
	ID3D12Resource* positionBuffer = NULL;
	ID3D12Resource* positionUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW positionBufferView;

	int	subMeshesNum = 0;
	int* subSetIndicesNum = NULL;
	UINT** subSetIndices = NULL;

	ID3D12Resource** subSetIndexBuffers = NULL;
	ID3D12Resource** subSetIndexUploadBuffers = NULL;
	D3D12_INDEX_BUFFER_VIEW* subSetIndexBufferViews = NULL;
public:
	UINT GetType() { return type; }
	virtual void ReleaseUploadBuffers();
	virtual char* GetMeshName() { return meshName; }

	virtual void Render(ID3D12GraphicsCommandList* commandList);
	virtual void Render(ID3D12GraphicsCommandList *commandList, UINT instancesNum, std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBufferViews);

	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum, std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBufferViews)
	{
		Render(commandList, instancesNum, instancingBufferViews);
	};
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum) { Render(commandList); };
	virtual bool CheckRayIntersect(XMVECTOR origin, XMVECTOR dir, float& pfNearHitDistance, XMFLOAT4X4 world, int bIdx) { return false; }

	virtual BoundingBox GetBoundingBox() { return aabbBox; }
	virtual void ChangeVertexHeight(XMFLOAT3 LocalPickPlace, float frameTime, float CircleSize, int RadioFlags, float maxHeight, float minHeight, float flatHeight , std::vector<int> bindices, int shape) {}
	virtual void ChangeVertexTexture(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT3 LocalPickPlace, float frameTime,
		float CircleSize, int textureNumber, int bIdx) {}
	virtual void SmoothingVertexHeight(XMFLOAT3 LocalPickPlace, float frameTime, float CircleSize, int RadioFlags, std::vector<int> bIndices) {}

	virtual void LoadVertexInform(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, std::ifstream* in) {}

	virtual void ResetVertexHeight() {}
	virtual void ResetVertexTexNum() {}
	virtual void ResetVertexPosition(XMFLOAT3 scale, HeightMapTerrain* terrain) {}
	virtual void CalculateNormal(int zStart, int xStart) {}
	virtual Diffused2TexturedVertex* GetDiffuseTexturedVertex() { return NULL; }

};

class TriangleMesh : public Mesh
{
public:
	TriangleMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual ~TriangleMesh() { }
};

class CubeMeshDiffused : public Mesh
{
public:
	//직육면체의 가로, 세로, 깊이의 길이를 지정하여 직육면체 메쉬를 생성한다.
	CubeMeshDiffused(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
		float width = 2.0f, float height = 2.0f, float depth = 2.0f);
	virtual ~CubeMeshDiffused();
};

class AirplaneMeshDiffused : public Mesh
{
public:
	AirplaneMeshDiffused(ID3D12Device *device, ID3D12GraphicsCommandList* commandList,
		float width = 20.0f, float height = 20.0f, float depth = 4.0f, XMFLOAT4 color = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f));
	virtual ~AirplaneMeshDiffused();
};

class MeshIlluminated : public Mesh
{
public:
	MeshIlluminated(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual ~MeshIlluminated();
public:
	void CalculateTriangleListVertexNormals(XMFLOAT3 *normals, XMFLOAT3* positions, int nVertices);
	void CalculateTriangleListVertexNormals(XMFLOAT3 *normals, XMFLOAT3* positions, UINT verticesNum, UINT *indices, UINT IndicesNum);
	void CalculateTriangleStripVertexNormals(XMFLOAT3* normals, XMFLOAT3* positions, UINT verticesNum, UINT *indices, UINT IndicesNum);
	void CalculateVertexNormals(XMFLOAT3 *normals, XMFLOAT3 *positions, int verticesNum, UINT *indices, int indicesNum);
};

class CubeMeshIlluminated : public MeshIlluminated
{
public:
	CubeMeshIlluminated(ID3D12Device *device, ID3D12GraphicsCommandList* commandList, float width = 2.0f, float height = 2.0f, float depth = 2.0f);
	virtual ~CubeMeshIlluminated();
};

// Normal값을 CalculateVertexNormals를 사용하여 계산하여 정해줌.
class CubeMeshIlluminatedTextured : public MeshIlluminated
{
public:
	CubeMeshIlluminatedTextured(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width = 2.0f, float height = 2.0f, float depth = 2.0f);
	virtual ~CubeMeshIlluminatedTextured();
};

class ArrowMesh : public Mesh {
public:
	ArrowMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth, float Red, float Green, float Blue);
	virtual ~ArrowMesh();
};

class BoundingBoxMesh : public Mesh {
public:
	BoundingBoxMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth);
	BoundingBoxMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, XMFLOAT3 center ,float width, float height, float depth);
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum, std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBufferViews);
	virtual ~BoundingBoxMesh();


};
class HeightMapInForm {
public:
	XMFLOAT3 position;
	int idx;

	HeightMapInForm(XMFLOAT3 pos, int idx)
	{
		position = pos;
		this->idx = idx;
	}

};


class HeightMapGridMesh : public Mesh
{
protected:
	int							width;
	int							length;
	// 격자의 크기(가로 : x-방향, 세로: z-방향)이다.
	int							boundingWidth;
	int							boundingLength;



	Diffused2TexturedVertex* diffused2TexturedVerices{ NULL };
	std::vector< Diffused2TexturedVertex> tempd;

	Diffused2TexturedVertex* mappedDiffued2TextureVerices{ NULL };
	std::vector<SubIndex> subIndex;

	XMFLOAT3					scale;
	BoundingBox terrainBoundings[TERRAINBOUNDINGS];
	int** boundingTree;

	int			boundingVertexIndex[TERRAINBOUNDINGS];


	/*
	격자의 스케일(가로 : x-방향, 세로: z-방향, 높이: y-방향) 벡터이다. 실제 격자 메쉬의 각 정점의 x-좌표, y-좌표,
	z-좌표는 스케일 벡터의 x-좌표, y-좌표, z-좌표로 곱한 값을 갖는다. 즉 실제 격자의 x-축 방향의 간격은 1이 아니라
	스케일 벡터의 x-좌표가 된다. 이렇게 하면 작은 격자(적은 정점)을 사용하더라도 큰 크기의 격자(지형)을 생성할 수 있다.
	*/
	HeightMapLayer* heightMapLayer{ NULL };
	int layerSize = 0;

	virtual void Render(ID3D12GraphicsCommandList* commandList, int layerIndex);
public:
	HeightMapGridMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
		int xStart, int zStart, int width, int length,
		XMFLOAT3 scale = XMFLOAT3(1.0f, 1.0f, 1.0f), XMFLOAT4 color = XMFLOAT4(1.0f, 1.0f, 0.0f, 0.0f),
		void* context = NULL);
	virtual ~HeightMapGridMesh();
	XMFLOAT3 GetScale() { return (scale); }
	int GetWidth() { return (width); }
	int GetLength() { return (length); }
	virtual bool CheckRayIntersect(XMVECTOR origin, XMVECTOR dir, float& pfNearHitDistance, XMFLOAT4X4 world, int bIdx);
	// 격자의 좌표가 (x, z)일 때, 교점(정점)의 높이를 반환하는 함수이다.
	virtual float OnGetHeight(int x, int z, void* context);

	// 격자의 좌표가 (x, z)일 때 교점(정점)의 색상을 반환하는 함수이다.
	virtual XMFLOAT4 OnGetColor(int x, int z, void* context);
	virtual void ChangeVertexHeight(XMFLOAT3 LocalPickPlace, float frameTime, float CircleSize, int RadioFlags, float maxHeight, float minHeight, float flatHeight, std::vector<int> bindices , int shape);
	virtual void ChangeVertexTexture(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT3 LocalPickPlace, float frameTime, float CircleSize,
		int textureNumber, int bIdx);
	virtual void SmoothingVertexHeight(XMFLOAT3 LocalPickPlace, float frameTime, float CircleSize, int RadioFlags, std::vector<int> bIndices);
	virtual void ResetVertexHeight();
	virtual void ResetVertexTexNum();
	virtual void ResetVertexPosition(XMFLOAT3 scale, HeightMapTerrain* terrain);
	XMFLOAT3 calNormal(int index);
	void calALLNormal();

	void SmoothingMesh(int idx, int width, int length, std::vector<HeightMapInForm> subHeight, std::vector<HeightMapInForm>& tempSubHeight);
	void BuildLayer(ID3D12Device * device, ID3D12GraphicsCommandList * commandList);
	virtual Diffused2TexturedVertex* GetDiffuseTexturedVertex() { return diffused2TexturedVerices; }
	HeightMapLayer* GetLayer() { return &heightMapLayer[0]; }
	int GetVertexNum() { return verticesNum; }
	BoundingBox* GetBoundings() { return terrainBoundings; }
	int*		 GetBoudingVertexIndex() { return boundingVertexIndex; }
};

class NaviMesh : public Mesh {
	int width;
	int length;
	bool isCreate = false;
	XMFLOAT3 scale;
	std::vector<SubIndex> subIndex;
	std::vector<LeftIndex> leftIndex;
	std::vector<NaviNode> NaviNode;


	DiffusedVertex* diffuseVertex{ NULL };
public:
	~NaviMesh();
	void CreateNaviMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
		int width, int length,
		XMFLOAT3 scale,
		std::vector<BoundingOrientedBox>& isBuffer , XMFLOAT4X4 tinverseWorld , Diffused2TexturedVertex* terrainvertices , int (*naviBuffer)[257] , std::vector<NaviTile>& ntvec);
	void ReCreateNaviMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
		int width, int length,
		XMFLOAT3 scale,
		std::vector<BoundingOrientedBox>& isBuffer, XMFLOAT4X4 tinverseWorld, Diffused2TexturedVertex* terrainvertices , NaviMesh& fNavimesh , int(*naviBuffer)[257] , std::vector<NaviTile>& ntvec);

	bool GetisCreate() { return isCreate; }
	DiffusedVertex* GetDiffuseVertex() { return diffuseVertex; }
	std::vector<LeftIndex> GetLeftIndex() { return leftIndex; }
};


class StandardMesh : public Mesh
{
public:
	StandardMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual ~StandardMesh();

protected:
	UINT							vertexSize = 0;

	XMFLOAT4						*colors = NULL;
	XMFLOAT3						*normals = NULL;
	XMFLOAT3						*tangents = NULL;
	XMFLOAT3						*biTangents = NULL;
	XMFLOAT2						*textureCoords0 = NULL;
	XMFLOAT2						*textureCoords1 = NULL;

	ID3D12Resource					*textureCoord0Buffer = NULL;
	ID3D12Resource					*textureCoord0UploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		textureCoord0BufferView;

	ID3D12Resource					*textureCoord1Buffer = NULL;
	ID3D12Resource					*textureCoord1UploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		textureCoord1BufferView;

	ID3D12Resource					*normalBuffer = NULL;
	ID3D12Resource					*normalUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		normalBufferView;

	ID3D12Resource					*tangentBuffer = NULL;
	ID3D12Resource					*tangentUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		tangentBufferView;

	ID3D12Resource					*biTangentBuffer = NULL;
	ID3D12Resource					*biTangentUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		biTangentBufferView;

public:
	void LoadMeshFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, FILE *pInFile , BoundingBoxMesh*& boundingMesh , bool isSkined);

	virtual void ReleaseUploadBuffers();
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subSet);
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subsetNum, UINT instancesNum, std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBufferViews);
};

class TexturedCubeMesh : public Mesh {
private:
	TexturedVertex* texturedVertex;
public:
	TexturedCubeMesh(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList * pd3dCommandList, float fWidth, float fHeight, float fDepth);
	virtual ~TexturedCubeMesh();
};

class CubeMeshNormalMapTextured : public StandardMesh
{
public:
	CubeMeshNormalMapTextured(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width = 2.0f, float height = 2.0f, float depth = 2.0f);
	virtual ~CubeMeshNormalMapTextured();

	void CalculateTriangleListTBNs(int verticesNum, XMFLOAT3 *positions, XMFLOAT2 *texCoords, XMFLOAT3 *tangents, XMFLOAT3 *biTangents, XMFLOAT3 *normals);
};

class TexturedRectMesh : public StandardMesh
{
public:
	TexturedRectMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float fWidth = 20.0f, float fHeight = 20.0f, float fDepth = 20.0f, float fxPosition = 0.0f, float fyPosition = 0.0f, float fzPosition = 0.0f);
	virtual ~TexturedRectMesh();
};

class SkinnedMesh : public StandardMesh
{
public:
	SkinnedMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList);
	virtual ~SkinnedMesh();

protected:
	int								bonesPerVertex = 4;

	XMINT4							*boneIndices = NULL;
	XMFLOAT4						*boneWeights = NULL;

	ID3D12Resource					*boneIndexBuffer = NULL;
	ID3D12Resource					*boneIndexUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		boneIndexBufferView;

	ID3D12Resource					*boneWeightBuffer = NULL;
	ID3D12Resource					*boneWeightUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW		boneWeightBufferView;

public:
	int								skinningBones = 0;

	char(*skinningBoneNames)[64];
	GameObject						**skinningBoneFrameCaches = NULL; //[m_nSkinningBones]

	XMFLOAT4X4						*xmf4x4BindPoseBoneOffsets = NULL; //Transposed

	ID3D12Resource					*bindPoseBoneOffsets = NULL;
	XMFLOAT4X4						*mappedBindPoseBoneOffsets = NULL;

	ID3D12Resource					*skinningBoneTransforms = NULL;
	XMFLOAT4X4						*mappedSkinningBoneTransforms = NULL;

public:
	void PrepareSkinning(GameObject *ModelRootObject);
	void LoadSkinInfoFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, FILE *pInFile);

	virtual void CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList *commandList);
	virtual void ReleaseShaderVariables();

	virtual void ReleaseUploadBuffers();

	virtual void OnPreRender(ID3D12GraphicsCommandList *commandList, void *cotext);
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subSet);
	virtual void Render(ID3D12GraphicsCommandList *commandList, int subSet, UINT instancesNum, std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBufferViews);


};

