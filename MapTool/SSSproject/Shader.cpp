#include "stdafx.h"
#include "Shader.h"
#include "Scene.h"
#include "DDSTextureLoader12.h"


Shader::Shader()
{
}


Shader::~Shader()
{
	if (pipelineStates)
	{
		for (int i = 0; i < pipelineStatesNum; ++i)
			if (pipelineStates[i]) pipelineStates[i]->Release();
		delete[] pipelineStates;
	}
}

//래스터라이저 상태를 설정하기 위한 구조체를 반환한다.
D3D12_RASTERIZER_DESC Shader::CreateRasterizerState()
{
	//FIllMode와 CullMode를 변경하여 그려지는 객체의 내부를 칠할지 말지 등을 결정할 수 있다.
	D3D12_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_BACK;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = TRUE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(rasterizerDesc);
}


//깊이-스텐실 검사를 위한 상태를 설정하기 위한 구조체를 반환한다.
D3D12_DEPTH_STENCIL_DESC Shader::CreateDepthStencilState()
{
	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = TRUE;
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ALL;
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_LESS;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return(depthStencilDesc);
}

//블렌딩 상태를 설정하기 위한 구조체를 반환한다.
D3D12_BLEND_DESC Shader::CreateBlendState()
{
	D3D12_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = FALSE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = FALSE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ZERO;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(blendDesc);
}

//입력 조립기에게 정점 버퍼의 구조를 알려주기 위한 구조체를 반환한다.
D3D12_INPUT_LAYOUT_DESC Shader::CreateInputLayout()
{
	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = NULL;
	inputLayoutDesc.NumElements = 0;

	return(inputLayoutDesc);
}

//정점 셰이더 바이트 코드를 생성(컴파일)한다.
D3D12_SHADER_BYTECODE Shader::CreateVertexShader()
{
	D3D12_SHADER_BYTECODE shaderByteCode;
	shaderByteCode.BytecodeLength = 0;
	shaderByteCode.pShaderBytecode = NULL;

	return(shaderByteCode);
}
D3D12_SHADER_BYTECODE Shader::CreatePixelShader()
{
	D3D12_SHADER_BYTECODE shaderByteCode;
	shaderByteCode.BytecodeLength = 0;
	shaderByteCode.pShaderBytecode = NULL;

	return(shaderByteCode);
}
D3D12_SHADER_BYTECODE Shader::CreateGeometryShader()
{
	D3D12_SHADER_BYTECODE shaderByteCode;
	shaderByteCode.BytecodeLength = 0;
	shaderByteCode.pShaderBytecode = NULL;

	return(shaderByteCode);
}
D3D12_PRIMITIVE_TOPOLOGY_TYPE Shader::SetTopologyType()
{
	return D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
}
//셰이더 소스 코드를 컴파일하여 바이트 코드 구조체를 반환한다.
D3D12_SHADER_BYTECODE Shader::CompileShaderFromFile(const WCHAR *fileName, LPCSTR shaderName, LPCSTR shaderProfile, ID3DBlob** shaderBlob)
{
	UINT compileFlags = 0;

#if defined(_DEBUG)
	compileFlags = D3DCOMPILE_DEBUG | D3DCOMPILE_SKIP_OPTIMIZATION;
#endif
	ID3DBlob *errorBlob = NULL;

	HRESULT hResult = ::D3DCompileFromFile(fileName, NULL, D3D_COMPILE_STANDARD_FILE_INCLUDE, shaderName, shaderProfile, compileFlags, 0, shaderBlob, &errorBlob);
	/*
	선택 과목. 포함 파일을 처리 하기위한 ID3DInclude에 대한 포인터 입니다.
	이 값을 NULL로 설정하면 셰이더에 #include가 포함되어 있으면 컴파일 오류가 발생합니다.
	기본 포함 핸들러에 대한 포인터 인 D3D_COMPILE_STANDARD_FILE_INCLUDE 매크로를 전달할 수 있습니다.
	이 기본 포함 핸들러에는 현재 디렉토리와 관련된 파일 및 초기 소스 파일의 디렉토리와 관련된 파일이 포함됩니다.
	D3D_COMPILE_STANDARD_FILE_INCLUDE 를 사용 하는 경우 pSourceName 매개 변수 에 소스 파일 이름을 지정해야합니다.
	컴파일러는 pSourceName 에서 초기 상대 디렉토리를 파생 시킵니다 .
	*/

	D3D12_SHADER_BYTECODE shaderByteCode;
	shaderByteCode.BytecodeLength = (*shaderBlob)->GetBufferSize();
	shaderByteCode.pShaderBytecode = (*shaderBlob)->GetBufferPointer();

	return(shaderByteCode);
}

//그래픽스 파이프라인 상태 객체를 생성한다.
void Shader::CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	::ZeroMemory(&pipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	pipelineStateDesc.pRootSignature = graphicsRootSignature;
	pipelineStateDesc.VS = CreateVertexShader();
	pipelineStateDesc.GS = CreateGeometryShader();
	pipelineStateDesc.PS = CreatePixelShader();
	pipelineStateDesc.RasterizerState = CreateRasterizerState();
	pipelineStateDesc.BlendState = CreateBlendState();
	pipelineStateDesc.DepthStencilState = CreateDepthStencilState();
	pipelineStateDesc.InputLayout = CreateInputLayout();
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = SetTopologyType();
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;

	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[0]);


}

void Shader::OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx)
{
	if (pipelineStates)
		commandList->SetPipelineState(pipelineStates[idx]);

}
void Shader::Render(ID3D12GraphicsCommandList* commandList, Camera* camera)
{
	OnPrepareRender(commandList, 0);
}

void Shader::CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList* commandList, int idx)
{
}
void Shader::UpdateShaderVariables(ID3D12GraphicsCommandList* commandList)
{

}
void Shader::UpdateShaderVariable(ID3D12GraphicsCommandList* commandList, XMFLOAT4X4* world)
{
}
void Shader::ReleaseShaderVariables()
{
}

//////////////////////////////////////////////////////////////////////////////////////
// Use StandardShader based Shader when using model file
StandardShader::StandardShader()
{
}

StandardShader::~StandardShader()
{
}

D3D12_INPUT_LAYOUT_DESC StandardShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 5;
	D3D12_INPUT_ELEMENT_DESC *inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[2] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 2, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[3] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 3, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[4] = { "BITANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 4, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	return(inputLayoutDesc);
}

void StandardShader::Render(ID3D12GraphicsCommandList * commandList, Camera * camera)
{
	for (int j = 0; j < objectBuffer->objects.size(); j++)
	{
		if (objectBuffer->objects[j])
		{
			Shader::OnPrepareRender(commandList, objectBuffer->pipeLineStateIdx);

			objectBuffer->objects[j]->Render(commandList, camera, j);
		}
	}
}

D3D12_SHADER_BYTECODE StandardShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSStandard", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE StandardShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSStandard", "ps_5_1", &pixelShaderBlob));

}

void StandardShader::ReleaseShaderVariables()
{

	if (objectBuffer->cbGameObjects)
		for (int j = 0; j < objectBuffer->framesNum; ++j)
		{
			objectBuffer->cbGameObjects[j]->Unmap(0, NULL);
			objectBuffer->cbGameObjects[j]->Release();
		}
}


void StandardShader::CreateShader(ID3D12Device *device, ID3D12RootSignature *graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 2;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	Shader::CreateShader(device, graphicsRootSignature);

	pipelineStateDesc.BlendState.RenderTarget[0].BlendEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[1]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;

}

void StandardShader::CreateShaderVariables(ID3D12Device * pd3dDevice, ID3D12GraphicsCommandList * pd3dCommandList)
{
	objectBuffer->cbGameObjects = new ID3D12Resource*[objectBuffer->framesNum];
	objectBuffer->cbMappedGameObjects = new CB_GAMEOBJECT_INFO*[objectBuffer->framesNum];
	objectBuffer->cbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수

	for (int j = 0; j < objectBuffer->framesNum; ++j)
	{
		objectBuffer->cbGameObjects[j] = ::CreateBufferResource(pd3dDevice, pd3dCommandList, NULL, objectBuffer->cbGameObjectBytes * (UINT)objectBuffer->objectsNum, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		objectBuffer->cbGameObjects[j]->Map(0, NULL, (void **)&objectBuffer->cbMappedGameObjects[j]);
	}

}

void StandardShader::ReleaseObjects()
{
	for (int k = 0; k < objectBuffer->objects.size(); ++k)
	{
		if (objectBuffer->objects[k])
			objectBuffer->objects[k]->Release();
	}
}

void StandardShader::ReleaseUploadBuffers()
{
	for (int i = 0; i < objectBuffer->objects.size(); ++i)
	{
		if (objectBuffer->objects[i])
			objectBuffer->objects[i]->ReleaseUploadBuffers();
	}
}


void StandardShader::AnimateObjects(float timeElapsed, Camera* camera)
{
	for (int k = 0; k < 1; ++k)
	{
		for (int j = 0; j < objectBuffer->objects.size(); j++)
		{
			if (objectBuffer->objects[j])
			{
				objectBuffer->objects[j]->Animate(timeElapsed);
				objectBuffer->objects[j]->UpdateTransform(objectBuffer->cbMappedGameObjects, objectBuffer->cbGameObjectBytes, j, NULL);
			}
		}
	}


	for (int i = 0; i < objectBuffer->objects.size(); ++i) {
		//setBounding Box
		if (objectBuffer->objects[i]->GetBoxType() == false)
			XMFLOAT3 pos = objectBuffer->objects[i]->GetPosition();
		float fscale = objectBuffer->objects[i]->GetScale();
		XMFLOAT3 tempExtents = objectBuffer->objects[i]->GetOBB().Extents;

		XMFLOAT3 Extents{ tempExtents.x * fscale, tempExtents.y * fscale , tempExtents.z * fscale };
		objectBuffer->objects[i]->SetOBBPos(objectBuffer->objects[i]->GetPosition());
		objectBuffer->objects[i]->SetOBBOriginalExtent(Extents);
		objectBuffer->objects[i]->SetOBBBoundingBox();
	}
}

StandardInstanceShader::StandardInstanceShader()
{
}

StandardInstanceShader::~StandardInstanceShader()
{
}

D3D12_INPUT_LAYOUT_DESC StandardInstanceShader::CreateInputLayout()
{
	UINT inputElementDescsNum = 10;
	D3D12_INPUT_ELEMENT_DESC *inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];

	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[2] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 2, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[3] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 3, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	inputElementDescs[4] = { "BITANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 4, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	inputElementDescs[5] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[6] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[7] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[8] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[9] = { "ISDUMMY", 0, DXGI_FORMAT_R8_UINT, 5, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	D3D12_INPUT_LAYOUT_DESC inputLayoutDesc;
	inputLayoutDesc.pInputElementDescs = inputElementDescs;
	inputLayoutDesc.NumElements = inputElementDescsNum;

	return(inputLayoutDesc);
}

D3D12_SHADER_BYTECODE StandardInstanceShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSStandardInstancing", "vs_5_1", &vertexShaderBlob));
}

void StandardInstanceShader::CreateShader(ID3D12Device *device, ID3D12RootSignature *graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 2;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	Shader::CreateShader(device, graphicsRootSignature);

	pipelineStateDesc.BlendState.RenderTarget[0].BlendEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[1]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;

}

StandardComputeShader::StandardComputeShader()
{
}

StandardComputeShader::~StandardComputeShader()
{
	if (computePipelineStates)
	{
		for (int i = 0; i < (int)computePipelineStatesNum; ++i)
			if (computePipelineStates[i]) computePipelineStates[i]->Release();
		delete[] computePipelineStates;
	}
}

D3D12_SHADER_BYTECODE StandardComputeShader::CreateComputeShader()
{
	D3D12_SHADER_BYTECODE shaderByteCode;
	shaderByteCode.BytecodeLength = 0;
	shaderByteCode.pShaderBytecode = NULL;

	return(shaderByteCode);
}

SkinnedAnimationStandardShader::SkinnedAnimationStandardShader()
{
}

SkinnedAnimationStandardShader::~SkinnedAnimationStandardShader()
{
}

D3D12_INPUT_LAYOUT_DESC SkinnedAnimationStandardShader::CreateInputLayout()
{
	UINT nInputElementDescs = 8;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];

	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 1, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "NORMAL", 0, DXGI_FORMAT_R32G32B32_FLOAT, 2, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[3] = { "TANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 3, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[4] = { "BITANGENT", 0, DXGI_FORMAT_R32G32B32_FLOAT, 4, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[5] = { "BONEINDEX", 0, DXGI_FORMAT_R32G32B32A32_SINT, 5, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[6] = { "BONEWEIGHT", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 6, 0, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	pd3dInputElementDescs[7] = { "OBJECTIDX", 0, DXGI_FORMAT_R32_UINT, 7, 0, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_SHADER_BYTECODE SkinnedAnimationStandardShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSSkinnedAnimationStandard", "vs_5_1", &vertexShaderBlob));
}

