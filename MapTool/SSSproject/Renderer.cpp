#include "Renderer.h"



Renderer::Renderer()
{
}


void Renderer::RenderTerrainAlphaTexture(ID3D12Device * device, ID3D12GraphicsCommandList * commandList)
{
	commandList->OMSetRenderTargets(3, alphaTextureArr, FALSE, &terraindsvCPUDescriptorHandle);
	camera->SetViewport(0, 0, 256.0f , 256.0f, 0.0f, 1.0f);
	commandList->RSSetViewports(1, &camera->GetViewport());
	for (int i = 0; i < TERRAINLAYERNUM; ++i) {
		if (scene) scene->TerrainRender(commandList, camera, TERRAINALPHA, i);
	}
	commandList->ClearDepthStencilView(dsvCPUDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
	camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
	commandList->RSSetViewports(1, &camera->GetViewport());
}

void Renderer::ClearRtvDepthAlpha(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, D3D12_CPU_DESCRIPTOR_HANDLE rtvCPUDescriptorHandle ,float* RtvClearColor )
{
	float clearDepthColor[4] = { 1.0f, 1.0f, 1.0f , 1.0f };
	float clearAlphaColor[4] = { 0.0f , 0.0f , 0.0f , 0.0f };
	commandList->ClearRenderTargetView(depthRtvCPUDescriptorHandle, clearDepthColor, 0, NULL);
	commandList->ClearRenderTargetView(normalRtvCPUDescriptorHandle, clearDepthColor, 0, NULL);
	commandList->ClearRenderTargetView(alphaRtvCPUDescriptorHandle0, clearAlphaColor, 0, NULL);
	commandList->ClearRenderTargetView(alphaRtvCPUDescriptorHandle1, clearAlphaColor, 0, NULL);
	commandList->ClearRenderTargetView(alphaRtvCPUDescriptorHandle2, clearAlphaColor, 0, NULL);
	commandList->ClearRenderTargetView(heightRtvCPUDescriptorHandle, clearAlphaColor, 0, NULL);

	commandList->ClearRenderTargetView(rtvCPUDescriptorHandle, RtvClearColor , 0, NULL);
	//����-���ٽ� �������� CPU �ּҸ� ����Ѵ�.
	commandList->ClearDepthStencilView(dsvCPUDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
	commandList->ClearDepthStencilView(terraindsvCPUDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
	//���ϴ� ������ ����-���ٽ�(��)�� �����.
}

void Renderer::RenderTerrainDepthMap(ID3D12Device * device, ID3D12GraphicsCommandList * commandList)
{
	commandList->OMSetRenderTargets(1, &depthRtvCPUDescriptorHandle, TRUE, &dsvCPUDescriptorHandle);
	camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
	commandList->RSSetViewports(1, &camera->GetViewport());
	if (scene) scene->TerrainRender(commandList, camera, TERRAINDEPTH, TERRAINLAYERNUM);
	commandList->ClearDepthStencilView(dsvCPUDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
}

void Renderer::RenderTerrainNormalMap(ID3D12Device * device, ID3D12GraphicsCommandList * commandList)
{

	commandList->OMSetRenderTargets(1, &normalRtvCPUDescriptorHandle, TRUE, &dsvCPUDescriptorHandle);
	camera->SetViewport(0, 0, 256.0f, 256.0f, 0.0f, 1.0f);
	commandList->RSSetViewports(1, &camera->GetViewport());
	int isNormal = 1;
	commandList->SetGraphicsRoot32BitConstants(15, 1, &isNormal, 4);
	if (scene) scene->TerrainRender(commandList, camera, TERRAINLAYER, TERRAINLAYERNUM);
	commandList->ClearDepthStencilView(dsvCPUDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
	camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
	commandList->RSSetViewports(1, &camera->GetViewport());
}

void Renderer::RenderTerrainHeightMap(ID3D12Device * device, ID3D12GraphicsCommandList * commandList)
{
	commandList->OMSetRenderTargets(1, &heightRtvCPUDescriptorHandle, TRUE, &dsvCPUDescriptorHandle);
	camera->SetViewport(0, 0, 256.0f, 256.0f, 0.0f, 1.0f);
	commandList->RSSetViewports(1, &camera->GetViewport());

	for (int i = 0; i < TERRAINLAYERNUM; ++i) {
		if (scene) scene->TerrainRender(commandList, camera, TERRAINHEIGHT, i);
	}
	commandList->ClearDepthStencilView(dsvCPUDescriptorHandle, D3D12_CLEAR_FLAG_DEPTH | D3D12_CLEAR_FLAG_STENCIL, 1.0f, 0, 0, NULL);
	camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
	camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
	commandList->RSSetViewports(1, &camera->GetViewport());

}

void Renderer::OnPrepareRender(ID3D12GraphicsCommandList* commandList)
{
	if(scene) scene->PrepareRender(commandList);
}

void Renderer::RenderScene(ID3D12Device * device, ID3D12GraphicsCommandList * commandList , D3D12_CPU_DESCRIPTOR_HANDLE rtvCPUDescriptorHandle)
{

	commandList->OMSetRenderTargets(1, &rtvCPUDescriptorHandle, TRUE, &dsvCPUDescriptorHandle);
	if (scene) scene->SkyBoxRender(commandList, camera);
	int isNormal = 0;
	commandList->SetGraphicsRoot32BitConstants(15, 1, &isNormal, 4);
	if (scene) scene->TerrainRender(commandList, camera, TERRAINLAYER, TERRAINLAYERNUM);
	if (scene) scene->TerrainNaviRender(commandList, camera);

	if (scene) scene->Render(commandList, camera , terrainGUI->GetTerrainShapeFlags());
}

Renderer::~Renderer()
{
	delete[] alphaTextureArr;
	delete[] NormalTextureArr;
}
