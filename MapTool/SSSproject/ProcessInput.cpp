#include "ProcessInput.h"



ProcessInput::ProcessInput(Scene *scene, int guiNum, GUI ** gui, OptionGUI * optiongui, ObjectGUI* objectgui, TerrainGUI* terraingui ,BoundingGUI* boundinggui , Player* player , Picking * picking , Camera* camera , int width , int height ) : scene(scene) , guiNum(guiNum) , gui(gui)
, optiongui(optiongui) , objectgui(objectgui) , terraingui(terraingui),boundinggui(boundinggui) ,player(player), picking(picking) , camera(camera) , windowClientWidth(width) , windowClientHeight(height)
{
}

void ProcessInput::ProcessBassicCameraKeyboardInput( HWND hwnd, GameTimer & gameTimer)
{
	static UCHAR keyBuffer[256];
	DWORD direction = 0;

	if (::GetKeyboardState(keyBuffer))
	{
		if (keyBuffer[VK_UP] & 0xF0) direction |= DIR_FORWARD;
		if (keyBuffer[VK_DOWN] & 0xF0) direction |= DIR_BACKWARD;
		if (keyBuffer[VK_LEFT] & 0xF0) direction |= DIR_LEFT;
		if (keyBuffer[VK_RIGHT] & 0xF0) direction |= DIR_RIGHT;
		if (keyBuffer[VK_PRIOR] & 0xF0) direction |= DIR_UP;
		if (keyBuffer[VK_NEXT] & 0xF0) direction |= DIR_DOWN;
	}
	float deltaX = 0.0f, deltaY = 0.0f;
	POINT cursorPos;
	if (::GetCapture() == hwnd)
	{
		::GetCursorPos(&cursorPos);
		deltaX = (float)(cursorPos.x - oldCursorPos.x) / 3.0f;
		deltaY = (float)(cursorPos.y - oldCursorPos.y) / 3.0f;
		//마우스 커서의 위치를 마우스가 눌려졌던 위치로 설정한다.
		//GUI가 켜져있으면 막아야한다. 
		::SetCursorPos(oldCursorPos.x, oldCursorPos.y);
	}
	if ((direction != 0) || (deltaX != 0.0f) || (deltaY != 0.0f))
	{
		if (deltaX || deltaY)
		{
			
			if (keyBuffer[VK_RBUTTON] & 0xF0)
				player->Rotate(deltaY, 0.0f, -deltaX);
			else
				player->Rotate(deltaY, deltaX, 0.0f);
		}
		
		if (direction) player->Move(direction, cameraSpeed * gameTimer.GetTimeElapsed() ,true);
	}

	//플레이어를 실제로 이동하고 카메라를 갱신한다. 중력과 마찰력의 영향을 속도 벡터에 적용한다.
	player->Update(gameTimer.GetTimeElapsed());
}

void ProcessInput::ProcessChangeTerrain(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, HWND hwnd, GameTimer & gameTimer)
{
	static UCHAR keyBuffer[256];
	DWORD direction = 0;

	if (::GetKeyboardState(keyBuffer))
	{
		if (keyBuffer[VK_UP] & 0xF0) direction |= DIR_FORWARD;
		if (keyBuffer[VK_DOWN] & 0xF0) direction |= DIR_BACKWARD;
		if (keyBuffer[VK_LEFT] & 0xF0) direction |= DIR_LEFT;
		if (keyBuffer[VK_RIGHT] & 0xF0) direction |= DIR_RIGHT;
		if (keyBuffer[VK_PRIOR] & 0xF0) direction |= DIR_UP;
		if (keyBuffer[VK_NEXT] & 0xF0) direction |= DIR_DOWN;
		if (keyBuffer[0x51] & 0xF0 || keyBuffer[0x71] & 0xF0) {
			gui[TERRAINGUI]->useShortCut('q');
			gui[OPTIONGUI]->useShortCut('n');
		}
		if (keyBuffer[0x57] & 0xF0 || keyBuffer[0x77] & 0xF0) {
			gui[TERRAINGUI]->useShortCut('w');
			gui[OPTIONGUI]->useShortCut('m');
		};
		if (keyBuffer[0x45] & 0xF0 || keyBuffer[0x65] & 0xF0) {
			gui[TERRAINGUI]->useShortCut('e');
			gui[OPTIONGUI]->useShortCut('m');
		}
		if (keyBuffer[0x52] & 0xF0 || keyBuffer[0x72] & 0xF0) {
			gui[TERRAINGUI]->useShortCut('r');
			gui[OPTIONGUI]->useShortCut('m');
		}
		if (keyBuffer[0x54] & 0xF0 || keyBuffer[0x74] & 0xF0) {
			gui[TERRAINGUI]->useShortCut('t');
			gui[OPTIONGUI]->useShortCut('n');
		}
		if (keyBuffer[0x41] & 0xF0 || keyBuffer[0x61] & 0xF0) {
			gui[TERRAINGUI]->useShortCut('a');
			gui[OPTIONGUI]->useShortCut('m');
		}
		if (keyBuffer[0x53] & 0xF0 || keyBuffer[0x73] & 0xF0) {
			gui[TERRAINGUI]->useShortCut('s');
			gui[OPTIONGUI]->useShortCut('m');
		}
		if (keyBuffer[0x64] & 0xF0 || keyBuffer[0x44] & 0xF0) {
			gui[TERRAINGUI]->useShortCut('d');
			gui[OPTIONGUI]->useShortCut('m');
		}
		if (keyBuffer[0x66] & 0xF0 || keyBuffer[0x46] & 0xF0) {
			gui[TERRAINGUI]->useShortCut('f');
			gui[OPTIONGUI]->useShortCut('n');
		}
	}
	float deltaX = 0.0f, deltaY = 0.0f;
	POINT cursorPos;

	::GetCursorPos(&cursorPos);
	auto tempCursorPos = cursorPos;

	ScreenToClient(hwnd, &tempCursorPos);
	if (::GetCapture() == hwnd)
	{
		if (!optiongui->Getmodifygui()) {
			deltaX = (float)(cursorPos.x - oldCursorPos.x) / 3.0f;
			deltaY = (float)(cursorPos.y - oldCursorPos.y) / 3.0f;
			//마우스 커서의 위치를 마우스가 눌려졌던 위치로 설정한다.
			//GUI가 켜져있으면 막아야한다. 
			::SetCursorPos(oldCursorPos.x, oldCursorPos.y);
		}
	}
	if (gui[TERRAINGUI]->RadioButtonState() == UPTERRAINHEIGHT || gui[TERRAINGUI]->RadioButtonState() == DOWNTERRAINHEIGHT || gui[TERRAINGUI]->RadioButtonState() == SPLATTING 
		|| gui[TERRAINGUI]->RadioButtonState() == FLATTERRAINHEIGHT || gui[TERRAINGUI]->RadioButtonState() == SMOOTHING) {
			//사각형 메쉬 
			std::vector<GameObject*> gameObject = scene->GetShaders()[SCREENSPACEDECALSHADER]->GetGameObjects();
			float nearDistance = FLT_MAX;
			HeightMapTerrain* terrain = scene->GetTerrain();
			XMFLOAT3 pickingPoint{ 0,0,0 };
			int bIdx = FAILPICKING;
			if (ProcessTerrainPicking(terrain, pickingPoint, tempCursorPos, nearDistance , bIdx)) {
				gameObject[0]->SetPosition(pickingPoint.x, pickingPoint.y, pickingPoint.z);
			}
			if (buttonDown) {
				if (bIdx != FAILPICKING) {
					if(gui[TERRAINGUI]->RadioButtonState() == UPTERRAINHEIGHT || gui[TERRAINGUI]->RadioButtonState() == DOWNTERRAINHEIGHT || gui[TERRAINGUI]->RadioButtonState() == FLATTERRAINHEIGHT
						|| gui[TERRAINGUI]->RadioButtonState() == SMOOTHING)
						ProcessChangeTerrainHeight(terrain, pickingPoint , bIdx, gameTimer.GetTimeElapsed() , terraingui->GetCircleScale() /2 , terraingui->RadioButtonState() , terraingui->GetMaxHeight() , terraingui->GetMinHeight());
					if (gui[TERRAINGUI]->RadioButtonState() == SPLATTING) {
						ProcessSplattingTextureTerrain(device ,commandList,terrain, pickingPoint, bIdx, gameTimer.GetTimeElapsed(), terraingui->GetCircleScale() / 2 , terraingui->GetSplittingTextureNumber());
					}
				}
				
			}
	}

	if (gui[TERRAINGUI]->RadioButtonState() == CONTROLCAMERA) {
		if ((direction != 0) || (deltaX != 0.0f) || (deltaY != 0.0f))
		{
			if (deltaX || deltaY)
			{

				if (keyBuffer[VK_RBUTTON] & 0xF0)
					player->Rotate(deltaY, 0.0f, -deltaX);
				else
					player->Rotate(deltaY, deltaX, 0.0f);
			}

			if (direction) player->Move(direction, cameraSpeed * gameTimer.GetTimeElapsed(), true);
		}
	}

	//플레이어를 실제로 이동하고 카메라를 갱신한다. 중력과 마찰력의 영향을 속도 벡터에 적용한다.
	player->Update(gameTimer.GetTimeElapsed());

}

void ProcessInput::ProcessObjectEditInput(ID3D12Device* device, ID3D12GraphicsCommandList* commandList , HWND hwnd  ,  GameTimer& gameTimer)
{
	static UCHAR keyBuffer[256];
	DWORD direction = 0;
	/*키보드의 상태 정보를 반환한다. 화살표 키(‘→’, ‘←’, ‘↑’, ‘↓’)를 누르면 플레이어를 오른쪽/왼쪽(로컬 x-축), 앞/
	뒤(로컬 z-축)로 이동한다. ‘Page Up’과 ‘Page Down’ 키를 누르면 플레이어를 위/아래(로컬 y-축)로 이동한다.*/
	if (::GetKeyboardState(keyBuffer))
	{
		if (keyBuffer[VK_UP] & 0xF0) direction |= DIR_FORWARD;
		if (keyBuffer[VK_DOWN] & 0xF0) direction |= DIR_BACKWARD;
		if (keyBuffer[VK_LEFT] & 0xF0) direction |= DIR_LEFT;
		if (keyBuffer[VK_RIGHT] & 0xF0) direction |= DIR_RIGHT;
		if (keyBuffer[VK_PRIOR] & 0xF0) direction |= DIR_UP;
		if (keyBuffer[VK_NEXT] & 0xF0) direction |= DIR_DOWN;
		if (keyBuffer[0x51] & 0xF0 || keyBuffer[0x71] & 0xF0) {
			gui[OBJECTGUI]->useShortCut('q');
			gui[OPTIONGUI]->useShortCut('n');
		}
		if (keyBuffer[0x57] & 0xF0 || keyBuffer[0x77] & 0xF0) {
			gui[OBJECTGUI]->useShortCut('w');
			gui[OPTIONGUI]->useShortCut('m');
		};
		if (keyBuffer[0x45] & 0xF0 || keyBuffer[0x65] & 0xF0) {
			gui[OBJECTGUI]->useShortCut('e');
			gui[OPTIONGUI]->useShortCut('m');
		}
		if (keyBuffer[0x52] & 0xF0 || keyBuffer[0x72] & 0xF0) {
			gui[OBJECTGUI]->useShortCut('r');
			gui[OPTIONGUI]->useShortCut('m');
		}
		if (keyBuffer[0x44] & 0xF0 || keyBuffer[0x64] & 0xF0) {
			gui[OBJECTGUI]->useShortCut('d');
			gui[OPTIONGUI]->useShortCut('m');
		}
	}
	float deltaX = 0.0f, deltaY = 0.0f;
	POINT cursorPos;
	/*마우스를 캡쳐했으면 마우스가 얼마만큼 이동하였는 가를 계산한다. 마우스 왼쪽 또는 오른쪽 버튼이 눌러질 때의
	메시지(WM_LBUTTONDOWN, WM_RBUTTONDOWN)를 처리할 때 마우스를 캡쳐하였다. 그러므로 마우스가 캡쳐된
	것은 마우스 버튼이 눌려진 상태를 의미한다. 마우스 버튼이 눌려진 상태에서 마우스를 좌우 또는 상하로 움직이면 플
	레이어를 x-축 또는 y-축으로 회전한다.*/
	if (::GetCapture() == hwnd)
	{
		//마우스 커서를 화면에서 없앤다(보이지 않게 한다).
		//::SetCursor(NULL);
		//현재 마우스 커서의 위치를 가져온다.
		::GetCursorPos(&cursorPos);
		auto tempCursorPos = cursorPos;

		//스크린 좌표계로 변환
		ScreenToClient(hwnd, &tempCursorPos);

		//마우스 버튼이 눌린 상태에서 마우스가 움직인 양을 구한다.
		if (!optiongui->Getmodifygui()) {
			deltaX = (float)(cursorPos.x - oldCursorPos.x) / 3.0f;
			deltaY = (float)(cursorPos.y - oldCursorPos.y) / 3.0f;
			//마우스 커서의 위치를 마우스가 눌려졌던 위치로 설정한다.
			//GUI가 켜져있으면 막아야한다. 
			::SetCursorPos(oldCursorPos.x, oldCursorPos.y);
		}

	
		std::vector<GameObject*> assistShaderObject = scene->GetShaders()[ASSISTSHADER]->GetGameObjects();
		if (objectgui->RadioButtonState() == CONTROLCAMERA) {
			picking->Setpicking(false);
		}
		int oldobjectIndex{ FAILPICKING };
		int objectIndex{ FAILPICKING };
		int BufferIndex{ FAILPICKING };
		int shaderIndex{ FAILPICKING };
		float nearDistance = FLT_MAX;
		Shader** shaders = scene->GetShaders();
		if (picking->Getpicking() && objectgui->RadioButtonState() == MOVEOBJECT) {
			//화살표 먼저 수행
			ProcessArrowPicking(objectIndex, tempCursorPos, nearDistance);
		}
		if (objectIndex == FAILPICKING) {
			//지금은 인스턴싱 쉐이더만 검사한다. 
			for (int i = 0; i < shaders[INSTANCSHADER]->GetBuffer().size(); ++i) {
				std::vector<GameObject*> objects = shaders[INSTANCSHADER]->GetGameObjectIdx(i);
				ProcessObjectPicking(objects, objectIndex, tempCursorPos, nearDistance);
				if (oldobjectIndex != objectIndex) {

					oldobjectIndex = objectIndex;
					BufferIndex = i;
					shaderIndex = INSTANCSHADER;
				}
			}
			for (int i = 0; i < shaders[SKINEDINSTANCSHADER]->GetSkinedBuffer().size(); ++i) {
				std::vector<GameObject*> objects = shaders[SKINEDINSTANCSHADER]->GetGameObjectIdx(i);
				ProcessObjectPicking(objects, objectIndex, tempCursorPos, nearDistance);
				if (oldobjectIndex != objectIndex) {

					oldobjectIndex = objectIndex;
					BufferIndex = i;
					shaderIndex = SKINEDINSTANCSHADER;
				}
			}
		}
		if (objectIndex == FAILPICKING && picking->Getpicking()) {
			//오브젝트까지 피킹되는 물체가 없다면 화살표를 멀리 보내버림
			HideArrowObject(assistShaderObject);
			BufferIndex = FAILPICKING;
		}
		else {
			//피킹 성공시 화살표 위치 변경 
			if (objectgui->RadioButtonState() == MOVEOBJECT) {
				objectgui->SetPickObjectImformation(BufferIndex, objectIndex, picking->GetOldObject()); // 현제 피킹된 객체의 정보 저장 
				//화살표 위치 설정 
				if (picking->GetSelectObject()->GetObjectType() != ARROWTYPE) {
					picking->SetArrowsPosition(assistShaderObject[LOOKARROW], LOOKARROW);
					picking->SetArrowsPosition(assistShaderObject[RIGHTARROW], RIGHTARROW);
					picking->SetArrowsPosition(assistShaderObject[UPARRROW], UPARRROW);
				}
				if (picking->GetSelectObject()->GetObjectType() == ARROWTYPE) {
					//객체 움직이기 
					deltaX = (float)(cursorPos.x - pickCursorPos.x) / 2.0f;
					deltaY = (float)(cursorPos.y - pickCursorPos.y) / 2.0f;
					pickCursorPos.x = cursorPos.x;
					pickCursorPos.y = cursorPos.y;
					picking->MoveObjectToDirection(assistShaderObject, deltaX, deltaY);
				}

			}
			else if (objectgui->RadioButtonState() == ROTATEOBJECT) {
				objectgui->SetPickObjectImformation(BufferIndex, objectIndex, picking->GetOldObject());
				HideArrowObject(assistShaderObject);
			}
			else if (objectgui->RadioButtonState() == SCALEOBJECT) {
				objectgui->SetPickObjectImformation(BufferIndex, objectIndex, picking->GetOldObject());
				HideArrowObject(assistShaderObject);
			}
			else if (objectgui->RadioButtonState() == DELETEOBJECT) {
				//오브젝트를 삭제해야한다. 
				if (buttonDown) {
					shaders[shaderIndex]->DeleteGameObject(device, commandList, BufferIndex, objectIndex);
					HideArrowObject(assistShaderObject);
					buttonDown = false;
				}
			}

		}
	}


	//마우스 또는 키 입력이 있으면 플레이어를 이동하거나(dwDirection) 회전한다(cxDelta 또는 cyDelta).
	if (gui[OBJECTGUI]->RadioButtonState() == CONTROLCAMERA) {
		if ((direction != 0) || (deltaX != 0.0f) || (deltaY != 0.0f))
		{
			if (deltaX || deltaY)
			{
				/*deltaX는 y-축의 회전을 나타내고 deltaY는 x-축의 회전을 나타낸다. 오른쪽 마우스 버튼이 눌려진 경우
				cxDelta는 z-축의 회전을 나타낸다.*/
				if (keyBuffer[VK_RBUTTON] & 0xF0)
					player->Rotate(deltaY, 0.0f, -deltaX);
				else
					player->Rotate(deltaY, deltaX, 0.0f);
			}
			/*플레이어를 dwDirection 방향으로 이동한다(실제로는 속도 벡터를 변경한다). 이동 거리는 시간에 비례하도록 한다.
			플레이어의 이동 속력은 (50/초)로 가정한다.*/
			if (direction) player->Move(direction, cameraSpeed * gameTimer.GetTimeElapsed(),
				true);
		}
	}
	//플레이어를 실제로 이동하고 카메라를 갱신한다. 중력과 마찰력의 영향을 속도 벡터에 적용한다.
	player->Update(gameTimer.GetTimeElapsed());
}

void ProcessInput::ProcessObjectNewInput(HWND hwnd, GameTimer& gameTimer , ID3D12Device* device, ID3D12GraphicsCommandList* commandList, bool& dummyFlag)
{
	static UCHAR keyBuffer[256];
	DWORD direction = 0;

	if (::GetKeyboardState(keyBuffer))
	{
		if (keyBuffer[VK_UP] & 0xF0) direction |= DIR_FORWARD;
		if (keyBuffer[VK_DOWN] & 0xF0) direction |= DIR_BACKWARD;
		if (keyBuffer[VK_LEFT] & 0xF0) direction |= DIR_LEFT;
		if (keyBuffer[VK_RIGHT] & 0xF0) direction |= DIR_RIGHT;
		if (keyBuffer[VK_PRIOR] & 0xF0) direction |= DIR_UP;
		if (keyBuffer[VK_NEXT] & 0xF0) direction |= DIR_DOWN;
		if (keyBuffer[0x51] & 0xF0 || keyBuffer[0x71] & 0xF0) {
			gui[OBJECTGUI]->useShortCut('q');
			gui[OPTIONGUI]->useShortCut('n');
		}
		if (keyBuffer[0x57] & 0xF0 || keyBuffer[0x77] & 0xF0) {
			gui[OBJECTGUI]->useShortCut('w');
			gui[OPTIONGUI]->useShortCut('m');
		};
	}
	float deltaX = 0.0f, deltaY = 0.0f;
	POINT cursorPos;

	::GetCursorPos(&cursorPos);
	auto tempCursorPos = cursorPos;

	ScreenToClient(hwnd, &tempCursorPos);
	if (::GetCapture() == hwnd)
	{
		
		if (!optiongui->Getmodifygui()) {
			deltaX = (float)(cursorPos.x - oldCursorPos.x) / 3.0f;
			deltaY = (float)(cursorPos.y - oldCursorPos.y) / 3.0f;
			//마우스 커서의 위치를 마우스가 눌려졌던 위치로 설정한다.
			//GUI가 켜져있으면 막아야한다. 
			::SetCursorPos(oldCursorPos.x, oldCursorPos.y);
		}

	}
	if (objectgui->RadioButtonState() == LOCATEOBJECT) {
		//피킹지점을 구하고 오브젝트를 배치한다. 마우스로 한번 눌렀을때만 배치한다. 
		float nearDistance = FLT_MAX;
		HeightMapTerrain* terrain = scene->GetTerrain();
		XMFLOAT3 pickingPoint{ 0,0,0 };
		int bIdx = FAILPICKING;
		if (ProcessTerrainPicking(terrain, pickingPoint, tempCursorPos, nearDistance, bIdx)) {
			ObjectInform objectType = objectgui->GetLocatedObjectType();
			if (objectType.objectIdx != FAILPICKING && objectType.shaderIdx != FAILPICKING)
			{
				Shader** shaders = scene->GetShaders();
				GameObject* temp;
				if (objectType.shaderIdx == INSTANCSHADER) {
					++objectType.objectIdx;
					temp = shaders[objectType.shaderIdx]->GetGameObject(objectType.objectIdx , 0);
					objectgui->SetDummy(temp);
				}
				if (objectType.shaderIdx == SKINEDINSTANCSHADER) {
					temp = shaders[objectType.shaderIdx]->GetGameObject(objectType.objectIdx, 0);
					objectgui->SetDummy(temp);
				}
				temp->SetPosition(pickingPoint);
				if (oldObjectIdx != objectType.objectIdx || oldShaderIdx != objectType.shaderIdx) {
					dummyFlag = false;
					oldObjectIdx = objectType.objectIdx;
					oldShaderIdx = objectType.shaderIdx;
				}

				if (buttonDown) {
					buttonDown = false;
				

					shaders[objectType.shaderIdx]->AddGameObject(device, commandList, pickingPoint, scene->GetGraphicsRootSignature(), objectType.objectIdx , objectgui->GetNoise());
				}
			}
		}
	}

	if (gui[OBJECTGUI]->RadioButtonState() == 0) {
		if ((direction != 0) || (deltaX != 0.0f) || (deltaY != 0.0f))
		{
			if (deltaX || deltaY)
			{
				/*deltaX는 y-축의 회전을 나타내고 deltaY는 x-축의 회전을 나타낸다. 오른쪽 마우스 버튼이 눌려진 경우
				cxDelta는 z-축의 회전을 나타낸다.*/
				if (keyBuffer[VK_RBUTTON] & 0xF0)
					player->Rotate(deltaY, 0.0f, -deltaX);
				else
					player->Rotate(deltaY, deltaX, 0.0f);
			}
			/*플레이어를 dwDirection 방향으로 이동한다(실제로는 속도 벡터를 변경한다). 이동 거리는 시간에 비례하도록 한다.
			플레이어의 이동 속력은 (50/초)로 가정한다.*/
			if (direction) player->Move(direction, cameraSpeed * gameTimer.GetTimeElapsed(),
				true);
		}
	}

	player->Update(gameTimer.GetTimeElapsed());
}

void ProcessInput::ProcessArrowPicking(int& index, POINT& tempCursorPos, float& nearDistance)
{
	std::vector<GameObject*> tempObjects = scene->GetShaders()[ASSISTSHADER]->GetGameObjects();
	size_t tempNum = tempObjects.size();
	int tempindex = -1;
	tempindex = picking->CheckPickingObjects(tempCursorPos, *camera, windowClientWidth, windowClientHeight, tempObjects, tempNum, nearDistance , index);
	if (index != FAILPICKING) {
		picking->SetSelectObject(tempObjects[index]);
		picking->SetSelectIndex(index);
	}
}

void ProcessInput::ProcessObjectPicking(std::vector<GameObject*> objects, int & index, POINT & tempCursorPos, float & nearDistance)
{
	if (picking->Getpicking()) {
		size_t tempNum = objects.size();
		int tempIndex;
		tempIndex = picking->CheckPickingObjects(tempCursorPos, *camera, windowClientWidth, windowClientHeight, objects, tempNum, nearDistance , index);
		if (tempIndex != FAILPICKING) {
			picking->SetOldObject(objects[index]);
			picking->SetSelectObject(objects[index]);
			picking->SetSelectIndex(index);
		}
	}
}

bool ProcessInput::ProcessTerrainPicking(GameObject* terrain, XMFLOAT3& PickingPoint, POINT & tempCursorPos, float & nearDistance , int& bIdx)
{	
	bool isPicking{ false };
	if (picking->Getpicking())
		isPicking = picking->CheckPickingTerrain(tempCursorPos, *camera, windowClientWidth, windowClientHeight, PickingPoint, *terrain, nearDistance , bIdx);

	return isPicking;

}

void ProcessInput::ProcessChangeTerrainHeight(HeightMapTerrain * terrain, XMFLOAT3 PickingPoint , int bIdx , float frameTime ,float CircleSize, int RadioFlags, float maxHeight, float minHeight)
{

	if (RadioFlags == UPTERRAINHEIGHT || RadioFlags == DOWNTERRAINHEIGHT || RadioFlags == FLATTERRAINHEIGHT)
	{
		XMFLOAT3 localPickingPoint = Vector3::TransformCoord(PickingPoint, terrain->GetInverseWorld());
		HeightMapGridMesh* terrainMesh = static_cast<HeightMapGridMesh*>(terrain->GetMesh(0));
		float flatHeight = terraingui->GetFlatHeight();
		int shape = terraingui->GetTerrainShapeFlags();


		std::vector<int> bindices;
		//여러개의 바운딩 박스와 체크해야함 
		if (RadioFlags == UPTERRAINHEIGHT || RadioFlags == DOWNTERRAINHEIGHT || RadioFlags == FLATTERRAINHEIGHT) {
			if (shape == CIRCLE) {
				auto tempbs = BoundingSphere(localPickingPoint, CircleSize);
				for (int i = 0; i < TERRAINBOUNDINGS; ++i) {
					if (tempbs.Intersects(terrainMesh->GetBoundings()[i])) {
						bindices.emplace_back(terrainMesh->GetBoudingVertexIndex()[i]);
					}
				}
			}
			if (shape == SQURE || shape == NOISE) {
				auto tempb = BoundingBox(localPickingPoint, XMFLOAT3(CircleSize , CircleSize , CircleSize) );
				for (int i = 0; i < TERRAINBOUNDINGS; ++i) {
					if (tempb.Intersects(terrainMesh->GetBoundings()[i])) {
						bindices.emplace_back(terrainMesh->GetBoudingVertexIndex()[i]);
					}
				}
			}
		}
		terrainMesh->ChangeVertexHeight(localPickingPoint, frameTime, CircleSize, RadioFlags, maxHeight, minHeight, flatHeight, bindices, shape);

	}
	if (RadioFlags == SMOOTHING) {
		XMFLOAT3 localPickingPoint = Vector3::TransformCoord(PickingPoint, terrain->GetInverseWorld());
		HeightMapGridMesh* terrainMesh = static_cast<HeightMapGridMesh*>(terrain->GetMesh(0));

		std::vector<int> bindices;
		//여러개의 바운딩 박스와 체크해야함 
		

			auto tempbs = BoundingSphere(localPickingPoint, CircleSize);
			for (int i = 0; i < TERRAINBOUNDINGS; ++i) {
				if (tempbs.Intersects(terrainMesh->GetBoundings()[i])) {
					bindices.emplace_back(terrainMesh->GetBoudingVertexIndex()[i]);
				}
			}
			terrainMesh->SmoothingVertexHeight(localPickingPoint, frameTime, CircleSize, RadioFlags, bindices);
		
	}



	//모든 오브젝트를 검사하여 범위 안에 들면 높이값을 바꿔준다. 
	Shader** shaders = scene->GetShaders();
	XMFLOAT2 Lengthxy;
	std::vector<InstanceBuffer*> buffer = shaders[INSTANCSHADER]->GetBuffer();
	for (int j = 0; j < buffer.size(); ++j) {
		std::vector<GameObject*> objects = buffer[j]->objects;
		for (int i = 0; i < objects.size(); ++i) {
			XMFLOAT3 pos = objects[i]->GetPosition();
			Lengthxy.x = pos.x - PickingPoint.x;
			Lengthxy.y = pos.z - PickingPoint.z;
			float Length = sqrt((Lengthxy.x * Lengthxy.x) + (Lengthxy.y * Lengthxy.y));
			if (Length < CircleSize) {
				float height = terrain->GetHeightRunTime(objects[i]->GetPosition());
				objects[i]->SetPosition(pos.x, height, pos.z);
				objects[i]->GetOBB().Center = XMFLOAT3(pos.x, height, pos.z);
			}
		}
	}
	

}
void ProcessInput::ProcessSplattingTextureTerrain(ID3D12Device * device, ID3D12GraphicsCommandList * commandList , GameObject * objects, XMFLOAT3 PickingPoint, int bIdx, float frameTime, float CircleSize , int textureNumber)
{
	if (textureNumber == NONESELECT)
		return;

	HeightMapTerrain* terrain = scene->GetTerrain();
	XMFLOAT3 localPickingPoint = Vector3::TransformCoord(PickingPoint, scene->GetTerrain()->GetInverseWorld());
	Mesh* terrainMesh = terrain->GetMesh(0);
	terrainMesh->ChangeVertexTexture(device, commandList,localPickingPoint, frameTime, CircleSize , textureNumber , bIdx);
}

void ProcessInput::ProcessLightInput(HWND hwnd, GameTimer & gameTimer)
{
	static UCHAR keyBuffer[256];
	DWORD direction = 0;

	if (::GetKeyboardState(keyBuffer))
	{
		if (keyBuffer[VK_UP] & 0xF0) direction |= DIR_FORWARD;
		if (keyBuffer[VK_DOWN] & 0xF0) direction |= DIR_BACKWARD;
		if (keyBuffer[VK_LEFT] & 0xF0) direction |= DIR_LEFT;
		if (keyBuffer[VK_RIGHT] & 0xF0) direction |= DIR_RIGHT;
		if (keyBuffer[VK_PRIOR] & 0xF0) direction |= DIR_UP;
		if (keyBuffer[VK_NEXT] & 0xF0) direction |= DIR_DOWN;
		if (keyBuffer[0x51] & 0xF0 || keyBuffer[0x71] & 0xF0) {
			gui[LIGHTGUI]->useShortCut('q');
			gui[LIGHTGUI]->useShortCut('n');
		}
		if (keyBuffer[0x57] & 0xF0 || keyBuffer[0x77] & 0xF0) {
			gui[LIGHTGUI]->useShortCut('w');
			gui[LIGHTGUI]->useShortCut('n');
		};
		if (keyBuffer[0x45] & 0xF0 || keyBuffer[0x65] & 0xF0) {
			gui[LIGHTGUI]->useShortCut('e');
			gui[OPTIONGUI]->useShortCut('n');
		}
		if (keyBuffer[0x52] & 0xF0 || keyBuffer[0x72] & 0xF0) {
			gui[LIGHTGUI]->useShortCut('r');
			gui[OPTIONGUI]->useShortCut('n');
		}
	
	}
	float deltaX = 0.0f, deltaY = 0.0f;
	POINT cursorPos;
	if (::GetCapture() == hwnd)
	{
		::GetCursorPos(&cursorPos);
		deltaX = (float)(cursorPos.x - oldCursorPos.x) / 3.0f;
		deltaY = (float)(cursorPos.y - oldCursorPos.y) / 3.0f;
		//마우스 커서의 위치를 마우스가 눌려졌던 위치로 설정한다.
		//GUI가 켜져있으면 막아야한다. 
		::SetCursorPos(oldCursorPos.x, oldCursorPos.y);
	}
	if ((direction != 0) || (deltaX != 0.0f) || (deltaY != 0.0f))
	{
		if (deltaX || deltaY)
		{

			if (keyBuffer[VK_RBUTTON] & 0xF0)
				player->Rotate(deltaY, 0.0f, -deltaX);
			else
				player->Rotate(deltaY, deltaX, 0.0f);
		}

		if (direction) player->Move(direction, cameraSpeed * gameTimer.GetTimeElapsed(), true);
	}

	//플레이어를 실제로 이동하고 카메라를 갱신한다. 중력과 마찰력의 영향을 속도 벡터에 적용한다.
	player->Update(gameTimer.GetTimeElapsed());

}

void ProcessInput::ProcessParticleInput(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, HWND hwnd , GameTimer& gameTimer)
{
	static UCHAR keyBuffer[256];
	DWORD direction = 0;
	Shader** shaders = scene->GetShaders();
	if (::GetKeyboardState(keyBuffer))
	{
		if (keyBuffer[VK_UP] & 0xF0) direction |= DIR_FORWARD;
		if (keyBuffer[VK_DOWN] & 0xF0) direction |= DIR_BACKWARD;
		if (keyBuffer[VK_LEFT] & 0xF0) direction |= DIR_LEFT;
		if (keyBuffer[VK_RIGHT] & 0xF0) direction |= DIR_RIGHT;
		if (keyBuffer[VK_PRIOR] & 0xF0) direction |= DIR_UP;
		if (keyBuffer[VK_NEXT] & 0xF0) direction |= DIR_DOWN;
		if (keyBuffer[0x51] & 0xF0 || keyBuffer[0x71] & 0xF0) {
			gui[PARTICLEGUI]->useShortCut('q');
			gui[OPTIONGUI]->useShortCut('n');
		}
		if (keyBuffer[0x57] & 0xF0 || keyBuffer[0x77] & 0xF0) {
			gui[PARTICLEGUI]->useShortCut('w');
			gui[OPTIONGUI]->useShortCut('n');
		};
		if (keyBuffer[0x45] & 0xF0 || keyBuffer[0x65] & 0xF0) {
			gui[PARTICLEGUI]->useShortCut('e');
			gui[OPTIONGUI]->useShortCut('n');
		}
	}
	float deltaX = 0.0f, deltaY = 0.0f;
	POINT cursorPos;
	if (::GetCapture() == hwnd)
	{

		::GetCursorPos(&cursorPos);
		auto tempCursorPos = cursorPos;
		//스크린 좌표계로 변환
		ScreenToClient(hwnd, &tempCursorPos);


		if (!optiongui->Getmodifygui()) {
			deltaX = (float)(cursorPos.x - oldCursorPos.x) / 3.0f;
			deltaY = (float)(cursorPos.y - oldCursorPos.y) / 3.0f;
			//마우스 커서의 위치를 마우스가 눌려졌던 위치로 설정한다.
			//GUI가 켜져있으면 막아야한다. 
			::SetCursorPos(oldCursorPos.x, oldCursorPos.y);
		}


		if (gui[PARTICLEGUI]->RadioButtonState() == ADDPARTICLE && buttonDown) {
			//피킹지점을 구하고 오브젝트를 배치한다. 마우스로 한번 눌렀을때만 배치한다. 
			float nearDistance = FLT_MAX;
			HeightMapTerrain* terrain = scene->GetTerrain();
			XMFLOAT3 pickingPoint{ 0,0,0 };
			UINT textureIdx{};
			int bIdx = FAILPICKING;
			if (ProcessTerrainPicking(terrain, pickingPoint, tempCursorPos, nearDistance, bIdx)) {
				buttonDown = false;
				int enableParticleIndex;
				int particleIndex = shaders[PARTICLESHADER]->AddParticle(device, commandList, pickingPoint , gui[PARTICLEGUI]->GetShapeIdx(), textureIdx);
				gui[PARTICLEGUI]->AddParticleIndex(particleIndex , 0);
			}
		}

	}
	if (gui[PARTICLEGUI]->RadioButtonState() == CONTROLCAMERA) {
		if ((direction != 0) || (deltaX != 0.0f) || (deltaY != 0.0f))
		{
			if (deltaX || deltaY)
			{

				if (keyBuffer[VK_RBUTTON] & 0xF0)
					player->Rotate(deltaY, 0.0f, -deltaX);
				else
					player->Rotate(deltaY, deltaX, 0.0f);
			}

			if (direction) player->Move(direction, cameraSpeed * gameTimer.GetTimeElapsed(), true);
		}
	}
	//플레이어를 실제로 이동하고 카메라를 갱신한다. 중력과 마찰력의 영향을 속도 벡터에 적용한다.
	player->Update(gameTimer.GetTimeElapsed());
}

void ProcessInput::ProcessBoundingInput(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, HWND hwnd, GameTimer & gameTimer , bool& dummyFlag , int& fobIdx)
{
	static UCHAR keyBuffer[256];
	DWORD direction = 0;

	if (::GetKeyboardState(keyBuffer))
	{
		if (keyBuffer[VK_UP] & 0xF0) direction |= DIR_FORWARD;
		if (keyBuffer[VK_DOWN] & 0xF0) direction |= DIR_BACKWARD;
		if (keyBuffer[VK_LEFT] & 0xF0) direction |= DIR_LEFT;
		if (keyBuffer[VK_RIGHT] & 0xF0) direction |= DIR_RIGHT;
		if (keyBuffer[VK_PRIOR] & 0xF0) direction |= DIR_UP;
		if (keyBuffer[VK_NEXT] & 0xF0) direction |= DIR_DOWN;
		if (keyBuffer[0x51] & 0xF0 || keyBuffer[0x71] & 0xF0) {
			gui[BOUNDINGGUI]->useShortCut('q');
			gui[OPTIONGUI]->useShortCut('n');
		}
		if (keyBuffer[0x57] & 0xF0 || keyBuffer[0x77] & 0xF0) {
			gui[BOUNDINGGUI]->useShortCut('w');
			gui[OPTIONGUI]->useShortCut('m');
		};
	}
	float deltaX = 0.0f, deltaY = 0.0f;
	POINT cursorPos;

	::GetCursorPos(&cursorPos);
	auto tempCursorPos = cursorPos;

	ScreenToClient(hwnd, &tempCursorPos);
	if (::GetCapture() == hwnd)
	{

		if (!optiongui->Getmodifygui()) {
			deltaX = (float)(cursorPos.x - oldCursorPos.x) / 3.0f;
			deltaY = (float)(cursorPos.y - oldCursorPos.y) / 3.0f;
			//마우스 커서의 위치를 마우스가 눌려졌던 위치로 설정한다.
			//GUI가 켜져있으면 막아야한다. 
			::SetCursorPos(oldCursorPos.x, oldCursorPos.y);
		}

	}


		
	ObjectInform objectType = boundinggui->GetLocatedObjectType();
	if (objectType.objectIdx != FAILPICKING && objectType.shaderIdx != FAILPICKING)
	{
		Shader** shaders = scene->GetShaders();
		GameObject* temp;
		if (objectType.shaderIdx == INSTANCSHADER) {
			++objectType.objectIdx;
			temp = shaders[objectType.shaderIdx]->GetGameObject(objectType.objectIdx, 0);
			boundinggui->SetSelectedObject(temp);
			fobIdx = objectType.objectIdx;
		}
		if (objectType.shaderIdx == SKINEDINSTANCSHADER) {
			temp = shaders[objectType.shaderIdx]->GetGameObject(objectType.objectIdx, 0);
			boundinggui->SetSelectedObject(temp);
		}
		temp->SetPosition(0.0f, 0.0f, 0.0f);
		if (oldObjectIdx != objectType.objectIdx || oldShaderIdx != objectType.shaderIdx) {
			dummyFlag = false;
			oldObjectIdx = objectType.objectIdx;
			oldShaderIdx = objectType.shaderIdx;
		}

		//바운딩 박스 번호를 넘겨줘야한다. 
		


	}
	


	if (gui[BOUNDINGGUI]->RadioButtonState() == 0) {
		if ((direction != 0) || (deltaX != 0.0f) || (deltaY != 0.0f))
		{
			if (deltaX || deltaY)
			{
				/*deltaX는 y-축의 회전을 나타내고 deltaY는 x-축의 회전을 나타낸다. 오른쪽 마우스 버튼이 눌려진 경우
				cxDelta는 z-축의 회전을 나타낸다.*/
				if (keyBuffer[VK_RBUTTON] & 0xF0)
					player->Rotate(deltaY, 0.0f, -deltaX);
				else
					player->Rotate(deltaY, deltaX, 0.0f);
			}
			/*플레이어를 dwDirection 방향으로 이동한다(실제로는 속도 벡터를 변경한다). 이동 거리는 시간에 비례하도록 한다.
			플레이어의 이동 속력은 (50/초)로 가정한다.*/
			if (direction) player->Move(direction, cameraSpeed * gameTimer.GetTimeElapsed(),
				true);
		}
	}

	player->Update(gameTimer.GetTimeElapsed());

}


void ProcessInput::HideArrowObject(std::vector<GameObject*> &depthshaderObject)
{
	for (int i = 0; i <= UPARRROW; ++i) {
		depthshaderObject[i]->SetPosition(XMFLOAT3(9999.0f, 9999.0f, 9999.0f));
		depthshaderObject[i]->SetOBB(depthshaderObject[LOOKARROW]->GetPosition(), XMFLOAT3(12.0f, 18.0f, 6.0f), XMFLOAT4(0, 0, 0, 0));
	}
}

ProcessInput::ProcessInput()
{
}


ProcessInput::~ProcessInput()
{
}
