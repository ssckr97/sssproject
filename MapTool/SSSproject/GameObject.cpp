#include "stdafx.h"
#include "GameObject.h"
#include "Shader.h"
#include "Scene.h"
#include "Animation.h"
#include "Texture.h"
#include "Material.h"


int ReadIntegerFromFile(FILE *pInFile)
{
	int nValue = 0;
	UINT nReads = (UINT)::fread(&nValue, sizeof(int), 1, pInFile);
	return(nValue);
}

float ReadFloatFromFile(FILE *pInFile)
{
	float fValue = 0;
	UINT nReads = (UINT)::fread(&fValue, sizeof(float), 1, pInFile);
	return(fValue);
}

BYTE ReadStringFromFile(FILE *pInFile, char *pstrToken)
{
	BYTE nStrLength = 0;
	UINT nReads = 0;
	nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, pInFile);
	nReads = (UINT)::fread(pstrToken, sizeof(char), nStrLength, pInFile);
	pstrToken[nStrLength] = '\0';

	return(nStrLength);
}

GameObject::GameObject(int meshesNum)
{
	XMStoreFloat4x4(&world, XMMatrixIdentity());
	transform = Matrix4x4::Identity();

	this->meshesNum = meshesNum;
	if (this->meshesNum > 0)
		meshes = new Mesh*[meshesNum];
	for (int i = 0; i < meshesNum; ++i)
	{
		meshes[i] = NULL;
	}

}

GameObject::~GameObject()
{
	ReleaseShaderVariables();

	if (meshes)
	{
		for (int i = 0; i < meshesNum; i++)
		{
			if (meshes[i]) {
				meshes[i]->Release();
				meshes[i] = NULL;
			}
		}
	}
	if (boundingMesh)
	{
		boundingMesh->Release();
		boundingMesh = NULL;
	}

	if (materials)
	{
		for (int i = 0; i < materialsNum; ++i)
		{
			if (materials[i]) {
				materials[i]->Release();
				materials[i] = NULL;
			}
		}
	}

	if (naviMesh) {
		delete[] naviMesh;

	}


}

void GameObject::AddRef()
{
	references++;

	if (sibling) sibling->AddRef();
	if (child) child->AddRef();
}

void GameObject::Release()
{
	if (sibling) sibling->Release();
	if (child) child->Release();

	if (--references <= 0) delete this;
}

void GameObject::SetShader(Shader *shader)
{
	materialsNum = 1;
	materials = new Material*[materialsNum];
	materials[0] = new Material(0);
	materials[0]->SetShader(shader);
}
void GameObject::SetMesh(int idx, Mesh *mesh)
{
	if (this->meshes[idx])
		this->meshes[idx]->Release();
	this->meshes[idx] = mesh;
	if (this->meshes[idx])
		this->meshes[idx]->AddRef();
}

void GameObject::SetBoundingMesh(BoundingBoxMesh* mesh)
{
	boundingMesh = mesh;

}

void GameObject::SetNaviMesh(NaviMesh * mesh)
{
	naviMesh = mesh;

}

void GameObject::SetMaterial(int idx, Material *material)
{
	if (this->materials[idx])
		this->materials[idx]->Release();
	this->materials[idx] = material;
	if (this->materials[idx])
		this->materials[idx]->AddRef();
}

void GameObject::SetMaterial(int idx, UINT reflection)
{
	if (!materials[idx]) materials[idx] = new Material(1);
	materials[idx]->reflection = reflection;
}

void GameObject::SetReflection(UINT reflection)
{
	this->reflection = reflection;

}

void GameObject::SetTextureMask(UINT texturemask)
{
	this->textureMask = texturemask;

}

void GameObject::SetTextureIdx(UINT idx)
{
	this->textureIdx = idx;
}

void GameObject::SetChild(GameObject *child)
{
	if (this->child)
	{
		if (child) child->sibling = this->child->sibling;
		this->child->sibling = child;
	}
	else
	{
		this->child = child;
	}
	if (child)
	{
		child->parent = this;
	}
}

void GameObject::SetChild(GameObject *pChild, bool referenceUpdate)
{
	if (pChild)
	{
		pChild->parent = this;
		if (referenceUpdate) pChild->AddRef();
	}
	if (child)
	{
		if (pChild) pChild->sibling = child->sibling;
		child->sibling = pChild;
	}
	else
	{
		child = pChild;
	}
}

void GameObject::SetPipelineStatesNum(UINT type)
{
	pipeLineStatesNum = type;
}

void GameObject::GetHierarchyMaterial(std::vector<MATERIAL>& materialReflection, int& idx, TEXTURENAME textureName)
{
	if (materials)
	{
		for (int i = 0; i < materialsNum; ++i)
		{
			MATERIAL temp;
			temp.diffuse = materials[i]->albedoColor;
			temp.ambient = materials[i]->ambientColor;
			temp.emissive = materials[i]->emissiveColor;
			temp.specular = materials[i]->specularColor;
		
			materialReflection.emplace_back(temp);
			materials[i]->reflection = 8 + idx++;
		}
	}
	if (sibling) sibling->GetHierarchyMaterial(materialReflection, idx, textureName);
	if (child) child->GetHierarchyMaterial(materialReflection, idx, textureName);
}


UINT GameObject::GetReflection()
{
	return reflection;
}

UINT GameObject::GetTextureMask()
{
	return textureMask;
}

int GameObject::GetFramesNum()
{
	return framesNum;
}

void GameObject::CreateMaterials(UINT materialsNum)
{
	materials = new Material*[materialsNum];
	this->materialsNum = materialsNum;
	for (int i = 0; i < (int)materialsNum; ++i)
	{
		materials[i] = NULL;
	}
}

void GameObject::CreateCbvGPUDescriptorHandles(int handlesNum)
{
	this->handlesNum = handlesNum;
	cbvGPUDescriptorHandle = new D3D12_GPU_DESCRIPTOR_HANDLE[handlesNum];
}

void GameObject::SetCbvGPUDescriptorHandlesOnlyStandardMesh(D3D12_GPU_DESCRIPTOR_HANDLE handle, int objectsNum, int framesNum)
{
	static int counter = 0;

	if (shaderType == ShaderTypeStandard)
	{
		CreateCbvGPUDescriptorHandles(objectsNum);

		for (int i = 0; i < objectsNum; ++i)
			cbvGPUDescriptorHandle[i].ptr = handle.ptr + (::gnCbvSrvDescriptorIncrementSize * (objectsNum * counter + i));

		counter++;
	}

	if (sibling) sibling->SetCbvGPUDescriptorHandlesOnlyStandardMesh(handle, objectsNum, framesNum);
	if (child) child->SetCbvGPUDescriptorHandlesOnlyStandardMesh(handle, objectsNum, framesNum);

	if (framesNum == counter)
		counter = 0;
}

void GameObject::SetCbvGPUDescriptorHandlePtrHierarchy(UINT64 cbvGPUDescriptorHandlePtr, int idx, int objectsNum)
{
	if (!cbvGPUDescriptorHandle && parent == NULL)
		CreateCbvGPUDescriptorHandles();
	if (!cbvGPUDescriptorHandle && parent != NULL)
		CreateCbvGPUDescriptorHandles(objectsNum);

	static int counter = 0;

	if (counter == 0)
		cbvGPUDescriptorHandle[counter++].ptr = cbvGPUDescriptorHandlePtr + (::gnCbvSrvDescriptorIncrementSize * idx);
	else
		cbvGPUDescriptorHandle[idx].ptr = cbvGPUDescriptorHandlePtr + (::gnCbvSrvDescriptorIncrementSize * (objectsNum * counter++ + idx));

	if (sibling) sibling->SetCbvGPUDescriptorHandlePtrHierarchy(cbvGPUDescriptorHandlePtr, idx, objectsNum);
	if (child) child->SetCbvGPUDescriptorHandlePtrHierarchy(cbvGPUDescriptorHandlePtr, idx, objectsNum);

	if (parent == NULL)
		counter = 0;
}

void GameObject::SetMaterialCbvGPUDescriptorHandlePtrHierarchy(UINT64 cbvGPUDescriptorHandlePtr, int maximum)
{
	static int counter = 0;

	if (materials)
	{
		for (int i = 0; i < materialsNum; ++i)
		{
			materials[i]->cbvGPUDescriptorHandle.ptr = cbvGPUDescriptorHandlePtr + (::gnCbvSrvDescriptorIncrementSize * ((maximum * counter) + i));
		}
	}
	counter++;
	if (sibling) sibling->SetMaterialCbvGPUDescriptorHandlePtrHierarchy(cbvGPUDescriptorHandlePtr, maximum);
	if (child) child->SetMaterialCbvGPUDescriptorHandlePtrHierarchy(cbvGPUDescriptorHandlePtr, maximum);

	if (parent == NULL)
		counter = 0;
}

void GameObject::ReleaseUploadBuffers()
{
	if (meshes)
	{
		for (int i = 0; i < meshesNum; i++)
		{
			if (meshes[i]) meshes[i]->ReleaseUploadBuffers();
		}
	}
	if (sibling) sibling->ReleaseUploadBuffers();
	if (child) child->ReleaseUploadBuffers();
}

void GameObject::UpdateTransform(XMFLOAT4X4 *parent)
{
	world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

	if (sibling) sibling->UpdateTransform(parent);
	if (child) child->UpdateTransform(&world);
}

void GameObject::UpdateTransformOnlyStandardMesh(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, int framesNum, XMFLOAT4X4 *parent)
{
	static int counter = 0;

	if (shaderType == ShaderTypeStandard) {

		world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

		CB_GAMEOBJECT_INFO *mappedCbGameObject = (CB_GAMEOBJECT_INFO *)((UINT8 *)cbMappedGameObjects[counter] + (cbGameObjectBytes * idx));
		XMStoreFloat4x4(&mappedCbGameObject->world, XMMatrixTranspose(XMLoadFloat4x4(&world)));

		counter++;
	}

	if (sibling) sibling->UpdateTransformOnlyStandardMesh(cbMappedGameObjects, cbGameObjectBytes, idx, framesNum, parent);
	if (child) child->UpdateTransformOnlyStandardMesh(cbMappedGameObjects, cbGameObjectBytes, idx, framesNum, &world);

	if (counter == framesNum)
		counter = 0;
}

void GameObject::UpdateTransformOnlyStandardMesh(VS_VB_INSTANCE_OBJECT** vbMappedGameObjects, int idx, int framesNum, XMFLOAT4X4 *parent)
{
	static int counter = 0;

	if (shaderType == ShaderTypeStandard) {

		world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

		XMStoreFloat4x4(&vbMappedGameObjects[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&world)));
		if (idx == 0) 	vbMappedGameObjects[counter][idx].isDummy = true;
		else vbMappedGameObjects[counter][idx].isDummy = false;
		counter++;
	}

	if (sibling) sibling->UpdateTransformOnlyStandardMesh(vbMappedGameObjects, idx, framesNum, parent);
	if (child) child->UpdateTransformOnlyStandardMesh(vbMappedGameObjects, idx, framesNum, &world);

	if (counter == framesNum)
		counter = 0;
}

void GameObject::UpdateTransform(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, XMFLOAT4X4 *parent)
{
	static int counter = 0;

	world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

	CB_GAMEOBJECT_INFO *mappedCbGameObject = (CB_GAMEOBJECT_INFO *)((UINT8 *)cbMappedGameObjects[counter] + (cbGameObjectBytes * idx));
	XMStoreFloat4x4(&mappedCbGameObject->world, XMMatrixTranspose(XMLoadFloat4x4(&world)));
	XMStoreFloat4x4(&mappedCbGameObject->inverseWorld, XMMatrixTranspose(XMLoadFloat4x4(&GetInverseWorld())));
	counter++;
	if (sibling) sibling->UpdateTransform(cbMappedGameObjects, cbGameObjectBytes, idx, parent);
	if (child) child->UpdateTransform(cbMappedGameObjects, cbGameObjectBytes, idx, &world);

	if (parent == NULL)
		counter = 0;
}

void GameObject::UpdateTransform(VS_VB_INSTANCE_OBJECT** vbMappedGameObjects, int idx, XMFLOAT4X4* parent)
{
	static int counter = 0;

	world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

	XMStoreFloat4x4(&vbMappedGameObjects[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&world)));
	if (idx == 0) 	vbMappedGameObjects[counter][idx].isDummy = true;
	else vbMappedGameObjects[counter][idx].isDummy = false;

	counter++;
	if (sibling) sibling->UpdateTransform(vbMappedGameObjects, idx, parent);
	if (child) child->UpdateTransform(vbMappedGameObjects, idx, &world);

	if (parent == NULL)
		counter = 0;
}

void GameObject::UpdateBoundingTransForm(VS_VB_INSTACE_BOUNDING * vbMappedBoudnigs, int idx, XMFLOAT4X4 pWorld , int bidx)
{
	//bidx는 바깥에 m의 인덱스랑 같다. 
	
	auto fworld = Matrix4x4::Multiply((*boundingObject)[bidx]->world, world);
	XMStoreFloat4x4(&vbMappedBoudnigs[idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&fworld)));
	if (idx == 0) 	vbMappedBoudnigs[idx].isDummy = true;
	else 	vbMappedBoudnigs[idx].isDummy = false;
	vbMappedBoudnigs[idx].bIdx = bidx;
}

void GameObject::UpdateSkinnedBoundingTransform(VS_VB_INSTACE_BOUNDING** vbMappedBoundings, std::vector<BoundingOrientedBox>& obbs, int idx, int framesNum, XMFLOAT4X4* parent)
{
	static int counter = 0;

	if (shaderType == ShaderTypeBounding) {

		world = (parent) ? Matrix4x4::Multiply(transform, *parent) : transform;

		XMStoreFloat4x4(&vbMappedBoundings[counter][idx].transform, XMMatrixTranspose(XMLoadFloat4x4(&world)));
		if (_isnan(world._11) == false) {
			BoundingOrientedBox tempObb{ XMFLOAT3(0.0f, 0.0f, 0.0f), XMFLOAT3(0.5f, 0.5f, 0.5f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
			tempObb.Transform(obbs[counter], XMLoadFloat4x4(&world));
		}

		if (idx == 0) 	vbMappedBoundings[counter][idx].isDummy = true;
		else vbMappedBoundings[counter][idx].isDummy = false;
		counter++;
	}

	if (sibling) sibling->UpdateSkinnedBoundingTransform(vbMappedBoundings, obbs, idx, framesNum, parent);
	if (child) child->UpdateSkinnedBoundingTransform(vbMappedBoundings, obbs, idx, framesNum, &world);

	if (counter == framesNum)
		counter = 0;
}


void GameObject::Animate(float timeElapsed, XMFLOAT4X4* parent)
{
	OnPrepareRender();

	if (skinnedAnimationController) skinnedAnimationController->AdvanceTime(timeElapsed, this);

	if (sibling) sibling->Animate(timeElapsed, parent);
	if (child) child->Animate(timeElapsed, &world);
}

void GameObject::SetOBBBoundingBox()
{
	XMStoreFloat4(&obbBox.Orientation, XMQuaternionRotationMatrix(XMLoadFloat4x4(&world)));
}

void GameObject::SetOBBRotate(XMFLOAT4X4 world , int idx)
{
	
	XMStoreFloat4(&obbvec[idx].Orientation, XMQuaternionRotationMatrix(XMLoadFloat4x4(&world)));
}

std::vector<BoundingOrientedBox>& GameObject::GetobbVector()
{
	return obbvec;
}


GameObject *GameObject::FindFrame(char *frameName)
{
	GameObject *frameObject = NULL;
	if (!strncmp(this->frameName, frameName, strlen(frameName))) return(this);

	if (sibling) if (frameObject = sibling->FindFrame(frameName)) return(frameObject);
	if (child) if (frameObject = child->FindFrame(frameName)) return(frameObject);

	return(NULL);
}

_TCHAR * GameObject::FindReplicatedTexture(_TCHAR * textureName)
{
	for (int i = 0; i < materialsNum; i++)
	{
		if (materials[i])
		{
			for (int j = 0; j < materials[i]->texturesNum; j++)
			{
				if (materials[i]->textures[j])
				{
					if (!_tcsncmp(materials[i]->textureNames[j], textureName, _tcslen(textureName))) return(materials[i]->textureNames[j]);
				}
			}
		}
	}
	_TCHAR *name = NULL;
	if (sibling) if (name = sibling->FindReplicatedTexture(textureName)) return(name);
	if (child) if (name = child->FindReplicatedTexture(textureName)) return(name);

	return(NULL);
}

void GameObject::FindAndSetSkinnedMesh(SkinnedMesh **SkinnedMeshes, int *SkinnedMeshnum)
{
	if (meshes[0] && (meshes[0]->GetType() & VERTEXT_BONE_INDEX_WEIGHT)) SkinnedMeshes[(*SkinnedMeshnum)++] = (SkinnedMesh *)(meshes[0]);

	if (sibling) sibling->FindAndSetSkinnedMesh(SkinnedMeshes, SkinnedMeshnum);
	if (child) child->FindAndSetSkinnedMesh(SkinnedMeshes, SkinnedMeshnum);
}

void GameObject::LoadMaterialsFromFile(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, GameObject * parent, FILE * inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5])
{
	char pstrToken[64] = { '\0' };

	int nMaterial = 0;
	BYTE nStrLength = 0;

	UINT nReads = (UINT)::fread(&materialsNum, sizeof(int), 1, inFile);


	materials = new Material*[materialsNum];
	for (int i = 0; i < materialsNum; i++)
		materials[i] = NULL;

	Material *pMaterial = NULL;

	for (; ; )
	{
		nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
		nReads = (UINT)::fread(pstrToken, sizeof(char), nStrLength, inFile);
		pstrToken[nStrLength] = '\0';

		if (!strcmp(pstrToken, "<Material>:"))
		{
			nReads = (UINT)::fread(&nMaterial, sizeof(int), 1, inFile);

			pMaterial = new Material(7); //0:Albedo, 1:Specular, 2:Metallic, 3:Normal, 4:Emission, 5:DetailAlbedo, 6:DetailNormal

			UINT nMeshType = GetMeshType();
			if (nMeshType & (VertexPosition | VertexNormal | VertexTangent | VertexTexCoord0))
			{
				if (nMeshType & VERTEXT_BONE_INDEX_WEIGHT)
				{
					pMaterial->SetSkinnedAnimationShader();
				}
				else
				{
					pMaterial->SetStandardShader();
				}
			}

			SetMaterial(nMaterial, pMaterial);
		}
		else if (!strcmp(pstrToken, "<AlbedoColor>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->albedoColor), sizeof(float), 4, inFile);
		}
		else if (!strcmp(pstrToken, "<EmissiveColor>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->emissiveColor), sizeof(float), 4, inFile);
		}
		else if (!strcmp(pstrToken, "<SpecularColor>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->specularColor), sizeof(float), 4, inFile);
		}
		else if (!strcmp(pstrToken, "<Glossiness>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->glossiness), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<Smoothness>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->smoothness), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<Metallic>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->metallic), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<SpecularHighlight>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->specularHighlight), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<GlossyReflection>:"))
		{
			nReads = (UINT)::fread(&(pMaterial->glossyReflection), sizeof(float), 1, inFile);
		}
		else if (!strcmp(pstrToken, "<AlbedoMap>:"))
		{
			pMaterial->LoadTextureFromFile(device, commandList, MATERIAL_DIFFUSE_MAP, 6, pMaterial->textureNames[0], &(pMaterial->textures[0]), parent, inFile, folderName, textureMapNames.diffuse, textureIdx[0]);
		}
		else if (!strcmp(pstrToken, "<NormalMap>:"))
		{
			materials[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_NORMAL_MAP, 7, pMaterial->textureNames[1], &(pMaterial->textures[1]), parent, inFile, folderName, textureMapNames.normal, textureIdx[1]);
		}
		else if (!strcmp(pstrToken, "<SpecularMap>:"))
		{
			materials[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_SPECULAR_MAP, 8, pMaterial->textureNames[2], &(pMaterial->textures[2]), parent, inFile, folderName, textureMapNames.specular, textureIdx[2]);
		}
		else if (!strcmp(pstrToken, "<MetallicMap>:"))
		{
			materials[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_METALLIC_MAP, 9, pMaterial->textureNames[3], &(pMaterial->textures[3]), parent, inFile, folderName, textureMapNames.metallic, textureIdx[3]);
		}
		else if (!strcmp(pstrToken, "<EmissionMap>:"))
		{
			materials[nMaterial]->LoadTextureFromFile(device, commandList, MATERIAL_EMISSION_MAP, 10, pMaterial->textureNames[4], &(pMaterial->textures[4]), parent, inFile, folderName, textureMapNames.emission, textureIdx[4]);
		}
		else if (!strcmp(pstrToken, "<DetailAlbedoMap>:"))
		{
			char pstrTextureName[64] = { '\0' };

			BYTE nStrLength = 64;
			UINT nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
			nReads = (UINT)::fread(pstrTextureName, sizeof(char), nStrLength, inFile);
		}
		else if (!strcmp(pstrToken, "<DetailNormalMap>:"))
		{
			char pstrTextureName[64] = { '\0' };

			BYTE nStrLength = 64;
			UINT nReads = (UINT)::fread(&nStrLength, sizeof(BYTE), 1, inFile);
			nReads = (UINT)::fread(pstrTextureName, sizeof(char), nStrLength, inFile);
		}
		else if (!strcmp(pstrToken, "</Materials>"))
		{
			break;
		}
	}
}

void GameObject::LoadAnimationFromFile(FILE *pInFile, LoadedModelInfo *pLoadedModel)
{
	char pstrToken[64] = { '\0' };
	UINT nReads = 0;

	int nAnimationSets = 0;

	for (; ; )
	{
		::ReadStringFromFile(pInFile, pstrToken);
		if (!strcmp(pstrToken, "<AnimationSets>:"))
		{
			nAnimationSets = ::ReadIntegerFromFile(pInFile);
			pLoadedModel->animationSets = new AnimationSets(nAnimationSets);
		}
		else if (!strcmp(pstrToken, "<FrameNames>:"))
		{
			pLoadedModel->animationSets->animatedBoneFrames = ::ReadIntegerFromFile(pInFile);
			pLoadedModel->animationSets->animatedBoneFrameCaches = new GameObject*[pLoadedModel->animationSets->animatedBoneFrames];

			for (int j = 0; j < pLoadedModel->animationSets->animatedBoneFrames; j++)
			{
				::ReadStringFromFile(pInFile, pstrToken);
				pLoadedModel->animationSets->animatedBoneFrameCaches[j] = pLoadedModel->modelRootObject->FindFrame(pstrToken);

#ifdef _WITH_DEBUG_SKINNING_BONE
				TCHAR pstrDebug[256] = { 0 };
				TCHAR pwstrAnimationBoneName[64] = { 0 };
				TCHAR pwstrBoneCacheName[64] = { 0 };
				size_t nConverted = 0;
				mbstowcs_s(&nConverted, pwstrAnimationBoneName, 64, pstrToken, _TRUNCATE);
				mbstowcs_s(&nConverted, pwstrBoneCacheName, 64, pLoadedModel->m_ppAnimatedBoneFrameCaches[j]->m_pstrFrameName, _TRUNCATE);
				_stprintf_s(pstrDebug, 256, _T("AnimationBoneFrame:: Cache(%s) AnimationBone(%s)\n"), pwstrBoneCacheName, pwstrAnimationBoneName);
				OutputDebugString(pstrDebug);
#endif
			}
		}
		else if (!strcmp(pstrToken, "<AnimationSet>:"))
		{
			int nAnimationSet = ::ReadIntegerFromFile(pInFile);

			::ReadStringFromFile(pInFile, pstrToken); //Animation Set Name

			float fLength = ::ReadFloatFromFile(pInFile);
			int nFramesPerSecond = ::ReadIntegerFromFile(pInFile);
			int nKeyFrames = ::ReadIntegerFromFile(pInFile);

			pLoadedModel->animationSets->animationSets[nAnimationSet] = new AnimationSet(fLength, nFramesPerSecond, nKeyFrames, pLoadedModel->animationSets->animatedBoneFrames, pstrToken);

			for (int i = 0; i < nKeyFrames; i++)
			{
				::ReadStringFromFile(pInFile, pstrToken);
				if (!strcmp(pstrToken, "<Transforms>:"))
				{
					int nKey = ::ReadIntegerFromFile(pInFile); //i
					float fKeyTime = ::ReadFloatFromFile(pInFile);

					AnimationSet *animationSet = pLoadedModel->animationSets->animationSets[nAnimationSet];
					animationSet->keyFrameTimes[i] = fKeyTime;
					nReads = (UINT)::fread(animationSet->keyFrameTransforms[i], sizeof(XMFLOAT4X4), pLoadedModel->animationSets->animatedBoneFrames, pInFile);
				}
			}
		}
		else if (!strcmp(pstrToken, "</AnimationSets>"))
		{
			break;
		}
	}
}


GameObject* GameObject::LoadFrameHierarchyFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, GameObject* parent, FILE* inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5], int& frameCounter, int& materialCounter, int* skinnedMeshsNum, int* standardMeshNum, int* boundingMeshNum)
{
	char token[64] = { '\0' };

	BYTE strLength = 0;
	UINT readsNum = 0;

	int frameNum = 0, texturesNum = 0;
	static int maximum = 0;
	GameObject *gameObject = NULL;

	for (; ; )
	{
		::ReadStringFromFile(inFile, token);

		if (!strcmp(token, "<Frame>:"))
		{
			gameObject = new GameObject(1);

			readsNum = (UINT)::fread(&frameNum, sizeof(int), 1, inFile);
			readsNum = (UINT)::fread(&texturesNum, sizeof(int), 1, inFile);

			readsNum = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
			readsNum = (UINT)::fread(gameObject->frameName, sizeof(char), strLength, inFile);
			gameObject->frameName[strLength] = '\0';
			frameCounter++;
		}
		else if (!strcmp(token, "<Transform>:"))
		{
			XMFLOAT3 xmf3Position, xmf3Rotation, xmf3Scale;
			XMFLOAT4 xmf4Rotation;
			readsNum = (UINT)::fread(&xmf3Position, sizeof(float), 3, inFile);
			readsNum = (UINT)::fread(&xmf3Rotation, sizeof(float), 3, inFile); //Euler Angle
			readsNum = (UINT)::fread(&xmf3Scale, sizeof(float), 3, inFile);
			readsNum = (UINT)::fread(&xmf4Rotation, sizeof(float), 4, inFile); //Quaternion
		}
		else if (!strcmp(token, "<TransformMatrix>:"))
		{
			readsNum = (UINT)::fread(&gameObject->transform, sizeof(float), 16, inFile);
		}
		else if (!strcmp(token, "<Mesh>:"))
		{
			StandardMesh *mesh = new StandardMesh(device, commandList);
			
			BoundingBoxMesh* bMesh = NULL;
			mesh->LoadMeshFromFile(device, commandList, inFile , bMesh , true);
			if (strncmp(mesh->GetMeshName(), "Cube", 256) == 0)
			{
				bMesh = new BoundingBoxMesh(device, commandList, 0.5f, 0.5f, 0.5f);
				gameObject->SetBoundingMesh(bMesh);

				gameObject->shaderType = ShaderTypeBounding;
				if (boundingMeshNum) (*boundingMeshNum)++;
			}
			
			else {
				gameObject->SetMesh(0, mesh);
				gameObject->shaderType = ShaderTypeStandard;
				if (standardMeshNum) (*standardMeshNum)++;
			}
		}
		else if (!strcmp(token, "<SkinningInfo>:"))
		{
			if (skinnedMeshsNum) (*skinnedMeshsNum)++;

			SkinnedMesh *pSkinnedMesh = new SkinnedMesh(device, commandList);
			pSkinnedMesh->LoadSkinInfoFromFile(device, commandList, inFile);
			pSkinnedMesh->CreateShaderVariables(device, commandList);
			BoundingBoxMesh* bMesh = NULL;
			::ReadStringFromFile(inFile, token); //<Mesh>:
			if (!strcmp(token, "<Mesh>:")) pSkinnedMesh->LoadMeshFromFile(device, commandList, inFile , bMesh, true);

			gameObject->SetMesh(0, pSkinnedMesh);
			gameObject->shaderType = ShaderTypeSkinned;
		}
		else if (!strcmp(token, "<Materials>:"))
		{
			gameObject->LoadMaterialsFromFile(device, commandList, parent, inFile, folderName, textureMapNames, textureIdx);
			materialCounter = max(materialCounter, gameObject->materialsNum);
		}
		else if (!strcmp(token, "<Children>:"))
		{
			int childsNum = 0;
			readsNum = (UINT)::fread(&childsNum, sizeof(int), 1, inFile);
			if (childsNum > 0)
			{
				for (int i = 0; i < childsNum; i++)
				{
					GameObject *child = GameObject::LoadFrameHierarchyFromFile(device, commandList, graphicsRootSignature, gameObject, inFile, folderName, textureMapNames, textureIdx, frameCounter, materialCounter, skinnedMeshsNum, standardMeshNum, boundingMeshNum);
					if (child) gameObject->SetChild(child);
#ifdef _WITH_DEBUG_FRAME_HIERARCHY
					TCHAR pstrDebug[256] = { 0 };
					_stprintf_s(pstrDebug, 256, _T("(Frame: %p) (Parent: %p)\n"), pChild, gameObject);
					OutputDebugString(pstrDebug);
#endif
				}
			}
		}
		else if (!strcmp(token, "</Frame>"))
		{
			gameObject->framesNum = frameCounter;
			gameObject->maximumMaterialsNum = materialCounter;
			break;
		}
	}
	return(gameObject);
}


void GameObject::PrintFrameInfo(GameObject* gameObject, GameObject* parent)
{
	TCHAR strDebug[256] = { 0 };

	_stprintf_s(strDebug, 256, _T("(Frame: %p) (Parent: %p)\n"), gameObject, parent);
	OutputDebugString(strDebug);

	if (gameObject->sibling) GameObject::PrintFrameInfo(gameObject->sibling, parent);
	if (gameObject->child) GameObject::PrintFrameInfo(gameObject->child, gameObject);
}

GameObject* GameObject::LoadGeometryFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, char* fileName, char* folderName)
{
	TEXTURENAME textureMapName;
	int textureMapIdx[5]{};
	FILE *inFile = NULL;
	::fopen_s(&inFile, fileName, "rb");
	::rewind(inFile);
	int frameCounter = 0, materialCounter = 0;
	GameObject *gameObject = GameObject::LoadFrameHierarchyFromFile(device, commandList, graphicsRootSignature, NULL, inFile, folderName, textureMapName, textureMapIdx, frameCounter, materialCounter, NULL, NULL, NULL);
	gameObject->textureName = textureMapName;
#ifdef _WITH_DEBUG_FRAME_HIERARCHY
	TCHAR pstrDebug[256] = { 0 };
	_stprintf_s(pstrDebug, 256, _T("Frame Hierarchy\n"));
	OutputDebugString(pstrDebug);

	CGameObject::PrintFrameInfo(pGameObject, NULL);
#endif

	return(gameObject);
}

LoadedModelInfo* GameObject::LoadGeometryAndAnimationFromFile(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, char* fileName, char* folderName)
{
	TEXTURENAME textureMapName;
	int textureMapIdx[5]{};

	char token[64] = { '\0' };
	LoadedModelInfo *modelInfo = new LoadedModelInfo();

	FILE *inFile = NULL;
	::fopen_s(&inFile, fileName, "rb");
	if (inFile == NULL)
	{
		std::cout << "빈파일!" << std::endl;
		return modelInfo;
	}
	::rewind(inFile);
	int frameCounter = 0, materialCounter = 0;

	for (; ; )
	{
		if (::ReadStringFromFile(inFile, token))
		{
			if (!strcmp(token, "<Hierarchy>:"))
			{
				modelInfo->modelRootObject = GameObject::LoadFrameHierarchyFromFile(device, commandList, graphicsRootSignature, NULL, inFile, folderName, textureMapName, textureMapIdx, frameCounter, materialCounter, &modelInfo->skinnedMeshNum, &modelInfo->standardMeshNum, &modelInfo->boundingMeshNum);
				::ReadStringFromFile(inFile, token); //"</Hierarchy>"
				modelInfo->modelRootObject->textureName = textureMapName;
			}
			else if (!strcmp(token, "<Animation>:"))
			{
				GameObject::LoadAnimationFromFile(inFile, modelInfo);
				modelInfo->PrepareSkinning();
			}
			else if (!strcmp(token, "</Animation>:"))
			{
				break;
			}
		}
		else
		{
			break;
		}
	}

#ifdef _WITH_DEBUG_FRAME_HIERARCHY
	TCHAR pstrDebug[256] = { 0 };
	_stprintf_s(pstrDebug, 256, _T("Frame Hierarchy\n"));
	OutputDebugString(pstrDebug);

	CGameObject::PrintFrameInfo(pGameObject, NULL);
#endif

	return(modelInfo);
}



void GameObject::OnPrepareRender()
{
}

void GameObject::Render(ID3D12GraphicsCommandList* commandList, Camera* camera, int idx)
{
	OnPrepareRender();

	if (materialsNum > 0)
	{
		for (int j = 0; j < materialsNum; ++j)
		{
			if (materials)
			{
				if (materials[j]->shader)
					materials[j]->shader->Render(commandList, camera);
				materials[j]->UpdateShaderVariables(commandList);
				// GameObject Material
			}

			if (meshes)
			{
				for (int i = 0; i < meshesNum; i++)
				{
					if (meshes[i])
					{
						if (shaderType != ShaderTypeSkinned)
							UpdateShaderVariables(commandList, idx);
						//GameObject World Matrix
						meshes[i]->Render(commandList, j);
					}
				}
			}
		}
	}

	if (sibling) sibling->Render(commandList, camera, idx);
	if (child) child->Render(commandList, camera, idx);
}

// 모델 인스턴싱, 애니메이션 단일객체 렌더링
void GameObject::Render(ID3D12GraphicsCommandList* commandList, Camera* camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, bool isAnimationModel)
{
	static int counter = 0;

	if (materialsNum > 0)
	{
		for (int j = 0; j < materialsNum; ++j)
		{
			if (materials)
			{
				if (materials[j]->shader && isAnimationModel)
					materials[j]->shader->OnPrepareRender(commandList, 0);
				materials[j]->UpdateShaderVariables(commandList);
				// GameObject Material
			}

			if (meshes)
			{
				for (int i = 0; i < meshesNum; i++)
				{
					if (shaderType == ShaderTypeSkinned)
					{
						if (meshes[i])
							meshes[i]->Render(commandList, j);
					}
					else if(shaderType != ShaderTypeBounding)
					{
						std::vector<D3D12_VERTEX_BUFFER_VIEW> bufferViews;

						bufferViews.emplace_back(instancingGameObjectBufferView[counter]);
						if (meshes[i])
							meshes[i]->Render(commandList, j, instancesNum, bufferViews);

						if (shaderType == ShaderTypeStandard && isAnimationModel)
							counter++;
					}

				}
			}
		}
	}
	if (!isAnimationModel)
		counter++;

	if (sibling) sibling->Render(commandList, camera, instancesNum, instancingGameObjectBufferView, isAnimationModel);
	if (child) child->Render(commandList, camera, instancesNum, instancingGameObjectBufferView, isAnimationModel);

	if (parent == NULL)
		counter = 0;
}


// 애니메이션 인스턴싱
void GameObject::Render(ID3D12GraphicsCommandList* commandList, Camera* camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingObjIdxBufferView, D3D12_VERTEX_BUFFER_VIEW* instancingStandardBufferView)
{
	static int counter = 0;

	OnPrepareRender();

	if (materialsNum > 0)
	{
		for (int j = 0; j < materialsNum; ++j)
		{
			if (materials)
			{
				if (materials[j]->shader)
					materials[j]->shader->OnPrepareRender(commandList, 1);
				materials[j]->UpdateShaderVariables(commandList);
				// GameObject Material
			}

			if (meshes)
			{
				for (int i = 0; i < meshesNum; i++)
				{
					if (meshes[i])
					{
						std::vector<D3D12_VERTEX_BUFFER_VIEW> bufferViews;

						if (shaderType == ShaderTypeSkinned)
							bufferViews.emplace_back(instancingObjIdxBufferView);
						else
							bufferViews.emplace_back(instancingStandardBufferView[counter]);
						meshes[i]->Render(commandList, j, instancesNum, bufferViews);
					}
				}
			}
		}
	}
	if (shaderType == ShaderTypeStandard)
		counter++;

	if (sibling) sibling->Render(commandList, camera, instancesNum, instancingObjIdxBufferView, instancingStandardBufferView);
	if (child) child->Render(commandList, camera, instancesNum, instancingObjIdxBufferView, instancingStandardBufferView);

	if (parent == NULL)
		counter = 0;
}

void GameObject::BoundingRender(ID3D12GraphicsCommandList * commandList, Camera * camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingGameObjectBufferView)
{
	OnPrepareRender();

	if (boundingObject != NULL) {
		if (boundingMesh) {
			std::vector<D3D12_VERTEX_BUFFER_VIEW> bufferView{ instancingGameObjectBufferView };
			boundingMesh->Render(commandList, 0, instancesNum, bufferView);
		}
	}
}

void GameObject::SkinnedBoundingRender(ID3D12GraphicsCommandList * commandList, Camera * camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView)
{
	static int counter = 0;

	OnPrepareRender();

	if (shaderType == ShaderTypeBounding)
	{
		std::vector<D3D12_VERTEX_BUFFER_VIEW> bufferView{ instancingGameObjectBufferView[counter] };
		boundingMesh->Render(commandList, 0, instancesNum, bufferView);
		counter++;
	}


	if (sibling) sibling->SkinnedBoundingRender(commandList, camera, instancesNum, instancingGameObjectBufferView);
	if (child) child->SkinnedBoundingRender(commandList, camera, instancesNum, instancingGameObjectBufferView);

	if (parent == NULL)
		counter = 0;
}

void GameObject::TerrainRender(ID3D12GraphicsCommandList * commandList, Camera * camera, int LayerIndex, int idx)
{
	OnPrepareRender();

	if (parent == NULL)
		commandList->SetGraphicsRootDescriptorTable(GraphicsRootGameobject, cbvGPUDescriptorHandle[0]);
	else
	{
		if (type != ObjectTypePlayer)
			commandList->SetGraphicsRootDescriptorTable(GraphicsRootGameobject, cbvGPUDescriptorHandle[idx]);
	}
	
	if (materialsNum > 0)
	{
		for (int j = 0; j < materialsNum; ++j)
		{
			if (meshes)
			{
				for (int i = 0; i < meshesNum; i++)
				{
					if (meshes[i]) meshes[i]->Render(commandList, LayerIndex);
				}
			}
		}
	}


	if (sibling) sibling->Render(commandList, camera, idx);
	if (child) child->Render(commandList, camera, idx);

}

void GameObject::NaviRender(ID3D12GraphicsCommandList * commandList, Camera * camera)
{
	OnPrepareRender();
	if (parent == NULL)
		commandList->SetGraphicsRootDescriptorTable(GraphicsRootGameobject, cbvGPUDescriptorHandle[0]);

	if (naviMesh) {
		for (int i = 0; i < 4; ++i) {
			if (naviMesh[i].GetisCreate()) {
				naviMesh[i].Render(commandList);
			}
		}
	}
}

void GameObject::BillboardRender(ID3D12GraphicsCommandList* commandList, Camera* camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferView)
{
	OnPrepareRender();

	commandList->IASetVertexBuffers(0, 1, &instancingBufferView);
	commandList->IASetPrimitiveTopology(D3D_PRIMITIVE_TOPOLOGY_POINTLIST);
	commandList->DrawInstanced(1, instancesNum, 0, 0);
}

RotatingObject::RotatingObject(int meshesNum)
{
	this->meshesNum = meshesNum;
	rotationAxis = XMFLOAT3(0.0f, 1.0f, 0.0f);
	rotationSpeed = 90.0f;
}

RotatingObject::~RotatingObject()
{
}

void RotatingObject::Animate(float timeElapsed, XMFLOAT4X4* parent)
{
	//GameObject::Rotate(&rotationAxis, rotationSpeed * timeElapsed);
}

void GameObject::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList *commandList)
{
	UINT cbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
	cbGameObject = ::CreateBufferResource(device, commandList, NULL, cbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

	cbGameObject->Map(0, NULL, (void **)&cbMappedGameObject);
}
void GameObject::ReleaseShaderVariables()
{
	if (cbGameObject)
	{
		cbGameObject->Unmap(0, NULL);
		cbGameObject->Release();
	}
}

void GameObject::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList, int idx)
{
	// Player using ConstantBufferView
	if (type != ObjectTypePlayer)
	{
		if (parent == NULL)
			commandList->SetGraphicsRootDescriptorTable(GraphicsRootGameobject, cbvGPUDescriptorHandle[0]);
		else
			commandList->SetGraphicsRootDescriptorTable(GraphicsRootGameobject, cbvGPUDescriptorHandle[idx]);
	}
}

void GameObject::SetPosition(float x, float y, float z)
{
	transform._41 = x;
	transform._42 = y;
	transform._43 = z;

	UpdateTransform(NULL);
}
void GameObject::SetPosition(XMFLOAT3 position)
{
	SetPosition(position.x, position.y, position.z);
}
void GameObject::SetScale(float x, float y, float z)
{
	XMMATRIX scale = XMMatrixScaling(x, y, z);
	transform = Matrix4x4::Multiply(scale, transform);

	UpdateTransform(NULL);
}
void GameObject::SetScaleTransform(float x, float y, float z) 
{

	transform._11 = x;
	transform._22 = y;
	transform._33 = z;

	UpdateTransform(NULL);
}

void GameObject::SetLook(XMFLOAT3 look)
{
	transform._31 = look.x;
	transform._32 = look.y;
	transform._33 = look.z;

	UpdateTransform(NULL);
}
void GameObject::SetUp(XMFLOAT3 up)
{
	transform._21 = up.x;
	transform._22 = up.y;
	transform._23 = up.z;

	UpdateTransform(NULL);
}
void GameObject::SetRight(XMFLOAT3 right)
{
	transform._11 = right.x;
	transform._12 = right.y;
	transform._13 = right.z;

	UpdateTransform(NULL);

}
void GameObject::SettPos(XMFLOAT3 tPos)
{
	this->tPos = tPos;
}
void GameObject::SetWorld(XMFLOAT4X4 world)
{
	this->world = world;
}
void GameObject::SetTransform(XMFLOAT4X4 transform)
{
	this->transform = transform;
}
float& GameObject::GetScale()
{
	return fscale;
}
float GameObject::GetTransFormScale()
{
	return transform._11;
}
XMFLOAT3 GameObject::GetAllScale()
{
	return XMFLOAT3(allScale.x , allScale.y , allScale.z);
}
void GameObject::SetAllScale(float x, float y, float z)
{
	allScale.x = x;
	allScale.y = y;
	allScale.z = z;

}
XMFLOAT3& GameObject::GetInputAngle()
{
	return inputangle;

}
XMFLOAT3 GameObject::GetRotatingAngle()
{
	return rotatingAngle;
}
XMFLOAT3 GameObject::GetPosition()
{
	return(XMFLOAT3(world._41, world._42, world._43));
}
XMFLOAT3 GameObject::GettPos()
{
	return XMFLOAT3(tPos.x, tPos.y, tPos.z);
}
//게임 객체의 로컬 z-축 벡터를 반환한다.
XMFLOAT3 GameObject::GetLook()
{
	return(Vector3::Normalize(XMFLOAT3(world._31, world._32, world._33)));
}
//게임 객체의 로컬 y-축 벡터를 반환한다.
XMFLOAT3 GameObject::GetUp()
{
	return(Vector3::Normalize(XMFLOAT3(world._21, world._22, world._23)));
}
//게임 객체의 로컬 x-축 벡터를 반환한다.
XMFLOAT3 GameObject::GetRight()
{
	return(Vector3::Normalize(XMFLOAT3(world._11, world._12, world._13)));
}

XMFLOAT4X4 GameObject::GetWorld()
{
	return XMFLOAT4X4(world);
}
XMFLOAT4X4 GameObject::GetInverseWorld()
{
	XMFLOAT4X4 inverseWorld = Matrix4x4::Inverse(world);

	return inverseWorld;
}

XMFLOAT4X4 GameObject::GetTransform()
{
	return transform;
}

void GameObject::SetOriginalTransform()
{
	originalTransform = transform;
}

Material* GameObject::GetMaterial(int idx)
{
	return materials[idx];
}

//게임 객체를 로컬 x-축 방향으로 이동한다.
void GameObject::MoveStrafe(float distance)
{
	XMFLOAT3 position = GetPosition();
	XMFLOAT3 right = GetRight();
	position = Vector3::Add(position, right, distance);
	GameObject::SetPosition(position);
}
//게임 객체를 로컬 y-축 방향으로 이동한다.
void GameObject::MoveUp(float distance)
{
	XMFLOAT3 position = GetPosition();
	XMFLOAT3 up = GetUp();
	position = Vector3::Add(position, up, distance);
	GameObject::SetPosition(position);
}
//게임 객체를 로컬 z-축 방향으로 이동한다.
void GameObject::MoveForward(float distance)
{
	XMFLOAT3 position = GetPosition();
	XMFLOAT3 look = GetLook();
	position = Vector3::Add(position, look, distance);
	GameObject::SetPosition(position);
}
//게임 객체를 주어진 각도로 회전한다.
void GameObject::Rotate(float pitch, float yaw, float roll)
{
	rotatingAngle.x += fmodf(pitch, 360.0f);
	rotatingAngle.y += fmodf(yaw, 360.0f);
	rotatingAngle.z += fmodf(roll, 360.0f);

	XMMATRIX rotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(pitch), XMConvertToRadians(yaw), XMConvertToRadians(roll));
	transform = Matrix4x4::Multiply(rotate, transform);

	UpdateTransform(NULL);
}

void GameObject::Rotate(XMFLOAT3* axis, float angle)
{
	if (axis->x == 1.0f)
		rotatingAngle.x = fmodf(rotatingAngle.x + angle, 360.0f);
	if (axis->y == 1.0f)
		rotatingAngle.y = fmodf(rotatingAngle.y + angle, 360.0f);
	if (axis->z == 1.0f)
		rotatingAngle.z = fmodf(rotatingAngle.z + angle, 360.0f);

	XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(axis), XMConvertToRadians(angle));
	transform = Matrix4x4::Multiply(rotate, transform);

	UpdateTransform(NULL);
}

void GameObject::Rotate(XMFLOAT4 *quaternion)
{
	XMMATRIX rotate = XMMatrixRotationQuaternion(XMLoadFloat4(quaternion));
	transform = Matrix4x4::Multiply(rotate, transform);

	UpdateTransform(NULL);
}
void GameObject::Scale(float scale)
{
	this->fscale = scale * originalScale.x;
	transform._11 = scale * originalScale.x;
	transform._22 = scale * originalScale.y;
	transform._33 = scale * originalScale.z;

	UpdateTransform(NULL);
}

HeightMapTerrain::HeightMapTerrain(ID3D12Device *device, ID3D12GraphicsCommandList* commandList, LPCTSTR fileName, int width, int length, int blockWidth, int blockLength, XMFLOAT3 scale, XMFLOAT4 color) : GameObject(0)
{
	this->width = width;
	this->length = length;

	int cxQuadsPerBlock = blockWidth - 1;
	int czQuadsPerBlock = blockLength - 1;

	this->scale = scale;


	long cxBlocks = (this->width - 1) / cxQuadsPerBlock;
	long czBlocks = (this->length - 1) / czQuadsPerBlock;

	meshesNum = cxBlocks * czBlocks;

	meshes = new Mesh*[meshesNum];
	naviMesh = new NaviMesh[4];

	


	for (int i = 0; i < meshesNum; i++)
		meshes[i] = NULL;
	HeightMapGridMesh *heightMapGridMesh = NULL;
	for (int z = 0, zStart = 0; z < czBlocks; z++)
	{
		for (int x = 0, xStart = 0; x < cxBlocks; x++)
		{
			xStart = x * (blockWidth - 1);
			zStart = z * (blockLength - 1);
			heightMapGridMesh = new HeightMapGridMesh(device, commandList, xStart,
				zStart, blockWidth, blockLength, scale, color, NULL);
			SetMesh(x + (z*cxBlocks), heightMapGridMesh);
		}
	}



}
HeightMapTerrain::~HeightMapTerrain(void)
{
}

SuperCobraObject::SuperCobraObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature)
{
}

SuperCobraObject::~SuperCobraObject()
{
}

void SuperCobraObject::PrepareAnimate()
{
	mainRotorFrame = FindFrame("MainRotor");
	tailRotorFrame = FindFrame("TailRotor");
}

void SuperCobraObject::Animate(float timeElapsed, XMFLOAT4X4 *parent)
{
	if (mainRotorFrame)
	{
		XMMATRIX rotate = XMMatrixRotationY(XMConvertToRadians(360.0f * 4.0f) * timeElapsed);
		mainRotorFrame->SetTransform(Matrix4x4::Multiply(rotate, mainRotorFrame->GetTransform()));
	}
	if (tailRotorFrame)
	{
		XMMATRIX rotate = XMMatrixRotationX(XMConvertToRadians(360.0f * 4.0f) * timeElapsed);
		tailRotorFrame->SetTransform(Matrix4x4::Multiply(rotate, tailRotorFrame->GetTransform()));
	}

}

TreeObject::TreeObject()
{
}
TreeObject::~TreeObject()
{
}
bool TreeObject::IsFar(Camera* camera)
{
	float length = Vector3::Length(Vector3::Subtract(GetPosition(), camera->GetPosition()));
	if (length > LOD_VALUE)
	{
		return true;
	}
	return false;
}

BillboardObject::BillboardObject()
{
}

BillboardObject::~BillboardObject()
{
}

void BillboardObject::UpdateTransform(VS_VB_INSTANCE_BILLBOARD * vbMappedBillboardObjects, int idx)
{
	vbMappedBillboardObjects[idx].position = billboardVertex.position;
	vbMappedBillboardObjects[idx].instanceInfo = billboardVertex.billboardInfo;
}

bool BillboardObject::IsFar(Camera* camera)
{
	float length = Vector3::Length(Vector3::Subtract(billboardVertex.position, camera->GetPosition()));
	if (length < LOD_VALUE)
	{
		return false;
	}
	return true;
}

AnimationObject::AnimationObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature, LoadedModelInfo *model, int animationTracks)
{
	LoadedModelInfo *pAngrybotModel = model;

	SetChild(pAngrybotModel->modelRootObject, true);
	skinnedAnimationController = new AnimationController(device, commandList, animationTracks, pAngrybotModel);
}
AnimationObject::AnimationObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature)
{
}

AnimationObject::~AnimationObject()
{
}


void AnimationObject::UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, XMFLOAT4X4 *xmf4x4World)
{
	XMFLOAT4X4 temp4x4World;
	XMStoreFloat4x4(&temp4x4World, XMMatrixTranspose(XMLoadFloat4x4(xmf4x4World)));
	commandList->SetGraphicsRoot32BitConstants(18, 16, &temp4x4World, 0);
}