#include "stdafx.h"
#include "Scene.h"
#include "Player.h"
#include "Texture.h"
#include "ObjectShader.h"
#include "TerrainShader.h"
#include "Animation.h"

ID3D12DescriptorHeap*				Scene::cbvSrvUavDescriptorHeap = NULL;

D3D12_CPU_DESCRIPTOR_HANDLE			Scene::cbvCPUDescriptorStartHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::cbvGPUDescriptorStartHandle;
D3D12_CPU_DESCRIPTOR_HANDLE			Scene::srvCPUDescriptorStartHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::srvGPUDescriptorStartHandle;
D3D12_CPU_DESCRIPTOR_HANDLE			Scene::uavCPUDescriptorStartHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::uavGPUDescriptorStartHandle;
D3D12_CPU_DESCRIPTOR_HANDLE			Scene::cbvCPUDescriptorNextHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::cbvGPUDescriptorNextHandle;
D3D12_CPU_DESCRIPTOR_HANDLE			Scene::srvCPUDescriptorNextHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::srvGPUDescriptorNextHandle;
D3D12_CPU_DESCRIPTOR_HANDLE			Scene::uavCPUDescriptorNextHandle;
D3D12_GPU_DESCRIPTOR_HANDLE			Scene::uavGPUDescriptorNextHandle;

std::vector<SRVROOTARGUMENTINFO>	Scene::rootArgumentInfos;
int									Scene::rootArgumentCurrentIdx = 0;


Scene::Scene(ID3D12Resource* terrainDepthResource, ID3D12Resource* terrainAlphaResource0, XMFLOAT3 terrainScale, ID3D12Resource* terrainHeightResource, ID3D12Resource* terrainAlphaResource1 , ID3D12Resource* terrainAlphaResource2, ID3D12Resource* depthResource)
{
	graphicsRootSignature = NULL;
	this->terrainDepthResource = terrainDepthResource;
	this->terrainAlphaResource0 = terrainAlphaResource0;
	this->terrainScale = terrainScale;
	this->terrainHeightResource = terrainHeightResource;
	this->terrainAlphaResource1 = terrainAlphaResource1;
	this->terrainAlphaResource2 = terrainAlphaResource2;
	this->depthResource = depthResource;
}
Scene::~Scene()
{
}

void Scene::BuildLightsAndMaterials()
{
	lights = new LIGHTS;
	::ZeroMemory(lights, sizeof(LIGHTS));

	lights->lights[0].enable = true;
	lights->lights[0].type = DIRECTIONAL_LIGHT;
	lights->lights[0].position = XMFLOAT3{ 0.0f, 100.0f, 0.0f };
	lights->lights[0].ambient = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	lights->lights[0].diffuse = XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f);
	lights->lights[0].specular = XMFLOAT4(0.2f, 0.2f, 0.2f, 10.0f);
	lights->lights[0].direction = Vector3::Normalize(XMFLOAT3(0.0f, -1.0f, 0.0f));

	lights->globalAmbient = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	materials = new MATERIALS;
	::ZeroMemory(materials, sizeof(MATERIALS));

	materials->reflections[0] = { XMFLOAT4(0.3f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[1] = { XMFLOAT4(0.5f, 0.5f, 0.5f, 1.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f), XMFLOAT4(0.2f, 0.2f, 0.2f, 1.0f), XMFLOAT4(0.2f, 0.2f, 0.2f, 0.2f) };
	materials->reflections[2] = { XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 15.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[3] = { XMFLOAT4(0.5f, 0.0f, 1.0f, 1.0f), XMFLOAT4(0.0f, 0.5f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 20.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[4] = { XMFLOAT4(0.0f, 0.5f, 1.0f, 1.0f), XMFLOAT4(0.5f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 25.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[5] = { XMFLOAT4(0.0f, 0.5f, 0.5f, 1.0f), XMFLOAT4(0.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 30.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[6] = { XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f), XMFLOAT4(0.5f, 0.5f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 35.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };
	materials->reflections[7] = { XMFLOAT4(1.0f, 0.5f, 1.0f, 1.0f), XMFLOAT4(1.0f, 0.0f, 1.0f, 1.0f), XMFLOAT4(1.0f, 1.0f, 1.0f, 40.0f), XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f) };

	for (int i = 0; i < materialsNum; ++i)
	{
		materials->reflections[8 + i] = materialReflection[i];
	}
}

void Scene::BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	graphicsRootSignature = CreateGraphicsRootSignature(device);
	computeRootSignature = CreateComputeRootSignature(device);

	Material::PrepareShaders(device, commandList, graphicsRootSignature);

	GameObject* treeModel0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Nature/tree_aspen_medium_a.bin", "Nature");
	GameObject* watchTowerModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WatchTower/WatchTower.bin", "WatchTower");
	GameObject* farmerHouseModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/FarmHouse/FarmHouse.bin", "FarmHouse");
	GameObject* oldHouseModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/OldHouse/OldHouse.bin", "OldHouse");
	GameObject* concreteBuilding0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/ConcreteBuilding0/Building_Example_1.bin", "UnityPack0");
	GameObject* factoryModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Factory/factory.bin", "Factory");
	GameObject* redRoofModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/RedRoofHouse/RedRoofHouse.bin", "RedRoofHouse");
	GameObject* ruins0Model = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Ruins0/ruins.bin", "Ruins0");
	GameObject* wireFenceModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WireFence/wireFence.bin", "WireFence");
	GameObject* rustStoreModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/RustStoreHouse/rustStoreHouse.bin", "RustStoreHouse");
	GameObject* castleModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Castle/castle.bin", "Castle");
	GameObject* stoneModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Stone/Stone_Pack1_Stone_1.bin", "Stone");
	GameObject* treeModel1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Nature/tree_aspen_medium_b.bin", "Nature");
	GameObject* treeModel2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Nature/tree_aspen_small_b.bin", "Nature");
	GameObject* treeModel3 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Nature/TreeAbundantLeaf.bin", "Nature");
	GameObject* treeModel4 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Nature/TreeWithOutLeaf.bin", "Nature");
	GameObject* containerModel1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Containers_1A2.bin", "Container");
	GameObject* wareHouseModel = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WareHouse/wareHouse.bin", "WareHouse");
	GameObject* busModel = GameObject::LoadGeometryFromFile(device, commandList,  graphicsRootSignature, "Assets/Model/Car/Bus.bin", "Car");
	GameObject* militaryCar = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Car/MilitaryCar.bin", "Car");
	GameObject* redCar = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Car/RedCar.bin", "Car");

	//새로운 unity pack 파일 

	GameObject* woodBeam0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WoodBeam/Wooden_Beam_1A1.bin", "UnityPack0");
	GameObject* woodBeam1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WoodBeam/Wooden_Beam_1A2.bin", "UnityPack0");
	GameObject* woodPallet = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WoodPallet/Pallet_Stack_1B2.bin", "UnityPack0");
	GameObject* woodPlatform0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WoodPlatform/Wooden_Platform_1A1.bin", "UnityPack0");
	GameObject* woodPlatform1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WoodPlatform/Wooden_Platform_1A3.bin", "UnityPack0");
	GameObject* woodStack0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WoodStack/Log_Stack_1A1.bin", "UnityPack0");
	GameObject* woodStack1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/WoodStack/Log_Stack_1A2.bin", "UnityPack0");
	GameObject* smallFence = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/SmallFence/Railing_Small_1A1.bin", "UnityPack0");
	GameObject* scaffold = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Scaffold/Scaffolding_1A2.bin", "UnityPack0");
	GameObject* brickStack0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Concrete/Brick_Stack_1A1.bin", "UnityPack0");
	GameObject* brickStack1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Concrete/Brick_Stack_1A2.bin", "UnityPack0");
	GameObject* brickStack2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Concrete/Brick_Stack_1A3.bin", "UnityPack0");
	GameObject* concreteBarrier = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Concrete/Concrete_Barrier_1A1.bin", "UnityPack0");
	GameObject* concreteBrickGroup0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Concrete/Concrete_Brick_Group_1A1.bin", "UnityPack0");
	GameObject* concreteBrickGroup1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Concrete/Concrete_Brick_Group_1A2.bin", "UnityPack0");
	GameObject* concreteBrickGroup2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Concrete/Concrete_Brick_Group_1A3.bin", "UnityPack0");
	GameObject* concreteBrickGroup3 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Concrete/Concrete_Brick_Group_1A4.bin", "UnityPack0");

	GameObject* brokenWall0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/SmallWall/Broken_Wall_1A1.bin", "UnityPack0");
	GameObject* brokenWall1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/SmallWall/Broken_Wall_1A2.bin", "UnityPack0");
	GameObject* brokenWall2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/SmallWall/Broken_Wall_1B1.bin", "UnityPack0");
	GameObject* brokenWall3 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/SmallWall/Broken_Wall_1B2.bin", "UnityPack0");
	GameObject* brokenWall4 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/SmallWall/Broken_Wall_1C1.bin", "UnityPack0");

	GameObject* cityWall0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/SmallWall/City_Wall_1A1.bin", "UnityPack0");
	GameObject* cityWall1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/SmallWall/City_Wall_1A2.bin", "UnityPack0");
	GameObject* cityWallPillar = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/SmallWall/City_Wall_Pillar_1A1.bin", "UnityPack0");

	GameObject* lamp = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Lamp/Light_1A1.bin", "UnityPack0");
	GameObject* tapestry0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Tapestry/Tapestry_1A1.bin", "UnityPack0");
	GameObject* tapestry1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Tapestry/Tapestry_1A2.bin", "UnityPack0");
	GameObject* awning0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Awning/Awning_1A1.bin", "UnityPack0");
	GameObject* awning1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Awning/Awning_1A2.bin", "UnityPack0");

	GameObject* containerClose0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Closed_1A1.bin", "UnityPack0");
	GameObject* containerClose1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Closed_1A2.bin", "UnityPack0");
	GameObject* containerClose2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Closed_1A3.bin", "UnityPack0");
	GameObject* containerClose3 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Closed_1A4.bin", "UnityPack0");
	GameObject* containerFill0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Filled_1A1.bin", "UnityPack0");
	GameObject* containerFill1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Filled_1A2.bin", "UnityPack0");
	GameObject* containerFill2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Filled_1A3.bin", "UnityPack0");
	GameObject* containerFill3 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Filled_1A4.bin", "UnityPack0");
	GameObject* containerOpen0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Open_1A1_2.bin", "UnityPack0");
	GameObject* containerOpen1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Open_1A2_2.bin", "UnityPack0");
	GameObject* containerOpen2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Open_1A3_2.bin", "UnityPack0");
	GameObject* containerOpen3 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Container/Container_Open_1A4_2.bin", "UnityPack0");

	GameObject* bigWallMain0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/BigWall/Fortification_Main_Gate_1A1.bin", "UnityPack0");
	GameObject* bigWallMain1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/BigWall/Fortification_Main_Gate_1A2.bin", "UnityPack0");
	GameObject* bigWallPillar0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/BigWall/Fortification_Pillar_1A1.bin", "UnityPack0");
	GameObject* bigWallPillar1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/BigWall/Fortification_Pillar_1A2.bin", "UnityPack0");
	GameObject* bigWallPillar2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/BigWall/Fortification_Pillar_1A3.bin", "UnityPack0");
	GameObject* bigWall0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/BigWall/Fortification_Wall_1A1.bin", "UnityPack0");
	GameObject* bigWall1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/BigWall/Fortification_Wall_1A2.bin", "UnityPack0");
	GameObject* tire = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Tire/Tire_1A1.bin", "UnityPack0");
	GameObject* barrel0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Barrel/Barrel_Group_1A1.bin", "UnityPack0");
	GameObject* barrel1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Barrel/Barrel_Group_1A2.bin", "UnityPack0");
	GameObject* barrel2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Barrel/Barrel_Group_1A3.bin", "UnityPack0");
	GameObject* barrelOpen = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Barrel/Barrel_Open_1A2.bin", "UnityPack0");
	
	GameObject* pipe0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Pipe/Concrete_Pipe_1A1.bin", "UnityPack0");
	GameObject* pipe1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Pipe/Concrete_Pipe_1B1.bin", "UnityPack0");

	GameObject* pipeGroup0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Pipe/Concrete_Pipe_Group_1A1.bin", "UnityPack0");
	GameObject* pipeGroup1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Pipe/Concrete_Pipe_Group_1B1.bin", "UnityPack0");
	GameObject* sign0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A1.bin", "UnityPack0");
	GameObject* sign1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A2.bin", "UnityPack0");
	GameObject* sign2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A3.bin", "UnityPack0");
	GameObject* sign3 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A4.bin", "UnityPack0");
	GameObject* sign4 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A5.bin", "UnityPack0");
	GameObject* sign5 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A6.bin", "UnityPack0");
	GameObject* sign6 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A7.bin", "UnityPack0");
	GameObject* sign7 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A8.bin", "UnityPack0");
	GameObject* sign8 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A9.bin", "UnityPack0");
	GameObject* sign9 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Sign_1A10.bin", "UnityPack0");

	GameObject* metalBorad0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Metal_Board_1A4.bin", "UnityPack0");
	GameObject* metalBorad1 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Metal_Board_1B1.bin", "UnityPack0");
	GameObject* metalBorad2 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Sign/Metal_Board_1B2.bin", "UnityPack0");
	GameObject* vent = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Vent/Vent_1A1.bin", "UnityPack0");

	GameObject* grass0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Grass/free_grass.bin", "Grass");
	grass0->SetPipelineStatesNum(InstanceShaderPS_Grass);
	GameObject* lamp0 = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Lamp/StreetLamp.bin", "Lamp");
	GameObject* arrow = GameObject::LoadGeometryFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Human/Arrow.bin", "Human");


	//스킨 메쉬 
	LoadedModelInfo* rifleModel = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Human/6GunMan.bin", "Human");
	LoadedModelInfo* swordModel = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Human/6SwordMan.bin", "Human");
	LoadedModelInfo* bowModel = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Human/6BowGirl.bin", "Human");

	LoadedModelInfo* bigPoliceZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/CopZombie.bin", "Zombie");
	LoadedModelInfo* derrickZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/DerrickZombie.bin", "Zombie");
	LoadedModelInfo* jillZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/JillZombie.bin", "Zombie");
	LoadedModelInfo* starkieZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/ParasiteZombie.bin", "Zombie");
	LoadedModelInfo* womanZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/GirlZombie.bin", "Zombie");
	LoadedModelInfo* pumpKinZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/HulkZombie.bin", "Zombie");
	LoadedModelInfo* warZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/WarZombie.bin", "Zombie");
	LoadedModelInfo* whiteclownZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/WhiteclownZombie.bin", "Zombie");
	LoadedModelInfo* igniteZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/YakuzaZombie.bin", "Zombie");
	LoadedModelInfo* baseZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/BaseZombie.bin", "Zombie");
	LoadedModelInfo* baldZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/BaldZombie.bin", "Zombie");
	LoadedModelInfo* bossZombie = GameObject::LoadGeometryAndAnimationFromFile(device, commandList, graphicsRootSignature, "Assets/Model/Zombie/Mutant.bin", "Zombie");


	treeModel0->GetHierarchyMaterial(materialReflection, materialsNum, treeModel0->textureName);
	watchTowerModel->GetHierarchyMaterial(materialReflection, materialsNum, watchTowerModel->textureName);
	farmerHouseModel->GetHierarchyMaterial(materialReflection, materialsNum, farmerHouseModel->textureName);
	oldHouseModel->GetHierarchyMaterial(materialReflection, materialsNum, oldHouseModel->textureName);
	concreteBuilding0->GetHierarchyMaterial(materialReflection, materialsNum, concreteBuilding0->textureName);
	factoryModel->GetHierarchyMaterial(materialReflection, materialsNum, factoryModel->textureName);
	redRoofModel->GetHierarchyMaterial(materialReflection, materialsNum, redRoofModel->textureName);
	ruins0Model->GetHierarchyMaterial(materialReflection, materialsNum, ruins0Model->textureName);
	wireFenceModel->GetHierarchyMaterial(materialReflection, materialsNum, wireFenceModel->textureName);
	rustStoreModel->GetHierarchyMaterial(materialReflection, materialsNum, rustStoreModel->textureName);
	castleModel->GetHierarchyMaterial(materialReflection, materialsNum, castleModel->textureName);
	stoneModel->GetHierarchyMaterial(materialReflection, materialsNum, stoneModel->textureName);
	treeModel1->GetHierarchyMaterial(materialReflection, materialsNum, treeModel1->textureName);
	treeModel2->GetHierarchyMaterial(materialReflection, materialsNum, treeModel2->textureName);
	treeModel3->GetHierarchyMaterial(materialReflection, materialsNum, treeModel3->textureName);
	treeModel4->GetHierarchyMaterial(materialReflection, materialsNum, treeModel4->textureName);
	containerModel1->GetHierarchyMaterial(materialReflection, materialsNum, containerModel1->textureName);
	wareHouseModel->GetHierarchyMaterial(materialReflection, materialsNum, wareHouseModel->textureName);
	busModel->GetHierarchyMaterial(materialReflection, materialsNum, busModel->textureName);
	militaryCar->GetHierarchyMaterial(materialReflection, materialsNum, militaryCar->textureName);
	redCar->GetHierarchyMaterial(materialReflection, materialsNum, redCar->textureName);

	woodBeam0->GetHierarchyMaterial(materialReflection, materialsNum, woodBeam0->textureName);
	woodBeam1->GetHierarchyMaterial(materialReflection, materialsNum, woodBeam1->textureName);
	woodPallet->GetHierarchyMaterial(materialReflection, materialsNum, woodPallet->textureName);
	woodPlatform0->GetHierarchyMaterial(materialReflection, materialsNum, woodPlatform0->textureName);
	woodPlatform1->GetHierarchyMaterial(materialReflection, materialsNum, woodPlatform1->textureName);
	woodStack0->GetHierarchyMaterial(materialReflection, materialsNum, woodStack0->textureName);
	woodStack1->GetHierarchyMaterial(materialReflection, materialsNum, woodStack1->textureName);
	smallFence->GetHierarchyMaterial(materialReflection, materialsNum, smallFence->textureName);
	scaffold->GetHierarchyMaterial(materialReflection, materialsNum, scaffold->textureName);
	brickStack0->GetHierarchyMaterial(materialReflection, materialsNum, brickStack0->textureName);
	brickStack1->GetHierarchyMaterial(materialReflection, materialsNum, brickStack1->textureName);
	brickStack2->GetHierarchyMaterial(materialReflection, materialsNum, brickStack2->textureName);
	concreteBarrier->GetHierarchyMaterial(materialReflection, materialsNum, concreteBarrier->textureName);
	concreteBrickGroup0->GetHierarchyMaterial(materialReflection, materialsNum, concreteBrickGroup0->textureName);
	concreteBrickGroup1->GetHierarchyMaterial(materialReflection, materialsNum, concreteBrickGroup1->textureName);
	concreteBrickGroup2->GetHierarchyMaterial(materialReflection, materialsNum, concreteBrickGroup2->textureName);
	concreteBrickGroup3->GetHierarchyMaterial(materialReflection, materialsNum, concreteBrickGroup3->textureName);

	brokenWall0->GetHierarchyMaterial(materialReflection, materialsNum, brokenWall0->textureName);
	brokenWall1->GetHierarchyMaterial(materialReflection, materialsNum, brokenWall1->textureName);
	brokenWall2->GetHierarchyMaterial(materialReflection, materialsNum, brokenWall2->textureName);
	brokenWall3->GetHierarchyMaterial(materialReflection, materialsNum, brokenWall3->textureName);
	brokenWall4->GetHierarchyMaterial(materialReflection, materialsNum, brokenWall4->textureName);
	cityWall0->GetHierarchyMaterial(materialReflection, materialsNum, cityWall0->textureName);
	cityWall1->GetHierarchyMaterial(materialReflection, materialsNum, cityWall1->textureName);
	cityWallPillar->GetHierarchyMaterial(materialReflection, materialsNum, cityWallPillar->textureName);

	lamp->GetHierarchyMaterial(materialReflection, materialsNum, lamp->textureName);
	tapestry0->GetHierarchyMaterial(materialReflection, materialsNum, tapestry0->textureName);
	tapestry1->GetHierarchyMaterial(materialReflection, materialsNum, tapestry1->textureName);
	awning0->GetHierarchyMaterial(materialReflection, materialsNum, awning0->textureName);
	awning1->GetHierarchyMaterial(materialReflection, materialsNum, awning1->textureName);
	containerClose0->GetHierarchyMaterial(materialReflection, materialsNum, containerClose0->textureName);
	containerClose1->GetHierarchyMaterial(materialReflection, materialsNum, containerClose1->textureName);
	containerClose2->GetHierarchyMaterial(materialReflection, materialsNum, containerClose2->textureName);
	containerClose3->GetHierarchyMaterial(materialReflection, materialsNum, containerClose3->textureName);
	containerFill0->GetHierarchyMaterial(materialReflection, materialsNum, containerFill0->textureName);
	containerFill1->GetHierarchyMaterial(materialReflection, materialsNum, containerFill1->textureName);
	containerFill2->GetHierarchyMaterial(materialReflection, materialsNum, containerFill2->textureName);
	containerFill3->GetHierarchyMaterial(materialReflection, materialsNum, containerFill3->textureName);
	containerOpen0->GetHierarchyMaterial(materialReflection, materialsNum, containerOpen0->textureName);
	containerOpen1->GetHierarchyMaterial(materialReflection, materialsNum, containerOpen1->textureName);
	containerOpen2->GetHierarchyMaterial(materialReflection, materialsNum, containerOpen2->textureName);
	containerOpen3->GetHierarchyMaterial(materialReflection, materialsNum, containerOpen3->textureName);
	bigWallMain0->GetHierarchyMaterial(materialReflection, materialsNum, bigWallMain0->textureName);
	bigWallMain1->GetHierarchyMaterial(materialReflection, materialsNum, bigWallMain1->textureName);
	bigWallPillar0->GetHierarchyMaterial(materialReflection, materialsNum, bigWallPillar0->textureName);
	bigWallPillar1->GetHierarchyMaterial(materialReflection, materialsNum, bigWallPillar1->textureName);
	bigWallPillar2->GetHierarchyMaterial(materialReflection, materialsNum, bigWallPillar2->textureName);
	bigWall0->GetHierarchyMaterial(materialReflection, materialsNum, bigWall0->textureName);
	bigWall1->GetHierarchyMaterial(materialReflection, materialsNum, bigWall1->textureName);
	tire->GetHierarchyMaterial(materialReflection, materialsNum, tire->textureName);
	barrel0->GetHierarchyMaterial(materialReflection, materialsNum, barrel0->textureName);
	barrel1->GetHierarchyMaterial(materialReflection, materialsNum, barrel1->textureName);
	barrel2->GetHierarchyMaterial(materialReflection, materialsNum, barrel2->textureName);
	barrelOpen->GetHierarchyMaterial(materialReflection, materialsNum, barrelOpen->textureName);
	pipe0->GetHierarchyMaterial(materialReflection, materialsNum, pipe0->textureName);
	pipe1->GetHierarchyMaterial(materialReflection, materialsNum, pipe1->textureName);
	pipeGroup0->GetHierarchyMaterial(materialReflection, materialsNum, pipeGroup0->textureName);
	pipeGroup1->GetHierarchyMaterial(materialReflection, materialsNum, pipeGroup1->textureName);
	sign0->GetHierarchyMaterial(materialReflection, materialsNum, sign0->textureName);
	sign1->GetHierarchyMaterial(materialReflection, materialsNum, sign1->textureName);
	sign2->GetHierarchyMaterial(materialReflection, materialsNum, sign2->textureName);
	sign3->GetHierarchyMaterial(materialReflection, materialsNum, sign3->textureName);
	sign4->GetHierarchyMaterial(materialReflection, materialsNum, sign4->textureName);
	sign5->GetHierarchyMaterial(materialReflection, materialsNum, sign5->textureName);
	sign6->GetHierarchyMaterial(materialReflection, materialsNum, sign6->textureName);
	sign7->GetHierarchyMaterial(materialReflection, materialsNum, sign7->textureName);
	sign8->GetHierarchyMaterial(materialReflection, materialsNum, sign8->textureName);
	sign9->GetHierarchyMaterial(materialReflection, materialsNum, sign9->textureName);
	metalBorad0->GetHierarchyMaterial(materialReflection, materialsNum, metalBorad0->textureName);
	metalBorad1->GetHierarchyMaterial(materialReflection, materialsNum, metalBorad1->textureName);
	metalBorad2->GetHierarchyMaterial(materialReflection, materialsNum, metalBorad2->textureName);
	vent->GetHierarchyMaterial(materialReflection, materialsNum, vent->textureName);

	// NO Unity2
	grass0->GetHierarchyMaterial(materialReflection, materialsNum, grass0->textureName);
	lamp0->GetHierarchyMaterial(materialReflection, materialsNum, lamp0->textureName);
	arrow->GetHierarchyMaterial(materialReflection, materialsNum, arrow->textureName);

	//스킨 메쉬 
	rifleModel->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, rifleModel->modelRootObject->textureName);
	swordModel->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, swordModel->modelRootObject->textureName);
	bowModel->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, bowModel->modelRootObject->textureName);

	bigPoliceZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, bigPoliceZombie->modelRootObject->textureName);
	derrickZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, derrickZombie->modelRootObject->textureName);
	jillZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, jillZombie->modelRootObject->textureName);
	starkieZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, starkieZombie->modelRootObject->textureName);
	womanZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, womanZombie->modelRootObject->textureName);
	pumpKinZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, pumpKinZombie->modelRootObject->textureName);
	warZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, warZombie->modelRootObject->textureName);
	whiteclownZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, whiteclownZombie->modelRootObject->textureName);
	igniteZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, igniteZombie->modelRootObject->textureName);
	baseZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, baseZombie->modelRootObject->textureName);
	baldZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, baldZombie->modelRootObject->textureName);
	bossZombie->modelRootObject->GetHierarchyMaterial(materialReflection, materialsNum, bossZombie->modelRootObject->textureName);

	int terrainObjectsNum = 1;
	int cubeObjectsNum = 5;
	int helicopterObjectsNum = 120 * 50;

	// 인스턴싱하지 않고 오브젝트를 렌더링할 때 사용되는 CBV의 갯수

	int terrainTexturesNum = 10;
	int cubeTexturesNum = 2;
	int objectTexturesNum = 300;
	int billboardTexturesNum = 1;
	int particleTexutresNum = 2;
	int decalTextureNum = 1;
	int skyBoxtextureNum = 18;

	objectsNum = terrainObjectsNum + cubeObjectsNum + helicopterObjectsNum;
	texturesNum = terrainTexturesNum + cubeTexturesNum + objectTexturesNum + billboardTexturesNum + particleTexutresNum + decalTextureNum + skyBoxtextureNum;

	CreateCbvSrvUavDescriptorHeaps(device, commandList, objectsNum, texturesNum, 0);
	CreateTextureResource(device, commandList);
	// 게임에 사용되는 텍스쳐들을 모두 Load하는 함수

	XMFLOAT3 scale{ 2.0f, 2.0f, 2.0f };

	shadersNum = 8;
	shaders = new Shader*[shadersNum];
	shaders[TERRAINSHADER] = new TerrainShader(scale);
	shaders[TERRAINSHADER]->CreateShader(device, graphicsRootSignature);
	shaders[TERRAINSHADER]->BuildTerrain(device, commandList, graphicsRootSignature, terrainAlphaResource0);
	// Terrain Build
	terrain = (HeightMapTerrain*)shaders[0]->GetObjects(0);
	// terrain Object Get

	objects.emplace_back(terrain);
	objects.emplace_back(treeModel0);
	objects.emplace_back(watchTowerModel);
	objects.emplace_back(farmerHouseModel);
	objects.emplace_back(oldHouseModel);
	objects.emplace_back(concreteBuilding0);
	objects.emplace_back(factoryModel);
	objects.emplace_back(redRoofModel);
	objects.emplace_back(ruins0Model);
	objects.emplace_back(wireFenceModel);
	objects.emplace_back(rustStoreModel);
	objects.emplace_back(castleModel);
	objects.emplace_back(stoneModel);
	objects.emplace_back(treeModel1);
	objects.emplace_back(treeModel2);
	objects.emplace_back(treeModel3);
	objects.emplace_back(treeModel4);
	objects.emplace_back(containerModel1);
	objects.emplace_back(wareHouseModel);
	objects.emplace_back(busModel);
	objects.emplace_back(militaryCar);
	objects.emplace_back(redCar);

	objects.emplace_back(woodBeam0);
	objects.emplace_back(woodBeam1);
	objects.emplace_back(woodPallet);
	objects.emplace_back(woodPlatform0);
	objects.emplace_back(woodPlatform1);
	objects.emplace_back(woodStack0);
	objects.emplace_back(woodStack1);
	objects.emplace_back(smallFence);
	objects.emplace_back(scaffold);
	objects.emplace_back(brickStack0);
	objects.emplace_back(brickStack1);
	objects.emplace_back(brickStack2);
	objects.emplace_back(concreteBarrier);
	objects.emplace_back(concreteBrickGroup0);
	objects.emplace_back(concreteBrickGroup1);
	objects.emplace_back(concreteBrickGroup2);
	objects.emplace_back(concreteBrickGroup3);
	objects.emplace_back(brokenWall0);
	objects.emplace_back(brokenWall1);
	objects.emplace_back(brokenWall2);
	objects.emplace_back(brokenWall3);
	objects.emplace_back(brokenWall4);
	objects.emplace_back(cityWall0);
	objects.emplace_back(cityWall1);
	objects.emplace_back(cityWallPillar);

	objects.emplace_back(lamp);
	objects.emplace_back(tapestry0);
	objects.emplace_back(tapestry1);
	objects.emplace_back(awning0);
	objects.emplace_back(awning1);
	objects.emplace_back(containerClose0);
	objects.emplace_back(containerClose1);
	objects.emplace_back(containerClose2);
	objects.emplace_back(containerClose3);
	objects.emplace_back(containerFill0);
	objects.emplace_back(containerFill1);
	objects.emplace_back(containerFill2);
	objects.emplace_back(containerFill3);
	objects.emplace_back(containerOpen0);
	objects.emplace_back(containerOpen1);
	objects.emplace_back(containerOpen2);
	objects.emplace_back(containerOpen3);
	objects.emplace_back(bigWallMain0);
	objects.emplace_back(bigWallMain1);
	objects.emplace_back(bigWallPillar0);
	objects.emplace_back(bigWallPillar1);
	objects.emplace_back(bigWallPillar2);
	objects.emplace_back(bigWall0);
	objects.emplace_back(bigWall1);

	objects.emplace_back(tire);
	objects.emplace_back(barrel0);
	objects.emplace_back(barrel1);
	objects.emplace_back(barrel2);
	objects.emplace_back(barrelOpen);
	objects.emplace_back(pipe0);
	objects.emplace_back(pipe1);
	objects.emplace_back(pipeGroup0);
	objects.emplace_back(pipeGroup1);
	objects.emplace_back(sign0);
	objects.emplace_back(sign1);
	objects.emplace_back(sign2);
	objects.emplace_back(sign3);
	objects.emplace_back(sign4);
	objects.emplace_back(sign5);
	objects.emplace_back(sign6);
	objects.emplace_back(sign7);
	objects.emplace_back(sign8);
	objects.emplace_back(sign9);
	objects.emplace_back(metalBorad0);
	objects.emplace_back(metalBorad1);
	objects.emplace_back(metalBorad2);
	objects.emplace_back(vent);

	// no Unity2
	objects.emplace_back(grass0);
	objects.emplace_back(lamp0);
	objects.emplace_back(arrow);

	fobjectsSize = objects.size();
	//skinMesh
	objects.emplace_back(rifleModel);
	objects.emplace_back(swordModel);
	objects.emplace_back(bowModel);

	objects.emplace_back(bigPoliceZombie);
	objects.emplace_back(derrickZombie);
	objects.emplace_back(jillZombie);
	objects.emplace_back(starkieZombie);
	objects.emplace_back(womanZombie);
	objects.emplace_back(pumpKinZombie);
	objects.emplace_back(warZombie);
	objects.emplace_back(whiteclownZombie);
	objects.emplace_back(igniteZombie);
	objects.emplace_back(baseZombie);
	objects.emplace_back(baldZombie);
	objects.emplace_back(bossZombie);


	shaders[INSTANCSHADER] = new InstancingShader();
	shaders[INSTANCSHADER]->CreateShader(device, graphicsRootSignature);
	shaders[INSTANCSHADER]->BuildObjects(device, commandList, graphicsRootSignature, objects , fobjectsSize);

	shaders[ASSISTSHADER] = new AssistShader();
	shaders[ASSISTSHADER]->CreateShader(device, graphicsRootSignature);
	shaders[ASSISTSHADER]->BuildObjects(device, commandList, graphicsRootSignature, terrain);

	shaders[SCREENSPACEDECALSHADER] = new ScreenSpaceDecalShader(depthStencilBuffer);
	shaders[SCREENSPACEDECALSHADER]->CreateShader(device, graphicsRootSignature);
	shaders[SCREENSPACEDECALSHADER]->BuildObjects(device, commandList, graphicsRootSignature, terrainDepthResource);

	shaders[SKYMAPSHADER] = new SkyBoxShader();
	shaders[SKYMAPSHADER]->CreateShader(device, graphicsRootSignature);
	shaders[SKYMAPSHADER]->BuildObjects(device, commandList, graphicsRootSignature, NULL);

	shaders[BOUNDINGSHADER] = new BoundingBoxShader();
	shaders[BOUNDINGSHADER]->CreateShader(device, graphicsRootSignature);
	shaders[BOUNDINGSHADER]->BuildObjects(device, commandList, graphicsRootSignature, terrain);

	shaders[SKINEDINSTANCSHADER] = new SkinnedInstancingShader();
	shaders[SKINEDINSTANCSHADER]->CreateShader(device, graphicsRootSignature);
	shaders[SKINEDINSTANCSHADER]->BuildObjects(device, commandList, graphicsRootSignature, objects , fobjectsSize);

	shaders[PARTICLESHADER] = new ParticleShader();
	shaders[PARTICLESHADER]->CreateShader(device, graphicsRootSignature, computeRootSignature);
	shaders[PARTICLESHADER]->BuildObjects(device, commandList, graphicsRootSignature, objects, fobjectsSize);

	BuildLightsAndMaterials();
	CreateShaderVariables(device, commandList);

}

void Scene::ReleaseObjects()
{
	if (graphicsRootSignature) graphicsRootSignature->Release();
	if (computeRootSignature) computeRootSignature->Release();
	if (cbvSrvUavDescriptorHeap) cbvSrvUavDescriptorHeap->Release();

	if (shaders)
	{
		for (int i = 0; i < shadersNum; i++)
		{
			if (shaders[i])
			{
				shaders[i]->ReleaseObjects();
				shaders[i]->Release();
			}
		}
	}

	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->Release();
	}

	if (shaders) delete[] shaders;
	if (lights) delete lights;
	if (materials) delete materials;
}

void Scene::ReleaseUploadBuffers()
{
	if (shaders)
	{
		for (int i = 0; i < shadersNum; i++)
		{
			if (shaders[i])
			{
				shaders[i]->ReleaseUploadBuffers();
			}
		}
	}

	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->ReleaseUploadBuffers();
	}

	if (terrain) terrain->ReleaseUploadBuffers();
	if (materialUploadBuffer) materialUploadBuffer->Release();
}

ID3D12RootSignature *Scene::GetGraphicsRootSignature()
{
	return(graphicsRootSignature);
}

ID3D12RootSignature *Scene::CreateGraphicsRootSignature(ID3D12Device* device)
{
	ID3D12RootSignature* graphicsRootSignature = NULL;

	D3D12_DESCRIPTOR_RANGE descriptorRanges[14];

	descriptorRanges[0].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_CBV;
	descriptorRanges[0].NumDescriptors = 1;
	descriptorRanges[0].BaseShaderRegister = 2; // b2 : GameObjectInfo
	descriptorRanges[0].RegisterSpace = 0;
	descriptorRanges[0].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[1].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[1].NumDescriptors = 4;
	descriptorRanges[1].BaseShaderRegister = 0; // t0 , t1 , t2 , t3
	descriptorRanges[1].RegisterSpace = 0;
	descriptorRanges[1].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[2].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[2].NumDescriptors = 10;
	descriptorRanges[2].BaseShaderRegister = 4; // t3: gtxtDiffuseTexture
	descriptorRanges[2].RegisterSpace = 0;
	descriptorRanges[2].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[3].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[3].NumDescriptors = 10;
	descriptorRanges[3].BaseShaderRegister = 14; // t20: gtxtNormalTexture
	descriptorRanges[3].RegisterSpace = 0;
	descriptorRanges[3].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[4].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[4].NumDescriptors = 10;
	descriptorRanges[4].BaseShaderRegister = 24; // t30: gtxtSpecularTexture
	descriptorRanges[4].RegisterSpace = 0;
	descriptorRanges[4].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[5].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[5].NumDescriptors = 10;
	descriptorRanges[5].BaseShaderRegister = 34; // t40: gtxtMetallicTexture
	descriptorRanges[5].RegisterSpace = 0;
	descriptorRanges[5].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[6].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[6].NumDescriptors = 10;
	descriptorRanges[6].BaseShaderRegister = 44; // t50: gtxtEmissionTexture
	descriptorRanges[6].RegisterSpace = 0;
	descriptorRanges[6].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[7].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[7].NumDescriptors = 10;
	descriptorRanges[7].BaseShaderRegister = 54; // t60: gtxtBillboardTexture
	descriptorRanges[7].RegisterSpace = 0;
	descriptorRanges[7].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[8].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[8].NumDescriptors = 10;
	descriptorRanges[8].BaseShaderRegister = 64; // t70: gtxtParticleTexture
	descriptorRanges[8].RegisterSpace = 0;
	descriptorRanges[8].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[9].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[9].NumDescriptors = 2;
	descriptorRanges[9].BaseShaderRegister = 74;// , t73 : depthTexure 
	descriptorRanges[9].RegisterSpace = 0;
	descriptorRanges[9].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[10].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[10].NumDescriptors = 3;
	descriptorRanges[10].BaseShaderRegister = 77; // t76: gtxtSkyBoxTexture
	descriptorRanges[10].RegisterSpace = 0;
	descriptorRanges[10].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[11].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[11].NumDescriptors = 12;
	descriptorRanges[11].BaseShaderRegister = 80; // t79: NormalTexture 
	descriptorRanges[11].RegisterSpace = 0;
	descriptorRanges[11].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	// t92 : bone Transform

	descriptorRanges[12].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[12].NumDescriptors = 12;
	descriptorRanges[12].BaseShaderRegister = 93; // t88: normalTexture
	descriptorRanges[12].RegisterSpace = 0;
	descriptorRanges[12].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;

	descriptorRanges[13].RangeType = D3D12_DESCRIPTOR_RANGE_TYPE_SRV;
	descriptorRanges[13].NumDescriptors = 1;
	descriptorRanges[13].BaseShaderRegister = 105; // t105: normalTexture
	descriptorRanges[13].RegisterSpace = 0;
	descriptorRanges[13].OffsetInDescriptorsFromTableStart = D3D12_DESCRIPTOR_RANGE_OFFSET_APPEND;



	D3D12_ROOT_PARAMETER rootParameters[25];

	rootParameters[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[0].Descriptor.ShaderRegister = 0;	// b0: Player
	rootParameters[0].Descriptor.RegisterSpace = 0;
	rootParameters[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[1].Descriptor.ShaderRegister = 1;	// b1: Camera
	rootParameters[1].Descriptor.RegisterSpace = 0;
	rootParameters[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[2].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[2].DescriptorTable.pDescriptorRanges = &descriptorRanges[0];	// b2: GameObjectInfo
	rootParameters[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[3].Constants.Num32BitValues = 3;
	rootParameters[3].Constants.ShaderRegister = 3;		//  b3: MaterialInfo
	rootParameters[3].Constants.RegisterSpace = 0;
	rootParameters[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[4].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[4].Constants.Num32BitValues = 2;
	rootParameters[4].Constants.ShaderRegister = 4;		//  b4: TimeInfo
	rootParameters[4].Constants.RegisterSpace = 0;
	rootParameters[4].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[5].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[5].Descriptor.ShaderRegister = 5;	// b5: Materials
	rootParameters[5].Descriptor.RegisterSpace = 0;
	rootParameters[5].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[6].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[6].Descriptor.ShaderRegister = 6;	// b6: Lights
	rootParameters[6].Descriptor.RegisterSpace = 0;
	rootParameters[6].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[7].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[7].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[7].DescriptorTable.pDescriptorRanges = &descriptorRanges[1]; // t0: gtxtTerrainBaseTexture, t1: gtxtTerrainDetailTexture
	rootParameters[7].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[8].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[8].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[8].DescriptorTable.pDescriptorRanges = &descriptorRanges[2]; // t5: gtxtDiffuseTexture
	rootParameters[8].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[9].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[9].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[9].DescriptorTable.pDescriptorRanges = &descriptorRanges[3]; // t15: gtxtNormalTexture
	rootParameters[9].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[10].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[10].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[10].DescriptorTable.pDescriptorRanges = &descriptorRanges[4]; // t25: gtxtSpecularTexture
	rootParameters[10].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[11].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[11].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[11].DescriptorTable.pDescriptorRanges = &descriptorRanges[5]; // t35: gtxtMetallicTexture
	rootParameters[11].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[12].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[12].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[12].DescriptorTable.pDescriptorRanges = &descriptorRanges[6]; // t45: gtxtEmissionTexture
	rootParameters[12].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[13].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[13].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[13].DescriptorTable.pDescriptorRanges = &descriptorRanges[7]; // t55: gtxtBillboardTexture
	rootParameters[13].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[14].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[14].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[14].DescriptorTable.pDescriptorRanges = &descriptorRanges[8]; // t65: gtxtParticleTexture
	rootParameters[14].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[15].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[15].Constants.Num32BitValues = 5;
	rootParameters[15].Constants.ShaderRegister = 7;		//  b7: terrainInfo 
	rootParameters[15].Constants.RegisterSpace = 0;
	rootParameters[15].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[16].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[16].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[16].DescriptorTable.pDescriptorRanges = &descriptorRanges[9];
	rootParameters[16].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[17].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[17].Descriptor.ShaderRegister = 76;	// t76: particleData
	rootParameters[17].Descriptor.RegisterSpace = 0;
	rootParameters[17].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[18].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[18].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[18].DescriptorTable.pDescriptorRanges = &descriptorRanges[10]; // t77 skyBox texture
	rootParameters[18].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[19].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[19].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[19].DescriptorTable.pDescriptorRanges = &descriptorRanges[11]; // 80 terrain normal texture
	rootParameters[19].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[20].ParameterType = D3D12_ROOT_PARAMETER_TYPE_CBV;
	rootParameters[20].Descriptor.ShaderRegister = 8;	// b6: skinned Bone Offsets
	rootParameters[20].Descriptor.RegisterSpace = 0;
	rootParameters[20].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[21].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[21].Descriptor.ShaderRegister = 92;	// t92: skinned Bone Transforms
	rootParameters[21].Descriptor.RegisterSpace = 0;
	rootParameters[21].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[22].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[22].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[22].DescriptorTable.pDescriptorRanges = &descriptorRanges[12]; //  t93 terrain detail texture
	rootParameters[22].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[23].ParameterType = D3D12_ROOT_PARAMETER_TYPE_DESCRIPTOR_TABLE;
	rootParameters[23].DescriptorTable.NumDescriptorRanges = 1;
	rootParameters[23].DescriptorTable.pDescriptorRanges = &descriptorRanges[13]; //  t105 terrain detail texture
	rootParameters[23].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	rootParameters[24].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[24].Constants.Num32BitValues = 2;
	rootParameters[24].Constants.ShaderRegister = 9;		//  b9: gfParticleDivAge
	rootParameters[24].Constants.RegisterSpace = 0;
	rootParameters[24].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	D3D12_STATIC_SAMPLER_DESC samplerDesc[4];
	::ZeroMemory(&samplerDesc, sizeof(D3D12_STATIC_SAMPLER_DESC));
	samplerDesc[0].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc[0].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[0].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[0].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[0].MipLODBias = 0;
	samplerDesc[0].MaxAnisotropy = 1;
	samplerDesc[0].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	samplerDesc[0].MinLOD = 0;
	samplerDesc[0].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[0].ShaderRegister = 0;
	samplerDesc[0].RegisterSpace = 0;
	samplerDesc[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	samplerDesc[1].Filter = D3D12_FILTER_MIN_MAG_MIP_LINEAR;
	samplerDesc[1].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[1].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[1].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[1].MipLODBias = 0;
	samplerDesc[1].MaxAnisotropy = 1;
	samplerDesc[1].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	samplerDesc[1].MinLOD = 0;
	samplerDesc[1].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[1].ShaderRegister = 1;
	samplerDesc[1].RegisterSpace = 0;
	samplerDesc[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	samplerDesc[2].Filter = D3D12_FILTER_MIN_MAG_MIP_POINT;
	samplerDesc[2].AddressU = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[2].AddressV = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[2].AddressW = D3D12_TEXTURE_ADDRESS_MODE_CLAMP;
	samplerDesc[2].MipLODBias = 0;
	samplerDesc[2].MaxAnisotropy = 1;
	samplerDesc[2].ComparisonFunc = D3D12_COMPARISON_FUNC_ALWAYS;
	samplerDesc[2].MinLOD = 0;
	samplerDesc[2].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[2].ShaderRegister = 2;
	samplerDesc[2].RegisterSpace = 0;
	samplerDesc[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	samplerDesc[3].Filter = D3D12_FILTER_ANISOTROPIC;
	samplerDesc[3].AddressU = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[3].AddressV = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[3].AddressW = D3D12_TEXTURE_ADDRESS_MODE_WRAP;
	samplerDesc[3].MipLODBias = 0;
	samplerDesc[3].MaxAnisotropy = 1;
	samplerDesc[3].ComparisonFunc = D3D12_COMPARISON_FUNC_GREATER_EQUAL;
	samplerDesc[3].MinLOD = 0;
	samplerDesc[3].MaxLOD = D3D12_FLOAT32_MAX;
	samplerDesc[3].ShaderRegister = 3;
	samplerDesc[3].RegisterSpace = 0;
	samplerDesc[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_PIXEL;

	D3D12_ROOT_SIGNATURE_FLAGS rootSignatureFlags =
		D3D12_ROOT_SIGNATURE_FLAG_ALLOW_INPUT_ASSEMBLER_INPUT_LAYOUT |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_HULL_SHADER_ROOT_ACCESS |
		D3D12_ROOT_SIGNATURE_FLAG_DENY_DOMAIN_SHADER_ROOT_ACCESS;

	D3D12_ROOT_SIGNATURE_DESC rootSignatureDesc;
	::ZeroMemory(&rootSignatureDesc, sizeof(D3D12_ROOT_SIGNATURE_DESC));
	rootSignatureDesc.NumParameters = _countof(rootParameters);
	rootSignatureDesc.pParameters = rootParameters;
	rootSignatureDesc.NumStaticSamplers = 4;
	rootSignatureDesc.pStaticSamplers = samplerDesc;
	rootSignatureDesc.Flags = rootSignatureFlags;

	ID3DBlob *signatureBlob = NULL;
	ID3DBlob *errorBlob = NULL;
	D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signatureBlob, &errorBlob);
	device->CreateRootSignature(0, signatureBlob->GetBufferPointer(),
		signatureBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void**)&graphicsRootSignature);
	if (signatureBlob) signatureBlob->Release();
	if (errorBlob) errorBlob->Release();

	return(graphicsRootSignature);
}

ID3D12RootSignature * Scene::CreateComputeRootSignature(ID3D12Device * device)
{
	ID3D12RootSignature* computeRootSignature = NULL;

	D3D12_ROOT_PARAMETER rootParameters[5];

	rootParameters[0].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[0].Constants.Num32BitValues = 2;
	rootParameters[0].Constants.ShaderRegister = 0;		//  b0: TimeInfo
	rootParameters[0].Constants.RegisterSpace = 0;
	rootParameters[0].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[1].ParameterType = D3D12_ROOT_PARAMETER_TYPE_32BIT_CONSTANTS;
	rootParameters[1].Constants.Num32BitValues = 7;
	rootParameters[1].Constants.ShaderRegister = 1;		//  b1: ParticleInfo
	rootParameters[1].Constants.RegisterSpace = 0;
	rootParameters[1].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[2].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[2].Descriptor.ShaderRegister = 0;	// t0: particleData
	rootParameters[2].Descriptor.RegisterSpace = 0;
	rootParameters[2].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[3].ParameterType = D3D12_ROOT_PARAMETER_TYPE_SRV;
	rootParameters[3].Descriptor.ShaderRegister = 1;	// t1: copyData
	rootParameters[3].Descriptor.RegisterSpace = 0;
	rootParameters[3].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	rootParameters[4].ParameterType = D3D12_ROOT_PARAMETER_TYPE_UAV;
	rootParameters[4].Descriptor.ShaderRegister = 0;	// u0: particleData
	rootParameters[4].Descriptor.RegisterSpace = 0;
	rootParameters[4].ShaderVisibility = D3D12_SHADER_VISIBILITY_ALL;

	D3D12_ROOT_SIGNATURE_DESC rootSignatureDesc;
	::ZeroMemory(&rootSignatureDesc, sizeof(D3D12_ROOT_SIGNATURE_DESC));
	rootSignatureDesc.NumParameters = _countof(rootParameters);
	rootSignatureDesc.pParameters = rootParameters;
	rootSignatureDesc.NumStaticSamplers = 0;
	rootSignatureDesc.pStaticSamplers = nullptr;
	rootSignatureDesc.Flags = D3D12_ROOT_SIGNATURE_FLAG_NONE;

	ID3DBlob *signatureBlob = NULL;
	ID3DBlob *errorBlob = NULL;
	D3D12SerializeRootSignature(&rootSignatureDesc, D3D_ROOT_SIGNATURE_VERSION_1, &signatureBlob, &errorBlob);
	device->CreateRootSignature(0, signatureBlob->GetBufferPointer(),
		signatureBlob->GetBufferSize(), __uuidof(ID3D12RootSignature), (void**)&computeRootSignature);
	if (signatureBlob) signatureBlob->Release();
	if (errorBlob) errorBlob->Release();

	return(computeRootSignature);
}

void Scene::GarbageCollection()
{
	for (int i = 0; i < shadersNum; ++i)
	{
		shaders[i]->GarbageCollection();
	}
}

void Scene::CreateCbvSrvUavDescriptorHeaps(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, int nConstantBufferViews, int nShaderResourceViews, int nUnorderedAccessViews)
{
	D3D12_DESCRIPTOR_HEAP_DESC d3dDescriptorHeapDesc;
	d3dDescriptorHeapDesc.NumDescriptors = nConstantBufferViews + nShaderResourceViews + nUnorderedAccessViews; //CBVs + SRVs 
	d3dDescriptorHeapDesc.Type = D3D12_DESCRIPTOR_HEAP_TYPE_CBV_SRV_UAV;
	d3dDescriptorHeapDesc.Flags = D3D12_DESCRIPTOR_HEAP_FLAG_SHADER_VISIBLE;
	d3dDescriptorHeapDesc.NodeMask = 0;
	device->CreateDescriptorHeap(&d3dDescriptorHeapDesc, __uuidof(ID3D12DescriptorHeap), (void **)&cbvSrvUavDescriptorHeap);

	cbvCPUDescriptorNextHandle = cbvCPUDescriptorStartHandle = cbvSrvUavDescriptorHeap->GetCPUDescriptorHandleForHeapStart();
	cbvGPUDescriptorNextHandle = cbvGPUDescriptorStartHandle = cbvSrvUavDescriptorHeap->GetGPUDescriptorHandleForHeapStart();
	srvCPUDescriptorNextHandle.ptr = srvCPUDescriptorStartHandle.ptr = cbvCPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nConstantBufferViews);
	srvGPUDescriptorNextHandle.ptr = srvGPUDescriptorStartHandle.ptr = cbvGPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nConstantBufferViews);
	uavCPUDescriptorNextHandle.ptr = uavCPUDescriptorStartHandle.ptr = srvCPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nShaderResourceViews);
	uavGPUDescriptorNextHandle.ptr = uavGPUDescriptorStartHandle.ptr = srvGPUDescriptorStartHandle.ptr + (::gnCbvSrvDescriptorIncrementSize * nShaderResourceViews);

}
// Scene에서 View들의 생성을 관리한다. Shader에서 View의 사용은 리턴값인 할당한 핸들의 시작주소로 접근할 수 있다.
D3D12_GPU_DESCRIPTOR_HANDLE Scene::CreateConstantBufferViews(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, int nConstantBufferViews, ID3D12Resource *pd3dConstantBuffers, UINT nStride)
{
	D3D12_GPU_DESCRIPTOR_HANDLE d3dCbvGPUDescriptorHandle = cbvGPUDescriptorNextHandle;
	D3D12_GPU_VIRTUAL_ADDRESS d3dGpuVirtualAddress = pd3dConstantBuffers->GetGPUVirtualAddress();
	D3D12_CONSTANT_BUFFER_VIEW_DESC d3dCBVDesc;
	d3dCBVDesc.SizeInBytes = nStride;
	for (int j = 0; j < nConstantBufferViews; j++)
	{
		d3dCBVDesc.BufferLocation = d3dGpuVirtualAddress + (nStride * j);
		device->CreateConstantBufferView(&d3dCBVDesc, cbvCPUDescriptorNextHandle);
		cbvCPUDescriptorNextHandle.ptr = cbvCPUDescriptorNextHandle.ptr + ::gnCbvSrvDescriptorIncrementSize;
		cbvGPUDescriptorNextHandle.ptr = cbvGPUDescriptorNextHandle.ptr + ::gnCbvSrvDescriptorIncrementSize;
	}
	return(d3dCbvGPUDescriptorHandle);
}

D3D12_SHADER_RESOURCE_VIEW_DESC GetShaderResourceViewDesc(D3D12_RESOURCE_DESC d3dResourceDesc, UINT nTextureType)
{
	D3D12_SHADER_RESOURCE_VIEW_DESC d3dShaderResourceViewDesc;
	d3dShaderResourceViewDesc.Format = d3dResourceDesc.Format;
	d3dShaderResourceViewDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	switch (nTextureType)
	{
	case ResourceTexture2D: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D)(d3dResourceDesc.DepthOrArraySize == 1)
	case ResourceTexture2D_Array:
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
		d3dShaderResourceViewDesc.Texture2D.MipLevels = -1;
		d3dShaderResourceViewDesc.Texture2D.MostDetailedMip = 0;
		d3dShaderResourceViewDesc.Texture2D.PlaneSlice = 0;
		d3dShaderResourceViewDesc.Texture2D.ResourceMinLODClamp = 0.0f;
		break;
	case ResourceTexture2DArray: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D)(d3dResourceDesc.DepthOrArraySize != 1)
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2DARRAY;
		d3dShaderResourceViewDesc.Texture2DArray.MipLevels = -1;
		d3dShaderResourceViewDesc.Texture2DArray.MostDetailedMip = 0;
		d3dShaderResourceViewDesc.Texture2DArray.PlaneSlice = 0;
		d3dShaderResourceViewDesc.Texture2DArray.ResourceMinLODClamp = 0.0f;
		d3dShaderResourceViewDesc.Texture2DArray.FirstArraySlice = 0;
		d3dShaderResourceViewDesc.Texture2DArray.ArraySize = d3dResourceDesc.DepthOrArraySize;
		break;
	case ResourceTextureCube: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_TEXTURE2D)(d3dResourceDesc.DepthOrArraySize == 6)
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURECUBE;
		d3dShaderResourceViewDesc.TextureCube.MipLevels = -1;
		d3dShaderResourceViewDesc.TextureCube.MostDetailedMip = 0;
		d3dShaderResourceViewDesc.TextureCube.ResourceMinLODClamp = 0.0f;
		break;
	case ResourceBuffer: //(d3dResourceDesc.Dimension == D3D12_RESOURCE_DIMENSION_BUFFER)
		d3dShaderResourceViewDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
		d3dShaderResourceViewDesc.Buffer.FirstElement = 0;
		d3dShaderResourceViewDesc.Buffer.NumElements = 0;
		d3dShaderResourceViewDesc.Buffer.StructureByteStride = 0;
		d3dShaderResourceViewDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;
		break;
	}
	return(d3dShaderResourceViewDesc);

}

void Scene::CreateTextureResource(ID3D12Device *device, ID3D12GraphicsCommandList *commandList)
{
	rootArgumentInfos.reserve(200);
	rootArgumentIdx.reserve(8);
	int i = 0;

	Texture *terrainTexture = new Texture(4, ResourceTexture2D, 0);
	terrainTexture->SetTexture(terrainHeightResource, 0);
	terrainTexture->SetTexture(terrainAlphaResource0, 1); // alpha texture
	terrainTexture->SetTexture(terrainAlphaResource1, 2);
	terrainTexture->SetTexture(terrainAlphaResource2, 3);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainTexture, GraphicsRootTerrainTexture));


	Texture *terrianNormal = new Texture(12, ResourceTexture2D, 0);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Grass_n.dds", 0);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Rock_n.dds", 1);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Mud_n.dds", 2);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/light-Rock_n.dds", 3);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/snow_n.dds", 4);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/ice_n.dds", 5);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Concrete_n.dds", 6);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Asphalt_n.dds", 7);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Beach_n.dds", 8);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/soil_n.dds", 9);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/FloorStreet_n.dds", 10);
	terrianNormal->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/SnowRoad_n.dds", 11);
	textures.emplace_back(terrianNormal);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrianNormal, GraphicsRootTerrainNormal));

	Texture *terrainDetailTexture = new Texture(12, ResourceTexture2D, 0);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Grass.dds", 0);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Rock.dds", 1);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Mud.dds", 2);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/light Rock.dds", 3);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/snow.dds", 4);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/ice.dds", 5);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Concrete.dds", 6);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Asphalt.dds", 7);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/Beach.dds", 8);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/soil.dds", 9);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/FloorStreet.dds", 10);
	terrainDetailTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Terrain/SnowRoad.dds", 11);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, terrainDetailTexture, GraphicsRootTerrainDetail));

	// TerrainTexture

	Texture *billboardTexture = new Texture(1, ResourceTexture2D, 0);
	billboardTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Billboard/Bulb.dds", 0);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, billboardTexture, GraphicsRootBillboardTextures));
	textures.emplace_back(billboardTexture);
	// BillboardTexture

	Texture *decalTexture = new Texture(2, ResourceTexture2D, 0);
	decalTexture->SetTexture(terrainDepthResource, 0);
	decalTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Decal/RedCircle.dds", 1);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, decalTexture, 16));
	textures.emplace_back(decalTexture);
	//decalTexture

	Texture *particleTexture = new Texture(4, ResourceTexture2D, 0);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/SmokeParticle1.dds", 0);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/LightParticle.dds", 1);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/FireParticle.dds", 2);
	particleTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/Particle/pngwave.dds", 3);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, particleTexture, GraphicsRootParticleTextures));
	textures.emplace_back(particleTexture);
	// ParticleTexture

	Texture *skyBoxTexture = new Texture(3, ResourceTextureCube, 0);
	skyBoxTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/SkyBox/cubeMap.dds", 0);
	skyBoxTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/SkyBox/cubeMap.dds", 1);
	skyBoxTexture->LoadTextureFromFile(device, commandList, L"Assets/Image/SkyBox/cubeMap.dds", 2);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, skyBoxTexture, GraphicsRootSkyBoxInfo));
	textures.emplace_back(skyBoxTexture);
	// SkyBoxTexture


	D3D12_SHADER_RESOURCE_VIEW_DESC depthSrvDesc = {};
	depthSrvDesc.Format = DXGI_FORMAT_R32_FLOAT_X8X24_TYPELESS;
	depthSrvDesc.ViewDimension = D3D12_SRV_DIMENSION_TEXTURE2D;
	depthSrvDesc.Texture2D.MipLevels = 1;
	depthSrvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;

	Texture* depthTexture = new Texture(1, ResourceTexture2D, 0);
	depthTexture->SetTexture(depthResource, 0);
	rootArgumentIdx.emplace_back(CreateShaderResourceViews(device, commandList, depthTexture, GraphicsRootDepthTexture, &depthSrvDesc));
	textures.emplace_back(depthTexture);
	// DepthStencilBuffer
}

void Scene::SetRootArgument(int idx, UINT rootParameterIndex, D3D12_GPU_DESCRIPTOR_HANDLE srvGpuDescriptorHandle)
{
	rootArgumentInfos[idx].rootParameterIndex = rootParameterIndex;
	rootArgumentInfos[idx].srvGpuDescriptorHandle = srvGpuDescriptorHandle;
}

SRVROOTARGUMENTINFO Scene::GetRootArgument(int idx)
{
	return rootArgumentInfos[idx];
}

int Scene::CreateShaderResourceViews(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, Texture *texture, UINT rootParameterStartIndex, D3D12_SHADER_RESOURCE_VIEW_DESC* desc)
{
	int startIdx = rootArgumentCurrentIdx;
	if (texture)
	{
		int nTextures = texture->GetTextures();
		int nTextureType = texture->GetTextureType();
		for (int i = 0; i < nTextures; i++)
		{
			ID3D12Resource *pShaderResource = texture->GetTexture(i);
			D3D12_RESOURCE_DESC	d3dResourceDesc = pShaderResource->GetDesc();
			D3D12_SHADER_RESOURCE_VIEW_DESC d3dShaderResourceViewDesc;
			if (desc)
				d3dShaderResourceViewDesc = *desc;
			else
				d3dShaderResourceViewDesc = GetShaderResourceViewDesc(d3dResourceDesc, nTextureType);
			device->CreateShaderResourceView(pShaderResource, &d3dShaderResourceViewDesc, srvCPUDescriptorNextHandle);
			srvCPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
			SRVROOTARGUMENTINFO temp(rootParameterStartIndex, srvGPUDescriptorNextHandle);
			rootArgumentInfos.emplace_back(temp);
			srvGPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
			rootArgumentCurrentIdx++;
		}
	}

	return startIdx;
}

int Scene::CreateStructuredShaderResourceViews(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, UINT numElements, UINT stride, ID3D12Resource* buffer, UINT rootParameterStartIndex)
{
	int startIdx = rootArgumentCurrentIdx;

	D3D12_SHADER_RESOURCE_VIEW_DESC srvDesc = {};
	srvDesc.Shader4ComponentMapping = D3D12_DEFAULT_SHADER_4_COMPONENT_MAPPING;
	srvDesc.Format = DXGI_FORMAT_UNKNOWN;
	srvDesc.ViewDimension = D3D12_SRV_DIMENSION_BUFFER;
	srvDesc.Buffer.FirstElement = 0;
	srvDesc.Buffer.NumElements = numElements;
	srvDesc.Buffer.StructureByteStride = stride;
	srvDesc.Buffer.Flags = D3D12_BUFFER_SRV_FLAG_NONE;

	device->CreateShaderResourceView(buffer, &srvDesc, srvCPUDescriptorNextHandle);
	srvCPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
	SetRootArgument(rootArgumentCurrentIdx++, rootParameterStartIndex, srvGPUDescriptorNextHandle);
	srvGPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;

	return startIdx;
}

int Scene::CreateStructuredUnorderedAccessViews(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, UINT numElements, UINT stride, ID3D12Resource* buffer, UINT rootParameterStartIndex)
{
	int startIdx = rootArgumentCurrentIdx;

	D3D12_UNORDERED_ACCESS_VIEW_DESC uavDesc = {};
	uavDesc.Format = DXGI_FORMAT_UNKNOWN;
	uavDesc.ViewDimension = D3D12_UAV_DIMENSION_BUFFER;
	uavDesc.Buffer.FirstElement = 0;
	uavDesc.Buffer.NumElements = numElements;
	uavDesc.Buffer.StructureByteStride = stride;
	uavDesc.Buffer.CounterOffsetInBytes = 0;
	uavDesc.Buffer.Flags = D3D12_BUFFER_UAV_FLAG_NONE;

	device->CreateUnorderedAccessView(buffer, nullptr, &uavDesc, uavCPUDescriptorNextHandle);
	uavCPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;
	SetRootArgument(rootArgumentCurrentIdx++, rootParameterStartIndex, uavGPUDescriptorNextHandle);
	uavGPUDescriptorNextHandle.ptr += ::gnCbvSrvDescriptorIncrementSize;

	return startIdx;
}

void Scene::CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList)
{
	UINT cbElementBytes = ((sizeof(LIGHTS) + 255) & ~255);
	cbLights = ::CreateBufferResource(device, commandList, NULL, cbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	cbLights->Map(0, NULL, (void**)& cbMappedLights);

	UINT cbMaterialBytes = ((sizeof(MATERIALS) + 255) & ~255);
	cbMaterials = ::CreateBufferResource(device, commandList, materials, cbMaterialBytes, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &materialUploadBuffer);


	// 조명은 매 프레임 위치가 바뀔 수 있으므로 UploadHeap에 생성하였고, 재질은 값이 바뀌지 않으므로 DefaultHeap에 생성하였다.
}

void Scene::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList)
{
	::memcpy(cbMappedLights, lights, sizeof(LIGHTS));

	D3D12_GPU_VIRTUAL_ADDRESS d3dcbLightsGpuVirtualAddress = cbLights->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootLights, d3dcbLightsGpuVirtualAddress);
	D3D12_GPU_VIRTUAL_ADDRESS d3dcbMaterialsGpuVirtualAddress = cbMaterials->GetGPUVirtualAddress();
	commandList->SetGraphicsRootConstantBufferView(GraphicsRootMaterials, d3dcbMaterialsGpuVirtualAddress);

	for (int i = 0; i < rootArgumentIdx.size(); ++i)
	{
		commandList->SetGraphicsRootDescriptorTable(rootArgumentInfos[rootArgumentIdx[i]].rootParameterIndex, rootArgumentInfos[rootArgumentIdx[i]].srvGpuDescriptorHandle);
	}
	// 텍스쳐들 Set
}

void Scene::ReleaseShaderVariables()
{
	if (cbLights)
	{
		cbLights->Unmap(0, NULL);
		cbLights->Release();
	}
	if (cbMaterials)
	{
		cbMaterials->Release();
	}
	for (int i = 0; i < shadersNum; ++i)
	{
		shaders[i]->ReleaseShaderVariables();
	}
}

bool Scene::OnProcessingMouseMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{
	return(false);
}
bool Scene::OnProcessingKeyboardMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam)
{
	return(false);
}

bool Scene::ProcessInput(UCHAR* keysBuffer)
{
	return(false);
}

void Scene::AnimateObjects(float timeElapsed, Camera* camera)
{
	if (shaders)
	{
		for (int i = 0; i < shadersNum; i++)
		{
			if (shaders[i])
				shaders[i]->AnimateObjects(timeElapsed, camera);
		}
	}
	// 조명의 위치와 방향을 플레이어의 위치와 방향으로 변경한다.
	if (lights)
	{
		/*	lights->lights[1].position = player->GetPosition();
			lights->lights[1].direction = player->GetLookVector();*/
	}
}

void Scene::PrepareRender(ID3D12GraphicsCommandList* commandList)
{
	if (graphicsRootSignature) commandList->SetGraphicsRootSignature(graphicsRootSignature);
	if (computeRootSignature) commandList->SetComputeRootSignature(computeRootSignature);
	if (cbvSrvUavDescriptorHeap) commandList->SetDescriptorHeaps(1, &cbvSrvUavDescriptorHeap);

}

void Scene::Render(ID3D12GraphicsCommandList* commandList, Camera* camera, int decalShape)
{
	camera->SetViewportsAndScissorRects(commandList);
	camera->UpdateShaderVariables(commandList);

	UpdateShaderVariables(commandList);



	if (shaders)
	{
		for (int i = SCREENSPACEDECALSHADER; i < shadersNum; i++)
		{
			if (shaders[i])
				if ( i == SKYMAPSHADER || i == BOUNDINGSHADER)
					continue;
			commandList->SetGraphicsRoot32BitConstants(15, 3, &terrain->GetScale(), 0);
			commandList->SetGraphicsRoot32BitConstants(15, 1, &decalShape, 3);
			shaders[i]->Render(commandList, camera);
		}
		
	}
}

void Scene::BoundingRender(ID3D12GraphicsCommandList * commandList, Camera * camera , int bIdx)
{
	camera->SetViewportsAndScissorRects(commandList);
	camera->UpdateShaderVariables(commandList);

	UpdateShaderVariables(commandList);

	if (shaders)
	{
		int decalShape = -1;
		int tbIdx = bIdx;

		XMFLOAT3 tempScale{ 1.0f  , 1.0f  , 1.0f };
		commandList->SetGraphicsRoot32BitConstants(15, 3, &tempScale, 0); // 
		commandList->SetGraphicsRoot32BitConstants(15, 1, &decalShape, 3); // 이걸로 더미빼고 다른오브젝트들의 출력 막음 
		commandList->SetGraphicsRoot32BitConstants(15, 1, &tbIdx, 4);

		shaders[INSTANCSHADER]->Render(commandList, camera);
		commandList->SetGraphicsRoot32BitConstants(15, 3, &terrain->GetScale(), 0); // 
		commandList->SetGraphicsRoot32BitConstants(15, 1, &decalShape, 3); 
		commandList->SetGraphicsRoot32BitConstants(15, 1, &tbIdx, 4);

		shaders[SKINEDINSTANCSHADER]->Render(commandList, camera);
	}

}

void Scene::TerrainRender(ID3D12GraphicsCommandList * commandList, Camera * camera, int pipeCounter, int LayerIndex)
{
	if (graphicsRootSignature) commandList->SetGraphicsRootSignature(graphicsRootSignature);
	if (cbvSrvUavDescriptorHeap) commandList->SetDescriptorHeaps(1, &cbvSrvUavDescriptorHeap);

	camera->SetViewportsAndScissorRects(commandList);
	camera->UpdateShaderVariables(commandList);


	terrain->SetLayer(LayerIndex);

	UpdateShaderVariables(commandList);

	commandList->SetGraphicsRoot32BitConstants(15, 3, &terrain->GetScale(), 0);
	commandList->SetGraphicsRoot32BitConstants(15, 1, &LayerIndex, 3);
	shaders[TERRAINSHADER]->SetPipeLineCounter(pipeCounter);
	shaders[TERRAINSHADER]->SetTerrainLayer(LayerIndex);

	if (shaders[TERRAINSHADER]) {
		shaders[TERRAINSHADER]->Render(commandList, camera);
	}
}

void Scene::TerrainNaviRender(ID3D12GraphicsCommandList * commandList, Camera * camera)
{
	static_cast<TerrainShader*>(shaders[TERRAINSHADER])->NaviRender(commandList , camera);

}

void Scene::SkyBoxRender(ID3D12GraphicsCommandList * commandList, Camera * camera)
{
	camera->SetViewportsAndScissorRects(commandList);
	camera->UpdateShaderVariables(commandList);

	UpdateShaderVariables(commandList);

	if (shaders) shaders[SKYMAPSHADER]->Render(commandList, camera);
}

void Scene::CreateTerrainAlphaTexture(ID3D12Device * device, ID3D12GraphicsCommandList * commandList)
{
}
