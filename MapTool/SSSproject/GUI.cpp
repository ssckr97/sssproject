#include "GUI.h"


GUI::GUI()
{
	
}

ImGuiColorEditFlags GUI::RadioButtonState()
{
	return Radioflags;
}



////////////////////////////////////////////////////////////////OptionGUI
OptionGUI::OptionGUI(Scene* scene, ID3D12Device* device, ID3D12GraphicsCommandList* commandList , ID3D12CommandQueue* commandQueue, 
	ID3D12Resource* alphaResource0, ID3D12Resource* alphaResource1, ID3D12Resource* alphaResource2, ID3D12Resource* heightmapResource,ID3D12Resource* normalResource ,LightGUI* lightGUI , ParticleGUI* particleGUI) {
	
	mapName.reserve(10);
	this->scene = scene;
	this->device = device;
	this->commandList = commandList;
	this->lightGUI = lightGUI;
	this->commandQueue = commandQueue;
	this->alphaResource0 = alphaResource0;
	this->alphaResource1 = alphaResource1;
	this->alphaResource2 = alphaResource2;
	this->heightmapResource = heightmapResource;
	this->normalResource = normalResource;
	this->particleGUI = particleGUI;

	std::string path = "Assets\\Map\\*.*";
	struct _finddata_t fd;
	intptr_t handle;
	if ((handle = _findfirst(path.c_str(), &fd)) == -1L)
		std::cout << "No file in directory!" << std::endl;
	do
	{
		std::cout << fd.name << std::endl;
		std::string str(fd.name);
		if (str.find(".txt") != std::string::npos)
			mapName.emplace_back(str);

	} while (_findnext(handle, &fd) == 0);

	_findclose(handle);
}

void OptionGUI::Render()
{
	ImGui::ShowDemoWindow();

	//1. 
	{
		static float f = 0.0f;
		static int counter = 0;

		ImGui::Begin("Maptool Option");                          // Create a window called "Hello, world!" and append into it.
		ImGui::ColorEdit3("clear color", (float*)&clear_color); // Edit 3 floats representing a color
		ImGui::Text("Application average %.3f ms/frame (%.1f FPS)", 1000.0f / ImGui::GetIO().Framerate, ImGui::GetIO().Framerate);
		ImGui::Text("Control Camera Speed");
		float speed = *cameraSpeed;

		ImGui::SliderFloat("Speed", &speed, 0.0f, 150.0f);
		*cameraSpeed = speed;
		ImGui::Text("--------------------------------------------");
		ImGui::Text("		       Input Map Name                ");
		ImGui::Text("--------------------------------------------");
		static char saveBuf[64] = ""; ImGui::InputText("default", saveBuf, 64);
		if (ImGui::Button("Save Map")) {
			SaveFile saveFile;
			std::string filePath = "Assets/Map/";
			std::string fileName;
			std::string alphaTname[3];
			std::string hightTname;
			std::string normalTname;
			std::wstring wc[3];
			std::wstring hc;
			std::wstring nc;

			for (int i = 0; i < 3; ++i) {
				alphaTname[i] = filePath + saveBuf + "_alphaTexture" + std::to_string(i) + ".dds";
				wc[i].resize(alphaTname[i].size(), L'#');
				mbstowcs(&wc[i][0], alphaTname[i].c_str(), alphaTname[i].size());
			}
			hightTname = filePath + saveBuf + "_hightTexture"  + ".PNG";

			hc.resize(hightTname.size());
			mbstowcs(&hc[0], hightTname.c_str(), hightTname.size());

			normalTname = filePath + saveBuf + "_normalTexture" + ".PNG";
			nc.resize(normalTname.size());
			mbstowcs(&nc[0], normalTname.c_str(), normalTname.size());

			std::string clfileName = filePath + saveBuf + ".txt";
			fileName = saveBuf;
		
		
			fileName = fileName + ".txt";

			mapName.emplace_back(fileName);

			std::ofstream out(clfileName.c_str(), std::ios::out | std::ios::app | std::ios::binary);
			if (!out.is_open()) {
				std::cout << "파일 생성 실패" << std::endl;
			}

			Shader** shader = scene->GetShaders();
			//객체들의 위치정보를 저장한다. 
			std::vector<InstanceBuffer*> buffer = shader[INSTANCSHADER]->GetBuffer();
			saveFile.GetSaveInstanceBuffer().resize(buffer.size() - 1);
			saveFile.SetInstanceBuffer(buffer);
			//버퍼 사이즈를 저장 
			int buffersize = saveFile.GetInstanceBufferSize();
			out.write((const char*)&buffersize, 4);
			for (int i = 0; i < saveFile.GetInstanceBufferSize(); ++i) {
				SaveInstanceBuffer tempInstance = saveFile.GetSaveInstanceBuffer()[i];
				// 오브젝트 개수를 저장 
				out.write((const char*)&tempInstance.objectNumber, sizeof(int));
				out.write((char*)&tempInstance.objectWorld[0], tempInstance.objectWorld.size() *  sizeof(XMFLOAT4X4));
			}
			//오브젝트 정보 저장
			saveFile.GetSaveInstanceBuffer().clear();
			std::vector<SkinnedInstanceBuffer*> sBuffer = shader[SKINEDINSTANCSHADER]->GetSkinedBuffer();
			saveFile.GetSaveInstanceBuffer().resize(sBuffer.size());
			saveFile.SetSkinedInstanceBuffer(sBuffer);
		

			buffersize = saveFile.GetInstanceBufferSize();
			out.write((const char*)&buffersize, 4);
			for (int i = 0; i < saveFile.GetInstanceBufferSize(); ++i) {
				SaveInstanceBuffer tempInstance = saveFile.GetSaveInstanceBuffer()[i];
				out.write((const char*)&tempInstance.objectNumber, sizeof(int));
				out.write((char*)&tempInstance.objectWorld[0], tempInstance.objectWorld.size() * sizeof(XMFLOAT4X4));
			}

			//캐릭터 정보 저장


			LIGHTS* lights = scene->GetLights();
			out.write((const char*)&lights->lights[0], sizeof(LIGHT) * MAX_LIGHTS);
			//조명 정보 저장 
			
			ParticleShader* pShader = static_cast<ParticleShader*>(shader[PARTICLESHADER]);
			std::vector<ParticleInfoInCPU> particles = pShader->GetParticles();
			std::vector<ParticleBlobModify> blob = pShader->blobList;

			int particlesSize = particles.size();
			int blobSize = blob.size();

			

			out.write((const char*)&particlesSize, 4);
			out.write((const char*)&blobSize, 4);

			if (particlesSize != 0) {
				out.write((char*)&particles[0], particles.size() * sizeof(ParticleInfoInCPU));
			}
			if (blobSize != 0) {
				out.write((char*)&blob[0], blob.size() * sizeof(ParticleBlobModify));
			}

			//파티클 정보 저장하기 


			HeightMapTerrain* terrain = scene->GetTerrain();
			HeightMapGridMesh* mesh = (HeightMapGridMesh*)terrain->GetMeshes()[0];
			int vertexnum = mesh->GetVertexNum();
			saveFile.GetSaveDiffuseVertex().resize(vertexnum);

			for (int i = 0; i < terrain->GetMeshNum(); ++i) {
				HeightMapGridMesh* tempMesh = (HeightMapGridMesh*)terrain->GetMesh(i);

				int vertices = tempMesh->GetVertexNum();
				for (int j = 0; j < vertices; ++j) {
					saveFile.GetSaveDiffuseVertex()[j].position = tempMesh->GetDiffuseTexturedVertex()[j].position;
					saveFile.GetSaveDiffuseVertex()[j].diffuse = tempMesh->GetDiffuseTexturedVertex()[j].diffuse;
					saveFile.GetSaveDiffuseVertex()[j].texNumber = tempMesh->GetDiffuseTexturedVertex()[j].texNumber;

				}
				out.write((char*)&saveFile.GetSaveDiffuseVertex()[0], vertices * sizeof(SaveDiffuseVertex));
			}
			//지형정보 저장.

			out.close();

			//텍스처 저장하기(.dds파일로)
			
			D3D12_RESOURCE_BARRIER resourceBarrier[5];
			for (int i = 0; i < 5; ++i) {
				::ZeroMemory(&resourceBarrier[i], sizeof(D3D12_RESOURCE_BARRIER));
				resourceBarrier[i].Type = D3D12_RESOURCE_BARRIER_TYPE_TRANSITION;
				resourceBarrier[i].Flags = D3D12_RESOURCE_BARRIER_FLAG_NONE;
				if(i == 0)
					resourceBarrier[i].Transition.pResource = heightmapResource;
				if (i == 1)
					resourceBarrier[i].Transition.pResource = alphaResource0;
				if(i == 2)
					resourceBarrier[i].Transition.pResource = alphaResource1;
				if(i == 3)
					resourceBarrier[i].Transition.pResource = alphaResource2;
				if (i == 4)
					resourceBarrier[i].Transition.pResource = normalResource;


				resourceBarrier[i].Transition.StateBefore = D3D12_RESOURCE_STATE_RENDER_TARGET;
				resourceBarrier[i].Transition.StateAfter = D3D12_RESOURCE_STATE_GENERIC_READ;
				resourceBarrier[i].Transition.Subresource = D3D12_RESOURCE_BARRIER_ALL_SUBRESOURCES;
			}
			
			
			SaveTextureToFile(commandQueue, alphaResource0, wc[0].c_str());
			SaveTextureToFile(commandQueue, alphaResource1, wc[1].c_str());
			SaveTextureToFile(commandQueue, alphaResource2, wc[2].c_str());
			SaveWICTextureToFile(commandQueue, heightmapResource, GUID_ContainerFormatPng, hc.c_str(), D3D12_RESOURCE_STATE_GENERIC_READ, D3D12_RESOURCE_STATE_RENDER_TARGET);
			//SaveTextureToFile(commandQueue, alphaResource1, nc.c_str());
			SaveWICTextureToFile(commandQueue, normalResource, GUID_ContainerFormatPng, nc.c_str(), D3D12_RESOURCE_STATE_GENERIC_READ, D3D12_RESOURCE_STATE_RENDER_TARGET);

			//지형 높이정보 저장하기 sever에게 줄 파일 
			std::string sfileName = filePath + saveBuf + "_server" + ".txt";
			std::ofstream sOut(sfileName.c_str(), std::ios::out | std::ios::app | std::ios::binary);
		
			saveFile.GetSaveTerrainPos().resize(vertexnum);
			
			for (int i = 0; i < terrain->GetMeshNum(); ++i) {
				HeightMapGridMesh* tempMesh = (HeightMapGridMesh*)terrain->GetMesh(i);
				int vertices = tempMesh->GetVertexNum();
				sOut.write((char*)&vertices, sizeof(int));
				for (int j = 0; j < vertices; ++j) {
					saveFile.GetSaveTerrainPos()[j] = tempMesh->GetDiffuseTexturedVertex()[j].position;
				}
				sOut.write((char*)&saveFile.GetSaveTerrainPos()[0], vertices * sizeof(XMFLOAT3));
			}
			//서버를 위한 지형 정보 저장 
			sOut.write((char*)&terrain->GetWorld(), sizeof(XMFLOAT4X4));
			//지형의 월드 변환행렬 저장.
			int bs = buffer.size();
			sOut.write((char*)&bs, sizeof(int));
			//버퍼사이즈를 저장 
			for (int i = 0; i < buffer.size(); ++i) {
				auto obbvec = buffer[i]->objects[0]->GetobbVector();
				auto bovec = buffer[i]->objects[0]->GetBoundingObject();
				int obs = obbvec.size();
				sOut.write((char*)&obs, sizeof(int));
				//바운딩 박스 사이즈를 저장 
				for (int j = 0; j < obbvec.size(); ++j) {
					XMFLOAT4X4 bWorld = (*bovec)[j]->GetWorld();
					sOut.write((char*)&bWorld, sizeof(XMFLOAT4X4));
					//바운딩 박스의 월드를 저장  
				}
				
				int bos = buffer[i]->objects.size();
				sOut.write((char*)&bos, sizeof(int));
				//오브젝트 개수를 저장 
				for (int j = 0; j < buffer[i]->objects.size(); ++j) {
					XMFLOAT4X4 boWorld = buffer[i]->objects[j]->GetWorld();
					sOut.write((char*)&boWorld, sizeof(XMFLOAT4X4));
					//오브젝트 월드 저장 
				}
			}

			//서버를 위한 고정 오브젝트 객체들의 바운딩 박스 정보 저장.
			//skin 버퍼 3번부터 ZOMBIE START
			int zombieStart = 3;
			int sbufferSize = sBuffer.size();
			sOut.write((char*)&zombieStart, sizeof(int));
			sOut.write((char*)&sbufferSize, sizeof(int));
			
			for (int i = zombieStart; i < sBuffer.size(); ++i) {
				int sObjSize = sBuffer[i]->objects.size();
				sOut.write((char*)&sObjSize, sizeof(int));
				for (int j = 0; j < sObjSize; ++j) {
					XMFLOAT4X4 sWorld = sBuffer[i]->objects[j]->GetWorld();
					sOut.write((char*)&sWorld, sizeof(XMFLOAT4X4));
	
				}
			}

			//스킨 오브젝트 정보 주기 

			sOut.close();
			std::string astarfile = filePath + saveBuf + "_astarNavi" + ".txt";
			std::ofstream aOut(astarfile.c_str(), std::ios::out | std::ios::app | std::ios::binary);

			int (*navibuffer)[257] = terrain->GetNaviBuffer();
			for (int i = 0; i < 257; ++i) {
				aOut.write((char*)&navibuffer[i], sizeof(int) * 257);
			}

			auto ntvec = terrain->Getntvec();
			int ntsize = ntvec.size();

			aOut.write((const char*)&ntsize, sizeof(int));

			for (int i = 0; i < ntvec.size(); ++i) {
				NaviInform naviInform;
				naviInform.idx = ntvec[i].idx;
				naviInform.center = ntvec[i].center;
				naviInform.utriv1 = ntvec[i].utri.v1;
				naviInform.utriv2 = ntvec[i].utri.v2;
				naviInform.utriv3 = ntvec[i].utri.v3;

				naviInform.dtriv1 = ntvec[i].dtri.v1;
				naviInform.dtriv2 = ntvec[i].dtri.v2;
				naviInform.dtriv3 = ntvec[i].dtri.v3;
				naviInform.width = ntvec[i].width;
				naviInform.x = ntvec[i].x;
				naviInform.z = ntvec[i].z;
				
				aOut.write((char*)&naviInform, sizeof(NaviInform));
				
				int lt = ntvec[i].leftTile.size();
				int rt = ntvec[i].rightTile.size();
				int dt = ntvec[i].downTile.size();
				int ut = ntvec[i].upTile.size();

				int lut = ntvec[i].leftupTile.size();
				int ldt = ntvec[i].leftDownTile.size();
				int rut = ntvec[i].rightupTile.size();
				int rdt = ntvec[i].rightDownTile.size();

				aOut.write((const char*)&lt, sizeof(int));
				aOut.write((const char*)&rt, sizeof(int));
				aOut.write((const char*)&dt, sizeof(int));
				aOut.write((const char*)&ut, sizeof(int));

				aOut.write((const char*)&lut, sizeof(int));
				aOut.write((const char*)&ldt, sizeof(int));
				aOut.write((const char*)&rut, sizeof(int));
				aOut.write((const char*)&rdt, sizeof(int));


				if(lt != 0)
				aOut.write((char*)&ntvec[i].leftTile[0], sizeof(int) *  ntvec[i].leftTile.size());
				if(rt != 0)
				aOut.write((char*)&ntvec[i].rightTile[0], sizeof(int) *  ntvec[i].rightTile.size());
				if(dt != 0)
				aOut.write((char*)&ntvec[i].downTile[0], sizeof(int) *  ntvec[i].downTile.size());
				if(ut != 0)
				aOut.write((char*)&ntvec[i].upTile[0], sizeof(int) *  ntvec[i].upTile.size());

				if (lut != 0)
					aOut.write((char*)&ntvec[i].leftupTile[0], sizeof(int) *  ntvec[i].leftupTile.size());
				if (ldt != 0)
					aOut.write((char*)&ntvec[i].leftDownTile[0], sizeof(int) *  ntvec[i].leftDownTile.size());
				if (rut != 0)
					aOut.write((char*)&ntvec[i].rightupTile[0], sizeof(int) *  ntvec[i].rightupTile.size());
				if (rdt != 0)
					aOut.write((char*)&ntvec[i].rightDownTile[0], sizeof(int) *  ntvec[i].rightDownTile.size());

			}

			
		



			aOut.close();

			//astar를 위한 정보 저장


			memset(saveBuf, 0, sizeof(char) * 64);


		}

		if (ImGui::BeginCombo("##combo", currentMap)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < mapName.size(); n++)
			{
				bool is_selected = (currentMap == mapName[n].c_str()); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(mapName[n].c_str(), is_selected)) {
					currentMap = mapName[n].c_str();
					mapNumber = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}

		if (ImGui::Button("Load Map")) {
			SaveFile LoadFile;
			std::string filePath = "Assets/Map/";
			Shader** shader = scene->GetShaders();
			std::vector<InstanceBuffer*> buffer = shader[INSTANCSHADER]->GetBuffer();

			filePath = filePath + mapName[mapNumber];
			//객체 읽어오기 
			std::ifstream in(filePath, std::ios_base::in | std::ios::binary);
			if (in.is_open() == false) {
				std::cout << "파일 열기 실패 " << std::endl;

			}

			int bufferSize;
			in.read((char*)&bufferSize, 4);

			LoadFile.GetSaveInstanceBuffer().resize(bufferSize);
			LoadFile.SetInstanceBufferSize(bufferSize);
			for (int i = 0; i < bufferSize; ++i) {
				int objectNumber;
				in.read((char*)&objectNumber, 4);
				LoadFile.GetSaveInstanceBuffer()[i].objectNumber = objectNumber;
				LoadFile.GetSaveInstanceBuffer()[i].objectWorld.resize(objectNumber);
				in.read((char*)&LoadFile.GetSaveInstanceBuffer()[i].objectWorld[0], objectNumber * sizeof(XMFLOAT4X4));
				//오브젝트 정보 로드 
			}
		
			shader[INSTANCSHADER]->DeleteAllObjcet();
			for (int i = 0; i < bufferSize; ++i) {
				int objectNumber = LoadFile.GetSaveInstanceBuffer()[i].objectNumber;
				for (int j = 1; /*except dummy*/ j < objectNumber; ++j) {
					shader[INSTANCSHADER]->LoadGameObject(device, commandList, 
						LoadFile.GetSaveInstanceBuffer()[i].objectWorld[j], scene->GetGraphicsRootSignature(), i);
				}
			}

			bufferSize;
			in.read((char*)&bufferSize, 4);

			LoadFile.GetSaveInstanceBuffer().clear();
			LoadFile.GetSaveInstanceBuffer().resize(bufferSize);
			LoadFile.SetInstanceBufferSize(bufferSize);
			
			for (int i = 0; i < bufferSize; ++i) {
				int objectNumber;
				in.read((char*)&objectNumber, 4);
				LoadFile.GetSaveInstanceBuffer()[i].objectNumber = objectNumber;
				LoadFile.GetSaveInstanceBuffer()[i].objectWorld.resize(objectNumber);
				in.read((char*)&LoadFile.GetSaveInstanceBuffer()[i].objectWorld[0], objectNumber * sizeof(XMFLOAT4X4));
				//캐릭터 
			}

			shader[SKINEDINSTANCSHADER]->DeleteAllObjcet();
			for (int i = 0; i < bufferSize; ++i) {
				int objectNumber = LoadFile.GetSaveInstanceBuffer()[i].objectNumber;
				for (int j = 1; /*except dummy*/ j < objectNumber; ++j) {
					shader[SKINEDINSTANCSHADER]->LoadGameObject(device, commandList,
						LoadFile.GetSaveInstanceBuffer()[i].objectWorld[j], scene->GetGraphicsRootSignature(), i);
				}
			}


			lightGUI->DeleteAllLight();
			//GUI의 모든 조명 정보를 지운다.

			LIGHTS* light = scene->GetLights();
			in.read((char*)&light->lights[0], sizeof(LIGHT) * MAX_LIGHTS);
			for (int i = 0; i < MAX_LIGHTS; ++i) {
				if (light->lights[i].enable == true) {
					lightGUI->AddLight(i, light->lights[i]);
				}
			}
			
			//조명 정보 읽어오기 

			ParticleShader* pShader = static_cast<ParticleShader*>(shader[PARTICLESHADER]);
			int pSize;
			int blobSize;
			particleGUI->ClearVec();
			pShader->ClearParticle();

			in.read((char*)&pSize, 4);
			in.read((char*)&blobSize, 4);

			std::vector<ParticleInfoInCPU> tempParticle;
			std::vector<ParticleBlobModify> tempblob;
			tempParticle.resize(pSize);
			tempblob.resize(blobSize);

			if (pSize != 0) {
				in.read((char*)&tempParticle[0], pSize * sizeof(ParticleInfoInCPU));
			}
			if (blobSize != 0) {
				in.read((char*)&tempblob[0], blobSize * sizeof(ParticleBlobModify));
			}
			for (int i = 0; i < blobSize; ++i) {
				pShader->CreateCopyBuffer(device, commandList, tempblob[i]);
			}
			particleGUI->InitShape();

			for (int i = 0; i < pSize; ++i) {
				pShader->CreateParticle(device, commandList, tempParticle[i]);
			}

			for (int i = 0; i < pSize; ++i) {
				particleGUI->AddParticleIndex(i, 0);
			}
			//파티클정보 읽어오기 
			HeightMapTerrain* terrain = scene->GetTerrain();
			HeightMapGridMesh* mesh = (HeightMapGridMesh*)terrain->GetMesh(0);
			int vertexNum = mesh->GetVertexNum();
			LoadFile.GetSaveDiffuseVertex().resize(vertexNum);
			for (int i = 0; i < terrain->GetMeshNum(); ++i) {
				HeightMapGridMesh* tempMesh = (HeightMapGridMesh*)terrain->GetMesh(i);
				for (int j = 0; j < TERRAINLAYERNUM; ++j)

				if (in.read((char*)&LoadFile.GetSaveDiffuseVertex()[0], vertexNum * sizeof(SaveDiffuseVertex))) {
					for (int j = 0; j < vertexNum; ++j) {
						tempMesh->GetDiffuseTexturedVertex()[j].position = LoadFile.GetSaveDiffuseVertex()[j].position;
						tempMesh->GetDiffuseTexturedVertex()[j].diffuse = LoadFile.GetSaveDiffuseVertex()[j].diffuse;
						tempMesh->GetDiffuseTexturedVertex()[j].texNumber = LoadFile.GetSaveDiffuseVertex()[j].texNumber;
					}
				}
				else {
					std::cout << "Error code: " << strerror(errno);
				}
				tempMesh->calALLNormal();
			}
			//파티클 정보 읽어오기 

			in.close();

			for (int i = 0; i < terrain->GetMeshNum(); ++i) {
				HeightMapGridMesh* tempMesh = (HeightMapGridMesh*)terrain->GetMesh(i);
				tempMesh->BuildLayer(device, commandList);
			}
		}

		ImGui::Checkbox("BoundingBox", &renderBoundingBox);
		ImGui::Checkbox("BillboardLight", &renderLight);
		ImGui::Checkbox("Show NaviMesh", &renderNavi);


		InstancingShader* insShader = reinterpret_cast<InstancingShader*>(scene->GetShaders()[INSTANCSHADER]);
		insShader->renderBoundingBox = renderBoundingBox;
		insShader->renderLight = renderLight;
		SkinnedAnimationStandardInstanceShader* skinnedInsShader = reinterpret_cast<SkinnedAnimationStandardInstanceShader*>(scene->GetShaders()[SKINEDINSTANCSHADER]);
		skinnedInsShader->renderBoundingBox = renderBoundingBox;

		if (ImGui::Button("save Bounding Box Imform")) {
			//바운딩 박스 0번에는 객체의 모든 크기를 담은 바운딩 박스가 담겨있다.

			std::ofstream out("Assets/Bounding/bInform.txt", std::ios::out | std::ios::trunc | std::ios::binary);
			InstancingShader* is = static_cast<InstancingShader*>(scene->GetShaders()[INSTANCSHADER]);
			auto isb = is->GetBuffer();
			for (int i = 0; i < isb.size(); ++i) {
				auto tbo = isb[i]->objects[0]->GetBoundingObject();
				if (tbo != NULL) {
					int bsize = tbo->size();
					out.write((const char*)&bsize, 4);
					std::vector<XMFLOAT4X4> twVec;
					std::vector<XMFLOAT3> tascale;
					std::vector<XMFLOAT3> trotate;
					std::vector<XMFLOAT3> tpos;

					for (int j = 0; j < bsize; ++j) {
						twVec.emplace_back((*tbo)[j]->GetWorld());
						tpos.emplace_back((*tbo)[j]->GettPos());
						tascale.emplace_back((*tbo)[j]->GetAllScale());
						trotate.emplace_back((*tbo)[j]->GetInputAngle());
					}


					out.write((const char*)&twVec[0], sizeof(XMFLOAT4X4) * bsize);
					out.write((const char*)&tascale[0], sizeof(XMFLOAT3) * bsize);
					out.write((const char*)&trotate[0], sizeof(XMFLOAT3) * bsize);
					out.write((const char*)&tpos[0], sizeof(XMFLOAT3) * bsize);

				}
			}
			out.close();

		}
		if (ImGui::Button("Calculate NaviMesh")) {
			
		}


		ImGui::End();
	}
	
	

}

void OptionGUI::useShortCut(char c)
{
	if (c == 'm')   modifiygui = 1;
	if (c == 'n')	modifiygui = 0;
}


/////////////////////////////////////////////////////////////////////////////ObjectGUI

ObjectGUI::ObjectGUI() {

	isGravity = false;
	tempTransform = { 1.0f , 0.0f , 0.0f ,0.0f,
					0.0f , 1.0f , 0.0f , 0.0f,
					0.0f , 0.0f , 1.0f , 0.0f,
					0.0f , 0.0f , 0.0f , 1.0f };

}

void ObjectGUI::Render()
{
	//1. 
	{
		static float f = 0.0f;
		static int counter = 0;

		ImGui::Begin("Object Option");                          // Create a window called "Hello, world!" and append into it.
		ImGui::RadioButton("New", &NewOrEditFlags, 0);
		ImGui::RadioButton("Edit", &NewOrEditFlags, 1);
		
		ImGui::Text("---------------------------------------");


		if (NewOrEditFlags) {
			EditObject();
		}
		
		if (!NewOrEditFlags) {
			NewObject();
		}
	
		ImGui::End();
	}

}

void ObjectGUI::SetDummy(GameObject * dummyObject)
{
	this->dummyObject = dummyObject;
}



void ObjectGUI::useShortCut(char c)
{
	if (c == 'q') {
		Radioflags = 0;
	}
	else if (c == 'w') {
		Radioflags = 1;
	}
	else if (c == 'e') {
		Radioflags = 2;
	}
	else if (c == 'r') {
		Radioflags = 3;
	}
	else if (c == 'd') {
		Radioflags = 4;
	}

}

ObjectInform ObjectGUI::GetLocatedObjectType()
{
	if (objectflags == 0) {
		return objectinform;
	}
	return  characeterinform;
}

void ObjectGUI::EditObject()
{
	ImGui::Text("World Positon");// Display some text (you can use a format strings too)
	if (pickingObject) {
		XMFLOAT3 objpos = pickingObject->GetPosition();
		XMFLOAT3 objrotate = pickingObject->GetRotatingAngle();

		ImGui::Text("x : %.3f   y : %.3f  z : %.3f", objpos.x, objpos.y, objpos.z);
		ImGui::Text("World Rotating");// Display some text (you can use a format strings too)
		ImGui::Text("x : %.3f   y : %.3f  z : %.3f", objrotate.x, objrotate.y, objrotate.z);
	}
	else
		ImGui::Text("Not yet selected Object");

	ImGui::RadioButton("Control Camera(Q)", &Radioflags, 0);
	ImGui::RadioButton("Move Pick Object(W)", &Radioflags, 1);
	ImGui::RadioButton("Rotate Pick Object(E)", &Radioflags, 2);
	ImGui::RadioButton("Scale Pick Object(R)", &Radioflags, 3);
	ImGui::RadioButton("Delete Pick Object(D)", &Radioflags, 4);

	//if (Radioflags == 1) {
	//	if (pickingObject != NULL) {
	//		XMFLOAT3 pos = pickingObject->GetPosition();

	//		ImGui::Text("Move x");
	//		ImGui::InputFloat("X", &pos.x, 0.1f, 1.0f, "%.3f");
	//		ImGui::Text("Move y");
	//		ImGui::InputFloat("Y", &pos.y, 0.1f, 1.0f, "%.3f");
	//		ImGui::Text("Move z");
	//		ImGui::InputFloat("Z", &pos.z, 0.1f, 1.0f, "%.3f");

	//		pickingObject->SetPosition(pos);

	//	}
	//}

	//if (Radioflags == 2) {
	//	if (pickingObject != NULL) {
	//		ImGui::InputFloat("rotateX", &pickingObject->GetInputAngle().x, 1.0f, 1.0f, "%.3f");
	//		pickingObject->Rotate(pickingObject->GetInputAngle().x - pickingObject->GetRotatingAngle().x, 0.0f, 0.0f);

	//		ImGui::InputFloat("rotateY", &pickingObject->GetInputAngle().y, 1.0f, 1.0f, "%.3f");
	//		pickingObject->Rotate(0.0f, pickingObject->GetInputAngle().y - pickingObject->GetRotatingAngle().y, 0.0f);

	//		ImGui::InputFloat("rotateZ", &pickingObject->GetInputAngle().z, 1.0f, 1.0f, "%.3f");
	//		pickingObject->Rotate(0.0f, 0.0f, pickingObject->GetInputAngle().z - pickingObject->GetRotatingAngle().z);
	//	}
	//}

	//if (Radioflags == 3) {
	//	if (pickingObject != NULL) {
	//		ImGui::InputFloat("Scale", &pickingObject->GetInputScale(), 0.1f, 1.0f, "%.3f");
	//		if (pickingObject->GetOldScale() != pickingObject->GetInputScale()) {
	//			pickingObject->Scale(pickingObject->GetInputScale());
	//			pickingObject->GetOldScale() = pickingObject->GetInputScale();
	//		}
	//	}
	//}

	if (pickingObject != NULL) {
		XMFLOAT4X4 transform = pickingObject->GetTransform();
		tempTransform._41 = transform._41;
		tempTransform._42 = transform._42;
		tempTransform._43 = transform._43;

		pickingObject->SetTransform(tempTransform);


		ImGui::Text("Input Scale");


		float scale = pickingObject->GetScale();
		ImGui::InputFloat("Scale", &scale, 0.1f, 1.0f, "%.3f");
		pickingObject->SetScaleTransform(scale, scale, scale);
		pickingObject->GetScale() = scale;


		ImGui::Text("Input Rotate");
		ImGui::InputFloat("rotateX", &pickingObject->GetInputAngle().x, 1.0f, 1.0f, "%.3f");
		pickingObject->Rotate(pickingObject->GetInputAngle().x, 0.0f, 0.0f);

		ImGui::InputFloat("rotateY", &pickingObject->GetInputAngle().y, 1.0f, 1.0f, "%.3f");
		pickingObject->Rotate(0.0f, pickingObject->GetInputAngle().y, 0.0f);

		ImGui::InputFloat("rotateZ", &pickingObject->GetInputAngle().z, 1.0f, 1.0f, "%.3f");
		pickingObject->Rotate(0.0f, 0.0f, pickingObject->GetInputAngle().z);


		XMFLOAT3 pos = pickingObject->GetPosition();

		ImGui::Text("Move x");
		ImGui::InputFloat("X", &pos.x, 0.1f, 1.0f, "%.3f");
		ImGui::Text("Move y");
		ImGui::InputFloat("Y", &pos.y, 0.1f, 1.0f, "%.3f");
		ImGui::Text("Move z");
		ImGui::InputFloat("Z", &pos.z, 0.1f, 1.0f, "%.3f");

		pickingObject->SetPosition(pos);
	}
}

void ObjectGUI::NewObject()
{
	//나중에 추가 오브젝트가 있으면 더 손볼 예정 
	
	ImGui::RadioButton("Object", &objectflags, 0);
	ImGui::RadioButton("character", &objectflags, 1);
	
	const char* objectsNameList[] = {   "PurpleTree0", "WatchTower" ,"WoodHouse" , "Ruins0" , "ConcreateBuilding0" , "Factory" ,"RedRoofHouse" , "Ruins1 ", "WireFence" , "OldStorge" , "Castle" , "Rock" , "PurpleTree1" , "PurpleTree2" , "pineTree" , "NoLeavesTree" , "Container" , "WareHouse" , "Bus" , "MilitaryCar0" , "RedCar"};

	const char* unityPackList0[] = { "WoodBeam0" , "WoodBeam1" , "woodPallet" , "woodPlatform0" ,  "woodPlatform1"  ,
		"woodStack0", "woodStack1" , "smallFence" , "scaffold" , "brickStack0" , "brickStack1" , "brickStack2" , "concreteBarrier" , "concreteBrickGroup0" , "concreteBrickGroup1", "concreteBrickGroup2" ,"concreteBrickGroup3" , "brokenWall0" , "brokenWall1" , "brokenWall2" , "brokenWall3" ,"brokenWall4" , "cityWall0" , "cityWall1" , "cityWallPillar" ,"lamp" ,"tapestry0" ,"tapestry1" ,"awning0" ,"awning1" ,"containerClose0" ,"containerClose1" ,"containerClose2" ,"containerClose3" ,"containerFill0" ,"containerFill1" ,"containerFill2" ,"containerFill3" ,"containerOpen0", "containerOpen1" , "containerOpen2" , "containerOpen3" ,"bigWallMain0" ,"bigWallMain1" ,"bigWallPillar0" ,"bigWallPillar1" ,"bigWallPillar2" ,"bigWall0" ,"bigWall1" ,"tire" ,"barrel0" ,"barrel1" ,"barrel2" ,"barrelOpen" ,"pipe0" ,"pipe1" ,"pipeGroup0" ,"pipeGroup1" ,"sign0" ,"sign1" ,"sign2" ,"sign3" ,"sign4" ,"sign5" ,"sign6" ,"sign7" ,"sign8" ,"sign9" ,"metalBorad0" ,"metalBorad1" ,"metalBorad2" , "vent" };

	const char* objectsNameList2[] = { "Grass0", "StreetLamp0", "Arrow" };

	static const char* current_item0 = NULL;
	static const char* current_item1 = NULL;
	static const char* current_item2 = NULL;

	if (objectflags == 0) {
		ImGui::Text("NO UNITY PACK");
		if (ImGui::BeginCombo("NO unity pack", current_item0)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(objectsNameList); n++)
			{
				bool is_selected = (current_item0 == objectsNameList[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(objectsNameList[n], is_selected)) {
					current_item0 = objectsNameList[n];
					objectinform.shaderIdx = INSTANCSHADER;
					objectinform.objectIdx = n;
					current_item1 = NULL;
					current_item2 = NULL;

				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}
		ImGui::Text("UNITY PACK0");
		if (ImGui::BeginCombo("unity pack0", current_item1)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(unityPackList0); n++)
			{
				bool is_selected = (current_item1 == unityPackList0[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(unityPackList0[n], is_selected)) {
					current_item1 = unityPackList0[n];
					objectinform.shaderIdx = INSTANCSHADER;
					objectinform.objectIdx = n + IM_ARRAYSIZE(objectsNameList);
					current_item0 = NULL;
					current_item2 = NULL;

				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}

		ImGui::Text("NO UNITY PACK2");
		if (ImGui::BeginCombo("NO unity pack2", current_item2)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(objectsNameList2); n++)
			{
				bool is_selected = (current_item2 == objectsNameList2[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(objectsNameList2[n], is_selected)) {
					current_item2 = objectsNameList2[n];
					objectinform.shaderIdx = INSTANCSHADER;
					objectinform.objectIdx = n + IM_ARRAYSIZE(objectsNameList) + IM_ARRAYSIZE(unityPackList0);
					current_item0 = NULL;
					current_item1 = NULL;

				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}


		if (dummyObject && objectinform.objectIdx != FAILPICKING) {
		
			ImGui::RadioButton("Scale", &transflags, 0);
			ImGui::RadioButton("Rotate", &transflags, 1);

			ImGui::Checkbox("Noise", &unitNoise);
			XMFLOAT4X4 transform = dummyObject->GetTransform();
			tempTransform._41 = transform._41;
			tempTransform._42 = transform._42;
			tempTransform._43 = transform._43;

			dummyObject->SetTransform(tempTransform);

			
			ImGui::Text("Input Scale");


			float scale = dummyObject->GetScale();
			ImGui::InputFloat("Scale", &scale, 0.1f, 1.0f, "%.3f");
			dummyObject->SetScaleTransform(scale, scale, scale);
			dummyObject->GetScale() = scale;


			ImGui::Text("Input Rotate");
			ImGui::InputFloat("rotateX", &dummyObject->GetInputAngle().x, 1.0f, 1.0f, "%.3f");
			dummyObject->Rotate(dummyObject->GetInputAngle().x, 0.0f, 0.0f);

			ImGui::InputFloat("rotateY", &dummyObject->GetInputAngle().y, 1.0f, 1.0f, "%.3f");
			dummyObject->Rotate(0.0f, dummyObject->GetInputAngle().y, 0.0f);

			ImGui::InputFloat("rotateZ", &dummyObject->GetInputAngle().z, 1.0f, 1.0f, "%.3f");
			dummyObject->Rotate(0.0f, 0.0f, dummyObject->GetInputAngle().z);

			
		}

	}

	if (objectflags == 1) {
		ImGui::Text("Choose the character to create ");
		const char* characterNameList[] = { "RifleMan", "SwordMan", "BowGirl" , "BigPoliceZombie" , "PoliceZombie" , "GirlZombie" , "MoleZombie" , "WomanZombie" , "TankerZombie" , "OfficerZombie" , "GentleZobmie" , "YasuoZombie" , "SimpleZombie" , "BurnedZombie", "BossZombie"};
		static const char* current_character = NULL;
		if (ImGui::BeginCombo("##combo", current_character)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(characterNameList); n++)
			{
				bool is_selected = (current_character == characterNameList[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(characterNameList[n], is_selected)) {
					current_character = characterNameList[n];
					characeterinform.shaderIdx = SKINEDINSTANCSHADER;
					characeterinform.objectIdx = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}
		if (dummyObject && characeterinform.objectIdx != FAILPICKING) {
		
			

			ImGui::RadioButton("Scale", &transflags, 0);
			ImGui::RadioButton("Rotate", &transflags, 1);

			XMFLOAT4X4 transform = dummyObject->GetTransform();
			tempTransform._41 = transform._41;
			tempTransform._42 = transform._42;
			tempTransform._43 = transform._43;

			dummyObject->SetTransform(tempTransform);


			
			ImGui::Text("Input Scale");
			XMFLOAT3 objrotate = dummyObject->GetRotatingAngle();
			float scale = dummyObject->GetScale();
			ImGui::InputFloat("Scale", &scale, 0.01f, 1.0f, "%.3f");

			dummyObject->SetScaleTransform(scale, scale, scale);
			dummyObject->GetScale() = scale;

			ImGui::Text("Input Rotate");
			ImGui::InputFloat("rotateX", &dummyObject->GetInputAngle().x, 1.0f, 1.0f, "%.3f");
			dummyObject->Rotate(dummyObject->GetInputAngle().x, 0.0f, 0.0f);

			ImGui::InputFloat("rotateY", &dummyObject->GetInputAngle().y, 1.0f, 1.0f, "%.3f");
			dummyObject->Rotate(0.0f, dummyObject->GetInputAngle().y, 0.0f);

			ImGui::InputFloat("rotateZ", &dummyObject->GetInputAngle().z, 1.0f, 1.0f, "%.3f");
			dummyObject->Rotate(0.0f, 0.0f, dummyObject->GetInputAngle().z);
			
		}
	}
	
	
	

	ImGui::RadioButton("Control Camera(Q)", &Radioflags, 0);
	ImGui::RadioButton("Locate Select Object(W)", &Radioflags, 1);

}

void MangerGUI::Render()
{
	ImGui::Begin("GUI Manger");
	for (int n = 0; n < 6; n++)
	{
		char buf[32];
		switch (n) {
		case 0:
			sprintf_s(buf, "Option GUI", n);
			break;
		case 1:
			sprintf_s(buf, "Object GUI", n);
			break;
		case 2:
			sprintf_s(buf, "Terrain GUI", n);
			break;
		case 3:
			sprintf_s(buf, "Light GUI", n);
			break;
		case 4:
			sprintf_s(buf, "Particle GUI", n);
			break;
		case 5:
			sprintf_s(buf, "Bounding GUI", n);
			break;
		}
		if (ImGui::Selectable(buf, selectedGui == n))
			selectedGui = n;
	}
	ImGui::End();
}



void TerrainGUI::useShortCut(char c)
{
	if (c == 'q') {
		Radioflags = 0;
	}
	else if (c == 'w') {
		Radioflags = 1;
	}
	else if (c == 'e') {
		Radioflags = 2;
	}
	else if (c == 'r') {
		Radioflags = 3;
	}
	else if (c == 't') {
		Radioflags = 4;
	}
	else if (c == 'a') {
		Radioflags = 5;
	}
	else if (c == 's') {
		Radioflags = 6;
	}
	else if (c == 'd') {
		Radioflags = 7;
	}
	else if (c == 'f') {
		Radioflags = 8;
	}

}

void TerrainGUI::Render()
{
	ImGui::Begin("Terrain GUI");

	ImGui::RadioButton("Control Camera(Q)", &Radioflags, 0);
	ImGui::RadioButton("UP Terrain Height(W)", &Radioflags, 1);
	ImGui::RadioButton("DOWN Terrain Height(E)", &Radioflags, 2);
	ImGui::RadioButton("FLAT Terrain Height(R)", &Radioflags, 3);
	ImGui::RadioButton("Change Circle Size(T)", &Radioflags, 4);
	ImGui::RadioButton("Change MinMax Hegiht(A)", &Radioflags, 5);
	ImGui::RadioButton("Splatting Texture(S)", &Radioflags, 6);
	ImGui::RadioButton("Smoothing Height(D)", &Radioflags, 7);
	ImGui::RadioButton("Terrain Options (A)", &Radioflags, 8);
	

	if (Radioflags == CHANGECIRCLESIZE) {
		if (circleObject != NULL) {
			ImGui::SliderFloat("Scale", &circleObject->GetInputScale(), 0.0f, 200.0f);
			if (circleObject->GetOldScale() != circleObject->GetInputScale()) {
				circleObject->Scale(circleObject->GetInputScale());
				if (circleObject->GetBoxType() == OBBBOX) {
					XMFLOAT3 extent = circleObject->GetOBB().Extents;
					extent.x = circleObject->GetOriginalExtent().x * circleObject->GetScale();
					extent.y = circleObject->GetOriginalExtent().y * circleObject->GetScale();
					extent.z = circleObject->GetOriginalExtent().z * circleObject->GetScale();
					circleObject->SetOBB(circleObject->GetPosition(), extent, circleObject->GetOBB().Orientation);
				}
				if (circleObject->GetBoxType() == AABBBOX) {
					XMFLOAT3 extent = circleObject->GetAABB().Extents;
					extent = Vector3::ScalarProduct(extent, circleObject->GetScale());
					circleObject->SetAABB(circleObject->GetPosition(), extent);
				}
				circleObject->GetOldScale() = circleObject->GetInputScale();
			}
		}
	}

	if (Radioflags == UPTERRAINHEIGHT || Radioflags == DOWNTERRAINHEIGHT) {
		ImGui::Text("Choose the TerrainShape");

		ImGui::RadioButton("CIRCLE", &terrainShapeFlags, 0);
		ImGui::RadioButton("SQURE", &terrainShapeFlags, 1);
		ImGui::RadioButton("NOISE", &terrainShapeFlags, 2);
	}
	if (Radioflags == FLATTERRAINHEIGHT) {
		ImGui::Text("Choose the FlatHeight");
		ImGui::SliderFloat("flatHeight", &flatHeight, -500.0f, 500.0f, "%.1f", 1.0f);
	}


	if (Radioflags == CHANGEMINMAX) {
		ImGui::SliderFloat("MAX UP HEIGHT", &maxHeight, 0.0f, 1000.0f);
		ImGui::SliderFloat("MIN DOWN HEIGHT", &minHeight, -1000.0f, 1000.0f);
	}
	if (Radioflags == TERRAINOPTIONS) {
		ImGui::Text("Reset");

		ImGui::Text("Select base");
		const char* textureNameList[] = { "Basic Grass" , "Cliff" , "Mude" , "Rock" , "Snow" , "Ice" , "Concrete" , "Asphalt" , "Beach" , "Soil" , "FloorStreet" , "SnowRoad" };
		static const char* current_item0 = NULL;

		if (ImGui::BeginCombo("##combo", current_item0)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(textureNameList); n++)
			{
				bool is_selected = (current_item0 == textureNameList[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(textureNameList[n], is_selected)) {
					current_item0 = textureNameList[n];
					autot0 = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}

		ImGui::Text("Select hill");
		static const char* current_item1 = NULL;

		if (ImGui::BeginCombo("##combo1", current_item1)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(textureNameList); n++)
			{
				bool is_selected = (current_item1 == textureNameList[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(textureNameList[n], is_selected)) {
					current_item1 = textureNameList[n];
					autot1 = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}

		ImGui::Text("Select cliff");

		static const char* current_item2 = NULL;
		if (ImGui::BeginCombo("##combo2", current_item2)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(textureNameList); n++)
			{
				bool is_selected = (current_item2 == textureNameList[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(textureNameList[n], is_selected)) {
					current_item2 = textureNameList[n];
					autot2 = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}

		if (ImGui::Button("Automatic Splatting")) {
			
			if (autot0 != FAILPICKING && autot1 != FAILPICKING && autot2 != FAILPICKING) {
				HeightMapGridMesh* terrainMesh = static_cast<HeightMapGridMesh*>(terrain->GetMesh(0));
				for (int i = 0; i < terrainMesh->GetVertexNum(); ++i) {
					float slope = 1.0f - terrainMesh->GetDiffuseTexturedVertex()[i].normal.y;
					if (slope < 0.2) {
						terrainMesh->GetDiffuseTexturedVertex()[i].texNumber = autot0;
					}
					else if (slope >= 0.2 && slope < 0.7) {
						terrainMesh->GetDiffuseTexturedVertex()[i].texNumber = autot1;
					}
					else if (slope >= 0.7) {
						terrainMesh->GetDiffuseTexturedVertex()[i].texNumber = autot2;
					}
				}
				
				terrainMesh->GetLayer()->ChangeLayerTexNum(terrainMesh->GetDiffuseTexturedVertex(), terrainMesh->GetVertexNum());
			}
			
		}

		ImGui::InputInt("Navi-Size", &naviSize);
		if (ImGui::Button("Create NavigationMesh")) {
			cbFlag = true;
		}

		if (ImGui::Button("Reset Object")) {
			Shader** shaders = scene->GetShaders();
			shaders[INSTANCSHADER]->DeleteAllObjcet();
			shaders[SKINEDINSTANCSHADER]->DeleteAllObjcet();
		}

	

	}
	if (Radioflags == SPLATTING) {
		ImGui::Text("Splatting Terrain Texture");
		ImGui::Text("Select Texture");
		const char* textureNameList[] = { "Basic Grass" , "Cliff" , "Mude" , "Rock" , "Snow" , "Ice" ,  "Concrete" , "Asphalt" , "Beach"  , "Soil" , "FloorStreet" , "SnowRoad"};
		static const char* current_item = NULL;

		if (ImGui::BeginCombo("##combo", current_item)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < IM_ARRAYSIZE(textureNameList); n++)
			{
				bool is_selected = (current_item == textureNameList[n]); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(textureNameList[n], is_selected)) {
					current_item = textureNameList[n];
					splittingTextureNumber = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}


	}

	ImGui::End();

}

LightGUI::LightGUI(Scene * scene)
{
	this->scene = scene;
	auto lights = scene->GetLights();
	for (int i = 0; i < MAX_LIGHTS; ++i) {
		if (lights->lights[i].enable == true)
			InitLight(i,lights->lights[i].type ,lights->lights[i]);
	}
}

void LightGUI::useShortCut(char c)
{
	if (c == 'q') {
		Radioflags = 0;
	}
	else if (c == 'w') {
		Radioflags = 1;
	}
	else if (c == 'e') {
		Radioflags = 2;
	}
	else if (c == 'r') {
		Radioflags = 3;
	}
}

void LightGUI::Render()
{
	LIGHTS* lights = scene->GetLights();
	ImGui::Begin("Light GUI");
	ImGui::RadioButton("Control Camera(Q)", &Radioflags, 0);
	ImGui::RadioButton("Add Light(W)", &Radioflags, 1);
	ImGui::RadioButton("Modify Light(E)", &Radioflags, 2);
	ImGui::RadioButton("Delete Light(R)", &Radioflags, 3);

	if (Radioflags == ADDLIGHT) {

		ImGui::Text("Add Light");
		ImGui::Text("Set Type");
		ImGui::RadioButton("Point Light", &lightFlags, POINT_LIGHT);
		ImGui::RadioButton("Spot Light", &lightFlags, SPOT_LIGHT);
		ImGui::RadioButton("Directional Light", &lightFlags, DIRECTIONAL_LIGHT);

		float posf3[3] = { pos.x , pos.y , pos.z };

		ImGui::Text("Input Position");
		ImGui::InputFloat3("Positon", posf3, "%.3f");
		pos.x = posf3[0];
		pos.y = posf3[1];
		pos.z = posf3[2];

		float dirf3[3] = { direction.x , direction.y , direction.z };
		ImGui::Text("Input Direction");
		ImGui::InputFloat3("Direction", dirf3, "%.3f");

		direction.x = dirf3[0];
		direction.y = dirf3[1];
		direction.z = dirf3[2];

		float dif3[3] = { diffuse.x , diffuse.y , diffuse.z };

		ImGui::Text("Input Diffuse");
		ImGui::InputFloat3("Diffuse", dif3, "%.3f");
		diffuse.x = dif3[0];
		diffuse.y = dif3[1];
		diffuse.z = dif3[2];

		ImGui::Text("Input Specualr");
		float specularf3[3] = { specular.x , specular.y , specular.z };
		ImGui::InputFloat3("specular", specularf3, "%.3f");
		specular.x = specularf3[0];
		specular.y = specularf3[1];
		specular.z = specularf3[2];
		ImGui::Text("Input Specualr factor");
		float specFactorf = specFactor;
		ImGui::InputFloat("Specualr factor", &specFactorf, 0.01f, 1.0f, "%.3f");
		specFactor = specFactorf;

		ImGui::Text("Input Ambient");
		float ambientf3[3] = { ambient.x , ambient.y ,ambient.z };
		ImGui::InputFloat3("ambient", ambientf3, "%.3f");
		ambient.x = ambientf3[0];
		ambient.y = ambientf3[1];
		ambient.z = ambientf3[2];

		if (lightFlags == POINT_LIGHT || lightFlags == SPOT_LIGHT) {

			ImGui::Text("Input Attenuation");
			float attenuationf3[3] = { attenuation.x , attenuation.y ,attenuation.z };
			ImGui::InputFloat3("attenuation", attenuationf3, "%.3f");
			attenuation.x = attenuationf3[0];
			attenuation.y = attenuationf3[1];
			attenuation.z = attenuationf3[2];

			ImGui::Text("Input Range");
			float frange = range;
			ImGui::InputFloat("Range", &frange, 0.01f, 1.0f, "%.3f");
			range = frange;
		}
		if (lightFlags == SPOT_LIGHT) {

			ImGui::Text("Input Falloff");
			float fall = falloff;
			ImGui::InputFloat("Falloff", &fall, 0.01f, 1.0f, "%.3f");
			falloff = fall;

			ImGui::Text("Input phi");
			float fphi = phi;
			ImGui::InputFloat("Phi", &fphi, 0.01f, 1.0f, "%.3f");
			phi = fphi;

			ImGui::Text("Input theta");
			float ftheta = theta;
			ImGui::InputFloat("Theta", &ftheta, 0.01f, 1.0f, "%.3f");
			theta = ftheta;
		}

		if (ImGui::Button("Add Light")) {
			for (int i = 0; i < MAX_LIGHTS; ++i) {
				if (!lights->lights[i].enable) {
					lights->lights[i].type = lightFlags;
					lights->lights[i].diffuse = XMFLOAT4(diffuse.x , diffuse.y , diffuse.z , 1.0f);
					lights->lights[i].ambient = XMFLOAT4(ambient.x, ambient.y, ambient.z, 1.0f);
					lights->lights[i].specular = XMFLOAT4{ specular.x ,specular.y , specular.z , specFactor };
					lights->lights[i].position = pos;
					lights->lights[i].direction = direction;
					lights->lights[i].enable = true;
					lights->lights[i].attenuation = attenuation;
					lights->lights[i].falloff = falloff;
					lights->lights[i].phi = phi;
					lights->lights[i].theta = theta;
					lights->lights[i].range = range;
					std::string s = "Light ";
					s = s + std::to_string(i) + "\0";
					vec.emplace_back(s);
					if (lightnumber != FAILPICKING)
						current_item = vec[lightnumber].c_str();
					else
						current_item = NULL;

					reinterpret_cast<InstancingShader*>(scene->GetShaders()[INSTANCSHADER])->AddLight(pos);

					break;

				}
			}
			pos = { 0.0f , 0.0f , 0.0f };
			direction = { 0.0f , 0.0f , 0.0f };
			diffuse = { 0.0f , 0.0f , 0.0f };
			specular ={ 0.0f , 0.0f , 0.0f };
			ambient = { 0.0f , 0.0f , 0.0f };
			specFactor = { 0.0 };
			attenuation = { 0.0f , 0.0f , 0.0f };
			falloff = 0;
			phi = 0;
			theta = 0;
			range = 0;

		}

	}
	
	if (Radioflags == MODIFYLIGHT) {
		if (ImGui::BeginCombo("##combo", current_item)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < vec.size(); n++)
			{
				bool is_selected = (current_item == vec[n].c_str()); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(vec[n].c_str(), is_selected)) {
					current_item = vec[n].c_str();
					lightnumber = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}

		if (lightnumber != FAILPICKING)
		{
			auto light = &lights->lights[lightnumber];

			if (light->type == DIRECTIONAL_LIGHT) {
				ImGui::Text("Type : Direction Light");
			}

			if (light->type == SPOT_LIGHT) {
				ImGui::Text("Type : Spot Light");
			}

			if (light->type == POINT_LIGHT) {
				ImGui::Text("Type : POINT Light");
			}

			float posf3[3] = { light->position.x , light->position.y , light->position.z };

			ImGui::Text("Input Position");
			ImGui::InputFloat3("Positon", posf3, "%.3f");
			light->position.x = posf3[0];
			light->position.y = posf3[1];
			light->position.z = posf3[2];
			reinterpret_cast<InstancingShader*>(scene->GetShaders()[INSTANCSHADER])->ModifyLight(lightnumber + 1, light->position);

			float dirf3[3] = { light->direction.x , light->direction.y , light->direction.z };
			ImGui::Text("Input Direction");
			ImGui::SliderFloat("dirX", &dirf3[0], -1.0f, 1.0f, "%.1f", 1.0f);
			ImGui::SliderFloat("dirY", &dirf3[1], -1.0f, 1.0f, "%.1f", 1.0f);
			ImGui::SliderFloat("dirZ", &dirf3[2], -1.0f, 1.0f ,"%.1f", 1.0f );

			light->direction.x = dirf3[0];
			light->direction.y = dirf3[1];
			light->direction.z = dirf3[2];

			float dif3[3] = { light->diffuse.x , light->diffuse.y , light->diffuse.z };

			ImGui::Text("Input Diffuse");
			ImGui::InputFloat3("Diffuse", dif3, "%.3f");
			light->diffuse.x = dif3[0];
			light->diffuse.y = dif3[1];
			light->diffuse.z = dif3[2];

			ImGui::Text("Input Specualr");
			float specularf3[3] = { light->specular.x , light->specular.y , light->specular.z };
			ImGui::InputFloat3("specular", specularf3, "%.3f");
			light->specular.x = specularf3[0];
			light->specular.y = specularf3[1];
			light->specular.z = specularf3[2];
			ImGui::Text("Input Specualr factor");
			float specFactorf = light->specular.w;
			ImGui::InputFloat("Specualr factor", &specFactorf, 0.01f, 1.0f, "%.3f");
			light->specular.w = specFactorf;

			ImGui::Text("Input ambient");
			float ambientf3[3] = { light->ambient.x ,  light->ambient.y , light->ambient.z };
			ImGui::InputFloat3("ambient", ambientf3, "%.3f");
			light->ambient.x = ambientf3[0];
			light->ambient.y = ambientf3[1];
			light->ambient.z = ambientf3[2];

			if (light->type == SPOT_LIGHT || light->type == POINT_LIGHT) {
				ImGui::Text("Input Attenuation");
				float attenuationf3[3] = { light->attenuation.x , light->attenuation.y ,light->attenuation.z };
				ImGui::InputFloat3("attenuation", attenuationf3, "%.3f");
				light->attenuation.x = attenuationf3[0];
				light->attenuation.y = attenuationf3[1];
				light->attenuation.z = attenuationf3[2];

				ImGui::Text("Input Range");
				float frange = light->range;
				ImGui::InputFloat("Range", &frange, 0.01f, 1.0f, "%.3f");
				light->range = frange;
			}

			if (light->type == SPOT_LIGHT) {
				ImGui::Text("Input Falloff");
				float fall = light->falloff;
				ImGui::InputFloat("Falloff", &fall, 0.01f, 1.0f, "%.3f");
				light->falloff = fall;

				ImGui::Text("Input phi");
				float fphi = light->phi;
				ImGui::InputFloat("Phi", &fphi, 0.01f, 1.0f, "%.3f");
				light->phi = fphi;

				ImGui::Text("Input theta");
				float ftheta = light->theta;
				ImGui::InputFloat("Theta", &ftheta, 0.01f, 1.0f, "%.3f");
				light->theta = ftheta;
			}
		}
	}
	if (Radioflags == DELETELIGHT) {
		if (ImGui::BeginCombo("##combo", current_deleteitem)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < vec.size(); n++)
			{
				bool is_selected = (current_deleteitem == vec[n].c_str()); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(vec[n].c_str(), is_selected)) {
					current_deleteitem = vec[n].c_str();
					deleteLightNumber = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}
		if (ImGui::Button("Delete Light")) {
			if (deleteLightNumber != FAILPICKING) {
				std::string str = vec[deleteLightNumber];
				std::stringstream changeStringToNumber(str);
				int disEnableNumber;
				changeStringToNumber >> disEnableNumber;
				lights->lights[disEnableNumber].enable = false;
				vec.erase(vec.begin() + deleteLightNumber);
				if (vec.empty()) {
					current_deleteitem = NULL;
					current_item = NULL;
					deleteLightNumber = -1;
					lightnumber = -1;
				}
				else {
					current_deleteitem = vec[0].c_str();
					current_item = vec[0].c_str();
					deleteLightNumber = 0;
					lightnumber = 0;
				}
			}
		}

	}
	ImGui::End();

}

void LightGUI::AddLight(int idx, LIGHT light)
{
	std::string s = "Light ";
	s = s + std::to_string(idx) + "\0";
	vec.emplace_back(s);
	if (lightnumber != FAILPICKING)
		current_item = vec[lightnumber].c_str();
	else
		current_item = NULL;

	reinterpret_cast<InstancingShader*>(scene->GetShaders()[INSTANCSHADER])->AddLight(light.position);

}

void LightGUI::InitLight(int idx, int type, LIGHT & light)
{
	std::string s = "Light ";
	s = s + std::to_string(idx) + "\0";
	vec.emplace_back(s);
	if (lightnumber != FAILPICKING)
		current_item = vec[lightnumber].c_str();
	else
		current_item = NULL;
}


void LightGUI::DeleteAllLight()
{
	for (int i = 0; i < MAX_LIGHTS; ++i) {
		scene->GetLights()->lights[i].enable = false;
	}


	vec.clear();
	current_deleteitem = NULL;
	current_item = NULL;
	deleteLightNumber = -1;
	lightnumber = -1;


}
ParticleGUI::ParticleGUI(Scene * scene ,ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	this->scene = scene;
	this->particleShader = (ParticleShader*)scene->GetShaders()[PARTICLESHADER];

	this->commandList = commandList;
	this->device = device;

	InitShape();
}

void ParticleGUI::InitShape()
{
	for (int i = 0; i < particleShader->blobList.size(); ++i)
	{
		std::string s = "Shape ";
		s = s + std::to_string(i) + "\0";
		shapeVec.emplace_back(s);
		if (shapeNumber != FAILPICKING)
			current_item = shapeVec[shapeNumber].c_str();
		else
			current_item = NULL;

		curShapeIdx++;
	}
}

void ParticleGUI::AddParticleIndex(int idx ,int copyidx)
{
	std::string s = "Particle ";
	s = s + std::to_string(idx);
	ParticleInForm temp;
	temp.name = s;
	temp.index = idx;
	temp.copyidx = copyidx;

	vec.emplace_back(temp);

}

void ParticleGUI::useShortCut(char c)
{
	if (c == 'q') {
		Radioflags = 0;
	}
	else if (c == 'w') {
		Radioflags = 1;
	}
	else if (c == 'e') {
		Radioflags = 2;
	}
	else if (c == 'r') {
		Radioflags = 3;
	}
}

void ParticleGUI::Render()
{
	ImGui::Begin("Particle GUI");

	ImGui::RadioButton("Control Camera(Q)", &Radioflags, 0);
	ImGui::RadioButton("Add Shape(A)", &Radioflags, 1);
	ImGui::RadioButton("Modify Shape(S)", &Radioflags, 2);
	ImGui::RadioButton("Delete Shape(D)", &Radioflags, 3);
	ImGui::RadioButton("Add Particle(W)", &Radioflags, 4);
	ImGui::RadioButton("Modify Paritcle(E)", &Radioflags, 5);
	ImGui::RadioButton("Delete Particle(R)", &Radioflags, 6);

	if (Radioflags == ADDSHAPE) {

		ImGui::Text("------------------");
		ImGui::Text("NEED BUTTON OPTION");
		ImGui::Text("------------------");

		ImGui::Text("Input MinMaxPos");
		float minPosf3[3] = { shapeData.positionMin.x , shapeData.positionMin.y , shapeData.positionMin.z };
		ImGui::InputFloat3("MinPositon", minPosf3, "%.3f");
		shapeData.positionMin = XMFLOAT3{ minPosf3[0] , minPosf3[1] , minPosf3[2] };

		float maxPosf3[3] = { shapeData.positionMax.x , shapeData.positionMax.y , shapeData.positionMax.z };
		ImGui::InputFloat3("MaxPositon", maxPosf3, "%.3f");
		shapeData.positionMax = XMFLOAT3{ maxPosf3[0] , maxPosf3[1] , maxPosf3[2] };

		ImGui::Text("Input MinMaxVelocity");
		float minVecf3[3] = { shapeData.velocityMin.x , shapeData.velocityMin.y , shapeData.velocityMin.z };
		ImGui::InputFloat3("MinVelocity", minVecf3, "%.3f");
		shapeData.velocityMin = XMFLOAT3{ minVecf3[0] , minVecf3[1] , minVecf3[2] };

		float maxVecf3[3] = { shapeData.velocityMax.x , shapeData.velocityMax.y , shapeData.velocityMax.z };
		ImGui::InputFloat3("MaxVelocity", maxVecf3, "%.3f");
		shapeData.velocityMax = XMFLOAT3{ maxVecf3[0] , maxVecf3[1] , maxVecf3[2] };

		ImGui::Text("Input MinMaxAge");
		float minAge = shapeData.ageMin;
		ImGui::InputFloat("MinAge", &minAge, 0.01f, 1.0f, "%.3f");
		shapeData.ageMin = minAge;

		float maxAge = shapeData.ageMax;
		ImGui::InputFloat("MaxAge", &maxAge, 0.01f, 1.0f, "%.3f");
		shapeData.ageMax = maxAge;

		ImGui::Text("Input ParticleAmount");
		int particleAmount = shapeData.amount;
		ImGui::InputInt("ParicleAmount", &particleAmount);
		shapeData.amount = particleAmount;

		if (ImGui::Button("AddShape")) {
			particleShader->CreateCopyBuffer(device, commandList, shapeData);
			std::string s = "Shape ";
			s = s + std::to_string(curShapeIdx++);
			shapeVec.emplace_back(s);
		}
	}

	if (Radioflags == MODIFYSHAPE) {
		if (ImGui::BeginCombo("##combo", current_item)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < shapeVec.size(); n++)
			{
				bool is_selected = (current_item == shapeVec[n].c_str()); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(shapeVec[n].c_str(), is_selected)) {
					current_item = shapeVec[n].c_str();
					shapeNumber = n;
				}
				if (is_selected) {
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
			}

			ImGui::EndCombo();
		}
		if (shapeNumber != FAILPICKING)
		{
			ParticleBlobModify& blob = particleShader->blobList[shapeNumber];

			ImGui::Text("------------------");
			ImGui::Text("NEED BUTTON OPTION");
			ImGui::Text("------------------");

			ImGui::Text("Input MinMaxPos");
			float minPosf3[3] = { blob.positionMin.x , blob.positionMin.y , blob.positionMin.z };
			ImGui::InputFloat3("MinPositon", minPosf3, "%.3f");
			blob.positionMin = XMFLOAT3{ minPosf3[0] , minPosf3[1] , minPosf3[2] };

			float maxPosf3[3] = { blob.positionMax.x , blob.positionMax.y , blob.positionMax.z };
			ImGui::InputFloat3("MaxPositon", maxPosf3, "%.3f");
			blob.positionMax = XMFLOAT3{ maxPosf3[0] , maxPosf3[1] , maxPosf3[2] };

			ImGui::Text("Input MinMaxVelocity");
			float minVecf3[3] = { blob.velocityMin.x , blob.velocityMin.y , blob.velocityMin.z };
			ImGui::InputFloat3("MinVelocity", minVecf3, "%.3f");
			blob.velocityMin = XMFLOAT3{ minVecf3[0] , minVecf3[1] , minVecf3[2] };

			float maxVecf3[3] = { blob.velocityMax.x , blob.velocityMax.y , blob.velocityMax.z };
			ImGui::InputFloat3("MaxVelocity", maxVecf3, "%.3f");
			blob.velocityMax = XMFLOAT3{ maxVecf3[0] , maxVecf3[1] , maxVecf3[2] };

			ImGui::Text("Input MinMaxAge");
			float minAge = blob.ageMin;
			ImGui::InputFloat("MinAge", &minAge, 0.01f, 1.0f, "%.3f");
			blob.ageMin = minAge;

			float maxAge = blob.ageMax;
			ImGui::InputFloat("MaxAge", &maxAge, 0.01f, 1.0f, "%.3f");
			blob.ageMax = maxAge;

			ImGui::Text("Input ParticleAmount");
			int particleAmount = blob.amount;
			ImGui::InputInt("ParicleAmount", &particleAmount);
			blob.amount = particleAmount;

			if (ImGui::Button("Modify")) {
				particleShader->ModifyCopyBuffer(device, commandList, blob, shapeNumber);
			}
		}

	}

	if (Radioflags == DELETESHAPE) {
		if (ImGui::BeginCombo("##combo", current_item)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < shapeVec.size(); n++)
			{
				bool is_selected = (current_item == shapeVec[n].c_str()); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(shapeVec[n].c_str(), is_selected)) {
					current_item = shapeVec[n].c_str();
					deleteNumber = n;
					deleteIdx = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}

		if (ImGui::Button("Delete Shape")) {
			if (deleteNumber != FAILPICKING) {
				std::vector<ParticleInfoInCPU>& particles = particleShader->GetParticles();

				for (int i = 0; i < particles.size(); ++i)
				{
					if (particles[i].copyIdx == deleteNumber)
						deleteFail = true;
				}
				if (deleteFail == true)
				{
					std::cout << "사용중인 Shape는 삭제할 수 없습니다." << std::endl;
				}
				else {
					particleShader->DeleteCopyBuffer(deleteNumber);

					shapeVec.erase(shapeVec.begin() + deleteNumber);

					if (!shapeVec.empty()) {
						delete_item = shapeVec[0].c_str();
						current_item = shapeVec[0].c_str();
						deleteNumber = 0;
						shapeNumber = 0;
						deleteIdx = 0;
						curShapeIdx--;
					}
					else {
						delete_item = NULL;
						current_item = NULL;
						deleteNumber = FAILPICKING;
						particleNumber = FAILPICKING;
					}
				}
				deleteFail = false;
			}
		}
	}


	if (Radioflags == ADDPARTICLE) {
		if (ImGui::BeginCombo("##combo", current_item)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < shapeVec.size(); n++)
			{
				bool is_selected = (current_item == shapeVec[n].c_str()); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(shapeVec[n].c_str(), is_selected)) {
					current_item = shapeVec[n].c_str();
					shapeNumber = n;
					shapeIdx = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}
	}

	if (Radioflags == MODIFYPARTICLE) {


		if (ImGui::BeginCombo("##combo", current_item)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < vec.size(); n++)
			{
				bool is_selected = (current_item == vec[n].name.c_str()); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(vec[n].name.c_str(), is_selected)) {
					current_item = vec[n].name.c_str();
					particleNumber = vec[n].index;

				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}

		if (particleNumber != FAILPICKING) {
			std::vector<ParticleInfoInCPU>& particles = particleShader->GetParticles();
			
			float posf3[3] = { particles[particleNumber].position.x , particles[particleNumber].position.y , particles[particleNumber].position.z };
			ImGui::Text("----------------------");
			ImGui::Text("NO NEED BUTTON OPTION");
			ImGui::Text("----------------------");
			ImGui::Text("Input Position");
			ImGui::InputFloat("PositionX", &posf3[0], 1.0f, 1.0f, "%.3f");
			ImGui::InputFloat("PositionY", &posf3[1], 1.0f, 1.0f, "%.3f");
			ImGui::InputFloat("PositionZ", &posf3[2], 1.0f, 1.0f, "%.3f");
			particles[particleNumber].position.x = posf3[0];
			particles[particleNumber].position.y = posf3[1];
			particles[particleNumber].position.z = posf3[2];

			float size = particles[particleNumber].size.x;
			ImGui::Text("Input Size");
			ImGui::InputFloat("Size", &size, 1.0f, 1.0f, "%.3f");

			particles[particleNumber].size.x = size;
			particles[particleNumber].size.y = size;

			float age = particles[particleNumber].age;
			ImGui::Text("Input Age");
			ImGui::InputFloat("Age", &age, 0.01f, 1.0f, "%.3f");
			particles[particleNumber].age = age;

			float divAge = particles[particleNumber].divAge;
			ImGui::Text("Input ParticleDivAge");
			ImGui::InputFloat("ParticleDivAge", &divAge);
			particles[particleNumber].divAge = divAge;


			int tIdx = particles[particleNumber].textureIdx;
			textureNumber = tIdx;
			const char* TextureNameList[] = { "Smoke" , "Light" , "Fire", "Pupple" };
			const char* currentTexture = TextureNameList[tIdx];
			if (ImGui::BeginCombo("##textureCombo", currentTexture)) // The second parameter is the label previewed before opening the combo.
			{
				for (int n = 0; n < IM_ARRAYSIZE(TextureNameList); n++)
				{
					bool is_selected = (currentTexture == TextureNameList[n]); // You can store your selection however you want, outside or inside your objects
					if (ImGui::Selectable(TextureNameList[n], is_selected)) {
						currentTexture = TextureNameList[n];
						textureNumber = n;
					}
					if (is_selected)
						ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
				ImGui::EndCombo();
			}
			particles[particleNumber].textureIdx = textureNumber;

			ImGui::Text("Input ParticleAcc");
			float particleAcc = particles[particleNumber].acc;
			ImGui::InputFloat("ParticleAcc", &particleAcc);
			particles[particleNumber].acc = particleAcc;
			
			ImGui::Text("Particle Shape");
			int cIdx = particles[particleNumber].copyIdx;
			shapeNumber = cIdx;
			if (ImGui::BeginCombo("##Shapecombo", current_item)) // The second parameter is the label previewed before opening the combo.
			{
				for (int n = 0; n < shapeVec.size(); n++)
				{
					bool is_selected = (current_item == shapeVec[n].c_str()); // You can store your selection however you want, outside or inside your objects
					if (ImGui::Selectable(shapeVec[n].c_str(), is_selected)) {
						current_item = shapeVec[n].c_str();
						shapeNumber = n;
					}
					if (is_selected)
						ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
				ImGui::EndCombo();
			}
			particles[particleNumber].copyIdx = shapeNumber;


			ImGui::Text("Particle type");
			int typeIdx = particles[particleNumber].type;
			typeNumber = typeIdx;
			const char* TypeNameList[] = { "Disappear", "Forever", "Tornado", "Smaller" };
			const char* currentType = TypeNameList[typeIdx];
			if (ImGui::BeginCombo("##typeCombo", currentType)) // The second parameter is the label previewed before opening the combo.
			{
				for (int n = 0; n < IM_ARRAYSIZE(TypeNameList); n++)
				{
					bool is_selected = (currentType == TypeNameList[n]); // You can store your selection however you want, outside or inside your objects
					if (ImGui::Selectable(TypeNameList[n], is_selected)) {
						currentType = TypeNameList[n];
						typeNumber = n;
					}
					if (is_selected)
						ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
				ImGui::EndCombo();
			}
			particles[particleNumber].type = typeNumber;

			ImGui::Text("Input AlphaDegree");
			float particleAlpha = particles[particleNumber].alphaDegree;
			ImGui::InputFloat("AlphaDegree", &particleAlpha);
			particles[particleNumber].alphaDegree = particleAlpha;

			ImGui::Text("Input Tornado");
			float radius = particles[particleNumber].radius;
			ImGui::InputFloat("Radius", &radius);
			particles[particleNumber].radius = radius;
			
			float yGab = particles[particleNumber].yGab;
			ImGui::InputFloat("YGab", &yGab);
			particles[particleNumber].yGab = yGab;
			
			float rotateAcc = particles[particleNumber].rotateAcc;
			ImGui::InputFloat("RotateAcc", &rotateAcc);
			particles[particleNumber].rotateAcc = rotateAcc;

		}
	}
	if (Radioflags == DELETEPARTICLE) {
		if (ImGui::BeginCombo("##combo", delete_item)) // The second parameter is the label previewed before opening the combo.
		{
			for (int n = 0; n < vec.size(); n++)
			{
				bool is_selected = (delete_item == vec[n].name.c_str()); // You can store your selection however you want, outside or inside your objects
				if (ImGui::Selectable(vec[n].name.c_str(), is_selected)) {
					delete_item = vec[n].name.c_str();
					deleteNumber = vec[n].index;
					deleteIdx = n;
				}
				if (is_selected)
					ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
			}
			ImGui::EndCombo();
		}


		if (ImGui::Button("Delete Particle")) {
			if (deleteNumber != FAILPICKING) {
				std::vector<ParticleInfoInCPU>& particles = particleShader->GetParticles();
				particles[deleteIdx].age = -1.0f;

				vec.erase(vec.begin() + deleteIdx);

				if (!vec.empty()) {
					delete_item = vec[0].name.c_str();
					current_item = vec[0].name.c_str();
					deleteNumber = vec[0].index;
					particleNumber = vec[0].index;
					deleteIdx = 0;
				}
				else {
					delete_item = NULL;
					current_item = NULL;
					deleteNumber = FAILPICKING;
					particleNumber = FAILPICKING;
				}
			}
		}

	}

	ImGui::End();

}

void ParticleGUI::ClearVec()
{
	curShapeIdx = 0;
	shapeVec.clear();
	vec.clear();
}

BoundingGUI::BoundingGUI(bool* cbFlag , bool* dbFlag)
{
	tempTransform = {   1.0f , 0.0f , 0.0f ,0.0f,
					    0.0f , 1.0f , 0.0f , 0.0f,
				    	0.0f , 0.0f , 1.0f , 0.0f,
					    0.0f , 0.0f , 0.0f , 1.0f };

	this->cbFlag = cbFlag;
	this->dbFlag = dbFlag;

}

void BoundingGUI::Render()
{
	ImGui::Begin("BOUNDING GUI");
	ImGui::RadioButton("Control Camera(Q)", &Radioflags, 0);
	ImGui::RadioButton("Setting BoundingBox(W)", &Radioflags, 1);

	if (Radioflags == 1) {
		ImGui::RadioButton("Object", &objectflags, 0);
		ImGui::RadioButton("character", &objectflags, 1);

		const char* objectsNameList[] = { "PurpleTree0", "WatchTower" ,"WoodHouse" , "Ruins0" , "ConcreateBuilding0" , "Factory" ,"RedRoofHouse" , "Ruins1 ", "WireFence" , "OldStorge" , "Castle" , "Rock" , "PurpleTree1" , "PurpleTree2" , "pineTree" , "NoLeavesTree" , "Container" , "WareHouse" , "Bus" , "MilitaryCar0" , "RedCar" };

		const char* unityPackList0[] = { "WoodBeam0" , "WoodBeam1" , "woodPallet" , "woodPlatform0" ,  "woodPlatform1"  ,
			"woodStack0", "woodStack1" , "smallFence" , "scaffold" , "brickStack0" , "brickStack1" , "brickStack2" , "concreteBarrier" , "concreteBrickGroup0" , "concreteBrickGroup1", "concreteBrickGroup2" ,"concreteBrickGroup3" , "brokenWall0" , "brokenWall1" , "brokenWall2" , "brokenWall3" ,"brokenWall4" , "cityWall0" , "cityWall1" , "cityWallPillar" ,"lamp" ,"tapestry0" ,"tapestry1" ,"awning0" ,"awning1" ,"containerClose0" ,"containerClose1" ,"containerClose2" ,"containerClose3" ,"containerFill0" ,"containerFill1" ,"containerFill2" ,"containerFill3" ,"containerOpen0", "containerOpen1" , "containerOpen2" , "containerOpen3" ,"bigWallMain0" ,"bigWallMain1" ,"bigWallPillar0" ,"bigWallPillar1" ,"bigWallPillar2" ,"bigWall0" ,"bigWall1" ,"tire" ,"barrel0" ,"barrel1" ,"barrel2" ,"barrelOpen" ,"pipe0" ,"pipe1" ,"pipeGroup0" ,"pipeGroup1" ,"sign0" ,"sign1" ,"sign2" ,"sign3" ,"sign4" ,"sign5" ,"sign6" ,"sign7" ,"sign8" ,"sign9" ,"metalBorad0" ,"metalBorad1" ,"metalBorad2" , "vent" };

		const char* objectsNameList2[] = { "Grass0", "StreetLamp0", "Arrow" };

		static const char* current_item0 = NULL;
		static const char* current_item1 = NULL;
		static const char* current_item2 = NULL;

		static const char* bBox_item = NULL;

		if (objectflags == 0) {
			ImGui::Text("NO UNITY PACK");
			if (ImGui::BeginCombo("NO unity pack", current_item0)) // The second parameter is the label previewed before opening the combo.
			{
				for (int n = 0; n < IM_ARRAYSIZE(objectsNameList); n++)
				{
					bool is_selected = (current_item0 == objectsNameList[n]); // You can store your selection however you want, outside or inside your objects
					if (ImGui::Selectable(objectsNameList[n], is_selected)) {
						current_item0 = objectsNameList[n];
						objectinform.shaderIdx = INSTANCSHADER;
						objectinform.objectIdx = n;
						bIdx = -1;
						current_item1 = NULL;
						current_item2 = NULL;

					}
					if (is_selected)
						ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
				ImGui::EndCombo();
			}
			ImGui::Text("UNITY PACK0");
			if (ImGui::BeginCombo("unity pack0", current_item1)) // The second parameter is the label previewed before opening the combo.
			{
				for (int n = 0; n < IM_ARRAYSIZE(unityPackList0); n++)
				{
					bool is_selected = (current_item1 == unityPackList0[n]); // You can store your selection however you want, outside or inside your objects
					if (ImGui::Selectable(unityPackList0[n], is_selected)) {
						current_item1 = unityPackList0[n];
						objectinform.shaderIdx = INSTANCSHADER;
						objectinform.objectIdx = n + IM_ARRAYSIZE(objectsNameList);
						bIdx = -1;
						current_item0 = NULL;
						current_item2 = NULL;

					}
					if (is_selected)
						ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
				ImGui::EndCombo();
			}

			ImGui::Text("NO UNITY PACK2");
			if (ImGui::BeginCombo("NO unity pack2", current_item2)) // The second parameter is the label previewed before opening the combo.
			{
				for (int n = 0; n < IM_ARRAYSIZE(objectsNameList2); n++)
				{
					bool is_selected = (current_item2 == objectsNameList2[n]); // You can store your selection however you want, outside or inside your objects
					if (ImGui::Selectable(objectsNameList2[n], is_selected)) {
						current_item2 = objectsNameList2[n];
						objectinform.shaderIdx = INSTANCSHADER;
						objectinform.objectIdx = n + IM_ARRAYSIZE(objectsNameList) + IM_ARRAYSIZE(unityPackList0);
						bIdx = -1;
						current_item0 = NULL;
						current_item1 = NULL;

					}
					if (is_selected)
						ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
				}
				ImGui::EndCombo();
			}


			if (objectinform.objectIdx != FAILPICKING && objectinform.shaderIdx != FAILPICKING) {

				if (selectedobject != NULL) {

					auto sbList = selectedobject->GetBoundingObject();

					if (sbList != NULL) {
						nameList.clear();
						nameList.reserve(10);

						for (int i = 0; i < sbList->size(); ++i) {
							std::string s = "b" + std::to_string(i) + "\0";
							nameList.emplace_back(s);
						}
						//gui 만들기
						ImGui::Text("Select Bounding Box");
						if (ImGui::BeginCombo("##bBox", bBox_item)) // The second parameter is the label previewed before opening the combo.
						{
							for (int n = 0; n < nameList.size(); n++)
							{
								bool is_selected = (bBox_item == nameList[n].c_str()); // You can store your selection however you want, outside or inside your objects
								if (ImGui::Selectable(nameList[n].c_str(), is_selected)) {
									bBox_item = nameList[n].c_str();
									bIdx = n;
								}
								if (is_selected)
									ImGui::SetItemDefaultFocus();   // You may set the initial focus when opening the combo (scrolling + for keyboard navigation support)
							}
							ImGui::EndCombo();
						}


						if (bIdx != FAILPICKING && objectinform.objectIdx != FAILPICKING) {

							auto bObject = (*selectedobject->GetBoundingObject())[bIdx];

							XMFLOAT4X4 transform = bObject->GetTransform();
							XMFLOAT3 allScale = bObject->GetAllScale();

						/*	tempTransform._11 = allScale.x;
							tempTransform._22 = allScale.y;
							tempTransform._33 = allScale.z;*/

						


							bObject->SetTransform(tempTransform);

							ImGui::Text("Input Rotate");
							XMFLOAT3 angle = bObject->GetInputAngle();

							ImGui::InputFloat("rotateX", &angle.x, 0.1f, 0.1f, "%.3f");
							bObject->Rotate(bObject->GetInputAngle().x, 0.0f, 0.0f);

							ImGui::InputFloat("rotateY", &angle.y, 0.1f, 0.1f, "%.3f");
							bObject->Rotate(0.0f, bObject->GetInputAngle().y, 0.0f);

							ImGui::InputFloat("rotateZ", &angle.z, 0.1f, 0.1f, "%.3f");
							bObject->Rotate(0.0f, 0.0f, bObject->GetInputAngle().z);

							bObject->GetInputAngle() = angle;

							ImGui::Text("Input Scale");

							ImGui::InputFloat("scaleX", &allScale.x, 0.1f, 0.1f, "%.3f");

							ImGui::InputFloat("scaleY", &allScale.y, 0.1f, 0.1f, "%.3f");

							ImGui::InputFloat("scaleZ", &allScale.z, 0.1f, 0.1f, "%.3f");
							bObject->SetScale(allScale.x, allScale.y, allScale.z);
							bObject->SetAllScale(allScale.x, allScale.y, allScale.z);

							//////////////////////////////////////////////////////////////

							

							ImGui::Text("Input TranForm");
							XMFLOAT3 pos = bObject->GettPos();
							ImGui::InputFloat("posX", &pos.x, 0.1f, 0.1f, "%.3f");

							ImGui::InputFloat("posY", &pos.y, 0.1f, 0.1f, "%.3f");

							ImGui::InputFloat("posZ", &pos.z, 0.1f, 0.1f, "%.3f");

							bObject->SetPosition(pos);
							bObject->SettPos(pos);


						}
					}
				}

				if (ImGui::Button("Insert New BoundingBox")) {
					std::vector<GameObject*>* bvec = selectedobject->GetBoundingObject();
					GameObject* nb = new GameObject;
					nb->SetPosition(0.0f, 0.0f, 0.0f);
					nb->SetScale(1.0f, 1.0f, 1.0f);
					bvec->emplace_back(nb);

					std::vector<BoundingOrientedBox>& oribvec = selectedobject->GetobbVector();
					oribvec.resize(bvec->size());


					*cbFlag = true;
				}

				if (ImGui::Button("Delete this BoundingBox")) {
					
					bBox_item = NULL;
					*dbFlag = true;


				}


			}


			

		}
	}

	ImGui::End();
}

void BoundingGUI::useShortCut(char c)
{
	if (c == 'q') {
		Radioflags = 0;
	}
	else if (c == 'w') {
		Radioflags = 1;
	}
}

ObjectInform BoundingGUI::GetLocatedObjectType()
{
	return objectinform;
}
