#pragma once
#include "Vertex.h"
class SubIndex {
	int index[3];
public:
	SubIndex(int index0, int index1, int index2) {
		index[0] = index0;
		index[1] = index1;
		index[2] = index2;
	}
	int* GetIndex() {
		return index;
	}
	bool CheckIndex(int indexNum) {
		for (int i = 0; i < 3; ++i) {
			if (index[i] == indexNum)
				return false;
			return true;
		}
	}
};




class HeightMapLayer
{
private:
	bool isBuild = false;
	int indicesNum = 0;
	int width;
	int length;
	XMFLOAT3 scale;


	Vertex* vertex;
	ID3D12Resource* vertexBuffer = NULL;
	ID3D12Resource* vertexUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW vertexBufferView;
	D3D12_PRIMITIVE_TOPOLOGY primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	UINT slot = 0;
	UINT verticesNum = 0;
	UINT stride = 0;
	UINT offset = 0;
	UINT primitiveNum;
	DiffusedVertex* vertices{ NULL };
	TexturedVertex* Texturedvertices{ NULL };

	UINT * indices{ NULL };
	//피킹 시 필요한 offset 
	UINT pickOffset;
	ID3D12Resource* indexBuffer = NULL;
	ID3D12Resource* indexUploadBuffer = NULL;
	D3D12_INDEX_BUFFER_VIEW indexBufferView;
	// VertexBuffer has Position, Color ...

	XMFLOAT3* positions;
	ID3D12Resource* positionBuffer = NULL;
	ID3D12Resource* positionUploadBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW positionBufferView;

	int	subMeshesNum = 0;
	int* subSetIndicesNum = NULL;
	UINT** subSetIndices = NULL;

	ID3D12Resource** subSetIndexBuffers = NULL;
	ID3D12Resource** subSetIndexUploadBuffers = NULL;
	D3D12_INDEX_BUFFER_VIEW* subSetIndexBufferViews = NULL;

	std::vector<Diffused2TexturedVertex> diffused2TexturedVerices{ NULL };
	Diffused2TexturedVertex* mappedDiffued2TextureVerices{ NULL };


public:
	HeightMapLayer();
	~HeightMapLayer();
	//HeightMapLayer(ID3D12Resource* vertexBufferView);
	void BuildLayer(ID3D12Device * device, ID3D12GraphicsCommandList * commandList,int width , int length , Diffused2TexturedVertex* diffusevertex , std::vector<SubIndex> subIndex ,int vertexNum ,int indexNum);
	void ChangeLayerHeight(Diffused2TexturedVertex* diffusevertex , int verticeNum );
	void ChangeLayerTexNum(Diffused2TexturedVertex* diffusevertex , int verticeNum);

	void ChangeLayer(int width , int length , Diffused2TexturedVertex* diffusevertex, std::vector<SubIndex> subIndex, int vertexNum, int indexNum);

	void Render(ID3D12GraphicsCommandList * commandList);
	bool GetIsBuild() { return isBuild; }
	void SetIsBuild(bool isBuild) { this->isBuild = isBuild; }
	XMFLOAT4 OnGetColor(int x, int z, void *context);
	float OnGetHeight(int x, int z, void *context);

	void ReleaseBuffers();

};


class HeightMapImage
{
private:
	// 높이 맵 이미지 픽셀(8-비트)들의 이차원 배열이다. 각 픽셀은 0~255 값을 갖는다.
	BYTE*						heightMapPixels;

	// 높이 맵 이미지의 가로와 세로 크기이다.
	int							width;
	int							length;

	// 높이 맵 이미지를 실제로 몇 배 확대하여 사용할 것인가를 나타내는 스케일 벡터이다.
	XMFLOAT3					scale;

public:
	HeightMapImage(LPCTSTR fileName, int width, int length, XMFLOAT3 scale);
	~HeightMapImage();

	// 높이 맵 이미지에서 (x, z) 위치의 픽셀 값에 기반한 지형의 높이를 반환한다.
	float GetHeight(float x, float z);

	// 높이 맵 이미지에서 (x, z) 위치의 법선 벡터를 반환한다.
	XMFLOAT3 GetHeightMapNormal(int x, int z);

	XMFLOAT3 GetScale() { return scale; }
	BYTE* GetHeightMapPixels() { return (heightMapPixels); }
	int GetHeightMapWidth() { return(width); }
	int GetHeightMapLength() { return (length); }

};