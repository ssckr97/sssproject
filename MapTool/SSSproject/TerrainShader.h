#pragma once
#include "Shader.h"

class TerrainShader : public Shader
{
private:
	int pipeLineCounter = 0;
	int terrainLayer = 0;
	XMFLOAT3 terrainScale;

protected:
	ID3D12Resource *cbGameObjects = NULL;
	CB_GAMEOBJECT_INFO *cbMappedGameObjects = NULL;
	UINT cbGameObjectBytes = 0;
public:
	TerrainShader(XMFLOAT3 terrainScale);
	virtual ~TerrainShader();
	virtual void BuildTerrain(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context = NULL);
	virtual void SetPipeLineCounter(int idx) { pipeLineCounter = idx; }
	virtual void SetTerrainLayer(int idx) { terrainLayer = idx; }
	virtual GameObject* GetObjects(int idx) { return objects[idx]; }
	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseShaderVariables();
	virtual void ReleaseObjects();
	virtual void ReleaseUploadBuffers();


	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	D3D12_INPUT_LAYOUT_DESC CreateNaviInputLayout();

	D3D12_RASTERIZER_DESC CreateAlphaRasterizerState();
	D3D12_RASTERIZER_DESC CreateNaviRasterizerState();



	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();

	D3D12_SHADER_BYTECODE CreateDepthVertexShader();
	D3D12_SHADER_BYTECODE CreateDepthPixelShader();

	D3D12_SHADER_BYTECODE CreateAlphaVertexShader();
	D3D12_SHADER_BYTECODE CreateAlphaPixelShader();

	D3D12_SHADER_BYTECODE CreateHeightVertexShader();
	D3D12_SHADER_BYTECODE CreateHeightPixelShader();

	D3D12_SHADER_BYTECODE CreateNavitVertexShader();
	D3D12_SHADER_BYTECODE CreateNaviPixelShader();

	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature *grapicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);

	virtual void Render(ID3D12GraphicsCommandList* commandList, Camera* camera);
	virtual void NaviRender(ID3D12GraphicsCommandList* commandList, Camera* camera);

	void CreatePipeLineStateIndex(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, int index);
	void CreatePipeLineStateAlpha(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, int index);
	void CreatePipeLineStateDetph(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature);
	void CreatePipeLineStateHeight(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature);
	void CreatePipeLineStateNavi(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature);


	D3D12_BLEND_DESC CreateAlphaBlendState();
	D3D12_DEPTH_STENCIL_DESC CreateAlphaDepthStencilState();
	HeightMapTerrain* GetTerrain() { return terrain; }


protected:

	HeightMapTerrain* terrain = NULL;
};


