#pragma once
#define DIR_FORWARD 0x01
#define DIR_BACKWARD 0x02
#define DIR_LEFT 0x04
#define DIR_RIGHT 0x08
#define DIR_UP 0x10
#define DIR_DOWN 0x20
#include "GameObject.h"
#include "Camera.h"

class Player : public GameObject
{
protected:
	XMFLOAT3 position;
	XMFLOAT3 right;
	XMFLOAT3 up;
	XMFLOAT3 look;
	//플레이어의 위치 벡터, x-축(Right), y-축(Up), z-축(Look) 벡터이다.

	float pitch;
	float yaw;
	float roll;
	//플레이어가 로컬 x-축(Right), y-축(Up), z-축(Look)으로 얼마만큼 회전했는가를 나타낸다.

	XMFLOAT3 velocity;
	//플레이어의 이동 속도를 나타내는 벡터이다.

	XMFLOAT3 gravity;
	//플레이어에 작용하는 중력을 나타내는 벡터이다.

	float maxVelocityXZ;
	//xz-평면에서 (한 프레임 동안) 플레이어의 이동 속력의 최대값을 나타낸다.

	float maxVelocityY;
	//y-축 방향으로 (한 프레임 동안) 플레이어의 이동 속력의 최대값을 나타낸다.

	float friction;
	//플레이어에 작용하는 마찰력을 나타낸다.

	LPVOID playerUpdatedContext;
	//플레이어의 위치가 바뀔 때마다 호출되는 OnPlayerUpdateCallback() 함수에서 사용하는 데이터이다.

	LPVOID cameraUpdatedContext;
	//카메라의 위치가 바뀔 때마다 호출되는 OnCameraUpdateCallback() 함수에서 사용하는 데이터이다.

	Camera *camera = NULL;
	//플레이어에 현재 설정된 카메라이다.

public:
	Player(int meshesNum = 1);
	virtual ~Player();

	XMFLOAT3 GetPosition() { return(position); }
	XMFLOAT3 GetLookVector() { return(look); }
	XMFLOAT3 GetUpVector() { return(up); }
	XMFLOAT3 GetRightVector() { return(right); }

	void SetFriction(float friction) { this->friction = friction; }
	void SetGravity(const XMFLOAT3& gravity) { this->gravity = gravity; }
	void SetMaxVelocityXZ(float maxVelocity) { this->maxVelocityXZ = maxVelocity; }
	void SetMaxVelocityY(float maxVelocity) { this->maxVelocityY = maxVelocity; }
	void SetVelocity(const XMFLOAT3& velocity) { this->velocity = velocity; }
	void SetPosition(const XMFLOAT3& position)
	{
		Move(XMFLOAT3(position.x - this->position.x, position.y - this->position.y, position.z - this->position.z), false);
	}
	/*플레이어의 위치를 xmf3Position 위치로 설정한다. xmf3Position 벡터에서 현재 플레이어의 위치 벡터를 빼면 현
	재 플레이어의 위치에서 xmf3Position 방향으로의 벡터가 된다. 현재 플레이어의 위치에서 이 벡터 만큼 이동한다.*/

	XMFLOAT3& GetVelocity() { return(velocity); }
	float GetYaw() { return(yaw); }
	float GetPitch() { return(pitch); }
	float GetRoll() { return(roll); }
	Camera *GetCamera() { return(camera); }
	void SetCamera(Camera* camera) { this->camera = camera; }

	void Move(ULONG direction, float distance, bool velocity = false);
	void Move(const XMFLOAT3& shift, bool velocity = false);
	void Move(float offsetX = 0.0f, float offsetY = 0.0f, float offsetZ = 0.0f);
	//플레이어를 이동하는 함수이다.

	void Rotate(float x, float y, float z);
	//플레이어를 회전하는 함수이다.

	void Update(float timeElapsed);
	//플레이어의 위치와 회전 정보를 경과 시간에 따라 갱신하는 함수이다.

	virtual void OnPlayerUpdateCallback(float timeElapsed) { }
	void SetPlayerUpdatedContext(LPVOID context) { playerUpdatedContext = context; }
	//플레이어의 위치가 바뀔 때마다 호출되는 함수와 그 함수에서 사용하는 정보를 설정하는 함수이다.

	virtual void OnCameraUpdateCallback(float timeElapsed) { }
	void SetCameraUpdatedContext(LPVOID context) { cameraUpdatedContext = context; }
	//카메라의 위치가 바뀔 때마다 호출되는 함수와 그 함수에서 사용하는 정보를 설정하는 함수이다.

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseShaderVariables();
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);

	Camera *OnChangeCamera(DWORD newCameraMode, DWORD currentCameraMode);
	virtual Camera *ChangeCamera(DWORD newCameraMode, float timeElapsed) { return(NULL); }
	//카메라를 변경하기 위하여 호출하는 함수이다.

	virtual void OnPrepareRender();
	//플레이어의 위치와 회전축으로부터 월드 변환 행렬을 생성하는 함수이다.

	virtual void Render(ID3D12GraphicsCommandList* commandList, Camera* camera = NULL);
	//플레이어의 카메라가 3인칭 카메라일 때 플레이어(메쉬)를 렌더링한다.

};

class AirplanePlayer : public Player
{
public:
	AirplanePlayer(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature, int meshesNum = 1);
	virtual ~AirplanePlayer();

	virtual Camera *ChangeCamera(DWORD newCameraMode, float timeElapsed);
	virtual void OnPrepareRender();
};

class TerrainPlayer : public Player
{
public:
	TerrainPlayer(ID3D12Device *device, ID3D12GraphicsCommandList *commandList,
		ID3D12RootSignature *graphicsRootSignature, void *context, int meshesNum = 1);
	virtual ~TerrainPlayer();
	virtual Camera *ChangeCamera(DWORD newCameraMode, float timeElapsed);
	virtual void OnPlayerUpdateCallback(float timeElapsed);
	virtual void OnCameraUpdateCallback(float timeElapsed);
};
