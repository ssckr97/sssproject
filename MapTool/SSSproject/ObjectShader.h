#pragma once
#include "Shader.h"

struct SkinnedObjectBuffer
{
	GameObject* object;

	int skinnedMeshNum = 0;
	int standardMeshNum = 0;

	SkinnedMesh** skinnedMeshes = NULL;

	ID3D12Resource** skinnedBuffer = NULL;
	XMFLOAT4X4** mappedSkinnedBuffer = NULL;	//[][][] skinNum, bone

	ID3D12Resource** vbGameObject = NULL;
	VS_VB_INSTANCE_OBJECT** vbMappedGameObject = 0;
	D3D12_VERTEX_BUFFER_VIEW* vbGameObjectView = NULL;
	// 무기와 같은 StandardMesh를 위한 버퍼

	TEXTURENAME textureName{};
	int diffuseIdx = 0;
	int normalIdx = 0;
	int specularIdx = 0;
	int metallicIdx = 0;
	int emissionIdx = 0;

};

class PlayerShader : public Shader
{
public:
	PlayerShader();
	virtual ~PlayerShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();

	virtual void CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList* commandList);
	virtual void UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, XMFLOAT4X4* world);
	virtual void ReleaseShaderVariables();

	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
	virtual void Render(ID3D12GraphicsCommandList* commandList, Camera* camera);

protected:
	ID3D12Resource					*cbPlayer = NULL;
	CB_PLAYER_INFO					*cbMappedPlayer = NULL;
};

class AssistShader : public  StandardShader {

public:

	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context);
	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature);
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();

};

class ScreenSpaceDecalShader : public StandardShader {
private:
	D3D12_CPU_DESCRIPTOR_HANDLE		depthSrvCPUDescriptorStartHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE		depthSrvGPUDescriptorStartHandle;

	ID3D12DescriptorHeap*			dsvDescriptorHeap = NULL;
	ID3D12Resource*					depthStencilBuffer = NULL;
	D3D12_CPU_DESCRIPTOR_HANDLE		dsvCPUDescriptorHandle;


public:
	ScreenSpaceDecalShader(ID3D12Resource * depthStencilBuffer);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context);
	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature);
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();
	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_RASTERIZER_DESC CreateRasterizerState();
};
class BoundingBoxShader : public StandardShader {

public:

	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context);
	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature);
	virtual D3D12_RASTERIZER_DESC CreateRasterizerState();

};

/*class ObjectShader : public StandardShader
{
protected:
	std::vector<ObjectBuffer*> objectBuffer;

	int					mainRotorFrame = -1;
	int					tailRotorFrame = -1;

public:
	ObjectShader();
	virtual ~ObjectShader();

	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*> context);
	virtual void ReleaseObjects();
	virtual void AnimateObjects(float timeElapsed, Camera* camera);

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ObjectBuffer* objectBuffer);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseShaderVariables();

	virtual void ReleaseUploadBuffers();

	virtual void Render(ID3D12GraphicsCommandList *commandList, Camera *camera);
};*/

class SkyBoxShader : public StandardShader {
private:

public:
	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context);
	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_RASTERIZER_DESC CreateRasterizerState();
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();
	virtual D3D12_BLEND_DESC CreateBlendState();

};

class SkinnedAnimationObjectsShader : public SkinnedAnimationStandardShader
{
public:
	SkinnedAnimationObjectsShader();
	virtual ~SkinnedAnimationObjectsShader();

	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature);
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context , int fobjectSize);
	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void ReleaseObjects();

	virtual void ReleaseUploadBuffers();

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx);
	virtual void UpdateShaderVariables(int idx);

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx, bool isShadow);
	virtual void Render(ID3D12GraphicsCommandList *commandList, Camera *camera, bool isShadow);

	virtual void SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);

protected:
	std::vector<SkinnedObjectBuffer*> objectBuffer;

};