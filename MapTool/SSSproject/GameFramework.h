#pragma once
#include "ProcessInput.h"
#include "Renderer.h"


class GameFramework
{
private:
	HINSTANCE hinstance;
	HWND hwnd;
	int windowClientWidth;
	int windowClientHeight;
	

	IDXGIFactory4* factory;
	//DXGI 팩토리 인터페이스에 대한 포인터이다.

	IDXGISwapChain3* swapChain;
	//스왑 체인 인터페이스에 대한 포인터이다. 주로 디스플레이를 제어하기 위하여 필요하다.

	ID3D12Device* device;
	//Direct3D 디바이스 인터페이스에 대한 포인터이다. 주로 리소스를 생성하기 위하여 필요하다.

	bool msaa4xEnable = false;
	UINT msaa4xQualityLevels = 0;
	//MSAA 다중 샘플링을 활성화하고 다중 샘플링 레벨을 설정한다.

	static const UINT renderTargetBuffersNum = 3;
	//스왑 체인의 후면 버퍼의 개수이다.

	UINT swapChainBufferIndex;
	//현재 스왑 체인의 후면 버퍼 인덱스이다.

	ID3D12Resource* renderTargetBuffers[renderTargetBuffersNum];
	ID3D12DescriptorHeap* rtvDescriptorHeap;
	UINT rtvDescriptorIncrementSize;
	//렌더 타겟 버퍼, 서술자 힙 인터페이스 포인터, 렌더 타겟 서술자 원소의 크기이다.

	ID3D12Resource* depthStencilBuffer;
	ID3D12Resource* terrainDepthStencilBuffer;

	ID3D12DescriptorHeap* dsvDescriptorHeap;
	UINT dsvDescriptorIncrementSize;
	//깊이-스텐실 버퍼, 서술자 힙 인터페이스 포인터, 깊이-스텐실 서술자 원소의 크기이다.

	ID3D12CommandQueue* commandQueue;
	ID3D12CommandAllocator* commandAllocator;
	ID3D12GraphicsCommandList* commandList;
	//명령 큐, 명령 할당자, 명령 리스트 인터페이스 포인터이다.

	ID3D12PipelineState* pipelineState;
	//그래픽스 파이프라인 상태 객체에 대한 인터페이스 포인터이다.

	ID3D12Fence* fence;
	UINT64 fenceValues[renderTargetBuffersNum];
	HANDLE fenceEvent;
	//펜스 인터페이스 포인터, 펜스의 값, 이벤트 핸들이다.

	ID3D12DescriptorHeap*  SrvDescHeap = NULL;
	//Srv를 저장할 디스크립터 힙이다. 

#if defined(_DEBUG)
	ID3D12Debug *debugController;
#endif
	ID3D12DescriptorHeap			*srvdepthbufferHeap = NULL;
	D3D12_CPU_DESCRIPTOR_HANDLE		cbvCPUDescriptorStartHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE		cbvGPUDescriptorStartHandle;
	D3D12_CPU_DESCRIPTOR_HANDLE		srvCPUDescriptorStartHandle;
	D3D12_GPU_DESCRIPTOR_HANDLE		srvGPUDescriptorStartHandle;
	
	D3D12_CPU_DESCRIPTOR_HANDLE dsvCPUDescriptorHandle;
	D3D12_CPU_DESCRIPTOR_HANDLE terraindsvCPUDescriptorHandle;

	D3D12_CPU_DESCRIPTOR_HANDLE depthRtvCPUDescriptorHandle;
	D3D12_CPU_DESCRIPTOR_HANDLE alphaRtvCPUDescriptorHandle0;
	D3D12_CPU_DESCRIPTOR_HANDLE alphaRtvCPUDescriptorHandle1;
	D3D12_CPU_DESCRIPTOR_HANDLE normalRtvCPUDescriptorHandle;
	D3D12_CPU_DESCRIPTOR_HANDLE alphaRtvCPUDescriptorHandle2;

	D3D12_CPU_DESCRIPTOR_HANDLE heightRtvCPUDescriptorHandle;
	


	ID3D12Resource* depthRenderResource = NULL;
	ID3D12Resource* terrainAlphaResource0 = NULL;
	ID3D12Resource* terrainAlphaResource1 = NULL;
	ID3D12Resource* terrainAlphaResource2 = NULL;
	ID3D12Resource* terrainHeightResource = NULL;
	ID3D12Resource* terrainNormalResource = NULL;

	GameTimer gameTimer;
	// 게임 프레임 워크에서 사용할 타이머이다.

	_TCHAR frameRate[90];
	// 프레임레이트를 주 윈도우의 캡션에 출력하기 위한 문자열이다.

	Scene* scene;
	int guiNum = 4;
	bool bFlag = true;
	bool cbFlag = false;
	bool dbFlag = false;

	int fobIdx = -1;

	GUI **gui;
	MangerGUI *mangergui;
	OptionGUI *optiongui;
	ObjectGUI *objectgui;
	TerrainGUI *terraingui;
	BoundingGUI *boundinggui;


	Picking *picking;
	ProcessInput *processinput;
	Renderer * renderer;

	XMFLOAT3 terrainScale;
public:
	Camera* camera = NULL;

	//플레이어 객체에 대한 포인터이다.
	Player* player = NULL;
	//마지막으로 마우스 버튼을 클릭할 때의 마우스 커서의 위치이다.
	

	GameFramework();
	~GameFramework();

	bool OnCreate(HINSTANCE hinstance, HWND hwnd);
	//프레임워크를 초기화하는 함수이다(주 윈도우가 생성되면 호출된다).

	void OnDestroy();
	void CreateSwapChain();
	void CreateDirect3DDevice();
	void CreateRtvAndDsvDescriptorHeaps();
	void CreateCommandQueueAndList();
	//스왑 체인, 디바이스, 서술자 힙, 명령 큐/할당자/리스트를 생성하는 함수이다.

	void CreateRenderTargetViews();
	void CreateDepthStencilView();
	// 랜더타겟 뷰와 뎁스스텐실 뷰를 생성한다.
	void CreateDetphRtvSrvView();

	void ChangeSwapChainState();
	// 창모드, 전체화면 모드를 전환하는 함수이다.

	void BuildObjects();
	void ReleaseObjects();
	//렌더링할 메쉬와 게임 객체를 생성하고 소멸하는 함수이다.

	void BuildGUI();
	//렌더링할 GUI들을 세팅하는 부분이다. 
	void BuildPicking();
	//피킹 기능에 대한 세팅을 한는 부분이다.

	void ProcessUserInput();
	void AnimateObjects();
	void FrameAdvance();
	//프레임워크의 핵심(사용자 입력, 애니메이션, 렌더링)을 구성하는 함수이다.

	void MoveToNextFrame();
	// 다음 프레임으로 이동한다.

	void WaitForGpuComplete();
	//CPU와 GPU를 동기화하는 함수이다.

	void OnProcessingMouseMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam);
	void OnProcessingKeyboardMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam);
	LRESULT CALLBACK OnProcessingWindowMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam);
	//윈도우의 메시지(키보드, 마우스 입력)를 처리하는 함수이다.

	void GarbageCollection();

	void UpdateShaderVariables();
	void ReleaseShaderVariables();
};
