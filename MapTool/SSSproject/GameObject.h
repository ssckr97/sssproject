#pragma once

#include "Mesh.h"
#include "Camera.h"

class Shader;
class StandardShader;
class Texture;
class GameObject;
class AnimationController;
class LoadedModelInfo;
class Material;

struct MATERIAL;
struct VS_VB_INSTANCE_OBJECT;
struct VS_VB_INSTACE_BOUNDING;
struct VS_VB_INSTANCE_MATERIAL;
struct VS_VB_INSTANCE_BILLBOARD;
struct VS_VB_INSTANCE_PARTICLE;
struct CB_GAMEOBJECT_INFO;




struct TEXTURENAME
{
	std::vector<std::pair<int, _TCHAR*>> diffuse;
	std::vector<std::pair<int, _TCHAR*>> normal;
	std::vector<std::pair<int, _TCHAR*>> specular;
	std::vector<std::pair<int, _TCHAR*>> metallic;
	std::vector<std::pair<int, _TCHAR*>> emission;

	int GetSize() {
		return (int)diffuse.size() + (int)normal.size() + (int)specular.size() + (int)metallic.size() + (int)emission.size();
	}
};

class GameObject
{
private:
	int references = 0;

public:
	GameObject(int meshesNum = 1);
	virtual ~GameObject();
	void AddRef();
	void Release();

	int animateSpeed = 0;

	XMFLOAT4X4 toParent;

protected:

	char frameName[128] = { '\0' };


	XMFLOAT3 originalScale{ 1.0f,1.0f,1.0f };
	XMFLOAT3 originalExtent{ 1.0f,1.0f,1.0f };

	Mesh **meshes = NULL;
	NaviMesh *naviMesh = NULL;



	BoundingBoxMesh *boundingMesh = NULL;
	std::vector<GameObject*>* boundingObject = NULL;
	std::vector<BoundingOrientedBox> obbvec;

	//정점의 노멀을 그려줄 라인 

	int meshesNum = 0;


	Material** materials = NULL;
	int materialsNum = 1;
	XMFLOAT4X4 originalTransform;
	XMFLOAT4X4 transform;
	XMFLOAT4X4 world;

	UINT pipeLineStatesNum = 1;

	int framesNum = 0;
	int maximumMaterialsNum = 0;

	GameObject 					*parent = NULL;
	GameObject 					*child = NULL;
	GameObject 					*sibling = NULL;

	D3D12_GPU_DESCRIPTOR_HANDLE*	cbvGPUDescriptorHandle = NULL;
	int handlesNum = 0;

	ID3D12Resource					*cbGameObject = NULL;
	CB_GAMEOBJECT_INFO				*cbMappedGameObject = NULL;

	int								type = -1;
	// Object Type
	UINT							reflection = 0;
	// Material Number
	UINT							textureMask = 0x00;
	// Texture Number
	UINT							textureIdx = -1;

	UINT							shaderType = 0;

	XMFLOAT3 inputangle{ 0,0,0 };
	XMFLOAT3 rotatingAngle = { 0 , 0  , 0 };
	XMFLOAT3 allScale = { 1.0f  , 1.0f , 1.0f };
	XMFLOAT3 tPos{ 0.0f , 0.0f , 0.0f };


	float fscale{ 1.0f };
	float oldScale{ 1.0f };
	float inputScale{ 1.0f };

	BoundingBox MeshaabbBox;
	BoundingBox aabbBox;
	BoundingOrientedBox obbBox;
	bool boxtype; // flase 이면 OBB  true이면 AABB
public:
	TEXTURENAME						textureName;
	AnimationController 			*skinnedAnimationController = NULL;

	void ReleaseUploadBuffers();

	virtual void SetMesh(int idx, Mesh *mesh);
	virtual void SetBoundingMesh(BoundingBoxMesh* mesh);
	virtual void SetNaviMesh(NaviMesh* mesh);
	virtual BoundingBoxMesh * GetBounding() { return boundingMesh; }
	virtual NaviMesh * GetNaviMesh() { return naviMesh; }
	virtual Mesh* GetMesh(int idx) { return meshes[idx]; };
	virtual Mesh** GetMeshes() { return meshes; }
	

	virtual void SetShader(Shader *shader);
	virtual void SetMaterial(int idx, Material *material);
	virtual void SetMaterial(int idx, UINT reflection);
	virtual void SetReflection(UINT reflection);
	virtual void SetTextureMask(UINT texturemask);
	virtual void SetTextureIdx(UINT idx);
	virtual void SetChild(GameObject* child);
	virtual void SetChild(GameObject* child, bool referenceUpdate);
	virtual void SetPipelineStatesNum(UINT type);

	virtual void AddFramesNum() { framesNum++; }

	virtual void GetHierarchyMaterial(std::vector<MATERIAL>& materialReflection, int& idx, TEXTURENAME textureName);
	virtual UINT GetReflection();
	virtual UINT GetTextureMask();
	virtual int GetFramesNum();
	virtual GameObject* GetChild() { return child; }
	virtual int GetMeshNum() { return meshesNum; }
	virtual int GetMaterialsNum() { return materialsNum; }
	virtual int GetMaximumMaterialsNum() { return maximumMaterialsNum; }
	virtual int GetPipeLineStatesNum() { return pipeLineStatesNum; }
	virtual BoundingBoxMesh* GetBoundingMesh() { return boundingMesh; }
	virtual void CreateMaterials(UINT materialsNum);

	void CreateCbvGPUDescriptorHandles(int handlesNum = 1);
	void SetCbvGPUDescriptorHandle(D3D12_GPU_DESCRIPTOR_HANDLE cbvGPUDescriptorHandle, int idx = 0) { this->cbvGPUDescriptorHandle[idx] = cbvGPUDescriptorHandle; }
	void SetCbvGPUDescriptorHandlePtr(UINT64 cbvGPUDescriptorHandlePtr, int idx = 0) { cbvGPUDescriptorHandle[idx].ptr = cbvGPUDescriptorHandlePtr; }
	void SetCbvGPUDescriptorHandlePtrHierarchy(UINT64 cbvGPUDescriptorHandlePtr, int idx, int objectsNum);
	void SetMaterialCbvGPUDescriptorHandlePtrHierarchy(UINT64 cbvGPUDescriptorHandlePtr, int maximum);
	void SetCbvGPUDescriptorHandlesOnlyStandardMesh(D3D12_GPU_DESCRIPTOR_HANDLE handle, int objectsNum = 1, int framesNum = 1);


	D3D12_GPU_DESCRIPTOR_HANDLE GetCbvGPUDescriptorHandle(int idx = 0) { return(cbvGPUDescriptorHandle[idx]); }

	virtual void PrepareAnimate() { }
	virtual void Animate(float timeElapsed, XMFLOAT4X4* parent = NULL);
	virtual void SetOBBBoundingBox();
	virtual void SetOBBRotate(XMFLOAT4X4 world , int idx);
	virtual std::vector<BoundingOrientedBox>& GetobbVector();


	virtual void OnPrepareRender();
	virtual void Render(ID3D12GraphicsCommandList *commandList, Camera* camera, int idx = 0);
	virtual void Render(ID3D12GraphicsCommandList *commandList, Camera* camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView, bool isAnimationModel = false);
	virtual void Render(ID3D12GraphicsCommandList* commandList, Camera* camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingObjIdxBufferView, D3D12_VERTEX_BUFFER_VIEW* instancingStandardBufferView);
	virtual void BoundingRender(ID3D12GraphicsCommandList *commandList, Camera* camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingGameObjectBufferView);
	virtual void SkinnedBoundingRender(ID3D12GraphicsCommandList * commandList, Camera * camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW* instancingGameObjectBufferView);

	virtual void TerrainRender(ID3D12GraphicsCommandList * commandList, Camera * camera, int LayerIndex, int idx = 0);
	virtual void NaviRender(ID3D12GraphicsCommandList * commandList, Camera * camera);

	virtual void BillboardRender(ID3D12GraphicsCommandList* commandList, Camera* camera, UINT instancesNum, D3D12_VERTEX_BUFFER_VIEW instancingBufferView);

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);

	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList, int idx);
	virtual void ReleaseShaderVariables();

	virtual void BuildMaterials(ID3D12Device *device, ID3D12GraphicsCommandList *commandList) { }
	float&    GetScale();
	float	 GetTransFormScale();
	XMFLOAT3 GetAllScale();
	void SetAllScale(float x, float y, float z);



	float& GetInputScale() { return inputScale; }
	float& GetOldScale() { return oldScale; }
	void Scale(float scale);

	XMFLOAT3& GetInputAngle();
	XMFLOAT3 GetRotatingAngle();
	XMFLOAT3 GetPosition();
	XMFLOAT3 GettPos();
	XMFLOAT3 GetLook();
	XMFLOAT3 GetUp();
	XMFLOAT3 GetRight();
	XMFLOAT4X4 GetWorld();
	XMFLOAT4X4 GetInverseWorld();
	XMFLOAT4X4 GetTransform();
	void SetOriginalTransform();

	Material* GetMaterial(int idx);
	UINT GetTextureIdx() { return textureIdx; }

	int	 GetObjectType() { return type; }
	void SetObjectType(int type) { this->type = type; }

	void SetPosition(float x, float y, float z);
	void SetPosition(XMFLOAT3 position);
	void SetLook(XMFLOAT3 look);
	void SetUp(XMFLOAT3 up);
	void SetRight(XMFLOAT3 right);
	void SettPos(XMFLOAT3 tPos);


	void SetScale(float x, float y, float z);
	void SetScaleTransform(float x, float y, float z);
	void SetWorld(XMFLOAT4X4 world);
	void SetTransform(XMFLOAT4X4 transform);

	void MoveStrafe(float distance = 1.0f);
	void MoveUp(float distance = 1.0f);
	void MoveForward(float distance = 1.0f);

	void Rotate(XMFLOAT3* axis, float angle);
	void Rotate(float pitch = 10.0f, float yaw = 10.0f, float roll = 10.0f);
	void Rotate(XMFLOAT4 *quaternion);

	GameObject *GetParent() { return(parent); }
	void UpdateTransform(XMFLOAT4X4 *parent);
	void UpdateTransform(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, XMFLOAT4X4 *parent = NULL);
	void UpdateTransform(VS_VB_INSTANCE_OBJECT** vbMappedGameObjects, int idx, XMFLOAT4X4 * parent = NULL);
	void UpdateBoundingTransForm(VS_VB_INSTACE_BOUNDING * vbMappedBoudnigs, int idx, XMFLOAT4X4 parent , int bidx);
	void UpdateSkinnedBoundingTransform(VS_VB_INSTACE_BOUNDING** vbMappedBoundings, std::vector<BoundingOrientedBox>& obbs, int idx, int framesNum, XMFLOAT4X4* parent);

	void UpdateTransformOnlyStandardMesh(CB_GAMEOBJECT_INFO** cbMappedGameObjects, UINT cbGameObjectBytes, int idx, int framesNum, XMFLOAT4X4 *parent);
	void UpdateTransformOnlyStandardMesh(VS_VB_INSTANCE_OBJECT** vbMappedGameObjects, int idx, int framesNum, XMFLOAT4X4 *parent);

	virtual void UpdateTransform(VS_VB_INSTANCE_BILLBOARD* vbMappedBillboardObject, int idx) {}
	virtual void UpdateTransform(VS_VB_INSTANCE_PARTICLE* vbMappedBillboardObject, int idx) {}

	GameObject *FindFrame(char *pstrFrameName);

	_TCHAR* FindReplicatedTexture(_TCHAR* textureName);

	UINT GetMeshType(int idx = 0) { return((meshes[idx]) ? meshes[idx]->GetType() : 0x00); }
	UINT GetShaderType() { return shaderType; }

	void FindAndSetSkinnedMesh(SkinnedMesh **SkinnedMeshes, int *SkinnedMesh);

	void LoadMaterialsFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, GameObject *parent, FILE *inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5]);

	static LoadedModelInfo *LoadGeometryAndAnimationFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature, char *fileName, char* folderName);
	static GameObject *LoadFrameHierarchyFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature, GameObject *parent, FILE *inFile, char* folderName, TEXTURENAME& textureMapNames, int textureIdx[5], int& frameCounter, int& materialCounter, int* skinnedMeshesNum, int* standardMeshCounter, int* boundingMeshNum);
	static void LoadAnimationFromFile(FILE *inFile, LoadedModelInfo *loadedModel);
	static GameObject *LoadGeometryFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature, char *fileName, char* folderName);
	static void PrintFrameInfo(GameObject *gameObject, GameObject *parent);

	virtual bool IsFar(Camera* camera) { return false; };

	void SetAABB(XMFLOAT3 Center, XMFLOAT3 Extents) {
		aabbBox.Center = Center;
		aabbBox.Extents = Extents;
	}
	void SetOBB(XMFLOAT3 Center, XMFLOAT3 Extents, XMFLOAT4 orientation) {
		obbBox.Center = Center;
		obbBox.Extents = Extents;
		obbBox.Orientation = orientation;
	}
	void SetOBBOriginalExtent(XMFLOAT3 Extents) {
		originalExtent = Extents;
	}
	void SetOBBPos(XMFLOAT3 Center) { obbBox.Center = Center; }
	void SetAABBPos(XMFLOAT3 Center) { aabbBox.Center = Center; }

	void SetBoxType(bool type) {
		boxtype = type;
	}
	XMFLOAT3 GetOriginalExtent() { return originalExtent; }
	bool GetBoxType() { return boxtype; }
	BoundingBox GetAABB() { return aabbBox; }
	BoundingBox GetMeshAABB() { return MeshaabbBox; }
	BoundingOrientedBox& GetOBB() { return obbBox; }
	std::vector<GameObject*>* GetBoundingObject() { return boundingObject; }
	void SetBoundingObject(std::vector<GameObject*>* tempObject) { this->boundingObject = tempObject; }
	void allocateBvec() { boundingObject = new std::vector<GameObject*>; }

};

class RotatingObject : public GameObject
{
public:
	RotatingObject(int meshesNum = 1);
	virtual ~RotatingObject();
private:
	XMFLOAT3 rotationAxis;
	float rotationSpeed;
public:
	void SetRotationSpeed(float rotationSpeed) { this->rotationSpeed = rotationSpeed; }
	void SetRotationAxis(XMFLOAT3 rotationAxis)
	{
		this->rotationAxis = rotationAxis;
	}
	virtual void Animate(float timeElapsed, XMFLOAT4X4* parent = NULL);

};

class HeightMapTerrain : public GameObject
{
public:
	HeightMapTerrain(ID3D12Device *device, ID3D12GraphicsCommandList
		*commandList, LPCTSTR pFileName, int nWidth, int nLength, int nBlockWidth, int nBlockLength,
		XMFLOAT3 xmf3Scale, XMFLOAT4 xmf4Color);
	virtual ~HeightMapTerrain();
private:
	HeightMapImage *heightMapImage;
	std::vector<NaviTile> ntvec;
	int navibuffer[257][257];


	int width;
	int length;
	int layer = 0;

	XMFLOAT3 scale;

public:
	float GetHeight(float x, float z)
	{
		return(heightMapImage->GetHeight(x / scale.x, z / scale.z) * scale.y);
	}
	XMFLOAT3 GetNormal(float x, float z)
	{
		return(heightMapImage->GetHeightMapNormal(int(x / scale.x), int(z / scale.z)));
	}

	float GetHeightRunTime(XMFLOAT3 Position) {
		//바운딩 박스와 충돌검사를 하고 -> 충돌된 메쉬의 프리미티브 단위로 검사를 하여 위치만큼 이동
		XMFLOAT3 direction = { 0.0f ,-1.0f , 0.0f };
		XMVECTOR pos = XMLoadFloat3(&Position);

		HeightMapGridMesh* heightMapMesh = (HeightMapGridMesh*)GetMesh(0);
		int bIdx = FAILPICKING;
		for (int i = 0; i < TERRAINBOUNDINGS; ++i) {

			BoundingBox worldBounding = heightMapMesh->GetBoundings()[i];
			worldBounding.Center = Vector3::TransformCoord(worldBounding.Center, world);
			if (worldBounding.Contains(pos) == CONTAINS) {
				bIdx = i;
				break;
			}
		}
		if (bIdx == FAILPICKING)
			std::cout << "gameobject out of terrain" << std::endl;
		else {
			float HitDistance = FLT_MAX;
			XMVECTOR tempDirection = XMLoadFloat3(&direction);
			Position.y = 10000.0f;
			XMVECTOR pos = XMLoadFloat3(&Position);
			int* bindex = heightMapMesh->GetBoudingVertexIndex();
			heightMapMesh->CheckRayIntersect(pos, tempDirection, HitDistance, this->world, bindex[bIdx]);
			XMFLOAT3 RayPlace = Vector3::Add(Position, Vector3::ScalarProduct(direction, HitDistance));
			return RayPlace.y;
		}
		return Position.y;

	}

	int GetHeightMapWidth() { return(heightMapImage->GetHeightMapWidth()); }
	int GetHeightMapLength() { return(heightMapImage->GetHeightMapLength()); }

	XMFLOAT3 GetScale() { return(scale); }
	void SetScale(float x, float y, float z) { scale.x = x; scale.y = y; scale.z = z; }
	float GetWidth() { return(width * scale.x); }
	float GetLength() { return(length * scale.z); }
	void SetLayer(int layer) { layer = layer; }
	int &GetLayer() {
		return (layer);
	}
	std::vector<NaviTile>& Getntvec() { return ntvec; }
	int(*GetNaviBuffer())[257]{ return navibuffer; }
};

class SuperCobraObject : public GameObject
{
public:
	SuperCobraObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature);
	virtual ~SuperCobraObject();

private:
	GameObject					*mainRotorFrame = NULL;
	GameObject					*tailRotorFrame = NULL;

public:
	virtual void PrepareAnimate();
	virtual void Animate(float fTimeElapsed, XMFLOAT4X4 *pxmf4x4Parent = NULL);
};

class TreeObject : public GameObject
{
public:
	TreeObject();
	virtual ~TreeObject();

	virtual void Animate(float timeElapsed, XMFLOAT4X4* parent = NULL) {};

	virtual bool IsFar(Camera* camera);
};




class BillboardObject : public GameObject
{
public:

	BillboardGSVertex billboardVertex;


	BillboardObject();
	virtual ~BillboardObject();

	virtual bool IsFar(Camera* camera);
	virtual void UpdateTransform(VS_VB_INSTANCE_BILLBOARD* vbMappedBillboardObjects, int idx);

};

class AnimationObject : public GameObject
{
public:
	AnimationObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature, LoadedModelInfo *model, int animationTracks);
	AnimationObject(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature);
	virtual ~AnimationObject();

	virtual void UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, XMFLOAT4X4 *xmf4x4World);

};