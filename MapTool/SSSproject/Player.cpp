#include "stdafx.h"
#include "Player.h"
#include "Shader.h"
#include "ObjectShader.h"


Player::Player(int meshesNum)
{
	this->meshesNum = meshesNum;
	camera = NULL;
	position = XMFLOAT3(0.0f, 0.0f, 0.0f);
	right = XMFLOAT3(1.0f, 0.0f, 0.0f);
	up = XMFLOAT3(0.0f, 1.0f, 0.0f);
	look = XMFLOAT3(0.0f, 0.0f, 1.0f);
	velocity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	gravity = XMFLOAT3(0.0f, 0.0f, 0.0f);
	maxVelocityXZ = 0.0f;
	maxVelocityY = 0.0f;
	friction = 0.0f;
	pitch = 0.0f;
	roll = 0.0f;
	yaw = 0.0f;
	playerUpdatedContext = NULL;
	cameraUpdatedContext = NULL;
}
Player::~Player()
{
	if (camera) delete camera;
}

void Player::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	if (camera) camera->CreateShaderVariables(device, commandList);
}
void Player::ReleaseShaderVariables()
{
	if (camera) camera->ReleaseShaderVariables();
}
void Player::UpdateShaderVariables(ID3D12GraphicsCommandList* commandList)
{
	if (camera) camera->UpdateShaderVariables(commandList);
}

/*플레이어의 위치를 변경하는 함수이다. 플레이어의 위치는 기본적으로 사용자가 플레이어를 이동하기 위한 키보드를
누를 때 변경된다. 플레이어의 이동 방향(dwDirection)에 따라 플레이어를 fDistance 만큼 이동한다.*/
void Player::Move(DWORD direction, float distance, bool updateVelocity)
{
	if (direction)
	{
		XMFLOAT3 shift = XMFLOAT3(0, 0, 0);
		if (direction & DIR_FORWARD && direction & DIR_LEFT) {
			distance = distance / 2;
		}
		else if (direction & DIR_FORWARD && direction & DIR_RIGHT) {
			distance = distance / 2;
		}
		else if (direction & DIR_BACKWARD && direction & DIR_LEFT) {
			distance = distance / 2;
		}
		else if (direction & DIR_BACKWARD && direction & DIR_RIGHT) {
			distance = distance / 2;
		}


		if (direction & DIR_FORWARD) shift = Vector3::Add(shift, look, distance);
		if (direction & DIR_BACKWARD) shift = Vector3::Add(shift, look, -distance);
		//화살표 키 ‘↑’를 누르면 로컬 z-축 방향으로 이동(전진)한다. ‘↓’를 누르면 반대 방향으로 이동한다.

		if (direction & DIR_RIGHT) shift = Vector3::Add(shift, right, distance);
		if (direction & DIR_LEFT) shift = Vector3::Add(shift, right, -distance);
		//화살표 키 ‘→’를 누르면 로컬 x-축 방향으로 이동한다. ‘←’를 누르면 반대 방향으로 이동한다.

		if (direction & DIR_UP) shift = Vector3::Add(shift, up, distance);
		if (direction & DIR_DOWN) shift = Vector3::Add(shift, up, -distance);
		//‘Page Up’을 누르면 로컬 y-축 방향으로 이동한다. ‘Page Down’을 누르면 반대 방향으로 이동한다.

		Move(shift, false);
		//플레이어를 현재 위치 벡터에서 Shift 벡터만큼 이동한다.
	}
}
void Player::Move(const XMFLOAT3& shift, bool updateVelocity)
{
	//bUpdateVelocity가 참이면 플레이어를 이동하지 않고 속도 벡터를 변경한다.
	if (updateVelocity)
	{
		//플레이어의 속도 벡터를 Shift 벡터만큼 변경한다.
		velocity = Vector3::Add(velocity, shift);
	}
	else
	{
		//플레이어를 현재 위치 벡터에서 Shift 벡터만큼 이동한다.
		position = Vector3::Add(position, shift);
		//플레이어의 위치가 변경되었으므로 카메라의 위치도 Shift 벡터만큼 이동한다.
		camera->Move(shift);
	}
}
//플레이어를 로컬 x-축, y-축, z-축을 중심으로 회전한다.
void Player::Rotate(float x, float y, float z)
{
	DWORD cameraMode = camera->GetMode();
	//1인칭 카메라 또는 3인칭 카메라의 경우 플레이어의 회전은 약간의 제약이 따른다.
	if ((cameraMode == FIRST_PERSON_CAMERA) || (cameraMode == THIRD_PERSON_CAMERA))
	{
		/*로컬 x-축을 중심으로 회전하는 것은 고개를 앞뒤로 숙이는 동작에 해당한다. 그러므로 x-축을 중심으로 회전하는
		각도는 -89.0~+89.0도 사이로 제한한다. x는 현재의 fPitch에서 실제 회전하는 각도이므로 x만큼 회전한 다음
		Pitch가 +89도 보다 크거나 -89도 보다 작으면 fPitch가 +89도 또는 -89도가 되도록 회전각도(x)를 수정한다.*/
		if (x != 0.0f)
		{
			pitch += x;
			if (pitch > +89.0f) { x -= (pitch - 89.0f); pitch = +89.0f; }
			if (pitch < -89.0f) { x -= (pitch + 89.0f); pitch = -89.0f; }
		}
		if (y != 0.0f)
		{
			//로컬 y-축을 중심으로 회전하는 것은 몸통을 돌리는 것이므로 회전 각도의 제한이 없다.
			yaw += y;
			if (yaw > 360.0f) yaw -= 360.0f;
			if (yaw < 0.0f) yaw += 360.0f;
		}
		if (z != 0.0f)
		{
			/*로컬 z-축을 중심으로 회전하는 것은 몸통을 좌우로 기울이는 것이므로 회전 각도는 -20.0~+20.0도 사이로 제한된
			다. z는 현재의 fRoll에서 실제 회전하는 각도이므로 z만큼 회전한 다음 fRoll이 +20도 보다 크거나 -20도보다
			작으면 fRoll이 +20도 또는 -20도가 되도록 회전각도(z)를 수정한다.*/
			roll += z;
			if (roll > +20.0f) { z -= (roll - 20.0f); roll = +20.0f; }
			if (roll < -20.0f) { z -= (roll + 20.0f); roll = -20.0f; }
		}
		//카메라를 x, y, z 만큼 회전한다. 플레이어를 회전하면 카메라가 회전하게 된다.
		camera->Rotate(x, y, z);
		/*플레이어를 회전한다. 1인칭 카메라 또는 3인칭 카메라에서 플레이어의 회전은 로컬 y-축에서만 일어난다. 플레이어
		의 로컬 y-축(Up 벡터)을 기준으로 로컬 z-축(Look 벡터)와 로컬 x-축(Right 벡터)을 회전시킨다. 기본적으로 Up 벡
		터를 기준으로 회전하는 것은 플레이어가 똑바로 서있는 것을 가정한다는 의미이다.*/
		if (y != 0.0f)
		{
			XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&up), XMConvertToRadians(y));
			look = Vector3::TransformNormal(look, rotate);
			right = Vector3::TransformNormal(right, rotate);
		}

		if (x != 0.0f)
		{
			XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&right), XMConvertToRadians(x));
			look = Vector3::TransformNormal(look, rotate);
			up = Vector3::TransformNormal(up, rotate);
		}
	}
	else if (cameraMode == SPACESHIP_CAMERA)
	{
		/*스페이스-쉽 카메라에서 플레이어의 회전은 회전 각도의 제한이 없다. 그리고 모든 축을 중심으로 회전을 할 수 있
		다.*/
		camera->Rotate(x, y, z);
		if (x != 0.0f)
		{
			XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&right), XMConvertToRadians(x));
			look = Vector3::TransformNormal(look, rotate);
			up = Vector3::TransformNormal(up, rotate);
		}
		if (y != 0.0f)
		{
			XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&up), XMConvertToRadians(y));
			look = Vector3::TransformNormal(look, rotate);
			right = Vector3::TransformNormal(right, rotate);
		}
		if (z != 0.0f)
		{
			XMMATRIX rotate = XMMatrixRotationAxis(XMLoadFloat3(&look), XMConvertToRadians(z));
			up = Vector3::TransformNormal(up, rotate);
			right = Vector3::TransformNormal(right, rotate);
		}
	}
	/*회전으로 인해 플레이어의 로컬 x-축, y-축, z-축이 서로 직교하지 않을 수 있으므로 z-축(LookAt 벡터)을 기준으
	로 하여 서로 직교하고 단위벡터가 되도록 한다.*/
	look = Vector3::Normalize(look);
	right = Vector3::CrossProduct(up, look, true);
	up = Vector3::CrossProduct(look, right, true);
}

//이 함수는 매 프레임마다 호출된다. 플레이어의 속도 벡터에 중력과 마찰력 등을 적용한다.
void Player::Update(float timeElapsed)
{
	// 플레이어의 속도 벡터를 중력 벡터와 더한다. 중력 벡터에 fTimeElapsed를 곱하는 것은 중력을 시간에 비례하도록 적용한다는 의미이다.
	velocity = Vector3::Add(velocity, Vector3::ScalarProduct(gravity, timeElapsed, false));
	// 플레이어의 속도 벡터의 XZ-성분의 크기를 구한다. 이것이 XZ-평면의 최대 속력보다 크면 속도 벡터의 x와 z-방향 성분을 조정한다.
	float length = sqrtf(velocity.x * velocity.x + velocity.z * velocity.z);
	float maxVelocityXZ = this->maxVelocityXZ * timeElapsed;
	if (length > this->maxVelocityXZ)
	{
		velocity.x *= (maxVelocityXZ / length);
		velocity.z *= (maxVelocityXZ / length);
	}
	//플레이어의 속도 벡터의 y-성분의 크기를 구한다. 이것이 y-축 방향의 최대 속력보다 크면 속도 벡터의 y-방향 성분을 조정한다.
	float maxVelocityY = this->maxVelocityY * timeElapsed;
	length = sqrtf(velocity.y * velocity.y);
	if (length > this->maxVelocityY) velocity.y *= (maxVelocityY / length);
	//플레이어를 속도 벡터 만큼 실제로 이동한다(카메라도 이동될 것이다).
	Move(velocity, false);
	/*플레이어의 위치가 변경될 때 추가로 수행할 작업을 수행한다. 플레이어의 새로운 위치가 유효한 위치가 아닐 수도
	있고 또는 플레이어의 충돌 검사 등을 수행할 필요가 있다. 이러한 상황에서 플레이어의 위치를 유효한 위치로 다시 변경할 수 있다.*/
	if (playerUpdatedContext) OnPlayerUpdateCallback(timeElapsed);
	DWORD cameraMode = camera->GetMode();
	//플레이어의 위치가 변경되었으므로 3인칭 카메라를 갱신한다.
	if (cameraMode == THIRD_PERSON_CAMERA) camera->Update(position, timeElapsed);
	//카메라의 위치가 변경될 때 추가로 수행할 작업을 수행한다.
	if (cameraUpdatedContext) OnCameraUpdateCallback(timeElapsed);
	//카메라가 3인칭 카메라이면 카메라가 변경된 플레이어 위치를 바라보도록 한다.
	if (cameraMode == THIRD_PERSON_CAMERA) camera->SetLookAt(position);
	//카메라의 카메라 변환 행렬을 다시 생성한다.
	camera->RegenerateViewMatrix();
	/*플레이어의 속도 벡터가 마찰력 때문에 감속이 되어야 한다면 감속 벡터를 생성한다. 속도 벡터의 반대 방향 벡터를
	구하고 단위 벡터로 만든다. 마찰 계수를 시간에 비례하도록 하여 마찰력을 구한다. 단위 벡터에 마찰력을 곱하여 감
	속 벡터를 구한다. 속도 벡터에 감속 벡터를 더하여 속도 벡터를 줄인다. 마찰력이 속력보다 크면 속력은 0이 될 것이다.*/
	length = Vector3::Length(velocity);
	float deceleration = (friction * timeElapsed);
	if (deceleration > length) deceleration = length;
	velocity = Vector3::Add(velocity, Vector3::ScalarProduct(velocity, -deceleration, true));
}
/*카메라를 변경할 때 ChangeCamera() 함수에서 호출되는 함수이다. nCurrentCameraMode는 현재 카메라의 모드
이고 nNewCameraMode는 새로 설정할 카메라 모드이다.*/
Camera *Player::OnChangeCamera(DWORD newCameraMode, DWORD currentCameraMode)
{
	//새로운 카메라의 모드에 따라 카메라를 새로 생성한다.
	Camera *newCamera = NULL;
	switch (newCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		newCamera = new FirstPersonCamera(camera);
		break;
	case THIRD_PERSON_CAMERA:
		newCamera = new ThirdPersonCamera(camera);
		break;
	case SPACESHIP_CAMERA:
		newCamera = new SpaceShipCamera(camera);
		break;
	}
	/*현재 카메라의 모드가 스페이스-쉽 모드의 카메라이고 새로운 카메라가 1인칭 또는 3인칭 카메라이면 플레이어의
	Up 벡터를 월드좌표계의 y-축 방향 벡터(0, 1, 0)이 되도록 한다. 즉, 똑바로 서도록 한다. 그리고 스페이스-쉽 카메
	라의 경우 플레이어의 이동에는 제약이 없다. 특히, y-축 방향의 움직임이 자유롭다. 그러므로 플레이어의 위치는 공
	중(위치 벡터의 y-좌표가 0보다 크다)이 될 수 있다. 이때 새로운 카메라가 1인칭 또는 3인칭 카메라이면 플레이어의
	위치는 지면이 되어야 한다. 그러므로 플레이어의 Right 벡터와 Look 벡터의 y 값을 0으로 만든다. 이제 플레이어의
	Right 벡터와 Look 벡터는 단위벡터가 아니므로 정규화한다.*/
	if (currentCameraMode == SPACESHIP_CAMERA)
	{
		right = Vector3::Normalize(XMFLOAT3(right.x, 0.0f, right.z));
		up = Vector3::Normalize(XMFLOAT3(0.0f, 1.0f, 0.0f));
		look = Vector3::Normalize(XMFLOAT3(look.x, 0.0f, look.z));
		pitch = 0.0f;
		roll = 0.0f;
		/*Look 벡터와 월드좌표계의 z-축(0, 0, 1)이 이루는 각도(내적=cos)를 계산하여 플레이어의 y-축의 회전 각도 yaw로 설정한다.*/
		yaw = Vector3::Angle(XMFLOAT3(0.0f, 0.0f, 1.0f), look);
		if (look.x < 0.0f) yaw = -yaw;
	}
	else if ((newCameraMode == SPACESHIP_CAMERA) && camera)
	{
		/*새로운 카메라의 모드가 스페이스-쉽 모드의 카메라이고 현재 카메라 모드가 1인칭 또는 3인칭 카메라이면 플레이
		어의 로컬 축을 현재 카메라의 로컬 축과 같게 만든다.*/
		right = camera->GetRightVector();
		up = camera->GetUpVector();
		look = camera->GetLookVector();
	}
	if (newCamera)
	{
		newCamera->SetMode(newCameraMode);
		//현재 카메라를 사용하는 플레이어 객체를 설정한다.
		newCamera->SetPlayer(this);
	}
	if (camera) delete camera;
	return(newCamera);
}

/*플레이어의 위치와 회전축으로부터 월드 변환 행렬을 생성하는 함수이다. 플레이어의 Right 벡터가 월드 변환 행렬
의 첫 번째 행 벡터, Up 벡터가 두 번째 행 벡터, Look 벡터가 세 번째 행 벡터, 플레이어의 위치 벡터가 네 번째 행
벡터가 된다.*/
void Player::OnPrepareRender()
{
	world._11 = right.x;
	world._12 = right.y;
	world._13 = right.z;
	world._21 = up.x;
	world._22 = up.y;
	world._23 = up.z;
	world._31 = look.x;
	world._32 = look.y;
	world._33 = look.z;
	world._41 = position.x;
	world._42 = position.y;
	world._43 = position.z;
}
void Player::Render(ID3D12GraphicsCommandList* commandList, Camera* camera)
{
	DWORD cameraMode = (camera) ? camera->GetMode() : 0x00;

	if (cameraMode == THIRD_PERSON_CAMERA)
		GameObject::Render(commandList, camera);
	//카메라 모드가 3인칭이면 플레이어 객체를 렌더링한다.
}

AirplanePlayer::AirplanePlayer(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, int meshesNum)
{
	this->meshesNum = meshesNum;

	//Mesh* airplaneMesh = new AirplaneMeshDiffused(device, commandList, 20.0f, 20.0f, 4.0f, XMFLOAT4(0.0f, 0.5f, 0.0f, 0.0f));
	Mesh* airplaneMesh = new CubeMeshIlluminatedTextured(device, commandList, 12.0f, 12.0f, 12.0f);
	SetMesh(0, airplaneMesh);
	//비행기 메쉬를 생성한다.

	camera = ChangeCamera(SPACESHIP_CAMERA, 0.0f);
	//플레이어의 카메라를 스페이스-쉽 카메라로 변경(생성)한다.

	CreateShaderVariables(device, commandList);
	//플레이어를 위한 셰이더 변수를 생성한다.

	SetPosition(XMFLOAT3(0.0f, 100.0f, 0.0f));
	//플레이어의 위치를 설정한다.

	PlayerShader *shader = new PlayerShader();
	shader->CreateShader(device, graphicsRootSignature);
	shader->CreateShaderVariables(device, commandList);
	SetShader(shader);
	//플레이어(비행기) 메쉬를 렌더링할 때 사용할 셰이더를 생성한다.
	type = ObjectTypePlayer;
}

AirplanePlayer::~AirplanePlayer()
{
}

void AirplanePlayer::OnPrepareRender()
{
	Player::OnPrepareRender();
	//비행기 모델을 그리기 전에 x-축으로 90도 회전한다.
	XMMATRIX rotate = XMMatrixRotationRollPitchYaw(XMConvertToRadians(90.0f), 0.0f, 0.0f);
	world = Matrix4x4::Multiply(rotate, world);
}
/*3인칭 카메라일 때 플레이어 메쉬를 로컬 x-축을 중심으로 +90도 회전하고 렌더링한다. 왜냐하면 비행기 모델 메쉬
는 다음 그림과 같이 y-축 방향이 비행기의 앞쪽이 되도록 모델링이 되었기 때문이다. 그리고 이 메쉬를 카메라의 z-
축 방향으로 향하도록 그릴 것이기 때문이다.*/

//카메라를 변경할 때 호출되는 함수이다. nNewCameraMode는 새로 설정할 카메라 모드이다.
Camera *AirplanePlayer::ChangeCamera(DWORD newCameraMode, float timeElapsed)
{
	DWORD currentCameraMode = (camera) ? camera->GetMode() : 0x00;
	if (currentCameraMode == newCameraMode) return(camera);
	switch (newCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		//플레이어의 특성을 1인칭 카메라 모드에 맞게 변경한다. 중력은 적용하지 않는다.
		SetFriction(200.0f);
		SetGravity(XMFLOAT3(0.0f, 0.0f, 0.0f));
		SetMaxVelocityXZ(125.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(FIRST_PERSON_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.0f);
		camera->SetOffset(XMFLOAT3(0.0f, 20.0f, 0.0f));
		camera->GenerateProjectionMatrix(1.00f, 5000.0f, ASPECT_RATIO, 60.0f);
		camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	case SPACESHIP_CAMERA:
		//플레이어의 특성을 스페이스-쉽 카메라 모드에 맞게 변경한다. 중력은 적용하지 않는다.
		SetFriction(125.0f);
		SetGravity(XMFLOAT3(0.0f, 0.0f, 0.0f));
		SetMaxVelocityXZ(125.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(SPACESHIP_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.0f);
		camera->SetOffset(XMFLOAT3(0.0f, 0.0f, 0.0f));
		camera->GenerateProjectionMatrix(0.01f, 1000.0f, ASPECT_RATIO, 90.0f);
		camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	case THIRD_PERSON_CAMERA:
		//플레이어의 특성을 3인칭 카메라 모드에 맞게 변경한다. 지연 효과와 카메라 오프셋을 설정한다.
		SetFriction(250.0f);
		SetGravity(XMFLOAT3(0.0f, 0.0f, 0.0f));
		SetMaxVelocityXZ(125.0f);
		SetMaxVelocityY(125.0f);
		camera = OnChangeCamera(THIRD_PERSON_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.25f);
		//3인칭 카메라의 지연 효과를 설정한다. 값을 0.25f 대신에 0.0f와 1.0f로 설정한 결과를 비교하기 바란다.
		camera->SetOffset(XMFLOAT3(0.0f, 20.0f, -50.0f));
		camera->GenerateProjectionMatrix(1.00, 5000.0f, ASPECT_RATIO, 60.0f);
		camera->SetViewport(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT, 0.0f, 1.0f);
		camera->SetScissorRect(0, 0, FRAME_BUFFER_WIDTH, FRAME_BUFFER_HEIGHT);
		break;
	default:
		break;
	}
	//플레이어를 시간의 경과에 따라 갱신(위치와 방향을 변경: 속도, 마찰력, 중력 등을 처리)한다.
	Update(timeElapsed);
	return(camera);
}

TerrainPlayer::TerrainPlayer(ID3D12Device *device, ID3D12GraphicsCommandList
	*commandList, ID3D12RootSignature *graphicsRootSignature, void *context,
	int meshesNum) : Player(meshesNum)
{
	camera = ChangeCamera(THIRD_PERSON_CAMERA, 0.0f);
	HeightMapTerrain *terrain = (HeightMapTerrain *)context;
	// 플레이어의 위치를 지형의 가운데(y-축 좌표는 지형의 높이보다 1500 높게)로 설정한다. 
	// 플레이어 위치 벡터의 y-좌표가 지형의 높이보다 크고 중력이 작용하도록 플레이어를
	// 설정하였으므로 플레이어는 점차적으로 하강하게 된다.
	float fHeight = terrain->GetHeight(terrain->GetWidth()*0.5f, terrain->GetLength()*0.5f);
	SetPosition(XMFLOAT3(terrain->GetWidth()*0.5f, fHeight + 1500.0f, terrain->GetLength()*0.5f));
	//플레이어의 위치가 변경될 때 지형의 정보에 따라 플레이어의 위치를 변경할 수 있도록 설정한다.
	SetPlayerUpdatedContext(terrain);
	//카메라의 위치가 변경될 때 지형의 정보에 따라 카메라의 위치를 변경할 수 있도록 설정한다.
	SetCameraUpdatedContext(terrain);
	CubeMeshDiffused *pCubeMesh = new CubeMeshDiffused(device, commandList, 4.0f, 12.0f, 4.0f);
	SetMesh(0, pCubeMesh);
	//플레이어를 렌더링할 셰이더를 생성한다.
	PlayerShader *playherShader = new PlayerShader();
	playherShader->CreateShader(device, graphicsRootSignature);
	SetShader(playherShader);
	CreateShaderVariables(device, commandList);
}

TerrainPlayer::~TerrainPlayer()
{
}

Camera *TerrainPlayer::ChangeCamera(DWORD newCameraMode, float timeElapsed)
{
	DWORD currentCameraMode = (camera) ? camera->GetMode() : 0x00;
	if (currentCameraMode == newCameraMode) return(camera);
	switch (newCameraMode)
	{
	case FIRST_PERSON_CAMERA:
		SetFriction(250.0f);
		//1인칭 카메라일 때 플레이어에 y-축 방향으로 중력이 작용한다.
		SetGravity(XMFLOAT3(0.0f, -250.0f, 0.0f));
		SetMaxVelocityXZ(300.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(FIRST_PERSON_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.0f);
		camera->SetOffset(XMFLOAT3(0.0f, 20.0f, 0.0f));
		camera->GenerateProjectionMatrix(1.01f, 50000.0f, ASPECT_RATIO, 60.0f);
		break;
	case SPACESHIP_CAMERA:
		SetFriction(125.0f);
		//스페이스 쉽 카메라일 때 플레이어에 중력이 작용하지 않는다.
		SetGravity(XMFLOAT3(0.0f, 0.0f, 0.0f));
		SetMaxVelocityXZ(300.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(SPACESHIP_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.0f);
		camera->SetOffset(XMFLOAT3(0.0f, 0.0f, 0.0f));
		camera->GenerateProjectionMatrix(1.01f, 50000.0f, ASPECT_RATIO, 60.0f);
		break;
	case THIRD_PERSON_CAMERA:
		SetFriction(250.0f);
		//3인칭 카메라일 때 플레이어에 y-축 방향으로 중력이 작용한다.
		SetGravity(XMFLOAT3(0.0f, -250.0f, 0.0f));
		SetMaxVelocityXZ(300.0f);
		SetMaxVelocityY(400.0f);
		camera = OnChangeCamera(THIRD_PERSON_CAMERA, currentCameraMode);
		camera->SetTimeLag(0.25f);
		camera->SetOffset(XMFLOAT3(0.0f, 20.0f, -50.0f));
		camera->GenerateProjectionMatrix(1.01f, 50000.0f, ASPECT_RATIO, 60.0f);
		break;
	default:
		break;
	}
	Update(timeElapsed);
	return(camera);
}

// 플레이어의 위치가 바뀔 때 마다 호출되는 함수
void TerrainPlayer::OnPlayerUpdateCallback(float fTimeElapsed)
{
	XMFLOAT3 playerPosition = GetPosition();
	HeightMapTerrain *terrain = (HeightMapTerrain *)playerUpdatedContext;
	/*지형에서 플레이어의 현재 위치 (x, z)의 지형 높이(y)를 구한다. 그리고 플레이어 메쉬의 높이가 12이고 플레이어의
	중심이 직육면체의 가운데이므로 y 값에 메쉬의 높이의 절반을 더하면 플레이어의 위치가 된다.*/
	float height = terrain->GetHeight(playerPosition.x, playerPosition.z) + 6.0f;
	/*플레이어의 위치 벡터의 y-값이 음수이면(예를 들어, 중력이 적용되는 경우) 플레이어의 위치 벡터의 y-값이 점점
	작아지게 된다. 이때 플레이어의 현재 위치 벡터의 y 값이 지형의 높이(실제로 지형의 높이 + 6)보다 작으면 플레이어
	의 일부가 지형 아래에 있게 된다. 이러한 경우를 방지하려면 플레이어의 속도 벡터의 y 값을 0으로 만들고 플레이어
	의 위치 벡터의 y-값을 지형의 높이(실제로 지형의 높이 + 6)로 설정한다. 그러면 플레이어는 항상 지형 위에 있게 된다.*/
	if (playerPosition.y < height)
	{
		XMFLOAT3 xmf3PlayerVelocity = GetVelocity();
		xmf3PlayerVelocity.y = 0.0f;
		SetVelocity(xmf3PlayerVelocity);
		playerPosition.y = height;
		SetPosition(playerPosition);
	}
}

void TerrainPlayer::OnCameraUpdateCallback(float timeElapsed)
{
	XMFLOAT3 cameraPosition = camera->GetPosition();
	/*높이 맵에서 카메라의 현재 위치 (x, z)에 대한 지형의 높이(y 값)를 구한다. 이 값이 카메라의 위치 벡터의 y-값 보
	다 크면 카메라가 지형의 아래에 있게 된다. 이렇게 되면 다음 그림의 왼쪽과 같이 지형이 그려지지 않는 경우가 발생
	한다(카메라가 지형 안에 있으므로 삼각형의 와인딩 순서가 바뀐다). 이러한 경우가 발생하지 않도록 카메라의 위치 벡
	터의 y-값의 최소값은 (지형의 높이 + 5)로 설정한다. 카메라의 위치 벡터의 y-값의 최소값은 지형의 모든 위치에서
	카메라가 지형 아래에 위치하지 않도록 설정해야 한다.*/
	HeightMapTerrain *terrain = (HeightMapTerrain *)cameraUpdatedContext;
	float height = terrain->GetHeight(cameraPosition.x, cameraPosition.z) + 5.0f;
	if (cameraPosition.y <= height)
	{
		cameraPosition.y = height;
		camera->SetPosition(cameraPosition);
		if (camera->GetMode() == THIRD_PERSON_CAMERA)
		{
			//3인칭 카메라의 경우 카메라 위치(y-좌표)가 변경되었으므로 카메라가 플레이어를 바라보도록 한다.
			ThirdPersonCamera *thirdPersonCamera = (ThirdPersonCamera *)camera;
			thirdPersonCamera->SetLookAt(GetPosition());
		}
	}
}