#pragma once
#include "GameObject.h"
#include "Camera.h"
#include "Animation.h"


//인스턴스 정보(게임 객체의 월드 변환 행렬과 객체의 색상)를 위한 구조체이다.


struct VS_VB_INSTANCE_OBJECT
{
	XMFLOAT4X4 transform;
	bool isDummy;
};

struct VS_VB_INSTACE_BOUNDING {
	XMFLOAT4X4 transform;
	bool isDummy;
	int bIdx;
};

struct VS_VB_INSTANCE_MATERIAL
{
	UINT reflection;	// 재질의 번호
	UINT textureMask;	// 텍스쳐 종류
	UINT textureIdx;	// 텍스쳐 인덱스 diffuse, normal, specular, metallic, emissive
};

struct VS_VB_INSTANCE_BILLBOARD
{
	XMFLOAT3 position;
	XMFLOAT3 instanceInfo;	// x : width, y : height, z : textureIdx
};

struct CB_GAMEOBJECT_INFO
{
	XMFLOAT4X4 world;
	XMFLOAT4X4 inverseWorld;

};

struct CB_PLAYER_INFO
{
	XMFLOAT4X4 world;
};

struct VS_SB_PARTICLE
{
	XMFLOAT3 position;
	XMFLOAT3 velocity;
	float age;
};

struct ObjectBuffer
{
	std::vector<GameObject*> objects;
	int objectsNum = 0;

	int framesNum = 0;
	int materialsNum = 0;

	ID3D12Resource** cbGameObjects = NULL;
	CB_GAMEOBJECT_INFO** cbMappedGameObjects = NULL;
	UINT cbGameObjectBytes = 0;

	UINT pipeLineStateIdx = 0;
};

struct InstanceBuffer
{
	std::vector<GameObject*> objects;
	int objectsNum = 0;

	int useBillboard = -1;
	int isBillboard = -1;
	// 빌보드 사각형의 idx정보

	int framesNum;
	int materialsNum;

	std::vector<ID3D12Resource**> vbGameObjects;
	std::vector<VS_VB_INSTANCE_OBJECT**> vbMappedGameObjects;
	// [][] : Frames, Objects
	std::vector<D3D12_VERTEX_BUFFER_VIEW*> instancingGameObjectBufferView;
	UINT vbGameObjectBytes = 0;

	std::vector<ID3D12Resource*> vbBounding;
	std::vector<VS_VB_INSTACE_BOUNDING*> vbMappedBounding;
	// [][] : Frames, Objects
	std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBoundingBufferView;
	UINT vbBoundingBytes = 0;



	std::vector<ID3D12Resource***> vbMaterials;
	std::vector<VS_VB_INSTANCE_MATERIAL***> vbMappedMaterials;
	// [][][] : Frames, Materials, Objects
	std::vector<D3D12_VERTEX_BUFFER_VIEW**> instancingMaterialBufferView;
	UINT vbMaterialBytes = 0;

	std::vector<ID3D12Resource*> vbBillboardObjects;
	std::vector<VS_VB_INSTANCE_BILLBOARD*> vbMappedBillboardObjects;
	std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBillboardObjectBufferView;
	UINT vbBillboardObjectBytes = 0;

	UINT pipeLineStateIdx = 0;

	TEXTURENAME textureName;
	int diffuseIdx = 0;
	int normalIdx = 0;
	int specularIdx = 0;
	int metallicIdx = 0;
	int emissionIdx = 0;
	// Scene::rootArgument의 idx 값.
};

struct SkinnedInstanceBuffer
{
	std::vector<GameObject*> objects;
	int objectsNum = 0;

	int skinnedMeshNum = 0;
	int standardMeshNum = 0;
	int boundingMeshNum = 0;

	SkinnedMesh** skinnedMeshes = NULL;

	ID3D12Resource** skinnedBuffer = NULL;
	XMFLOAT4X4** mappedSkinnedBuffer = NULL;	//[][][] skinNum, objectNum, bone

	ID3D12Resource* objIdxBuffer = NULL;
	UINT* mappedObjIdxBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW instancingObjIdxBufferView;

	ID3D12Resource** standardBuffer = NULL;
	VS_VB_INSTANCE_OBJECT** mappedStandardBuffer = NULL;	//[][][] standardNum, objectNum
	D3D12_VERTEX_BUFFER_VIEW* instancingStandardBufferView = NULL;

	ID3D12Resource** boundingBuffer = NULL;
	VS_VB_INSTACE_BOUNDING** mappedBoundingBuffer = NULL;
	D3D12_VERTEX_BUFFER_VIEW* instancingBoundingBufferView;


	TEXTURENAME textureName{};
	int diffuseIdx = 0;
	int normalIdx = 0;
	int specularIdx = 0;
	int metallicIdx = 0;
	int emissionIdx = 0;

};



//셰이더 소스 코드를 컴파일하고 그래픽스 상태 객체를 생성한다.
class Shader
{
public:
	Shader();
	virtual ~Shader();
private:
	int references = 0;

public:
	void AddRef() { ++references; }
	void Release() { if (--references <= 0) delete this; }

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_RASTERIZER_DESC CreateRasterizerState();
	virtual D3D12_BLEND_DESC CreateBlendState();
	virtual D3D12_DEPTH_STENCIL_DESC CreateDepthStencilState();

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual D3D12_SHADER_BYTECODE CreateGeometryShader();

	virtual D3D12_PRIMITIVE_TOPOLOGY_TYPE SetTopologyType();

	D3D12_SHADER_BYTECODE CompileShaderFromFile(const WCHAR* fileName, LPCSTR shaderName, LPCSTR shaderProfile, ID3DBlob** shaderBlob);

	virtual void CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);

	virtual void CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx = 0);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList* commandList);
	virtual void ReleaseShaderVariables();

	virtual void UpdateShaderVariable(ID3D12GraphicsCommandList* commandList, XMFLOAT4X4* world);
	virtual void UpdateShaderVariable(ID3D12GraphicsCommandList *commandList, MATERIAL* material) { }
	virtual void ReleaseUploadBuffers() {}

	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context = NULL) {}
	virtual void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, std::vector<void*>& context, int fobjectSize) {}

	virtual void BuildTerrain(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature* graphicsRootSignature, void* context = NULL) {}
	virtual void BuildInstanceObject(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, InstanceBuffer* instanceBuffer, int objectNum,
		int pipeLineStateIdx, GameObject* objectModel, int modelNumber, XMFLOAT3 boundingSize, XMFLOAT3 rotate, HeightMapTerrain *terrain, XMFLOAT3 position) {}

	virtual void ReleaseObjects() {}
	virtual void AnimateObjects(float timeElapsed, Camera* camera) {}

	virtual void OnPrepareRender(ID3D12GraphicsCommandList* commandList, UINT idx);
	virtual void Render(ID3D12GraphicsCommandList* commandList, Camera* camera);
	virtual int GetGameObjectsNum() { return 0; }
	virtual void AddGameObject(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, XMFLOAT3 pointxyz, ID3D12RootSignature* graphicsRootSignature, int objectType , bool noiseFlag ) {}
	virtual int AddParticle(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, XMFLOAT3 pointxyz, int objectType, UINT textureIdx) { return 0; }
	virtual void LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType) {}
	virtual void DeleteGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int bufferIndex, int objectindex) {};
	virtual void DeleteAllObjcet() {}
	virtual void SetPipeLineCounter(int idx) {}
	virtual void SetTerrainLayer(int idx) {}
	virtual void ApplyLOD(Camera* camera, int idx) {}
	virtual void SetDummyFarAway() {}
	virtual void GarbageCollection() {}

	virtual GameObject* GetGameObject(int idx , int objectidx) { GameObject* temp; return temp; }
	virtual GameObject* GetObjects(int idx) { return NULL; }
	virtual std::vector<InstanceBuffer*> GetBuffer() { std::vector<InstanceBuffer*> temp;   return temp; }
	virtual std::vector< SkinnedInstanceBuffer*> GetSkinedBuffer() { std::vector<SkinnedInstanceBuffer*> temp; return temp; }
	virtual std::vector<GameObject*> GetGameObjects() { std::vector<GameObject*> temp;   return temp; }
	virtual std::vector<GameObject*> GetGameObjectIdx(int idx) { std::vector<GameObject*> temp;   return temp; }
protected:
	int									objectsNum;
	std::vector<GameObject*>		    objects;

	ID3DBlob*							vertexShaderBlob = NULL;
	ID3DBlob*							pixelShaderBlob = NULL;

	int									pipelineStatesNum = 0;
	ID3D12PipelineState**				pipelineStates = NULL;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC	pipelineStateDesc;

	float								elapsedTime = 0.0f;

};

class StandardShader : public Shader
{
protected:
	ObjectBuffer* objectBuffer;
public:
	StandardShader();
	virtual ~StandardShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual void AnimateObjects(float timeElapsed, Camera* camera);
	virtual void Render(ID3D12GraphicsCommandList* commandList, Camera* camera);
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
	virtual D3D12_SHADER_BYTECODE CreatePixelShader();
	virtual void ReleaseShaderVariables();
	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature *graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);
	virtual void CreateShaderVariables(ID3D12Device *pd3dDevice, ID3D12GraphicsCommandList *pd3dCommandList);
	virtual void ReleaseObjects();
	virtual void ReleaseUploadBuffers();
	virtual GameObject* GetObjects(int idx) { return objectBuffer->objects[idx]; }
	virtual std::vector<GameObject*> GetGameObjects() { return objectBuffer->objects; }
};

class StandardInstanceShader : public StandardShader
{
public:
	StandardInstanceShader();
	virtual ~StandardInstanceShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();

	virtual D3D12_SHADER_BYTECODE CreateVertexShader();

	virtual void CreateShader(ID3D12Device *device, ID3D12RootSignature *graphicsRootSignature, ID3D12RootSignature* computeRootSignature = NULL);

};

class StandardComputeShader : public Shader
{
protected:
	D3D12_COMPUTE_PIPELINE_STATE_DESC computePipelineStateDesc;
	ID3D12PipelineState** computePipelineStates = NULL;
	UINT computePipelineStatesNum = 0;

	ID3DBlob* computeShaderBlob = NULL;

public:
	StandardComputeShader();
	virtual ~StandardComputeShader();
	virtual D3D12_SHADER_BYTECODE CreateComputeShader();

};

class SkinnedAnimationStandardShader : public StandardShader
{
public:

	SkinnedAnimationStandardShader();
	virtual ~SkinnedAnimationStandardShader();

	virtual D3D12_INPUT_LAYOUT_DESC CreateInputLayout();
	virtual D3D12_SHADER_BYTECODE CreateVertexShader();
};

class SkinnedAnimationStandardInstanceShader : public SkinnedAnimationStandardShader
{
public:
	bool renderBoundingBox = false;
};

