#pragma once
#include "Shader.h"
#include "Texture.h"

struct MATERIAL
{
	XMFLOAT4 diffuse = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	XMFLOAT4 emissive = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT4 specular = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f); //(r,g,b,a=power)
	XMFLOAT4 ambient = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
};
class Material
{
public:
	Material(int texturesNum);
	virtual ~Material();

private:
	int								references = 0;


public:
	void AddRef() { references++; }
	void Release() { if (--references <= 0) delete this; }

	XMFLOAT4						albedoColor = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
	XMFLOAT4						emissiveColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT4						specularColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);
	XMFLOAT4						ambientColor = XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f);

	float							glossiness = 0.0f;
	float							smoothness = 0.0f;
	float							specularHighlight = 0.0f;
	float							metallic = 0.0f;
	float							glossyReflection = 0.0f;


	UINT reflection = 0;
	// 객체에 적용될 재질정보
	UINT textureMask = 0x00;
	// 객체에 적용될 텍스쳐정보

	UINT textureIdx = 0; // diffuse, normal, specular, metallic, emission 10자리, 두자리씩 할당. (emission은 최대 42)
	// 객체에 적용될 텍스쳐의 인덱스정보

	UINT							diffuseTextureIdx = 0;
	UINT							normalTextureIdx = 0;
	UINT							specularTextureIdx = 0;
	UINT							metallicTextureIdx = 0;
	UINT							emissionTextureIdx = 0;

	int								texturesNum = 0;
	_TCHAR(*textureNames)[128] = NULL;
	Texture							**textures = NULL;
	Shader							*shader = NULL;

	D3D12_GPU_DESCRIPTOR_HANDLE	cbvGPUDescriptorHandle;

	void SetAlbedo(XMFLOAT4 albedo) { this->albedoColor = albedo; }
	void SetTexture(Texture *texture, UINT idx);
	void SetShader(Shader *shader);
	void SetTextureMask(UINT textureMask) { this->textureMask |= textureMask; }
	void SetTextureIdx(UINT type, int idx);

	void UpdateShaderVariables(ID3D12GraphicsCommandList *commandList);

	void ReleaseShaderVariables();

	void ReleaseUploadBuffers();

	UINT GetRelfection() { return reflection; }
	UINT GetTextureMask() { return textureMask; }

	void LoadTextureFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, UINT type, UINT rootParameter, _TCHAR *textureName, Texture **textures, GameObject *parent, FILE *inFile, char* folderName, std::vector<std::pair<int, _TCHAR*>>& textureMapNames, int& idx);

	static Shader					*pStandardShader;
	static Shader					*pSkinnedAnimationShader;

	static void Material::PrepareShaders(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, ID3D12RootSignature *graphicsRootSignature);

	void SetStandardShader() { Material::SetShader(pStandardShader); }
	void SetSkinnedAnimationShader() { Material::SetShader(pSkinnedAnimationShader); }
};
