#include "stdafx.h"
#include "TerrainShader.h"
#include "Scene.h"

TerrainShader::TerrainShader(XMFLOAT3 terrainScale)
{
	this->terrainScale = terrainScale;
}

TerrainShader::~TerrainShader()
{
}

void TerrainShader::BuildTerrain(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, void * context)
{
	//ID3D12Resource *terrainAlphaTexture = (ID3D12Resource *)context;

	objectsNum = 1;
	objects.reserve(objectsNum);
	POINT terrainSize{ 257.0f , 257.0f };
	POINT terrainBlockSize{ 257.0f, 257.0f };

	UINT cbElementBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255);

	CreateShaderVariables(device, commandList);

	D3D12_GPU_DESCRIPTOR_HANDLE handle = Scene::CreateConstantBufferViews(device, commandList, objectsNum, cbGameObjects, cbElementBytes);

	XMFLOAT3 scale(8.0f, 2.0f, 8.0f);
	XMFLOAT4 color(0.0f, 0.5f, 0.0f, 0.0f);
#ifdef _WITH_TERRAIN_PARTITION
	HeightMapTerrain* terrain = new CHeightMapTerrain(pd3dDevice, pd3dCommandList, m_pd3dGraphicsRootSignature, _T("Assets/Image/Terrain/HeightMap.raw"), 257, 257, 17, 17, xmf3Scale, xmf4Color);
#else
	HeightMapTerrain* terrain = new HeightMapTerrain(device, commandList, _T("Assets/Image/Terrain/HeightMap.raw"), terrainSize.x, terrainSize.y, terrainBlockSize.x, terrainBlockSize.y, terrainScale, color);
#endif
	this->terrain = terrain;
	terrain->SetPosition((-terrainSize.x * terrainScale.x) / 2, 0, (-terrainSize.y * terrainScale.z) / 2);
	terrain->SetBoxType(AABBBOX);
	objects.emplace_back(terrain);
	objects[0]->CreateCbvGPUDescriptorHandles();
	objects[0]->SetCbvGPUDescriptorHandle(handle);

	
}

D3D12_INPUT_LAYOUT_DESC TerrainShader::CreateInputLayout()
{
	UINT nInputElementDescs = 6;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];

	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[2] = { "TEXCOORD", 0, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[3] = { "TEXCOORD", 1, DXGI_FORMAT_R32G32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[4] = { "TEXNUMBER" , 0 , DXGI_FORMAT_R32_UINT , 0 , D3D12_APPEND_ALIGNED_ELEMENT , D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA , 0 };
	pd3dInputElementDescs[5] = { "NORMAL" , 0 , DXGI_FORMAT_R32G32B32_FLOAT , 0 , D3D12_APPEND_ALIGNED_ELEMENT , D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA , 0 };
	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_INPUT_LAYOUT_DESC TerrainShader::CreateNaviInputLayout()
{
	UINT nInputElementDescs = 2;
	D3D12_INPUT_ELEMENT_DESC *pd3dInputElementDescs = new D3D12_INPUT_ELEMENT_DESC[nInputElementDescs];

	pd3dInputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	pd3dInputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };

	D3D12_INPUT_LAYOUT_DESC d3dInputLayoutDesc;
	d3dInputLayoutDesc.pInputElementDescs = pd3dInputElementDescs;
	d3dInputLayoutDesc.NumElements = nInputElementDescs;

	return(d3dInputLayoutDesc);
}

D3D12_RASTERIZER_DESC TerrainShader::CreateAlphaRasterizerState()
{
	//FIllMode와 CullMode를 변경하여 그려지는 객체의 내부를 칠할지 말지 등을 결정할 수 있다.
	D3D12_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_SOLID;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(rasterizerDesc);
}

D3D12_RASTERIZER_DESC TerrainShader::CreateNaviRasterizerState()
{
	//FIllMode와 CullMode를 변경하여 그려지는 객체의 내부를 칠할지 말지 등을 결정할 수 있다.
	D3D12_RASTERIZER_DESC rasterizerDesc;
	::ZeroMemory(&rasterizerDesc, sizeof(D3D12_RASTERIZER_DESC));
	rasterizerDesc.FillMode = D3D12_FILL_MODE_WIREFRAME;
	rasterizerDesc.CullMode = D3D12_CULL_MODE_NONE;
	rasterizerDesc.FrontCounterClockwise = FALSE;
	rasterizerDesc.DepthBias = 0;
	rasterizerDesc.DepthBiasClamp = 0.0f;
	rasterizerDesc.SlopeScaledDepthBias = 0.0f;
	rasterizerDesc.DepthClipEnable = FALSE;
	rasterizerDesc.MultisampleEnable = FALSE;
	rasterizerDesc.AntialiasedLineEnable = FALSE;
	rasterizerDesc.ForcedSampleCount = 0;
	rasterizerDesc.ConservativeRaster = D3D12_CONSERVATIVE_RASTERIZATION_MODE_OFF;

	return(rasterizerDesc);
}

D3D12_SHADER_BYTECODE TerrainShader::CreateVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSTerrain", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreatePixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSTerrain", "ps_5_1", &pixelShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateDepthVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSTerrainDepthShader", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateDepthPixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSTerrainDepthShader", "ps_5_1", &pixelShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateAlphaVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSAlphaTerrain", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateAlphaPixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSAlphaTerrain", "ps_5_1", &pixelShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateHeightVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSHeightTerrain", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateHeightPixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSHeightTerrain", "ps_5_1", &pixelShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateNavitVertexShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSDiffused", "vs_5_1", &vertexShaderBlob));
}

D3D12_SHADER_BYTECODE TerrainShader::CreateNaviPixelShader()
{
	return(Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSDiffused", "ps_5_1", &pixelShaderBlob));
}

D3D12_BLEND_DESC TerrainShader::CreateAlphaBlendState()
{
	D3D12_BLEND_DESC blendDesc;
	::ZeroMemory(&blendDesc, sizeof(D3D12_BLEND_DESC));
	blendDesc.AlphaToCoverageEnable = FALSE;
	blendDesc.IndependentBlendEnable = FALSE;
	blendDesc.RenderTarget[0].BlendEnable = FALSE;
	blendDesc.RenderTarget[0].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[0].DestBlend = D3D12_BLEND_DEST_ALPHA;
	blendDesc.RenderTarget[0].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].DestBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[0].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[0].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[0].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;


	blendDesc.RenderTarget[1].BlendEnable = FALSE;
	blendDesc.RenderTarget[1].LogicOpEnable = FALSE;
	blendDesc.RenderTarget[1].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	blendDesc.RenderTarget[1].DestBlend = D3D12_BLEND_DEST_ALPHA;
	blendDesc.RenderTarget[1].BlendOp = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[1].SrcBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[1].DestBlendAlpha = D3D12_BLEND_ONE;
	blendDesc.RenderTarget[1].BlendOpAlpha = D3D12_BLEND_OP_ADD;
	blendDesc.RenderTarget[1].LogicOp = D3D12_LOGIC_OP_NOOP;
	blendDesc.RenderTarget[1].RenderTargetWriteMask = D3D12_COLOR_WRITE_ENABLE_ALL;
	return(blendDesc);
}

D3D12_DEPTH_STENCIL_DESC TerrainShader::CreateAlphaDepthStencilState()
{
	D3D12_DEPTH_STENCIL_DESC depthStencilDesc;
	::ZeroMemory(&depthStencilDesc, sizeof(D3D12_DEPTH_STENCIL_DESC));
	depthStencilDesc.DepthEnable = FALSE;
	depthStencilDesc.DepthWriteMask = D3D12_DEPTH_WRITE_MASK_ZERO;
	depthStencilDesc.DepthFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.StencilEnable = FALSE;
	depthStencilDesc.StencilReadMask = 0x00;
	depthStencilDesc.StencilWriteMask = 0x00;
	depthStencilDesc.FrontFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.FrontFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;
	depthStencilDesc.BackFace.StencilFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilDepthFailOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilPassOp = D3D12_STENCIL_OP_KEEP;
	depthStencilDesc.BackFace.StencilFunc = D3D12_COMPARISON_FUNC_NEVER;

	return(depthStencilDesc);
}

void TerrainShader::CreateShader(ID3D12Device *device, ID3D12RootSignature *pd3dGraphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 5;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	Shader::CreateShader(device, pd3dGraphicsRootSignature);
	CreatePipeLineStateAlpha(device, pd3dGraphicsRootSignature, TERRAINALPHA);
	CreatePipeLineStateDetph(device, pd3dGraphicsRootSignature); // 터레인의 뎁스값을 얻는 것 
	CreatePipeLineStateHeight(device, pd3dGraphicsRootSignature);
	CreatePipeLineStateNavi(device, pd3dGraphicsRootSignature);
	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}
void TerrainShader::CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList)
{
	UINT cbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
	cbGameObjects = ::CreateBufferResource(device, commandList, NULL, cbGameObjectBytes * objectsNum, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	cbGameObjects->Map(0, NULL, (void **)&cbMappedGameObjects);
}

void TerrainShader::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList)
{
	UINT cbGameObjectBytes = ((sizeof(CB_GAMEOBJECT_INFO) + 255) & ~255); //256의 배수
	XMFLOAT4X4 world;
	for (int j = 0; j < objects.size(); j++)
	{
		XMStoreFloat4x4(&world, XMMatrixTranspose(XMLoadFloat4x4(&objects[j]->GetWorld())));
		CB_GAMEOBJECT_INFO *mappedcbGameObject = (CB_GAMEOBJECT_INFO*)(cbMappedGameObjects + (j * cbGameObjectBytes));
		::memcpy(&mappedcbGameObject->world, &world, sizeof(XMFLOAT4X4));
	}
}

void TerrainShader::ReleaseShaderVariables()
{
	if (cbGameObjects) cbGameObjects->Unmap(0, NULL);
	if (cbGameObjects) cbGameObjects->Release();
}

void TerrainShader::ReleaseObjects()
{
	for (int i = 0; i < objects.size(); ++i)
	{
		objects[i]->Release();
	}
}

void TerrainShader::ReleaseUploadBuffers()
{
	for (int i = 0; i < objects.size(); ++i)
	{
		objects[i]->ReleaseUploadBuffers();
	}
}

void TerrainShader::Render(ID3D12GraphicsCommandList *commandList, Camera* camera)
{
	Shader::OnPrepareRender(commandList, pipeLineCounter);
	Shader::UpdateShaderVariables(commandList);
	UpdateShaderVariables(commandList);

	for (int i = 0; i < objects.size(); ++i)
	{
		objects[i]->TerrainRender(commandList, camera, terrainLayer);
	}



}

void TerrainShader::NaviRender(ID3D12GraphicsCommandList * commandList, Camera * camera)
{
	commandList->SetPipelineState(pipelineStates[4]);
	for (int i = 0; i < objects.size(); ++i)
	{
		objects[i]->NaviRender(commandList, camera);
	}

}

void TerrainShader::CreatePipeLineStateIndex(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature, int index)
{
	ID3DBlob* vertexShaderBlob = NULL, *pixelShaderBlob = NULL;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
	::ZeroMemory(&pipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	pipelineStateDesc.pRootSignature = graphicsRootSignature;
	pipelineStateDesc.VS = CreateVertexShader();
	pipelineStateDesc.PS = CreatePixelShader();
	pipelineStateDesc.RasterizerState = CreateRasterizerState();
	pipelineStateDesc.BlendState = CreateBlendState();
	pipelineStateDesc.DepthStencilState = CreateDepthStencilState();
	pipelineStateDesc.InputLayout = CreateInputLayout();
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[index]);
	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[]
		pipelineStateDesc.InputLayout.pInputElementDescs;

}

void TerrainShader::CreatePipeLineStateAlpha(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature, int index)
{

	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
	::ZeroMemory(&pipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	pipelineStateDesc.pRootSignature = graphicsRootSignature;
	pipelineStateDesc.VS = CreateAlphaVertexShader();
	pipelineStateDesc.PS = CreateAlphaPixelShader();
	pipelineStateDesc.RasterizerState = CreateAlphaRasterizerState();
	pipelineStateDesc.BlendState = CreateAlphaBlendState();
	pipelineStateDesc.DepthStencilState = CreateAlphaDepthStencilState();
	pipelineStateDesc.InputLayout = CreateInputLayout();
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.NumRenderTargets = 3;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pipelineStateDesc.RTVFormats[1] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pipelineStateDesc.RTVFormats[2] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[index]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[]
		pipelineStateDesc.InputLayout.pInputElementDescs;
}

void TerrainShader::CreatePipeLineStateDetph(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature)
{
	ID3DBlob* vertexShaderBlob = NULL, *pixelShaderBlob = NULL;

	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
	::ZeroMemory(&pipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	pipelineStateDesc.pRootSignature = graphicsRootSignature;
	pipelineStateDesc.VS = CreateDepthVertexShader();
	pipelineStateDesc.PS = CreateDepthPixelShader();
	pipelineStateDesc.RasterizerState = CreateRasterizerState();
	pipelineStateDesc.BlendState = CreateBlendState();
	pipelineStateDesc.DepthStencilState = CreateDepthStencilState();
	pipelineStateDesc.InputLayout = CreateInputLayout();
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[TERRAINDEPTH]);

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[]
		pipelineStateDesc.InputLayout.pInputElementDescs;
}

void TerrainShader::CreatePipeLineStateHeight(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
	::ZeroMemory(&pipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	pipelineStateDesc.pRootSignature = graphicsRootSignature;
	pipelineStateDesc.VS = CreateHeightVertexShader();
	pipelineStateDesc.PS = CreateHeightPixelShader();
	pipelineStateDesc.RasterizerState = CreateAlphaRasterizerState();
	pipelineStateDesc.BlendState = CreateBlendState();
	pipelineStateDesc.DepthStencilState = CreateAlphaDepthStencilState();
	pipelineStateDesc.InputLayout = CreateInputLayout();
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R32G32B32A32_FLOAT;
	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[TERRAINHEIGHT]);
}

void TerrainShader::CreatePipeLineStateNavi(ID3D12Device * device, ID3D12RootSignature * graphicsRootSignature)
{
	D3D12_GRAPHICS_PIPELINE_STATE_DESC pipelineStateDesc;
	::ZeroMemory(&pipelineStateDesc, sizeof(D3D12_GRAPHICS_PIPELINE_STATE_DESC));
	pipelineStateDesc.pRootSignature = graphicsRootSignature;
	pipelineStateDesc.VS = CreateNavitVertexShader();
	pipelineStateDesc.PS = CreateNaviPixelShader();
	pipelineStateDesc.RasterizerState = CreateNaviRasterizerState();
	pipelineStateDesc.BlendState = CreateBlendState();
	pipelineStateDesc.DepthStencilState = CreateAlphaDepthStencilState();
	pipelineStateDesc.InputLayout = CreateNaviInputLayout();
	pipelineStateDesc.SampleMask = UINT_MAX;
	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_TRIANGLE;
	pipelineStateDesc.NumRenderTargets = 1;
	pipelineStateDesc.RTVFormats[0] = DXGI_FORMAT_R8G8B8A8_UNORM;
	pipelineStateDesc.DSVFormat = DXGI_FORMAT_D32_FLOAT_S8X24_UINT;
	pipelineStateDesc.SampleDesc.Count = 1;
	pipelineStateDesc.Flags = D3D12_PIPELINE_STATE_FLAG_NONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[4]);

}



