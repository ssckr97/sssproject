#include "stdafx.h"
#include "Mesh.h"
#include "GameObject.h"
#define _WITH_APPROXIMATE_OPPOSITE_CORNER


Mesh::Mesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
}
Mesh::~Mesh()
{
	if (vertexBuffer) vertexBuffer->Release();
	if (indexBuffer) indexBuffer->Release();

	if (subMeshesNum > 0)
	{
		for (int i = 0; i < subMeshesNum; i++)
		{
			if (subSetIndexBuffers[i]) {
				subSetIndexBuffers[i]->Release();
				subSetIndexBuffers[i] = NULL;
			}
			if (subSetIndices[i]) {
				delete[] subSetIndices[i];
				subSetIndices[i] = NULL;
			}
		}
		if (subSetIndexBuffers) {
			delete[] subSetIndexBuffers;
			subSetIndexBuffers = NULL;
		}
		if (subSetIndexBufferViews) {
			delete[] subSetIndexBufferViews;
			subSetIndexBufferViews = NULL;
		}

		if (subSetIndicesNum) {
			delete[] subSetIndicesNum;
			subSetIndicesNum = NULL;
		}
		if (subSetIndices) {
			delete[] subSetIndices;
			subSetIndices = NULL;
		}
		subMeshesNum = 0;
	}
}

void Mesh::ReleaseUploadBuffers()
{
	if (vertexUploadBuffer) vertexUploadBuffer->Release();
	if (indexUploadBuffer) indexUploadBuffer->Release();
	vertexUploadBuffer = NULL;
	indexUploadBuffer = NULL;
	if ((subMeshesNum > 0) && subSetIndexUploadBuffers)
	{
		for (int i = 0; i < subMeshesNum; i++)
		{
			if (subSetIndexUploadBuffers[i]) subSetIndexUploadBuffers[i]->Release();
			subSetIndexUploadBuffers[i] = NULL;
		}
		if (subSetIndexUploadBuffers) delete[] subSetIndexUploadBuffers;
		subSetIndexUploadBuffers = NULL;
	}
};

void Mesh::Render(ID3D12GraphicsCommandList* commandList)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);
	commandList->IASetVertexBuffers(slot, 1, &vertexBufferView);
	if (indexBuffer)
	{
		commandList->IASetIndexBuffer(&indexBufferView);
		commandList->DrawIndexedInstanced(indicesNum, 1, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, 1, offset, 0);
	}
}

void Mesh::Render(ID3D12GraphicsCommandList* commandList, UINT instancesNum, std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBufferViews)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);

	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[1 + instancingBufferViews.size()];
	vertexBufferViews[0] = vertexBufferView;
	for (int i = 0; i < instancingBufferViews.size(); ++i)
	{
		vertexBufferViews[i + 1] = instancingBufferViews[i];
	}
	//정점 버퍼 뷰와 인스턴싱 버퍼 뷰를 입력-조립 단계에 설정한다.

	commandList->IASetVertexBuffers(slot, 1 + UINT(instancingBufferViews.size()), vertexBufferViews);
	if (indexBuffer)
	{
		commandList->IASetIndexBuffer(&indexBufferView);
		commandList->DrawIndexedInstanced(indicesNum, instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}

	delete[] vertexBufferViews;

}
//
//bool Mesh::CheckRayIntersect(XMVECTOR origin, XMVECTOR dir, float &pfNearHitDistance, XMFLOAT4X4 world)
//{
//	int nIntersections = 0;
//	if (!Texturedvertices)
//		return 0;
//	BYTE *pbPositions = (BYTE *)Texturedvertices;
//	for (int i = 0; i < primitiveNum; ++i) {
//		XMFLOAT3* v0 = (XMFLOAT3 *)(pbPositions + ((indices) ?
//			(indices[(i*pickOffset) + 0]) : ((i*pickOffset) + 0)) * stride);
//		XMVECTOR worldv0 = XMLoadFloat3(&Vector3::TransformCoord(*v0, world));
//
//		XMFLOAT3* v1 = (XMFLOAT3 *)(pbPositions + ((indices) ?
//			(indices[(i*pickOffset) + 1]) : ((i*pickOffset) + 1)) * stride);
//		XMVECTOR worldv1 = XMLoadFloat3(&Vector3::TransformCoord(*v1, world));
//
//		XMFLOAT3* v2 = (XMFLOAT3 *)(pbPositions + ((indices) ?
//			(indices[(i*pickOffset) + 2]) : ((i*pickOffset) + 2)) * stride);
//		XMVECTOR worldv2 = XMLoadFloat3(&Vector3::TransformCoord(*v2, world));
//
//		float fHitDistance;
//		BOOL bIntersected = TriangleTests::Intersects(origin, dir, worldv0,
//			worldv1, worldv2, fHitDistance);
//		if (bIntersected)
//		{
//			if (fHitDistance < pfNearHitDistance)
//			{
//				pfNearHitDistance = fHitDistance;
//			}
//			nIntersections++;
//		}
//	}
//	return(nIntersections);
//
//
//}

TriangleMesh::TriangleMesh(ID3D12Device* device, ID3D12GraphicsCommandList* commandList) : Mesh(device, commandList)
{
	verticesNum = 3;
	int stride = sizeof(DiffusedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	//삼각형 메쉬를 정의한다.

	DiffusedVertex vertices[3];
	vertices[0] = DiffusedVertex(XMFLOAT3(0.0f, 0.5f, 0.0f), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[1] = DiffusedVertex(XMFLOAT3(0.5f, -0.5f, 0.0f), XMFLOAT4(0.0f, 1.0f, 0.0f, 1.0f));
	vertices[2] = DiffusedVertex(XMFLOAT3(-0.5f, -0.5f, 0.0f), XMFLOAT4(Colors::Blue));
	/*정점(삼각형의 꼭지점)의 색상은 시계방향 순서대로 빨간색, 녹색, 파란색으로 지정한다. RGBA(Red, Green, Blue,
	Alpha) 4개의 파라메터를 사용하여 색상을 표현한다. 각 파라메터는 0.0~1.0 사이의 실수값을 가진다.*/

	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	//삼각형 메쉬를 리소스(정점 버퍼)로 생성한다.

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
	//정점 버퍼 뷰를 생성한다.
}

CubeMeshDiffused::CubeMeshDiffused(ID3D12Device* device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth) : Mesh(device, commandList)
{
	verticesNum = 8;
	int stride = sizeof(DiffusedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	float halfX = width * 0.5f, halfY = height * 0.5f, halfZ = depth * 0.5f;

	DiffusedVertex vertices[8];
	vertices[0] = DiffusedVertex(XMFLOAT3(-halfX, +halfY, -halfZ), RANDOM_COLOR);
	vertices[1] = DiffusedVertex(XMFLOAT3(+halfX, +halfY, -halfZ), RANDOM_COLOR);
	vertices[2] = DiffusedVertex(XMFLOAT3(+halfX, +halfY, +halfZ), RANDOM_COLOR);
	vertices[3] = DiffusedVertex(XMFLOAT3(-halfX, +halfY, +halfZ), RANDOM_COLOR);
	vertices[4] = DiffusedVertex(XMFLOAT3(-halfX, -halfY, -halfZ), RANDOM_COLOR);
	vertices[5] = DiffusedVertex(XMFLOAT3(+halfX, -halfY, -halfZ), RANDOM_COLOR);
	vertices[6] = DiffusedVertex(XMFLOAT3(+halfX, -halfY, +halfZ), RANDOM_COLOR);
	vertices[7] = DiffusedVertex(XMFLOAT3(-halfX, -halfY, +halfZ), RANDOM_COLOR);
	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum,
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;

	indicesNum = 36;
	UINT indices[36];
	indices[0] = 3; indices[1] = 1; indices[2] = 0;
	indices[3] = 2; indices[4] = 1; indices[5] = 3;
	indices[6] = 0; indices[7] = 5; indices[8] = 4;
	indices[9] = 1; indices[10] = 5; indices[11] = 0;
	indices[12] = 3; indices[13] = 4; indices[14] = 7;
	indices[15] = 0; indices[16] = 4; indices[17] = 3;
	indices[18] = 1; indices[19] = 6; indices[20] = 5;
	indices[21] = 2; indices[22] = 6; indices[23] = 1;
	indices[24] = 2; indices[25] = 7; indices[26] = 6;
	indices[27] = 3; indices[28] = 7; indices[29] = 2;
	indices[30] = 6; indices[31] = 4; indices[32] = 5;
	indices[33] = 7; indices[34] = 4; indices[35] = 6;

	indexBuffer = ::CreateBufferResource(device, commandList, indices, sizeof(UINT) * indicesNum,
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &indexUploadBuffer);

	indexBufferView.BufferLocation = indexBuffer->GetGPUVirtualAddress();
	indexBufferView.Format = DXGI_FORMAT_R32_UINT;
	indexBufferView.SizeInBytes = sizeof(UINT) * indicesNum;
}

CubeMeshDiffused::~CubeMeshDiffused()
{
}

CubeMeshIlluminatedTextured::CubeMeshIlluminatedTextured(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth) : MeshIlluminated(device, commandList)
{
	verticesNum = 36;
	int stride = sizeof(IlluminatedTexturedVertex);
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float x = width * 0.5f, y = height * 0.5f, z = depth * 0.5f;

	IlluminatedTexturedVertex vertices[36];
	XMFLOAT3 position[36];
	XMFLOAT3 nomals[36];
	int i = 0;

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));

	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	vertices[i++] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	vertices[i] = IlluminatedTexturedVertex(position[i] = XMFLOAT3(+x, -y, -z), XMFLOAT2(0.0f, 1.0f));

	for (int j = 0; j < 36; ++j) nomals[j] = XMFLOAT3(0.0f, 0.0f, 0.0f);

	CalculateVertexNormals(nomals, position, verticesNum, NULL, 0);

	for (int j = 0; j < 36; ++j) vertices[j].normal = nomals[j];

	vertexBuffer = CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}

CubeMeshIlluminatedTextured::~CubeMeshIlluminatedTextured()
{
}

CubeMeshNormalMapTextured::CubeMeshNormalMapTextured(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth) : StandardMesh(device, commandList)
{
	verticesNum = 36;
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	float x = width * 0.5f, y = height * 0.5f, z = depth * 0.5f;

	positions = new XMFLOAT3[verticesNum];
	textureCoords0 = new XMFLOAT2[verticesNum];
	normals = new XMFLOAT3[verticesNum];
	biTangents = new XMFLOAT3[verticesNum];
	tangents = new XMFLOAT3[verticesNum];

	int i = 0;

	positions[i] = XMFLOAT3(-x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);

	positions[i] = XMFLOAT3(-x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);

	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);

	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);

	positions[i] = XMFLOAT3(-x, -y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);

	positions[i] = XMFLOAT3(-x, -y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);

	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);

	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, -y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);

	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(-x, +y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);

	positions[i] = XMFLOAT3(-x, +y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(-x, -y, -z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(-x, -y, +z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);

	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, +y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);

	positions[i] = XMFLOAT3(+x, +y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
	positions[i] = XMFLOAT3(+x, -y, +z), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
	positions[i] = XMFLOAT3(+x, -y, -z), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);

	CalculateTriangleListTBNs(verticesNum, positions, textureCoords0, tangents, biTangents, normals);

	positionBuffer = CreateBufferResource(device, commandList, positions, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &positionUploadBuffer);
	positionBufferView.BufferLocation = positionBuffer->GetGPUVirtualAddress();
	positionBufferView.StrideInBytes = sizeof(XMFLOAT3);
	positionBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	type |= VertexPosition;
	vertexSize++;

	textureCoord0Buffer = CreateBufferResource(device, commandList, textureCoords0, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord0UploadBuffer);
	textureCoord0BufferView.BufferLocation = textureCoord0Buffer->GetGPUVirtualAddress();
	textureCoord0BufferView.StrideInBytes = sizeof(XMFLOAT2);
	textureCoord0BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
	type |= VertexTexCoord0;
	vertexSize++;

	normalBuffer = CreateBufferResource(device, commandList, normals, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &normalUploadBuffer);
	normalBufferView.BufferLocation = normalBuffer->GetGPUVirtualAddress();
	normalBufferView.StrideInBytes = sizeof(XMFLOAT3);
	normalBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	type |= VertexNormal;
	vertexSize++;

	biTangentBuffer = CreateBufferResource(device, commandList, biTangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &biTangentUploadBuffer);
	biTangentBufferView.BufferLocation = biTangentBuffer->GetGPUVirtualAddress();
	biTangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
	biTangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	type |= VertexTangent;
	vertexSize++;

	tangentBuffer = CreateBufferResource(device, commandList, tangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &tangentUploadBuffer);
	tangentBufferView.BufferLocation = tangentBuffer->GetGPUVirtualAddress();
	tangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
	tangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	vertexSize++;
}

CubeMeshNormalMapTextured::~CubeMeshNormalMapTextured()
{
}

void CubeMeshNormalMapTextured::CalculateTriangleListTBNs(int verticesNum, XMFLOAT3 *positions, XMFLOAT2 *texCoords, XMFLOAT3 *tangents, XMFLOAT3 *biTangents, XMFLOAT3 *normals)
{
	float u10, v10, u20, v20, x10, x20, y10, y20, z10, z20;
	for (int i = 0; i < verticesNum; i += 3)
	{
		u10 = texCoords[i + 1].x - texCoords[i].x;
		v10 = texCoords[i + 1].y - texCoords[i].y;
		u20 = texCoords[i + 2].x - texCoords[i].x;
		v20 = texCoords[i + 2].y - texCoords[i].y;
		x10 = positions[i + 1].x - positions[i].x;
		y10 = positions[i + 1].y - positions[i].y;
		z10 = positions[i + 1].z - positions[i].z;
		x20 = positions[i + 2].x - positions[i].x;
		y20 = positions[i + 2].y - positions[i].y;
		z20 = positions[i + 2].z - positions[i].z;
		float invDeterminant = 1.0f / (u10 * v20 - u20 * v10);
		tangents[i] = tangents[i + 1] = tangents[i + 2] = XMFLOAT3(invDeterminant * (v20 * x10 - v10 * x20), invDeterminant * (v20 * y10 - v10 * y20), invDeterminant * (v20 * z10 - v10 * z20));
		biTangents[i] = biTangents[i + 1] = biTangents[i + 2] = XMFLOAT3(invDeterminant * (-u20 * x10 + u10 * x20), invDeterminant * (-u20 * y10 + u10 * y20), invDeterminant * (-u20 * z10 + u10 * z20));
		normals[i] = Vector3::CrossProduct(tangents[i], biTangents[i], true);
		normals[i + 1] = Vector3::CrossProduct(tangents[i + 1], biTangents[i + 1], true);
		normals[i + 2] = Vector3::CrossProduct(tangents[i + 2], biTangents[i + 2], true);

	}
}

AirplaneMeshDiffused::AirplaneMeshDiffused(ID3D12Device *device, ID3D12GraphicsCommandList *commandList,
	float width, float height, float depth, XMFLOAT4 color) : Mesh(device, commandList)
{
	verticesNum = 24 * 3;
	int stride = sizeof(DiffusedVertex);
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	float halfX = width * 0.5f, halfY = height * 0.5f, halfZ = depth * 0.5f;
	//위의 그림과 같은 비행기 메쉬를 표현하기 위한 정점 데이터이다.
	DiffusedVertex vertices[24 * 3];
	float x1 = halfX * 0.2f, y1 = halfY * 0.2f, x2 = halfX * 0.1f, y3 = halfY * 0.3f, y2 = ((y1 - (halfY - y3)) / x1) * x2 + (halfY - y3);
	int i = 0;
	//비행기 메쉬의 위쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));

	//비행기 메쉬의 아래쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));

	//비행기 메쉬의 오른쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));

	//비행기 메쉬의 뒤쪽/오른쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, +halfZ), Vector4::Add(color, RANDOM_COLOR));

	//비행기 메쉬의 왼쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +(halfY + y3), +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x2, +y2, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));

	//비행기 메쉬의 뒤쪽/왼쪽 면
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, 0.0f, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x1, -y1, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, +halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-halfX, -y3, -halfZ), Vector4::Add(color, RANDOM_COLOR));
	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum,
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}
AirplaneMeshDiffused::~AirplaneMeshDiffused()
{
}

HeightMapGridMesh::HeightMapGridMesh(ID3D12Device *device,
	ID3D12GraphicsCommandList *commandList, int xStart, int zStart, int width, int
	length, XMFLOAT3 scale, XMFLOAT4 color, void *context) : Mesh(device, commandList)
{

	heightMapLayer = new HeightMapLayer[TERRAINLAYERNUM];
	width = width;
	length = length;
	verticesNum = width * length;

	offset = 0;
	slot = 0;
	stride = sizeof(Diffused2TexturedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	this->width = width;
	this->length = length;
	this->scale = scale;
	pickOffset = (primitiveTopology == D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ? 3 : 1;

	int cxHeightMap = 257;
	int czHeightMap = 257;

	diffused2TexturedVerices = new Diffused2TexturedVertex[verticesNum];

	subIndex.reserve(verticesNum * 2);
	bool isCreateBoundingZ = false;
	int boundingCount{ 0 };
	boundingWidth = 16;
	boundingLength = 16;

	float fHeight = 0.0f, fMinHeight = +FLT_MAX, fMaxHeight = -FLT_MAX;
	int texNumber = 0;
	for (int i = 0, z = zStart; z < (zStart + this->length); z++)
	{
		if (z % boundingLength == 0) isCreateBoundingZ = true;

		for (int x = xStart; x < (xStart + this->width); x++, i++)
		{
		
			XMFLOAT3 position = XMFLOAT3((x * this->scale.x), 0.0f, (z * this->scale.z));
			XMFLOAT4 diffuseColor = XMFLOAT4(1.0f, 1.0f, 1.0f, 1.0f);
			XMFLOAT2 texCoord0 = XMFLOAT2(float(x) / float(cxHeightMap - 1), float(czHeightMap - 1 - z) / float(czHeightMap - 1));
			XMFLOAT2 texCoord1 = XMFLOAT2(float(x) / float(this->scale.x * 0.5f * 8.0f), float(z) / float(this->scale.z*0.5f * 8.0f));
			diffused2TexturedVerices[i] = Diffused2TexturedVertex(position, diffuseColor, texCoord0, texCoord1, (texNumber));
			if (fHeight < fMinHeight) fMinHeight = fHeight;
			if (fHeight > fMaxHeight) fMaxHeight = fHeight;
			int indexX = x - xStart;
			int indexZ = z - zStart;
			if (texNumber == 0 && x != xStart + this->width - 1 && z != zStart + this->length - 1) {
				subIndex.emplace_back(indexX + indexZ * width, indexX + (indexZ * width) + width, indexX + (indexZ * width) + 1);
				subIndex.emplace_back(indexX + indexZ * width + width, indexX + indexZ * width + width + 1, indexX + indexZ * width + 1);
			}
			if (isCreateBoundingZ) {
				if (x == (xStart + this->width) - 1) {
					isCreateBoundingZ = false;
					continue;
				}
				if (x % boundingWidth == 0 && z != (zStart + this->length) - 1) {
					terrainBoundings[boundingCount].Center = XMFLOAT3((x * scale.x) + scale.x * ((float)boundingWidth / 2), 0, (z * scale.z) + scale.z * ((float)boundingLength / 2));
					terrainBoundings[boundingCount].Extents = XMFLOAT3(scale.x * ((float)boundingWidth / 2), 300, scale.z * ((float)boundingLength / 2));
					boundingVertexIndex[boundingCount] = i;
					boundingCount++;
				}

			}

		}

	}

	
	
	//노말 계산
	for (int i = 0; i < verticesNum; ++i) {
		diffused2TexturedVerices[i].normal = calNormal(i);
		tempd.emplace_back(diffused2TexturedVerices[i]);
	}

	
	boundingTree = new int*[TERRAINBOUNDINGS];

	for (int i = 0; i < TERRAINBOUNDINGS; ++i) {
		boundingTree[i] = new int[8];
		//위 
		if (i + boundingWidth < TERRAINBOUNDINGS)
			boundingTree[i][UPTREE] = boundingVertexIndex[i + boundingWidth];
		else
			boundingTree[i][UPTREE] = NULL;
		//아래
		if (i - boundingWidth >= 0)
			boundingTree[i][DOWNTREE] = boundingVertexIndex[i - boundingWidth];
		else
			boundingTree[i][DOWNTREE] = NULL;

		//왼쪽
		if (i % boundingWidth != 0)
			boundingTree[i][LEFTTREE] = boundingVertexIndex[i - 1];
		else
			boundingTree[i][LEFTTREE] = NULL;
		//오른쪽
		if ((i + 1) % boundingWidth != 0)
			boundingTree[i][RIGHTTREE] = boundingVertexIndex[i + 1];
		else
			boundingTree[i][RIGHTTREE] = NULL;
		//왼쪽위 대각선
		if (i + (boundingWidth - 1) < TERRAINBOUNDINGS && (i % boundingWidth) != 0)
			boundingTree[i][UPLEFTTREE] = boundingVertexIndex[i + (boundingWidth - 1)];
		else
			boundingTree[i][UPLEFTTREE] = NULL;
		//오른위 대각선
		if (((i + 1) % boundingWidth != 0) && i + (boundingWidth + 1) < TERRAINBOUNDINGS)
			boundingTree[i][UPRIGHTTREE] = boundingVertexIndex[i + (boundingWidth + 1)];
		else
			boundingTree[i][UPRIGHTTREE] = NULL;
		//왼쪽아래 대각선
		if (i - (boundingWidth + 1) >= 0 && (i % boundingWidth) != 0)
			boundingTree[i][DOWNLEFTTREE] = boundingVertexIndex[i - (boundingWidth + 1)];
		else
			boundingTree[i][DOWNLEFTTREE] = NULL;
		//오른아래 대각선 
		if ((i + 1) % boundingWidth != 0 && i - (boundingWidth - 1) >= 0)
			boundingTree[i][DOWNRIGHTTREE] = boundingVertexIndex[i - (boundingWidth - 1)];
		else
			boundingTree[i][DOWNRIGHTTREE] = NULL;
	}


	aabbBox.Center = XMFLOAT3(xStart * scale.x + (width * scale.x / 2), 0.0f, zStart * scale.z + (length * scale.z / 2));
	aabbBox.Extents = XMFLOAT3(width * scale.x / 2, 300.0f, length * scale.z / 2);
	indicesNum = subIndex.size() * 3;
	indices = new UINT[indicesNum];

	if (indicesNum > 0) primitiveNum = (primitiveTopology ==
		D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ? (indicesNum / 3) : (indicesNum - 2);

	for (int i = 0; i < subIndex.size(); ++i) {
		indices[i * 3 + 0] = subIndex[i].GetIndex()[0];
		indices[i * 3 + 1] = subIndex[i].GetIndex()[1];
		indices[i * 3 + 2] = subIndex[i].GetIndex()[2];
	}

	heightMapLayer[0].BuildLayer(device, commandList, width, length, diffused2TexturedVerices, subIndex, verticesNum, 0);


	


}



void HeightMapGridMesh::Render(ID3D12GraphicsCommandList * commandList, int layerIndex)
{
	if (layerIndex != TERRAINLAYERNUM) {
		if (heightMapLayer[layerIndex].GetIsBuild())
			heightMapLayer[layerIndex].Render(commandList);
	}
	else {
		for (int i = 0; i < TERRAINLAYERNUM; ++i) {
			if (heightMapLayer[i].GetIsBuild()) {
				heightMapLayer[i].Render(commandList);
			}
		}
	}

}


HeightMapGridMesh::~HeightMapGridMesh()
{
	if (diffused2TexturedVerices)
		delete[] diffused2TexturedVerices;

	for (int i = 0; i < TERRAINBOUNDINGS; ++i)
	{
		delete boundingTree[i];
	}
	delete[] boundingTree;

	if (heightMapLayer)
	{
		for (int i = 0; i < TERRAINLAYERNUM; ++i) {
			heightMapLayer[i].ReleaseBuffers();
		}
	}

	if (heightMapLayer) {
		delete[] heightMapLayer;
	}
}

bool HeightMapGridMesh::CheckRayIntersect(XMVECTOR origin, XMVECTOR dir, float & pfNearHitDistance, XMFLOAT4X4 world, int bIdx)
{
	int nIntersections = 0;

	BYTE *pbPositions = (BYTE *)diffused2TexturedVerices;

	for (int z = 0; z < boundingLength; ++z) {
		for (int x = 0; x < boundingWidth; ++x) {
			//삼각형을 구성해야한다. 점하나당 삼각형 2개가 나온다 .
			auto v0 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + z * (width)].position, world));
			auto v1 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + (z + 1) * (width)].position, world));
			auto v2 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + z * (width)+1].position, world));

			float fHitDistance = FLT_MAX;
			BOOL bIntersected = TriangleTests::Intersects(origin, dir, v0,
				v1, v2, fHitDistance);
			if (bIntersected)
			{
				if (fHitDistance < pfNearHitDistance)
				{
					pfNearHitDistance = fHitDistance;
				}
				nIntersections++;
			}


			v0 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + (z + 1) * (width)].position, world));
			v1 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + (z + 1) * (width)+1].position, world));
			v2 = XMLoadFloat3(&Vector3::TransformCoord(diffused2TexturedVerices[bIdx + x + z * (width)+1].position, world));

			fHitDistance = FLT_MAX;
			bIntersected = TriangleTests::Intersects(origin, dir, v0,
				v1, v2, fHitDistance);
			if (bIntersected)
			{
				if (fHitDistance < pfNearHitDistance)
				{
					pfNearHitDistance = fHitDistance;
				}
				nIntersections++;
			}

		}
	}




	if (pfNearHitDistance < FLT_MAX - 1.0f) {
		return true;
	}

	else {
		return false;
	}
}

float HeightMapGridMesh::OnGetHeight(int x, int z, void *context)
{
	HeightMapImage *heightMapImage = (HeightMapImage *)context;
	BYTE *heightMapPixels = heightMapImage->GetHeightMapPixels();
	XMFLOAT3 scale = heightMapImage->GetScale();
	int width = heightMapImage->GetHeightMapWidth();

	float height = heightMapPixels[x + (z*width)] * scale.y;
	return(height);
}

XMFLOAT4 HeightMapGridMesh::OnGetColor(int x, int z, void *context)
{
	XMFLOAT3 lightDirection = XMFLOAT3(-1.0f, 1.0f, 1.0f);

	lightDirection = Vector3::Normalize(lightDirection);
	HeightMapImage *heightMapImage = (HeightMapImage *)context;
	XMFLOAT3 scale = heightMapImage->GetScale();
	XMFLOAT4 incidentLightColor(0.9f, 0.8f, 0.4f, 1.0f);

	float lightScale = Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x, z), lightDirection);
	lightScale += Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x + 1, z), lightDirection);
	lightScale += Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x + 1, z + 1), lightDirection);
	lightScale += Vector3::DotProduct(heightMapImage->GetHeightMapNormal(x, z + 1), lightDirection);
	lightScale = (lightScale / 4.0f) + 0.05f;


	if (lightScale > 1.0f) lightScale = 1.0f;
	if (lightScale < 0.25f) lightScale = 0.25f;

	XMFLOAT4 color = Vector4::Multiply(lightScale, incidentLightColor);
	return(color);
}

void HeightMapGridMesh::ChangeVertexHeight(XMFLOAT3 LocalPickPlace, float frameTime, float CircleSize, int RadioFlags, float maxHeight, float minHeight, float flatHeight, std::vector<int> bindices, int shape)
{

	
	for (int k = 0; k < bindices.size(); ++k) {

		for (int z = 0; z < boundingLength; ++z) {
			for (int x = 0; x < boundingWidth; ++x) {
				int i = bindices[k] + x + z * (width);
				XMFLOAT2 Lengthxy;
				Lengthxy.x = diffused2TexturedVerices[i].position.x - LocalPickPlace.x;
				Lengthxy.y = diffused2TexturedVerices[i].position.z - LocalPickPlace.z;

				float Length = sqrt((Lengthxy.x * Lengthxy.x) + (Lengthxy.y * Lengthxy.y));

				if (RadioFlags == UPTERRAINHEIGHT || RadioFlags == DOWNTERRAINHEIGHT) {
					if (shape == CIRCLE) {
						XMFLOAT3 tempPoint = diffused2TexturedVerices[i].position;
						if (RadioFlags == UPTERRAINHEIGHT) {

							if (Length < CircleSize &&  diffused2TexturedVerices[i].position.y < maxHeight) {
								tempPoint.y = tempPoint.y + (((CircleSize - Length) / CircleSize) * CIRCLESPEED * frameTime);
								diffused2TexturedVerices[i].position.y = tempPoint.y;
							}
						}
						if (RadioFlags == DOWNTERRAINHEIGHT) {
							if (Length < CircleSize &&  diffused2TexturedVerices[i].position.y > minHeight) {
								tempPoint.y = tempPoint.y - (((CircleSize - Length) / CircleSize) * CIRCLESPEED * frameTime);
								diffused2TexturedVerices[i].position = tempPoint;
							}
						}
					}
					else if (shape == SQURE) {
						XMFLOAT3 pos = LocalPickPlace;
						XMFLOAT4 rect = { pos.z + CircleSize , pos.x + CircleSize , pos.x - CircleSize , pos.z - CircleSize }; // top /right /left /bottom
						XMFLOAT3 dpos = diffused2TexturedVerices[i].position;
						if (RadioFlags == UPTERRAINHEIGHT) {

							if (rect.z <= dpos.x && dpos.x <= rect.y  &&  rect.x >= dpos.z && dpos.z >= rect.w  && diffused2TexturedVerices[i].position.y < maxHeight) {
								dpos.y = dpos.y + SQURESPEED * frameTime;
								diffused2TexturedVerices[i].position.y = dpos.y;
							}
						}
						if (RadioFlags == DOWNTERRAINHEIGHT) {
							if (rect.z <= dpos.x && dpos.x <= rect.y  &&  rect.x >= dpos.z && dpos.z >= rect.w  && diffused2TexturedVerices[i].position.y < maxHeight) {
								dpos.y = dpos.y - SQURESPEED * frameTime;
								diffused2TexturedVerices[i].position.y = dpos.y;
							}
						}
					}
					else if (shape == NOISE) {
						XMFLOAT3 pos = LocalPickPlace;
						XMFLOAT4 rect = { pos.z + CircleSize , pos.x + CircleSize , pos.x - CircleSize , pos.z - CircleSize }; // top /right /left /bottom
						XMFLOAT3 dpos = diffused2TexturedVerices[i].position;
						if (RadioFlags == UPTERRAINHEIGHT) {
							if (rect.z <= dpos.x && dpos.x <= rect.y  &&  rect.x >= dpos.z && dpos.z >= rect.w  && diffused2TexturedVerices[i].position.y < maxHeight) {
								dpos.y = dpos.y + (rand() % NOISESPEED) * frameTime;
								diffused2TexturedVerices[i].position.y = dpos.y;
							}
						}
						if (RadioFlags == DOWNTERRAINHEIGHT) {
							if (rect.z <= dpos.x && dpos.x <= rect.y  &&  rect.x >= dpos.z && dpos.z >= rect.w  && diffused2TexturedVerices[i].position.y < maxHeight) {
								dpos.y = dpos.y + (rand() % NOISESPEED) * frameTime;
								diffused2TexturedVerices[i].position.y = dpos.y;
							}
						}


					}
				}
				if (RadioFlags == FLATTERRAINHEIGHT) {
					if (Length < CircleSize) {
						diffused2TexturedVerices[i].position.y = flatHeight;
					}

				}
			}

		}
	}
	for (int k = 0; k < bindices.size(); ++k) {

		for (int z = 0; z < boundingLength; ++z) {
			for (int x = 0; x < boundingWidth; ++x) {
				int i = bindices[k] + x + z * (width);
				diffused2TexturedVerices[i].normal = calNormal(i);
			}
		}
	}

	for (int i = 0; i < TERRAINLAYERNUM; ++i) {
			heightMapLayer[0].ChangeLayerHeight(diffused2TexturedVerices, verticesNum);
	}


}

void HeightMapGridMesh::ChangeVertexTexture(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT3 LocalPickPlace, float frameTime, float CircleSize, int textureNumber, int bIdx )
{

	for (int z = 0; z < boundingLength; ++z) {
		for (int x = 0; x < boundingWidth; ++x) {
			int i = bIdx + x + z * (width);
			XMFLOAT2 Lengthxy;
			Lengthxy.x = diffused2TexturedVerices[i].position.x - LocalPickPlace.x;
			Lengthxy.y = diffused2TexturedVerices[i].position.z - LocalPickPlace.z;

			float Length = sqrt((Lengthxy.x * Lengthxy.x) + (Lengthxy.y * Lengthxy.y));

			if (Length < CircleSize) {
				diffused2TexturedVerices[i].texNumber = textureNumber;
			}

		}
	}
	int Idx = 0;
	for (int i = 0; i < TERRAINBOUNDINGS; ++i) {
		if (boundingVertexIndex[i] == bIdx) {
			Idx = i;
			break;
		}
	}

	for (int i = 0; i < 8; ++i) {
		if (boundingTree[Idx][i] != NULL) {
			for (int z = 0; z < boundingLength; ++z) {
				for (int x = 0; x < boundingWidth; ++x) {
					int j = boundingTree[Idx][i] + x + z * (width);
					XMFLOAT2 Lengthxy;
					Lengthxy.x = diffused2TexturedVerices[j].position.x - LocalPickPlace.x;
					Lengthxy.y = diffused2TexturedVerices[j].position.z - LocalPickPlace.z;

					float Length = sqrt((Lengthxy.x * Lengthxy.x) + (Lengthxy.y * Lengthxy.y));



					if (Length < CircleSize) {
						diffused2TexturedVerices[j].texNumber = textureNumber;
					}
				}
			}
		}
	}

	for (int i = 0; i < 8; ++i) {
		heightMapLayer[0].ChangeLayerTexNum( diffused2TexturedVerices , verticesNum);
	}
}

void HeightMapGridMesh::SmoothingVertexHeight(XMFLOAT3 LocalPickPlace, float frameTime, float CircleSize, int RadioFlags, std::vector<int> bIndices)
{
	int size = 0;

	std::vector<HeightMapInForm> subHeight;
	std::vector<HeightMapInForm> tempsubHeight;

	for (int k = 0; k < bIndices.size(); ++k) {
		for (int z = 0; z < boundingLength; ++z) {
			for (int x = 0; x < boundingWidth; ++x) {
				int i = bIndices[k] + x + z * (width);
				XMFLOAT2 Lengthxy;
				Lengthxy.x = diffused2TexturedVerices[i].position.x - LocalPickPlace.x;
				Lengthxy.y = diffused2TexturedVerices[i].position.z - LocalPickPlace.z;

				float Length = sqrt((Lengthxy.x * Lengthxy.x) + (Lengthxy.y * Lengthxy.y));
				if (Length < CircleSize) {
					HeightMapInForm heigtMapInform(diffused2TexturedVerices[i].position, i);
					subHeight.emplace_back(heigtMapInform);
					tempsubHeight.emplace_back(heigtMapInform);
				}

			}
		}
	}

	//y 값 계산 

	for (int i = 0; i < subHeight.size(); ++i) {
		SmoothingMesh(i, width, length, subHeight, tempsubHeight);
		diffused2TexturedVerices[tempsubHeight[i].idx].position.y = tempsubHeight[i].position.y;
	}


	//노멀 계산 
	for (int k = 0; k < bIndices.size(); ++k) {
		for (int z = 0; z < boundingLength; ++z) {
			for (int x = 0; x < boundingWidth; ++x) {
				int i = bIndices[k] + x + z * (width);
				diffused2TexturedVerices[i].normal = calNormal(i);
			}
		}
	}




	for (int i = 0; i < TERRAINLAYERNUM; ++i) {
			heightMapLayer[0].ChangeLayerHeight(diffused2TexturedVerices, verticesNum);
	}
}

void HeightMapGridMesh::ResetVertexHeight()
{
	for (int i = 0; i < verticesNum; ++i)
		diffused2TexturedVerices[i].position.y = 0;
	for (int i = 0; i < TERRAINLAYERNUM; ++i) {
			heightMapLayer[0].ChangeLayerHeight(diffused2TexturedVerices, verticesNum);
	}
}

void HeightMapGridMesh::ResetVertexTexNum()
{
	for (int i = 0; i < verticesNum; ++i)
		diffused2TexturedVerices[i].texNumber = 0;
	for (int i = 1; i < TERRAINLAYERNUM; ++i) {
		heightMapLayer[i].SetIsBuild(false);
	}
	heightMapLayer[0].ChangeLayerTexNum(diffused2TexturedVerices , 0);
}

void HeightMapGridMesh::ResetVertexPosition(XMFLOAT3 scale, HeightMapTerrain* terrain)
{
	/*POINT terrainSize{ 257.0f , 257.0f };
	POINT terrainBlockSize{ 16.0f, 16.0f };

	int cxQuadsPerBlock = terrainBlockSize.x - 1;
	int czQuadsPerBlock = terrainBlockSize.y - 1;

	long cxBlocks = (terrainSize.x - 1) / cxQuadsPerBlock;
	long czBlocks = (terrainSize.y - 1) / czQuadsPerBlock;*/

	for (int i = 0; i < verticesNum; ++i) {
		float tempX = diffused2TexturedVerices[i].position.x / terrain->GetScale().x;
		float tempZ = diffused2TexturedVerices[i].position.z / terrain->GetScale().z;

		diffused2TexturedVerices[i].position.x = (tempX)* scale.x;
		diffused2TexturedVerices[i].position.y = 0;
		diffused2TexturedVerices[i].position.z = (tempZ)* scale.z;

		diffused2TexturedVerices->texCoord1 = XMFLOAT2(float(tempX) / float(scale.x*0.5f), float(tempZ) / float(scale.z*0.5f));
	}

	heightMapLayer[0].ChangeLayer(width, length, diffused2TexturedVerices, subIndex, verticesNum, indicesNum);
}

XMFLOAT3 HeightMapGridMesh::calNormal(int index)
{
	bool isleftup = false;
	bool isupright = false;
	bool isrightdown = false;
	bool isdownleft = false;

	XMFLOAT3 leftupNormal{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 uprightNormal{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 rightdownNormal{ 0.0f , 0.0f , 0.0f };
	XMFLOAT3 downleftNormal{ 0.0f , 0.0f , 0.0f };
	float bias = 1.0;
	//4개의 삼각형의 노멀을 구한다 
	auto& d2t = diffused2TexturedVerices;
	if (index % width != 0 && index + width < verticesNum) // 왼쪽 위 
	{
		//왼
		auto v1 = Vector3::Subtract(d2t[index - 1].position, d2t[index].position);
		//위
		auto v2 = Vector3::Subtract(d2t[index + width].position, d2t[index].position);
		leftupNormal = Vector3::CrossProduct(v1, v2);
		leftupNormal.y = (leftupNormal.y * bias);
		leftupNormal = (leftupNormal);
		isleftup = true;
	}
	if (index + width < verticesNum && (index + 1) % width != 0) // 위 오른쪽  
	{
		//위
		auto v1 = Vector3::Subtract(d2t[index + width].position, d2t[index].position);
		//오른
		auto v2 = Vector3::Subtract(d2t[index + 1].position, d2t[index].position);

		uprightNormal = Vector3::CrossProduct(v1, v2);
		uprightNormal.y = (uprightNormal.y * bias);
		uprightNormal = (uprightNormal);
		isupright = true;
	}
	if ((index + 1) % width != 0 && index - width >= 0) // 오른쪽 아래   
	{
		//오른
		auto v1 = Vector3::Subtract(d2t[index + 1].position, d2t[index].position);
		//아래
		auto v2 = Vector3::Subtract(d2t[index - width].position, d2t[index].position);

		rightdownNormal = Vector3::CrossProduct(v1, v2);
		rightdownNormal.y = (rightdownNormal.y * bias);
		rightdownNormal = (rightdownNormal);
		isrightdown = true;
	}
	if (index - width >= 0 && index % width != 0) // 아래 왼쪽 
	{
		//아래
		auto v1 = Vector3::Subtract(d2t[index - width].position, d2t[index].position);
		//왼쪽
		auto v2 = Vector3::Subtract(d2t[index - 1].position, d2t[index].position);

		downleftNormal = Vector3::CrossProduct(v1, v2);
		downleftNormal.y = (downleftNormal.y * bias);
		downleftNormal = (downleftNormal);
		isdownleft = true;
	}

	XMFLOAT3 finalnormal = (Vector3::Add(Vector3::Add(Vector3::Add(leftupNormal, uprightNormal), rightdownNormal), downleftNormal));
	finalnormal.x = (finalnormal.x / (isleftup + isupright + isrightdown + isdownleft));
	finalnormal.y = (finalnormal.y / (isleftup + isupright + isrightdown + isdownleft));
	finalnormal.z = (finalnormal.z / (isleftup + isupright + isrightdown + isdownleft));
	if (finalnormal.y <= 0) {
		std::cout << finalnormal.y << std::endl;
	}

	return Vector3::Normalize(finalnormal);

}

void HeightMapGridMesh::calALLNormal()
{
	for (int i = 0; i < verticesNum; ++i) {
		XMFLOAT3 normal = calNormal(i);
		diffused2TexturedVerices[i].normal = normal;
	}
}



void HeightMapGridMesh::SmoothingMesh(int idx, int width, int length, std::vector<HeightMapInForm> subHeight, std::vector<HeightMapInForm>& tempSubHeight)
{
	float upHeight = 0.0f;
	float downHeight = 0.0f;
	float rightHeight = 0.0f;
	float leftHeight = 0.0f;
	float uprightHeight = 0.0f;
	float upleftHieght = 0.0f;
	float downrightHeight = 0.0f;
	float downleftHeight = 0.0f;

	bool isExist[8] = { false };

	//위

	if (subHeight[idx].idx + width < (width * length) - (width - 1)) {
		upHeight = diffused2TexturedVerices[subHeight[idx].idx + width].position.y;
		isExist[0] = true;
	}


	//아래
	if (subHeight[idx].idx - width >= 0) {
		downHeight = diffused2TexturedVerices[subHeight[idx].idx - width].position.y;
		isExist[1] = true;
	}

	//오른쪽
	if ((subHeight[idx].idx + 1) % width != 0) {
		rightHeight = diffused2TexturedVerices[subHeight[idx].idx + 1].position.y;
		isExist[2] = true;
	}


	//왼쪽
	if (subHeight[idx].idx % width != 0) {
		leftHeight = diffused2TexturedVerices[subHeight[idx].idx - 1].position.y;
		isExist[3] = true;
	}

	//위오른쪽 대각선
	if (subHeight[idx].idx + width < (width * length) - (width - 1) && (subHeight[idx].idx + 1) % 256 != 0) {
		uprightHeight = diffused2TexturedVerices[subHeight[idx].idx + width + 1].position.y;
		isExist[4] = true;
	}


	//위왼쪽 대각선
	if (subHeight[idx].idx + width < (width * length) - (width - 1) && subHeight[idx].idx % 256 != 0) {
		upleftHieght = diffused2TexturedVerices[subHeight[idx].idx + width - 1].position.y;
		isExist[5] = true;
	}

	//아래오른쪽 대각선 
	if (subHeight[idx].idx - width >= 0 && (subHeight[idx].idx + 1) % 256 != 0) {
		downrightHeight = diffused2TexturedVerices[subHeight[idx].idx - width + 1].position.y;
		isExist[6] = true;
	}

	//아래왼쪽 대각선 
	if (subHeight[idx].idx - width >= 0 && subHeight[idx].idx % 256 != 0) {
		downleftHeight = diffused2TexturedVerices[subHeight[idx].idx - width - 1].position.y;
		isExist[7] = true;
	}


	float plusHeight = upHeight + downHeight + leftHeight + rightHeight + uprightHeight + upleftHieght + downrightHeight + downleftHeight;
	float divide = 0.0f;
	for (int i = 0; i < 8; ++i)
		divide += isExist[i];
	float avgHeight = plusHeight / divide;


	tempSubHeight[idx].position.y = (tempSubHeight[idx].position.y + avgHeight) / 2;
}

void HeightMapGridMesh::BuildLayer(ID3D12Device * device, ID3D12GraphicsCommandList * commandList)
{
	heightMapLayer[0].ChangeLayerHeight(diffused2TexturedVerices, GetVertexNum());
	heightMapLayer[0].ChangeLayerTexNum(diffused2TexturedVerices, GetVertexNum());
	

}

MeshIlluminated::MeshIlluminated(ID3D12Device *device, ID3D12GraphicsCommandList* commandList) : Mesh(device, commandList)
{
}
MeshIlluminated::~MeshIlluminated()
{
}

void MeshIlluminated::CalculateTriangleListVertexNormals(XMFLOAT3 *normals, XMFLOAT3 *positions, int verticesNum)
{
	int primitives = verticesNum / 3;
	UINT index0, index1, index2;
	for (int i = 0; i < primitives; i++)
	{
		index0 = i * 3 + 0;
		index1 = i * 3 + 1;
		index2 = i * 3 + 2;
		XMFLOAT3 edge01 = Vector3::Subtract(positions[index1], positions[index0]);
		XMFLOAT3 edge02 = Vector3::Subtract(positions[index2], positions[index0]);
		normals[index0] = normals[index1] = normals[index2] = Vector3::CrossProduct(edge01, edge02, true);
	}
}

void MeshIlluminated::CalculateTriangleListVertexNormals(XMFLOAT3 *normals, XMFLOAT3 *positions, UINT verticesNum, UINT *indices, UINT indicesNum)
{
	UINT primitives = (indices) ? (indicesNum / 3) : (verticesNum / 3);
	XMFLOAT3 sumOfNormal, edge01, edge02, normal;
	UINT index0, index1, index2;
	for (UINT j = 0; j < verticesNum; j++)
	{
		sumOfNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
		for (UINT i = 0; i < primitives; i++)
		{
			index0 = indices[i * 3 + 0];
			index1 = indices[i * 3 + 1];
			index2 = indices[i * 3 + 2];
			if (indices && ((index0 == j) || (index1 == j) || (index2 == j)))
			{
				edge01 = Vector3::Subtract(positions[index1], positions[index0]);
				edge02 = Vector3::Subtract(positions[index2], positions[index0]);
				normal = Vector3::CrossProduct(edge01, edge02, false);
				sumOfNormal = Vector3::Add(sumOfNormal, normal);
			}
		}
		normals[j] = Vector3::Normalize(sumOfNormal);
	}
}

void MeshIlluminated::CalculateTriangleStripVertexNormals(XMFLOAT3 *normals, XMFLOAT3 *positions, UINT verticesNum, UINT *indices, UINT indicesNum)
{
	UINT primitives = (indices) ? (indicesNum - 2) : (verticesNum - 2);
	XMFLOAT3 sumOfNormal(0.0f, 0.0f, 0.0f);
	UINT index0, index1, index2;
	for (UINT j = 0; j < verticesNum; j++)
	{
		sumOfNormal = XMFLOAT3(0.0f, 0.0f, 0.0f);
		for (UINT i = 0; i < primitives; i++)
		{
			index0 = ((i % 2) == 0) ? (i + 0) : (i + 1);
			if (indices) index0 = indices[index0];
			index1 = ((i % 2) == 0) ? (i + 1) : (i + 0);
			if (indices) index1 = indices[index1];
			index2 = (indices) ? indices[i + 2] : (i + 2);
			if ((index0 == j) || (index1 == j) || (index2 == j))
			{
				XMFLOAT3 edge01 = Vector3::Subtract(positions[index1], positions[index0]);
				XMFLOAT3 edge02 = Vector3::Subtract(positions[index2], positions[index0]);
				XMFLOAT3 normal = Vector3::CrossProduct(edge01, edge02, true);
				sumOfNormal = Vector3::Add(sumOfNormal, normal);
			}
		}
		normals[j] = Vector3::Normalize(sumOfNormal);
	}
}

void MeshIlluminated::CalculateVertexNormals(XMFLOAT3* normals, XMFLOAT3* positions, int verticesNum, UINT *indices, int indicesNum)
{
	switch (primitiveTopology)
	{
	case D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST:
		if (indices)
			CalculateTriangleListVertexNormals(normals, positions, verticesNum, indices, indicesNum);
		else
			CalculateTriangleListVertexNormals(normals, positions, verticesNum);
		break;
	case D3D_PRIMITIVE_TOPOLOGY_TRIANGLESTRIP:
		CalculateTriangleStripVertexNormals(normals, positions, verticesNum, indices, indicesNum);
		break;
	default:
		break;
	}
}

CubeMeshIlluminated::CubeMeshIlluminated(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth) : MeshIlluminated(device, commandList)
{
	verticesNum = 8;
	int stride = sizeof(IlluminatedVertex);
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	indicesNum = 36;
	UINT indices[36];
	indices[0] = 3; indices[1] = 1; indices[2] = 0;
	indices[3] = 2; indices[4] = 1; indices[5] = 3;
	indices[6] = 0; indices[7] = 5; indices[8] = 4;
	indices[9] = 1; indices[10] = 5; indices[11] = 0;
	indices[12] = 3; indices[13] = 4; indices[14] = 7;
	indices[15] = 0; indices[16] = 4; indices[17] = 3;
	indices[18] = 1; indices[19] = 6; indices[20] = 5;
	indices[21] = 2; indices[22] = 6; indices[23] = 1;
	indices[24] = 2; indices[25] = 7; indices[26] = 6;
	indices[27] = 3; indices[28] = 7; indices[29] = 2;
	indices[30] = 6; indices[31] = 4; indices[32] = 5;
	indices[33] = 7; indices[34] = 4; indices[35] = 6;
	indexBuffer = ::CreateBufferResource(device, commandList, indices, sizeof(UINT) * indicesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &indexUploadBuffer);
	indexBufferView.BufferLocation = indexBuffer->GetGPUVirtualAddress();
	indexBufferView.Format = DXGI_FORMAT_R32_UINT;
	indexBufferView.SizeInBytes = sizeof(UINT) * indicesNum;
	XMFLOAT3 positions[8];
	float fx = width * 0.5f, fy = height * 0.5f, fz = depth * 0.5f;
	positions[0] = XMFLOAT3(-fx, +fy, -fz);
	positions[1] = XMFLOAT3(+fx, +fy, -fz);
	positions[2] = XMFLOAT3(+fx, +fy, +fz);
	positions[3] = XMFLOAT3(-fx, +fy, +fz);
	positions[4] = XMFLOAT3(-fx, -fy, -fz);
	positions[5] = XMFLOAT3(+fx, -fy, -fz);
	positions[6] = XMFLOAT3(+fx, -fy, +fz);
	positions[7] = XMFLOAT3(-fx, -fy, +fz);
	XMFLOAT3 normals[8];
	for (int i = 0; i < 8; i++) normals[i] = XMFLOAT3(0.0f, 0.0f, 0.0f);
	CalculateVertexNormals(normals, positions, verticesNum, indices, indicesNum);

	IlluminatedVertex vertices[8];
	for (int i = 0; i < 8; i++)
		vertices[i] = IlluminatedVertex(positions[i], normals[i]);
	vertexBuffer = CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}
CubeMeshIlluminated::~CubeMeshIlluminated()
{
}

ArrowMesh::ArrowMesh(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, float width, float height, float depth, float Red, float Green, float Blue) : Mesh(device, commandList)
{
	verticesNum = 60;
	stride = sizeof(DiffusedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	float x = width * 0.5f, y = height, z = depth * 0.5f;
	float doublex = width; float doubley = height * 2;

	pickOffset = 3;
	primitiveNum = (primitiveTopology == D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ?
		(verticesNum / 3) : (verticesNum - 2);

	int i = 0;
	vertices = new DiffusedVertex[66];
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	//화살표 삼각형 아랫면 
	vertices[i++] = DiffusedVertex(XMFLOAT3(+doublex, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+doublex, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-doublex, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-doublex, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+doublex, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-doublex, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));


	//왼쪽 면 
	vertices[i++] = DiffusedVertex(XMFLOAT3(-doublex, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-doublex, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +doubley, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +doubley, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +doubley, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-doublex, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	//오른쪽 면 
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +doubley, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+doublex, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+doublex, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(+doublex, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +doubley, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, +doubley, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	// 앞
	vertices[i++] = DiffusedVertex(XMFLOAT3(-doublex, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, doubley, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+doublex, +y, -z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));

	// 뒤
	vertices[i++] = DiffusedVertex(XMFLOAT3(+doublex, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(0.0f, doubley, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-doublex, +y, +z), XMFLOAT4(Red + ((i / 6) % 6) * 0.1f, Green + ((i / 6) % 6) * 0.1f, Blue + ((i / 6) % 6) * 0.1f, 0.0f));



	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum,
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;
}

ArrowMesh::~ArrowMesh()
{
}

/////////////////////////////////////////////////////////////////////////////////////////////////
//
StandardMesh::StandardMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList) : Mesh(device, commandList)
{
}

StandardMesh::~StandardMesh()
{
	if (positionBuffer)
		positionBuffer->Release();
	if (textureCoord0Buffer)
		textureCoord0Buffer->Release();
	if (normalBuffer)
		normalBuffer->Release();
	if (tangentBuffer)
		tangentBuffer->Release();
	if (biTangentBuffer)
		biTangentBuffer->Release();

	if (colors) {
		delete[] colors;
		colors = NULL;
	}
	if (normals) {
		delete[] normals;
		normals = NULL;
	}
	if (tangents) {
		delete[] tangents;
		tangents = NULL;
	}
	if (biTangents) {
		delete[] biTangents;
		biTangents = NULL;
	}
	if (textureCoords0) {
		delete[] textureCoords0;
		textureCoords0 = NULL;
	}
	if (textureCoords1) {
		delete[] textureCoords1;
		textureCoords1 = NULL;
	}
}

void StandardMesh::ReleaseUploadBuffers()
{
	Mesh::ReleaseUploadBuffers();

	if (positionUploadBuffer) {
		positionUploadBuffer->Release();
		positionUploadBuffer = NULL;
	}

	if (textureCoord0UploadBuffer) {
		textureCoord0UploadBuffer->Release();
		textureCoord0UploadBuffer = NULL;
	}

	if (textureCoord1UploadBuffer) {
		textureCoord1UploadBuffer->Release();
		textureCoord1UploadBuffer = NULL;
	}

	if (normalUploadBuffer) {
		normalUploadBuffer->Release();
		normalUploadBuffer = NULL;
	}

	if (tangentUploadBuffer) {
		tangentUploadBuffer->Release();
		tangentUploadBuffer = NULL;
	}

	if (biTangentUploadBuffer) {
		biTangentUploadBuffer->Release();
		biTangentUploadBuffer = NULL;
	}
}

void StandardMesh::LoadMeshFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, FILE *inFile , BoundingBoxMesh*& boundingMesh , bool isSkined)
{
	char token[64] = { '\0' };
	BYTE strLength = 0;

	int nPositions = 0, nColors = 0, nNormals = 0, nTangents = 0, nBiTangents = 0, nTextureCoords = 0, nIndices = 0, nSubMeshes = 0, nSubIndices = 0;

	UINT nReads = (UINT)::fread(&verticesNum, sizeof(int), 1, inFile);
	nReads = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
	nReads = (UINT)::fread(meshName, sizeof(char), strLength, inFile);
	meshName[strLength] = '\0';

	for (; ; )
	{
		nReads = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
		nReads = (UINT)::fread(token, sizeof(char), strLength, inFile);
		token[strLength] = '\0';

		if (!strcmp(token, "<Bounds>:"))
		{
			nReads = (UINT)::fread(&boundingBoxAABBCenter, sizeof(XMFLOAT3), 1, inFile);
			nReads = (UINT)::fread(&boundingBoxAABBExtents, sizeof(XMFLOAT3), 1, inFile);
			if(isSkined == false)
			boundingMesh = new BoundingBoxMesh(device, commandList, boundingBoxAABBCenter, boundingBoxAABBExtents.x, boundingBoxAABBExtents.y, boundingBoxAABBExtents.z);
		}
		else if (!strcmp(token, "<Positions>:"))
		{
			nReads = (UINT)::fread(&nPositions, sizeof(int), 1, inFile);
			if (nPositions > 0)
			{
				type |= VertexPosition;
				positions = new XMFLOAT3[nPositions];
				nReads = (UINT)::fread(positions, sizeof(XMFLOAT3), nPositions, inFile);

				positionBuffer = ::CreateBufferResource(device, commandList, positions, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &positionUploadBuffer);

				positionBufferView.BufferLocation = positionBuffer->GetGPUVirtualAddress();
				positionBufferView.StrideInBytes = sizeof(XMFLOAT3);
				positionBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<Colors>:"))
		{
			nReads = (UINT)::fread(&nColors, sizeof(int), 1, inFile);
			if (nColors > 0)
			{
				type |= VertexColor;
				colors = new XMFLOAT4[nColors];
				nReads = (UINT)::fread(colors, sizeof(XMFLOAT4), nColors, inFile);
			}
		}
		else if (!strcmp(token, "<TextureCoords0>:"))
		{
			nReads = (UINT)::fread(&nTextureCoords, sizeof(int), 1, inFile);
			if (nTextureCoords > 0)
			{
				type |= VertexTexCoord0;
				textureCoords0 = new XMFLOAT2[nTextureCoords];
				nReads = (UINT)::fread(textureCoords0, sizeof(XMFLOAT2), nTextureCoords, inFile);

				textureCoord0Buffer = ::CreateBufferResource(device, commandList, textureCoords0, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord0UploadBuffer);

				textureCoord0BufferView.BufferLocation = textureCoord0Buffer->GetGPUVirtualAddress();
				textureCoord0BufferView.StrideInBytes = sizeof(XMFLOAT2);
				textureCoord0BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<TextureCoords1>:"))
		{
			nReads = (UINT)::fread(&nTextureCoords, sizeof(int), 1, inFile);
			if (nTextureCoords > 0)
			{
				type |= VertexTexCoord1;
				textureCoords1 = new XMFLOAT2[nTextureCoords];
				nReads = (UINT)::fread(textureCoords1, sizeof(XMFLOAT2), nTextureCoords, inFile);

				textureCoord1Buffer = ::CreateBufferResource(device, commandList, textureCoords1, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord1UploadBuffer);

				textureCoord1BufferView.BufferLocation = textureCoord1Buffer->GetGPUVirtualAddress();
				textureCoord1BufferView.StrideInBytes = sizeof(XMFLOAT2);
				textureCoord1BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<Normals>:"))
		{
			nReads = (UINT)::fread(&nNormals, sizeof(int), 1, inFile);
			if (nNormals > 0)
			{
				type |= VertexNormal;
				normals = new XMFLOAT3[nNormals];
				nReads = (UINT)::fread(normals, sizeof(XMFLOAT3), nNormals, inFile);

				normalBuffer = ::CreateBufferResource(device, commandList, normals, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &normalUploadBuffer);

				normalBufferView.BufferLocation = normalBuffer->GetGPUVirtualAddress();
				normalBufferView.StrideInBytes = sizeof(XMFLOAT3);
				normalBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<Tangents>:"))
		{
			nReads = (UINT)::fread(&nTangents, sizeof(int), 1, inFile);
			if (nTangents > 0)
			{
				type |= VertexTangent;
				tangents = new XMFLOAT3[nTangents];
				nReads = (UINT)::fread(tangents, sizeof(XMFLOAT3), nTangents, inFile);

				tangentBuffer = ::CreateBufferResource(device, commandList, tangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &tangentUploadBuffer);

				tangentBufferView.BufferLocation = tangentBuffer->GetGPUVirtualAddress();
				tangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
				tangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<BiTangents>:"))
		{
			nReads = (UINT)::fread(&nBiTangents, sizeof(int), 1, inFile);
			if (nBiTangents > 0)
			{
				biTangents = new XMFLOAT3[nBiTangents];
				nReads = (UINT)::fread(biTangents, sizeof(XMFLOAT3), nBiTangents, inFile);

				biTangentBuffer = ::CreateBufferResource(device, commandList, biTangents, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &biTangentUploadBuffer);

				biTangentBufferView.BufferLocation = biTangentBuffer->GetGPUVirtualAddress();
				biTangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
				biTangentBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
				vertexSize++;
			}
		}
		else if (!strcmp(token, "<SubMeshes>:"))
		{
			nReads = (UINT)::fread(&(subMeshesNum), sizeof(int), 1, inFile);
			if (subMeshesNum > 0)
			{
				subSetIndicesNum = new int[subMeshesNum];
				subSetIndices = new UINT*[subMeshesNum];

				subSetIndexBuffers = new ID3D12Resource*[subMeshesNum];
				subSetIndexUploadBuffers = new ID3D12Resource*[subMeshesNum];
				subSetIndexBufferViews = new D3D12_INDEX_BUFFER_VIEW[subMeshesNum];

				for (int i = 0; i < subMeshesNum; i++)
				{
					nReads = (UINT)::fread(&strLength, sizeof(BYTE), 1, inFile);
					nReads = (UINT)::fread(token, sizeof(char), strLength, inFile);
					token[strLength] = '\0';
					if (!strcmp(token, "<SubMesh>:"))
					{
						int nIndex = 0;
						nReads = (UINT)::fread(&nIndex, sizeof(int), 1, inFile);
						nReads = (UINT)::fread(&(subSetIndicesNum[i]), sizeof(int), 1, inFile);
						if (subSetIndicesNum[i] > 0)
						{
							subSetIndices[i] = new UINT[subSetIndicesNum[i]];
							nReads = (UINT)::fread(subSetIndices[i], sizeof(UINT)*subSetIndicesNum[i], 1, inFile);

							subSetIndexBuffers[i] = ::CreateBufferResource(device, commandList, subSetIndices[i], sizeof(UINT) * subSetIndicesNum[i], D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_INDEX_BUFFER, &subSetIndexUploadBuffers[i]);

							subSetIndexBufferViews[i].BufferLocation = subSetIndexBuffers[i]->GetGPUVirtualAddress();
							subSetIndexBufferViews[i].Format = DXGI_FORMAT_R32_UINT;
							subSetIndexBufferViews[i].SizeInBytes = sizeof(UINT) * subSetIndicesNum[i];
						}

					}
				}
			}
		}
		else if (!strcmp(token, "</Mesh>"))
		{
			break;
		}
	}
}

void StandardMesh::Render(ID3D12GraphicsCommandList *commandList, int subSet)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);

	D3D12_VERTEX_BUFFER_VIEW pVertexBufferViews[5] = { positionBufferView, textureCoord0BufferView, normalBufferView, tangentBufferView, biTangentBufferView };
	commandList->IASetVertexBuffers(slot, 5, pVertexBufferViews);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], 1, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, 1, offset, 0);
	}
}

void StandardMesh::Render(ID3D12GraphicsCommandList *commandList, int nSubSet, UINT instancesNum, std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBufferViews)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);
	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[5 + instancingBufferViews.size()];
	vertexBufferViews[0] = positionBufferView;
	vertexBufferViews[1] = textureCoord0BufferView;
	vertexBufferViews[2] = normalBufferView;
	vertexBufferViews[3] = tangentBufferView;
	vertexBufferViews[4] = biTangentBufferView;


	for (int i = 0; i < instancingBufferViews.size(); ++i)
	{
		vertexBufferViews[i + 5] = instancingBufferViews[i];
	}
	commandList->IASetVertexBuffers(slot, 5 + UINT(instancingBufferViews.size()), vertexBufferViews);

	if ((subMeshesNum > 0) && (nSubSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[nSubSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[nSubSet], instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}

	delete[] vertexBufferViews;

}

TexturedCubeMesh::TexturedCubeMesh(ID3D12Device * pd3dDevice, ID3D12GraphicsCommandList * pd3dCommandList, float fWidth, float fHeight, float fdepth) : Mesh(pd3dDevice, pd3dCommandList)
{
	verticesNum = 36;//버텍스 개수 6개
	stride = sizeof(TexturedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	texturedVertex = new TexturedVertex[36];
	float x = (fWidth * 0.5f), y = (fHeight * 0.5f), z = (fdepth * 0.5f);
	int i = 0;

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT2(0.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT2(1.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));

	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT2(0.0f, 0.0f));
	texturedVertex[i++] = TexturedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT2(1.0f, 1.0f));
	texturedVertex[i] = TexturedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT2(0.0f, 1.0f));

	vertexBuffer = ::CreateBufferResource(pd3dDevice, pd3dCommandList, texturedVertex, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	//삼각형 메쉬를 리소스(정점 버퍼)로 생성한다.

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;

}
TexturedCubeMesh::~TexturedCubeMesh()
{
}

TexturedRectMesh::TexturedRectMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, float width, float height, float depth, float positionX, float positionY, float positionZ) : StandardMesh(device, commandList)
{
	verticesNum = 6;
	offset = 0;
	slot = 0;
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;

	positions = new XMFLOAT3[verticesNum];
	textureCoords0 = new XMFLOAT2[verticesNum];

	float fx = (width * 0.5f) + positionX, fy = (height * 0.5f) + positionY, fz = (depth * 0.5f) + positionZ;
	int i;
	if (width == 0.0f)
	{
		i = 0;
		if (positionX > 0.0f)
		{
			positions[i] = XMFLOAT3(fx, +fy, -fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(fx, -fy, -fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, -fy, +fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, -fy, +fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, +fy, +fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(fx, +fy, -fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
		else
		{
			positions[i] = XMFLOAT3(fx, +fy, +fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(fx, -fy, +fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, -fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, -fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(fx, +fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(fx, +fy, +fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
	}
	else if (height == 0.0f)
	{
		i = 0;
		if (positionY > 0.0f)
		{
			positions[i] = XMFLOAT3(+fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, fy, -fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
		else
		{
			positions[i] = XMFLOAT3(+fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, -fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, fy, +fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, fy, +fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
	}
	else if (depth == 0.0f)
	{
		i = 0;
		if (positionZ > 0.0f)
		{
			positions[i] = XMFLOAT3(+fx, +fy, fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(-fx, +fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(+fx, +fy, fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
		else
		{
			positions[i] = XMFLOAT3(-fx, +fy, fz), textureCoords0[i++] = XMFLOAT2(1.0f, 0.0f);
			positions[i] = XMFLOAT3(-fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(1.0f, 1.0f);
			positions[i] = XMFLOAT3(+fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(+fx, -fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 1.0f);
			positions[i] = XMFLOAT3(+fx, +fy, fz), textureCoords0[i++] = XMFLOAT2(0.0f, 0.0f);
			positions[i] = XMFLOAT3(-fx, +fy, fz), textureCoords0[i] = XMFLOAT2(1.0f, 0.0f);
		}
	}

	positionBuffer = CreateBufferResource(device, commandList, positions, sizeof(XMFLOAT3) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &positionUploadBuffer);
	positionBufferView.BufferLocation = positionBuffer->GetGPUVirtualAddress();
	positionBufferView.StrideInBytes = sizeof(XMFLOAT3);
	positionBufferView.SizeInBytes = sizeof(XMFLOAT3) * verticesNum;
	type |= VertexPosition;
	vertexSize++;

	textureCoord0Buffer = CreateBufferResource(device, commandList, textureCoords0, sizeof(XMFLOAT2) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &textureCoord0UploadBuffer);
	textureCoord0BufferView.BufferLocation = textureCoord0Buffer->GetGPUVirtualAddress();
	textureCoord0BufferView.StrideInBytes = sizeof(XMFLOAT2);
	textureCoord0BufferView.SizeInBytes = sizeof(XMFLOAT2) * verticesNum;
	type |= VertexTexCoord0;
	vertexSize++;

	XMFLOAT3* dummy = new XMFLOAT3;

	normalBuffer = CreateBufferResource(device, commandList, dummy, sizeof(XMFLOAT3), D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &normalUploadBuffer);
	normalBufferView.BufferLocation = normalBuffer->GetGPUVirtualAddress();
	normalBufferView.StrideInBytes = sizeof(XMFLOAT3);
	normalBufferView.SizeInBytes = sizeof(XMFLOAT3);

	tangentBuffer = CreateBufferResource(device, commandList, dummy, sizeof(XMFLOAT3), D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &tangentUploadBuffer);
	tangentBufferView.BufferLocation = tangentBuffer->GetGPUVirtualAddress();
	tangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
	tangentBufferView.SizeInBytes = sizeof(XMFLOAT3);

	biTangentBuffer = CreateBufferResource(device, commandList, dummy, sizeof(XMFLOAT3), D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &biTangentUploadBuffer);
	biTangentBufferView.BufferLocation = biTangentBuffer->GetGPUVirtualAddress();
	biTangentBufferView.StrideInBytes = sizeof(XMFLOAT3);
	biTangentBufferView.SizeInBytes = sizeof(XMFLOAT3);

}
TexturedRectMesh::~TexturedRectMesh()
{
}

BoundingBoxMesh::BoundingBoxMesh(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, float width, float height, float depth) : Mesh(device, commandList)
{
	verticesNum = 36;//버텍스 개수 6개
	stride = sizeof(DiffusedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	vertices = new DiffusedVertex[36];
	float x = (width), y = (height), z = (depth);
	int i = 0;

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, +y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x, -y, +z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i] = DiffusedVertex(XMFLOAT3(+x, -y, -z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	//삼각형 메쉬를 리소스(정점 버퍼)로 생성한다.

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;

}

BoundingBoxMesh::BoundingBoxMesh(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT3 center ,float width, float height, float depth) : Mesh(device, commandList)
{
	verticesNum = 36;//버텍스 개수 6개
	stride = sizeof(DiffusedVertex);
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	vertices = new DiffusedVertex[36];
	float x = (width), y = (height), z = (depth);
	int i = 0;

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
																			 
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
																			 
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
																			 
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y + center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
																		  
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
																		    
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y+ center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y+ center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
																		    
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y+ center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
																		    
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y+ center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y+ center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
																		    
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, +y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y+ center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(-x + center.x, -y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
																		    
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y+ center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
															 			    
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, +y+ center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i++] = DiffusedVertex(XMFLOAT3(+x + center.x, -y+ center.y, +z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));
	vertices[i] = DiffusedVertex(XMFLOAT3(+x + center.x,   -y+ center.y, -z + center.z), XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f));

	vertexBuffer = ::CreateBufferResource(device, commandList, vertices, stride * verticesNum, D3D12_HEAP_TYPE_DEFAULT,
		D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);
	//삼각형 메쉬를 리소스(정점 버퍼)로 생성한다.

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;

}


void BoundingBoxMesh::Render(ID3D12GraphicsCommandList * commandList, int subsetNum, UINT instancesNum, std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBufferViews)
{
	commandList->IASetPrimitiveTopology(primitiveTopology);
	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[2];
	vertexBufferViews[0] = vertexBufferView;
	vertexBufferViews[1] = instancingBufferViews[0];

	commandList->IASetVertexBuffers(slot, 2, vertexBufferViews);

	if ((subMeshesNum > 0) && (subsetNum < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subsetNum]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subsetNum], instancesNum, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNum, offset, 0);
	}

	delete[] vertexBufferViews;
}

BoundingBoxMesh::~BoundingBoxMesh()
{
}
SkinnedMesh::SkinnedMesh(ID3D12Device *device, ID3D12GraphicsCommandList *commandList) : StandardMesh(device, commandList)
{
}

SkinnedMesh::~SkinnedMesh()
{
	if (boneIndices) delete[] boneIndices;
	if (boneWeights) delete[] boneWeights;

	if (skinningBoneFrameCaches) delete[] skinningBoneFrameCaches;
	if (skinningBoneNames) delete[] skinningBoneNames;

	if (xmf4x4BindPoseBoneOffsets) delete[] xmf4x4BindPoseBoneOffsets;
	if (bindPoseBoneOffsets) bindPoseBoneOffsets->Release();

	if (boneIndexBuffer) boneIndexBuffer->Release();
	if (boneWeightBuffer) boneWeightBuffer->Release();

	ReleaseShaderVariables();
}

void SkinnedMesh::CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList)
{
}

void SkinnedMesh::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList)
{
	if (bindPoseBoneOffsets)
	{
		D3D12_GPU_VIRTUAL_ADDRESS d3dcbBoneOffsetsGpuVirtualAddress = bindPoseBoneOffsets->GetGPUVirtualAddress();
		commandList->SetGraphicsRootConstantBufferView(GraphicsRootSkinnedBoneOffset, d3dcbBoneOffsetsGpuVirtualAddress); //Skinned Bone Offsets
	}

	if (skinningBoneTransforms)
	{
		D3D12_GPU_VIRTUAL_ADDRESS d3dcbBoneTransformsGpuVirtualAddress = skinningBoneTransforms->GetGPUVirtualAddress();
		commandList->SetGraphicsRootShaderResourceView(GraphicsRootSkinnedBoneTransform, d3dcbBoneTransformsGpuVirtualAddress); //Skinned Bone Transforms
	}
}

void SkinnedMesh::ReleaseShaderVariables()
{
}

void SkinnedMesh::ReleaseUploadBuffers()
{
	StandardMesh::ReleaseUploadBuffers();

	if (boneIndexUploadBuffer) boneIndexUploadBuffer->Release();
	boneIndexUploadBuffer = NULL;

	if (boneWeightUploadBuffer) boneWeightUploadBuffer->Release();
	boneWeightUploadBuffer = NULL;
}

void SkinnedMesh::PrepareSkinning(GameObject *pModelRootObject)
{
	for (int j = 0; j < skinningBones; j++)
	{
		skinningBoneFrameCaches[j] = pModelRootObject->FindFrame(skinningBoneNames[j]);
	}
}


void SkinnedMesh::LoadSkinInfoFromFile(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, FILE *pInFile)
{
	char pstrToken[64] = { '\0' };
	UINT nReads = 0;

	::ReadStringFromFile(pInFile, meshName);

	for (; ; )
	{
		::ReadStringFromFile(pInFile, pstrToken);
		if (!strcmp(pstrToken, "<BonesPerVertex>:"))
		{
			bonesPerVertex = ::ReadIntegerFromFile(pInFile);
		}
		else if (!strcmp(pstrToken, "<Bounds>:"))
		{
			nReads = (UINT)::fread(&boundingBoxAABBCenter, sizeof(XMFLOAT3), 1, pInFile);
			nReads = (UINT)::fread(&boundingBoxAABBExtents, sizeof(XMFLOAT3), 1, pInFile);
		}
		else if (!strcmp(pstrToken, "<BoneNames>:"))
		{
			skinningBones = ::ReadIntegerFromFile(pInFile);
			if (skinningBones > 0)
			{
				skinningBoneNames = new char[skinningBones][64];
				skinningBoneFrameCaches = new GameObject*[skinningBones];
				for (int i = 0; i < skinningBones; i++)
				{
					::ReadStringFromFile(pInFile, skinningBoneNames[i]);
					skinningBoneFrameCaches[i] = NULL;
				}
			}
		}
		else if (!strcmp(pstrToken, "<BoneOffsets>:"))
		{
			skinningBones = ::ReadIntegerFromFile(pInFile);
			if (skinningBones > 0)
			{
				xmf4x4BindPoseBoneOffsets = new XMFLOAT4X4[skinningBones];
				nReads = (UINT)::fread(xmf4x4BindPoseBoneOffsets, sizeof(XMFLOAT4X4), skinningBones, pInFile);

				UINT ncbElementBytes = (((sizeof(XMFLOAT4X4) * SKINNED_ANIMATION_BONES) + 255) & ~255); //256의 배수
				bindPoseBoneOffsets = ::CreateBufferResource(device, commandList, NULL, ncbElementBytes, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
				bindPoseBoneOffsets->Map(0, NULL, (void **)&mappedBindPoseBoneOffsets);

				for (int i = 0; i < skinningBones; i++)
				{
					XMStoreFloat4x4(&mappedBindPoseBoneOffsets[i], XMMatrixTranspose(XMLoadFloat4x4(&xmf4x4BindPoseBoneOffsets[i])));
				}
			}
		}
		else if (!strcmp(pstrToken, "<BoneIndices>:"))
		{
			type |= VERTEXT_BONE_INDEX_WEIGHT;

			verticesNum = ::ReadIntegerFromFile(pInFile);
			if (verticesNum > 0)
			{
				boneIndices = new XMINT4[verticesNum];

				nReads = (UINT)::fread(boneIndices, sizeof(XMINT4), verticesNum, pInFile);
				boneIndexBuffer = ::CreateBufferResource(device, commandList, boneIndices, sizeof(XMINT4) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &boneIndexUploadBuffer);

				boneIndexBufferView.BufferLocation = boneIndexBuffer->GetGPUVirtualAddress();
				boneIndexBufferView.StrideInBytes = sizeof(XMINT4);
				boneIndexBufferView.SizeInBytes = sizeof(XMINT4) * verticesNum;
			}
		}
		else if (!strcmp(pstrToken, "<BoneWeights>:"))
		{
			type |= VERTEXT_BONE_INDEX_WEIGHT;

			verticesNum = ::ReadIntegerFromFile(pInFile);
			if (verticesNum > 0)
			{
				boneWeights = new XMFLOAT4[verticesNum];

				nReads = (UINT)::fread(boneWeights, sizeof(XMFLOAT4), verticesNum, pInFile);
				boneWeightBuffer = ::CreateBufferResource(device, commandList, boneWeights, sizeof(XMFLOAT4) * verticesNum, D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &boneWeightUploadBuffer);

				boneWeightBufferView.BufferLocation = boneWeightBuffer->GetGPUVirtualAddress();
				boneWeightBufferView.StrideInBytes = sizeof(XMFLOAT4);
				boneWeightBufferView.SizeInBytes = sizeof(XMFLOAT4) * verticesNum;
			}
		}
		else if (!strcmp(pstrToken, "</SkinningInfo>"))
		{
			break;
		}
	}
}

void SkinnedMesh::OnPreRender(ID3D12GraphicsCommandList *commandList, void *context)
{
	D3D12_VERTEX_BUFFER_VIEW pVertexBufferViews[7] = { positionBufferView, textureCoord0BufferView, normalBufferView, tangentBufferView, biTangentBufferView, boneIndexBufferView, boneWeightBufferView };
	commandList->IASetVertexBuffers(slot, 7, pVertexBufferViews);
}

void SkinnedMesh::Render(ID3D12GraphicsCommandList *commandList, int subSet)
{
	UpdateShaderVariables(commandList);

	OnPreRender(commandList, NULL);

	commandList->IASetPrimitiveTopology(primitiveTopology);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], 1, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, 1, offset, 0);
	}
}

void SkinnedMesh::Render(ID3D12GraphicsCommandList *commandList, int subSet, UINT instancesNumm, std::vector<D3D12_VERTEX_BUFFER_VIEW> instancingBufferViews)
{
	UpdateShaderVariables(commandList);

	D3D12_VERTEX_BUFFER_VIEW* vertexBufferViews;
	vertexBufferViews = new D3D12_VERTEX_BUFFER_VIEW[7 + instancingBufferViews.size()];
	vertexBufferViews[0] = positionBufferView;
	vertexBufferViews[1] = textureCoord0BufferView;
	vertexBufferViews[2] = normalBufferView;
	vertexBufferViews[3] = tangentBufferView;
	vertexBufferViews[4] = biTangentBufferView;
	vertexBufferViews[5] = boneIndexBufferView;
	vertexBufferViews[6] = boneWeightBufferView;

	for (int i = 0; i < instancingBufferViews.size(); ++i)
		vertexBufferViews[7 + i] = instancingBufferViews[i];

	commandList->IASetVertexBuffers(slot, 7 + (UINT)instancingBufferViews.size(), vertexBufferViews);
	commandList->IASetPrimitiveTopology(primitiveTopology);

	if ((subMeshesNum > 0) && (subSet < subMeshesNum))
	{
		commandList->IASetIndexBuffer(&(subSetIndexBufferViews[subSet]));
		commandList->DrawIndexedInstanced(subSetIndicesNum[subSet], instancesNumm, 0, 0, 0);
	}
	else
	{
		commandList->DrawInstanced(verticesNum, instancesNumm, offset, 0);
	}

	delete[] vertexBufferViews;
}



NaviMesh::~NaviMesh()
{
	if(vertexBuffer)
	vertexBuffer->Release();
	if(indexBuffer)
	indexBuffer->Release();

	delete[] diffuseVertex;
	diffuseVertex = NULL;
}

void NaviMesh::CreateNaviMesh(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int width, int length, XMFLOAT3 scale, std::vector<BoundingOrientedBox>& tbvec , XMFLOAT4X4 tInverseWorld , Diffused2TexturedVertex* terrainvertices , int (*naviBuffer)[257]
 ,std::vector<NaviTile>& ntvec )
{
	
	int startnt = ntvec.size();

	isCreate = true;
	if (vertexBuffer)
		vertexBuffer->Release();
	if (indexBuffer) {
		indexBuffer->Release();
	}
	if (diffuseVertex) {
		delete[] diffuseVertex;
		diffuseVertex = NULL;
		subIndex.clear();
		leftIndex.clear();
	}

	this->width = width;
	this->length = length;
	this->scale = scale;
	pickOffset = (primitiveTopology == D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ? 3 : 1;
	stride = sizeof(DiffusedVertex);
	verticesNum = width * length;
	diffuseVertex = new DiffusedVertex[verticesNum];
	
	std::vector<DiffusedVertex> leftVertex;

	
	for (int i = 0 , z = 0; z < length; ++z) {
		for (int x = 0; x < width; ++x) {
			diffuseVertex[i].position = XMFLOAT3(x * this->scale.x, terrainvertices[(x * (int(scale.x) / 2)) + ((z * (int(scale.x) / 2)) * 257)].position.y, z * this->scale.z);
			diffuseVertex[i].diffuse = XMFLOAT4(1.0f, 0.0f, 0.0f, 1.0f);
			i++;
		}
	}
	for (int i = 0, z = 0; z < length; ++z) {
		for (int x = 0; x < width; ++x) {
			bool isRv = true;
			for (int j = 0; j < tbvec.size(); ++j) {
				BoundingOrientedBox toriBox;
				tbvec[j].Transform(toriBox, XMLoadFloat4x4(&tInverseWorld));
				if (x != this->width - 1 && z != this->length - 1) {

					auto v1 = XMLoadFloat3(&diffuseVertex[x + z * width].position);
					auto v2 = XMLoadFloat3(&diffuseVertex[x + (z * width) + width].position);
					auto v3 = XMLoadFloat3(&diffuseVertex[x + (z * width) + 1].position);
					//아래 삼각형 
					if (toriBox.Intersects(v1, v2 , v3)) {
						isRv = false;
					}

					 v1 = XMLoadFloat3(&diffuseVertex[x + z * width + width].position);
					 v2 = XMLoadFloat3(&diffuseVertex[x + z * width + width + 1].position);
					 v3 = XMLoadFloat3(&diffuseVertex[x + z * width + 1].position);
					 if (toriBox.Intersects(v1, v2, v3)) {
						 isRv = false;
					 }
				}
			}
			if (x != this->width - 1 && z != this->length - 1 && isRv == true) {
				subIndex.emplace_back(x + z * width, x + (z * width) + width, x + (z * width) + 1); // 아래 삼각형 
				subIndex.emplace_back(x + z * width + width, x + z * width + width + 1, x + z * width + 1); // 위에 삼각형 

				//타일을 넣어주자.
				XMFLOAT3 tv0 = diffuseVertex[x + z * width].position;
				XMFLOAT3 tv1 = diffuseVertex[x + (z * width) + width].position;
				XMFLOAT3 tv2 = diffuseVertex[x + (z * width) + 1].position;
				Tri tri0{ tv0, tv1, tv2};
				
				XMFLOAT3 tv3 = diffuseVertex[x + z * width + width].position;
				XMFLOAT3 tv4 = diffuseVertex[x + z * width + width + 1].position;
				XMFLOAT3 tv5 = diffuseVertex[x + z * width + 1].position;

				Tri tri1{ tv3, tv4, tv5 };
				
				NaviTile nt{ tri0 , tri1 };
				
				int tempx = (x * (int(scale.x) / 2));
				int tempz = ((z * (int(scale.x) / 2)));
				nt.x = tempx;
				nt.z = tempz;
				nt.width = (int(scale.x) / 2);
				
				for (int tz = tempz; tz < (z + 1) * (int(scale.z) / 2); ++tz) {
					for (int tx = tempx; tx < (x + 1) *(int(scale.x) / 2); ++tx) {
						naviBuffer[tz][tx] = ntvec.size();
						
					}
				}
				nt.idx = ntvec.size();
				ntvec.emplace_back(nt);

				

			}
			else if(x != this->width - 1 && z != this->length - 1 && isRv != true){
				LeftIndex tleft;
				tleft.x = (x * (int(scale.x) / 2));
				tleft.z = (z * (int(scale.x) / 2));
				leftIndex.emplace_back(tleft); // 뒤에 x , y 는 진짜 terrain의 vertex에서의 x,y

			}

		}
	}

	//다했다면 이제 인접성 검사를해야한다.
	for (int i = startnt; i < ntvec.size(); ++i) {
		int nx = ntvec[i].x;
		int nz = ntvec[i].z;

		//왼쪽
		if ((nx - 1) >= 0) {
			
			if (naviBuffer[nz][nx - 1] != -1) {
				int ltile = naviBuffer[nz][nx -1];
				ntvec[i].leftTile.emplace_back(ltile);
			}
		}
		// 오른쪽 
		if ((nx + (int(scale.x) / 2)) < 257) {
			if (naviBuffer[nz][nx + (int(scale.x) / 2)] != -1) {
				int rtile = naviBuffer[nz][nx + (int(scale.x) / 2)];
				ntvec[i].rightTile.emplace_back(rtile);
			}
		}
		//  아래
		if ((nz - 1) >= 0) {
			if (naviBuffer[nz - 1][nx] != -1) {
				int dtile = naviBuffer[nz - 1][nx];
				ntvec[i].downTile.emplace_back(dtile);
			}
		}
		//위
		if ((nz + (int(scale.z) / 2)) < 257) {
			if (naviBuffer[nz + (int(scale.z) / 2)][nx] != -1) {
				int utile = naviBuffer[nz + (int(scale.z) / 2)][nx];
				ntvec[i].upTile.emplace_back(utile);
			}
		}
		//왼위
		if ((nx - 1) >= 0 && (nz + (int(scale.z) / 2)) < 257) {
			if (naviBuffer[nz + (int(scale.z) / 2)][nx - 1] != -1) {
				int lutile = naviBuffer[nz + (int(scale.z) / 2)][nx - 1];
				ntvec[i].leftupTile .emplace_back(lutile);
			}
		}
		//왼아래
		if ((nx - 1) >= 0 && (nz - 1) >= 0) {
			if (naviBuffer[nz - 1][nx - 1] != -1) {
				int ldtile = naviBuffer[nz - 1][nx - 1];
				ntvec[i].leftDownTile.emplace_back(ldtile);
			}
		}
		//오른위
		if ((nx + (int(scale.x) / 2)) < 257 && (nz + (int(scale.z) / 2)) < 257) {
			if (naviBuffer[nz + (int(scale.z) / 2)][nx + (int(scale.x) / 2)] != -1) {
				int rutile = naviBuffer[nz + (int(scale.z) / 2)][nx + (int(scale.x) / 2)];
				ntvec[i].rightupTile.emplace_back(rutile);
			}
		}
		//오른 아래 
		if ((nx + (int(scale.x) / 2)) < 257 && (nz - 1) >= 0) {
			if (naviBuffer[nz - 1][nx + (int(scale.x) / 2)] != -1) {
				int rdtile = naviBuffer[nz - 1][nx + (int(scale.x) / 2)];
				ntvec[i].rightDownTile.emplace_back(rdtile);
			}
		}
	}



	vertexBuffer = ::CreateBufferResource(device, commandList, diffuseVertex, stride * verticesNum,
		D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);

	vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
	vertexBufferView.StrideInBytes = stride;
	vertexBufferView.SizeInBytes = stride * verticesNum;


	indicesNum = subIndex.size() * 3;
	
		if (indicesNum > 0) primitiveNum = (primitiveTopology ==
			D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ? (indicesNum / 3) : (indicesNum - 2);
		indexBuffer = CreateBufferResource(device, commandList, NULL, sizeof(UINT) * indicesNum, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_INDEX_BUFFER, NULL);
		indexBuffer->Map(0, NULL, (void**)&indices);
		indexBufferView.BufferLocation = indexBuffer->GetGPUVirtualAddress();
		indexBufferView.Format = DXGI_FORMAT_R32_UINT;
		indexBufferView.SizeInBytes = sizeof(UINT) * indicesNum;
	

	for (int i = 0; i < subIndex.size(); ++i) {
		indices[i * 3 + 0] = subIndex[i].GetIndex()[0];
		indices[i * 3 + 1] = subIndex[i].GetIndex()[1];
		indices[i * 3 + 2] = subIndex[i].GetIndex()[2];
	}


}

void NaviMesh::ReCreateNaviMesh(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int width, int length, XMFLOAT3 scale, std::vector<BoundingOrientedBox>& tbvec, XMFLOAT4X4 tinverseWorld, Diffused2TexturedVertex * terrainvertices, NaviMesh& fNavimesh , int(*naviBuffer)[257] , std::vector<NaviTile>& ntvec)
{

	int startnt = ntvec.size();

	isCreate = true;
	if (vertexBuffer)
		vertexBuffer->Release();
	if (indexBuffer) {
		indexBuffer->Release();
	}
	if (diffuseVertex) {
		delete[] diffuseVertex;
		diffuseVertex = NULL;
		leftIndex.clear();
	}

	auto livec = fNavimesh.GetLeftIndex();//앞에 네비메쉬의 버려진 인덱스들 
	stride = sizeof(DiffusedVertex);
	//버텍스 개수를 모르니깐 일단 vector로 생성하자 
	primitiveTopology = D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST;
	pickOffset = (primitiveTopology == D3D_PRIMITIVE_TOPOLOGY_TRIANGLELIST) ? 3 : 1;
	std::vector<DiffusedVertex> tdv;
	//점부터 넣어주자
	for (int i = 0; i < livec.size(); ++i) {
		int x = livec[i].Getx();
		int z = livec[i].Getz();
		//검사
		for (int j = 0; j < 4; ++j) { //타일만큼 루프 
			int tempx = x;
			int tempz = z;
			bool isBreak = false;
			
			XMFLOAT3 centerPoint;
			if (j == 0)
				centerPoint = terrainvertices[tempx + (tempz * 257)].position;
			if (j == 1) {
				centerPoint = Vector3::Add(terrainvertices[tempx + (tempz * 257)].position, XMFLOAT3(scale.x, 0.0f, 0.0f));
				tempx = x + int(scale.x / 2);
				centerPoint.y = terrainvertices[tempx + (257 * (tempz))].position.y;
			}
			if (j == 2) {
				centerPoint = Vector3::Add(terrainvertices[tempx + (tempz * 257)].position, XMFLOAT3(0.0f, 0.0f, scale.z));
				tempz = z + int(scale.z / 2);
				centerPoint.y = terrainvertices[tempx + (257 * (tempz))].position.y;
			}
			if (j == 3) {
				centerPoint = Vector3::Add(terrainvertices[tempx + (tempz * 257)].position, XMFLOAT3(scale.x, 0.0f, scale.z));
				tempx = x + int(scale.x / 2);
				tempz = z + int(scale.z / 2);
				centerPoint.y = terrainvertices[tempx + (257 * (tempz))].position.y;
			}

			//삼각형 아래 
			auto v1 = centerPoint; //중심점 
			auto v2 = Vector3::Add(centerPoint, XMFLOAT3(0.0f, 0.0f, scale.z));
			v2.y = terrainvertices[tempx + (257 * (tempz + int(scale.z / 2)))].position.y;
			auto v3 = Vector3::Add(centerPoint, XMFLOAT3(scale.x, 0.0f, 0.0f));
			v3.y = terrainvertices[(tempx + int(scale.x / 2)) + (257 * (tempz))].position.y;

			auto iv1 = XMLoadFloat3(&v1);
			auto iv2 = XMLoadFloat3(&v2);
			auto iv3 = XMLoadFloat3(&v3);

			//삼각형 위 
			auto v4 = Vector3::Add(centerPoint, XMFLOAT3(0.0f, 0.0f, scale.z));
			v4.y = terrainvertices[tempx + (257 * (tempz + int(scale.z / 2)))].position.y;

			auto v5 = Vector3::Add(centerPoint, XMFLOAT3(scale.x, 0.0f, scale.z));
			v5.y = terrainvertices[tempx + int(scale.x / 2) + (257 * (tempz + int(scale.z / 2)))].position.y;

			auto v6 = Vector3::Add(centerPoint, XMFLOAT3(scale.x, 0.0f, 0.0f));
			v6.y = terrainvertices[(tempx + int(scale.x / 2)) + (257 * (tempz))].position.y;

			auto iv4 = XMLoadFloat3(&v4);
			auto iv5 = XMLoadFloat3(&v5);
			auto iv6 = XMLoadFloat3(&v6);

			for (int k = 0; k < tbvec.size(); ++k) {
				BoundingOrientedBox toriBox;
				tbvec[k].Transform(toriBox, XMLoadFloat4x4(&tinverseWorld));

				if (toriBox.Intersects(iv1, iv2, iv3))
					isBreak = true;


				if (toriBox.Intersects(iv4, iv5, iv6))
					isBreak = true;
				
			
			}
			if (isBreak == false) {
				tdv.emplace_back(DiffusedVertex(v1, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f)));
				tdv.emplace_back(DiffusedVertex(v2, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f)));
				tdv.emplace_back(DiffusedVertex(v3, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f)));
				tdv.emplace_back(DiffusedVertex(v4, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f)));
				tdv.emplace_back(DiffusedVertex(v5, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f)));
				tdv.emplace_back(DiffusedVertex(v6, XMFLOAT4(0.0f, 0.0f, 0.0f, 1.0f)));



				//타일을 넣어주자.
			
				Tri tri0{ v1, v2, v3 };
				Tri tri1{ v4, v5, v6 };

				NaviTile nt{ tri0 , tri1 };
				nt.x = tempx;
				nt.z = tempz;
				nt.width = (int(scale.x) / 2);


				for (int tz = tempz; tz < (tempz + (int(scale.z) / 2)); ++tz) {
					for (int tx = tempx; tx < (tempx + (int(scale.x) / 2)); ++tx) {
						naviBuffer[tz][tx] = ntvec.size();
					}
				}
				nt.idx = ntvec.size();
				ntvec.emplace_back(nt);

			}
			else {
				LeftIndex tleft;
				tleft.x = tempx;
				tleft.z = tempz;
				leftIndex.emplace_back(tleft);
			}
		}
	}
	verticesNum = tdv.size();


	//다했다면 이제 인접성 검사를해야한다.
	for (int i = startnt; i < ntvec.size(); ++i) {
		int nx = ntvec[i].x;
		int nz = ntvec[i].z;

		//왼쪽
		if ((nx - 1) >= 0) {

			if (naviBuffer[nz][nx - 1] != -1) {
				int ltile = naviBuffer[nz][nx - 1];
				ntvec[i].leftTile.emplace_back(ltile);

				if (ntvec[ltile].width > ntvec[i].width) {
					ntvec[ltile].rightTile.emplace_back(i);
				}

			}
		}
		// 오른쪽 
		if ((nx + (int(scale.x) / 2)) < 257) {
			if (naviBuffer[nz][nx + (int(scale.x) / 2)] != -1) {
				int rtile = naviBuffer[nz][nx + (int(scale.x) / 2)];
				ntvec[i].rightTile.emplace_back(rtile);

				if (ntvec[rtile].width > ntvec[i].width) {
					ntvec[rtile].leftTile.emplace_back(i);
				}
			}
		}
		//  아래
		if ((nz - 1) >= 0) {
			if (naviBuffer[nz - 1][nx] != -1) {
				int dtile = naviBuffer[nz - 1][nx];
				ntvec[i].downTile.emplace_back(dtile);

				if (ntvec[dtile].width > ntvec[i].width) {
					ntvec[dtile].upTile.emplace_back(i);
				}
			}
		}
		//위
		if ((nz + (int(scale.z) / 2)) < 257) {
			if (naviBuffer[nz + (int(scale.z) / 2)][nx] != -1) {
				int utile = naviBuffer[nz + (int(scale.z) / 2)][nx];
				ntvec[i].upTile.emplace_back(utile);

				if (ntvec[utile].width > ntvec[i].width) {
					ntvec[utile].downTile.emplace_back(i);
				}

			}
		}
		//왼위
		if ((nx - 1) >= 0 && (nz + (int(scale.z) / 2)) < 257) {
			if (naviBuffer[nz + (int(scale.z) / 2)][nx - 1] != -1) {
				int lutile = naviBuffer[nz + (int(scale.z) / 2)][nx - 1];
				ntvec[i].leftupTile.emplace_back(lutile);

				if (ntvec[lutile].width > ntvec[i].width) {
					ntvec[lutile].rightDownTile.emplace_back(i);
				}
			}
		}
		//왼아래
		if ((nx - 1) >= 0 && (nz - 1) >= 0) {
			if (naviBuffer[nz - 1][nx - 1] != -1) {
				int ldtile = naviBuffer[nz - 1][nx - 1];
				ntvec[i].leftDownTile.emplace_back(ldtile);

				if (ntvec[ldtile].width > ntvec[i].width) {
					ntvec[ldtile].rightupTile.emplace_back(i);
				}

			}
		}
		//오른위
		if ((nx + (int(scale.x) / 2)) < 257 && (nz + (int(scale.z) / 2)) < 257) {
			if (naviBuffer[nz + (int(scale.z) / 2)][nx + (int(scale.x) / 2)] != -1) {
				int rutile = naviBuffer[nz + (int(scale.z) / 2)][nx + (int(scale.x) / 2)];
				ntvec[i].rightupTile.emplace_back(rutile);


				if (ntvec[rutile].width > ntvec[i].width) {
					ntvec[rutile].leftDownTile.emplace_back(i);
				}

			}
		}
		//오른 아래 
		if ((nx + (int(scale.x) / 2)) < 257 && (nz - 1) >= 0) {
			if (naviBuffer[nz - 1][nx + (int(scale.x) / 2)] != -1) {
				int rdtile = naviBuffer[nz - 1][nx + (int(scale.x) / 2)];
				ntvec[i].rightDownTile.emplace_back(rdtile);

				if (ntvec[rdtile].width > ntvec[i].width) {
					ntvec[rdtile].leftupTile.emplace_back(i);
				}
			}
		}
	}


	if (verticesNum > 0) {
		diffuseVertex = new DiffusedVertex[verticesNum];

		for (int i = 0; i < verticesNum; ++i) {
			diffuseVertex[i] = tdv[i];
		}

		if (verticesNum != 0) {
			vertexBuffer = ::CreateBufferResource(device, commandList, diffuseVertex, stride * verticesNum,
				D3D12_HEAP_TYPE_DEFAULT, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, &vertexUploadBuffer);

			vertexBufferView.BufferLocation = vertexBuffer->GetGPUVirtualAddress();
			vertexBufferView.StrideInBytes = stride;
			vertexBufferView.SizeInBytes = stride * verticesNum;
		}
	}
	else {
		isCreate = false;
	}
}
