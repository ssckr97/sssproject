#include "SaveFile.h"

SaveFile::SaveFile()
{
}

void SaveFile::SetInstanceBuffer(std::vector<InstanceBuffer*> instanceBuffer)
{
	for (int i = 0; i < instanceBuffer.size() - 1; ++i)
	{
		instanceBufferSize = instanceBuffer.size() - 1;
		saveInstanceBuffer[i].objectNumber = instanceBuffer[i]->objects.size();
		for (int j = 0; j < saveInstanceBuffer[i].objectNumber; ++j){
			auto temp = instanceBuffer[i]->objects[j]->GetWorld();
			temp._44 = 1.0f;
			saveInstanceBuffer[i].objectWorld.emplace_back(temp);
		}

	}
}

void SaveFile::SetSkinedInstanceBuffer(std::vector<SkinnedInstanceBuffer*> instanceBuffer)
{
	for (int i = 0; i < instanceBuffer.size(); ++i)
	{
		instanceBufferSize = instanceBuffer.size();
		saveInstanceBuffer[i].objectNumber = instanceBuffer[i]->objects.size();
		for (int j = 0; j < saveInstanceBuffer[i].objectNumber; ++j) {
			saveInstanceBuffer[i].objectWorld.emplace_back(instanceBuffer[i]->objects[j]->GetWorld());
		}

	}
}

