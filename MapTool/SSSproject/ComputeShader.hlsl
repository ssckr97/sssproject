cbuffer cbTimeInfo : register(b0)
{
    float gfTotalTime : packoffset(c0.x);
    float gfTimeElapsed : packoffset(c0.y);
}

cbuffer cbParticleInfo : register(b1)
{
    int gnParticleType : packoffset(c0.x);
    float gfParticleAcc : packoffset(c0.y);
    int gnParticleSize : packoffset(c0.z);
    float gfParticleAge : packoffset(c0.w);
    float gfRadius : packoffset(c1.x);
    float gfYgab : packoffset(c1.y);
    float gfRotateAcc : packoffset(c1.z);
}

struct ParticleData
{
    float3 position;
    float3 velocity;
    float age;
};

#define BLOCKSIZE 128
#define MAX_PARTICLESIZE 1024

#define PARTICLE_TYPE_DISAPPEAR 0
#define PARTICLE_TYPE_FOREVER 1
#define PARTICLE_TYPE_TORNADO 2
#define PARTICLE_TYPE_SMALLER 3

#define PI 3.141592

groupshared float3 sharedPos[BLOCKSIZE];

StructuredBuffer<ParticleData> gsbReadParticleData : register(t0);
StructuredBuffer<ParticleData> gsbCopyParticleData : register(t1);
RWStructuredBuffer<ParticleData> gsbWriteParticleData : register(u0);

[numthreads(BLOCKSIZE, 1, 1)]
void main(uint Gid : SV_GroupID, uint3 DTid : SV_DispatchThreadID, uint3 GTid : SV_GroupThreadID, uint GI : SV_GroupIndex)
{
    float3 position = gsbReadParticleData[DTid.x].position;
    float3 velocity = normalize(gsbReadParticleData[DTid.x].velocity) * gfParticleAcc;
    float age = gsbReadParticleData[DTid.x].age;
    
    if (gnParticleType == PARTICLE_TYPE_TORNADO)
    {
        float satAge = age / gfParticleAge * gfRotateAcc;
        velocity.x += gfRadius * cos(satAge * 4 * PI);
        velocity.z += gfRadius * sin(satAge * 4 * PI);
        velocity.y += gfYgab;
    }
    
    position.xyz += velocity.xyz * gfTimeElapsed;
    age -= gfTimeElapsed;

   
    ParticleData temp;
    temp.position = position;
    temp.velocity = velocity * gfTimeElapsed;
    temp.age = age;
    
    if (DTid.x < gnParticleSize)
    {
        if (age < gfParticleAge)
        {
            gsbWriteParticleData[DTid.x] = temp;
        }
        else
        {
            gsbWriteParticleData[DTid.x].age = temp.age;
        }
        if (age <= 0.0f)
        {
            if (gnParticleType == PARTICLE_TYPE_FOREVER)
            {
                gsbWriteParticleData[DTid.x].age = gsbCopyParticleData[DTid.x].age;
                gsbWriteParticleData[DTid.x].velocity *= -1;
            }
            else
            {
                gsbWriteParticleData[DTid.x].age = gsbCopyParticleData[DTid.x].age;
                gsbWriteParticleData[DTid.x].position = gsbCopyParticleData[DTid.x].position;
                gsbWriteParticleData[DTid.x].velocity = gsbCopyParticleData[DTid.x].velocity;
            }
           
        }
        
    }
}