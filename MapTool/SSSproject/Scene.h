#pragma once

#include "Timer.h"
#include "Shader.h"
#include "Camera.h"
#include "Material.h"
#include "ComputeShader.h"
#include "InstanceShader.h"
#define MAX_LIGHTS					50

#define MAX_MATERIALS				700

#define POINT_LIGHT					1
#define SPOT_LIGHT					2
#define DIRECTIONAL_LIGHT			3

//struct LIGHT
//{
//	int type;					// 조명의 타입
//	bool enable;				// 조명 on/off 여부
//
//	XMFLOAT4 ambient;	
//	XMFLOAT4 diffuse;
//	XMFLOAT4 specular;
//	XMFLOAT3 position;
//	XMFLOAT3 direction;
//	XMFLOAT3 attenuation;		// 점 조명은 거리에 비례해서 빛의 양이 점차적으로 줄어듬
//								// ex) Laf(i) = 1 / (Attenuation0 + (Attenuation1 * D) + (Attenuation2 * D * D))
//	float range;				// 점과 광원의 거리가 range보다 크면 조명의 영향을 받지 않는다.
//	float falloff;				// 안쪽 원의 끝에서 바깥 원 방향으로 감쇠가 일어난다.
//	float theta;				// 안쪽 원 cos(m_fTheta)
//	float phi;					// 바깥 원 cos(m_fPhi)
//	float padding;
//
//};

struct MATERIAL;

struct LIGHT
{
	XMFLOAT4				ambient;
	XMFLOAT4				diffuse;
	XMFLOAT4				specular;
	XMFLOAT3				position;
	float 					falloff;
	XMFLOAT3				direction;
	float 					theta; //cos(m_fTheta)
	XMFLOAT3				attenuation;
	float					phi; //cos(m_fPhi)
	bool					enable{ false };
	int						type;
	float					range;
	float					padding;
};

struct LIGHTS
{
	LIGHT lights[MAX_LIGHTS];
	XMFLOAT4 globalAmbient;
};
struct MATERIALS
{
	MATERIAL reflections[MAX_MATERIALS];
};

struct SRVROOTARGUMENTINFO;

class Scene
{
private:
	XMFLOAT3 terrainScale;

public:
	Scene(ID3D12Resource* terrainDepthResource, ID3D12Resource* terrainAlphaResource0, XMFLOAT3 terrainScale, ID3D12Resource* terrainHeightResource, ID3D12Resource* terrainAlphaResource1, ID3D12Resource* terrainAlphaResource2, ID3D12Resource* depthStencilBuffer);
	~Scene();

	bool OnProcessingMouseMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam);
	bool OnProcessingKeyboardMessage(HWND hwnd, UINT messageID, WPARAM wparam, LPARAM lparam);
	//씬에서 마우스와 키보드 메시지를 처리한다.

	void BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);
	void ReleaseObjects();

	bool ProcessInput(UCHAR* keysBuffer);
	void AnimateObjects(float timeElapsed, Camera* camera);
	void Render(ID3D12GraphicsCommandList* commandList, Camera* camera , int decalShape);
	void BoundingRender(ID3D12GraphicsCommandList* commandList, Camera* camera , int bIdx);
	void PrepareRender(ID3D12GraphicsCommandList* commandList);
	void TerrainRender(ID3D12GraphicsCommandList* commandList, Camera* camera, int pipeCounter, int LayerIndex);
	void TerrainNaviRender(ID3D12GraphicsCommandList* commandList, Camera* camera);

	void SkyBoxRender(ID3D12GraphicsCommandList* commandList, Camera* camera);

	void CreateTerrainAlphaTexture(ID3D12Device* device, ID3D12GraphicsCommandList* commandList);

	void ReleaseUploadBuffers();

	ID3D12RootSignature *CreateGraphicsRootSignature(ID3D12Device* device);
	ID3D12RootSignature *CreateComputeRootSignature(ID3D12Device* device);
	ID3D12RootSignature *GetGraphicsRootSignature();
	void SetGraphicsRootSignature(ID3D12GraphicsCommandList *commandList) { commandList->SetGraphicsRootSignature(graphicsRootSignature); }

	//그래픽 루트 시그너쳐를 생성한다.

	void BuildLightsAndMaterials();
	//씬의 모든 조명과 재질을 생성

	virtual void CreateShaderVariables(ID3D12Device *device, ID3D12GraphicsCommandList *commandList);
	virtual void UpdateShaderVariables(ID3D12GraphicsCommandList *commandList);
	virtual void ReleaseShaderVariables();
	//씬의 모든 조명과 재질을 위한 리소스를 생성하고 갱신

	void GarbageCollection();

	static void CreateCbvSrvUavDescriptorHeaps(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, int nConstantBufferViews, int nShaderResourceViews, int nUnorderedAccessViews);
	static D3D12_GPU_DESCRIPTOR_HANDLE CreateConstantBufferViews(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, int nConstantBufferViews, ID3D12Resource *pd3dConstantBuffers, UINT nStride);
	static int CreateShaderResourceViews(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, Texture *pTexture, UINT nRootParameterStartIndex, D3D12_SHADER_RESOURCE_VIEW_DESC* desc = NULL);
	static int CreateStructuredShaderResourceViews(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, UINT numElements, UINT stride, ID3D12Resource* buffer, UINT rootParameterStartIndex);
	static int CreateStructuredUnorderedAccessViews(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, UINT numElements, UINT stride, ID3D12Resource* buffer, UINT rootParameterStartIndex);

	// 씬의 모든 뷰들과 디스크립터 힙을 생성. 

	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUDescriptorHandleForHeapStart() { return(cbvSrvUavDescriptorHeap->GetCPUDescriptorHandleForHeapStart()); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUDescriptorHandleForHeapStart() { return(cbvSrvUavDescriptorHeap->GetGPUDescriptorHandleForHeapStart()); }

	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUCbvDescriptorStartHandle() { return(cbvCPUDescriptorStartHandle); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUCbvDescriptorStartHandle() { return(cbvGPUDescriptorStartHandle); }
	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUSrvDescriptorStartHandle() { return(srvCPUDescriptorStartHandle); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUSrvDescriptorStartHandle() { return(srvGPUDescriptorStartHandle); }
	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUUavDescriptorStartHandle() { return(uavCPUDescriptorStartHandle); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUUavDescriptorStartHandle() { return(uavGPUDescriptorStartHandle); }

	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUCbvDescriptorNextHandle() { return(cbvCPUDescriptorNextHandle); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUCbvDescriptorNextHandle() { return(cbvGPUDescriptorNextHandle); }
	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUSrvDescriptorNextHandle() { return(srvCPUDescriptorNextHandle); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUSrvDescriptorNextHandle() { return(srvGPUDescriptorNextHandle); }
	D3D12_CPU_DESCRIPTOR_HANDLE GetCPUUavDescriptorNextHandle() { return(uavCPUDescriptorNextHandle); }
	D3D12_GPU_DESCRIPTOR_HANDLE GetGPUUavDescriptorNextHandle() { return(uavGPUDescriptorNextHandle); }
	// 디스크립터 핸들에 대한 Get 함수

	void CreateTextureResource(ID3D12Device *device, ID3D12GraphicsCommandList *commandList);
	static void SetRootArgument(int nIndex, UINT nRootParameterIndex, D3D12_GPU_DESCRIPTOR_HANDLE d3dsrvGpuDescriptorHandle);
	static SRVROOTARGUMENTINFO GetRootArgument(int idx);


	Player* player = NULL;
	Camera* camera = NULL;
	int objectsNum = 0;
	int texturesNum = 0;
	int fobjectsSize = 0;


	Shader** GetShaders() { return shaders; }
	int GetShaderNum() { return shadersNum; }
protected:

	std::vector<void*> objects;
	// terrain, helicoptor, tree ... 오브젝트들.
	std::vector<Texture*> textures;

	Shader** shaders = NULL;
	int shadersNum = 0;

	ID3D12RootSignature* graphicsRootSignature = NULL;
	ID3D12RootSignature* computeRootSignature = NULL;
	ID3D12Resource * depthStencilBuffer = NULL;

	HeightMapTerrain*	terrain = NULL;

	LIGHTS* lights = NULL;

	ID3D12Resource* cbLights = NULL;
	LIGHTS* cbMappedLights = NULL;

	MATERIALS* materials = NULL;
	int materialsNum = 0;
	

	ID3D12Resource* depthResource = NULL;
	ID3D12Resource* terrainDepthResource = NULL;
	ID3D12Resource* terrainAlphaResource0 = NULL;
	ID3D12Resource* terrainAlphaResource1 = NULL;
	ID3D12Resource* terrainAlphaResource2 = NULL;
	ID3D12Resource* terrainHeightResource = NULL;


	ID3D12Resource* cbMaterials = NULL;
	ID3D12Resource* materialUploadBuffer = NULL;
	MATERIAL* cbMappedMaterials = NULL;

	static ID3D12DescriptorHeap			*cbvSrvUavDescriptorHeap;

	static D3D12_CPU_DESCRIPTOR_HANDLE		cbvCPUDescriptorStartHandle;
	static D3D12_GPU_DESCRIPTOR_HANDLE		cbvGPUDescriptorStartHandle;
	static D3D12_CPU_DESCRIPTOR_HANDLE		srvCPUDescriptorStartHandle;
	static D3D12_GPU_DESCRIPTOR_HANDLE		srvGPUDescriptorStartHandle;
	static D3D12_CPU_DESCRIPTOR_HANDLE		uavCPUDescriptorStartHandle;
	static D3D12_GPU_DESCRIPTOR_HANDLE		uavGPUDescriptorStartHandle;

	static D3D12_CPU_DESCRIPTOR_HANDLE		cbvCPUDescriptorNextHandle;
	static D3D12_GPU_DESCRIPTOR_HANDLE		cbvGPUDescriptorNextHandle;
	static D3D12_CPU_DESCRIPTOR_HANDLE		srvCPUDescriptorNextHandle;
	static D3D12_GPU_DESCRIPTOR_HANDLE		srvGPUDescriptorNextHandle;
	static D3D12_CPU_DESCRIPTOR_HANDLE		uavCPUDescriptorNextHandle;
	static D3D12_GPU_DESCRIPTOR_HANDLE		uavGPUDescriptorNextHandle;

	static std::vector<SRVROOTARGUMENTINFO> rootArgumentInfos;
	static int rootArgumentCurrentIdx;

	std::vector<int> rootArgumentIdx;
	std::vector<MATERIAL> materialReflection;

	TEXTURENAME textureName;
	int textureIdx[5] = { 0 };	// Diffuse : 0, Normal : 1, Specular : 2, Metallic : 3, Emission : 4


public:
	LIGHTS* GetLights() { return lights; }
	HeightMapTerrain* GetTerrain() { return (terrain); }
	std::vector<void*> GetModels() { return objects; }
};