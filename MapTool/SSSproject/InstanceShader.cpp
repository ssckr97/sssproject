#include "stdafx.h"
#include "InstanceShader.h"
#include "Material.h"
#include "Scene.h"
std::uniform_real_distribution<float> suid(-0.3f, 0.3f);
std::default_random_engine  sdre;
InstancingShader::InstancingShader()
{
}
InstancingShader::~InstancingShader()
{
}

// 0 : Default 1 : Blending 2: Billboard(GS)
void InstancingShader::CreateShader(ID3D12Device* device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = InstanceShaderPS_Num;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];
	Shader::CreateShader(device, graphicsRootSignature);
	auto boundingPipeLineStateDesc = pipelineStateDesc;

	pipelineStateDesc.BlendState.AlphaToCoverageEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].BlendEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[1]);
	// 블렌딩 오브젝트.

	pipelineStateDesc.RasterizerState.CullMode = D3D12_CULL_MODE_NONE;
	hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[2]);

	UINT inputElementDescsNum = 2;
	D3D12_INPUT_ELEMENT_DESC *inputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	inputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, 0, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	inputElementDescs[1] = { "BILLBOARDINFO", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	pipelineStateDesc.InputLayout.NumElements = inputElementDescsNum;
	pipelineStateDesc.InputLayout.pInputElementDescs = inputElementDescs;

	pipelineStateDesc.VS = Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSBillboard", "vs_5_1", &vertexShaderBlob);
	pipelineStateDesc.GS = Shader::CompileShaderFromFile(L"Shaders.hlsl", "GSBillboard", "gs_5_1", &geometryShaderBlob);
	pipelineStateDesc.PS = Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSBillboard", "ps_5_1", &pixelShaderBlob);

	pipelineStateDesc.PrimitiveTopologyType = D3D12_PRIMITIVE_TOPOLOGY_TYPE_POINT;

	hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[3]);
	// 빌보드(기하쉐이더) 오브젝트

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();
	if (geometryShaderBlob) geometryShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;

	inputElementDescsNum = 8;
	D3D12_INPUT_ELEMENT_DESC *BoundinginputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	BoundinginputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	BoundinginputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	BoundinginputElementDescs[2] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[3] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[4] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[5] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[6] = { "ISDUMMY", 0, DXGI_FORMAT_R8_UINT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[7] = { "BIDX", 0, DXGI_FORMAT_R32_UINT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

	// 첫번째가 시멘틱 이름 , 두번째가 시멘틱 번호 , 세번째가 변수 포멧 , 네번째가 버퍼 슬릇 , 다섯번째는 그냥 오브셋 , 여섯번째는 버텍스 데이터인지 인스턴스 데이터인지 , 일곱번째도 같다 .

	boundingPipeLineStateDesc.InputLayout.NumElements = inputElementDescsNum;
	boundingPipeLineStateDesc.InputLayout.pInputElementDescs = BoundinginputElementDescs;

	boundingPipeLineStateDesc.RasterizerState.FillMode = D3D12_FILL_MODE_WIREFRAME;
	boundingPipeLineStateDesc.VS = Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSBOUNDING", "vs_5_1", &vertexShaderBlob);
	boundingPipeLineStateDesc.PS = Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSBOUNDING", "ps_5_1", &pixelShaderBlob);

	hResult = device->CreateGraphicsPipelineState(&boundingPipeLineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[4]);



	//바운딩박스 오브젝트 

	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (boundingPipeLineStateDesc.InputLayout.pInputElementDescs) delete[] boundingPipeLineStateDesc.InputLayout.pInputElementDescs;


}

void InstancingShader::CreateShaderVariables(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, int idx)
{
	if (instanceBuffer[idx]->isBillboard < 0)
	{
		UINT tempObjectByte = 0;
		ID3D12Resource** tempObject = NULL;
		VS_VB_INSTANCE_OBJECT** tempMappedObject = NULL;
		D3D12_VERTEX_BUFFER_VIEW* tempObjectBufferView = NULL;


		UINT tempMaterialByte = 0;
		ID3D12Resource*** tempMaterial = NULL;
		VS_VB_INSTANCE_MATERIAL*** tempMappedMaterial = NULL;
		D3D12_VERTEX_BUFFER_VIEW** tempMaterialBufferView = NULL;


		tempObjectByte = sizeof(VS_VB_INSTANCE_OBJECT);
		tempObject = new ID3D12Resource*[instanceBuffer[idx]->framesNum];
		tempMappedObject = new VS_VB_INSTANCE_OBJECT*[instanceBuffer[idx]->framesNum];
		tempObjectBufferView = new D3D12_VERTEX_BUFFER_VIEW[instanceBuffer[idx]->framesNum];

		tempMaterialByte = sizeof(VS_VB_INSTANCE_MATERIAL);
		tempMaterial = new ID3D12Resource**[instanceBuffer[idx]->framesNum];
		tempMappedMaterial = new VS_VB_INSTANCE_MATERIAL**[instanceBuffer[idx]->framesNum];
		tempMaterialBufferView = new D3D12_VERTEX_BUFFER_VIEW*[instanceBuffer[idx]->framesNum];

		for (int i = 0; i < instanceBuffer[idx]->framesNum; ++i)
		{
			tempMaterial[i] = new ID3D12Resource*[instanceBuffer[idx]->materialsNum];
			tempMappedMaterial[i] = new VS_VB_INSTANCE_MATERIAL*[instanceBuffer[idx]->materialsNum];
			tempMaterialBufferView[i] = new D3D12_VERTEX_BUFFER_VIEW[instanceBuffer[idx]->materialsNum];
		}

		for (int i = 0; i < instanceBuffer[idx]->framesNum; ++i)
		{
			tempObject[i] = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTANCE_OBJECT) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
			//인스턴스 정보를 저장할 정점 버퍼를 업로드 힙 유형으로 생성한다.

			tempObject[i]->Map(0, NULL, (void **)&tempMappedObject[i]);
			//정점 버퍼(업로드 힙)에 대한 포인터를 저장한다.

			tempObjectBufferView[i].BufferLocation = tempObject[i]->GetGPUVirtualAddress();
			tempObjectBufferView[i].StrideInBytes = tempObjectByte;
			tempObjectBufferView[i].SizeInBytes = tempObjectByte * ((UINT)BUFFERSIZE);
			//정점 버퍼에 대한 뷰를 생성한다.


			for (int j = 0; j < instanceBuffer[idx]->materialsNum; ++j)
			{
				tempMaterial[i][j] = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTANCE_MATERIAL) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

				tempMaterial[i][j]->Map(0, NULL, (void **)&tempMappedMaterial[i][j]);

				tempMaterialBufferView[i][j].BufferLocation = tempMaterial[i][j]->GetGPUVirtualAddress();
				tempMaterialBufferView[i][j].StrideInBytes = tempMaterialByte;
				tempMaterialBufferView[i][j].SizeInBytes = tempMaterialByte * ((UINT)BUFFERSIZE);
			}
		}

		//바운딩 정점 버퍼 생성 
		if (instanceBuffer[idx]->objects[0]->GetBoundingObject() != NULL) {
			for (int i = 0; i < instanceBuffer[idx]->objects[0]->GetBoundingObject()->size(); ++i) {
				//boundingBuffer 
				UINT tempBoundingByte = 0;
				ID3D12Resource* tempBounding = NULL;
				VS_VB_INSTACE_BOUNDING* tempMappedBounding = NULL;
				D3D12_VERTEX_BUFFER_VIEW tempBoundingBufferView;

				tempBoundingByte = sizeof(VS_VB_INSTACE_BOUNDING);

				tempBounding = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTACE_BOUNDING) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
				//인스턴스 정보를 저장할 정점 버퍼를 업로드 힙 유형으로 생성한다.

				tempBounding->Map(0, NULL, (void **)&tempMappedBounding);
				//정점 버퍼(업로드 힙)에 대한 포인터를 저장한다.

				tempBoundingBufferView.BufferLocation = tempBounding->GetGPUVirtualAddress();
				tempBoundingBufferView.StrideInBytes = tempBoundingByte;
				tempBoundingBufferView.SizeInBytes = tempBoundingByte * ((UINT)BUFFERSIZE);
				//정점 버퍼에 대한 뷰를 생성한다.

				instanceBuffer[idx]->vbBoundingBytes = tempBoundingByte;
				instanceBuffer[idx]->vbBounding.emplace_back(tempBounding);
				instanceBuffer[idx]->vbMappedBounding.emplace_back(tempMappedBounding);
				instanceBuffer[idx]->instancingBoundingBufferView.emplace_back(tempBoundingBufferView);
			}
		}
		instanceBuffer[idx]->vbGameObjectBytes = tempObjectByte;
		instanceBuffer[idx]->vbGameObjects.emplace_back(tempObject);
		instanceBuffer[idx]->vbMappedGameObjects.emplace_back(tempMappedObject);
		instanceBuffer[idx]->instancingGameObjectBufferView.emplace_back(tempObjectBufferView);


		instanceBuffer[idx]->vbMaterialBytes = tempMaterialByte;
		instanceBuffer[idx]->vbMaterials.emplace_back(tempMaterial);
		instanceBuffer[idx]->vbMappedMaterials.emplace_back(tempMappedMaterial);
		instanceBuffer[idx]->instancingMaterialBufferView.emplace_back(tempMaterialBufferView);

	}
	// 빌보드 오브젝트
	else
	{
		UINT tempBillboardByte = 0;
		ID3D12Resource* tempBillboard = NULL;
		VS_VB_INSTANCE_BILLBOARD* tempMappedBillboard = NULL;
		D3D12_VERTEX_BUFFER_VIEW tempBillboardBufferView;

		tempBillboardByte = sizeof(VS_VB_INSTANCE_BILLBOARD);
		tempMappedBillboard = new VS_VB_INSTANCE_BILLBOARD;

		tempBillboard = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTANCE_BILLBOARD) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);

		tempBillboard->Map(0, NULL, (void **)&tempMappedBillboard);

		tempBillboardBufferView.BufferLocation = tempBillboard->GetGPUVirtualAddress();
		tempBillboardBufferView.StrideInBytes = tempBillboardByte;
		tempBillboardBufferView.SizeInBytes = tempBillboardByte * ((UINT)BUFFERSIZE);

		instanceBuffer[idx]->vbBillboardObjectBytes = tempBillboardByte;
		instanceBuffer[idx]->vbBillboardObjects.emplace_back(tempBillboard);
		instanceBuffer[idx]->vbMappedBillboardObjects.emplace_back(tempMappedBillboard);
		instanceBuffer[idx]->instancingBillboardObjectBufferView.emplace_back(tempBillboardBufferView);
	}
}
void InstancingShader::ReleaseShaderVariables()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (!instanceBuffer[k]->vbGameObjects.empty())
			for (int m = 0; m < instanceBuffer[k]->vbGameObjects.size(); ++m)
				for (int j = 0; j < instanceBuffer[k]->framesNum; ++j)
				{
					instanceBuffer[k]->vbGameObjects[m][j]->Unmap(0, NULL);
					instanceBuffer[k]->vbGameObjects[m][j]->Release();
				}

		if (!instanceBuffer[k]->vbMaterials.empty())
			for (int m = 0; m < instanceBuffer[k]->vbMaterials.size(); ++m)
				for (int j = 0; j < instanceBuffer[k]->framesNum; ++j)
					for (int i = 0; i < instanceBuffer[k]->materialsNum; ++i)
					{
						instanceBuffer[k]->vbMaterials[m][j][i]->Unmap(0, NULL);
						instanceBuffer[k]->vbMaterials[m][j][i]->Release();
					}

		if (!instanceBuffer[k]->vbBillboardObjects.empty())
			for (int m = 0; m < instanceBuffer[k]->vbBillboardObjects.size(); ++m)
			{
				instanceBuffer[k]->vbBillboardObjects[m]->Unmap(0, NULL);
				instanceBuffer[k]->vbBillboardObjects[m]->Release();
			}
	}
}

void InstancingShader::BuildObjects(ID3D12Device* device, ID3D12GraphicsCommandList* commandList, ID3D12RootSignature *graphicsRootSignature, std::vector<void*>& context, int fobjectSize)
{
	objects.reserve(3);

	HeightMapTerrain *terrain = (HeightMapTerrain *)context[0];
	objects.emplace_back(terrain);
	float terrainWidth = terrain->GetWidth(), terrainLength = terrain->GetLength();

	// Cube Object Build
	InstanceBuffer* cubeObject = new InstanceBuffer();
	cubeObject->objectsNum = 3 + 1;		// + 1 : dummy
	cubeObject->objects.reserve(cubeObject->objectsNum);
	cubeObject->framesNum = 1;
	cubeObject->materialsNum = 1;
	cubeObject->pipeLineStateIdx = 0;

	_TCHAR* diffuseTextureName = L"Assets/Image/Object/Rocks.dds";
	_TCHAR* normalTextureName = L"Assets/Image/Object/RocksNormal.dds";

	cubeObject->textureName.diffuse.emplace_back(0, diffuseTextureName);
	cubeObject->textureName.normal.emplace_back(0, normalTextureName);

	RotatingObject* rotatingObject = NULL;
	RotatingObject* dummyCube = new RotatingObject();
	dummyCube->SetPosition(0.0f, 50.0f, 0.0f);
	dummyCube->CreateMaterials(1);
	Material* tt = new Material(1);
	tt->textureMask = MATERIAL_DIFFUSE_MAP + MATERIAL_NORMAL_MAP;
	tt->reflection = 0;
	tt->textureIdx = 0000;
	dummyCube->SetMaterial(0, tt);
	dummyCube->SetBoxType(false);
	cubeObject->objects.emplace_back(dummyCube);
	// dummy object

	for (int i = 1; i < cubeObject->objectsNum; ++i)
	{
		rotatingObject = new RotatingObject();
		float xPosition = (float)(i + 1) * 30;
		float zPosition = 0;
		rotatingObject->SetPosition(xPosition, 0, zPosition);
		rotatingObject->SetRotationAxis(XMFLOAT3(0.0f, 1.0f, 0.0f));
		rotatingObject->SetRotationSpeed(10.0f*(i % 10));
		rotatingObject->SetOBB(rotatingObject->GetPosition(), XMFLOAT3(12.0f, 18.0f, 6.0f), XMFLOAT4(0, 0, 0, 0));
		rotatingObject->CreateMaterials(1);
		Material* temp = new Material(1);
		temp->textureMask = MATERIAL_DIFFUSE_MAP + MATERIAL_NORMAL_MAP;
		temp->reflection = i;
		temp->textureIdx = 0000;
		rotatingObject->SetMaterial(0, temp);
		rotatingObject->SetBoxType(false);
		cubeObject->objects.emplace_back(rotatingObject);
	}
	//인스턴싱을 사용하여 렌더링하기 위하여 하나의 게임 객체만 메쉬를 가진다.
	CubeMeshNormalMapTextured *cubeMesh = new CubeMeshNormalMapTextured(device, commandList, 12.0f, 12.0f, 12.0f);
	BoundingBoxMesh *cubeBoundingMesh = NULL;
	cubeObject->objects[0]->SetMesh(0, cubeMesh);
	cubeObject->objects[0]->SetBoundingMesh(cubeBoundingMesh);
	instanceBuffer.emplace_back(cubeObject);







	//obejct Functinal
	int iter = 1;
	for (int i = 0; i < fobjectSize - 1; ++i) {
		InstanceBuffer* tempBuffer = new InstanceBuffer();
		GameObject* tempObjcet = (GameObject*)context[iter++];

		BuildInstanceObject(device, commandList, tempBuffer, 0, tempObjcet->GetPipeLineStatesNum(), tempObjcet, 0, XMFLOAT3{ 2.0f , 2.0f , 2.0f }, XMFLOAT3(0, 0, 0), terrain, XMFLOAT3(0, 0, 0));
	}

	//바운딩 박스 정보 읽기 
	std::ifstream in{ "Assets/Bounding/bInform.txt" ,std::ios::out | std::ios::binary };
	std::vector<bImform> bivec;

	for (int i = 0; i < instanceBuffer.size(); ++i) {
		if (instanceBuffer[i]->objects[0]->GetBoundingObject() != NULL) { // 큐브 때문에 이렇게 막음 

			bImform tb;
			in.read((char*)&tb.size, sizeof(int));
			tb.bworld.resize(tb.size);
			tb.tscale.resize(tb.size);
			tb.trotate.resize(tb.size);
			tb.tpos.resize(tb.size);
			if (tb.size == 0)
				continue;
			instanceBuffer[i]->objects[0]->GetobbVector().resize(tb.size);
			in.read((char*)&tb.bworld[0], sizeof(XMFLOAT4X4) * tb.size);
			in.read((char*)&tb.tscale[0], sizeof(XMFLOAT3) * tb.size);
			in.read((char*)&tb.trotate[0], sizeof(XMFLOAT3) * tb.size);
			in.read((char*)&tb.tpos[0], sizeof(XMFLOAT3) * tb.size);

			for (int j = 0; j < tb.size; ++j) {
				GameObject* BoundingObject = new GameObject();
				BoundingObject->SetWorld(tb.bworld[j]);
				BoundingObject->SetAllScale(tb.tscale[j].x , tb.tscale[j].y , tb.tscale[j].z);
				BoundingObject->GetInputAngle() = tb.trotate[j];
				BoundingObject->SettPos(tb.tpos[j]);
				instanceBuffer[i]->objects[0]->GetBoundingObject()->emplace_back(BoundingObject);
			}
		}
	}





	// Billboard Light
	InstanceBuffer* billboardLight = new InstanceBuffer();

	billboardLight->objectsNum = 0 + 1;
	billboardLight->objects.reserve(billboardLight->objectsNum);
	billboardLight->framesNum = 1;
	billboardLight->materialsNum = 1;
	billboardLight->pipeLineStateIdx = InstanceShaderPS_BIllboard;
	billboardLight->isBillboard = 0;

	BillboardObject* obj;
	BillboardObject* billboardDummy = new BillboardObject;
	BoundingBoxMesh *BillBoradTreeBoundingMesh = new BoundingBoxMesh(device, commandList, 6.0f, 6.0f, 6.0f);
	billboardDummy->SetBoundingMesh(BillBoradTreeBoundingMesh);
	billboardLight->objects.emplace_back(billboardDummy);

	instanceBuffer.emplace_back(billboardLight);

	AddLight(XMFLOAT3(0, 100, 0));

	for (int i = 0; i < instanceBuffer.size(); ++i)
		CreateShaderVariables(device, commandList, i);

	SetTextures(device, commandList);

}

void InstancingShader::AnimateObjects(float timeElapsed, Camera* camera)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		//ApplyLOD(camera, k);

		for (int j = 0; j < instanceBuffer[k]->objects.size(); ++j) // except dummy 
		{
			if (instanceBuffer[k]->objects[j])
			{
				if (k == 1)
				{
					if (j == 1)
						instanceBuffer[k]->objects[j]->Animate(timeElapsed);
				}
				else
					instanceBuffer[k]->objects[j]->Animate(timeElapsed);
				if (instanceBuffer[k]->isBillboard >= 0)
				{
					for (int m = 0; m < instanceBuffer[k]->vbMappedBillboardObjects.size(); ++m)
						instanceBuffer[k]->objects[j]->UpdateTransform(instanceBuffer[k]->vbMappedBillboardObjects[m], j - 1);
				}
				else {
					for (int m = 0; m < instanceBuffer[k]->vbMappedGameObjects.size(); ++m) {
						instanceBuffer[k]->objects[j]->UpdateTransform(instanceBuffer[k]->vbMappedGameObjects[m], j, NULL);
					}
					//vbMappedBounding// 한 오브젝트 종류에 1000개까지 오브젝트를 넣을 수 있는데 그 중 1개의 바운딩박스의 월드 변환 

					for (int m = 0; m < instanceBuffer[k]->vbMappedBounding.size(); ++m) {
						if (instanceBuffer[k]->objects[j]->GetBoundingObject() != NULL) {
							auto world = (*instanceBuffer[k]->objects[j]->GetBoundingObject())[m]->GetWorld();
							instanceBuffer[k]->objects[j]->UpdateBoundingTransForm(instanceBuffer[k]->vbMappedBounding[m], j, world, m);
						}
					}
				}

				//bounding Box Set
				if (instanceBuffer[k]->objects[0]->GetBoundingObject() != NULL) {
					std::vector<GameObject*>* bounding = instanceBuffer[k]->objects[j]->GetBoundingObject();
					for (int i = 0; i < bounding->size(); ++i) {
						std::vector<BoundingOrientedBox>& obvec = instanceBuffer[k]->objects[j]->GetobbVector();
						
						obvec[i].Center = XMFLOAT3(0.0f, 0.0f, 0.0f);
						obvec[i].Extents = XMFLOAT3(2.0f, 2.0f, 2.0f);
						obvec[i].Orientation = XMFLOAT4(0, 0, 0, 1.f);
						auto fworld = Matrix4x4::Multiply((*bounding)[i]->GetWorld(), instanceBuffer[k]->objects[j]->GetWorld());
						obvec[i].Transform(obvec[i], XMLoadFloat4x4(&fworld));
					}
				}				
			}
		}
	}
}

void InstancingShader::DeleteGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int bufferIndex, int objectIndex)
{
	GameObject* temp = instanceBuffer[bufferIndex]->objects[objectIndex];
	instanceBuffer[bufferIndex]->objects.erase(instanceBuffer[bufferIndex]->objects.begin() + objectIndex);
	instanceBuffer[bufferIndex]->objectsNum--;
	delete temp;
}

void InstancingShader::DeleteAllObjcet()
{
	for (int i = 0; i < instanceBuffer.size(); ++i) {
		while (true) {
			if (instanceBuffer[i]->objects.size() < 2) {
				break;
			}
			GameObject* temp = instanceBuffer[i]->objects[instanceBuffer[i]->objects.size() - 1];
			instanceBuffer[i]->objects.pop_back();
			delete temp;

		}
		instanceBuffer[i]->objectsNum = 1;
	}
}

void InstancingShader::AddGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT3 pointxyz, ID3D12RootSignature * graphicsRootSignature, int objectType , bool noiseFlag)
{
	GameObject* tempObject = new GameObject();
	tempObject->SetChild(objects[objectType]);
	tempObject->SetPosition(pointxyz);
	tempObject->SetOBB(tempObject->GetPosition(), instanceBuffer[objectType]->objects[0]->GetOBB().Extents, XMFLOAT4(0, 0, 0, 0));
	tempObject->CreateMaterials(1);
	tempObject->SetScale(1.0f, 1.0f, 1.0f);
	Material* tempMatrial = new Material(1);
	tempMatrial->reflection = 0;
	tempMatrial->textureMask = 0000;
	tempObject->SetMaterial(0, tempMatrial);
	tempObject->SetBoxType(false);

	XMFLOAT4X4 transForm = instanceBuffer[objectType]->objects[0]->GetTransform();
	if (instanceBuffer[objectType]->objects[0]->GetBoundingObject() != NULL) {
		std::vector<GameObject*>* boVec = instanceBuffer[objectType]->objects[0]->GetBoundingObject();
		tempObject->GetobbVector().resize(boVec->size());
	}
	tempObject->SetBoundingObject(instanceBuffer[objectType]->objects[0]->GetBoundingObject());
	
	if (noiseFlag) {
		float randomScale = suid(sdre);
		transForm._11 = transForm._11 + randomScale;
		transForm._22 = transForm._22 + randomScale;
		transForm._33 = transForm._33 + randomScale;
	}
	tempObject->SetTransform(transForm);

	

	instanceBuffer[objectType]->objects.emplace_back(tempObject);
	instanceBuffer[objectType]->objectsNum++;

}

void InstancingShader::AddLight(XMFLOAT3 pointxyz) 
{
	BillboardObject* tempObject = new BillboardObject();

	tempObject->billboardVertex = BillboardGSVertex(XMFLOAT3(pointxyz), XMFLOAT3(6, 6, 0));

	instanceBuffer.back()->objects.emplace_back(tempObject);
}

void InstancingShader::ModifyLight(int idx, XMFLOAT3 pointxyz)
{
	reinterpret_cast<BillboardObject*>(instanceBuffer.back()->objects[idx])->billboardVertex.position = pointxyz;
}

void InstancingShader::LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType) {


	GameObject* rotatingObject = new GameObject();
	if (objectType != 0 && objectType != (instanceBuffer.size() - 1)) {
		rotatingObject->SetChild(objects[objectType]);
	}
	rotatingObject->SetTransform(world);
	rotatingObject->SetOBB(rotatingObject->GetPosition(), XMFLOAT3(12.0f, 18.0f, 6.0f), XMFLOAT4(0, 0, 0, 0));
	rotatingObject->CreateMaterials(1);
	Material* temp = new Material(1);
	temp->textureMask = MATERIAL_DIFFUSE_MAP + MATERIAL_NORMAL_MAP;
	temp->reflection = 0;
	temp->textureIdx = 0000;
	rotatingObject->SetMaterial(0, temp);
	rotatingObject->SetBoxType(false);
	if (instanceBuffer[objectType]->objects[0]->GetBoundingObject() != NULL) {
		std::vector<GameObject*>* boVec = instanceBuffer[objectType]->objects[0]->GetBoundingObject();
		rotatingObject->GetobbVector().resize(boVec->size());
	}
	rotatingObject->SetBoundingObject(instanceBuffer[objectType]->objects[0]->GetBoundingObject());



	instanceBuffer[objectType]->objects.emplace_back(rotatingObject);
	instanceBuffer[objectType]->objectsNum++;

}


void InstancingShader::ReleaseObjects()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		instanceBuffer[k]->objects[0]->Release();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->Release();
	}
}

void InstancingShader::BuildInstanceObject(ID3D12Device* device, ID3D12GraphicsCommandList* commandList,
	InstanceBuffer * instanceObject, int objectNum, int pipeLineStateIdx, GameObject * objectModel, int modelNumber, XMFLOAT3 boundingSize, XMFLOAT3 rotate, HeightMapTerrain *terrain, XMFLOAT3 position)
{

	objects.emplace_back(objectModel);
	instanceObject->objectsNum = objectNum + 1; //dummy
	instanceObject->objects.reserve(instanceObject->objectsNum);
	instanceObject->framesNum = objectModel->GetFramesNum() + 1;
	instanceObject->materialsNum = objectModel->GetMaximumMaterialsNum();
	instanceObject->pipeLineStateIdx = pipeLineStateIdx;
	instanceObject->useBillboard = 0;
	instanceObject->textureName = objectModel->textureName;

	GameObject* dummy = new GameObject();
	BoundingBoxMesh *tempBounding = new BoundingBoxMesh(device, commandList, boundingSize.x, boundingSize.y, boundingSize.z);
	objectModel->AddRef();
	dummy->SetChild(objectModel);

	dummy->SetPosition(0, 50, 0);
	dummy->SetBoxType(false);
	dummy->SetOBB(dummy->GetPosition(), boundingSize, XMFLOAT4(0, 0, 0, 0));
	dummy->SetScale(1.0f, 1.0f, 1.0f);
	dummy->SetBoundingMesh(tempBounding);
	dummy->allocateBvec();


	instanceObject->objects.emplace_back(dummy);

	for (int i = 0; i < instanceObject->objectsNum - 1; ++i)
	{
		
		GameObject* gameobject = new GameObject();
		gameobject->SetChild(objectModel);
		//float fHeight = terrain->GetHeight(position.x, position.z);
		gameobject->SetPosition(position.x, position.y, position.z);
		gameobject->Rotate(rotate.x, rotate.y, rotate.z);
		gameobject->SetScale(1.0f, 1.0f, 1.0f);
		gameobject->SetOBB(gameobject->GetPosition(), boundingSize, XMFLOAT4(0, 0, 0, 0));
		gameobject->SetBoxType(false);
		instanceObject->objects.emplace_back(gameobject);
	}
	instanceBuffer.emplace_back(instanceObject);
}

void InstancingShader::ReleaseUploadBuffers()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->objects[0])
			instanceBuffer[k]->objects[0]->ReleaseUploadBuffers();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->ReleaseUploadBuffers();
	}
}

void InstancingShader::SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	
	std::vector<std::pair<int , std::wstring>> dtname;
	std::vector<std::pair<int, std::wstring>> nlname;
	std::vector<std::pair<int, std::wstring>> spname;
	std::vector<std::pair<int, std::wstring>> mtname;
	

	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		bool isEqual = false;
		int equalidx = FAILPICKING;
		if (instanceBuffer[k]->textureName.diffuse.size() > 0)
		{
			//dtname.emplace_back(instanceBuffer[k]->textureName.diffuse);
			Texture* diffuseTexture = NULL;
			for (int i = 0; i < dtname.size(); ++i) {
				for (int j = 0; j < instanceBuffer[k]->textureName.diffuse.size(); ++j) {
					if (dtname[i].second.compare(instanceBuffer[k]->textureName.diffuse[j].second) == 0) {
						isEqual = true;
						equalidx = dtname[i].first;
						break;
					}
				}
				if (isEqual)
					break;
			}
			if (!isEqual) {
				diffuseTexture = new Texture((int)instanceBuffer[k]->textureName.diffuse.size(), ResourceTexture2D, 0);
				for (int i = 0; i < instanceBuffer[k]->textureName.diffuse.size(); ++i)
				{
					dtname.emplace_back(k, instanceBuffer[k]->textureName.diffuse[i].second);
					diffuseTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.diffuse[i].second, instanceBuffer[k]->textureName.diffuse[i].first);

				}

				instanceBuffer[k]->diffuseIdx = Scene::CreateShaderResourceViews(device, commandList, diffuseTexture, GraphicsRootDiffuseTextures);
			}
			else {
				instanceBuffer[k]->diffuseIdx = instanceBuffer[equalidx]->diffuseIdx;
			}

			textures.emplace_back(diffuseTexture);
			
		}
		if (instanceBuffer[k]->textureName.normal.size() > 0)
		{
			bool isEqual = true;
			int equalidx = FAILPICKING;
			Texture* normalTexture = NULL;
			for (int i = 0; i < nlname.size(); ++i) {
				for (int j = 0; j < instanceBuffer[k]->textureName.normal.size(); ++j) {
					if (nlname[i].second.compare(instanceBuffer[k]->textureName.normal[j].second) == 0) {
						isEqual = false;
						equalidx = nlname[i].first;
						break;
					}
				}
				if (!isEqual)
					break;
			}
			if (isEqual) {
				Texture* normalTexture = new Texture((int)instanceBuffer[k]->textureName.normal.size(), ResourceTexture2D, 0);
				
				for (int i = 0; i < instanceBuffer[k]->textureName.normal.size(); ++i)
				{
					nlname.emplace_back(k , instanceBuffer[k]->textureName.normal[i].second);
					normalTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.normal[i].second, instanceBuffer[k]->textureName.normal[i].first);
				}
				instanceBuffer[k]->normalIdx = Scene::CreateShaderResourceViews(device, commandList, normalTexture, GraphicsRootNormalTextures);
				
			}
			else {
				instanceBuffer[k]->normalIdx = instanceBuffer[equalidx]->normalIdx;
			}
			textures.emplace_back(normalTexture);
		}
		if (instanceBuffer[k]->textureName.specular.size() > 0)
		{

			bool isEqual = true;
			int equalidx = FAILPICKING;
			Texture* specularTexture = NULL;
			for (int i = 0; i < spname.size(); ++i) {
				for (int j = 0; j < instanceBuffer[k]->textureName.normal.size(); ++j) {
					if (spname[i].second.compare(instanceBuffer[k]->textureName.normal[j].second) == 0) {
						isEqual = false;
						equalidx = spname[i].first;
						break;
					}
				}
				if (!isEqual)
					break;
			}
			if (isEqual) {
				specularTexture = new Texture((int)instanceBuffer[k]->textureName.specular.size(), ResourceTexture2D, 0);
				for (int i = 0; i < instanceBuffer[k]->textureName.specular.size(); ++i)
				{
					spname.emplace_back(k, instanceBuffer[k]->textureName.specular[i].second);
					specularTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.specular[i].second, instanceBuffer[k]->textureName.specular[i].first);
				}
				instanceBuffer[k]->specularIdx = Scene::CreateShaderResourceViews(device, commandList, specularTexture, GraphicsRootSpecularTextures);
			}
			else {
				instanceBuffer[k]->specularIdx = instanceBuffer[equalidx]->specularIdx;
			}
			
			textures.emplace_back(specularTexture);
		}
		if (instanceBuffer[k]->textureName.metallic.size() > 0)
		{
			bool isEqual = true;
			int equalidx = FAILPICKING;
			Texture* metallicTexture = NULL;
			for (int i = 0; i < mtname.size(); ++i) {
				for (int j = 0; j < instanceBuffer[k]->textureName.normal.size(); ++j) {
					if (mtname[i].second.compare(instanceBuffer[k]->textureName.normal[j].second) == 0) {
						isEqual = false;
						equalidx = mtname[i].first;
						break;
					}
				}
				if (!isEqual)
					break;
			}

			if (isEqual) {
				metallicTexture = new Texture((int)instanceBuffer[k]->textureName.metallic.size(), ResourceTexture2D, 0);
				for (int i = 0; i < instanceBuffer[k]->textureName.metallic.size(); ++i)
				{
					mtname.emplace_back(k, instanceBuffer[k]->textureName.metallic[i].second);
					metallicTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.metallic[i].second, instanceBuffer[k]->textureName.metallic[i].first);
				}
				instanceBuffer[k]->metallicIdx = Scene::CreateShaderResourceViews(device, commandList, metallicTexture, GraphicsRootMetallicTextures);
			}
			else {
				instanceBuffer[k]->metallicIdx = instanceBuffer[equalidx]->metallicIdx;
			}
			textures.emplace_back(metallicTexture);
		}
		if (instanceBuffer[k]->textureName.emission.size() > 0)
		{
			Texture* emissionTexture = new Texture((int)instanceBuffer[k]->textureName.emission.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.emission.size(); ++i)
			{
				emissionTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.emission[i].second, instanceBuffer[k]->textureName.emission[i].first);
			}
			instanceBuffer[k]->emissionIdx = Scene::CreateShaderResourceViews(device, commandList, emissionTexture, GraphicsRootEmissionTextures);
			textures.emplace_back(emissionTexture);
		}
	}
}

void InstancingShader::OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx)
{
	if (instanceBuffer[idx]->diffuseIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->diffuseIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->diffuseIdx).srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->normalIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->normalIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->normalIdx).srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->specularIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->specularIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->specularIdx).srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->metallicIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->metallicIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->metallicIdx).srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->emissionIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->emissionIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->emissionIdx).srvGpuDescriptorHandle);

	if (pipelineStates) commandList->SetPipelineState(pipelineStates[instanceBuffer[idx]->pipeLineStateIdx]);
}

void InstancingShader::Render(ID3D12GraphicsCommandList *commandList, Camera* camera)
{
	UpdateShaderVariables(commandList);
	int billboardObjIdx = -1;
	int objIdx = -1;
	if (pipelineStates)
		commandList->SetPipelineState(pipelineStates[1]);
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		OnPrepareRender(commandList, k);

		if (!instanceBuffer[k]->objects.empty())
		{
			if (instanceBuffer[k]->isBillboard >= 0)
			{
				if (renderLight) {
					for (int m = 0; m < instanceBuffer[k]->instancingBillboardObjectBufferView.size(); ++m)
						instanceBuffer[k]->objects[0]->BillboardRender(commandList, camera, (UINT)instanceBuffer[k]->objects.size() - 1,
							instanceBuffer[k]->instancingBillboardObjectBufferView[m]);
				}
			}
			else
			{
				for (int m = 0; m < instanceBuffer[k]->instancingGameObjectBufferView.size(); ++m)
					instanceBuffer[k]->objects[0]->Render(commandList, camera, UINT(instanceBuffer[k]->objects.size()),
						instanceBuffer[k]->instancingGameObjectBufferView[m]);
			}
		}
	}
	if (renderBoundingBox) {
		if (pipelineStates)
			commandList->SetPipelineState(pipelineStates[InstanceShaderPS_BoundingBox]);

		for (int k = 0; k < instanceBuffer.size(); ++k) {
			if (!instanceBuffer[k]->objects.empty())
			{
				for (int m = 0; m < instanceBuffer[k]->instancingBoundingBufferView.size(); ++m)
					instanceBuffer[k]->objects[0]->BoundingRender(commandList, camera, UINT(instanceBuffer[k]->objects.size()),
						instanceBuffer[k]->instancingBoundingBufferView[m]);
			}
		}
	}

}

void InstancingShader::SetDummyFarAway()
{
	for (int i = 0; i < instanceBuffer.size(); ++i) {
		
		instanceBuffer[i]->objects[0]->SetPosition(9999.0, 9999.0, 9999.0);
	}
}

void InstancingShader::ApplyLOD(Camera* camera, int idx)
{
	int billboardObjIdx = -1;
	int objIdx = -1;

	if (instanceBuffer[idx]->useBillboard >= 0)
	{
		for (int i = 0; i < instanceBuffer.size(); ++i)
		{
			if (instanceBuffer[idx]->useBillboard == instanceBuffer[i]->isBillboard)
			{
				billboardObjIdx = i;
				break;
			}
		}
		for (int j = 1; j < instanceBuffer[idx]->objects.size(); ++j)
		{
			if (instanceBuffer[idx]->objects[j]->IsFar(camera))
			{
				GameObject* temp = instanceBuffer[idx]->objects[j];
				XMFLOAT3 position = temp->GetPosition();
				instanceBuffer[idx]->objects[j] = instanceBuffer[idx]->objects[instanceBuffer[idx]->objects.size() - 1];
				instanceBuffer[idx]->objects[instanceBuffer[idx]->objects.size() - 1] = temp;
				instanceBuffer[idx]->objects.pop_back();
				instanceBuffer[idx]->objectsNum--;

				BillboardObject* obj = new BillboardObject;
				position.x += 7.5, position.z += 7.5, position.y += 10;
				obj->billboardVertex.position = position;
				obj->billboardVertex.billboardInfo = XMFLOAT3(15, 20, float(instanceBuffer[billboardObjIdx]->isBillboard));

				instanceBuffer[billboardObjIdx]->objects.emplace_back(obj);
				instanceBuffer[billboardObjIdx]->objectsNum++;
			}
		}
	}
	if (instanceBuffer[idx]->isBillboard >= 0) {
		for (int i = 0; i < instanceBuffer.size(); ++i)
		{
			if (instanceBuffer[idx]->isBillboard == instanceBuffer[i]->useBillboard)
			{
				objIdx = i;
				break;
			}
		}
		for (int j = 1; j < instanceBuffer[idx]->objects.size(); ++j)
		{

			if (!instanceBuffer[idx]->objects[j]->IsFar(camera))
			{
				BillboardObject* temp2 = (BillboardObject*)instanceBuffer[idx]->objects[j];
				XMFLOAT3 position = temp2->billboardVertex.position;
				instanceBuffer[idx]->objects[j] = instanceBuffer[idx]->objects[instanceBuffer[idx]->objects.size() - 1];
				instanceBuffer[idx]->objects[instanceBuffer[idx]->objects.size() - 1] = temp2;
				instanceBuffer[idx]->objects.pop_back();
				instanceBuffer[idx]->objectsNum--;

				GameObject* obj;
				obj = new TreeObject();
				obj->SetChild(objects[2]);
				position.x -= 7.5, position.z -= 7.5, position.y -= 10;
				obj->SetPosition(position);
				obj->SetScale(3.0f, 3.0f, 3.0f);

				instanceBuffer[objIdx]->objects.emplace_back(obj);
				instanceBuffer[objIdx]->objectsNum++;
			}
		}
	}
}

void InstancingShader::CreateNewBounding(ID3D12Device * device, ID3D12GraphicsCommandList * commandList , int idx)
{
	//boundingBuffer 
	UINT tempBoundingByte = 0;
	ID3D12Resource* tempBounding = NULL;
	VS_VB_INSTACE_BOUNDING* tempMappedBounding = NULL;
	D3D12_VERTEX_BUFFER_VIEW tempBoundingBufferView;


	tempBoundingByte = sizeof(VS_VB_INSTACE_BOUNDING);


	tempBounding = ::CreateBufferResource(device, commandList, NULL, sizeof(VS_VB_INSTACE_BOUNDING) * ((UINT)BUFFERSIZE), D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	//인스턴스 정보를 저장할 정점 버퍼를 업로드 힙 유형으로 생성한다.

	tempBounding->Map(0, NULL, (void **)&tempMappedBounding);
	//정점 버퍼(업로드 힙)에 대한 포인터를 저장한다.

	tempBoundingBufferView.BufferLocation = tempBounding->GetGPUVirtualAddress();
	tempBoundingBufferView.StrideInBytes = tempBoundingByte;
	tempBoundingBufferView.SizeInBytes = tempBoundingByte * ((UINT)BUFFERSIZE);

	instanceBuffer[idx]->vbBoundingBytes = tempBoundingByte;
	instanceBuffer[idx]->vbBounding.emplace_back(tempBounding);
	instanceBuffer[idx]->vbMappedBounding.emplace_back(tempMappedBounding);
	instanceBuffer[idx]->instancingBoundingBufferView.emplace_back(tempBoundingBufferView);


}

void InstancingShader::DeleteBounding(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx, int bIdx)
{
	std::vector<GameObject*>* bvec = instanceBuffer[idx]->objects[0]->GetBoundingObject();
	GameObject* temp = (*bvec)[bIdx];
	bvec->erase(bvec->begin() + bIdx);
	delete temp;

	for (int i = 0; i < instanceBuffer[idx]->objects.size(); ++i) {
		instanceBuffer[idx]->objects[i]->GetobbVector().erase(instanceBuffer[idx]->objects[i]->GetobbVector().begin() + bIdx);
	}

	auto bResource = instanceBuffer[idx]->vbBounding[bIdx];
	auto bMap = instanceBuffer[idx]->vbMappedBounding[bIdx];
	auto bView = instanceBuffer[idx]->instancingBoundingBufferView[bIdx];
	instanceBuffer[idx]->vbBounding.erase(instanceBuffer[idx]->vbBounding.begin() + bIdx);
	instanceBuffer[idx]->vbMappedBounding.erase(instanceBuffer[idx]->vbMappedBounding.begin() + bIdx);
	instanceBuffer[idx]->instancingBoundingBufferView.erase(instanceBuffer[idx]->instancingBoundingBufferView.begin() + bIdx);
	bResource->Release();
	
}


/////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
SkinnedInstancingShader::SkinnedInstancingShader()
{
}

SkinnedInstancingShader::~SkinnedInstancingShader()
{
	
}

void SkinnedInstancingShader::CreateShader(ID3D12Device *device, ID3D12RootSignature* graphicsRootSignature, ID3D12RootSignature * computeRootSignature)
{
	pipelineStatesNum = 3;
	pipelineStates = new ID3D12PipelineState*[pipelineStatesNum];

	Shader::CreateShader(device, graphicsRootSignature);
	D3D12_GRAPHICS_PIPELINE_STATE_DESC boundingPipeLineStateDesc = pipelineStateDesc;

	pipelineStateDesc.BlendState.AlphaToCoverageEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].BlendEnable = TRUE;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlend = D3D12_BLEND_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].DestBlend = D3D12_BLEND_INV_SRC_ALPHA;
	pipelineStateDesc.BlendState.RenderTarget[0].SrcBlendAlpha = D3D12_BLEND_ONE;
	HRESULT hResult = device->CreateGraphicsPipelineState(&pipelineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[1]);

	int inputElementDescsNum = 8;
	D3D12_INPUT_ELEMENT_DESC *BoundinginputElementDescs = new D3D12_INPUT_ELEMENT_DESC[inputElementDescsNum];
	BoundinginputElementDescs[0] = { "POSITION", 0, DXGI_FORMAT_R32G32B32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	BoundinginputElementDescs[1] = { "COLOR", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 0, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_VERTEX_DATA, 0 };
	BoundinginputElementDescs[2] = { "WORLDMATRIX", 0, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[3] = { "WORLDMATRIX", 1, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[4] = { "WORLDMATRIX", 2, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[5] = { "WORLDMATRIX", 3, DXGI_FORMAT_R32G32B32A32_FLOAT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[6] = { "ISDUMMY", 0, DXGI_FORMAT_R8_UINT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };
	BoundinginputElementDescs[7] = { "BIDX", 0, DXGI_FORMAT_R32_UINT, 1, D3D12_APPEND_ALIGNED_ELEMENT, D3D12_INPUT_CLASSIFICATION_PER_INSTANCE_DATA, 1 };

	// 첫번째가 시멘틱 이름 , 두번째가 시멘틱 번호 , 세번째가 변수 포멧 , 네번째가 버퍼 슬릇 , 다섯번째는 그냥 오브셋 , 여섯번째는 버텍스 데이터인지 인스턴스 데이터인지 , 일곱번째도 같다 .

	boundingPipeLineStateDesc.InputLayout.NumElements = inputElementDescsNum;
	boundingPipeLineStateDesc.InputLayout.pInputElementDescs = BoundinginputElementDescs;

	boundingPipeLineStateDesc.RasterizerState.FillMode = D3D12_FILL_MODE_WIREFRAME;
	boundingPipeLineStateDesc.VS = Shader::CompileShaderFromFile(L"Shaders.hlsl", "VSBOUNDING", "vs_5_1", &vertexShaderBlob);
	boundingPipeLineStateDesc.PS = Shader::CompileShaderFromFile(L"Shaders.hlsl", "PSBOUNDING", "ps_5_1", &pixelShaderBlob);

	hResult = device->CreateGraphicsPipelineState(&boundingPipeLineStateDesc, __uuidof(ID3D12PipelineState), (void **)&pipelineStates[2]);



	if (vertexShaderBlob) vertexShaderBlob->Release();
	if (pixelShaderBlob) pixelShaderBlob->Release();

	if (pipelineStateDesc.InputLayout.pInputElementDescs) delete[] pipelineStateDesc.InputLayout.pInputElementDescs;
}

void SkinnedInstancingShader::BuildObjects(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, std::vector<void*>& context , int fobjectSize)
{
	int iter = fobjectSize;

	for (int i = fobjectSize; i < context.size(); ++i) {
		LoadedModelInfo* tempModel = (LoadedModelInfo*)context[iter++];
		BuildNewSkinedObject(device, commandList, graphicsRootSignature, tempModel, XMFLOAT3(0.7f, 1.8f, 0.7f));
	}
	SetTextures(device, commandList);

	for (int i = 0; i < instanceBuffer.size(); ++i)
		CreateShaderVariables(device, commandList, i);


}

void SkinnedInstancingShader::CreateShaderVariables(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int idx)
{
	UINT skinnedStride = 0;
	ID3D12Resource** tempSkinned = NULL;
	XMFLOAT4X4** tempMappedSkinned = NULL;

	UINT objIdxStride = 0;
	ID3D12Resource* tempObjIdx = NULL;
	UINT* tempMappedObjIdx = NULL;
	D3D12_VERTEX_BUFFER_VIEW tempObjIdxBufferView;

	UINT standardStride = 0;
	ID3D12Resource** tempStandard = NULL;
	VS_VB_INSTANCE_OBJECT** tempMappedStandard = NULL;
	D3D12_VERTEX_BUFFER_VIEW* tempStandardBufferView = NULL;
	
	UINT boundStride = 0;
	ID3D12Resource** tempbounding = NULL;
	VS_VB_INSTACE_BOUNDING** tempMappedBounding = NULL;
	D3D12_VERTEX_BUFFER_VIEW* tempBoundingBufferView;

	skinnedStride = sizeof(XMFLOAT4X4) * SKINNED_ANIMATION_BONES;
	tempSkinned = new ID3D12Resource*[instanceBuffer[idx]->skinnedMeshNum];
	tempMappedSkinned = new XMFLOAT4X4*[instanceBuffer[idx]->skinnedMeshNum];

	objIdxStride = sizeof(UINT);

	boundStride = sizeof(VS_VB_INSTACE_BOUNDING);
	tempbounding = new ID3D12Resource*[instanceBuffer[idx]->boundingMeshNum];
	tempMappedBounding = new VS_VB_INSTACE_BOUNDING*[instanceBuffer[idx]->boundingMeshNum];
	tempBoundingBufferView = new D3D12_VERTEX_BUFFER_VIEW[instanceBuffer[idx]->boundingMeshNum];

	standardStride = sizeof(VS_VB_INSTANCE_OBJECT);
	tempStandard = new ID3D12Resource*[instanceBuffer[idx]->standardMeshNum];
	tempMappedStandard = new VS_VB_INSTANCE_OBJECT*[instanceBuffer[idx]->standardMeshNum];
	tempStandardBufferView = new D3D12_VERTEX_BUFFER_VIEW[instanceBuffer[idx]->standardMeshNum];

	

	for (int i = 0; i < instanceBuffer[idx]->skinnedMeshNum; ++i)
	{
		tempSkinned[i] = ::CreateBufferResource(device, commandList, NULL, skinnedStride * SBUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_GENERIC_READ, NULL);
		tempSkinned[i]->Map(0, NULL, (void **)&tempMappedSkinned[i]);
	}
	// Skinned Bone Transform Buffers ( SRV )

	tempObjIdx = ::CreateBufferResource(device, commandList, NULL, objIdxStride * (UINT)SBUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
	tempObjIdx->Map(0, NULL, (void**)&tempMappedObjIdx);
	tempObjIdxBufferView.BufferLocation = tempObjIdx->GetGPUVirtualAddress();
	tempObjIdxBufferView.StrideInBytes = objIdxStride;
	tempObjIdxBufferView.SizeInBytes = objIdxStride * (UINT)SBUFFERSIZE;
	// Object Index Bufer ( VertexBuffer )

	for (int i = 0; i < instanceBuffer[idx]->boundingMeshNum; ++i)
	{
		tempbounding[i] = ::CreateBufferResource(device, commandList, NULL, boundStride * (UINT)SBUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		tempbounding[i]->Map(0, NULL, (void**)&tempMappedBounding[i]);

		tempBoundingBufferView[i].BufferLocation = tempbounding[i]->GetGPUVirtualAddress();
		tempBoundingBufferView[i].StrideInBytes = boundStride;
		tempBoundingBufferView[i].SizeInBytes = boundStride * (UINT)SBUFFERSIZE;
	}
	// worldTransformBuffer 

	for (int i = 0; i < instanceBuffer[idx]->standardMeshNum; ++i)
	{
		tempStandard[i] = ::CreateBufferResource(device, commandList, NULL, standardStride * (UINT)SBUFFERSIZE, D3D12_HEAP_TYPE_UPLOAD, D3D12_RESOURCE_STATE_VERTEX_AND_CONSTANT_BUFFER, NULL);
		tempStandard[i]->Map(0, NULL, (void **)&tempMappedStandard[i]);

		tempStandardBufferView[i].BufferLocation = tempStandard[i]->GetGPUVirtualAddress();
		tempStandardBufferView[i].StrideInBytes = standardStride;
		tempStandardBufferView[i].SizeInBytes = standardStride * ((UINT)SBUFFERSIZE);
	}
	// Standard Mesh Transform Buffers ( VertexBuffer)

	instanceBuffer[idx]->skinnedBuffer = tempSkinned;
	instanceBuffer[idx]->standardBuffer = tempStandard;
	instanceBuffer[idx]->mappedSkinnedBuffer = tempMappedSkinned;
	instanceBuffer[idx]->mappedStandardBuffer = tempMappedStandard;
	instanceBuffer[idx]->instancingStandardBufferView = tempStandardBufferView;

	instanceBuffer[idx]->objIdxBuffer = tempObjIdx;
	instanceBuffer[idx]->mappedObjIdxBuffer = tempMappedObjIdx;
	instanceBuffer[idx]->instancingObjIdxBufferView = tempObjIdxBufferView;

	instanceBuffer[idx]->boundingBuffer = tempbounding;
	instanceBuffer[idx]->mappedBoundingBuffer = tempMappedBounding;
	instanceBuffer[idx]->instancingBoundingBufferView = tempBoundingBufferView;
	//to draw Bounding Box make world transFormBuffer 


}

void SkinnedInstancingShader::UpdateShaderVariables(int idx, int objIdx)
{
	for (int i = 0; i < instanceBuffer[idx]->skinnedMeshNum; ++i)
	{
		for (int j = 0; j < instanceBuffer[idx]->skinnedMeshes[i]->skinningBones; ++j)
		{
			XMStoreFloat4x4(&instanceBuffer[idx]->mappedSkinnedBuffer[i][(objIdx * SKINNED_ANIMATION_BONES) + j],
				XMMatrixTranspose(XMLoadFloat4x4(&instanceBuffer[idx]->skinnedMeshes[i]->skinningBoneFrameCaches[j]->GetWorld())));
		}
	}

	instanceBuffer[idx]->mappedObjIdxBuffer[objIdx] = objIdx;
}

void SkinnedInstancingShader::ReleaseShaderVariables()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->skinnedBuffer)
			for (int j = 0; j < instanceBuffer[k]->skinnedMeshNum; ++j)
			{
				instanceBuffer[k]->skinnedBuffer[j]->Unmap(0, NULL);
				instanceBuffer[k]->skinnedBuffer[j]->Release();
			}

		if (instanceBuffer[k]->objIdxBuffer)
		{
			instanceBuffer[k]->objIdxBuffer->Unmap(0, NULL);
			instanceBuffer[k]->objIdxBuffer->Release();
		}

		if (instanceBuffer[k]->standardBuffer)
			for (int j = 0; j < instanceBuffer[k]->standardMeshNum; ++j)
			{
				instanceBuffer[k]->standardBuffer[j]->Unmap(0, NULL);
				instanceBuffer[k]->standardBuffer[j]->Release();
			}
	}
}

void SkinnedInstancingShader::ReleaseObjects()
{


	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (!instanceBuffer[k]->objects.empty())
			instanceBuffer[k]->objects[0]->Release();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->Release();
	}
}

void SkinnedInstancingShader::ReleaseUploadBuffers()
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->objects[0])
			instanceBuffer[k]->objects[0]->ReleaseUploadBuffers();
	}
	for (int i = 0; i < textures.size(); ++i)
	{
		if (textures[i])
			textures[i]->ReleaseUploadBuffers();
	}
}


void SkinnedInstancingShader::SetTextures(ID3D12Device* device, ID3D12GraphicsCommandList* commandList)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		if (instanceBuffer[k]->textureName.diffuse.size() > 0)
		{
			Texture* diffuseTexture = new Texture((int)instanceBuffer[k]->textureName.diffuse.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.diffuse.size(); ++i)
			{
				diffuseTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.diffuse[i].second, instanceBuffer[k]->textureName.diffuse[i].first);
			}
			instanceBuffer[k]->diffuseIdx = Scene::CreateShaderResourceViews(device, commandList, diffuseTexture, GraphicsRootDiffuseTextures);
			textures.emplace_back(diffuseTexture);
		}
		if (instanceBuffer[k]->textureName.normal.size() > 0)
		{
			Texture* normalTexture = new Texture((int)instanceBuffer[k]->textureName.normal.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.normal.size(); ++i)
			{
				normalTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.normal[i].second, instanceBuffer[k]->textureName.normal[i].first);
			}
			instanceBuffer[k]->normalIdx = Scene::CreateShaderResourceViews(device, commandList, normalTexture, GraphicsRootNormalTextures);
			textures.emplace_back(normalTexture);
		}
		if (instanceBuffer[k]->textureName.specular.size() > 0)
		{
			Texture* specularTexture = new Texture((int)instanceBuffer[k]->textureName.specular.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.specular.size(); ++i)
			{
				specularTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.specular[i].second, instanceBuffer[k]->textureName.specular[i].first);
			}
			instanceBuffer[k]->specularIdx = Scene::CreateShaderResourceViews(device, commandList, specularTexture, GraphicsRootSpecularTextures);
			textures.emplace_back(specularTexture);
		}
		if (instanceBuffer[k]->textureName.metallic.size() > 0)
		{
			Texture* metallicTexture = new Texture((int)instanceBuffer[k]->textureName.metallic.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.metallic.size(); ++i)
			{
				metallicTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.metallic[i].second, instanceBuffer[k]->textureName.metallic[i].first);
			}
			instanceBuffer[k]->metallicIdx = Scene::CreateShaderResourceViews(device, commandList, metallicTexture, GraphicsRootMetallicTextures);
			textures.emplace_back(metallicTexture);
		}
		if (instanceBuffer[k]->textureName.emission.size() > 0)
		{
			Texture* emissionTexture = new Texture((int)instanceBuffer[k]->textureName.emission.size(), ResourceTexture2D, 0);
			for (int i = 0; i < instanceBuffer[k]->textureName.emission.size(); ++i)
			{
				emissionTexture->LoadTextureFromFile(device, commandList, instanceBuffer[k]->textureName.emission[i].second, instanceBuffer[k]->textureName.emission[i].first);
			}
			instanceBuffer[k]->emissionIdx = Scene::CreateShaderResourceViews(device, commandList, emissionTexture, GraphicsRootEmissionTextures);
			textures.emplace_back(emissionTexture);
		}
	}
}

void SkinnedInstancingShader::SetDummyFarAway()
{
	for (int i = 0; i < instanceBuffer.size(); ++i) {
		instanceBuffer[i]->objects[0]->SetPosition(9999.0, 9999.0, 9999.0);
	}
}

void SkinnedInstancingShader::AnimateObjects(float timeElapsed, Camera* camera)
{
	elapsedTime = timeElapsed;

	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		for (int j = 0; j < instanceBuffer[k]->objects.size(); ++j)
		{
			instanceBuffer[k]->objects[j]->Animate(elapsedTime);
			UpdateShaderVariables(k, j);
			instanceBuffer[k]->objects[j]->UpdateTransformOnlyStandardMesh(instanceBuffer[k]->mappedStandardBuffer, j, instanceBuffer[k]->standardMeshNum, NULL);
			instanceBuffer[k]->objects[j]->UpdateSkinnedBoundingTransform(instanceBuffer[k]->mappedBoundingBuffer, instanceBuffer[k]->objects[j]->GetobbVector(), j, instanceBuffer[k]->boundingMeshNum, NULL);
		
			XMFLOAT3 pos = instanceBuffer[k]->objects[j]->GetPosition();
			float fscale = instanceBuffer[k]->objects[j]->GetScale();
			XMFLOAT3 tempExtents = instanceBuffer[k]->objects[j]->GetOBB().Extents;

			XMFLOAT3 Extents{ tempExtents.x * fscale, tempExtents.y * fscale , tempExtents.z * fscale };
			instanceBuffer[k]->objects[j]->SetOBBPos(instanceBuffer[k]->objects[j]->GetPosition());
			instanceBuffer[k]->objects[j]->SetOBBOriginalExtent(Extents);
			instanceBuffer[k]->objects[j]->SetOBBBoundingBox();

		}

		for (int i = 0; i < instanceBuffer[k]->skinnedMeshNum; ++i)
		{
			instanceBuffer[k]->skinnedMeshes[i]->skinningBoneTransforms = instanceBuffer[k]->skinnedBuffer[i];
			instanceBuffer[k]->skinnedMeshes[i]->mappedSkinningBoneTransforms = instanceBuffer[k]->mappedSkinnedBuffer[i];
		}
	}

}

void SkinnedInstancingShader::OnPrepareRender(ID3D12GraphicsCommandList * commandList, UINT idx)
{
	if (instanceBuffer[idx]->diffuseIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->diffuseIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->diffuseIdx).srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->normalIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->normalIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->normalIdx).srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->specularIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->specularIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->specularIdx).srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->metallicIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->metallicIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->metallicIdx).srvGpuDescriptorHandle);
	if (instanceBuffer[idx]->emissionIdx >= 0)
		commandList->SetGraphicsRootDescriptorTable(Scene::GetRootArgument(instanceBuffer[idx]->emissionIdx).rootParameterIndex, Scene::GetRootArgument(instanceBuffer[idx]->emissionIdx).srvGpuDescriptorHandle);

	if (pipelineStates) commandList->SetPipelineState(pipelineStates[1]);
}

void SkinnedInstancingShader::Render(ID3D12GraphicsCommandList *commandList, Camera *camera)
{
	for (int k = 0; k < instanceBuffer.size(); ++k)
	{
		OnPrepareRender(commandList, k);

		instanceBuffer[k]->objects[0]->Render(commandList, camera, instanceBuffer[k]->objects.size(), instanceBuffer[k]->instancingObjIdxBufferView, instanceBuffer[k]->instancingStandardBufferView);


		if (renderBoundingBox) {
			commandList->SetPipelineState(pipelineStates[2]);
			instanceBuffer[k]->objects[0]->SkinnedBoundingRender(commandList, camera, instanceBuffer[k]->objects.size(), instanceBuffer[k]->instancingBoundingBufferView);
		}
	}
}

void SkinnedInstancingShader::AddGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT3 pointxyz, ID3D12RootSignature * graphicsRootSignature, int objectType , bool noiseFlag)
{
	AnimationObject* tempObject = new AnimationObject(device, commandList, graphicsRootSignature, instanceModels[objectType], 1);
	tempObject->skinnedAnimationController->SetTrackAnimationSet(0, 0);
	tempObject->skinnedAnimationController->SetTrackSpeed(0, 1);
	tempObject->skinnedAnimationController->SetTrackPosition(0.0f, 0.0f);
	tempObject->SetPosition(XMFLOAT3(pointxyz));
	tempObject->SetBoxType(false);
	
	GameObject* dummyObject = instanceBuffer[objectType]->objects[0];
	tempObject->GetScale() = dummyObject->GetScale();
	tempObject->GetInputAngle() = dummyObject->GetInputAngle();

	tempObject->SetOBB(tempObject->GetPosition(), dummyObject->GetOBB().Extents, XMFLOAT4(0, 0, 0, 0));
	std::vector<BoundingOrientedBox>& tempObb = tempObject->GetobbVector();
	tempObb.resize(instanceBuffer[objectType]->boundingMeshNum);
	XMFLOAT4X4 transForm = dummyObject->GetTransform();
	if (noiseFlag) {

		float randomScale = suid(sdre);

		transForm._11 = transForm._11 + randomScale;
		transForm._22 = transForm._22 + randomScale;
		transForm._33 = transForm._33 + randomScale;
	}
	tempObject->SetTransform(transForm);

	instanceBuffer[objectType]->objects.emplace_back(tempObject);
}

void SkinnedInstancingShader::LoadGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, XMFLOAT4X4 world, ID3D12RootSignature * graphicsRootSignature, int objectType)
{
	AnimationObject* tempObject = new AnimationObject(device, commandList, graphicsRootSignature, instanceModels[objectType], 1);
	tempObject->skinnedAnimationController->SetTrackAnimationSet(0, 0);
	tempObject->skinnedAnimationController->SetTrackSpeed(0, 1);
	tempObject->skinnedAnimationController->SetTrackPosition(0.0f, 0.0f);
	tempObject->SetTransform(world);
	tempObject->SetBoxType(false);
	GameObject* dummyObject = instanceBuffer[objectType]->objects[0];
	tempObject->SetOBB(tempObject->GetPosition(), dummyObject->GetOBB().Extents, XMFLOAT4(0, 0, 0, 0));
	std::vector<BoundingOrientedBox>& tempObb = tempObject->GetobbVector();
	tempObb.resize(instanceBuffer[objectType]->boundingMeshNum);
	instanceBuffer[objectType]->objects.emplace_back(tempObject);
}

void SkinnedInstancingShader::DeleteGameObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, int bufferIndex, int objectIndex)
{
	GameObject* temp = instanceBuffer[bufferIndex]->objects[objectIndex];
	instanceBuffer[bufferIndex]->objects.erase(instanceBuffer[bufferIndex]->objects.begin() + objectIndex);
	instanceBuffer[bufferIndex]->objectsNum--;
	delete temp;
}

void SkinnedInstancingShader::DeleteAllObjcet()
{
	for (int i = 0; i < instanceBuffer.size(); ++i) {
		while (true) {
			if (instanceBuffer[i]->objects.size() < 2) {
				break;
			}
			GameObject* temp = instanceBuffer[i]->objects[instanceBuffer[i]->objects.size() - 1];
			instanceBuffer[i]->objects.pop_back();
			delete temp;

		}
		instanceBuffer[i]->objectsNum = 1;
	}
}

void SkinnedInstancingShader::BuildNewSkinedObject(ID3D12Device * device, ID3D12GraphicsCommandList * commandList, ID3D12RootSignature * graphicsRootSignature, LoadedModelInfo * context , XMFLOAT3 boundingSize)
{
	SkinnedInstanceBuffer* tempBuffer = new SkinnedInstanceBuffer;

	tempBuffer->objectsNum = 1;
	tempBuffer->objects.resize(tempBuffer->objectsNum);

	LoadedModelInfo* tempModel = (LoadedModelInfo*)context;
	instanceModels.emplace_back(tempModel);

	tempBuffer->textureName = tempModel->modelRootObject->textureName;
	tempBuffer->skinnedMeshNum = tempModel->skinnedMeshNum;
	tempBuffer->standardMeshNum = tempModel->standardMeshNum;
	tempBuffer->boundingMeshNum = tempModel->boundingMeshNum;
	tempBuffer->skinnedMeshes = new SkinnedMesh*[tempBuffer->skinnedMeshNum];
	for (int i = 0; i < tempBuffer->skinnedMeshNum; i++) tempBuffer->skinnedMeshes[i] = tempModel->skinnedMeshes[i];


	for (int i = 0; i < tempBuffer->objectsNum; ++i) {
		tempBuffer->objects[i] = new AnimationObject(device, commandList, graphicsRootSignature, tempModel, 1);
		tempBuffer->objects[i]->skinnedAnimationController->SetTrackAnimationSet(0, 0);
		tempBuffer->objects[i]->skinnedAnimationController->SetTrackSpeed(0, 1);
		tempBuffer->objects[i]->skinnedAnimationController->SetTrackPosition(0.0f, 0.0f);
		tempBuffer->objects[i]->SetPosition(XMFLOAT3(100 + 30 * i, 250, 300));
		BoundingBoxMesh *boundingMesh = new BoundingBoxMesh(device, commandList, boundingSize.x, boundingSize.y, boundingSize.z);
		
		tempBuffer->objects[i]->SetBoundingMesh(boundingMesh);
		tempBuffer->objects[i]->SetOBB(tempBuffer->objects[i]->GetPosition(), XMFLOAT3(boundingSize.x, boundingSize.y, boundingSize.z), XMFLOAT4(0, 0, 0, 0));
		tempBuffer->objects[i]->SetBoxType(false);
		tempBuffer->objects[i]->GetobbVector().resize(tempBuffer->boundingMeshNum);
	}

	instanceBuffer.emplace_back(tempBuffer);

}
