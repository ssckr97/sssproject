#pragma once
#include "GUI.h"
class Picking
{
private:
	XMFLOAT3 direction;
	int pickingIndex;
	GameObject* oldObject = NULL;
	GameObject* selectObject = NULL;
	bool isPicking = true;


public:
	bool Getpicking() { return isPicking; }
	void Setpicking(bool isPicking) { this->isPicking = isPicking; }
	//피킹 성공 여부 

	
	XMFLOAT3 GetDirection() const { return direction; }
	GameObject* GetOldObject() { return oldObject; }
	GameObject* GetSelectObject() { return selectObject; }
	int GetSelectIndex() { return pickingIndex; }
	int CheckPickingObjects(POINT& CursorPos, Camera& camera, int windowClientWidth, int windowClientHeight, std::vector<GameObject*> obj, int objNum, float& nearDistance , int& index);
	bool CheckPickingTerrain(POINT& CursorPos, Camera& camera, int windowClientWidth, int windowClientHeight, XMFLOAT3& pickPlace , GameObject& obj,  float& nearDistance , int& bIdx);
	void MoveObjectToDirection(std::vector<GameObject*> ArrowObject, float deltaX, float deltaY);

	void SetArrowsPosition(GameObject* object, int index);
	void SetOldObject(GameObject* oldObject) { this->oldObject = oldObject; }
	void SetSelectObject(GameObject *selectObject) { this->selectObject = selectObject; }
	void SetSelectIndex(int index) { pickingIndex = index; }
	void SetDirection(XMFLOAT3& direction) { this->direction = direction; }
};

