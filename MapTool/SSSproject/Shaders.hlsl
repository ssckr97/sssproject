
// Define
//  texture resource
#define MATERIAL_DIFFUSE_MAP		0x01
#define MATERIAL_NORMAL_MAP			0x02
#define MATERIAL_SPECULAR_MAP		0x04
#define MATERIAL_METALLIC_MAP		0x08
#define MATERIAL_EMISSION_MAP		0x10

// Skinning Model
#define MAX_VERTEX_INFLUENCES			4
#define SKINNED_ANIMATION_BONES			128

// Screen Size
#define FRAME_BUFFER_WIDTH		1280
#define FRAME_BUFFER_HEIGHT		720

// Particle Type
#define PARTICLE_TYPE_DISAPPEAR 0
#define PARTICLE_TYPE_FOREVER 1
#define PARTICLE_TYPE_TORNADO 2
#define PARTICLE_TYPE_SMALLER 3

//////////////////////////////////////////////////////////////////////////////////////////////
//  constant buffer
cbuffer cbPlayerInfo : register(b0)
{
    matrix gmtxPlayerWorld : packoffset(c0);
};
cbuffer cbCameraInfo : register(b1)
{
    matrix gmtxView : packoffset(c0);
    matrix gmtxInverseView : packoffset(c4);
    matrix gmtxProjection : packoffset(c8);
    matrix gmtxInversePro : packoffset(c12);
    float3 gvCameraPosition : packoffset(c16);
  
};
cbuffer cbGameObjectInfo : register(b2)
{
    matrix gmtxWorld : packoffset(c0);
    matrix gmtxInverseWorld : packoffset(c4);
};
cbuffer cbMaterialInfo : register(b3)
{
    uint gnMaterial : packoffset(c0.x);
    uint gnTextureMask : packoffset(c0.y);
    uint gnTextureIdx : packoffset(c0.z);
};
cbuffer cbTimeInfo : register(b4)
{
    float gfTotalTime : packoffset(c0.x);
    float gfTimeElapsed : packoffset(c0.y);
}

cbuffer cbTerrainInfo : register(b7)
{
    float3 terrainScale : packoffset(c0);
    int DecalShape : packoffset(c0.w);
    int isNormal : packoffset(c1.x);
    
};

cbuffer cbBoneOffsets : register(b8)
{
    float4x4 gpmtxBoneOffsets[SKINNED_ANIMATION_BONES];
};

cbuffer cbParticleAge : register(b9)
{
    float gfParticleAge : packoffset(c0.x);
    int gnParticleType : packoffset(c0.y);
};

Texture2D gtxtTerrainHeightTexture : register(t0);
Texture2D gtxTerrainAlphaTexture0 : register(t1);
Texture2D gtxTerrainAlphaTexture1 : register(t2);
Texture2D gtxTerrainAlphaTexture2 : register(t3);
Texture2D gtxtDiffuseTexture[10] : register(t4);
Texture2D gtxtNormalTexture[10] : register(t14);
Texture2D gtxtSpecularTexture[10] : register(t24);
Texture2D gtxtMetallicTexture[10] : register(t34);
Texture2D gtxtEmissionTexture[10] : register(t44);
Texture2D gtxtBillboardTexture[10] : register(t54);
Texture2D gtxtParticleTexture[10] : register(t64);
Texture2D gtxtTerrainDepthTexture : register(t74);
Texture2D gtxtTerrainDecalTexture : register(t75);
TextureCube gtxtSkyBoxTexture[3] : register(t77);
Texture2D gtxtTerrainDetailNormal[12] : register(t80);
Texture2D gtxtTerrainDetailTexture[12] : register(t93);
Texture2D gtxtDepthTexture : register(t105);


SamplerState gssWrap : register(s0);
SamplerState gssClamp : register(s1);
SamplerState gssDepthClamp : register(s2);
SamplerState gssTerrainWrap : register(s3);

#include "Light.hlsl"

struct ParticleData
{
    float3 position;
    float3 velocity;
    float age;
};

StructuredBuffer<ParticleData> gsbParticleData : register(t76);
StructuredBuffer<float4x4> gsbBoneTransforms : register(t92);


static float3 gf3BillboardPos[4] = { float3(-1.0f, 1.0f, 0.0f), float3(1.0f, 1.0f, 0.0f), float3(-1.0f, -1.0f, 0.0f), float3(1.0f, -1.0f, 0.0f) };
static float2 gf2BillboardUVs[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };

//////////////////////////////////////////////////////////////////////////////////////////////
// Player Shader Code

struct VS_PLAYERINPUT
{
    float3 position : POSITION;
    float4 color : COLOR;
};
struct VS_PLAYEROUTPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

VS_PLAYEROUTPUT VSPlayer(VS_PLAYERINPUT input)
{
    VS_PLAYEROUTPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxPlayerWorld), gmtxView), gmtxProjection);
    output.color = input.color;
    return (output);
}
float4 PSPlayer(VS_PLAYEROUTPUT input) : SV_TARGET
{
    return (input.color);
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Diffuse Shader Code

struct VS_INPUT
{
    float3 position : POSITION;
    float4 color : COLOR;
};
struct VS_OUTPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
};

VS_OUTPUT VSDiffused(VS_INPUT input)
{
    VS_OUTPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.color = input.color;
    return (output);
}
float4 PSDiffused(VS_OUTPUT input) : SV_TARGET
{
    return (input.color);
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Lighing Shader Code


// Using pixel light

struct VS_STANDARD_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
};
struct VS_STANDARD_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float2 uv : TEXCOORD;
    float3 normalW : NORMAL;
    float3 tangentW : TANGENT;
    float3 bitangentW : BITANGENT;
    bool isDummy : ISDUMMY;
    
};

VS_STANDARD_OUTPUT VSStandard(VS_STANDARD_INPUT input)
{
    VS_STANDARD_OUTPUT output;
    output.positionW = (float3) mul(float4(input.position, 1.0f), gmtxWorld);
    output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
    output.normalW = (float3) mul(float4(input.normal, 1.0f), gmtxWorld);
    output.tangentW = (float3) mul(float4(input.tangent, 1.0f), gmtxWorld);
    output.bitangentW = (float3) mul(float4(input.bitangent, 1.0f), gmtxWorld);
    output.isDummy = false;
    
    output.uv = input.uv;
    
    return output;
}

float4 PSStandard(VS_STANDARD_OUTPUT input) : SV_TARGET
{
    uint textureIdx = gnTextureIdx;
    uint uTextureIdx[5];
    
    for (int i = 0; i < 5; ++i)
    {
        uTextureIdx[i] = textureIdx % 100;
        textureIdx = textureIdx / 100;
    }
    
    float4 cDiffuseColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_DIFFUSE_MAP)
        cDiffuseColor = gtxtDiffuseTexture[uTextureIdx[0]].Sample(gssWrap, input.uv);
    float4 cNormalColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_NORMAL_MAP)
        cNormalColor = gtxtNormalTexture[uTextureIdx[1]].Sample(gssWrap, input.uv);
    float4 cSpecularColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_SPECULAR_MAP)
        cSpecularColor = gtxtSpecularTexture[uTextureIdx[2]].Sample(gssWrap, input.uv);
    float4 cMetallicColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_METALLIC_MAP)
        cMetallicColor = gtxtMetallicTexture[uTextureIdx[3]].Sample(gssWrap, input.uv);
    float4 cEmissionColor = float4(0.0f, 0.0f, 0.0f, 0.0f);
    if (gnTextureMask & MATERIAL_EMISSION_MAP)
        cEmissionColor = gtxtEmissionTexture[uTextureIdx[4]].Sample(gssWrap, input.uv);
    if (cDiffuseColor.a < 0.2)
        clip(cDiffuseColor.a - 1.0f);
    
    float3 normalW;
    float4 cColor = cDiffuseColor + cEmissionColor + cMetallicColor;
    if (gnTextureMask & MATERIAL_NORMAL_MAP)
    {
        float3x3 TBN = float3x3(normalize(input.tangentW), normalize(input.bitangentW), normalize(input.normalW));
        float3 vNormal = normalize(cNormalColor.rgb * 2.0f - 1.0f); //[0, 1] → [-1, 1]
        normalW = normalize(mul(vNormal, TBN));
    }
    else
    {
        return cColor;
        normalW = normalize(input.normalW);
    }

    
    uint materialNum = gnMaterial;
    float4 cIllumination = Lighting(input.positionW, normalW, materialNum, cSpecularColor);
    cIllumination = cIllumination + float4(0.5f, 0.5f, 0.5f, 0.0f);
    float4 finalColor = saturate(cColor * cIllumination);
    
    if (DecalShape != -1)
    {
        if (input.isDummy)
            finalColor.a = 0.51;
        else
        {
            finalColor.a = 1.0f;
        
        }
    }
    else
    {
        if (input.isDummy)
            finalColor.a = 1.0;
        else
        {
            clip(-1);
        
        }
    }
    return finalColor;
    
    
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Instancing Illumination Shader Code

struct VS_INSTANCING_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    
    float4x4 transform : WORLDMATRIX;
    bool isDummy : ISDUMMY;
};
struct VS_INSTANCING_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITION;
    float2 uv : TEXCOORD;
    float3 normalW : NORMAL;
    float3 tangentW : TANGENT;
    float3 bitangentW : BITANGENT;   
    bool isDummy : ISDUMMY;
    
    
};


VS_INSTANCING_OUTPUT VSStandardInstancing(VS_INSTANCING_INPUT input)
{
    VS_INSTANCING_OUTPUT output;
    output.positionW = mul(float4(input.position, 1.0f), input.transform).xyz;
    output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
    output.normalW = mul(input.normal, (float3x3) input.transform);
    output.tangentW = mul(input.tangent, (float3x3) input.transform);
    output.bitangentW = mul(input.bitangent, (float3x3) input.transform);
    output.isDummy = input.isDummy;
    
    output.uv = input.uv;
 
    return (output);
}

//////////////////////////////////////////////////////////////////////////////////////////////
// BillboardTexture(GS) Shader Code

struct VS_BILLBOARD_GS_INPUT
{
    float3 positionW : POSITION;
    float3 billboardInfo : BILLBOARDINFO; //(width, height, textureIdx)
};

struct VS_BILLBOARD_GS_OUTPUT
{
    float3 centerW : POSITION;
    float2 size : SIZE;
    uint textureIdx : TEXTUREIDX;
};

struct GS_BILLBOARD_GS_OUTPUT
{
    float4 posH : SV_POSITION;
    float3 posW : POSITION;
    float3 normalW : NORMAL;
    float2 uv : TEXCOORD;
    uint primID : SV_PrimitiveID;
};

VS_BILLBOARD_GS_OUTPUT VSBillboard(VS_BILLBOARD_GS_INPUT input)
{
    VS_BILLBOARD_GS_OUTPUT output;

    output.centerW = input.positionW;

    output.size.xy = input.billboardInfo.xy;
    output.textureIdx = (uint) input.billboardInfo.z;

    return (output);
}

[maxvertexcount(4)]
void GSBillboard(point VS_BILLBOARD_GS_OUTPUT input[1], uint primID : SV_PrimitiveID, inout TriangleStream<GS_BILLBOARD_GS_OUTPUT> outStream)
{
    float3 vUp = float3(0.0f, 1.0f, 0.0f);
    float3 vLook = gvCameraPosition.xyz - input[0].centerW;
    vLook = normalize(vLook);
    float3 vRight = cross(vUp, vLook);
    float fHalfW = input[0].size.x * 0.5f;
    float fHalfH = input[0].size.y * 0.5f;
    float4 pVertices[4];
    
    pVertices[0] = float4(input[0].centerW + fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[1] = float4(input[0].centerW + fHalfW * vRight + fHalfH * vUp, 1.0f);
    pVertices[2] = float4(input[0].centerW - fHalfW * vRight - fHalfH * vUp, 1.0f);
    pVertices[3] = float4(input[0].centerW - fHalfW * vRight + fHalfH * vUp, 1.0f);

    float2 pUVs[4] = { float2(0.0f, 1.0f), float2(0.0f, 0.0f), float2(1.0f, 1.0f), float2(1.0f, 0.0f) };
    GS_BILLBOARD_GS_OUTPUT output;
    
    for (int j = 0; j < 4; ++j)
    {
        output.posW = pVertices[j].xyz;
        output.posH = mul(mul(pVertices[j], gmtxView), gmtxProjection);
        output.normalW = vLook;
        output.uv = pUVs[j];
        output.primID = input[0].textureIdx;
        outStream.Append(output);
    }
}

float4 PSBillboard(GS_BILLBOARD_GS_OUTPUT input) : SV_TARGET
{
    
    float4 cColor = gtxtBillboardTexture[NonUniformResourceIndex(input.primID)].Sample(gssWrap, input.uv);
    
    return (cColor);
}

//////////////////////////////////////////////////////////////////////////////////////////////
// Textured Shader Code

struct VS_TEXTURED_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
};

struct VS_TEXTURED_OUTPUT
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD;
};

VS_TEXTURED_OUTPUT VSTextured(VS_TEXTURED_INPUT input)
{
    VS_TEXTURED_OUTPUT output;

    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.uv = input.uv;

    return (output);
}

float4 PSTextured(VS_TEXTURED_OUTPUT input) : SV_TARGET
{
    float4 cColor = gtxtDiffuseTexture[0].Sample(gssWrap, input.uv);

    return (cColor);
}

struct VS_SKYED_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
};

struct VS_SKYED_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionL : POSITION;
    float2 uv : TEXCOORD;
};
VS_SKYED_OUTPUT VSSkyBoxed(VS_SKYED_INPUT input)
{
    VS_SKYED_OUTPUT output;

    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.positionL = input.position;
    output.uv = input.uv;

    return (output);
}

float4 PSSkyBoxed(VS_SKYED_OUTPUT input) : SV_TARGET
{
    float4 cColor = gtxtSkyBoxTexture[0].Sample(gssClamp, input.positionL);
    return (cColor);
}
////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Terrain Detail Textured Shader

struct VS_TERRAIN_INPUT
{
    float3 position : POSITION;
    float4 color : COLOR;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    uint texNumber : TEXNUMBER;
    float3 normal : NORMAL;
};

struct VS_TERRAIN_OUTPUT
{
    float4 position : SV_POSITION;
    float3 positionW : POSITIONTWORLD;
    float4 color : COLOR;
    float2 uv0 : TEXCOORD0;
    float2 uv1 : TEXCOORD1;
    uint texNumber : TEXNUMBER;
    float3 normal : NORMAL;
};
VS_TERRAIN_OUTPUT VSTerrain(VS_TERRAIN_INPUT input)
{
    VS_TERRAIN_OUTPUT output;
    float3 pos = input.position.xyz;
    output.position = mul(mul(mul(float4((input.position.xyz), 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    if (isNormal == 1)
    {
        pos.x = ((((input.position.x / terrainScale.x) / 256.0f) * 2) - 1.0f);
        pos.y = ((((input.position.z / terrainScale.z) / 256.0f) * 2) - 1.0f);
        pos.z = 0;
    //output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
        output.position = float4(pos, 1.0f);
    }
    
    output.positionW = (mul(float4(pos, 1.0f), gmtxWorld)).xyz;
    output.color = input.color;
    output.uv0 = input.uv0;
    output.uv1 = input.uv1;
    output.texNumber = input.texNumber;
    output.normal = input.normal, 1.0f;
    return (output);
}

float4 PSTerrain(VS_TERRAIN_OUTPUT input) : SV_TARGET
{
    
    //합쳐야할 컬러
    float4 cRenderTextureColor = float4(0.0, 0.0f, 0.0f, 1.0f);
    float4 cDetailTexColor0 = { 0.0, 0.0, 0.0, 1.0f };
    float4 cDetailTexColor1 = { 0.0, 0.0, 0.0, 1.0f };
    float4 finalColor = cRenderTextureColor;
    
    float4 cDetailTexNormal[12];
    //알파맵의 컬러값을 가져온다. 
    float4 alphaColor0 = gtxTerrainAlphaTexture0.Sample(gssTerrainWrap, input.uv0);
    float4 alphaColor1 = gtxTerrainAlphaTexture1.Sample(gssTerrainWrap, input.uv0);
    float4 alphaColor2 = gtxTerrainAlphaTexture2.Sample(gssTerrainWrap, input.uv0);
    
    float alphaNumber[12];
    alphaNumber[0] = alphaColor0.x;
    alphaNumber[1] = alphaColor0.y;
    alphaNumber[2] = alphaColor0.z;
    alphaNumber[3] = alphaColor0.a;
    alphaNumber[4] = alphaColor1.x;
    alphaNumber[5] = alphaColor1.y;
    alphaNumber[6] = alphaColor1.z;
    alphaNumber[7] = alphaColor1.a;
    alphaNumber[8] = alphaColor2.x;
    alphaNumber[9] = alphaColor2.y;
    alphaNumber[10] = alphaColor2.z;
    alphaNumber[11] = alphaColor2.a;
    
    
    float2 uv1 = input.uv0;
    float2 uv2 = input.uv0;
    
    float3 dPdx = ddx(input.positionW);
    float3 dPdy = ddy(input.positionW);
    float2 dUVdx = ddx(input.uv0);
    float2 dUVdy = ddy(input.uv0);
    float3 tangent = normalize((dUVdx.y * dPdy - dUVdy.y * dPdx));
    float3 bitangent = cross(tangent, input.normal);
    float3x3 TBN = float3x3((tangent), (bitangent), (input.normal));

// 객체 공간에서 텍스처 공간으로 변환하는 행렬

    int maxIndex = 0;
    int secondMaxIndex = 0;
    float maxValue = 0;
    
    for (int i = 0; i < 12; ++i)
    {
        if (maxValue < alphaNumber[i])
        {
            maxValue = alphaNumber[i];
            maxIndex = i;
        }
    }
    maxValue = 0;
    for (int j = 0; j < 12; ++j)
    {
        if (j != maxIndex && maxValue < alphaNumber[j])
        {
            maxValue = alphaNumber[j];
            secondMaxIndex = j;
        }
    }
    
    cDetailTexNormal[maxIndex] = gtxtTerrainDetailNormal[maxIndex].Sample(gssTerrainWrap, input.uv1);
    cDetailTexNormal[secondMaxIndex] = gtxtTerrainDetailNormal[secondMaxIndex].Sample(gssTerrainWrap, input.uv1);
    cDetailTexColor0 = gtxtTerrainDetailTexture[maxIndex].Sample(gssTerrainWrap, input.uv1);
    cDetailTexColor1 = gtxtTerrainDetailTexture[secondMaxIndex].Sample(gssTerrainWrap, input.uv1);
    float lerpValue = 0.0f;
    if (secondMaxIndex == maxIndex)
    {
        lerpValue = 0.0f;
        cDetailTexColor1 = float4(0.0f, 0.0f, 0.0f, 0.0f);
    }
    else
        lerpValue = alphaNumber[secondMaxIndex];
    float4 finalNormal = lerp(cDetailTexNormal[maxIndex], cDetailTexNormal[secondMaxIndex], lerpValue);
    
    float3 vNormal;
    float3 realNormal;
    
    vNormal = normalize(finalNormal.rgb * 2.0f - 1.0f); //[0, 1] → [-1, 1]
    realNormal = normalize(mul(vNormal, TBN));
    if (isNormal == 1)
    {
        return float4(realNormal, 1);
    }
    
    finalColor = lerp(cDetailTexColor0, cDetailTexColor1, lerpValue);
    
    //finalColor = saturate((cDetailTexColor0 * alphaColor0.r) + (cDetailTexColor1 * alphaColor0.g) + (cDetailTexColor2 * alphaColor0.b) + (cDetailTexColor3 * alphaColor0.a)
    //+ (cDetailTexColor4 * alphaColor1.r) + (cDetailTexColor5 * alphaColor1.g) + (cDetailTexColor6 * alphaColor1.b) + (cDetailTexColor7 * alphaColor1.a));
   

    
    float4 cIllumination = Lighting(input.positionW, input.normal, 1 , float4(1.0f , 1.0f , 1.0f , 1.0f));
    
    finalColor = saturate(finalColor * cIllumination);
    
    return finalColor;
}
//create terrain alpha map shader  
VS_TERRAIN_OUTPUT VSAlphaTerrain(VS_TERRAIN_INPUT input)
{
    VS_TERRAIN_OUTPUT output;
    
    float3 pos;
    pos.x = ((((input.position.x / terrainScale.x) / 256.0f) * 2) - 1.0f);
    pos.y = ((((input.position.z / terrainScale.z) / 256.0f) * 2) - 1.0f);
    pos.z = 0;
    //output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.position = float4(pos, 1.0f);
    output.color = input.color;
    output.uv0 = input.uv0;
    output.uv1 = input.uv1;
    output.texNumber = input.texNumber;
    output.normal = input.normal;
    return (output);
}

struct PSAlpha_OUTPUT
{
    float4 Color0 : SV_Target0;
    float4 Color1 : SV_Target1;
    float4 Color2 : SV_Target2;
};

PSAlpha_OUTPUT PSAlphaTerrain(VS_TERRAIN_OUTPUT input) : SV_TARGET
{
    
    PSAlpha_OUTPUT output;
    output.Color0 = float4(0.0f, 0.0f, 0.0f, 0.0f);
    output.Color1 = float4(0.0f, 0.0f, 0.0f, 0.0f);
    output.Color2 = float4(0.0f, 0.0f, 0.0f, 0.0f);
    
    
    if (input.texNumber == 0)
    {
        output.Color0 = float4(1.0f, 0.0f, 0.0f, 0.0f);
    }
    if (input.texNumber == 1)
    {
        output.Color0 = float4(0.0f, 1.0f, 0.0f, 0.0f);
    }
    if (input.texNumber == 2)
    {
        output.Color0 = float4(0.0f, 0.0f, 1.0f, 0.0f);
    }
    if (input.texNumber == 3)
    {
        output.Color0 = float4(0.0f, 0.0f, 0.0f, 1.0f);
    }
    if (input.texNumber == 4)
    {
        output.Color1 = float4(1.0f, 0.0f, 0.0f, 0.0f);
    }
    if (input.texNumber == 5)
    {
        output.Color1 = float4(0.0f, 1.0f, 0.0f, 0.0f);
    }
    if (input.texNumber == 6)
    {
        output.Color1 = float4(0.0f, 0.0f, 1.0f, 0.0f);
    }
    if (input.texNumber == 7)
    {
        output.Color1 = float4(0.0f, 0.0f, 0.0f, 1.0f);
    }
    if (input.texNumber == 8)
    {
        output.Color2 = float4(1.0f, 0.0f, 0.0f, 0.0f);
    }
    if (input.texNumber == 9)
    {
        output.Color2 = float4(0.0f, 1.0f, 0.0f, 0.0f);
    }
    if (input.texNumber == 10)
    {
        output.Color2 = float4(0.0f, 0.0f, 1.0f, 0.0f);
    }
    if (input.texNumber == 11)
    {
        output.Color2 = float4(0.0f, 0.0f, 0.0f, 1.0f);
    }
    
    return output;
}


VS_TERRAIN_OUTPUT VSHeightTerrain(VS_TERRAIN_INPUT input)
{
    VS_TERRAIN_OUTPUT output;
    
    float3 pos;
    pos.x = ((((input.position.x / terrainScale.x) / 256.0f) * 2) - 1.0f);
    pos.y = ((((input.position.z / terrainScale.z) / 256.0f) * 2) - 1.0f);
    pos.z = 0.0f;
    output.position = float4(pos, 1.0f);
    float posx = (input.position.x / terrainScale.x) / 256.0f;
    float posy = (input.position.y / terrainScale.y) / 256.0f;
    float posz = (input.position.z / terrainScale.z) / 256.0f;
    float4 color = float4(posy, posy, posy, 1.0f);
    output.positionW = (mul(float4(input.position.xyz, 1.0f), gmtxWorld)).xyz;
    output.color = color;
    output.uv0 = input.uv0;
    output.uv1 = input.uv1;
    output.texNumber = input.texNumber;
    output.normal = input.normal;
    return output;
}

float4 PSHeightTerrain(VS_TERRAIN_OUTPUT input) : SV_Target
{
    float4 alphaColor0 = gtxTerrainAlphaTexture0.Sample(gssTerrainWrap, input.uv0);
    float4 alphaColor1 = gtxTerrainAlphaTexture1.Sample(gssTerrainWrap, input.uv0);
    float4 alphaColor2 = gtxTerrainAlphaTexture2.Sample(gssTerrainWrap, input.uv0);
    
    
    float alphaNumber[12];
    alphaNumber[0] = alphaColor0.x;
    alphaNumber[1] = alphaColor0.y;
    alphaNumber[2] = alphaColor0.z;
    alphaNumber[3] = alphaColor0.a;
    alphaNumber[4] = alphaColor1.x;
    alphaNumber[5] = alphaColor1.y;
    alphaNumber[6] = alphaColor1.z;
    alphaNumber[7] = alphaColor1.a;
    alphaNumber[8] = alphaColor2.x;
    alphaNumber[9] = alphaColor2.y;
    alphaNumber[10] = alphaColor2.z;
    alphaNumber[11] = alphaColor2.a;
    
    
    int maxIndex = 0;
    int secondMaxIndex = 0;
    float maxValue = 0;
    
    for (int i = 0; i < 12; ++i)
    {
        if (maxValue < alphaNumber[i])
        {
            maxValue = alphaNumber[i];
            maxIndex = i;
        }
    }
    maxValue = 0;
    for (int j = 0; j < 12; ++j)
    {
        if (j != maxIndex && maxValue < alphaNumber[j])
        {
            maxValue = alphaNumber[j];
            secondMaxIndex = j;
        }
    }
    
    float4 cDetailTexColor0 = gtxtTerrainDetailTexture[maxIndex].Sample(gssTerrainWrap, input.uv1);
    float4 cDetailTexColor1 = gtxtTerrainDetailTexture[secondMaxIndex].Sample(gssTerrainWrap, input.uv1);
    
    float lerpValue = 0.0f;
    if (secondMaxIndex == maxIndex)
    {
        lerpValue = 0.0f;
        cDetailTexColor1 = float4(0.0f, 0.0f, 0.0f, 0.0f);
    }
    else
        lerpValue = alphaNumber[secondMaxIndex];
    
    float4 finalColor = lerp(cDetailTexColor0, cDetailTexColor1, lerpValue);
    float4 cIllumination = Lighting(input.positionW, input.normal, 1, float4(1.0f, 1.0f, 1.0f, 1.0f));
    
    finalColor = saturate(finalColor * cIllumination);
    
    return input.color;

}

//screen space decal shader 
struct VS_DEACAL_OUPUT
{
    float4 position : SV_POSITION;
    float2 uv : TEXCOORD;
}; 

float2 GetPixelUV(VS_DEACAL_OUPUT _In)
{
    float2 uv = float2(_In.position.x / FRAME_BUFFER_WIDTH, _In.position.y / FRAME_BUFFER_HEIGHT);
    return uv;
}
VS_DEACAL_OUPUT VSDecalShader(VS_TEXTURED_INPUT input)
{
    VS_DEACAL_OUPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.uv = input.uv;
    return (output);
}
float4 PSDecalShader(VS_DEACAL_OUPUT input) : SV_TARGET
{
    float2 uv = GetPixelUV(input);
    float2 tempUV = uv;
    
    float depth = gtxtTerrainDepthTexture.Sample(gssWrap, uv).x;
   
    uv = uv * 2.0f - 1.0f;
    uv.y = -uv.y;
    float4 projPos = float4(uv, depth, 1);
    float4 viewPos = mul(projPos, gmtxInversePro);
    viewPos /= viewPos.w;
    
    float4 worldPos = mul(viewPos, gmtxInverseView);
    float4 localPos = mul(worldPos, gmtxInverseWorld);
    
    localPos.xyz = abs(localPos.xyz);
    clip(0.5f - localPos.xyz);
    tempUV = localPos.xz + 0.5;
    float4 cColor = float4(1.0f, 0.0, 0.0, 1.0);
    if (DecalShape == 0)
    {
        cColor = gtxtTerrainDecalTexture.Sample(gssWrap, tempUV);
        clip(cColor.r < 0.99);
    }
    else{
        cColor = float4(1.0f, 0.0, 0.0, 1.0);
    }
    
    return cColor;
    
    //픽셀의 스크린 뎁스값 
}

/////////////////////////////////////////////////////
//AsistShader 

struct VS_TERRAINDEPTH_OUPUT
{
    float4 position : SV_POSITION;
    float4 viewpos : TEXCOORD0;
    float4 color : COLOR;
    float depth : DETPH;
};


VS_TERRAINDEPTH_OUPUT VSTerrainDepthShader(VS_TERRAIN_INPUT input)
{
    VS_TERRAINDEPTH_OUPUT output = (VS_TERRAINDEPTH_OUPUT) 0;
    output.position = mul(mul(mul(float4(input.position, 1.0f), gmtxWorld), gmtxView), gmtxProjection);
    output.viewpos = output.position;
    output.depth = output.viewpos.z / output.viewpos.w;
    output.color = float4(1.0f, 1.0f, 1.0f, 1.0f);
    return output;
}

float4 PSTerrainDepthShader(VS_TERRAINDEPTH_OUPUT input) : SV_Target
{
    
    float depth_color = input.depth;
    return float4(depth_color.xxx, 1.0f);
}

//Render Bounding Box


struct VS_BOUNDING_INPUT
{
    float3 position : POSITION;
    float4 color : COLOR;
    float4x4 transform : WORLDMATRIX;
    bool isDummy : ISDUMMY;
    uint bIdx : BIDX;
    
};
struct VS_BOUNDING_OUTPUT
{
    float4 position : SV_POSITION;
    float4 color : COLOR;
    bool isDummy : ISDUMMY;
    uint bIdx : BIDX;
};

VS_BOUNDING_OUTPUT VSBOUNDING(VS_BOUNDING_INPUT input)
{
    VS_BOUNDING_OUTPUT output;
    output.position = mul(mul(mul(float4(input.position, 1.0f), input.transform), gmtxView), gmtxProjection);
    output.color = input.color;
    output.isDummy = input.isDummy;
    output.bIdx = input.bIdx;
    return (output);
}
float4 PSBOUNDING(VS_BOUNDING_OUTPUT input) : SV_TARGET
{

    bool dummy = input.isDummy;
    
    if (DecalShape != -1)
    {
    }
    else
    {
        if (dummy == false)
        {
            clip(-1);
        }
       
    }
    
    float4 color = input.color;
    
    if (isNormal == input.bIdx)
    {
        color = float4(0.0f, 1.0f, 0.0f, 1.0f);
    }
    
    
    return (color);
}

////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////////
// Particle Shader Code

struct VS_PARTICLE_INPUT
{
    float2 size : SIZE;
    uint textureIdx : TEXTUREIDX;
    float3 position : POSITION;
    uint id : SV_VERTEXID;
    float4 color : COLOR;
    float alphaDegree : ALPHA;
};

struct GS_PARTICLE_INPUT
{
    float3 position : POSITION;
    float2 size : SIZE;
    uint textureIdx : TEXTUREIDX;
    float age : AGE;
    float4 color : COLOR;
    float alhpaDegree : ALPHA;
    
};

struct GS_PARTICLE_OUTPUT
{
    float4 position : SV_Position;
    float2 uv : TEXCOORD0;
    float2 screenTex : TEXCOORD1;
    float2 depth : TEXCOORD2;
    float age : AGE;
    float4 color : COLOR;
    uint textureIdx : TEXTUREIDX;
    float alphaDegree : ALPHA;
 
};

GS_PARTICLE_INPUT VSParticle(VS_PARTICLE_INPUT input)
{
    GS_PARTICLE_INPUT output;
    
    output.position = gsbParticleData[input.id].position.xyz + input.position;
    output.age = gsbParticleData[input.id].age;
    output.size = input.size;
    output.textureIdx = input.textureIdx;
    output.color = input.color;
    output.alhpaDegree = input.alphaDegree;
    
    return output;
}

[maxvertexcount(4)]
void GSParticleDraw(point GS_PARTICLE_INPUT input[1], inout TriangleStream<GS_PARTICLE_OUTPUT> outStream)
{
    if (input[0].age < 0.0f || input[0].age > gfParticleAge)
        return;
   
    GS_PARTICLE_OUTPUT output;
    
    float size = input[0].size.x;
    if(gnParticleType == PARTICLE_TYPE_SMALLER)
    {
        size -= (1 - (input[0].age) / gfParticleAge) * input[0].size.x;
    }
    [unroll]
    for (int j = 0; j < 4; ++j)
    {
        float3 position = gf3BillboardPos[j] * size;
        position = mul(position, (float3x3) gmtxInverseView) + input[0].position;
        output.position = mul(mul(float4(position, 1.0f), gmtxView), gmtxProjection);
        output.uv = gf2BillboardUVs[j];
        output.age = input[0].age;
        output.color = input[0].color;
        output.screenTex = output.position.xy / output.position.w;
        output.depth = output.position.zw;
        output.textureIdx = input[0].textureIdx;
        output.alphaDegree = input[0].alhpaDegree;
        
        outStream.Append(output);
    }
    outStream.RestartStrip();
}

float4 PSParticle(GS_PARTICLE_OUTPUT input) : SV_TARGET
{
    float2 screenTex = ((input.screenTex) + float2(1.0f, 1.0f)) * 0.5f;
    screenTex.y = 1 - screenTex.y;
    
    float depthSample = gtxtTerrainDepthTexture.Sample(gssDepthClamp, screenTex);
    // depth버퍼의 depth값
    float4 particleSample = gtxtParticleTexture[input.textureIdx].Sample(gssWrap, input.uv);

    float particleDepth = input.depth.x;
    particleDepth /= input.depth.y;
    // 파티클 입자의 depth값
    
    float depthFade = 0.0f;

    float4 depthViewSample = mul(float4(input.screenTex, depthSample, 1), gmtxInversePro);
    float4 depthViewParticle = mul(float4(input.screenTex, particleDepth, 1), gmtxInversePro);
    
    float depthDiff = (depthViewSample.z / depthViewSample.w) - (depthViewParticle.z / depthViewParticle.w);
    if (depthDiff < 0.0f)
        discard;
    
    depthFade = saturate(depthDiff / 10.0f);
    
    float4 light = float4(0.992f, 1.0f, 0.880f, 1.0f) + float4(0.525f, 0.474f, 0.474f, 1.0f);
    particleSample.rgb *= light.xyz * input.color.rgb;
    
    float age;
    if (gnParticleType == PARTICLE_TYPE_DISAPPEAR){
        age = input.age / gfParticleAge;
        age = saturate(age);
    }
    else{
        age = 0.0f;
    }


    particleSample.a *= (age + input.alphaDegree);
    particleSample.a *= depthFade;
    
    return particleSample;
}


/////////////////////////////////////////////////////////////////////////////


struct VS_SKINNED_STANDARD_INPUT
{
    float3 position : POSITION;
    float2 uv : TEXCOORD;
    float3 normal : NORMAL;
    float3 tangent : TANGENT;
    float3 bitangent : BITANGENT;
    int4 indices : BONEINDEX;
    float4 weights : BONEWEIGHT;
    uint objectIdx : OBJECTIDX;
    
};

VS_STANDARD_OUTPUT VSSkinnedAnimationStandard(VS_SKINNED_STANDARD_INPUT input)
{
    VS_STANDARD_OUTPUT output;

    float4x4 mtxVertexToBoneWorld = (float4x4) 0.0f;
    for (int i = 0; i < MAX_VERTEX_INFLUENCES; i++)
    {
        mtxVertexToBoneWorld += input.weights[i] * mul(gpmtxBoneOffsets[input.indices[i]], gsbBoneTransforms[(input.objectIdx * SKINNED_ANIMATION_BONES) + input.indices[i]]);
    }
    output.positionW = mul(float4(input.position, 1.0f), mtxVertexToBoneWorld).xyz;
    output.normalW = mul(input.normal, (float3x3) mtxVertexToBoneWorld).xyz;
    output.tangentW = mul(input.tangent, (float3x3) mtxVertexToBoneWorld).xyz;
    output.bitangentW = mul(input.bitangent, (float3x3) mtxVertexToBoneWorld).xyz;

    output.position = mul(mul(float4(output.positionW, 1.0f), gmtxView), gmtxProjection);
    output.uv = input.uv;
    output.isDummy = false;
        if(input.objectIdx == 0)
        output.isDummy = true; 
    
    return (output);
}
