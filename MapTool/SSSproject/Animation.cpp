#include "stdafx.h"
#include "Animation.h"


AnimationSets::AnimationSets(int AnimationSets)
{
	animationSetnum = AnimationSets;
	animationSets = new AnimationSet*[AnimationSets];
}

AnimationSets::~AnimationSets()
{
	for (int i = 0; i < animationSetnum; i++) if (animationSets[i]) delete animationSets[i];
	if (animationSets) delete[] animationSets;

	if (animatedBoneFrameCaches) delete[] animatedBoneFrameCaches;
}

void AnimationSets::SetCallbackKeys(int AnimationSet, int CallbackKeys)
{
	animationSets[AnimationSet]->callbackKeynum = CallbackKeys;
	animationSets[AnimationSet]->callbackKeys = new CALLBACKKEY[CallbackKeys];
}

void AnimationSets::SetCallbackKey(int AnimationSet, int KeyIndex, float KeyTime, void *Data)
{
	animationSets[AnimationSet]->callbackKeys[KeyIndex].time = KeyTime;
	animationSets[AnimationSet]->callbackKeys[KeyIndex].callbackData = Data;
}

void AnimationSets::SetAnimationCallbackHandler(int AnimationSet, AnimationCallbackHandler *CallbackHandler)
{
	animationSets[AnimationSet]->SetAnimationCallbackHandler(CallbackHandler);
}

AnimationController::AnimationController(ID3D12Device *device, ID3D12GraphicsCommandList *commandList, int AnimationTracks, LoadedModelInfo *Model)
{
	animationTracknum = AnimationTracks;
	animationTracks = new AnimationTrack[AnimationTracks];

	animationSets = Model->animationSets;
	animationSets->AddRef();

	skinnedMeshnum = Model->skinnedMeshNum;
	skinnedMeshes = new SkinnedMesh*[skinnedMeshnum];
	for (int i = 0; i < skinnedMeshnum; i++) skinnedMeshes[i] = Model->skinnedMeshes[i];
}

AnimationController::~AnimationController()
{
	if (animationTracks) delete[] animationTracks;

	for (int i = 0; i < skinnedMeshnum; i++)
	{
		skinningBoneTransforms[i]->Unmap(0, NULL);
		skinningBoneTransforms[i]->Release();
	}
	if (skinningBoneTransforms) delete[] skinningBoneTransforms;
	if (xm4x4MappedSkinningBoneTransforms) delete[] xm4x4MappedSkinningBoneTransforms;

	if (animationSets) animationSets->Release();

	if (skinnedMeshes) delete[] skinnedMeshes;
}

void AnimationController::SetCallbackKeys(int AnimationSet, int CallbackKeys)
{
	if (animationSets) animationSets->SetCallbackKeys(AnimationSet, CallbackKeys);
}

void AnimationController::SetCallbackKey(int nAnimationSet, int nKeyIndex, float fKeyTime, void *pData)
{
	if (animationSets) animationSets->SetCallbackKey(nAnimationSet, nKeyIndex, fKeyTime, pData);
}

void AnimationController::SetAnimationCallbackHandler(int nAnimationSet, AnimationCallbackHandler *pCallbackHandler)
{
	if (animationSets) animationSets->SetAnimationCallbackHandler(nAnimationSet, pCallbackHandler);
}

void AnimationController::SetTrackAnimationSet(int AnimationTrack, int AnimationSet)
{
	if (animationTracks) animationTracks[AnimationTrack].animationSet = AnimationSet;
}

void AnimationController::SetTrackEnable(int AnimationTrack, bool Enable)
{
	if (animationTracks) animationTracks[AnimationTrack].SetEnable(Enable);
}

void AnimationController::SetTrackPosition(int AnimationTrack, float Position)
{
	if (animationTracks) animationTracks[AnimationTrack].SetPosition(Position);
}

void AnimationController::SetTrackSpeed(int AnimationTrack, float Speed)
{
	if (animationTracks) animationTracks[AnimationTrack].SetSpeed(Speed);
}

void AnimationController::SetTrackWeight(int AnimationTrack, float Weight)
{
	if (animationTracks) animationTracks[AnimationTrack].SetWeight(Weight);
}

void AnimationController::UpdateShaderVariables(ID3D12GraphicsCommandList *commandList)
{
	for (int i = 0; i < skinnedMeshnum; i++)
	{
		skinnedMeshes[i]->skinningBoneTransforms = skinningBoneTransforms[i];
		skinnedMeshes[i]->mappedSkinningBoneTransforms = xm4x4MappedSkinningBoneTransforms[i];
	}
}

void AnimationController::AdvanceTime(float TimeElapsed, GameObject *RootGameObject)
{
	time += TimeElapsed;
	if (animationTracks)
	{
		for (int i = 0; i < animationTracknum; i++) animationTracks[i].position += (TimeElapsed * animationTracks[i].speed);

		for (int j = 0; j < animationSets->animatedBoneFrames; j++)
		{
			XMFLOAT4X4 xmf4x4Transform = Matrix4x4::Zero();
			for (int k = 0; k < animationTracknum; k++)
			{
				if (animationTracks[k].enable)
				{
					AnimationSet *pAnimationSet = animationSets->animationSets[animationTracks[k].animationSet];
					pAnimationSet->SetPosition(animationTracks[k].position);
					XMFLOAT4X4 xmf4x4TrackTransform = pAnimationSet->GetSRT(j);
					xmf4x4Transform = Matrix4x4::Add(xmf4x4Transform, Matrix4x4::Scale(xmf4x4TrackTransform, animationTracks[k].weight));
				}
			}
			animationSets->animatedBoneFrameCaches[j]->SetTransform(xmf4x4Transform);
		}

		RootGameObject->UpdateTransform(NULL);
	}
}

LoadedModelInfo::~LoadedModelInfo()
{
	if (skinnedMeshes) delete[] skinnedMeshes;
}

void LoadedModelInfo::PrepareSkinning()
{
	int nSkinnedMesh = 0;
	skinnedMeshes = new SkinnedMesh*[skinnedMeshNum];
	modelRootObject->FindAndSetSkinnedMesh(skinnedMeshes, &nSkinnedMesh);

	for (int i = 0; i < skinnedMeshNum; i++) skinnedMeshes[i]->PrepareSkinning(modelRootObject);
}

// 애니메이션 컨트롤러 수정 중

AnimationSet::AnimationSet(float Length, int FramesPerSecond, int KeyFrames, int AnimatedBones, char *Name)
{
	length = Length;
	framesPerSecond = FramesPerSecond;
	keyFrames = KeyFrames;

	strcpy_s(animationSetName, 64, Name);

	keyFrameTimes = new float[KeyFrames];
	keyFrameTransforms = new XMFLOAT4X4*[KeyFrames];

	//이 부분 변경해야함(뼈마다 키프레임 개 수 다르게 나옴)
	for (int i = 0; i < KeyFrames; i++) keyFrameTransforms[i] = new XMFLOAT4X4[AnimatedBones];
}

AnimationSet::~AnimationSet()
{
	if (keyFrameTimes) delete[] keyFrameTimes;
	for (int j = 0; j < keyFrames; j++) if (keyFrameTransforms[j]) delete[] keyFrameTransforms[j];
	if (keyFrameTransforms) delete[] keyFrameTransforms;

	if (callbackKeys) delete[] callbackKeys;
	if (animationCallbackHandler) delete animationCallbackHandler;
}

void *AnimationSet::GetCallbackData()
{
	for (int i = 0; i < callbackKeynum; i++)
	{
		if (::IsEqual(callbackKeys[i].time, position, ANIMATION_CALLBACK_EPSILON)) return(callbackKeys[i].callbackData);
	}
	return(NULL);
}

void AnimationSet::SetPosition(float TrackPosition)
{
	position = TrackPosition;
	switch (animationType)
	{
	case AnimationTypeLoop:
	{
		position = fmod(TrackPosition, keyFrameTimes[keyFrames - 1]); // m_fPosition = fTrackPosition - int(fTrackPosition / m_pfKeyFrameTimes[m_nKeyFrames-1]) * m_pfKeyFrameTimes[m_nKeyFrames-1];
//			m_fPosition = fmod(fTrackPosition, m_fLength); //if (m_fPosition < 0) m_fPosition += m_fLength;
//			m_fPosition = fTrackPosition - int(fTrackPosition / m_fLength) * m_fLength;
		break;
	}
	case AnimationTypeOnce:
		break;
	case AnimationTypePingpong:
		break;
	}

	if (animationCallbackHandler)
	{
		void *callbackData = GetCallbackData();
		if (callbackData) animationCallbackHandler->HandleCallback(callbackData);
	}
}

XMFLOAT4X4 AnimationSet::GetSRT(int nBone)
{
	XMFLOAT4X4 xmf4x4Transform = Matrix4x4::Identity();
#ifdef _WITH_ANIMATION_SRT
	XMVECTOR S, R, T;
	for (int i = 0; i < (m_nKeyFrameTranslations - 1); i++)
	{
		if ((m_pfKeyFrameTranslationTimes[i] <= m_fPosition) && (m_fPosition <= m_pfKeyFrameTranslationTimes[i + 1]))
		{
			float t = (m_fPosition - m_pfKeyFrameTranslationTimes[i]) / (m_pfKeyFrameTranslationTimes[i + 1] - m_pfKeyFrameTranslationTimes[i]);
			T = XMVectorLerp(XMLoadFloat3(&m_ppxmf3KeyFrameTranslations[i][nBone]), XMLoadFloat3(&m_ppxmf3KeyFrameTranslations[i + 1][nBone]), t);
			break;
		}
	}
	for (UINT i = 0; i < (m_nKeyFrameScales - 1); i++)
	{
		if ((m_pfKeyFrameScaleTimes[i] <= m_fPosition) && (m_fPosition <= m_pfKeyFrameScaleTimes[i + 1]))
		{
			float t = (m_fPosition - m_pfKeyFrameScaleTimes[i]) / (m_pfKeyFrameScaleTimes[i + 1] - m_pfKeyFrameScaleTimes[i]);
			S = XMVectorLerp(XMLoadFloat3(&m_ppxmf3KeyFrameScales[i][nBone]), XMLoadFloat3(&m_ppxmf3KeyFrameScales[i + 1][nBone]), t);
			break;
		}
	}
	for (UINT i = 0; i < (m_nKeyFrameRotations - 1); i++)
	{
		if ((m_pfKeyFrameRotationTimes[i] <= m_fPosition) && (m_fPosition <= m_pfKeyFrameRotationTimes[i + 1]))
		{
			float t = (m_fPosition - m_pfKeyFrameRotationTimes[i]) / (m_pfKeyFrameRotationTimes[i + 1] - m_pfKeyFrameRotationTimes[i]);
			R = XMQuaternionSlerp(XMLoadFloat4(&m_ppxmf4KeyFrameRotations[i][nBone]), XMLoadFloat4(&m_ppxmf4KeyFrameRotations[i + 1][nBone]), t);
			break;
		}
	}

	XMStoreFloat4x4(&xmf4x4Transform, XMMatrixAffineTransformation(S, NULL, R, T));
#else   
	for (int i = 0; i < (keyFrames - 1); i++)
	{
		if ((keyFrameTimes[i] <= position) && (position <= keyFrameTimes[i + 1]))
		{
			float t = (position - keyFrameTimes[i]) / (keyFrameTimes[i + 1] - keyFrameTimes[i]);
			xmf4x4Transform = Matrix4x4::Interpolate(keyFrameTransforms[i][nBone], keyFrameTransforms[i + 1][nBone], t);
			break;
		}
	}
#endif
	return(xmf4x4Transform);
}

void AnimationSet::SetCallbackKeys(int CallbackKeys)
{
	callbackKeynum = CallbackKeys;
	callbackKeys = new CALLBACKKEY[callbackKeynum];
}

void AnimationSet::SetCallbackKey(int KeyIndex, float KeyTime, void *Data)
{
	callbackKeys[KeyIndex].time = KeyTime;
	callbackKeys[KeyIndex].callbackData = Data;
}

void AnimationSet::SetAnimationCallbackHandler(AnimationCallbackHandler *CallbackHandler)
{
	animationCallbackHandler = CallbackHandler;
}

// 애니메이션 컨트롤러 수정 중