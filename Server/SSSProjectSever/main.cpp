#include "protocol.h"
#include "stdafx.h"
#include "terrain.h"

mutex all_l;

constexpr auto MAX_PACKET_SIZE = 255;
constexpr auto MAX_BUF_SIZE = 1024;
constexpr auto MAX_USER = 3000;
enum ENUMOP { OP_RECV, OP_SEND, OP_ACCEPT , OP_NPC_WANDER , OP_NPC_MOVE , OP_NPC_IDLE , OP_NPC_CHASE , OP_NPC_ATTACK};
enum C_STATUS { ST_FREE, ST_ALLOC, ST_ACTIVE };
enum C_JOB {
	RIFLE
};
enum C_RIFLE_ANITRACK {
	MainCharacterAim = 0,
	MainCharacterIdle = 1,

	MainCharacterWalk = 7,
	MainCharacterRun = 8,

	MainCharacterFire = 11,
	MainCharacterWalkingFire = 12,
	MainCharacterReload = 13,
	MainCharacterWalkingReload = 14,
	MainCharacterHit = 15,
	MainCharacterDeath = 16
};
struct EXOVER {		// overlapped 확장 구조체
	WSAOVERLAPPED	overlapped;
	ENUMOP			op;
	char			io_buf[MAX_BUF_SIZE];	// 한 개의 패킷보다 더 많이 올 수 있기때문에 충분히 크게 만든다.
	union {
		WSABUF			wsabuf;
		SOCKET			c_socket;
	};
};
bool isMapMove = false;
char curMap = 0;
mutex im_cl;
class CLIENT {
public:

	CLIENT() = default;
	CLIENT(const CLIENT&) = default;
	CLIENT& operator=(const CLIENT&) = default;
	mutex m_cl;
	SOCKET		socket;
	int			m_id;
	EXOVER		recv_over;
	int			prev_size;
	char		packet_buf[MAX_PACKET_SIZE];
	C_STATUS	m_status;

	XMFLOAT3	velocity = XMFLOAT3(0.0f, 0.0f ,0.0f);
	XMFLOAT3	pos = XMFLOAT3(0.0f , 0.0f , 0.0f );
	XMFLOAT3	look = XMFLOAT3(0.0f, 0.0f, 0.0f);
	BoundingOrientedBox obbBox;
	char		aniTrackUp;
	char		aniTrackLo;
	char		cJop;
	char		isReady = 0;
	char		scene = 0;
	char		weapon = 0;
	char		dir;
	short		hp = 100;
	char		mapNum;
	char		sectorx;
	char		sectorz;
	char		state;
	char		bufferidx;
	short		objectidx;
	bool		gameStart = false;
	float		stime = 0.0f;
	int		    tileX;
	int			tileZ;
	int			target;
	char		potal;
	bool		isLive = true;
	bool		isAstar = false;
	vector<AstarInform> atvec;

};


struct event_type {
	int id;
	ENUMOP event_id;
	chrono::high_resolution_clock::time_point time;

	constexpr bool operator < (const event_type& left) const
	{
		return (time > left.time);
	}
};

struct copyClient {
	int id;
	XMFLOAT3 pos;
	XMFLOAT3 look;
	XMFLOAT3 velocity = XMFLOAT3(0.0f , 0.0f , 0.0f);
	BoundingOrientedBox obbBox;
	bool		isAstar = false;
	vector<AstarInform> atvec;


	int		 state;
	short	 hp;
	char	 aniTrack;
	float    stime;
	int	 tileX;
	int  tileZ;
	int	 target;
	char bIdx;
	

};


CLIENT g_clients[MAX_USER];
int g_cur_user_id = 0;
mutex g_muid;
int g_cur_user_count = 0;
mutex g_ucount;
HANDLE g_iocp;
SOCKET listen_socket;
priority_queue<event_type> timer_queue;
mutex timer_lock;
int total_zombie;


void add_timer(int c_id, ENUMOP op_type, int duration)
{
	timer_lock.lock();
	event_type ev{ c_id ,op_type, high_resolution_clock::now() + milliseconds(duration) };
	timer_queue.push(ev);
	timer_lock.unlock();
}

bool CAS(bool* addr, int expected, int new_val)
{
	return atomic_compare_exchange_strong
	(
		reinterpret_cast<atomic_int*>(addr),
		&expected,
		new_val
	);
}
void send_packet(int user_id, void* p)
{
	char* buf = reinterpret_cast<char*>(p);

	CLIENT& user = g_clients[user_id];

	EXOVER* exover = new EXOVER; // 포인터로 만들지 않으면 다른 함수에서 재사용이 되는 이 변수가 send_packet함수가
	// 종료되면 로컬변수인 이유로 날아가기 때문에 포인터로 만들어야 한다.
	exover->op = OP_SEND;
	ZeroMemory(&(exover->overlapped), sizeof(exover->overlapped));
	memcpy(exover->io_buf, buf, buf[0]);
	exover->wsabuf.buf = exover->io_buf;
	exover->wsabuf.len = buf[0];
	WSASend(user.socket, &exover->wsabuf, 1, NULL, 0, &exover->overlapped, NULL); // 두 번째 인자에 recv에 사용되는 user.recv_over.overlapped는 사용할 수 없다.
}

void send_move_packet(int user_id, int mover_id)
{
	sc_packet_move p;
	p.id = mover_id;
	p.size = sizeof(p);
	p.type = S2C_MOVE;
	p.pos = g_clients[mover_id].pos;
	p.look = g_clients[mover_id].look;
	p.aniTrackUp = g_clients[mover_id].aniTrackUp;
	p.aniTrackLo = g_clients[mover_id].aniTrackLo;
	p.direction = g_clients[mover_id].dir;
	p.velocity = g_clients[mover_id].velocity;
	p.hp = g_clients[mover_id].hp;
	p.jump = false;
	send_packet(user_id, &p); 
}

void send_endgame_pacekt(int user_id)
{
	sc_packet_endgame p;
	p.size = sizeof(p);
	p.type = S2C_ENDGAME;
	send_packet(user_id, &p);
}

void send_loadMap_packet(int user_id , int mapNum) {
	sc_packet_loadmap p;
	p.id = user_id;
	p.size = sizeof(p);
	p.type = S2C_MAP;
	p.mapNum = mapNum;
	send_packet(user_id , &p);
}

void send_zombie_move_packet(int user_id, int mover_id) {
	sc_packet_zombie p;
	p.id = mover_id;
	p.size = sizeof(p);
	p.type = S2C_ZOMBIE;
	g_clients[mover_id].m_cl.lock();
	p.pos = g_clients[mover_id].pos;
	p.look = g_clients[mover_id].look;
	p.aniTrack = g_clients[mover_id].aniTrackUp;
	p.velocity = g_clients[mover_id].velocity;
	p.hp = g_clients[mover_id].hp;
	p.state = g_clients[mover_id].state;
	p.bidx = g_clients[mover_id].bufferidx;
	p.oidx = g_clients[mover_id].objectidx;
	g_clients[mover_id].m_cl.unlock();
	send_packet(user_id, &p);
}

void send_changejob_packet(int user_id, int change_id)
{
	sc_packet_lobby p;
	p.id = change_id;
	p.size = sizeof(p);
	p.type = S2C_CHANGE_JOB;
	p.cJob = g_clients[change_id].cJop;
	p.isReady = g_clients[change_id].isReady;
	send_packet(user_id, &p);
}

void send_changeweapon_packet(int user_id, int change_id)
{
	sc_packet_weapon p;
	p.id = change_id;
	p.size = sizeof(p);
	p.type = S2C_WEAPON;
	p.weaponNum = g_clients[change_id].weapon;
	send_packet(user_id, &p);
}

void send_enter_lobby_packet(int user_id, int enter_id)
{
	sc_packet_lobby p;
	p.id = enter_id;
	p.size = sizeof(p);
	p.type = S2C_ENTER_LOBBY;
	p.cJob = g_clients[enter_id].cJop;
	p.isReady = g_clients[enter_id].isReady;
	send_packet(user_id, &p);
}

void send_leave_packet(int user_id, int leaver_id)
{
	sc_packet_leave p;

	p.id = leaver_id;
	p.size = sizeof(p);
	p.type = S2C_LEAVE;

	send_packet(user_id, &p);
}

void send_loginok_packet(int user_id)
{
	sc_packet_lobby p;
	p.id = user_id;
	p.size = sizeof(p);
	p.type = S2C_LOGIN_OK;
	p.cJob = PlayerJobRifle;
	p.isReady = 0;
	g_clients[user_id].m_cl.lock();
	g_clients[user_id].isReady = p.isReady;
	g_clients[user_id].cJop = p.cJob;
	g_clients[user_id].m_cl.unlock();
	send_packet(user_id, &p); // 패킷을 통채로 넣어주면 곤란하다. 메모리가 너무 크다..
}
void send_effect_packet(int user_id, char effectType, XMFLOAT3 pos)
{
	sc_packet_effect p;
	p.size = sizeof(p);
	p.type = S2C_EFFECT;
	p.effectType = effectType;
	p.pos = pos;
	send_packet(user_id, &p);
}
void send_ingame_packet(int user_id)
{
	sc_packet_ingame_enter p;
	p.id = user_id;
	p.size = sizeof(p);
	p.type = S2C_ENTER_INGAME;
	p.pos = g_clients[user_id].pos;
	p.look = g_clients[user_id].look;
	p.cJob = g_clients[user_id].cJop;
	p.hp = g_clients[user_id].hp;
	send_packet(user_id, &p); // 패킷을 통채로 넣어주면 곤란하다. 메모리가 너무 크다..
}
void ConvertZombie() {
	int count = 0;
	for (int i = 0; i < sbuffer.size(); ++i) {
		for (int j = 1; j < sbuffer[i].sObjvec.size(); ++j) {
			int id = ZOMBIE_START_NUMBER + count;
			g_clients[id].pos = sbuffer[i].sObjvec[j].GetPos();
			g_clients[id].look = sbuffer[i].sObjvec[j].GetLook();
			g_clients[id].hp = 100;
			g_clients[id].state = IDLE;
			g_clients[id].dir = -1;
			g_clients[id].m_id = id;
			g_clients[id].aniTrackUp = rand() % 5;
			g_clients[id].bufferidx = i;
			g_clients[id].objectidx = j;
			g_clients[id].obbBox = sbuffer[i].sObjvec[j].obbBox;
			g_clients[id].isLive = true;
			g_clients[id].isAstar = false;

			count++;
		}
	}
	total_zombie = count;

	for(int i = ZOMBIE_START_NUMBER; i < ZOMBIE_START_NUMBER + total_zombie; ++i){
		XMFLOAT3 zpos = g_clients[i].pos;
		zpos.x = zpos.x + 257.0f;
		zpos.z = zpos.z + 257.0f;
		int sectorX = zpos.x / SECTOR_SIZE;
		int sectorZ = zpos.z / SECTOR_SIZE;
		SectorList[sectorZ][sectorX].insert(i);
	}
}
XMFLOAT3 GetFinalVec(copyClient& ob , float force , XMFLOAT3 dir) {
	float maxVelocityXZ = 6.0f;
	float mass = 6.0f;
	float friction = 20.0f;

	XMFLOAT3 vForce = XMFLOAT3(0, 0, 0);
	vForce = Vector3::Add(vForce, dir, force);

	XMFLOAT3 acc = XMFLOAT3(vForce.x / mass, 0, vForce.z / mass); // 가속도 
	float vlength = Vector3::Length(ob.velocity);

	if (vlength < maxVelocityXZ)
		ob.velocity = Vector3::Add(ob.velocity, XMFLOAT3((acc.x * 0.01f), 0, (acc.z * 0.01f)));

	XMFLOAT3 finalvec = Vector3::Add(XMFLOAT3(ob.velocity.x * 0.01f, 0.0f, ob.velocity.z * 0.01f), XMFLOAT3(acc.x * 0.01f * 0.01f * 0.5f, 0.0f, acc.z * 0.01f * 0.01f * 0.5f));

	return finalvec;
}
void ProcessGarbageZombie(int bIdx , int id ){
	if (bIdx == (MIDDLEBOSS - 3) || bIdx == (FINALBOSS - 3)) {
		//중간보스를 잡았다면 
		for (int i = ZOMBIE_START_NUMBER; i < ZOMBIE_START_NUMBER + total_zombie; ++i) {
			//모든 좀비들 
			g_clients[i].m_cl.lock();
			if (g_clients[i].state != DEATH) {
				g_clients[i].state = DEATH;
				g_clients[i].aniTrackUp = rand() % 2 + ZombieDeath1;
				g_clients[i].velocity = XMFLOAT3(0, 0, 0);
			}
			else {
				g_clients[i].m_cl.unlock();
				continue;
				//죽었다면 패킷 보내줄 필요없음 
			}
			g_clients[i].m_cl.unlock();

			for (int j = 0; j < 10; ++j) {
				g_clients[j].m_cl.lock();
				if (g_clients[j].m_status == ST_ACTIVE) {
					send_zombie_move_packet(j, i);
				}
				g_clients[j].m_cl.unlock();
			}
			//죽은 상태 아닌것들이 죽었다고 보내줌 

			for (int j = 0; j < 10; ++j) {
				g_clients[j].m_cl.lock();
				if (g_clients[j].m_status == ST_ACTIVE) {
					send_zombie_move_packet(j, id);
				}
				g_clients[j].m_cl.unlock();
			}

		}
		im_cl.lock();
		isMapMove = true;
		im_cl.unlock();

	}
	else {
		//중간보스가 아니라면 그냥 브로드 캐스트 
		for (int i = 0; i < ZOMBIE_START_NUMBER; ++i) {
			g_clients[i].m_cl.lock();
			if (g_clients[i].m_status == ST_ACTIVE) {
				send_zombie_move_packet(i, id);
			}
			g_clients[i].m_cl.unlock();
		}
	}
}
void MoveZombie(copyClient& zombie , float force, bool updateVelocity ,XMFLOAT3 direction)
{

	//좀비끼리 충돌 
	XMFLOAT3 tpos;
	XMFLOAT3 finalvec = XMFLOAT3(0,0,0);
	auto& ob = zombie;
	float friction = 20.0f;


	for (int i = ZOMBIE_START_NUMBER; i < ZOMBIE_START_NUMBER + total_zombie; ++i) {
		copyClient otherZombie;
		if (i == zombie.id)
			continue;
		g_clients[i].m_cl.lock();
		otherZombie.target = g_clients[i].target;
		otherZombie.obbBox = g_clients[i].obbBox;
		otherZombie.pos = g_clients[i].pos;
		otherZombie.state = g_clients[i].state;
		otherZombie.target = g_clients[i].target;
		g_clients[i].m_cl.unlock();
		if (otherZombie.state == DEATH)
			continue;

		if (zombie.target == otherZombie.target && otherZombie.state == CHASE && zombie.state == CHASE) {
			if (Vector3::Length(Vector3::Subtract(zombie.pos, otherZombie.pos)) < 2.0f) {
				XMFLOAT3 targetPos;
				g_clients[zombie.target].m_cl.lock();
				targetPos = g_clients[zombie.target].pos;
				g_clients[zombie.target].m_cl.unlock();

				auto mlen = Vector3::Length(Vector3::Subtract(zombie.pos, targetPos));
				auto olen = Vector3::Length(Vector3::Subtract(otherZombie.pos, targetPos));
				if (olen <= mlen) {
					XMFLOAT3 ozDir = Vector3::Normalize(Vector3::Subtract(zombie.pos, otherZombie.pos)); // 내가 밀려날 방향 
					finalvec = Vector3::Add(finalvec, GetFinalVec(zombie, force, ozDir));
				}
				else {
					XMFLOAT3 ozDir = Vector3::Normalize(Vector3::Subtract(otherZombie.pos, zombie.pos)); // 다른좀비가 밀려날 방향 
					finalvec = Vector3::Add(finalvec, GetFinalVec(otherZombie, force, ozDir));
					g_clients[i].m_cl.lock();
					Vector3::Add(g_clients[i].pos, finalvec);
					g_clients[i].m_cl.unlock();

				}
			}
		}
		else {
			if (Vector3::Length(Vector3::Subtract(zombie.pos, otherZombie.pos)) < 2.0f) {
				XMFLOAT3 ozDir = Vector3::Normalize(Vector3::Subtract(zombie.pos, otherZombie.pos)); // 내가 밀려날 방향 
				finalvec = Vector3::Add(finalvec, GetFinalVec(zombie, force, ozDir));
			}
		}
		
	}


		finalvec = Vector3::Add(finalvec, GetFinalVec(zombie, force, direction));


		tpos = (Vector3::Add(ob.pos, finalvec));
		ob.obbBox.Center = tpos;


		int objsectorx = int(tpos.x + 257.0f) / OBJSECTOR_SIZE;
		int objsectorz = int(tpos.z + 257.0f) / OBJSECTOR_SIZE;

		bool isColl = false;
		for (auto a : objSectorList[objsectorz][objsectorx]) {
			for (int i = 0; i < objvec[a].obbvec.size(); ++i) {
				if (objvec[a].obbvec[i].Intersects(ob.obbBox)) {
					isColl = true;
				}
			}
		}



		if (tpos.x < 256.0f && tpos.x > -256.0f && tpos.z < 256.0f && tpos.z > -256.0f && isColl == false) {
			ob.pos = (Vector3::Add(ob.pos, finalvec));
			ob.obbBox.Center = ob.pos;
		}
		else {


			ob.pos = (Vector3::Add(ob.pos, Vector3::ScalarProduct((finalvec) , -10.0f , false)));
			ob.obbBox.Center = ob.pos;
		}
		ob.obbBox.Center = ob.pos;

		//플레이어를 현재 위치 벡터에서 Shift 벡터만큼 이동한다.
		float length = sqrtf(ob.velocity.x * ob.velocity.x + ob.velocity.z * ob.velocity.z);

		if (ob.state == IDLE || ob.state == ATTACK || ob.state == DEATH)
		{
			length = Vector3::Length(ob.velocity);
			float deceleration = (friction * 0.01f);
			if (deceleration > length) deceleration = length;
			ob.velocity = Vector3::Add(ob.velocity, Vector3::ScalarProduct(ob.velocity, -deceleration, false));
		}
		else {
			length = Vector3::Length(ob.velocity);
			float deceleration = ((friction - 19.0f) * 0.01f);
			if (deceleration > length) deceleration = length;
			ob.velocity = Vector3::Add(ob.velocity, Vector3::ScalarProduct(ob.velocity, -deceleration, false));
		}
}
bool RayTest(XMFLOAT3 pPos , XMFLOAT3 zPos) {
	XMFLOAT3 rayOrigin = XMFLOAT3(zPos.x, 0.1f, zPos.z);
	pPos.y = 0.1f;

	float pzDist = Vector3::Length(Vector3::Subtract(pPos, zPos));

	XMFLOAT3 rayDir = Vector3::Normalize(Vector3::Subtract(pPos, zPos));



	auto ro = XMLoadFloat3(&rayOrigin);
	auto rd = XMLoadFloat3(&rayDir);
	std::vector<POINTXZ> checkSector;

	for (int z = 0; z < OBJSECTOR_WIDTH; ++z) {
		for (int x = 0; x < OBJSECTOR_WIDTH; ++x) {
			float dist = FLT_MAX;
			if (tob[z][x].Intersects(ro, rd, dist)) {
				POINTXZ txz;
				txz.x = x;
				txz.z = z;
				if (dist < pzDist) { // 플레이어와 좀비 사이의 거리보다 작다면 넣는다.
					checkSector.emplace_back(txz);
				}
			}
		}
	}
	bool isColl = false;
	for (int i = 0; i < checkSector.size(); ++i) {
		int tx = checkSector[i].x;
		int tz = checkSector[i].z;

		for (auto &a : objSectorList[tz][tx]) {
			for (int j = 0; j < objvec[a].obbvec.size(); ++j) {
				float dist = FLT_MAX;
				if (objvec[a].obbvec[j].Intersects(ro, rd, dist)) {
					if (dist < pzDist) {
						isColl = true;
					}
					break;
				}
			}
			if (isColl)
				break;
		}
		if (isColl)
			break;

	}
	return isColl;
}

void CheckEightDir(vector<AstarInform>& openList , vector<AstarInform>& closeList,  int startx , int startz , int destx , int destz, int obx , int obz , AstarInform& pNode) {

	auto destPos = GetNodePos(destx, destz);
	auto CurPos = GetNodePos(obx, obz);

	if ((obx - 1) >= 0 && navibuffer[obz][obx - 1] != -1) { // 왼쪽 

		AstarInform New_Node;
		New_Node.x = obx - 1;
		New_Node.z = obz;

		New_Node.Parentx = obx;
		New_Node.Parentz = obz;

		auto NewPos = GetNodePos(obx - 1, obz);
		New_Node.G = Vector3::Length(Vector3::Subtract(CurPos, NewPos)) + pNode.G;
		New_Node.H = Vector3::Length(Vector3::Subtract(destPos, NewPos));
		New_Node.F = New_Node.G + New_Node.H;

		//오픈리스트에 넣기전에 있나 검사를 하고 있으면 갱신 
		bool isFind = false;
		for (int i = 0; i < openList.size(); ++i) {
			if (New_Node.x == openList[i].x && New_Node.z == openList[i].z) {
				if (New_Node.F < openList[i].F) {
					openList[i] = New_Node;
				}
				isFind = true;
			}
		}

		for (int i = 0; i < closeList.size(); ++i) {
			if (New_Node.x == closeList[i].x && New_Node.z == closeList[i].z) {
				isFind = true;
			}
		}

		if (isFind == false)
			openList.emplace_back(New_Node);
	}

	if ((obx + 1) < 257 && navibuffer[obz][obx + 1] != -1) { // 오른쪽 

		AstarInform New_Node;
		New_Node.x = obx + 1;
		New_Node.z = obz;

		New_Node.Parentx = obx;
		New_Node.Parentz = obz;

		auto NewPos = GetNodePos(obx + 1, obz);
		New_Node.G = Vector3::Length(Vector3::Subtract(CurPos, NewPos)) + pNode.G;
		New_Node.H = Vector3::Length(Vector3::Subtract(destPos, NewPos));
		New_Node.F = New_Node.G + New_Node.H;

		bool isFind = false;
		for (int i = 0; i < openList.size(); ++i) {
			if (New_Node.x == openList[i].x && New_Node.z == openList[i].z) {
				if (New_Node.F < openList[i].F) {
					openList[i] = New_Node;
				}
				isFind = true;
			}
		}
		for (int i = 0; i < closeList.size(); ++i) {
			if (New_Node.x == closeList[i].x && New_Node.z == closeList[i].z) {
				isFind = true;
			}
		}

		if (isFind == false)
			openList.emplace_back(New_Node);
	}

	if ((obz - 1) >= 0 && navibuffer[obz - 1][obx] != -1) {

		AstarInform New_Node;
		New_Node.x = obx;
		New_Node.z = obz - 1;

		New_Node.Parentx = obx;
		New_Node.Parentz = obz;

		auto NewPos = GetNodePos(obx, obz - 1);
		New_Node.G = Vector3::Length(Vector3::Subtract(CurPos, NewPos)) + pNode.G;
		New_Node.H = Vector3::Length(Vector3::Subtract(destPos, NewPos));
		New_Node.F = New_Node.G + New_Node.H;

		bool isFind = false;
		for (int i = 0; i < openList.size(); ++i) {
			if (New_Node.x == openList[i].x && New_Node.z == openList[i].z) {
				if (New_Node.F < openList[i].F) {
					openList[i] = New_Node;
				}
				isFind = true;
			}
		}
		for (int i = 0; i < closeList.size(); ++i) {
			if (New_Node.x == closeList[i].x && New_Node.z == closeList[i].z) {
				isFind = true;
			}
		}

		if (isFind == false)
			openList.emplace_back(New_Node);
	}

	if ((obz + 1) < 257 && navibuffer[obz + 1][obx] != -1) {

		AstarInform New_Node;
		New_Node.x = obx;
		New_Node.z = obz + 1;

		New_Node.Parentx = obx;
		New_Node.Parentz = obz;

		auto NewPos = GetNodePos(obx, obz + 1);
		New_Node.G = Vector3::Length(Vector3::Subtract(CurPos, NewPos)) + pNode.G;
		New_Node.H = Vector3::Length(Vector3::Subtract(destPos, NewPos));
		New_Node.F = New_Node.G + New_Node.H;

		bool isFind = false;
		for (int i = 0; i < openList.size(); ++i) {
			if (New_Node.x == openList[i].x && New_Node.z == openList[i].z) {
				if (New_Node.F < openList[i].F) {
					openList[i] = New_Node;
				}
				isFind = true;
			}
		}
		for (int i = 0; i < closeList.size(); ++i) {
			if (New_Node.x == closeList[i].x && New_Node.z == closeList[i].z) {
				isFind = true;
			}
		}

		if (isFind == false)
			openList.emplace_back(New_Node);
	}

	//if ((obx + 1) < 257 && (obz + 1) < 257 && navibuffer[obz + 1][obx + 1] != -1) {

	//	AstarInform New_Node;
	//	New_Node.x = obx + 1;
	//	New_Node.z = obz + 1;

	//	New_Node.Parentx = obx;
	//	New_Node.Parentz = obz;

	//	auto NewPos = GetNodePos(obx + 1, obz + 1);
	//	New_Node.G = Vector3::Length(Vector3::Subtract(CurPos, NewPos)) + pNode.G;
	//	New_Node.H = Vector3::Length(Vector3::Subtract(destPos, NewPos));
	//	New_Node.F = New_Node.G + New_Node.H;

	//	bool isFind = false;
	//	for (int i = 0; i < openList.size(); ++i) {
	//		if (New_Node.x == openList[i].x && New_Node.z == openList[i].z) {
	//			if (New_Node.F < openList[i].F) {
	//				openList[i] = New_Node;
	//			}
	//			isFind = true;
	//		}
	//	}
	//	if (isFind == false)
	//		openList.emplace_back(New_Node);
	//}

	//if ((obx - 1) >= 0 && (obz - 1) >= 0 && navibuffer[obz - 1][obx - 1] != -1) {

	//	AstarInform New_Node;
	//	New_Node.x = obx - 1;
	//	New_Node.z = obz - 1;

	//	New_Node.Parentx = obx;
	//	New_Node.Parentz = obz;

	//	auto NewPos = GetNodePos(obx - 1, obz - 1);
	//	New_Node.G = Vector3::Length(Vector3::Subtract(CurPos, NewPos)) + pNode.G;
	//	New_Node.H = Vector3::Length(Vector3::Subtract(destPos, NewPos));
	//	New_Node.F = New_Node.G + New_Node.H;

	//	bool isFind = false;
	//	for (int i = 0; i < openList.size(); ++i) {
	//		if (New_Node.x == openList[i].x && New_Node.z == openList[i].z) {
	//			if (New_Node.F < openList[i].F) {
	//				openList[i] = New_Node;
	//			}
	//			isFind = true;
	//		}
	//	}
		//for (int i = 0; i < closeList.size(); ++i) {
	//	if (New_Node.x == closeList[i].x && New_Node.z == closeList[i].z) {
	//		isFind = true;
	//	}
	//}
	//	if (isFind == false)
	//		openList.emplace_back(New_Node);
	//}

	//if ((obx + 1) < 257 && (obz - 1) >= 0 && navibuffer[obz + 1][obx - 1] != -1) {

	//	AstarInform New_Node;
	//	New_Node.x = obx + 1;
	//	New_Node.z = obz - 1;

	//	New_Node.Parentx = obx;
	//	New_Node.Parentz = obz;

	//	auto NewPos = GetNodePos(obx + 1, obz - 1);
	//	New_Node.G = Vector3::Length(Vector3::Subtract(CurPos, NewPos)) + pNode.G;
	//	New_Node.H = Vector3::Length(Vector3::Subtract(destPos, NewPos));
	//	New_Node.F = New_Node.G + New_Node.H;

	//	bool isFind = false;
	//	for (int i = 0; i < openList.size(); ++i) {
	//		if (New_Node.x == openList[i].x && New_Node.z == openList[i].z) {
	//			if (New_Node.F < openList[i].F) {
	//				openList[i] = New_Node;
	//			}
	//			isFind = true;
	//		}
	//	}
	//for (int i = 0; i < closeList.size(); ++i) {
	//	if (New_Node.x == closeList[i].x && New_Node.z == closeList[i].z) {
	//		isFind = true;
	//	}
	//}

	//	if (isFind == false)
	//		openList.emplace_back(New_Node);
	//}


	//if ((obx - 1) >= 0 && (obz + 1) < 257 && navibuffer[obz - 1][obx + 1] != -1) {

	//	AstarInform New_Node;
	//	New_Node.x = obx - 1;
	//	New_Node.z = obz + 1;

	//	New_Node.Parentx = obx;
	//	New_Node.Parentz = obz;

	//	auto NewPos = GetNodePos(obx - 1, obz + 1);
	//	New_Node.G = Vector3::Length(Vector3::Subtract(CurPos, NewPos)) + pNode.G;
	//	New_Node.H = Vector3::Length(Vector3::Subtract(destPos, NewPos));
	//	New_Node.F = New_Node.G + New_Node.H;

	//	bool isFind = false;
	//	for (int i = 0; i < openList.size(); ++i) {
	//		if (New_Node.x == openList[i].x && New_Node.z == openList[i].z) {
	//			if (New_Node.F < openList[i].F) {
	//				openList[i] = New_Node;
	//			}
	//			isFind = true;
	//		}
	//	}
	//for (int i = 0; i < closeList.size(); ++i) {
	//	if (New_Node.x == closeList[i].x && New_Node.z == closeList[i].z) {
	//		isFind = true;
	//	}
	//}
	//	if (isFind == false)
	//		openList.emplace_back(New_Node);
	//}
}
vector<AstarInform> Astar(copyClient& ob, copyClient destob) {
	// 에이스타 알고리즘 
	//플레이어와 자기 타일을 비교해서 look을 설정하자
	vector<AstarInform> openList; // 최단경로 상태값 갱신 타일들의 번호를 넣어줌 
	vector<AstarInform> closeList; // 처리 완료된 노드를 담아넣는다.

	int destx = destob.tileX;
	int destz = destob.tileZ;

	int startx = ob.tileX;
	int startz = ob.tileZ;
	//먼저 시작노드를 닫힌 목록에 넣어줌 
	AstarInform Node;
	Node.x = startx;
	Node.z = startz;


	closeList.emplace_back(Node);

	//ntvec tileNumber넣으면 정보를 뱉는 벡터 

	//연결된 노드를 열린목록에 넣는다.
	//8방향의 노드를 루프를 돌려서 넣자
	CheckEightDir(openList, closeList, startx , startz , destx, destz , startx, startz, Node);
	sort(openList.begin(), openList.end(), [](const AstarInform& f1 , const AstarInform& f2) {
		return f1.F < f2.F;
	});

	//openList에서 F가 가장 작은 노드를 뺀다 
	while (1) {
		if (openList.size() == 0) {
			break;
		}

		AstarInform sNode = openList[0];
		//closelist에 추가한다 .
		if (closeList.size() > 300)
			break;

		closeList.emplace_back(sNode);
		openList.erase(openList.begin());
		if (sNode.x == destx && sNode.z == destz) {
			break;
		}
		CheckEightDir(openList, closeList, startx, startz, destx, destz, sNode.x, sNode.z, sNode);
		sort(openList.begin(), openList.end(), [](const AstarInform& f1, const AstarInform& f2) {
			return f1.F < f2.F;
			});

	}
	//최소경로

	AstarInform endNode = closeList[closeList.size() - 1];

	std::vector<AstarInform> finalList;
	

	while (1) {

		finalList.emplace_back(endNode);
		if (endNode.Parentx == -1) {
			break;
		}
		for (int i = 0; i < closeList.size(); ++i) {
			if (closeList[i].x == endNode.Parentx && closeList[i].z == endNode.Parentz) {
				endNode = closeList[i];
				break;
			}
		}
	}

	return finalList;
}


void GetTile(copyClient& client) {
			auto& siObj = client;
			XMFLOAT3 pos = siObj.pos;
			XMFLOAT3 lpos = Vector3::Add(pos , XMFLOAT3(257.0f , 10.0f, 257.0f));
			//위치로 부터 속한 타일을 알아야함
			XMFLOAT3 dir = XMFLOAT3(0.0f, -1.0f, 0.0f);
			float finaldist = FLT_MAX;
			auto vlpos = XMLoadFloat3(&lpos);
			auto vdir = XMLoadFloat3(&dir);
			int tx = int(lpos.x) / 2;
			int tz = int(lpos.z) / 2;
			bool isCrash = false;
			

			int ftx = tx + 5;
			int ftz = tz + 5;

			if (ftx > 257) {
				ftx = 257;
			}
			if (ftz > 257) {
				ftz = 257;
			}
			bool isCheck = false;
			int maxtileX = siObj.tileX;
			int maxtileZ = siObj.tileZ;
			float sdist = FLT_MAX;

			for (int z = tz - 5; z < ftz; ++z) {
				for (int x = tx - 5; x < ftx; ++x) {
					float dist = FLT_MAX;
					int tileNumber = navibuffer[z][x]; //네비 메쉬 타일의 인덱스가 들어있음 
					
					if (tileNumber != -1) {

						
						float dist = abs(tx - x) + abs(tz - z);
						if (dist < sdist) {
							sdist = dist;
							maxtileX = x;
							maxtileZ = z;
							
						}
					}
				}
			}

			siObj.tileX = maxtileX;
			siObj.tileZ = maxtileZ;
}
void GetHeight(copyClient& client) {

		auto& siObj = client;
		XMFLOAT3 pos = siObj.pos;
		XMFLOAT3 lpos = Vector3::Add(pos, XMFLOAT3(257.0f, 10.0f, 257.0f));
		//위치로 부터 속한 타일을 알아야함
		XMFLOAT3 dir = XMFLOAT3(0.0f, -1.0f, 0.0f);
		float finaldist = FLT_MAX;
		auto vlpos = XMLoadFloat3(&lpos);
		auto vdir = XMLoadFloat3(&dir);
		int tx = int(lpos.x) / 2;
		int tz = int(lpos.z) / 2;
		bool isCrash = false;
		

		int ftx = tx + 5;
		int ftz = tz + 5;

		if (ftx > 257) {
			ftx = 257;
		}
		if (ftz > 257) {
			ftz = 257;
		}

		for (int z = tz; z < ftz; ++z) {
			for (int x = tx; x < ftx; ++x) {
				float dist = FLT_MAX;
				
				if ((x + 1) != (ftx - 1) && (z + 1) != (ftz - 1)) {
					int idx = x + (257 * z); 
					

						auto lv1 = XMLoadFloat3(&tVertices[idx]);
						auto lv2 = XMLoadFloat3(&tVertices[idx + 257]);
						auto lv3 = XMLoadFloat3(&tVertices[idx + 1]);

						if (TriangleTests::Intersects(vlpos, vdir, lv1, lv2, lv3, dist)) {
							if (dist < finaldist) {
								finaldist = dist;
							}
							isCrash = true;
						}
						float dist = FLT_MAX;
						lv1 = XMLoadFloat3(&tVertices[idx + 257]);
						lv2 = XMLoadFloat3(&tVertices[idx + 257 + 1]);
						lv3 = XMLoadFloat3(&tVertices[idx + 1]);

						if (TriangleTests::Intersects(vlpos, vdir, lv1, lv2, lv3, dist)) {
							if (dist < finaldist) {
								finaldist = dist;
							}
							isCrash = true;
						}

					if (isCrash) break;
				}
				if (isCrash) break;

			}

		}
		if (isCrash) {
			auto finalPos = Vector3::Add(Vector3::Add(lpos, Vector3::ScalarProduct(dir, finaldist)), XMFLOAT3(-257.0f, 0.0f, -257.0f));
			client.pos = finalPos;
		}
		else {
		}

}





std::vector<int> GetPlayerNearListZombie(int stx ,int stz) {
	vector<int> tzlist;

	s_cl[stz][stx].lock();
	for (auto& z : SectorList[stz][stx]) {
		tzlist.emplace_back(z);
	}
	s_cl[stz][stx].unlock();

	if ((stz + 1) < SECTOR_WIDTH) {
		s_cl[stz + 1][stx].lock();
		for (auto& z : SectorList[stz + 1][stx]) {
			tzlist.emplace_back(z);
		}
		s_cl[stz + 1][stx].unlock();
	}

	if ((stz - 1) > 0) {
		s_cl[stz - 1][stx].lock();
		for (auto& z : SectorList[stz - 1][stx]) {
			tzlist.emplace_back(z);
		}
		s_cl[stz - 1][stx].unlock();
	}

	if ((stx + 1) < SECTOR_WIDTH) {
		s_cl[stz][stx + 1].lock();
		for (auto& z : SectorList[stz][stx + 1]) {
			tzlist.emplace_back(z);
		}
		s_cl[stz][stx + 1].unlock();
	}

	if ((stx - 1) > 0) {
		s_cl[stz][stx - 1].lock();
		for (auto& z : SectorList[stz][stx - 1]) {
			tzlist.emplace_back(z);
		}
		s_cl[stz][stx - 1].unlock();
	}

	if ((stx - 1) > 0 && (stz - 1) > 0) {
		s_cl[stz - 1][stx - 1].lock();
		for (auto& z : SectorList[stz - 1][stx - 1]) {
			tzlist.emplace_back(z);
		}
		s_cl[stz - 1][stx - 1].unlock();
	}

	if ((stx + 1) < SECTOR_WIDTH && (stz - 1) < SECTOR_WIDTH) {
		s_cl[stz + 1][stx + 1].lock();
		for (auto& z : SectorList[stz + 1][stx + 1]) {
			tzlist.emplace_back(z);
		}
		s_cl[stz + 1][stx + 1].unlock();
	}
	return tzlist;

}


void do_move(int user_id)
{
	CLIENT& user = g_clients[user_id];
	copyClient ucy;

	XMFLOAT3 userPos;
	g_clients[user_id].m_cl.lock();
	ucy.pos = g_clients[user_id].pos;
	ucy.tileX = g_clients[user_id].tileX;
	ucy.tileZ = g_clients[user_id].tileZ;

	g_clients[user_id].sectorx = int((g_clients[user_id].pos.x + 257.0f) / SECTOR_SIZE);//플레이어 sector 설정 
	g_clients[user_id].sectorz = int((g_clients[user_id].pos.z + 257.0f) / SECTOR_SIZE);
	userPos = g_clients[user_id].pos;
	g_clients[user_id].m_cl.unlock();
	
	GetTile(ucy);

	
	for (auto& cl : g_clients) {
		cl.m_cl.lock();
		if (cl.m_status == ST_ACTIVE) {
			if (cl.m_id == user_id) {
				cl.m_cl.unlock();
				continue;
			}
			send_move_packet(cl.m_id, user_id);
		}
		cl.m_cl.unlock();
	}

	user.m_cl.lock();
	user.sectorx = int((g_clients[user_id].pos.x + 257.0f) / SECTOR_SIZE);//플레이어 sector 설정 
	user.sectorz = int((g_clients[user_id].pos.z + 257.0f) / SECTOR_SIZE);
	int usx = user.sectorx;
	int usz = user.sectorz;
	user.tileX = ucy.tileX;
	user.tileZ = ucy.tileZ;

	user.m_cl.unlock();

	//좀비 상태 변경하기 
	std::vector<int> zList = GetPlayerNearListZombie(usx, usz);
	
	for (int i = 0; i < zList.size(); ++i) {
		
		int cid = zList[i];
		copyClient tcl;
	
		g_clients[cid].m_cl.lock();
		tcl.pos = g_clients[cid].pos;
		tcl.look = g_clients[cid].look;
		tcl.state = g_clients[cid].state;
		g_clients[cid].m_cl.unlock();

		if (Vector3::Length(Vector3::Subtract(tcl.pos , userPos)) < ZOMBIECHASEDIST && tcl.state < CHASE) {

			tcl.state = CHASE;
			XMFLOAT3 tpos = Vector3::Subtract(user.pos, tcl.pos);
			tpos.y = 0.0f;
			tcl.look = Vector3::Normalize(tpos);
			tcl.target = user_id;
			tcl.aniTrack = tcl.aniTrack = rand() % 4 + ZombieRun;
			g_clients[cid].m_cl.lock();
			g_clients[cid].pos = tcl.pos;
			g_clients[cid].look = tcl.look;
			g_clients[cid].state = tcl.state;
			g_clients[cid].target = tcl.target;
			if (g_clients[cid].bufferidx == (FINALBOSS - 3)) {
				g_clients[cid].aniTrackUp = BossRun;
			}
			else {
				g_clients[cid].aniTrackUp = tcl.aniTrack;
			}
			g_clients[cid].m_cl.unlock();
			add_timer(cid, OP_NPC_CHASE, 100);
		}
	
	
		if (tcl.state == IDLE) {
			int dir = (rand() % 12);
			tcl.look.x = cos((dir * 30.0f) * (PI / 180.f));
			tcl.look.z = sin((dir * 30.0f) * (PI / 180.f));
	
			XMFLOAT3 pos = tcl.pos;
			XMFLOAT3 look = tcl.look;
			XMFLOAT3 up = XMFLOAT3(0.0f, 1.0f, 0.0f);
			XMFLOAT3 right = Vector3::CrossProduct(up, look);
			XMFLOAT4X4 world = {1.0f , 0 ,0 , 0, 
								0 ,   1.0f , 0 , 0
								,0 ,   0 ,    1.0f , 0
								,0 ,   0 ,     0 ,  1.0f};
			world._11 = right.x;
			world._12 = right.y;
			world._13 = right.z;
	
			world._21 = up.x;
			world._22 = up.y;
			world._23 = up.z;
	
			world._31 = look.x;
			world._32 = look.y;
			world._33 = look.z;
	
			world._41 = pos.x;
			world._42 = pos.y;
			world._43 = pos.z;
	
			XMStoreFloat4(&g_clients[cid].obbBox.Orientation, XMQuaternionRotationMatrix(XMLoadFloat4x4(&world)));
	
			tcl.state = WANDER;
			g_clients[cid].m_cl.lock();
			g_clients[cid].pos = tcl.pos;
			g_clients[cid].look = tcl.look;
			g_clients[cid].state = tcl.state;
			g_clients[cid].m_cl.unlock();
			add_timer(cid, OP_NPC_WANDER, (rand() % 8000) + 3000);
		}
	
		
	}

}

void enter_lobby(int user_id)
{
	send_loginok_packet(user_id);

	g_clients[user_id].m_cl.lock();
	for (int i = 0; i < 10; ++i) {
		if (user_id == i) continue;
		g_clients[i].m_cl.lock();
		if (g_clients[i].m_status == ST_ACTIVE) {
			if (user_id != i) {
				send_enter_lobby_packet(i, user_id);
				send_enter_lobby_packet(user_id, i);
			}
		}
		g_clients[i].m_cl.unlock();
	}
	g_clients[user_id].m_status = ST_ACTIVE;
	g_clients[user_id].scene = 0;
	g_clients[user_id].m_cl.unlock();
	//원래는 여기서 unlock해줘야한다.

}

void enter_game(int user_id)
{
	g_clients[user_id].m_cl.lock();
	send_ingame_packet(user_id);
	g_clients[user_id].scene = 1;
	g_clients[user_id].m_cl.unlock();
	//원래는 여기서 unlock해줘야한다.

}

void change_job(int user_id)
{
	bool allReady = true;
	if (g_clients[user_id].isReady == 2)
	{
		// 게임 시작 조건
		for (int i = 1; i < g_cur_user_count; ++i)
		{
			if (g_clients[i].isReady != 1) allReady = false;
		}
		if (allReady == true)
		{
			for (int i = 0; i < g_cur_user_count; ++i)
			{
				enter_game(i);
			}
		}
		else
		{
			g_clients[user_id].m_cl.lock();
			g_clients[user_id].isReady = 0;
			g_clients[user_id].m_cl.unlock();
		}
	}
	else {
		allReady = false;
	}
	if(allReady == false)
	{
		for (int i = 0; i < 10; ++i) {
			if (user_id == i) continue;
			g_clients[i].m_cl.lock();
			if (g_clients[i].m_status == ST_ACTIVE) {
				if (user_id != i) {
					send_changejob_packet(i, user_id);
				}
			}
			g_clients[i].m_cl.unlock();
		}
	}
}

void change_weapon(int user_id)
{
	for (int i = 0; i < ZOMBIE_START_NUMBER; ++i) {
		if (user_id == i) continue;
		g_clients[i].m_cl.lock();
		if (g_clients[i].m_status == ST_ACTIVE) {
			if (user_id != i) {
				send_changeweapon_packet(i, user_id);
			}
		}
		g_clients[i].m_cl.unlock();
	}
}

void game_start() {

}
void process_packet(int user_id, char* buf)
{
	switch (buf[1]) {
	case C2S_LOGIN: {
		cs_packet_login* packet = reinterpret_cast<cs_packet_login*>(buf);
		enter_lobby(user_id);
		g_ucount.lock();
		++g_cur_user_count;
		g_ucount.unlock();

	}
					break;
	case C2S_CHANGE_JOB: {
		cs_packet_lobby* packet = reinterpret_cast<cs_packet_lobby*>(buf);
		g_clients[user_id].m_cl.lock();
		g_clients[user_id].cJop = packet->cJob;
		g_clients[user_id].isReady = packet->isReady;
		g_clients[user_id].m_cl.unlock();
		change_job(user_id);
	}
						 break;

	case C2S_WEAPON: {
		cs_packet_weapon* packet = reinterpret_cast<cs_packet_weapon*>(buf);
		g_clients[user_id].m_cl.lock();
		g_clients[user_id].weapon = packet->weaponNum;
		g_clients[user_id].m_cl.unlock();
		change_weapon(user_id);
	}
					 break;

	case C2S_LOADMAP: {
		cs_packet_loadmap* packet = reinterpret_cast<cs_packet_loadmap*>(buf);
		//여기서 받을 맵을 써야함 , 클라이언트들의 맵 num을 바꿔줘야함 
		packet->mapNum;
		CLIENT& user = g_clients[user_id];
		bool temp = user.mapNum;
		CAS(&temp, user.mapNum, packet->mapNum);
		CAS(&user.gameStart, false, true);


		user.m_cl.lock();
		user.sectorx = int((g_clients[user_id].pos.x + 257.0f) / SECTOR_SIZE);//플레이어 sector 설정 
		user.sectorz = int((g_clients[user_id].pos.z + 257.0f) / SECTOR_SIZE);
		int usx = user.sectorx;
		int usz = user.sectorz;
		user.m_cl.unlock();
	}
					  break;

	case C2S_MOVE: {
			cs_packet_move* packet = reinterpret_cast<cs_packet_move*>(buf);
			g_clients[user_id].m_cl.lock();
			CLIENT& user = g_clients[user_id];
			int potal = packet->potal;
			user.aniTrackUp = packet->aniTrackUp;
			user.aniTrackLo = packet->aniTrackLo;
			user.dir = packet->direction;
			user.look = packet->look;
			user.pos = packet->pos;
			user.velocity = packet->velocity;
			user.potal = packet->potal;
			g_clients[user_id].m_cl.unlock();

			im_cl.lock();
			bool imm = isMapMove; // 중간보스 잡았는지 여부 
			im_cl.unlock();
			bool caps = false;

			if (imm == true) {
				for (int i = 0; i < 10; ++i) {
					g_clients[i].m_cl.lock();
					if (g_clients[i].m_status == ST_ACTIVE) {
						if (potal == g_clients[i].potal && g_clients[i].potal != NOPOTAL)
						{
							caps = true;
						}
						else {
							caps = false;
							g_clients[i].m_cl.unlock();
							break;
						}

					}
					g_clients[i].m_cl.unlock();
				}
			}
			//맵로딩 검사 
			int mapNumber = -1;
			if (potal > NOPOTAL && potal < 110)
			{
				caps = true;
				mapNumber -= 101;
			}

			if (caps == true) {

				if (potal)

					if (potal == 1)
						potal = -1;
					else if (potal == 2)
						potal = 1;
					else if (potal == 3)
						potal = 3;
					else if (potal == 4)
						potal = -3;
				im_cl.lock();
				curMap = curMap + potal;
				im_cl.unlock();


				for (int i = 0; i < ZOMBIE_START_NUMBER; ++i) {
					g_clients[i].m_cl.lock();
					if (g_clients[i].m_status == ST_ACTIVE) {
						send_loadMap_packet(i, curMap); // 현제 맵 번호에 포탈 넘버를 더해주던가 해야함 
					}
					g_clients[i].m_cl.unlock();
				}

				all_l.lock();
				isMapMove = false;
				LoadMap(mapinform[curMap].serverdata, mapinform[curMap].nodedata , curMap);

				ConvertZombie();
				all_l.unlock();
			}
			//맵로딩 

			do_move(user_id);
	}
				   break;
	case C2S_ATTACK: {
		cs_packet_attack* packet = reinterpret_cast<cs_packet_attack*>(buf);
		int id = packet->id;
		float damage = packet->damage;
		XMFLOAT3 pos = packet->pos;

		int state;
		int bIdx;
		XMFLOAT3 upos;
		g_clients[user_id].m_cl.lock();
		upos = g_clients[user_id].pos;
		g_clients[user_id].m_cl.unlock();
	

		if (id >= ZOMBIE_START_NUMBER) {
			g_clients[id].m_cl.lock();
			g_clients[id].hp -= damage;
			state = g_clients[id].state;
			bIdx = g_clients[id].bufferidx;

			if (g_clients[id].hp < 0) {
				g_clients[id].state = DEATH;
				if (g_clients[id].bufferidx == (FINALBOSS - 3)) {
					g_clients[id].aniTrackUp = BossDeath;
				}
				else {
					g_clients[id].aniTrackUp = (rand() % 2) + ZombieDeath1;
				}
				g_clients[id].velocity = XMFLOAT3(0, 0, 0);
				state = DEATH;
			}
			else {
				if (g_clients[id].state != CHASE) {
					g_clients[id].state = CHASE;
					XMFLOAT3 tpos = Vector3::Subtract(upos, g_clients[id].pos);
					tpos.y = 0.0f;
					g_clients[id].look = Vector3::Normalize(tpos);
					g_clients[id].target = user_id;
					if (g_clients[id].bufferidx == (FINALBOSS - 3)) {
						g_clients[id].aniTrackUp = BossRun;
					}
					else {
						g_clients[id].aniTrackUp = (rand() % 4) + ZombieRun;
					}
					add_timer(id, OP_NPC_CHASE, 0);
				}
			}
			g_clients[id].m_cl.unlock();


			if (state == DEATH) {
				if (bIdx == (MIDDLEBOSS - 3) || bIdx == (FINALBOSS - 3)) {
					//중간보스를 잡았다면 
					for (int i = ZOMBIE_START_NUMBER; i < ZOMBIE_START_NUMBER + total_zombie; ++i) {
						//모든 좀비들 
						g_clients[i].m_cl.lock();
						if (g_clients[i].state != DEATH) {
							g_clients[i].state = DEATH;
							g_clients[i].hp = -1.0f;
							if (g_clients[i].bufferidx == (FINALBOSS - 3)) {
								g_clients[i].aniTrackUp = BossDeath;
							}
							else {
								g_clients[i].aniTrackUp = rand() % 2 + ZombieDeath1;
							}
							g_clients[i].velocity = XMFLOAT3(0, 0, 0);
							state = DEATH;
							//죽은 상태가 아니면 바꿔줌 
						}
						else {
							g_clients[i].m_cl.unlock();
							continue;
							//죽었다면 패킷 보내줄 필요없음 
						}
						g_clients[i].m_cl.unlock();

						for (int j = 0; j < 10; ++j) {
							g_clients[j].m_cl.lock();
							if (g_clients[j].m_status == ST_ACTIVE) {
								send_zombie_move_packet(j, i);
							}
							g_clients[j].m_cl.unlock();
						}
						//죽은 상태 아닌것들이 죽었다고 보내줌 

						for (int j = 0; j < 10; ++j) {
							g_clients[j].m_cl.lock();
							if (g_clients[j].m_status == ST_ACTIVE) {
								send_zombie_move_packet(j, id);
							}
							g_clients[j].m_cl.unlock();
						}

					}
					im_cl.lock();
					isMapMove = true;
					im_cl.unlock();

				}
				else {
					//중간보스가 아니라면 그냥 브로드 캐스트 
					for (int i = 0; i < ZOMBIE_START_NUMBER; ++i) {
						g_clients[i].m_cl.lock();
						if (g_clients[i].m_status == ST_ACTIVE) {
							send_zombie_move_packet(i, id);
						}
						g_clients[i].m_cl.unlock();
					}
				}
			}
			else {
				for (int i = 0; i < 10; ++i) {
					g_clients[i].m_cl.lock();
					if (g_clients[i].m_status == ST_ACTIVE) {
						send_zombie_move_packet(i, id);
						if (user_id != i)
							send_effect_packet(i, EffectTypeBlood, pos);
					}
					g_clients[i].m_cl.unlock();
				}
			}
		}
		else
		{
			if (id == 0) {
				for (int i = 0; i < 10; ++i) {
					g_clients[i].m_cl.lock();
					if (g_clients[i].m_status == ST_ACTIVE) {
						if (user_id != i)
							send_effect_packet(i, EffectTypeDust, pos);
					}
					g_clients[i].m_cl.unlock();
				}
			}
			else
			{
				for (int i = 0; i < 10; ++i) {
					g_clients[i].m_cl.lock();
					if (g_clients[i].m_status == ST_ACTIVE) {
						if (user_id != i)
							send_effect_packet(i, EffectTypeBlood, pos);
					}
					g_clients[i].m_cl.unlock();
				}
			}
		}
		

	}
			 break;
	case C2S_ZOMBIEATTACK:{
		cs_packet_zombieattack* packet = reinterpret_cast<cs_packet_zombieattack*>(buf);
		float damage = packet->damage;
		g_clients[user_id].m_cl.lock();
		g_clients[user_id].hp -= damage;
		g_clients[user_id].m_cl.unlock();
		if (g_clients[user_id].hp >= 0)
			send_move_packet(user_id, user_id);
		else
		{
			for (int i = 0; i < 10; ++i)
			{
				g_clients[i].m_cl.lock();
				if (g_clients[i].m_status == ST_ACTIVE) {
					send_endgame_pacekt(i);
				}
				g_clients[i].m_cl.unlock();
			}
		}
	}
					break;
	case C2S_HILL: {
		cs_packet_hill* packet = reinterpret_cast<cs_packet_hill*>(buf);
		short hp = packet->hp;
		g_clients[user_id].m_cl.lock();
		g_clients[user_id].hp = hp;
		g_clients[user_id].m_cl.unlock();
	}
				   break;
	default:
		std::cout << "Unknown Packet Type Error!" << std::endl;
		DebugBreak();
		exit(-1);
	}
}

void initialize_clients() {
	for (int i = 0; i < MAX_USER; ++i) {
		g_clients[i].m_id = i;
		g_clients[i].m_status = ST_FREE;
		g_clients[i].pos = XMFLOAT3(0.0f + i * 10.0f, 0.0f, 0.0f);
		g_clients[i].sectorx = (g_clients[i].pos.x + 257.0f) / SECTOR_SIZE;
		g_clients[i].sectorz = (g_clients[i].pos.z + 257.0f) / SECTOR_SIZE;
		g_clients[i].look = XMFLOAT3(0.0f, 0.0f, 1.0f);
		g_clients[i].hp = 100;
		g_clients[i].potal = NOPOTAL;
		g_clients[i].cJop = RIFLE;
	}
}

void disconnect(int user_id)
{
	g_clients[user_id].m_cl.lock();
	g_clients[user_id].m_status = ST_ALLOC;
	g_clients[user_id].cJop = 0;
	g_clients[user_id].scene = 0;
	g_clients[user_id].isReady = 0;
	g_cur_user_count--;
	send_leave_packet(user_id, user_id);
	closesocket(g_clients[user_id].socket);
	for (auto& cl : g_clients) {
		if (cl.m_id == user_id)
			continue;
		cl.m_cl.lock();
		if (cl.m_status == ST_ACTIVE)
			send_leave_packet(cl.m_id, user_id);
		cl.m_cl.unlock();
	}
	g_clients[user_id].m_status = ST_FREE;
	g_clients[user_id].m_cl.unlock();
}

void recv_packet_construct(int user_id, int io_byte)
{
	EXOVER& recvOver = g_clients[user_id].recv_over;
	CLIENT& cl = g_clients[user_id];

	int rest_byte = io_byte;
	char* p = recvOver.io_buf;
	int packetSize = 0;
	if (cl.prev_size != 0) packetSize = cl.packet_buf[0];

	while (rest_byte > 0) {
		if (packetSize == 0) packetSize = p[0];

		if (rest_byte >= packetSize - cl.prev_size) {	// 패킷 완성 가능
			memcpy(cl.packet_buf + cl.prev_size, p, packetSize - cl.prev_size);
			process_packet(user_id, cl.packet_buf);
			p += packetSize - cl.prev_size;
			rest_byte -= packetSize - cl.prev_size;
			cl.prev_size = 0;
			packetSize = 0;
		}
		else {	// 패킷 완성 불가능
			memcpy(cl.packet_buf + cl.prev_size, p, rest_byte);
			cl.prev_size += rest_byte;
			rest_byte = 0;
			p += rest_byte;
		}
	}
}

void worker_thread()
{
	while (1) {
		DWORD io_byte;
		ULONG_PTR key;
		WSAOVERLAPPED* over;
		GetQueuedCompletionStatus(g_iocp, &io_byte, &key, &over, INFINITE);
		// 블로킹이다. 클라에게 정보를 보내기 전에는 계속 멈춰있다. 멈춰있기 때문에 accpet를 받을 수 없다.
		// 만약 accept에서 멈춰있다면 이곳에서는 recv할 수 없다. 서로서로 상대방의 실행에 방해를 놓는다. 
		// 따라서 accept도 AcceptEx/GetQueuedCompletionStatus함수를 사용하여 받도록 하자.
		// 플레이어의 접속 종료를 알 수 있는 2가지 방법 : io_byte에서 0바이트를 받았거나(정상종료), 디버그 중지로 강제종료 했을 때

		EXOVER* exover = reinterpret_cast<EXOVER*>(over);
		int user_id = static_cast<int>(key);
		CLIENT& clinet = g_clients[user_id];


		switch (exover->op) {
		case OP_RECV:
			if (io_byte == 0) {	// 접속 종료
				disconnect(user_id);
			}
			else {
				recv_packet_construct(user_id, io_byte);
				ZeroMemory(&(clinet.recv_over.overlapped), sizeof(clinet.recv_over.overlapped));
				DWORD flags = 0;
				WSARecv(clinet.socket, &(clinet.recv_over.wsabuf), 1, NULL, &flags, &(clinet.recv_over.overlapped), NULL);
			}
			break;
		case OP_SEND:
			if (io_byte == 0) {	// 접속 종료
				disconnect(user_id);
			}
			delete exover;
			break;
		case OP_ACCEPT: {
			int user_id = -1;
			for (int i = 0; i < ZOMBIE_START_NUMBER; ++i) {
				lock_guard<mutex>  gl{ g_clients[i].m_cl };
				if (ST_FREE == g_clients[i].m_status) {
					g_clients[i].m_status = ST_ALLOC;
					user_id = i;
					break;
				}

			}
			SOCKET client_socket = exover->c_socket;
			if (-1 == user_id)
				closesocket(exover->c_socket);
			else {
				CreateIoCompletionPort(reinterpret_cast<HANDLE>(client_socket), g_iocp, user_id, 0);
				CLIENT& user = g_clients[user_id];
				user.prev_size = 0;
				user.recv_over.op = OP_RECV;
				ZeroMemory(&(user.recv_over.overlapped), sizeof(user.recv_over.overlapped));
				user.recv_over.wsabuf.buf = user.recv_over.io_buf;
				user.recv_over.wsabuf.len = MAX_BUF_SIZE;
				user.socket = client_socket;
				user.pos = XMFLOAT3(0.0f, 0.0f, 0.0f);
				DWORD flags = 0;
				WSARecv(client_socket, &(user.recv_over.wsabuf), 1, NULL, &flags, &(user.recv_over.overlapped), NULL);
			}
			client_socket = WSASocketW(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
			exover->c_socket = client_socket;
			ZeroMemory(&(exover->overlapped), sizeof(exover->overlapped));
			AcceptEx(listen_socket, client_socket, exover->io_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &(exover->overlapped));
			// 굳이  listen_socket을 넘겨줄 필요 없다. 
		}
			break;

		case OP_NPC_WANDER:
		{
			int userState;
			g_clients[user_id].m_cl.lock();
			userState = g_clients[user_id].state;
			g_clients[user_id].m_cl.unlock();
			if (userState < CHASE) {

				g_clients[user_id].m_cl.lock();
				if (g_clients[user_id].bufferidx == (FINALBOSS - 3)) {
					g_clients[user_id].aniTrackUp = BossWalk;
				}
				else {
					g_clients[user_id].aniTrackUp = (rand() % 3) + ZombieWalk1;
				}
				g_clients[user_id].state = WANDER;
				g_clients[user_id].m_cl.unlock();
				for (int i = 0; i < 10; ++i) {
					g_clients[i].m_cl.lock();
					if (g_clients[i].m_status == ST_ACTIVE && g_clients[i].scene == 1) {
						send_zombie_move_packet(i, user_id);
					}
					g_clients[i].m_cl.unlock();
				}
				add_timer(user_id, OP_NPC_MOVE, 100);
			}
		}
		delete exover;
		break;
		case OP_NPC_MOVE: {
			int userState;
			g_clients[user_id].m_cl.lock();
			userState = g_clients[user_id].state;
			g_clients[user_id].m_cl.unlock();
			
			if (userState < CHASE) {

				copyClient tcl;
				g_clients[user_id].m_cl.lock();
				tcl.id = user_id;
				XMFLOAT3 tpos = g_clients[user_id].pos;
				g_clients[user_id].stime += 100;
				tcl.look = g_clients[user_id].look;
				tcl.pos = g_clients[user_id].pos;
				tcl.state = g_clients[user_id].state;
				tcl.velocity = g_clients[user_id].velocity;
				tcl.stime = g_clients[user_id].stime;
				tcl.obbBox = g_clients[user_id].obbBox;
				tcl.tileX = g_clients[user_id].tileX;
				tcl.tileZ = g_clients[user_id].tileZ;
				g_clients[user_id].m_cl.unlock();
				for (int i = 0; i < 10; ++i) {
					MoveZombie(tcl, ZOMBIESPEED, false , tcl.look);
				}
			

				//섹터 계산 
				XMFLOAT3 cpos = tcl.pos;

				int tsx = int((tpos.x + 257.0f) / SECTOR_SIZE);
				int tsz = int((tpos.z + 257.0f) / SECTOR_SIZE);

				int csx = int((cpos.x + 257.0f) / SECTOR_SIZE);
				int csz = int((cpos.z + 257.0f) / SECTOR_SIZE);

				if (tsx != csx || csz != tsz) {
					s_cl[tsx][tsz].lock();
					SectorList[tsz][tsx].erase(user_id);
					s_cl[tsx][tsz].unlock();

					s_cl[csx][csz].lock();
					SectorList[csz][csx].insert(user_id);
					s_cl[csx][csz].unlock();
				}


				GetTile(tcl);
				GetHeight(tcl);
				g_clients[user_id].m_cl.lock();
				g_clients[user_id].look = tcl.look;
				g_clients[user_id].pos = tcl.pos;
				g_clients[user_id].state = tcl.state;
				g_clients[user_id].velocity = tcl.velocity;
				g_clients[user_id].stime = tcl.stime;
				g_clients[user_id].tileX = tcl.tileX;
				g_clients[user_id].tileZ = tcl.tileZ;
				g_clients[user_id].m_cl.unlock();



				//모든 클라이언트에게 좀비 위치 전송 
				for (int i = 0; i < 10; ++i) {
					g_clients[i].m_cl.lock();
					if (g_clients[i].m_status == ST_ACTIVE && g_clients[i].scene == 1) {
						send_zombie_move_packet(i, user_id);
					}
					g_clients[i].m_cl.unlock();
				}
				//2초 뒤에 IDLE로 전환 
				if (tcl.stime >= ZOMBIE_WANDER_TIME) {
					g_clients[user_id].m_cl.lock();
					g_clients[user_id].stime = 0.0f;
					if (g_clients[user_id].bufferidx == (FINALBOSS - 3)) {
						g_clients[user_id].aniTrackUp = (rand() % 4);
					}
					else {
						g_clients[user_id].aniTrackUp = (rand() % 5);
					}
					g_clients[user_id].m_cl.unlock();
					add_timer(user_id, OP_NPC_IDLE, 10);
				}
				else {
					//아니면 계속 MOVE
					add_timer(user_id, OP_NPC_MOVE, 100);
				}
			}
		}
						  delete exover;
		break;
		case OP_NPC_IDLE: {
			copyClient tcl;
			g_clients[user_id].m_cl.lock();
			tcl.id = user_id;
			XMFLOAT3 tpos = g_clients[user_id].pos;
			g_clients[user_id].state = IDLE;
			tcl.look = g_clients[user_id].look;
			tcl.pos = g_clients[user_id].pos;
			tcl.state = g_clients[user_id].state;
			tcl.velocity = g_clients[user_id].velocity;
			tcl.stime = g_clients[user_id].stime;
			tcl.obbBox = g_clients[user_id].obbBox;
			tcl.tileX = g_clients[user_id].tileX;
			tcl.tileZ = g_clients[user_id].tileZ;
			g_clients[user_id].m_cl.unlock();

			//60분의 10초마다 움직이므로 10번 움직인다.
			for (int i = 0; i < 10; ++i) {
				MoveZombie(tcl, ZOMBIESPEED, false , tcl.look);
			}
			GetHeight(tcl);
			//섹터 계산 
			XMFLOAT3 cpos = tcl.pos;
			int tsx = int((tpos.x + 257.0f) / SECTOR_SIZE);
			int tsz = int((tpos.z + 257.0f) / SECTOR_SIZE);

			int csx = int((cpos.x + 257.0f) / SECTOR_SIZE);
			int csz = int((cpos.z + 257.0f) / SECTOR_SIZE);

			if (tsx != csx || csz != tsz) {
				s_cl[tsx][tsz].lock();
				SectorList[tsz][tsx].erase(user_id);
				s_cl[tsx][tsz].unlock();

				s_cl[csx][csz].lock();
				SectorList[csz][csx].insert(user_id);
				s_cl[csx][csz].unlock();
			}

			g_clients[user_id].m_cl.lock();
			g_clients[user_id].look = tcl.look;
			g_clients[user_id].pos = tcl.pos;
			g_clients[user_id].state = tcl.state;
			g_clients[user_id].velocity = XMFLOAT3(0, 0 , 0);
			g_clients[user_id].stime = tcl.stime;
			g_clients[user_id].m_cl.unlock();

			for (int i = 0; i < 10; ++i) {
				g_clients[i].m_cl.lock();
				if (g_clients[i].m_status == ST_ACTIVE && g_clients[i].scene == 1) {
					send_zombie_move_packet(i, user_id);
				}
				g_clients[i].m_cl.unlock();
			}

			
		}
		delete exover;
		break;
		case OP_NPC_CHASE: {

			copyClient tcl;
			XMFLOAT3 tpos;

			g_clients[user_id].m_cl.lock();
			tcl.isAstar = g_clients[user_id].isAstar;
			tcl.atvec = g_clients[user_id].atvec;
			tcl.id = user_id;
			tpos = g_clients[user_id].pos;
			tcl.look = g_clients[user_id].look;
			tcl.pos = g_clients[user_id].pos;
			tcl.state = g_clients[user_id].state;
			tcl.velocity = g_clients[user_id].velocity;
			tcl.stime = g_clients[user_id].stime;
			tcl.obbBox = g_clients[user_id].obbBox;
			tcl.target = g_clients[user_id].target;
			tcl.aniTrack = g_clients[user_id].aniTrackUp;
			tcl.tileX = g_clients[user_id].tileX;
			tcl.tileZ = g_clients[user_id].tileZ;
			tcl.bIdx = g_clients[user_id].bufferidx;
			tcl.hp = g_clients[user_id].hp;
			tcl.target = g_clients[user_id].target;
			g_clients[user_id].m_cl.unlock();

			if (tcl.hp > 0) {

				g_clients[tcl.target].m_cl.lock();
				XMFLOAT3 targetPos = g_clients[tcl.target].pos;
				g_clients[tcl.target].m_cl.unlock();

				//플레이어와 좀비 사이에 아무런 오브젝트가 없다면 
				float pzdist = 0;

				if (RayTest(targetPos, tcl.pos) == false) {
					XMFLOAT3 ztpos = Vector3::Subtract(targetPos, tcl.pos);
					ztpos.y = 0.0f;
					pzdist = Vector3::Length(ztpos);
					tcl.look = Vector3::Normalize(ztpos);
					tcl.isAstar = false;
					tcl.atvec.clear();
				}
				else if (tcl.isAstar == false) {
					// 에이스타 알고리즘 
					//플레이어와 자기 타일을 비교해서 look을 설정하자
					copyClient tgcl;
					g_clients[tcl.target].m_cl.lock();
					tgcl.id = tcl.target;
					tgcl.pos = g_clients[tcl.target].pos;
					tgcl.tileX = g_clients[tcl.target].tileX;
					tgcl.tileZ = g_clients[tcl.target].tileZ;
					g_clients[tcl.target].m_cl.unlock();
					
					auto flist = Astar(tcl, tgcl);
					int fsize = flist.size();
					XMFLOAT3 nodePos;
					nodePos = GetNodePos(flist[flist.size() - 1].x, flist[flist.size() - 1].z);
					nodePos.x = nodePos.x - 257.0f;
					nodePos.z = nodePos.z - 257.0f;
					XMFLOAT3 ztpos = Vector3::Subtract(nodePos, tcl.pos);
					ztpos.y = 0.0f;
					pzdist = Vector3::Length(ztpos);
					tcl.look = Vector3::Normalize(ztpos);
					pzdist = 10.0f;
					if (flist.size() != 0) {
						tcl.isAstar = true;
						for (auto& u : flist)
							tcl.atvec.emplace_back(u);
					}
				}
				else if (tcl.isAstar == true) {
					tcl.atvec;
					XMFLOAT3 nodePos;
					int nidx = 0;
					nodePos = GetNodePos(tcl.atvec[tcl.atvec.size() - 1].x, tcl.atvec[tcl.atvec.size() - 1].z);
					nodePos.x = nodePos.x - 257.0f;
					nodePos.z = nodePos.z - 257.0f;
					nodePos.y = 0.0f;

					XMFLOAT3 ztpos = Vector3::Subtract(nodePos, tcl.pos);
					ztpos.y = 0.0f;
					pzdist = Vector3::Length(ztpos);
					float popdist = pzdist;
					tcl.look = Vector3::Normalize(ztpos);
					pzdist = 10.0f;


					if (popdist < 1.5f) {
						tcl.atvec.pop_back();
					}
					if (tcl.atvec.size() != 0) {
						tcl.isAstar = true;
					}
					else {
						tcl.isAstar = false;
					}
				}

				if (tcl.bIdx == (FINALBOSS - 3)) {
					if (pzdist < BOSSATTACKDIST) {
						tcl.aniTrack = (rand() % 3) + BossPunch;
						tcl.velocity = XMFLOAT3(0, 0, 0);
						tcl.state = ATTACK;
					}
					else {
						tcl.state = CHASE;
					}
				}
				else {
					if (pzdist < ATTACKDIST) {
						tcl.aniTrack = (rand() % 3) + ZombieAttack1;
						tcl.velocity = XMFLOAT3(0, 0, 0);
						tcl.state = ATTACK;
					}
					else {
						tcl.state = CHASE;
					}
				}

				for (int i = 0; i < 10; ++i) {
					MoveZombie(tcl, ZOMBIERUN, false, tcl.look);
				}


				//섹터 계산 
				XMFLOAT3 cpos = tcl.pos;

				int tsx = int((tpos.x + 257.0f) / SECTOR_SIZE);
				int tsz = int((tpos.z + 257.0f) / SECTOR_SIZE);

				int csx = int((cpos.x + 257.0f) / SECTOR_SIZE);
				int csz = int((cpos.z + 257.0f) / SECTOR_SIZE);

				if (tsx != csx || csz != tsz) {
					s_cl[tsx][tsz].lock();
					SectorList[tsz][tsx].erase(user_id);
					s_cl[tsx][tsz].unlock();

					s_cl[csx][csz].lock();
					SectorList[csz][csx].insert(user_id);
					s_cl[csx][csz].unlock();
				}

				GetTile(tcl);
				GetHeight(tcl);

				int gchp;

				g_clients[user_id].m_cl.lock();
				g_clients[user_id].look = tcl.look;
				g_clients[user_id].pos = tcl.pos;
				g_clients[user_id].state = tcl.state;
				g_clients[user_id].velocity = tcl.velocity;
				g_clients[user_id].stime = tcl.stime;
				g_clients[user_id].tileX = tcl.tileX;
				g_clients[user_id].tileZ = tcl.tileZ;
				g_clients[user_id].target = tcl.target;
				g_clients[user_id].aniTrackUp = tcl.aniTrack;
				g_clients[user_id].isAstar = tcl.isAstar;
				g_clients[user_id].atvec = tcl.atvec;
				g_clients[user_id].target = tcl.target;
				gchp = g_clients[user_id].hp;
				g_clients[user_id].m_cl.unlock();


				if (gchp > 0) {
					//모든 클라이언트에게 좀비 위치 전송 
					for (int i = 0; i < 10; ++i) {
						g_clients[i].m_cl.lock();
						if (g_clients[i].m_status == ST_ACTIVE && g_clients[i].scene == 1) {
							send_zombie_move_packet(i, user_id);
						}
						g_clients[i].m_cl.unlock();
					}


					//아니면 계속 MOVE
					if (g_clients[user_id].bufferidx == (FINALBOSS - 3)) {
					if (pzdist < BOSSATTACKDIST) {
						if (tcl.aniTrack == BossPunch)
							add_timer(user_id, OP_NPC_ATTACK, 1060);
						if (tcl.aniTrack == BossSwiping)
							add_timer(user_id, OP_NPC_ATTACK, 2400);
						if (tcl.aniTrack == BossJumpAttack)
							add_timer(user_id, OP_NPC_ATTACK, 3210);
						}
						else {
							add_timer(user_id, OP_NPC_CHASE, 100);
						}
					}
					else {
						if (pzdist < ATTACKDIST) {

							if (tcl.aniTrack == ZombieAttack1)
								add_timer(user_id, OP_NPC_ATTACK, 4360);
							if (tcl.aniTrack == ZombieAttack2)
								add_timer(user_id, OP_NPC_ATTACK, 2370);
							if (tcl.aniTrack == ZombieKick)
								add_timer(user_id, OP_NPC_ATTACK, 3150);

						}
						else {
							add_timer(user_id, OP_NPC_CHASE, 100);

						}
					}
				}
				else {
					g_clients[user_id].m_cl.lock();
					g_clients[user_id].state = DEATH;
					g_clients[user_id].m_cl.unlock();
				}

			}
			
		}
		delete exover;
		break;
		case OP_NPC_ATTACK: {
			copyClient tcl;
			XMFLOAT3 tpos;
			g_clients[user_id].m_cl.lock();
			tcl.id = user_id;
			tpos = g_clients[user_id].pos;
			tcl.look = g_clients[user_id].look;
			tcl.pos = g_clients[user_id].pos;
			tcl.state = g_clients[user_id].state;
			tcl.velocity = g_clients[user_id].velocity;
			tcl.stime = g_clients[user_id].stime;
			tcl.obbBox = g_clients[user_id].obbBox;
			tcl.target = g_clients[user_id].target;
			tcl.tileX = g_clients[user_id].tileX;
			tcl.tileZ = g_clients[user_id].tileZ;
			tcl.isAstar = g_clients[user_id].isAstar;
			tcl.atvec = g_clients[user_id].atvec;
			tcl.bIdx = g_clients[user_id].bufferidx;
			tcl.hp = g_clients[user_id].hp;
			tcl.target = g_clients[user_id].target;
			g_clients[user_id].m_cl.unlock();

			if (tcl.hp > 0) {

				g_clients[tcl.target].m_cl.lock();
				XMFLOAT3 targetPos = g_clients[tcl.target].pos;
				g_clients[tcl.target].m_cl.unlock();

				XMFLOAT3 ztpos = Vector3::Subtract(targetPos, tcl.pos);
				ztpos.y = 0.0f;
				float pzdist = Vector3::Length(ztpos);
				tcl.look = Vector3::Normalize(ztpos);

				if (tcl.bIdx == (FINALBOSS - 3)) {
					if (pzdist < BOSSATTACKDIST) {
						tcl.aniTrack = (rand() % 3) + BossPunch;
						tcl.velocity = XMFLOAT3(0, 0, 0);
						tcl.state = ATTACK;
					}
					else {
						tcl.aniTrack = BossRun;
						tcl.state = CHASE;
					}
				}
				else {
					if (pzdist < ATTACKDIST) {


						tcl.aniTrack = (rand() % 3) + ZombieAttack1;
						tcl.velocity = XMFLOAT3(0, 0, 0);
						tcl.state = ATTACK;
					}
					else {
						tcl.aniTrack = (rand() % 4) + ZombieRun;
						tcl.state = CHASE;
					}
				}
				for (int i = 0; i < 100; ++i) {
					MoveZombie(tcl, 0, false, tcl.look);
				}



				//섹터 계산 
				XMFLOAT3 cpos = tcl.pos;

				int tsx = int((tpos.x + 257.0f) / SECTOR_SIZE);
				int tsz = int((tpos.z + 257.0f) / SECTOR_SIZE);

				int csx = int((cpos.x + 257.0f) / SECTOR_SIZE);
				int csz = int((cpos.z + 257.0f) / SECTOR_SIZE);

				if (tsx != csx || csz != tsz) {
					s_cl[tsx][tsz].lock();
					SectorList[tsz][tsx].erase(user_id);
					s_cl[tsx][tsz].unlock();


					s_cl[csx][csz].lock();
					SectorList[csz][csx].insert(user_id);
					s_cl[csx][csz].unlock();
				}


				GetTile(tcl);
				GetHeight(tcl);

				int gchp;
				g_clients[user_id].m_cl.lock();
				g_clients[user_id].look = tcl.look;
				g_clients[user_id].pos = tcl.pos;
				g_clients[user_id].state = tcl.state;
				g_clients[user_id].velocity = tcl.velocity;
				g_clients[user_id].stime = tcl.stime;
				g_clients[user_id].tileX = tcl.tileX;
				g_clients[user_id].tileZ = tcl.tileZ;
				g_clients[user_id].target = tcl.target;
				g_clients[user_id].aniTrackUp = tcl.aniTrack;
				g_clients[user_id].isAstar = tcl.isAstar;
				g_clients[user_id].atvec = tcl.atvec;
				g_clients[user_id].target = tcl.target;
				gchp = g_clients[user_id].hp;
				g_clients[user_id].m_cl.unlock();

				
				if (gchp > 0) {
					//모든 클라이언트에게 좀비 위치 전송 
					for (int i = 0; i < 10; ++i) {
						g_clients[i].m_cl.lock();
						if (g_clients[i].m_status == ST_ACTIVE && g_clients[i].scene == 1) {
							send_zombie_move_packet(i, user_id);
						}
						g_clients[i].m_cl.unlock();
					}
					//아니면 계속 MOVE
					if (g_clients[user_id].bufferidx == (FINALBOSS - 3)) {
						if (pzdist < BOSSATTACKDIST) {
							if (tcl.aniTrack == BossPunch)
								add_timer(user_id, OP_NPC_ATTACK, 1060);
							if (tcl.aniTrack == BossSwiping)
								add_timer(user_id, OP_NPC_ATTACK, 2400);
							if (tcl.aniTrack == BossJumpAttack)
								add_timer(user_id, OP_NPC_ATTACK, 3210);
						}
						else {
							add_timer(user_id, OP_NPC_CHASE, 100);
						}
					}
					else {
						if (pzdist < ATTACKDIST) {

							if (tcl.aniTrack == ZombieAttack1)
								add_timer(user_id, OP_NPC_ATTACK, 4360);
							if (tcl.aniTrack == ZombieAttack2)
								add_timer(user_id, OP_NPC_ATTACK, 2370);
							if (tcl.aniTrack == ZombieKick)
								add_timer(user_id, OP_NPC_ATTACK, 3150);

						}
						else {
							add_timer(user_id, OP_NPC_CHASE, 100);

						}
					}
				}
			else
				{
					g_clients[user_id].m_cl.lock();
					g_clients[user_id].state = DEATH;
					g_clients[user_id].m_cl.unlock();
				}
			}
		}
		delete exover;
		break;
		}
				


	}
}

void do_timer()
{
	while (true) {
		this_thread::sleep_for(1ms);
		while (true) {
			timer_lock.lock();
			if (true == timer_queue.empty()) {
				timer_lock.unlock();
				break;
			}
			auto now_t = high_resolution_clock::now();
			event_type temp_ev = timer_queue.top();
			if (timer_queue.top().time > high_resolution_clock::now()) {
				timer_lock.unlock();
				break;
			}
			event_type ev = timer_queue.top();
			timer_queue.pop();
			timer_lock.unlock();
			switch (ev.event_id) {
			case OP_NPC_WANDER:{
				EXOVER* over = new EXOVER;
				over->op = ev.event_id;
				PostQueuedCompletionStatus(g_iocp, 1, ev.id, &over->overlapped);
			}
				break;
			case OP_NPC_MOVE: {
				EXOVER* over = new EXOVER;
				over->op = ev.event_id;
				PostQueuedCompletionStatus(g_iocp, 1, ev.id, &over->overlapped);
			}
				break;
			case OP_NPC_IDLE: {
				EXOVER* over = new EXOVER;
				over->op = ev.event_id;
				PostQueuedCompletionStatus(g_iocp, 1, ev.id, &over->overlapped);
			}
			break;
			case OP_NPC_CHASE: {
				EXOVER* over = new EXOVER;
				over->op = ev.event_id;
				PostQueuedCompletionStatus(g_iocp, 1, ev.id, &over->overlapped);
			}
		    break;
			case OP_NPC_ATTACK: {
				EXOVER* over = new EXOVER;
				over->op = ev.event_id;
				PostQueuedCompletionStatus(g_iocp, 1, ev.id, &over->overlapped);
			}
			break;
			
			}
		}
	}
}


int main()
{
	WSADATA WSAData;
	WSAStartup(MAKEWORD(2, 2), &WSAData);

	listen_socket = WSASocketW(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);

	SOCKADDR_IN sock_addr;
	memset(&sock_addr, 0, sizeof(sock_addr));
	sock_addr.sin_family = AF_INET;
	sock_addr.sin_port = htons(SERVER_PORT);
	sock_addr.sin_addr.S_un.S_addr = htonl(INADDR_ANY);
	::bind(listen_socket, reinterpret_cast<sockaddr*>(&sock_addr), sizeof(sock_addr));

	listen(listen_socket, SOMAXCONN);

	g_iocp = CreateIoCompletionPort(INVALID_HANDLE_VALUE, NULL, NULL, 0);
	
	
	curMap = 0;
	initialize_clients();

	mapinform[0].serverdata = "Map/map_00_server.txt";
	mapinform[0].nodedata = "Map/map_00_server_node.txt";

	mapinform[1].serverdata = "Map/map_08_server.txt";
	mapinform[1].nodedata = "Map/map_08_server_node.txt";

	mapinform[2].serverdata = "Map/map_08_server.txt";
	mapinform[2].nodedata = "Map/map_08_server_node.txt";

	mapinform[3].serverdata = "Map/map_08_server.txt";
	mapinform[3].nodedata = "Map/map_08_server_node.txt";

	mapinform[4].serverdata = "Map/map_08_server.txt";
	mapinform[4].nodedata = "Map/map_08_server_node.txt";

	mapinform[5].serverdata = "Map/map_08_server.txt";
	mapinform[5].nodedata = "Map/map_08_server_node.txt";

	mapinform[6].serverdata = "Map/map_08_server.txt";
	mapinform[6].nodedata = "Map/map_08_server_node.txt";

	mapinform[7].serverdata = "Map/map_08_server.txt";
	mapinform[7].nodedata = "Map/map_08_server_node.txt";

	mapinform[8].serverdata = "Map/map_08_server.txt";
	mapinform[8].nodedata = "Map/map_08_server_node.txt";



	LoadMap(mapinform[0].serverdata , mapinform[0].nodedata , curMap);
	ConvertZombie();

	CreateIoCompletionPort(reinterpret_cast<HANDLE>(listen_socket), g_iocp, 999, 0); // listen_sock을 IOCP에 등록
	SOCKET client_socket = WSASocketW(AF_INET, SOCK_STREAM, 0, NULL, 0, WSA_FLAG_OVERLAPPED);
	EXOVER accept_over;
	ZeroMemory(&(accept_over.overlapped), sizeof(accept_over.overlapped));
	accept_over.op = OP_ACCEPT;
	// accpet에서는 accept_over.wsabuf를 사용하지 않기때문에 초기화하지 않아도 된다.

	accept_over.c_socket = client_socket;
	AcceptEx(listen_socket, client_socket, accept_over.io_buf, NULL, sizeof(sockaddr_in) + 16, sizeof(sockaddr_in) + 16, NULL, &(accept_over.overlapped));
	// 원래 accept는 listen_sock에서 clien_sock를 만들어서 리턴한다. 하지만 이 함수의 동작은 다르다.
	// 소켓을 미리 만들어놓고 클라이언트에 연결을 시켜주는데 두 번째 인자에 연결 될 소켓을 넣으면 된다.


	vector<thread> worker_threads;
	
	for (int i = 0; i < 4; ++i)
		worker_threads.emplace_back(worker_thread);
	thread timer_thread{ do_timer };
	for (auto& th : worker_threads)
		th.join();

	return 0;
}