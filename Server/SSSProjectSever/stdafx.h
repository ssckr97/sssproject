#pragma once

#include <iostream>
#include <WS2tcpip.h>
#include <MSWSock.h>
#include <vector>
#include <thread>
#include <mutex>
#include <atomic>
#include <queue>
#include <concurrent_unordered_set.h>
#include <unordered_set>
#include <map>
#include <set>
#define _WINSOCK_DEPRECATED_NO_WARNINGS
#pragma comment (lib, "WS2_32.lib")
#pragma comment (lib, "mswsock.lib")

#include <d3d12.h>
#include <DirectXMath.h>
#include <DirectXCollision.h>
using namespace std;
using namespace DirectX;
using namespace chrono;
#define ZOMBIE_START_NUMBER 1000
#define ZOMBIE_WANDER_TIME 5000
#define ZOMBIESPEED 5.0f

#define ZOMBIERUN  25.0f
#define ZOMBIEBOUNCING 25.0f
#define ZOMBIECHASEDIST 20.0f

#define ATTACKDIST 1.5f
#define BOSSATTACKDIST 3.0f
#define EPSILION				1.0e-10f
#define GRAVITY -250
#define PLAYERSPEED 10.0f
#define FRICTION 250
#define PI 3.141592
#define NOPARENTS -1
#define MIDDLEBOSS 8
#define FINALBOSS 14
#define NOPOTAL 100
inline bool IsZero(float value) { return ((fabsf(value) < EPSILION)); }
inline bool IsZero(float fValue, float fEpsilon) { return((fabsf(fValue) < fEpsilon)); }
inline bool IsEqual(float A, float B) { return (::IsZero(A - B)); }
inline bool IsEqual(float fA, float fB, float fEpsilon) { return(::IsZero(fA - fB, fEpsilon)); }
inline float InverseSqrt(float value) { return 1.0f / sqrtf(value); }
inline void Swap(float* S, float* T) { float temp = *S; *S = *T; *T = temp; }

enum PlayerJob
{
	PlayerJobRifle = 0,
	PlayerJobSword,
	PlayerJobBow
};
enum PlayerDirection
{
	DirectionForword = 2,
	DirectionBackword = 4,
	DirectionRight = 7,
	DirectionLeft = 8,

	DirectionFR = DirectionForword + DirectionRight,
	DirectionFL = DirectionForword + DirectionLeft,
	DirectionBR = DirectionBackword + DirectionRight,
	DirectionBL = DirectionBackword + DirectionLeft,



	DirectionUp = 0x10,
	DirectionDown = 0x20
};

enum ZombieAnimationChange {
	ZombieDizzIdle,
	ZombieDrunkIdle,
	ZombieHeadMoveIdle,
	ZombieMiddleHandle,
	ZombieScreamIdle,
	ZombieWalk1,
	ZombieWalk2,
	ZombieWalk3,
	ZombieRun,
	InjuredRun,
	ShakeHandRun,
	HeadFreeRun,
	ZombieAttack1,
	ZombieAttack2,
	ZombieKick,
	ZombieReaction1,
	ZombieReaction2,
	ZombieMiddleReaction,
	ZombieDeath1,
	ZombieDeath2
};
enum BossAnimationChange {
	BossIdle,
	BossIdle2,
	BossIdle3,
	BossIdle4,
	BossRoar,
	BossWalk,
	BossRun,
	BossPunch,
	BossSwiping,
	BossJumpAttack,
	BossDeath
};




//3차원 벡터의 연산
namespace Vector3
{
	// XMVECTOR의 값을 XMFLOAT3로 변환시켜주는 함수
	inline XMFLOAT3 XMVectorToFloat3(const XMVECTOR& vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, vector);
		return(result);
	}

	// XMFLOAT3의 스칼라 곱 연산 (정규화된 벡터로 연산 수행)
	inline XMFLOAT3 ScalarProduct(const XMFLOAT3& vector, float scalar, bool normalize = true)
	{
		XMFLOAT3 result;
		if (normalize)
			XMStoreFloat3(&result, XMVector3Normalize(XMLoadFloat3(&vector)) *scalar);
		else
			XMStoreFloat3(&result, XMLoadFloat3(&vector) * scalar);
		return(result);
	}

	// XMFLOAT3 두개의 덧셈 연산
	inline XMFLOAT3 Add(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMLoadFloat3(&vector1) + XMLoadFloat3(&vector2));
		return(result);
	}

	// XMFLOAT3 두개의 덧셈 연산( 두번째 값은 스칼라 곱 후 더함 )
	inline XMFLOAT3 Add(const XMFLOAT3& vector1, const XMFLOAT3& vector2, float scalar)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMLoadFloat3(&vector1) + (XMLoadFloat3(&vector2) *scalar));
		return(result);
	}

	// XMFLOAT3 두개의 뺄셈 연산
	inline XMFLOAT3 Subtract(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMLoadFloat3(&vector1) - XMLoadFloat3(&vector2));
		return(result);
	}

	// XMFLOAT3 두개의 내적 연산
	inline float DotProduct(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3Dot(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2)));
		return(result.x);
	}

	// XMFLOAT3 두개의 외적 연산 (정규화된 벡터로 연산 수행)
	inline XMFLOAT3 CrossProduct(const XMFLOAT3& vector1, const XMFLOAT3& vector2, bool normalize = true)
	{
		XMFLOAT3 result;
		if (normalize)
			XMStoreFloat3(&result, XMVector3Normalize(XMVector3Cross(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2))));
		else
			XMStoreFloat3(&result, XMVector3Cross(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2)));
		return(result);
	}

	// XMFLOAT3 의 정규화 연산
	inline XMFLOAT3 Normalize(const XMFLOAT3& vector)
	{
		XMFLOAT3 normal;
		XMStoreFloat3(&normal, XMVector3Normalize(XMLoadFloat3(&vector)));
		return(normal);
	}

	// XMFLOAT3 의 크기를 구하는 함수
	inline float Length(const XMFLOAT3& vector)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3Length(XMLoadFloat3(&vector)));
		return(result.x);
	}

	// XMFLOAT3 두개 사이의 각도를 구하는 함수 (도 값으로 리턴함)
	inline float Angle(const XMVECTOR& vector1, const XMVECTOR& vector2)
	{
		XMVECTOR angle = XMVector3AngleBetweenNormals(vector1, vector2);
		return(XMConvertToDegrees(acosf(XMVectorGetX(angle))));
		// 라디안 값을 도 값으로 바꾸어 반환
	}

	// XMFLOAT3 두개 사이의 각도를 구하는 함수 (도 값으로 리턴함)
	inline float Angle(const XMFLOAT3& vector1, const XMFLOAT3& vector2)
	{
		return(Angle(XMLoadFloat3(&vector1), XMLoadFloat3(&vector2)));
	}

	// XMFLOAT3 와 행렬 연산 (입력 벡터는 노멀 벡터)
	// 회전 및 스케일링을 위해 입력 행 0, 1, 2를 사용하여 변환을 수행하고 행 3을 무시합니다.
	inline XMFLOAT3 TransformNormal(const XMFLOAT3& vector, const XMMATRIX& transform)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3TransformNormal(XMLoadFloat3(&vector), transform));
		return(result);
	}

	// XMFLOAT3 와 행렬 연산 
	// 입력 벡터의 w 구성 요소를 무시하고 대신 1.0 값을 사용합니다. 반환되는 벡터의 w 구성 요소는 항상 1.0입니다.
	inline XMFLOAT3 TransformCoord(const XMFLOAT3& vector, const XMMATRIX& transform)
	{
		XMFLOAT3 result;
		XMStoreFloat3(&result, XMVector3TransformCoord(XMLoadFloat3(&vector), transform));
		return(result);
	}

	// XMFLOAT3 와 행렬 연산 
	inline XMFLOAT3 TransformCoord(const XMFLOAT3& vector, const XMFLOAT4X4& matrix)
	{
		return(TransformCoord(vector, XMLoadFloat4x4(&matrix)));
	}

	// 3-차원 벡터가 영벡터인 가를 반환하는 함수이다
	inline bool IsZero(XMFLOAT3& vector)
	{
		if (::IsZero(vector.x) && ::IsZero(vector.y) && ::IsZero(vector.z))
			return (true);
		return false;
	}

}
//4차원 벡터의 연산
namespace Vector4
{
	// XMFLAOT4 두개의 덧셈 연산
	inline XMFLOAT4 Add(const XMFLOAT4& vector1, const XMFLOAT4& vector2)
	{
		XMFLOAT4 result;
		XMStoreFloat4(&result, XMLoadFloat4(&vector1) + XMLoadFloat4(&vector2));
		return(result);
	}

	// 4-차원 벡터와 스칼라(실수)의 곱을 반환하는 함수이다.
	inline XMFLOAT4 Multiply(float scalar, XMFLOAT4& vector)
	{
		XMFLOAT4 result;
		XMStoreFloat4(&result, scalar * XMLoadFloat4(&vector));
		return(result);
	}
}
//행렬의 연산
namespace Matrix4x4
{
	// 단위행렬 생성
	inline XMFLOAT4X4 Identity()
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixIdentity());
		return(result);
	}

	// 행렬 두개의 곱을 연산
	inline XMFLOAT4X4 Multiply(const XMFLOAT4X4& matrix1, const XMFLOAT4X4& matrix2)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMLoadFloat4x4(&matrix1) * XMLoadFloat4x4(&matrix2));
		return(result);
	}

	// 행렬 두개의 곱을 연산
	inline XMFLOAT4X4 Multiply(const XMFLOAT4X4& matrix1, const XMMATRIX& matrix2)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMLoadFloat4x4(&matrix1) * matrix2);
		return(result);
	}

	// 행렬 두개의 곱을 연산
	inline XMFLOAT4X4 Multiply(const XMMATRIX& matrix1, const XMFLOAT4X4& matrix2)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, matrix1 * XMLoadFloat4x4(&matrix2));
		return(result);
	}

	// 행렬의 역행렬을 구하는 함수
	inline XMFLOAT4X4 Inverse(const XMFLOAT4X4& matrix)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixInverse(NULL, XMLoadFloat4x4(&matrix)));
		return(result);
	}

	// 0.0f 의 값으로 채워진 4x4 행렬
	inline XMFLOAT4X4 Zero()
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4(&xmf4x4Result, XMMatrixSet(0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f, 0.0f));
		return(xmf4x4Result);
	}

	// 두 행렬을 보간 계산하는 함수
	inline XMFLOAT4X4 Interpolate(XMFLOAT4X4& xmf4x4Matrix1, XMFLOAT4X4& xmf4x4Matrix2, float t)
	{
		XMFLOAT4X4 xmf4x4Result;
		XMVECTOR S0, R0, T0, S1, R1, T1;
		XMMatrixDecompose(&S0, &R0, &T0, XMLoadFloat4x4(&xmf4x4Matrix1));
		XMMatrixDecompose(&S1, &R1, &T1, XMLoadFloat4x4(&xmf4x4Matrix2));
		XMVECTOR S = XMVectorLerp(S0, S1, t);
		XMVECTOR T = XMVectorLerp(T0, T1, t);
		XMVECTOR R = XMQuaternionSlerp(R0, R1, t);
		XMStoreFloat4x4(&xmf4x4Result, XMMatrixAffineTransformation(S, XMVectorZero(), R, T));
		return(xmf4x4Result);
	}

	// 행렬의 스케일을 늘리는 함수
	inline XMFLOAT4X4 Scale(XMFLOAT4X4& xmf4x4Matrix, float fScale)
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4(&xmf4x4Result, XMLoadFloat4x4(&xmf4x4Matrix) * fScale);
		/*
				XMVECTOR S, R, T;
				XMMatrixDecompose(&S, &R, &T, XMLoadFloat4x4(&xmf4x4Matrix));
				S = XMVectorScale(S, fScale);
				T = XMVectorScale(T, fScale);
				R = XMVectorScale(R, fScale);
				//R = XMQuaternionMultiply(R, XMVectorSet(0, 0, 0, fScale));
				XMStoreFloat4x4(&xmf4x4Result, XMMatrixAffineTransformation(S, XMVectorZero(), R, T));
		*/
		return(xmf4x4Result);
	}

	// 두 행렬을 더하는 함수
	inline XMFLOAT4X4 Add(XMFLOAT4X4& xmmtx4x4Matrix1, XMFLOAT4X4& xmmtx4x4Matrix2)
	{
		XMFLOAT4X4 xmf4x4Result;
		XMStoreFloat4x4(&xmf4x4Result, XMLoadFloat4x4(&xmmtx4x4Matrix1) + XMLoadFloat4x4(&xmmtx4x4Matrix2));
		return(xmf4x4Result);
	}

	// 행렬의 전치행렬을 구하는 함수
	inline XMFLOAT4X4 Transpose(const XMFLOAT4X4& xmmtx4x4Matrix)
	{
		XMFLOAT4X4 xmmtx4x4Result;
		XMStoreFloat4x4(&xmmtx4x4Result, XMMatrixTranspose(XMLoadFloat4x4(&xmmtx4x4Matrix)));
		return(xmmtx4x4Result);
	}

	// 시야 기반 왼손좌표계 투영변환행렬을 만든다. 
	// FovAngleY:하향시야각(라디안), AspectRatio:뷰 공간의 가로,세로 비율X:Y, NearZ:근평면까지의 거리(0이상), FarZ:원평면까지의 거리(0이상)
	inline XMFLOAT4X4 PerspectiveFovLH(float fovAngleY, float aspectRatio, float nearZ, float farZ)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixPerspectiveFovLH(fovAngleY, aspectRatio, nearZ, farZ));
		return(result);
	}

	// 카메라의 위치, Look벡터, Up벡터를 사용하여 왼손좌표계의 카메라변환 행렬을 만든다.
	inline XMFLOAT4X4 LookAtLH(const XMFLOAT3& eyePosition, const XMFLOAT3& lookAtPosition, const XMFLOAT3& upDirection)
	{
		XMFLOAT4X4 result;
		XMStoreFloat4x4(&result, XMMatrixLookAtLH(XMLoadFloat3(&eyePosition), XMLoadFloat3(&lookAtPosition), XMLoadFloat3(&upDirection)));
		return(result);
	}
}
