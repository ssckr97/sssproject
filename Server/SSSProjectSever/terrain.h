#pragma once
#include <fstream>
#include"stdafx.h"
#include "zombie.h"

int astarBuffer[257][257];
vector<int> GoneMap;
struct MapInform {
	string  serverdata;
	string  nodedata;
};
BoundingBox tob[OBJSECTOR_WIDTH][OBJSECTOR_WIDTH];
MapInform mapinform[9];


struct POINTXZ {
	int x;
	int z;
};

struct AstarInform {
	int x;
	int z;

	int Parentx = -1;
	int Parentz = -1;
	float F = 0;
	float G = 0;
	float H = 0;
	constexpr bool operator < (const AstarInform& left) const
	{
		return (F > left.F);
	}
};


XMFLOAT3 *tVertices;
int tWidth = 257;
int tLength = 257;
int navibuffer[257][257]; // 257 by 257 타일 정보 
XMFLOAT4X4 terrainWorld;
int bufferSize;

struct obj {
	XMFLOAT4X4 objworld;
	std::vector<BoundingOrientedBox> obbvec;
};
struct gbuffer {
	vector<obj> objvec;
	std::vector<XMFLOAT4X4> bwvec;
	
};
std::vector<gbuffer> gbvec;

std::vector<obj> objvec;

XMFLOAT3 GetNodePos(int x, int z) {
	return XMFLOAT3(x * 2.0f, 0.0f, z * 2.0f);
}
void SaveMap() {
	int astarBuffer[257][257];
	for (int i = 0; i < 257; ++i) {
		memset(astarBuffer[i], 0, sizeof(int) * 257);
	}
	for (int i = 0; i < 257; ++i) {
		for (int j = 0; j < 257; ++j) {
			for (int k = 0; k < objvec.size(); ++k) {
				for (int l = 0; l < objvec[k].obbvec.size(); ++l) {
					XMFLOAT3 pos = GetNodePos(j, i);
					pos.y = 100.f;
					XMFLOAT3 rayorigin = pos;
					XMFLOAT3 raydir = XMFLOAT3(0, -1, 0);


					BoundingOrientedBox toriBox = objvec[k].obbvec[l];
					toriBox.Center.x = toriBox.Center.x + 257.0f;
					toriBox.Center.z = toriBox.Center.z + 257.0f;
					toriBox.Extents = Vector3::Add(toriBox.Extents, XMFLOAT3(3.0f , 0.0f , 3.0f));


					auto ro = XMLoadFloat3(&rayorigin);
					auto rd = XMLoadFloat3(&raydir);
					float rdist = FLT_MAX;
					if (toriBox.Intersects(ro, rd, rdist)) {
						astarBuffer[i][j] = -1;
					}


				}
			}
		}
	}

	ofstream tout{ "testmap_node.txt" , ios::out | ios::binary };

	for (int i = 0; i < 257; ++i) {
		tout.write((char*)&astarBuffer[i], sizeof(int) * 257);
	}


}

void LoadMap(string serverdata , string nodedata , int num) {

	bool isGone = false;
	for (int i = 0; i < GoneMap.size(); ++i) {
		if (GoneMap[i] == num) {
			isGone = true;
		}
	}


	for (int i = 0; i < gbvec.size(); ++i) {
		gbvec[i].bwvec.clear();
		gbvec[i].objvec.clear();

	}

	for (int i = 0; i < sbuffer.size(); ++i) {
		sbuffer[i].sObjvec.clear();
	}

	sbuffer.clear();
	gbvec.clear();

	for (int z = 0; z < OBJSECTOR_WIDTH; ++z) {
		for (int x = 0; x < OBJSECTOR_WIDTH; ++x) {
			objSectorList[z][x].clear();
		}
	}

	if (tVertices) {
		delete[] tVertices;
		tVertices = NULL;
	}


	ifstream in(serverdata.c_str(), std::ios_base::in | std::ios::binary);

	int nVertices;
	in.read((char*)&nVertices, sizeof(int));
	tVertices = new XMFLOAT3[nVertices];
	in.read((char*)&tVertices[0], sizeof(XMFLOAT3) * nVertices);
	in.read((char*)&terrainWorld, sizeof(XMFLOAT4X4));
	//지형의 월드변환 행렬을 받아온다 .
	in.read((char*)&bufferSize, sizeof(int));
	//버퍼 사이즈를 받아온다.
	gbvec.resize(bufferSize);

	for (int i = 0; i < gbvec.size(); ++i) {
		int obs;
		in.read((char*)&obs, sizeof(int));
		//바운딩 박스 사이즈를 읽어옴
		for (int j = 0; j < obs; ++j) {
			XMFLOAT4X4 bworld;
			in.read((char*)&bworld, sizeof(XMFLOAT4X4));
			gbvec[i].bwvec.emplace_back(bworld);
			//바운딩박스 월드를 읽어옴
			
		}

		int objs;
		in.read((char*)&objs, sizeof(int));
		//오브젝트 개수를 읽어옴 
		gbvec[i].objvec.resize(objs);
		for (int j = 0; j < objs; ++j) {
			XMFLOAT4X4 objWorld;
			in.read((char*)&objWorld, sizeof(XMFLOAT4X4));
			gbvec[i].objvec[j].objworld = objWorld;
			//오브젝트의 월드를 읽어옴 
			for (int k = 0; k < gbvec[i].bwvec.size(); ++k) {
				BoundingOrientedBox tobb;
				tobb.Center = XMFLOAT3(0.0f, 0.0f, 0.0f);
				tobb.Extents = XMFLOAT3(2.0f, 2.0f, 2.0f);
				tobb.Orientation = XMFLOAT4(0, 0, 0, 1.f);
				tobb.Transform(tobb, XMLoadFloat4x4(&gbvec[i].bwvec[k]));
				tobb.Transform(tobb, XMLoadFloat4x4(&objWorld));
				gbvec[i].objvec[j].obbvec.emplace_back(tobb);
			}
			
		}

	}

	auto terraininverse = Matrix4x4::Inverse(terrainWorld);
	in.read((char*)&zombieStart, sizeof(int));

	if (isGone == false) {
	int sbufferSize;
	in.read((char*)&sbufferSize, sizeof(int));

	sbuffer.resize(sbufferSize);

	
	
		for (int i = 0; i < sbuffer.size() - zombieStart; ++i) {
			int sobjSize;
			in.read((char*)&sobjSize, sizeof(int));
			sbuffer[i].sObjvec.resize(sobjSize);
			for (int j = 0; j < sobjSize; ++j) {
				XMFLOAT4X4 sWorld;
				in.read((char*)&sWorld, sizeof(XMFLOAT4X4));
				sbuffer[i].sObjvec[j].world = sWorld;
				BoundingOrientedBox oribox;
				oribox.Center = XMFLOAT3(0, 0, 0);
				oribox.Extents = XMFLOAT3(0.1f, 0.1f, 0.1f);
				oribox.Orientation = XMFLOAT4(0, 0, 0, 1.0f);
				auto bworld = XMLoadFloat4x4(&sWorld);
				oribox.Transform(oribox, bworld);
				sbuffer[i].sObjvec[j].obbBox = oribox;
			}
		}
	}	

	//스킨 오브젝트 정보를 얻어옴 
	cout << "loadmap complete" << endl;
	in.close();



	for (int i = 0; i < 257; ++i) {
		memset(navibuffer[i], 0, sizeof(int) * 257);
	}

	std::string arm = nodedata;
	std::ifstream inrm{ arm , std::ios::in | std::ios::binary };

	for (int i = 0; i < 257; ++i) {
		inrm.read((char*)&navibuffer[i], sizeof(int) * 257);
	}

	inrm.close();

	for (int i = 0; i < gbvec.size(); ++i) {
		for (int j = 1; j < gbvec[i].objvec.size(); ++j) {
			objvec.emplace_back(gbvec[i].objvec[j]);
		}
	}


	

	tob[OBJSECTOR_WIDTH][OBJSECTOR_WIDTH];

	
	for (int z = 0; z < OBJSECTOR_WIDTH; ++z) {
		for (int x = 0; x < OBJSECTOR_WIDTH; ++x) {
			tob[z][x].Center.x = (x * OBJSECTOR_SIZE) + (OBJSECTOR_SIZE / 2);
			tob[z][x].Center.z = (z * OBJSECTOR_SIZE) + (OBJSECTOR_SIZE / 2);
			tob[z][x].Center.y = 0.0f;

			tob[z][x].Extents = XMFLOAT3(8.0f, 300.0f, 8.0f);
			
			tob[z][x].Transform(tob[z][x], XMLoadFloat4x4(&terrainWorld)); //월드공간으로 변환 
			for (int i = 0; i < objvec.size(); ++i) {
				for (int j = 0; j < objvec[i].obbvec.size(); ++j) {
					if (tob[z][x].Intersects(objvec[i].obbvec[j])) {
						objSectorList[z][x].insert(i);
						break;
					}
				}
			}
		}

	}


	GoneMap.emplace_back(num);
	

	//SaveMap();
	

	cout << "obj 섹터 구성 끝" << endl;
}

//
//seek zombie tile 


