#pragma once
#include "stdafx.h"

unordered_set<int> SectorList[(WORLD_WIDTH / SECTOR_SIZE) + 1][(WORLD_HEIGHT / SECTOR_SIZE) + 1]; // terrain ����ǥ�� sector
int zombieStart;
mutex  s_cl[(WORLD_WIDTH / SECTOR_SIZE) + 1][(WORLD_HEIGHT / SECTOR_SIZE) + 1];

unordered_set<int> objSectorList[(WORLD_WIDTH / OBJSECTOR_SIZE) + 1][(WORLD_HEIGHT / OBJSECTOR_SIZE) + 1]; // ���� ���� 


enum {
	IDLE,
	WANDER,
	CHASE,
	ATTACK,
	DEATH
};


struct skinedObj {
	XMFLOAT4X4 world;
	BoundingOrientedBox obbBox;
	int state = WANDER;
	XMFLOAT3 GetPos() {
		return XMFLOAT3(world._41, world._42, world._43);
	}
	XMFLOAT3 GetLook() {
		return XMFLOAT3(world._31, world._32, world._33);
	}
	void SetPos(XMFLOAT3 pos) {
		world._41 = pos.x;
		world._42 = pos.y;
		world._43 = pos.z;
	}

	XMFLOAT3 GetTerrainPos() {
		return XMFLOAT3(world._41 + 257.0f, world._42, world._43 + 257.0f);
	}

};
struct sBuffer{
	std::vector<skinedObj> sObjvec;


};

std::vector<sBuffer> sbuffer;


